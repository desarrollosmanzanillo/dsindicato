﻿<?php 
//phpinfo();
require_once("php/conexion/soluciones-mysql.php");
require_once("php/conexion/sistema.php");
session_start();
$pagina = new sistema($_GET['ePagina']);
$pagina->redirigirInicio();
$nombreSeccion=str_replace(".php", "",$_GET['ePagina']);
$eSeccion=substr($nombreSeccion,0,3);
$eUSeccion=substr($nombreSeccion,0,1); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="UTF-8">
	<title>Sindicato TAP V3.0</title>
	<link rel="icon" href="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/favicon.ico" type="imagen/png"/>
	<link rel="shortcut icon" href="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/favicon.ico" type="imagen/png"/>
	<link rel="stylesheet" type="text/css" href="css/styles.css"/>
	<link rel="stylesheet" type="text/css" href="css/webticker.css"/>
	<link rel="stylesheet" type="text/css" href="js/dojo/dijit/themes/tundra/tundra.css"/>
	<link type="text/css" href="css/css3.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="css/aplicacion.css"/>
	<script type="text/javascript" src="js/dojo/dojo/dojo.js" djConfig="parseOnLoad:true, isDebug: false"></script>
	<script type="text/javascript" src="js/jquery-1.6.js"></script>
	<script type="text/javascript" src="js/jquery.pikachoose.js"></script>
	<script type="text/javascript" src="js/dojo/dojo/dojo.js"></script>
	<script type="text/javascript" src="js/funciones.js"></script>
	<script type="text/javascript" src="js/jquery.webticker.js"></script>
	<script type="text/javascript" src="js/jquery.webticker.min.js" language="javascript"></script>
	<script language="javascript">
		dojo.require("dijit.form.Button");
		dojo.require("dijit.form.TextBox");
        $(document).ready(function (){
			$("#pikame").PikaChoose();
			//$("#webticker").webTicker();
		});
    </script>
</head>
<body>
	<div id="containerhead">
		<header>
			<table width="980px" border="0">
				<tr>
					<td rowspan="2">
						<img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/logosistema.png" width="150" height="120" alt=""/>
					</td>
					<td align="right" valign="top">
						<?=$pagina->cargarFecha();?>
					</td>
				</tr>
				<tr>
					<td nowrap="nowrap">
						<nav>
						  <div id='cssmenu' style="position:absolute; z-index:5; left: 0px; top: -5px;">
							<?php $tModulo=explode(".", $eSeccion);
							if($tModulo[0]==1 || (int)$_SESSION['sesionUsuario']['eCodUsuario']==0){ ?>
								<ul>
							<?php }else{ 
								$eAdminM=($_SESSION['sesionUsuario']['bSindicato']==1 ? 1 : 0); ?>
								<ul>
								  <li><a href="?ePagina=2.1.php" <?=($eSeccion=='2.1' ? "class='current'" : "");?>>Inicio</a></li>
								  <?php if((int)$_SESSION['sesionUsuario']['bSindicato']==1){
								  		if($_SESSION['sesionUsuario']['eCodUsuario']==1 || $_SESSION['sesionUsuario']['bAdministrador']==1){ ?>
								  				<li><a href="?ePagina=2.2.php" <?=($eSeccion=='2.2' ? "class='current'" : "");?>>Opciones</a></li>
								  		<?php } 
								  		if($eAdminM==1){ ?>
								  			<li ><a href="?ePagina=2.3.php" <?=($eSeccion=='2.3' ? "class='current'" : "");?>>Cat&aacute;logos</a></li>
								  		<?php }
								  	}
								  	if($eAdminM==1){ ?>
										  <li ><a href="?ePagina=2.4.php" <?=($eSeccion=='2.4' ? "class='current'" : "");?>>Operaci&oacute;n</a></li>
										<?php } ?>
								  <li ><a href="?ePagina=2.5.php" <?=($eSeccion=='2.5' ? "class='current'" : "");?>>Consultas</a></li>
								  <li ><a href="JavaScript:cerrarSesion();">Salir</a></li>
								</ul>
							<?php } ?>
						  </div>
						</nav>
					</td>
				</tr>
				<tr>
					<td colspan="2" nowrap="nowrap" align="right"><b><?=((int)$_SESSION['sesionUsuario']['eCodUsuario']>0 ? "<img width=\"16\" height=\"16\" src=\"https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-usuario.png\" alt=\"Usuario\"> ".$_SESSION['sesionUsuario']['Usuario']." / ".$_SESSION['sesionUsuario']['Entidad'] : "");?></b></td>
				</tr>
			</table>
	  </header>
	</div>
<!--end container-->
<div id="container">
  <table border="0">
    <input name="ePagina" id="ePagina" value="<?=$nombreSeccion;?>" type="hidden"/>
    <tr><td class="sanLR04 sanTIT"><?php $pagina->tituloSeccion(); ?></td></tr>
    <tr><td><?php if(array_key_exists('sesionUsuario', $_SESSION) && (int)$_SESSION['sesionUsuario']['eCodUsuario']>0){$pagina->crearBotones();} ?></td></tr>
    <tr><td><?php $pagina->mostrarContenido($eUSeccion); ?></td></tr>
  </table>
</div>
<!--start footer-->
<footer>
  <div class="container" align="center">
	<table border="0">
		<tr><td class="sanLR04" align="center"><label style="color: #FFFFFF;">&copy; <?=date('Y');?>, Todos los Derechos Reservados</label></td></tr>

		<tr></tr>
		<tr><td class="sanLR04" align="center">
			<span class="carga" style="display: none;"><img src="gif/loading10.gif" height="15px"></span>
			<label style="color: #FFFFFF; display: none;"><strong>Su licencia esta por vencer, restan solo </strong></label>
			<label style="color: #FE2E2E; display: none;"><strong><?php  $fecha1 = strtotime('2018-09-10'); $fecha2 = strtotime(date('Y-m-d')); $diff = $fecha1 - $fecha2; $dias = $diff/(60*60*24); echo floor($dias);   ;?> dias</strong></label>
			<label style="color: #FFFFFF; display: none;"><strong> contacte a su proveedor</strong></label>
			<span class="carga" style="display: none;"><img src="gif/loading10.gif" height="15px"></span>
		</td></tr>
	</table>
  </div>
</footer>
</body>
</html>
