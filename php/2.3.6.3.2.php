<?php require_once("conexion/soluciones-mysql.php");  
$link = getLink();
	
$select=" SELECT cc.*, ce.tNombre AS Estatus, cb.tNombre AS Banco ".
		" FROM catcuentasbancarias cc ".
		" INNER JOIN catestatus ce ON ce.tCodEstatus=cc.tCodEstatus ".
		" LEFT JOIN catbancos cb ON cb.eCodBanco=cc.eCodBanco ".
		" WHERE cc.eCodCuentaBancaria=".$_GET['eCodCuentaBancaria'];
$rComprobante = mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC); ?>
<script type="text/javascript">
function consultar(){
	document.location = './?ePagina=2.3.6.3.php';
}
function nuevo(){
	document.location = './?ePagina=2.3.6.3.1.php';
}
</script>
<div id="dvCNS" style="display:none;"></div>
<form action="" method="post" name="Datos" id="Datos" onsubmit="return false;">
<table border="0" cellpadding="0" cellspacing="0" width="980px">
    <tr><td height="20"></td></tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"> C&oacute;digo</td>
	    <td width="50%" nowrap class="sanLR04"><?=sprintf("%07d",$rComprobante{'eCodCuentaBancaria'});?></td>
		<td nowrap class="sanLR04"> Estatus </td>
	    <td width="50%" nowrap class="sanLR04"><?=utf8_encode($rComprobante{'Estatus'});?></td>
    </tr>
    <tr>
		<td height="23" nowrap class="sanLR04"> Nombre </td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rComprobante{'tNombre'});?></td>
        <td height="23" nowrap class="sanLR04"> Banco</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rComprobante{'Banco'});?></td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"> N&oacute;. Cuenta</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rComprobante{'tNumeroCuenta'});?></td>
		<td height="23" nowrap class="sanLR04"> CLABE </td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rComprobante{'tCLABE'});?></td>
    </tr>
</table>
</form>