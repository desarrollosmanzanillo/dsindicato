<style>
#divgral {
     overflow:scroll;
     height:450px;
     width:980px;
}
</style>
<?php require_once("conexion/soluciones-mysql.php"); 
$link = getLink();
if($_POST){
	if($_POST['eAccion']==1){
		foreach($_POST as $k => $valor){
			$nombre = strval($k);
			$campo = "eCodDia";
			if(strstr($nombre,$campo) && (int)$valor>0){
				$update=" UPDATE catdiasfestivos SET tCodEstatus='CA' WHERE eCodDia=".$valor;
				mysqli_query($link,$update);
			}
		}
	}

	$select=" SELECT * ".
			" FROM catdiasfestivos ".
			" WHERE 1=1".
			($_POST['eCodDia']       ? " AND eCodDia=".$_POST['eCodDia'] : "").
			($_POST['fhFecha']       ? " AND fhFecha='".$_POST['fhFecha']."'"  : "").
			($_POST['tObservacion']  ? " AND tObservacion LIKE '%".utf8_decode($_POST['tObservacion'])."%'" : "");
	$rsTiposEntidades=mysqli_query($link,$select);
	$registros=(int)mysqli_num_rows($rsTiposEntidades); ?>
<table cellspacing="0" border="0" width="980px">
  <tr>
    <td width="50%"><hr color="#666666" /></td>
    <td class="sanLR04" nowrap="nowrap"><b>Registros Encontrados: ( <?=$registros;?> )</b></td>
    <td width="50%"><hr color="#666666" /></td>
  </tr>
</table>
<div style="display:block; top:0; left:0; width:980px; z-index=1; overflow: auto;">
  <table cellspacing="0" border="0" width="980px">
    <thead>
      <tr class="thEncabezado">
        <td nowrap="nowrap" class="sanLR04" height="20" align="center">C</td>
        <td nowrap="nowrap" class="sanLR04" align="center">E</td>
        <td nowrap="nowrap" class="sanLR04">C&oacute;digo</td>
        <td nowrap="nowrap" class="sanLR04">Fecha</td>
        <td nowrap="nowrap" class="sanLR04" width="100%">Observaciones</td>        
      </tr>
    </thead>
    <tbody>
      <?php $i=1; while($rTipoEntidad=mysqli_fetch_array($rsTiposEntidades,MYSQLI_ASSOC)){ ?>
      <tr>
        <td nowrap="nowrap" class="sanLR04" height="20" align="center"><input type="checkbox" name="eCodDia<?=$i?>" id="eCodDia<?=$i?>" value="<?=$rTipoEntidad{'eCodDia'};?>" ></td>
        <td nowrap="nowrap" class="sanLR04" height="20" align="center"><img width="16" height="16" alt="Usuario" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-<?=$rTipoEntidad{'ecodEstatus'};?>.png"></td>
        <td nowrap="nowrap" class="sanLR04 colmenu"><a href="?ePagina=2.3.2.4.1.php&eCodDia=<?=$rTipoEntidad{'eCodDia'};?>"><b><?=sprintf("%07d",$rTipoEntidad{'eCodDia'});?></b></a></td>
        <td nowrap="nowrap" class="sanLR04"><a class="txtCO12" href="?ePagina=2.3.2.4.2.php&eCodDia=<?=$rTipoEntidad{'eCodDia'};?>"><?=date("d/m/Y", strtotime($rTipoEntidad{'fhFecha'}));?></a></td>        
        <td nowrap="nowrap" class="sanLR04 columnB" width="100%"><?=utf8_encode($rTipoEntidad{'tObservacion'});?></td>
      </tr>
      <?php $i++; } ?>
    </tbody>
  </table>
</div>
<?php }else{ ?>
<script type="text/javascript">
	function nuevo(){
		document.location = "?ePagina=2.3.2.4.1.php";
	}

	function eliminar(){
		var eChk = 0;
		dojo.byId('eAccion').value = 1;	
		dojo.query("[id*=\"eCodDia\"]:checked").forEach(function(nodo, index, array){eChk++;});
	
		if(eChk!=0){
			if(confirm("¿Desea eliminar los dias festivos?")){
				dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+'.php', handleAs: "text", load: function(tRespuesta, ioArgs){
					dojo.byId('dvCNS').innerHTML = tRespuesta; 
					alert("¡Los registros se han eliminado exitosamente!");	
				}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});
			}
		}else{
			alert("No ha seleccionado ningun dia festivo");
		}
		dojo.byId('eAccion').value = "";	
	}

	dojo.addOnLoad(function(){filtrarConsulta();});
</script>
<form name="Datos" id="Datos" method="post" action="" onsubmit="return false;">
  <input type="hidden" value="0" name="ePagina" id="ePagina" />
  <input type="hidden" value="" name="eAccion" id="eAccion" />
  <table width="980px" border="0">
    <tr>
      <td colpan="3" width="100%"></td>
      <td align="right" nowrap="nowrap"><a class="fntBR11" href="javaScript:mostrarFiltros('Busqueda');">Busqueda de Registros</a></td>
    </tr>
    <tr id="trBusqueda" style="display:none">
      <td colspan="4"><table width="100%" bgcolor="#f9f9f9">
          <tr>
            <td class="sanLR04" height="5"></td>
          </tr>
          <tr>
            <td class="sanLR04" height="20">C&oacute;digo</td>
            <td class="sanLR04" width="50%">
              <input type="text" name="eCodDia" dojoType="dijit.form.TextBox" id="eCodDia" value="" style="width:80px">
            </td>
            <td class="sanLR04">Fecha</td>
            <td class="sanLR04" width="50%">
              <input name="fhFecha" type="date" dojoType="dijit.form.TextBox" id="fhFecha" value="" style="width:130px" >
            </td>
          </tr>
          <tr>
            <td class="sanLR04" height="20">Observaciones</td>
            <td class="sanLR04" width="50%"><input type="text" name="tObservacion" dojoType="dijit.form.TextBox" id="tObservacion" value="" style="width:175px"></td>
            <td class="sanLR04"></td>
            <td class="sanLR04" width="50%"></td>
          </tr>
          <tr>
            <td class="sanLR04" height="5"></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td colspan="4"><div id="dvCNS"></div></td>
    </tr>
  </table>
</form>
<?php } ?>