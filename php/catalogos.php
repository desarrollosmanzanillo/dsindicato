<!doctype html>  
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="icon" href="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/favicon.ico" type="image/x-icon"/> 
    <link rel="stylesheet" type="text/css" href="css/styles.css"/>
    <link type="text/css" href="css/css3.css" rel="stylesheet" />
    <script type="text/javascript" src="js/jquery-1.6.js"></script>
    <script type="text/javascript" src="js/jquery.pikachoose.js"></script>
    <script language="javascript">
        $(document).ready(
            function (){
                $("#pikame").PikaChoose();
            });
    </script>
</head>
<body>
    <div id="container">
        <header>
            <a href="#" id="logo"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/LogoEmpresa.png" width="221" height="100" alt=""/></a>    
            <nav>
            	<div id='cssmenu' style="position:absolute; z-index:10;">
                <ul>
                    <li><a href="#" class="current">[Bienvenido]</a></li>
                    <li><a href="#">Historia</a>
                    	<ul>
                            <li><a href='#'><span>¿Quienes somos?</span></a></li>
                            <li><a href='#' class="middle"><span>Misi&oacute;n/Visi&oacute;n</span></a></li>
                            <li class="caption"><a href='#' class='last'><span>Pol&iacute;tica de Calidad</span></a></li>
						</ul>
                    </li>
                    <li class='has-sub'><a href="#">Servicios</a>
                    	<ul>
                            <li><a href='#'><span>Deposito</span></a></li>
                            <li><a href='#' class='last'><span>Transporte</span></a></li>
						</ul>
                    </li>
                    <li><a href="#">Infraestructura</a>
                    	<ul>
                        	<li><a href="#"><span>Ubicaci&oacute;n</span></a></li>
                            <li><a href="#" class="middle"><span>Equipo</span></a></li>
                            <li><a href="#" class='last'><span>Instalaciones</span></a></li>
                        </ul>
                    </li>
                    
                    <li><a href="#">Servicios en L&iacute;nea</a></li>
                </ul>
                </div>
            </nav>
        </header>
        <div id="intro">
        	<div class="pikachoose">
                <ul id="pikame" >
                    <li><a href="#"><img src="images/C-01.jpg" width="900" height="280" alt="picture"/></a><span>“Almacenaje de contenedores o carga general.”</span></li>
                    <li><a href="#"><img src="images/C-02.jpg" width="900" height="280" alt="picture"/></a><span>“Buscamos la satisfacci&oacute;n del cliente“</span></li>
                    <li><a href="#"><img src="images/C-03.jpg" width="900" height="280" alt="picture"/></a><span>“Fletes a toda la republica.”</span></li>
                    <li><a href="#"><img src="images/C-04.jpg" width="900" height="280" alt="picture"/></a><span>“Carga y descarga de mercanc&iacute;a.”</span></li>
                    <li><a href="#"><img src="images/C-05.jpg" width="900" height="280" alt="picture"/></a><span>“Simplicity is the essence of happiness.”</span></li>
                    <li><a href="#"><img src="images/C-06.jpg" width="900" height="280" alt="picture"/></a><span>“Simplicity is the essence of happiness.”</span></li>
                    <li><a href="#"><img src="images/C-07.jpg" width="900" height="280" alt="picture"/></a><span>“Simplicity is the essence of happiness.”</span></li>
                    <li><a href="#"><img src="images/C-08.jpg" width="900" height="280" alt="picture"/></a><span>“Simplicity is the essence of happiness.”</span></li>
                    <li><a href="#"><img src="images/C-09.jpg" width="900" height="280" alt="picture"/></a><span>“Simplicity is the essence of happiness.”</span></li>
                    <li><a href="#"><img src="images/C-10.jpg" width="900" height="280" alt="picture"/></a><span>“Simplicity is the essence of happiness.”</span></li>
                </ul>
            </div>
        </div>
        <div class="holder_content">    
            <section class="group1">
                <h3>Historia</h3>
				<p>Somos una empresa Mexicana especializada en el manejo de todo tipo de contenedores DRY CARGO, REEFERS, ISO TANQUES, 
				OPEN-TOP, FLAT RACK; además manejamos carga general, contamos con modernos equipos...</p>	
                <a href=""><span class="read_more">Leer más...</span></a>
            </section>    
            <section class="group2">
                <h3>Servicios</h3>
                <p>Servicios de maniobras de carga y descarga de carga general, contenedores llenos o vacios. Servicio de almacenaje.
                Desconsolidacion o consolidacion de contenedores. Reparaci&oacute;n, lavado de contendores. Manejo de carga peligrosa... </p>
                <a href=""><span class="read_more">Leer más...</span></a>        
            </section>
            <section class="group3">
                <h3>Infraestructura</h3>
				<p>Contamos con una superficie de tres hectáreas para el almacenajes de contenedores en el puerto de Manzanillo
				y otras tres hectareas en la Cd. de Monterrey, Nuevo Leon...</p>
                <a href=""><span class="read_more">Leer más...</span></a>        
            </section>
        </div>
    </div>
   <!--end container-->
   
   <!--start footer-->
   <footer>
   <div class="container">  
   <div id="FooterTwo"> Think simple </div>
   <div id="FooterTree"> </div> 
   </div>
   </footer>
</body>
</html>
