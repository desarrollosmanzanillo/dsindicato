<?php require_once("conexion/soluciones-mysql.php");  
$link = getLink();
if($_POST){
	if($_POST['eProceso']==1){
		$exito= 0;
		$tKey= ($_POST['tKey']				? "'".trim(utf8_decode($_POST['tKey']))."'"			: "NULL");
		$tCer= ($_POST['tCer']				? "'".trim(utf8_decode($_POST['tCer']))."'"			: "NULL");
		$eCodCertificado= ($_POST['eCodCertificado']			? (int)$_POST['eCodCertificado']								: "NULL");
		$eCodEntidad= ($_POST['eCodEntidad']			? (int)$_POST['eCodEntidad']								: "NULL");
		$tNumeroCSD= ($_POST['tNumeroCSD']		? "'".trim(utf8_decode($_POST['tNumeroCSD']))."'"							: "NULL");
		$tContrasenia= ($_POST['tContrasenia']		? "'".trim(utf8_decode($_POST['tContrasenia']))."'"	: "NULL");

		if((int)$eCodCertificado>0){
			$update=" UPDATE catcertificadosdigitales ".
					" SET eCodEntidad=".$eCodEntidad.",".
					" tCodCertificado=".$tNumeroCSD.",".
					" tContraseniaSAT=".$tContrasenia.",".
					" tCer=".$tCer.",".
					" tKey=".$tKey.
					" WHERE eCodCertificado=".$eCodCertificado;
			if($res=mysqli_query($link,$update)){
				$exito=1;
			}else{
				$exito=0;
			}
		}else{
			$insert=" INSERT INTO catcertificadosdigitales(eCodEntidad, tCodCertificado, tCodEstatus, tContraseniaSAT, tCer, tKey) ".
					" VALUES (".$eCodEntidad.", ".$tNumeroCSD.", 'AC', ".$tContrasenia.", ".$tCer.", ".$tKey.")";
			if($res=mysqli_query($link,$insert)){
				$exito=1;
				$select=" select last_insert_id() AS Llave ";
				$rCodigo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
				$eCodCertificado=(int)$rCodigo['Llave'];
			}else{
				$exito=0;
			}
		}
		print "<input type=\"text\" value=\"".($exito==1 ? $eCodCertificado : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}
	
	if($_POST['eProceso']==2){
		header('Content-Type: text/html; charset=windows-1250');
		$tRespuesta = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">";
		$i=0;

		$indice = $_POST['eArchivoIndice'];
		$tTMP = $_FILES['tCertificado'.$indice]['tmp_name'];

		if (is_uploaded_file($tTMP)){
			$tArchivo = "../archivos/2.3.6.2.1/".md5(uniqid(date('YmdHis'),true));
			$tExt = pathinfo($_FILES['tCertificado'.$indice]['name'], PATHINFO_EXTENSION);
			$tMIME = $_FILES['tCertificado'.$indice]['type'];

			move_uploaded_file($tTMP, $tArchivo);
			$data=base64_encode(addslashes(fread(fopen($tArchivo, "r"), filesize($tArchivo))));
			//unlink($tArchivo);

			if(trim($data)!=""){
				$tRespuesta.="<tr>".
							 "<td class=\"sanLR04\" width=\"100%\">".
							 "<input type=\"hidden\" name=\"t".$indice."\" id=\"t".$indice."\" value=\"".$data."\" />Ok".
							 "</td></tr>";
			}else{
				$tRespuesta ="<tr>".
							 "<td class=\"sanLR04\" width=\"100%\">".
							 "<span class=\"fntG10K\">&iexcl;Ha ocurrido un error a la subida del archivo, intente de nuevo!</span>".
							 "</td></tr>";
			}
		}
		$tRespuesta.="</table>";	
		print "<textarea>".$tRespuesta."</textarea>";
	}
}
if(!$_POST){
$select=" SELECT * ".
		" FROM catcertificadosdigitales ".
		" WHERE eCodCertificado=".(int)$_GET['eCodCertificado'];
$rCertificado=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
$select=" SELECT * ".
		" FROM catentidades ".
		" WHERE tCodEstatus='AC' ".
		" ORDER BY tNombre ";
$rsEntidades=mysqli_query($link,$select); ?>
<script type="text/javascript">
dojo.require("dojo.number");
dojo.require("dijit.form.NumberTextBox");
dojo.require("dijit.form.DateTextBox");
dojo.require("dojo.io.iframe");

function guardar(){
	dojo.byId('eProceso').value=1;
	bandera = false;
	mensaje = "¡Verifique lo siguiente!\n";
	if (!dojo.byId("eCodEntidad").value){
		mensaje+="* Entidad\n";
		bandera = true;
	}
	if (!dojo.byId("tContrasenia").value){
		mensaje+="* Contraseña\n";
		bandera = true;
	}
	if (!dojo.byId("tNumeroCSD").value){
		mensaje+="* Número CSD\n";
		bandera = true;
	}
	if (!dojo.byId("tKey")){
		mensaje+="* Certificado (.key)\n";
		bandera = true;
	}
	if (!dojo.byId("tCer")){
		mensaje+="* Certificado de Seguridad (.cer)\n";
		bandera = true;
	}

	if (bandera==true){
		alert(mensaje);		
	}else{
		if (confirm("¿Desea Guardar la Información?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				if(dojo.byId("eCodigo").value>0){
					alert("¡La información se guardó correctamente!");
					tURL = './?ePagina=2.3.6.2.2.php&eCodCertificado='+dojo.byId("eCodigo").value;
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
		}
	}
}	

function subirArchivo(indice){
	dojo.byId('eArchivoIndice').value = indice;
	if(dojo.byId("tCertificado"+indice).value){
		dojo.byId('eProceso').value=2;
		dojo.io.iframe.send({url: "php/"+dojo.byId('ePagina').value+".php", method: "post", handleAs: "text", form: 'Datos', handle: function(tRespuesta, ioArgs){
			dojo.byId("divArchivo"+indice).innerHTML = tRespuesta;
		}});
	}
}

function consultar(){
	document.location = './?ePagina=2.3.6.1.php';
}
</script>
<div id="dvCNS" style="display:none;"></div>
<form id="Datos" name="Datos" method="post" onSubmit="return false;" enctype="multipart/form-data">
<input type="hidden" name="eProceso" id="eProceso" value="" />
<input type="hidden" name="eArchivoIndice" id="eArchivoIndice" value="" />
<input type="hidden" name="eCodCertificado" id="eCodCertificado" value="<?=$_GET['eCodCertificado'];?>" />
<table border="0" cellpadding="0" cellspacing="0" width="980px">
    <tr><td height="20"></td></tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Entidad</td>
	    <td width="50%" nowrap class="sanLR04">
        	<select name="eCodEntidad" id="eCodEntidad" style="width:175px" >
                <option value="">Seleccione...</option>
                <?php while($rEntidad=mysqli_fetch_array($rsEntidades,MYSQLI_ASSOC)){ ?>
                    <option value="<?=$rEntidad{'eCodEntidad'};?>" <?=($rCertificado{'eCodEntidad'}==$rEntidad{'eCodEntidad'} ? "selected" : "");?>><?=utf8_encode($rEntidad{'tNombre'})?></option>
                <?php } ?>
            </select>
        </td>
        <td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Contrase&ntilde;a</td>
	    <td width="50%" nowrap class="sanLR04">
        	<input name="tContrasenia" type="text" dojoType="dijit.form.TextBox" id="tContrasenia" value="<?=$rCertificado{'tContraseniaSAT'};?>" style="width:175px">
        </td>
    </tr>
    <tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> N&uacute;mero CSD</td>
	    <td width="50%" nowrap class="sanLR04">
        	<input name="tNumeroCSD" type="text" dojoType="dijit.form.TextBox" id="tNumeroCSD" value="<?=$rCertificado{'tCodCertificado'};?>" style="width:175px">
        </td>
        <td nowrap class="sanLR04"></td>
	    <td width="50%" nowrap class="sanLR04"></td>
    </tr>
    <tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Certificado (.key)</td>
	    <td nowrap class="sanLR04" align="left">
           	<input type="file" name="tCertificadoKey" id="tCertificadoKey" onChange="subirArchivo('Key');" onkeyup="this.value=''" style="width:325px">
        </td>
		<td nowrap class="sanLR04" colspan="2"><div id="divArchivoKey"><?=(trim($rCertificado{'tKey'})!="" ? "<input type=\"hidden\" name=\"tKey\" id=\"tKey\" value=\"".$rCertificado{'tKey'}."\" />Ok" : "");?></div>
        </td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Certificado de Seguridad (.cer) </td>
	    <td nowrap class="sanLR04" align="left">
           	<input type="file" name="tCertificadoCer" id="tCertificadoCer" onChange="subirArchivo('Cer');" onkeyup="this.value=''" style="width:325px">
        </td>
		<td nowrap class="sanLR04" colspan="2"><div id="divArchivoCer"><?=(trim($rCertificado{'tCer'})!="" ? "<input type=\"hidden\" name=\"tCer\" id=\"tCer\" value=\"".$rCertificado{'tCer'}."\" />Ok" : "");?></div>
        </td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle" /> <font class="fntG10K">Indica que es obligatorio llenar el campo.
    </font></td>
    </tr>
</table>
</form>
<?php } ?>