﻿<? require_once("conexion/soluciones-mysql.php");
$link = getLink();

if($_POST){
	date_default_timezone_set('America/Mexico_City');
	$varfecha=isset($_POST["fecha"]) ? $_POST["fecha"] : date("Y-m-d");
	$fechaayer = date('Y-m-d',strtotime('-1 days', strtotime($varfecha)));   

	$registrado=" SELECT asi.idEmpleado ".
				" FROM asistencias AS asi ".
				" LEFT JOIN relasistenciamovimientos AS rel ON rel.eCodAsistencia=asi.id ".
				" LEFT JOIN movimientosafiliatorios AS mov ON mov.eCodMovimiento=rel.eCodMovimiento ".
				" WHERE asi.Estado='AC' AND asi.fecha='".$varfecha."' AND mov.tFolio>'0' ";

    $select=" SELECT EMP.id, chek.id AS numeral, chek.idEmpresa, chek.Turno, EMP.Empleado, chek.folioIMSS, chek.Estado as tCodEstatus, chek.dBono, ".
			" chek.Fecha, INC.eCodEntidad AS iden, INC.tRegistroPatronal AS rp, chek.idEmpleado, EMP.IMSS AS imss, EMP.Paterno AS pat, EMP.Materno AS mat, ".
			" EMP.Nombre AS nomb, tem.eCodTipoSalario as TipoSalario, tem.dFactorIntegracion as FactorIntegracion, tem.eCodTipoJornada, tem.eCodTipoTrabajador, ".
			" EMP.Clinica AS clin, EMP.CURP AS curp, CAT.Turno1, CAT.Turno2, CAT.Turno3 ".
			" FROM asistencias AS chek ".
			" LEFT JOIN empleados AS EMP ON chek.idEmpleado = EMP.id ".
			" INNER JOIN categorias AS CAT ON chek.idCategorias = CAT.indice ".
			" INNER JOIN cattipoempleado AS tem ON tem.eCodTipoEntidad=EMP.eCodTipo ".
			" LEFT JOIN catentidades AS INC ON chek.idEmpresa = INC.eCodEntidad ".
			" LEFT JOIN relasistenciamovimientos AS rel ON rel.eCodAsistencia=chek.id ".
			" LEFT JOIN asistencias AS asi ON asi.idEmpleado=chek.idEmpleado AND asi.Estado='AC' AND asi.Fecha='".$fechaayer."' ".
			" WHERE chek.Estado='AC' AND rel.eCodAsistencia is NULL AND chek.Fecha ='".$varfecha."' AND asi.idEmpleado IS NULL ".
			" AND EMP.eCodTipo=1 AND chek.idEmpleado NOT IN (".$registrado.") ".
			" GROUP BY chek.idEmpleado ".
			" ORDER BY chek.Turno, EMP.Empleado ";
	$rsServicios=mysqli_query($link,$select); 
	$registros=(int)mysqli_num_rows($rServicios);                     
	$cont=0;	 ?>
    <table cellspacing="0" border="0" width="980px">
      <tr>
        <td width="50%"><hr color="#666666" /></td>
        <td class="sanLR04" nowrap="nowrap"><b>Registros Encontrados: ( <?=$registros;?> )</b></td>
        <td width="50%"><hr color="#666666" /></td>
      </tr>
    </table>
    <div style="display:block; top:0; left:0; width:965px; z-index=1; overflow: auto; height:400px;">
        <table cellspacing="0" border="0" width="980px">
            <thead>
                <tr class="thEncabezado">	      	
                    <td nowrap="nowrap" class="sanLR04" height="20">R. Patronal</td>
                    <td nowrap="nowrap" class="sanLR04">NSS</th>
                    <td nowrap="nowrap" class="sanLR04">Paterno</td> 
                    <td nowrap="nowrap" class="sanLR04">Materno</td>
                    <td nowrap="nowrap" class="sanLR04">Nombre(s)</td>
                    <td nowrap="nowrap" class="sanLR04">S.D.I.</td>
                    <td nowrap="nowrap" class="sanLR04">S. Infornavit</td>
                    <td nowrap="nowrap" class="sanLR04">T. Trabajador</td>
                    <td nowrap="nowrap" class="sanLR04">T. Salario</td>
                    <td nowrap="nowrap" class="sanLR04">Jornada</td>
                    <td nowrap="nowrap" class="sanLR04">F. Movimiento</td>
                    <td nowrap="nowrap" class="sanLR04">U.M.F</td>
                    <td nowrap="nowrap" class="sanLR04">Movimiento</td>
                    <td nowrap="nowrap" class="sanLR04">Guia</td>
                    <td nowrap="nowrap" class="sanLR04">C. Trabajador</td>
                    <td nowrap="nowrap" class="sanLR04">Filler</td>
                    <td nowrap="nowrap" class="sanLR04">CURP</td>      
                    <td nowrap="nowrap" class="sanLR04" width="100%">Identificador</td>
                </tr>   
            </thead>
            <tbody>
				<? $i=1; 
                $select=" SELECT * FROM sisconfiguracion ";
                $configuracion=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
				while($rServicio=mysqli_fetch_array($rsServicios,MYSQLI_ASSOC)){
					$salario=0;
					if($rServicio["Turno"]==1)
					$salario=$rServicio["Turno1"];
					if($rServicio["Turno"]==2)
					$salario=$rServicio["Turno2"];
					if($rServicio["Turno"]==3)
					$salario=$rServicio["Turno3"];
					$salario=($salario*$rServicio["FactorIntegracion"])+$rServicio["dBono"];
					$salario=number_format($salario,2);
					$salario=explode(".",$salario);

					if(strlen($salario[0])<=3)
					$cadena="0".$salario[0].$salario[1];
					else
					$cadena=$salario[0].$salario[1];
					$fech=$rServicio["Fecha"]; 
					$fecha=explode("-",$fech); 
					$abc=$fecha[2].$fecha[1].$fecha[0]; ?>
                    <tr>
                        <input type="hidden" id="id<?=$i?>" name="id<?=$i?>" value="<?=$rServicio["numeral"]?>">        
                        <td nowrap="nowrap" class="sanLR04 columnB"><b><?=$rServicio{'rp'};?></b></td>                              
                        <td nowrap="nowrap" class="sanLR04"><?= utf8_decode($rServicio["imss"]); ?></td>
                        <td nowrap="nowrap" class="sanLR04 columnB"><?= utf8_decode($rServicio["pat"]); ?></td>
                        <td nowrap="nowrap" class="sanLR04"><?= utf8_decode($rServicio["mat"]); ?></td>
                        <td nowrap="nowrap" class="sanLR04 columnB"><?= utf8_decode($rServicio["nomb"]); ?></td>
                        <td nowrap="nowrap" class="sanLR04"><?=((float)$configuracion{'dTopeSalario'}<(float)(str_replace(",", "", $salario[0])+($salario[1]/100)) ? (float)$configuracion{'dTopeSalario'} : (float)(str_replace(",", "", $salario[0])+($salario[1]/100)));?></td>
                        <td nowrap="nowrap" class="sanLR04 columnB">000000</td>
                        <td nowrap="nowrap" class="sanLR04"><?= utf8_decode($rServicio["eCodTipoTrabajador"]); ?></td>
                        <td nowrap="nowrap" class="sanLR04 columnB"><?= utf8_decode($rServicio["TipoSalario"]); ?></td>
                        <td nowrap="nowrap" class="sanLR04"><?= utf8_decode($rServicio["eCodTipoJornada"]); ?></td>
                        <td nowrap="nowrap" class="sanLR04 columnB"><?= utf8_decode($abc); ?></td>
                        <td nowrap="nowrap" class="sanLR04"><?= utf8_decode($rServicio["clin"]); ?></td>
                        <td nowrap="nowrap" class="sanLR04 columnB">08</td>
                        <td nowrap="nowrap" class="sanLR04">03400</td>
                        <td nowrap="nowrap" class="sanLR04 columnB">ESTIBADOR</td>
                        <td nowrap="nowrap" class="sanLR04"></td>
                        <td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_decode($rServicio["curp"]);?></td>
                        <td nowrap="nowrap" class="sanLR04" width="100%">9</td>
                    </tr>
					<? $i++;
				} ?>
            </tbody>
        </table>
    </div>
<? }else{ ?>
<script type="text/javascript">
function generarBatch(){
	var UrlBatch = "php/batch/2.5.4.1.php?"+
    (dojo.byId('fecha').value ? "&fecha="+dojo.byId('fecha').value : "");    
    var Datos = document.createElement("FORM");
    document.body.appendChild(Datos);
    Datos.name='Datos';
    Datos.method = "POST";
    Datos.action = UrlBatch;
    Datos.submit();
}

dojo.addOnLoad(function(){filtrarConsulta();});
</script>
<form name="Datos" id="Datos" method="post" action="" onsubmit="return false;">
  <input type="hidden" value="0" name="ePagina" id="ePagina" />
  <input type="hidden" value="" name="eAccion" id="eAccion" />
  <table width="900px" border="0">
    <tr>
      <td colpan="3" width="100%"></td>
      <td align="right" nowrap="nowrap"><a class="fntBR11" href="javaScript:mostrarFiltros('Busqueda');">Busqueda de Registros</a></td>
    </tr>
    <tr id="trBusqueda" style="display:none">
      <td colspan="4"><table width="900px" bgcolor="#f9f9f9">
          <tr>
            <td class="sanLR04" height="5"></td>
          </tr>
          <tr>
            <td class="sanLR04" height="20">Fecha</td>
            <td class="sanLR04" width="50%"><input type="date" name="fecha" dojoType="dijit.form.TextBox" id="fecha" value="<?=$_POST["fecha"] ? $_POST["fecha"] : date("Y-m-d")?>" style="width:140px"></td>
            <td class="sanLR04"></td>
            <td class="sanLR04" width="50%"></td>
          </tr>
          <tr>
            <td class="sanLR04" height="5"></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td colspan="4"><div id="dvCNS"></div></td>
    </tr>
  </table>
</form>
<? } ?>