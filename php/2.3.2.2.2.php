<?php require_once("conexion/soluciones-mysql.php");  
$link = getLink();
	
$select=" SELECT ce.*, cte.tNombre AS TipoEntidad, cp.tNombre AS Pais, ".
		" ces.tNombre AS Estado, cci.tNombre AS Ciudad ".
		" FROM catentidades ce ".
		" LEFT JOIN cattiposentidades cte ON cte.eCodTipoEntidad=ce.eCodTipoEntidad ".
		" LEFT JOIN catpaises cp ON cp.eCodPais=ce.eCodPais ".
		" LEFT JOIN catestados ces ON ces.eCodEstado=ce.eCodEstado ".
		" LEFT JOIN catciudades cci ON cci.eCodCiudad=ce.eCodCiudad ".
		" WHERE ce.eCodEntidad=".$_GET['eCodEntidad'];
$rEstado = mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC); ?>
<script type="text/javascript">
function consultar(){
	document.location = './?ePagina=2.3.2.2.php';
}
function nuevo(){
	document.location = './?ePagina=2.3.2.1.1.php';
}
</script>
<div id="dvCNS" style="display:none;"></div>
<form action="" method="post" name="Datos" id="Datos" onsubmit="return false;">
<table border="0" cellpadding="0" cellspacing="0">
    <tr><td height="20"></td></tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"> C&oacute;digo</td>
	    <td width="50%" nowrap class="sanLR04"><?=sprintf("%07d",$rEstado{'eCodEntidad'});?></td>
		<td nowrap class="sanLR04"> RFC </td>
	    <td width="50%" nowrap class="sanLR04"><?=utf8_encode($rEstado{'tRFC'});?></td>
    </tr>
    <tr>
		<td height="23" nowrap class="sanLR04"> Nombre</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rEstado{'tNombre'});?></td>
		<td height="23" nowrap class="sanLR04"> Siglas </td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rEstado{'tSiglas'});?></td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"> Tipo</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rEstado{'TipoEntidad'});?></td>
		<td height="23" nowrap class="sanLR04"> Pa&iacute;s </td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rEstado{'Pais'});?></td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"> Estado</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rEstado{'Estado'});?></td>
		<td height="23" nowrap class="sanLR04"> Ciudad</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rEstado{'Ciudad'});?></td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"> Colonia</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rEstado{'tColonia'});?></td>
		<td height="23" nowrap class="sanLR04"> Calle</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rEstado{'tDireccion'});?></td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"> C.P.</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rEstado{'tCodigoPostal'});?></td>
		<td height="23" nowrap class="sanLR04"> N&uacute;mero Exterior</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rEstado{'tNumeroExterior'});?></td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"> N&uacute;mero Interior</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rEstado{'tNumeroInterior'});?></td>
		<td height="23" nowrap class="sanLR04"> Telefono</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rEstado{'tTelefono'});?></td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"> Referencia</td>
	    <td nowrap class="sanLR04" ><?=utf8_encode($rEstado{'tReferencia'});?></td>
	    <td height="23" nowrap class="sanLR04"> Registro Patronal</td>
	    <td nowrap class="sanLR04" colspan="3"><?=utf8_encode($rEstado{'tRegistroPatronal'});?></td>
    </tr>
</table>
</form>