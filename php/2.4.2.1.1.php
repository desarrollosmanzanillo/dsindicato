<?php require_once("conexion/soluciones-mysql.php");  
$link = getLink();
if($_POST){
	if($_POST['eProceso']==1){
		$exito				= 0;
		$eCodUsuario		= ($_POST['eCodUsuario']		? (int)$_POST['eCodUsuario']											: "NULL");
		$eCodRegistro		= ($_POST['eCodRegistro']		? (int)$_POST['eCodRegistro']											: "NULL");
		$tCodEstatus		= ($_POST['tCodEstatus']		? "'".$_POST['tCodEstatus']."'"											: "NULL");
		$eCodTipoSolicitud	= ($_POST['eCodTipoSolicitud']	? (int)$_POST['eCodTipoSolicitud']										: "NULL");
		$tMotivoCancelacion	= ($_POST['tMotivoCancelacion']	? "'".trim(utf8_decode(addslashes($_POST['tMotivoCancelacion'])))."'"	: "NULL");
		
		mysqli_query($link,"BEGIN TRANSACTION");

		$update=" UPDATE opesolicitudesaltasclientes ".
				" SET	tCodEstatus=".$tCodEstatus.",";
				if($_POST['tCodEstatus']=="AU"){
					$update.=" eCodUsuarioAutorizacion=".$eCodUsuario.",".
							" fhFechaAutorizacion=current_timestamp ";
				}else{
					$update.=" eCodUsuarioCancelacion=".$eCodUsuario.",".
							" fhFechaCancelacion=current_timestamp, ".
							" tMotivoCancelacion=".tMotivoCancelacion;
				}
		$update.=" WHERE eCodRegistro=".$eCodRegistro;
		if($res=mysqli_query($link,$update)){
			$exito=1;
			$dTotal=0;
			if((int)$eCodTipoSolicitud==1){
				if($_POST['tCodEstatus']=="AU"){
					$insert=" INSERT INTO catentidades(tCodEstatus, tNombre, tSiglas, eCodTipoEntidad, eCodPais, eCodEstado, eCodCiudad, tColonia, ".
							" tDireccion, tCodigoPostal, tNumeroExterior, tNumeroInterior, tTelefono, tReferencia, tRFC) ".
							" SELECT 'AC', tNombre, tSiglas, 3, eCodPais, eCodEstado, eCodCiudad, tColonia, ".
							" tDireccion, tCodigoPostal, tNumeroExterior, tNumeroInterior, tTelefono, tReferencia, tRFC ".
							" FROM opesolicitudesaltasclientes ".
							" WHERE eCodRegistro=".$eCodRegistro;
					if($res=mysqli_query($link,$insert)){
						$select=" select last_insert_id() AS Llave ";
						$rCodigo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
						$eCodEntidad=(int)$rCodigo['Llave'];
					}else{
						$exito=0;
					}
					$select=" SELECT eCodEntidadSolicitante FROM opesolicitudesaltasclientes WHERE eCodRegistro=".$eCodRegistro;
					$rAgente=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
					$insert=" INSERT INTO relclientesagentes(eCodCliente, eCodAgente) ".
							" VALUES (".$eCodEntidad.", ".$rAgente{'eCodEntidadSolicitante'}.")";
					if($res=mysqli_query($link,$insert)){
						
					}else{
						$exito=0;
					}
				}
			}else{
				if($_POST['tCodEstatus']=="AU"){
					$insert=" INSERT INTO catsucursalesentidades(tCodEstatus, eCodEntidad, tNombre, eCodPais, eCodEstado, eCodCiudad, tColonia, ".
							" tDireccion, tCodigoPostal, tNumeroExterior, tNumeroInterior, tTelefono, tReferencia) ".
							" SELECT 'AC', eCodEntidadRelacion, tNombre, eCodPais, eCodEstado, eCodCiudad, tColonia, ".
							" tDireccion, tCodigoPostal, tNumeroExterior, tNumeroInterior, tTelefono, tReferencia ".
							" FROM opesolicitudesaltasclientes ".
							" WHERE eCodRegistro=".$eCodRegistro;
					if($res=mysqli_query($link,$insert)){
						$exito=1;
						$select=" select last_insert_id() AS Llave ";
						$rCodigo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
						$eCodSucursal=(int)$rCodigo['Llave'];
					}else{
						$exito=0;
					}
				}
			}
		}else{
			$exito=0;
		}

		if((int)$exito==1){
			mysqli_query($link,"COMMIT TRANSACTION");
		}else{
			mysqli_query($link,"ROLLBACK TRANSACTION");
		}
		print "<input type=\"text\" value=\"".($exito==1 ? $eCodRegistro : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}

//	
}
if(!$_POST){

$select=" SELECT oss.*, ces.tNombre AS Estatus, cu.tNombre AS nUsuario, cu.tApellidos AS aUsuario, ".
		" cte.tNombre AS TipoSolicitud, cp.tNombre AS Pais, cest.tNombre AS Estado, cci.tNombre AS Ciudad, ".
		" oss.tCodEstatus ".
		" FROM opesolicitudesaltasclientes oss ".
		" INNER JOIN catestatus ces ON ces.tCodEstatus=oss.tCodEstatus ".
		" INNER JOIN cattipossolicitudesaltas cte ON cte.eCodTipoSolicitud=oss.eCodTipoSolicitud ".
		" LEFT JOIN catusuarios cu ON cu.eCodUsuario=oss.eCodUsuario ".
		" LEFT JOIN catpaises cp ON cp.eCodPais=oss.eCodPais ".
		" LEFT JOIN catestados cest ON cest.eCodEstado=oss.eCodEstado ".
		" LEFT JOIN catciudades cci ON cci.eCodCiudad=oss.eCodCiudad ".
		" WHERE oss.eCodRegistro=".$_GET['eCodRegistro'];
$rSolicitud=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
?>
<script type="text/javascript">
dojo.require("dojo.number");
dojo.require("dijit.form.NumberTextBox");
dojo.require("dijit.form.DateTextBox");
dojo.require("dojo.io.iframe");
function autorizar(){
	if(dojo.byId('tCodEstatus').value=="AU" || dojo.byId('tCodEstatus').value=="CA"){
		if(dojo.byId('tCodEstatus').value=="AU"){
			alert('Solicitud autorizada');
		}else{
			alert('No se puede autorizar una solicitud cancelada');
		}
	}else{
		dojo.byId('tCodEstatus').value="AU";
		guardar();
	}
}

function cancelar(){
	if(!dojo.byId('tMotivoCancelacion').value){
		alert("Motivo de Cancelación");
	}else{
		if(dojo.byId('tCodEstatus').value=="CA"){
			alert('No se puede cancelar una solicitud cancelada');
		}else{
			dojo.byId('tCodEstatus').value="CA";
			guardar();
		}
	}
}

function guardar(){
	dojo.byId('eProceso').value=1;
	var bandera = false;
	var bServicios = false;
	var bMercancias = false;
	mensaje = "¡Verifique lo siguiente!\n";
	if (!dojo.byId("eCodRegistro").value){
		mensaje+="* Registro\n";
		bandera = true;		
	}
	
	if (bandera==true){
		alert(mensaje);		
	}else{
		if (confirm("¿Desea Guardar la Información?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				if(dojo.byId("eCodigo").value>0){
					alert("¡La información se guardó correctamente!");
					tURL = './?ePagina=2.5.2.1.1.php&eCodRegistro='+dojo.byId("eCodigo").value;
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
		}
	}
}	

function consultar(){
	document.location = './?ePagina=2.5.2.1.php';
}
</script>
<div id="dvCNS" style="display:none;"></div>
<form action="" method="post" name="Datos" id="Datos" onsubmit="return false;">
<input type="hidden" name="eProceso" id="eProceso" value="" />
<input type="hidden" name="eCodUsuario" id="eCodUsuario" value="<?=$_SESSION['sesionUsuario']['eCodUsuario'];?>" />
<input type="hidden" name="tCodEstatus" id="tCodEstatus" value="<?=$rSolicitud{'tCodEstatus'};?>" />
<input type="hidden" name="eCodRegistro" id="eCodRegistro" value="<?=$rSolicitud{'eCodRegistro'};?>" />
<input type="hidden" name="eCodTipoSolicitud" id="eCodTipoSolicitud" value="<?=$rSolicitud{'eCodTipoSolicitud'};?>" />
<table border="0" cellpadding="0" cellspacing="0" width="980px">
    <tr><td height="20"></td></tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> C&oacute;digo</td>
	    <td width="50%" nowrap class="sanLR04"><?=sprintf("%07d",$rSolicitud{'eCodRegistro'});?></td>
		<td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Estatus</td>
	    <td width="50%" nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'Estatus'});?></td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Usuario</td>
	    <td width="50%" nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'nUsuario'}." ".$rSolicitud{'aUsuario'});?></td>
		<td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Fecha de Solicitud</td>
	    <td width="50%" nowrap class="sanLR04"><?=date("d/m/Y H:i", strtotime($rSolicitud{'fhFecha'}));?></td>
    </tr>
	<tr>
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Tipo de Solicitud </td>
	    <td width="50%" nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'TipoSolicitud'});?></td>
    </tr>
	<tr><td colspan="4" align="center"><hr width="95%" size="0" align="center" color="#CACACA"></td></tr>
	<tr>
		<td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> RFC </td>
	    <td width="50%" nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'tRFC'});?></td>
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Tipo</td>
	    <td nowrap class="sanLR04">Cliente</td>
    </tr>
    <tr>
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Nombre</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'tNombre'});?></td>
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Siglas </td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'tSiglas'});?></td>
    </tr>
	<tr>
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Pa&iacute;s </td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'Pais'});?></td>
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Estado</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'Estado'});?></td>
    </tr>
	<tr>
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Ciudad</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'Ciudad'});?></td>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Colonia</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'tColonia'});?></td>
	</tr>
	<tr>
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Calle</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'tDireccion'});?></td>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> C.P.</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'tCodigoPostal'});?></td>
	</tr>
	<tr>
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> N&uacute;mero Exterior</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'tNumeroExterior'});?></td>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> N&uacute;mero Interior</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'tNumeroInterior'});?></td>
	</tr>
	<tr>
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Telefono</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'tTelefono'});?></td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Referencia</td>
	    <td nowrap class="sanLR04" colspan="3"><?=utf8_encode($rSolicitud{'tReferencia'});?></td>
    </tr>
    <? if($rSolicitud{'tURLRFC'}){ ?>
			<tr>
				<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Archivo</td>
				<td nowrap class="sanLR04" colspan="3"><a href="Patio/<?=$rSolicitud{'tURLRFC'};?>" target="_blank" class="sanT12" >RFC</a></td>
			</tr>
		<?php } ?>
	<tr>
	    <td height="23" nowrap class="sanLR04" valign="top"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" width="16" align="absmiddle"> Motivo de Cancelaci&oacute;n</td>
	    <td nowrap class="sanLR04" colspan="3"><textarea name="tMotivoCancelacion" id="tMotivoCancelacion" rows="4" cols="96"></textarea></td>
    </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle" /> <font class="fntG10K">Indica que es obligatorio llenar el campo.
    </font></td>
    </tr>
</table>
</form>
<?php } ?>