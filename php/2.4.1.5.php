﻿<?php require_once("conexion/soluciones-mysql.php");
$link = getLink();

if($_POST){

	if($_POST['eProceso']==1){
		$exito 		= 1;
		$eCountVac 	= 0;
		$idUsuario	= ($_POST['eUsuario']	? (int)$_POST['eUsuario']	: "NULL");

		foreach($_POST as $k => $v){
			$nombre = strval($k);
			if(strstr($nombre,"eRegistro") && $v){
				$eFila=str_replace("eRegistro", "", $nombre);
				$idEmpleado 	= ($_POST['idEmpleado'.$eFila]		? (int)$_POST['idEmpleado'.$eFila]			: "NULL");
				$fhFechaInicio 	= ($_POST['fhFechaInicio'.$eFila]	? "'".$_POST['fhFechaInicio'.$eFila]."'"	: "NULL");
				$fhFechaTermino = ($_POST['fhFechaTermino'.$eFila]	? "'".$_POST['fhFechaTermino'.$eFila]."'"	: "NULL");
				$eCodDiaDescanso= ($_POST['eDiaDescanso'.$eFila]	? (int)$_POST['eDiaDescanso'.$eFila]		: "NULL");
				print $eFila."-//-".$idEmpleado."<br>";
				
				if((int)$idEmpleado>0){
					$select=" SELECT DATEDIFF('".$_POST['fhFechaTermino'.$eFila]."', '".$_POST['fhFechaInicio'.$eFila]."') AS Dias, descanso
							FROM empleados
							WHERE id=".$idEmpleado;
					$rDiferencia=mysqli_fetch_array(mysqli_query($link,$select), MYSQLI_ASSOC);
					$eFor = (int)$rDiferencia{'Dias'};
					$i = 0;
					while ($i <= $eFor){
						//print $i." < ".$eFor."<br>";
						$select=" SELECT DATE_ADD('".$_POST['fhFechaInicio'.$eFila]."', INTERVAL +".$i." DAY) AS dActual,
								WEEKDAY(DATE_ADD('".$_POST['fhFechaInicio'.$eFila]."', INTERVAL +".$i." DAY))+1 Descanso ";
						$dActual=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
						$fhFechaVaca = $dActual{'dActual'};

						$entreSemana=date("w",strtotime($fhFechaVaca))+1;
						$i++;
						//print $rDiferencia{'descanso'}."!=".$entreSemana."<br>";
						if($rDiferencia{'descanso'}!=$entreSemana){
							$insert=" INSERT INTO bitvacaciones (eCodEmpleado, fhFecha, tCodEstatus, eCodUsuario, fhFechaRegistro, eCodDiaDescanso) 
									VALUES (".(int)$idEmpleado.", '".$fhFechaVaca."', 'AC', ".$idUsuario.", CURRENT_TIMESTAMP, ".$eCodDiaDescanso.") ";
							if($res=mysqli_query($link,$insert)){
								$cadena.=$insert;
								$eCountVac++;
							}else{
								$cadena.=$insert;
								$exito=0;
							}
						}
					}
				}
			}
		}
		print "<input type=\"text\" value=\"".((int)$exito>0 && (int)$eCountVac>0 ? 1 : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}

	if($_POST['eProceso']==2){
		$select=" SELECT em.eCodCategoriaVacacion, ca.Categoria, em.descanso, cd.tNombre AS Descanso
				FROM empleados em
				LEFT JOIN categorias ca ON ca.indice=em.eCodCategoriaVacacion
				LEFT JOIN catdias cd ON cd.eCodDia=em.descanso
				WHERE em.id=".$_POST['idEmpleado'.(int)$_POST['eFilaEmp']];
		$rCategoria=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

		$select=" SELECT eCodVacacion
				FROM bitvacaciones 
				WHERE fhFecha BETWEEN '".$_POST['fhFechaInicio'.(int)$_POST['eFilaEmp']]."' AND '".$_POST['fhFechaTermino'.(int)$_POST['eFilaEmp']]."'
				AND tCodEstatus='AC' AND eCodEmpleado=".(int)$_POST['idEmpleado'.(int)$_POST['eFilaEmp']];
		$rVacacion=mysqli_fetch_array(mysqli_query($link,$select), MYSQLI_ASSOC);

		$select=" SELECT id 
				FROM asistencias
				WHERE Estado LIKE 'AC' 
				AND Fecha BETWEEN '".$_POST['fhFechaInicio'.(int)$_POST['eFilaEmp']]."' AND '".$_POST['fhFechaTermino'.(int)$_POST['eFilaEmp']]."'
				AND idEmpleado=".(int)$_POST['idEmpleado'.(int)$_POST['eFilaEmp']];
		$asistencia=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

		$select=" SELECT id
				FROM incapacidades
				WHERE Estado LIKE 'AC' AND idEmpleado=".(int)$_POST['idEmpleado'.(int)$_POST['eFilaEmp']].
				" AND (FechaInicial BETWEEN '".$_POST['fhFechaInicio'.(int)$_POST['eFilaEmp']]."' AND '".$_POST['fhFechaTermino'.(int)$_POST['eFilaEmp']]."' ".
				" OR FechaFinal BETWEEN '".$_POST['fhFechaInicio'.(int)$_POST['eFilaEmp']]."' AND '".$_POST['fhFechaTermino'.(int)$_POST['eFilaEmp']]."') ";
		$incapacidad=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

		$select=" SELECT eCodAusentismo ".
				" FROM ausentismo ".
				" WHERE eCodEmpleado=".(int)$_POST['idEmpleado'.(int)$_POST['eFilaEmp']].
				" AND fhFecha BETWEEN '".$_POST['fhFechaInicio'.(int)$_POST['eFilaEmp']]."' AND '".$_POST['fhFechaTermino'.(int)$_POST['eFilaEmp']]."' AND eCodEstatus='AC' ";
		$ausentis=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

		$select=" SELECT CASE WHEN '".$_POST['fhFechaTermino'.(int)$_POST['eFilaEmp']]."'<'".$_POST['fhFechaInicio'.(int)$_POST['eFilaEmp']]."' THEN 1 END AS fechFiMenor ";
		$fechaFiMenor=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
		print "<input type=\"hidden\" name=\"eCodCategoriaVacacion\" id=\"eCodCategoriaVacacion\" value=\"".(int)$rCategoria{'eCodCategoriaVacacion'}."\">";
		print "<input type=\"hidden\" name=\"Categoria\" id=\"Categoria\" value=\"".$rCategoria{'Categoria'}."\">";

		print "<input type=\"hidden\" name=\"eCodDiaDescanso\" id=\"eCodDiaDescanso\" value=\"".(int)$rCategoria{'descanso'}."\">";
		print "<input type=\"hidden\" name=\"tDescanso\" id=\"tDescanso\" value=\"".$rCategoria{'Descanso'}."\">";

		print "<input type=\"hidden\" name=\"eVacaciones\" id=\"eVacaciones\" value=\"".(int)$rVacacion{'eCodVacacion'}."\">";
		print "<input type=\"hidden\" name=\"eAusentismoBD\" id=\"eAusentismoBD\" value=\"".(int)$ausentis{'eCodAusentismo'}."\">";
		print "<input type=\"hidden\" name=\"eEmpleadoBD\" id=\"eEmpleadoBD\" value=\"".(int)$asistencia{'id'}."\">";
		print "<input type=\"hidden\" name=\"eIncapacidad\" id=\"eIncapacidad\" value=\"".(int)$incapacidad{'id'}."\">";
		print "<input type=\"hidden\" name=\"fFechaMayoMen\" id=\"fFechaMayoMen\" value=\"".(int)$fechaFiMenor{'fechFiMenor'}."\">";

		
		
	}
//	
}else{ ?>
<style>
.eNumero{
	text-align:right;
}
</style>
<script src="../js/dojo/dijit/form/FilteringSelect.js" type="text/javascript"></script> 
<script src="../js/dojo/dojo/data/ItemFileReadStore.js" type="text/javascript"></script> 
<script src="../js/dojo/dijit/form/FilteringSelect.js" type="text/javascript"></script> 
<script src="../js/dojo/dijit/TooltipDialog.js" type="text/javascript"></script> 
<script type="text/javascript">
dojo.require("dojo.number");
dojo.require("dijit.form.NumberTextBox");
dojo.require("dijit.form.DateTextBox");
dojo.require("dojo.io.iframe");

function guardar(){
	dojo.byId('eProceso').value=1;
	var bandera = false;
	var bServicios = false;
	var bMercancias = false;
	var eTipoSolicitud = 0;
	mensaje = "¡Verifique lo siguiente!\n";

	var ultimafila=0;

	dojo.query("[id^=\"eRegistro\"]").forEach(function(nodo, index, array){
		ultimafila++;
	});

	if(parseInt(ultimafila)<=1){
		mensaje+="* Agregar registro\n";
		bandera = true;		
	}else{
		var bIncompleto=false;
		var bEmpleado=false;
		var bCategoria=false;
		var bCosto=false;
		var bFechaI=false;
		var bFechaT=false;
		var bCategoriaT=false;
		var bFolio=true;
		var bDescansoT=true;
		dojo.query("[id^=\"eRegistro\"]").forEach(function(nodo, index, array){
			var eFilaValida=parseInt(nodo.value);
			if(ultimafila>eFilaValida){
				if(!dijit.byId('idEmpleado'+eFilaValida).value){
					bEmpleado=true;
					bIncompleto=true;
				}
				if(!dojo.byId("fhFechaInicio"+eFilaValida).value){
					bFechaI=true;
					bIncompleto=true;
				}
				if(!dojo.byId("fhFechaTermino"+eFilaValida).value){
					bFechaT=true;
					bIncompleto=true;
				}
				if(!dojo.byId("eCategoriaVacacion"+eFilaValida).value){
					bCategoriaT=true;
					bIncompleto=true;
				}
				if(!dojo.byId("eDiaDescanso"+eFilaValida).value){
					bDescansoT=true;
					bIncompleto=true;
				}
			}else{
				if(dijit.byId('idEmpleado'+eFilaValida).value){
					bEmpleado=true;
					bIncompleto=true;
				}
				if(dojo.byId("fhFechaInicio"+eFilaValida).value){
					bFechaI=true;
					bIncompleto=true;
				}
				if(dojo.byId("fhFechaTermino"+eFilaValida).value){
					bFechaT=true;
					bIncompleto=true;
				}
				if(dojo.byId("eCategoriaVacacion"+eFilaValida).value){
					bCategoriaT=true;
					bIncompleto=true;
				}
				if(dojo.byId("eDiaDescanso"+eFilaValida).value){
					bDescansoT=true;
					bIncompleto=true;
				}
			}
		});
		if(bIncompleto==true){
			mensaje+="* Conceptos incompletos\n";
			if(bEmpleado==true){
				mensaje+="   Empleado\n";
			}
			if(bFechaI==true){
				mensaje+="   Fecha Inicio\n";
			}
			if(bFechaT==true){
				mensaje+="   Fecha Término\n";
			}
			if(bCategoriaT==true){
				mensaje+="   Asigne Categoría Vacaciones\n";
			}
			if(bDescansoT==true){
				mensaje+="   Asigne Día Descanso\n";
			}
			bandera = true;
		}
	}

	if (bandera==true){
		alert(mensaje);
	}else{
		if (confirm("¿Desea Guardar la Información?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				if(dojo.byId("eCodigo").value>0){
					alert("¡La información se guardó correctamente!");
					tURL = './?ePagina=2.5.1.5.php';
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
		}
	}
}

function borrarAsistencia(eFilaAsistencia){
	var ultimafila=0;
	dojo.query("[id^=\"eRegistro\"]").forEach(function(nodo, index, array){
		ultimafila=parseInt(nodo.id.replace('eRegistro',''));
	});

	if(eFilaAsistencia!=ultimafila){
		if(confirm("¿Seguro de eliminar la incapacidad?")){
			dojo.destroy(dojo.byId('filaAsistencia'+eFilaAsistencia));
		}
	}
}

function verificarEmpleados(){
	dojo.query("[id^=\"idEmpleado\"]").forEach(function(nodo, index, array){
		var filaEmpleado=nodo.id.replace('idEmpleado','');
		if(parseInt(filaEmpleado)>0){
			validarEmpleado(filaEmpleado);
		}
	});
}

function limpiarUsr(fila){
	dojo.byId('idEmpleado'+fila).value			= "";
	dijit.byId('idEmpleado'+fila).value			= "";
	dojo.byId('tCategoria'+fila).innerHTML 		= "";
	dojo.byId('eCategoriaVacacion'+fila).value 	= "";
	dojo.byId('tDiaDescanso'+fila).innerHTML 	= "";
	dojo.byId('eDiaDescanso'+fila).value 		= "";
}

function validarEmpleado(fila){
	var ultimafila=0;
	var bDistinto=true;
	dojo.query("[id^=\"idEmpleado\"]").forEach(function(nodo, index, array){
		ultimafila=nodo.id.replace('idEmpleado','');
		if(ultimafila>0){
			if(ultimafila!=fila && dijit.byId(nodo.id).value){
				if(dijit.byId('idEmpleado'+fila).value==dijit.byId(nodo.id).value){
					alert('¡Este empleado ya se encuentra seleccionado!');
					dojo.byId('idEmpleado'+fila).value="";
					dijit.byId('idEmpleado'+fila).value="";
					dojo.byId('idEmpleado'+fila).focus();
					bDistinto=false;
				}
			}
		}
	});

	if(bDistinto==true && parseInt(dijit.byId("idEmpleado"+fila).value)>0 && dojo.byId("fhFechaInicio"+fila).value!="" && dojo.byId("fhFechaTermino"+fila).value!=""){
		dojo.byId('eProceso').value=2;
		dojo.byId('eFilaEmp').value=fila;
		dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
			dojo.byId("dvCNS").innerHTML = tRespuesta;
			if(parseInt(dojo.byId('fFechaMayoMen').value)>0){
				alert("¡La Fecha Término, no puede ser menor a Fecha Inicio!");
				dojo.byId('fhFechaTermino'+fila).value="";
				dijit.byId('fhFechaTermino'+fila).value="";
				dojo.byId('fhFechaTermino'+fila).focus();
			}else{
				if(parseInt(dojo.byId('eVacaciones').value)>0){
					alert("¡El empleado cuenta con registro de vacaciones en el periodo de días seleccionado!");
					limpiarUsr(fila);
					dojo.byId('idEmpleado'+fila).focus();
				}else{
					if(parseInt(dojo.byId('eAusentismoBD').value)>0){
						alert("¡El empleado cuenta con registro de ausentismo en el periodo de dias!");
						limpiarUsr(fila);
						dojo.byId('idEmpleado'+fila).focus();
					}else{
						if(parseInt(dojo.byId('eIncapacidad').value)>0){
							alert("¡El empleado cuenta con registro de incapacidad!");
							limpiarUsr(fila);
							dojo.byId('idEmpleado'+fila).focus();
						}else{
							if(parseInt(dojo.byId("eEmpleadoBD").value)>0){
								alert("¡El empleado ya tiene asistencia en el dia!");
								limpiarUsr(fila);
								dojo.byId('idEmpleado'+fila).focus();
							}else{
								dojo.byId('eDiaDescanso'+fila).value 		= (parseInt(dojo.byId('eCodDiaDescanso').value)>0 ? dojo.byId('eCodDiaDescanso').value : "");
								dojo.byId('tCategoria'+fila).innerHTML 		= (parseInt(dojo.byId('eCodCategoriaVacacion').value)>0 ? dojo.byId('Categoria').value : "SIN ASIGNAR!!");
								dojo.byId('tDiaDescanso'+fila).innerHTML 	= (parseInt(dojo.byId('eCodDiaDescanso').value)>0 ? dojo.byId('tDescanso').value : "SIN ASIGNAR!!");
								dojo.byId('eCategoriaVacacion'+fila).value 	= (parseInt(dojo.byId('eCodCategoriaVacacion').value)>0 ? dojo.byId('eCodCategoriaVacacion').value : "");
								validarFila(fila);
								//
								//
							}
						}
					}
				}
			}
		}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});
	}
}

function agregarConcepto(filaAsistencia){ 
	var tb = dojo.byId('tbAsistencias');
	var tr = null;
	var td = null;
	var fila = 0;
	fila = parseInt(dojo.byId("eFilas").value)+1;
	var rows = document.getElementById('tablaAsistencias').getElementsByTagName('tbody')[0].getElementsByTagName('tr');
    for (i = 0; i < rows.length; i++) {
		if(rows[i].id == "filaAsistencia"+filaAsistencia){
			tr = tb.insertRow(rows[i].rowIndex);
		}
    }

	tr.id = "filaAsistencia"+fila;
	td = tr.insertCell(0);
	td.height = '23px';
	td.className = "sanLR04";
	td.innerHTML="<img width=\"16\" align=\"absmiddle\" height=\"16\" onclick=\"borrarAsistencia("+fila+");\" id=\"filaConcepto"+fila+"\" src=\"https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-borrar.png\" title=\"Eliminar Concepto\">";	
	td.noWrap = "true";

	td = tr.insertCell(1);
	td.className = "sanLR04 columnB";
	td.innerHTML='<input type="hidden" name="eRegistro'+fila+'" id="eRegistro'+fila+'" value="'+fila+'"><div dojoType="dojo.data.ItemFileReadStore" jsId="acEmpledos'+fila+'" url="php/auto/bases.php" clearOnClose="true" urlPreventCache="true"></div><input dojoType="dijit.form.FilteringSelect" value="" jsId="cmbEmpleados'+fila+'" store="acEmpledos'+fila+'" searchAttr="nombre" hasDownArrow="false" name="idEmpleado'+fila+'" id="idEmpleado'+fila+'" autoComplete="false" searchDelay="300" highlightMatch="none" placeHolder="Empleados" style="width:330px;" displayMessage="false" required="false" queryExpr = "*" pageSize="10" onChange="validarEmpleado('+fila+');">';
	td.noWrap = "true";
	
	td = tr.insertCell(2);
	td.className = "sanLR04";
	td.innerHTML='<input name="fhFechaInicio'+fila+'" type="date" dojoType="dijit.form.TextBox" id="fhFechaInicio'+fila+'" value="" style="width:140px" onChange="validarEmpleado('+fila+');">';
	
	td = tr.insertCell(3);
	td.className = "sanLR04 columnB";
	td.innerHTML='<input name="fhFechaTermino'+fila+'" type="date" dojoType="dijit.form.TextBox" id="fhFechaTermino'+fila+'" value="" style="width:140px" onChange="validarEmpleado('+fila+');">';

	td = tr.insertCell(4);
	td.className = "sanLR04";
	td.innerHTML='<input type="hidden" name="eCategoriaVacacion'+fila+'" id="eCategoriaVacacion'+fila+'" value=""><label id="tCategoria'+fila+'"></label>';

	td = tr.insertCell(5);
	td.className = "sanLR04 columnB";
	td.innerHTML='<input type="hidden" name="eDiaDescanso'+fila+'" id="eDiaDescanso'+fila+'" value=""><label id="tDiaDescanso'+fila+'"></label>';

	dojo.byId("eFilas").value++;
	dojo.parser.parse('tablaAsistencias');
	asignaEmpleados();
}

function validarFila(filaEmpleado){
	var ultimafila=0;
	var bDistinto=true;
	dojo.query("[id^=\"idEmpleado\"]").forEach(function(nodo, index, array){
		ultimafila=nodo.id.replace('idEmpleado','');
		if(ultimafila>0){
			if(ultimafila!=filaEmpleado && dijit.byId(nodo.id).value){
				if(dijit.byId('idEmpleado'+filaEmpleado).value==dijit.byId(nodo.id).value){
					alert('¡Este empleado ya se encuentra seleccionado!');
					dojo.byId('idEmpleado'+filaEmpleado).value="";
					dijit.byId('idEmpleado'+filaEmpleado).value="";
					dojo.byId('idEmpleado'+filaEmpleado).focus();
					bDistinto=false;
				}
			}
		}
	});

	if(bDistinto==true){
		var ultimafila=0;
		var bGenerarLinea=true;
		var eCodTipoServicio=0;
		
		dojo.query("[id^=\"eRegistro\"]").forEach(function(nodo, index, array){
			ultimafila=parseInt(nodo.id.replace('eRegistro',''));
		});
			
		if(filaEmpleado==ultimafila){
			if(dojo.byId('idEmpleado'+filaEmpleado)){
				if(!dijit.byId('idEmpleado'+filaEmpleado).value){
					bGenerarLinea=false;
				}
			}
			if(dojo.byId('fhFechaInicio'+filaEmpleado)){
				if(!dojo.byId('fhFechaInicio'+filaEmpleado).value){
					bGenerarLinea=false;
				}
			}
			if(dojo.byId('fhFechaTermino'+filaEmpleado)){
				if(!dojo.byId('fhFechaTermino'+filaEmpleado).value){
					bGenerarLinea=false;
				}
			}
			if(bGenerarLinea==true){
				agregarConcepto(filaEmpleado);
			}
		}
	}
}

function consultar(){
	document.location = './?ePagina=2.5.1.5.php';
}

function listaempleados(codigo){
	var id = codigo.replace("idEmpleado","")
	var busqueda = dojo.byId(codigo).value;
	var acEmp='acEmpledos'+id;
	var cmbEmp='cmbEmpleados'+id;
	eval(acEmp).url = "php/auto/bases.php?busqueda="+busqueda;
	eval(acEmp).close();
	eval(cmbEmp).store=eval(acEmp);
}

function asignaEmpleados(){
	dojo.query("[id*=\"idEmpleado\"]").forEach(function(nodo, index, array){
		dojo.connect(dijit.byId(nodo.id), "onKeyUp", function(event){
			if(event.keyCode!=40&&event.keyCode!=38){
				listaempleados(nodo.id);
			}
		});
	});
}

dojo.addOnLoad(function(){
	dojo.parser.parse('tablaAsistencias');
	asignaEmpleados();
});
</script>
<div id="dvCNS" style="display:none;"></div>
<form id="Datos" name="Datos" method="post" onSubmit="return false;" enctype="multipart/form-data">
<input type="hidden" name="eProceso" id="eProceso" value="" />
<input type="hidden" name="eServicio" id="eServicio" value="" />
<input type="hidden" name="eUsuario" id="eUsuario" value="<?=(int)$_SESSION['sesionUsuario']['eCodUsuario'];?>" />
<input type="hidden" name="eCodEntidad" id="eCodEntidad" value="<?=(int)$_SESSION['sesionUsuario']['eCodEntidad'];?>" />
<input type="hidden" name="mail" id="mail" value="<?=$_SESSION['sesionUsuario']['Correo'];?>" />
<input type="hidden" name="nombreusuario" id="nombreusuario" value="<?=$_SESSION['sesionUsuario']['Usuario'];?>" />
<input type="hidden" name="eFilaEmp" id="eFilaEmp" value="" />
<input type="hidden" name="eFDiaria" id="eFDiaria" value="<?=date('Ymd');?>" />
<div id="dvFacturacion"></div>
<table border="0" cellpadding="0" cellspacing="0" width="980px">
	    
    <!--tr>
        <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" width="16" height="16" align="absmiddle"> Fecha de Ausentismo </td>
        <td width="50%" nowrap class="sanLR04">
			<input name="fhFechaAsistencia" type="date" dojoType="dijit.form.TextBox" id="fhFechaAsistencia" value="<?=date('Y-m-d');?>" style="width:140px" onChange="verificarEmpleados();">	
        </td>
    </tr-->
    <tr>
        <td height="23" nowrap class="sanLR04"></td>
        <td width="50%" nowrap class="sanLR04"></td>
        <td nowrap class="sanLR04"></td>
        <td width="50%" nowrap class="sanLR04"></td>
    </tr>
    <tr>
    	<td colspan="4">
    		<div id="divgral" style="display:block; top:0; left:0; width:970px; z-index=1; overflow: auto;">
	            <table cellspacing="0" border="0" width="100%" id="tablaAsistencias" name="tablaAsistencias">
	                <thead>
	                    <tr class="thEncabezado">
	                        <td nowrap="nowrap" class="sanLR04" height="23"> </td>
	                        <td nowrap="nowrap" class="sanLR04"> Empleado</td>
	                        <td nowrap="nowrap" class="sanLR04"> Fecha Inicio</td>
	                        <td nowrap="nowrap" class="sanLR04"> Fecha T&eacute;rmino</td>
	                        <td nowrap="nowrap" class="sanLR04"> Categor&iacute;a</td>
	                        <td nowrap="nowrap" class="sanLR04" width="100%">Descanso</td>
	                    </tr>
	                </thead>
	                <tbody id="tbAsistencias" name="tbAsistencias">
	                    <tr id="filaAsistencia1" name="filaAsistencia1">
	                        <td nowrap="nowrap" class="sanLR04" height="23"><img width="16" align="absmiddle" height="16" onclick="borrarAsistencia(1);" id="filaConcepto1" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-borrar.png" title="Eliminar Concepto"></td>
	                        <td nowrap="nowrap" class="sanLR04 columnB" height="23"><input type="hidden" name="eRegistro1" id="eRegistro1" value="1">
	                        <div dojoType="dojo.data.ItemFileReadStore" jsId="acEmpledos1" url="php/auto/bases.php" clearOnClose="true" urlPreventCache="true"></div>
	                        <input dojoType="dijit.form.FilteringSelect" 
	                            value="<?=$rDepositoMovimiento['eCodSucursalCliente'];?>"
	                            jsId="cmbEmpleados1"
	                            store="acEmpledos1"
	                            searchAttr="nombre"
	                            hasDownArrow="false"
	                            name="idEmpleado1"
	                            id="idEmpleado1"
	                            autoComplete="false"
	                            searchDelay="300"
	                            highlightMatch="none"
	                            placeHolder="Empleados" 
	                            style="width:330px;"
	                            displayMessage="false"
	                            required="false"
	                            queryExpr = "*"
	                            pageSize="10"
	                            onChange="validarEmpleado(1);"
	                            onChange=""
	                            >
	                        </td>
	                        <td nowrap="nowrap" class="sanLR04"><input name="fhFechaInicio1" type="date" dojoType="dijit.form.TextBox" id="fhFechaInicio1" value="" style="width:140px" onChange="validarEmpleado(1);"></td>
	                        <td nowrap="nowrap" class="sanLR04 columnB"><input name="fhFechaTermino1" type="date" dojoType="dijit.form.TextBox" id="fhFechaTermino1" value="" style="width:140px" onChange="validarEmpleado(1);"></td>
	                        <td nowrap="nowrap" class="sanLR04"><input type="hidden" name="eCategoriaVacacion1" id="eCategoriaVacacion1" value=""><label id="tCategoria1"></label></td>
	                        <td nowrap="nowrap" class="sanLR04 columnB"><input type="hidden" name="eDiaDescanso1" id="eDiaDescanso1" value=""><label id="tDiaDescanso1"></label></td>
	                    </tr>
	                </tbody>
	            </table>
	        </div>
            <input name="eFilas" id="eFilas" value="1" type="hidden">
        </td>
    </tr>
	<tr><td height="20"></td></tr>
    <tr><td valign="top" nowrap="nowrap" class="sanLR04" colspan="4"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle" /> <font class="fntG10K">Indica que es obligatorio llenar el campo.</font></td></tr>
</table>
</form>
<?php } ?>