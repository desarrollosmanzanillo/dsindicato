<?php require_once("conexion/soluciones-mysql.php"); 
$link = getLink(); 
$select=" SELECT oss.eCodSolicitud, ces.tNombre AS Estatus, osm.fhFechaSalida, oss.fhFechaEntrada, ".
		" cen.tNombre AS Naviera, oss.tPatente, cec.tNombre AS Cliente, cef.tNombre AS FacturarA, ".
		" cbu.tNombre AS Buque, oss.tNumeroViaje, cto.tNombre AS TipoServicio, osm.tObservaciones, ".
		" oss.eCodTipoServicio, oss.tCodContenedor, ctc.tNombreCorto AS TipoContenedor ".
		" FROM opesalidasmercancias osm ".
		" INNER JOIN opeentradasmercancias oss ON oss.eCodSalida=osm.eCodSalida ".
		" INNER JOIN catestatus ces ON ces.tCodEstatus=osm.tCodEstatus ".
		" INNER JOIN catentidades cen ON cen.eCodEntidad=osm.eCodNaviera ".
		" INNER JOIN catentidades cec ON cec.eCodEntidad=osm.eCodCliente ".
		" INNER JOIN catentidades cef ON cef.eCodEntidad=osm.eCodFacturarA ".
		" LEFT JOIN catbuques cbu ON cbu.eCodBuque=oss.eCodBuque ".
		" INNER JOIN cattiposservicios cto ON cto.eCodTipoServicio=oss.eCodTipoServicio ".
		" LEFT JOIN cattiposcontenedores ctc ON ctc.eCodTipoContenedor=oss.eCodTipoContenedor ".
		" WHERE osm.eCodSalida=".(int)$_GET['eCodSalida'];
$rSolicitud=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

?>
<script type="text/javascript">
function consultar(){
	document.location = './?ePagina=2.5.5.2.php';
}
</script>
<div id="dvCNS" style="display:none;"></div>
<form action="" method="post" name="Datos" id="Datos" onsubmit="return false;">
<table border="0" cellpadding="0" cellspacing="0">
    <tr><td height="20"></td></tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"> C&oacute;digo</td>
	    <td width="50%" nowrap class="sanLR04"><?=sprintf("%07d",(int)$_GET['eCodSalida']);?></td>
		<td nowrap class="sanLR04"> Estatus</td>
	    <td width="50%" nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'Estatus'})?></td>
    </tr>
	<tr>
		<td height="23" nowrap class="sanLR04"> Fecha de Salida</td>
	    <td nowrap class="sanLR04"><?=date("d/m/Y", strtotime($rSolicitud{'fhFechaSalida'}));?></td>
    </tr>
	<tr>
		<td height="23" nowrap class="sanLR04"> Naviera</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'Naviera'})?></td>
		<td nowrap class="sanLR04"> Patente</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'tPatente'} ? $rSolicitud{'tPatente'} : "N/D")?></td>
    </tr>
	<tr>
		<td height="23" nowrap class="sanLR04"> Cliente</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'Cliente'})?></td>
		<td nowrap class="sanLR04"> Facturar a</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'FacturarA'})?></td>
    </tr>
	<tr>
		<td height="23" nowrap class="sanLR04"> Buque</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'Buque'})?></td>
		<td nowrap class="sanLR04"> Viaje</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'tNumeroViaje'})?></td>
    </tr>
	<tr>
		<td height="23" nowrap class="sanLR04"> Tipo de Servicio</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'TipoServicio'})?></td>
		<td height="23" nowrap class="sanLR04"></td>
	    <td nowrap class="sanLR04"></td>
    </tr>
	<?php if((int)$rSolicitud{'eCodTipoServicio'}==1){ ?>
		<tr>
			<td height="23" nowrap class="sanLR04"> Contenedor</td>
			<td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'tCodContenedor'})?></td>
			<td height="23" nowrap class="sanLR04"> Tipo de Contenedor</td>
			<td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'TipoContenedor'})?></td>
		</tr>
		<tr>
			<td height="23" nowrap class="sanLR04"> Observaciones</td>
			<td nowrap class="sanLR04" colspan="3"><?=utf8_encode($rSolicitud{'tObservaciones'})?></td>
		</tr>
	<?php } ?>
</table>
</form>