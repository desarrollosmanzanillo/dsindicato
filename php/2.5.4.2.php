﻿<?php 
require_once("conexion/soluciones-mysql.php"); 
$link = getLink();
if($_POST){
	date_default_timezone_set('America/Mexico_City');
	$varfecha=isset($_POST["fecha"]) ? $_POST["fecha"] : date("Y-m-d");
	$fechaayer = date('Y-m-d',strtotime('-1 days', strtotime($varfecha)));
	$fechaantier = date('Y-m-d',strtotime('-2 days', strtotime($varfecha)));
		
	$select=" SELECT * FROM sisconfiguracion ";
	$configura=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
	
	$select=" SELECT * FROM categorias WHERE indice=27 ";
	$salarioMin=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);//

	$select=" SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."' ";
	$incapacidades=mysqli_query($link,$select);
	$UsuariosInc="0";
	while($incapacidad=mysqli_fetch_array($incapacidades,MYSQLI_ASSOC)){
		$UsuariosInc.=", ".$incapacidad{'idEmpleado'};
	}
	$select=" SELECT assi.id ".
			" FROM movimientosafiliatorios mos ".
			" INNER JOIN relasistenciamovimientos rassi ON rassi.eCodMovimiento=mos.eCodMovimiento ".
			" INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia ".
			" WHERE assi.Fecha Between '".$fechaantier."' AND '".$fechaayer."' ";
	$afiliatorios=mysqli_query($link,$select);
	$UsuariosAfil="0";
	while($afiliatorio=mysqli_fetch_array($afiliatorios,MYSQLI_ASSOC)){
		$UsuariosAfil.=", ".$afiliatorio{'id'};
	}
	$select=" SELECT assihh.idEmpleado ".
			" FROM movimientosafiliatorios moshh ".
			" INNER JOIN relasistenciamovimientos rassihh ON rassihh.eCodMovimiento=moshh.eCodMovimiento ".
			" INNER JOIN asistencias assihh ON assihh.id=rassihh.eCodAsistencia ".
			" WHERE moshh.fhFecha='".$fechaantier."' AND moshh.tCodTipoMovimiento NOT IN ('02') AND moshh.eCodEstatus='AC' ";
	$movimientos=mysqli_query($link,$select);
	$UsiariosMovi="0";
	while($movimiento=mysqli_fetch_array($movimientos,MYSQLI_ASSOC)){
		$UsiariosMovi.=", ".$movimiento{'idEmpleado'};
	}
	//Asistencias de ayer
	//Asistencias activas
	//Empleado sin incapacidad
	//La asistencia no debe tener movimiento
	//
	$select=" 
	SELECT DISTINCT cent.tRegistroPatronal AS rp, emple.IMSS AS imss, emple.Paterno AS pat, Materno AS mat, Nombre AS nomb, ctiemp.eCodTipoTrabajador, ctiemp.eCodTipoSalario as TipoSalario, ctiemp.eCodTipoJornada AS tipjo, emple.Clinica AS clin, emple.CURP AS curp, 
CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)>".(float)$configura{'dTopeSalario'}." 
THEN ".(float)$configura{'dTopeSalario'}." 
ELSE 
	CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)=0 
	THEN FORMAT((".(float)$salarioMin{'Turno1'}."*ctiemp.dFactorIntegracion),2) 
	ELSE
		CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)<".$configura{'dSalarioMinimo'}." 
		THEN ".$configura{'dSalarioMinimo'}." 
		ELSE (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado) 
		END
	END
END AS Salario
FROM asistencias asis
INNER JOIN empleados emple ON emple.id=asis.idEmpleado
INNER JOIN cattipoempleado ctiemp ON ctiemp.eCodTipoEntidad=emple.eCodTipo
LEFT JOIN catentidades cent ON cent.eCodEntidad=asis.idEmpresa
WHERE asis.Fecha Between '".$fechaantier."' AND '".$fechaayer."' 
AND emple.eCodTipo=1
AND asis.Estado='AC' 
AND asis.idEmpleado NOT IN (".$UsuariosInc.") 
AND asis.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asis.id) 
AND asis.id NOT IN (".$UsuariosAfil.")
AND asis.idEmpleado IN (SELECT assi.idEmpleado 
FROM movimientosafiliatorios mos 
INNER JOIN relasistenciamovimientos rassi ON rassi.eCodMovimiento=mos.eCodMovimiento 
INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia
WHERE assi.idEmpleado=asis.idEmpleado AND mos.fhFecha='".$fechaantier."' AND mos.tCodTipoMovimiento NOT IN ('02') AND mos.eCodEstatus='AC')
AND 
CASE WHEN (SELECT mosa.dImporte
FROM movimientosafiliatorios mosa 
INNER JOIN relasistenciamovimientos rassia ON rassia.eCodMovimiento=mosa.eCodMovimiento 
INNER JOIN asistencias assia ON assia.id=rassia.eCodAsistencia
WHERE assia.idEmpleado=asis.idEmpleado AND mosa.fhFecha='".$fechaantier."' AND mosa.tCodTipoMovimiento NOT IN ('02') AND mosa.eCodEstatus='AC' LIMIT 1) IS NULL 
THEN 0 
ELSE (SELECT mosa.dImporte
FROM movimientosafiliatorios mosa 
INNER JOIN relasistenciamovimientos rassia ON rassia.eCodMovimiento=mosa.eCodMovimiento 
INNER JOIN asistencias assia ON assia.id=rassia.eCodAsistencia
WHERE assia.idEmpleado=asis.idEmpleado AND mosa.fhFecha='".$fechaantier."' AND mosa.tCodTipoMovimiento NOT IN ('02') AND mosa.eCodEstatus='AC' LIMIT 1) END
<> 
CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)>".(float)$configura{'dTopeSalario'}." 
THEN ".(float)$configura{'dTopeSalario'}." 
ELSE 
	CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)=0 
	THEN FORMAT((".(float)$salarioMin{'Turno1'}."*ctiemp.dFactorIntegracion),2) 
	ELSE 
		CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)<".$configura{'dSalarioMinimo'}." 
		THEN ".$configura{'dSalarioMinimo'}." 
		ELSE (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado) 
		END
	END
END ";
$rsServicios=mysqli_query($link,$select);
$cont=0;
$registros=(int)mysqli_num_rows($rsServicios);
?>
<table cellspacing="0" border="0" width="980px">
  <tr>
    <td width="50%"><hr color="#666666" /></td>
    <td class="sanLR04" nowrap="nowrap"><b>Registros Encontrados: (     <?=$registros;?>     )</b></td>
    <td width="50%"><hr color="#666666" /></td>
  </tr>
</table>
<div style="display:block; top:0; left:0; width:980px; z-index=1; overflow: auto;">
  <table cellspacing="0" border="0" width="980px">
    <thead>
      <tr class="thEncabezado">	      	
      <td nowrap="nowrap" class="sanLR04">R. Patronal</td>
	    <td nowrap="nowrap" class="sanLR04">NSS</td>
	    <td nowrap="nowrap" class="sanLR04">Paterno</td> 
	    <td nowrap="nowrap" class="sanLR04">Materno</td>
      <td nowrap="nowrap" class="sanLR04">Nombre(s)</td>
      <td nowrap="nowrap" class="sanLR04">S.D.I.</td>
      <td nowrap="nowrap" class="sanLR04">S. Infornavit</td>
      <td nowrap="nowrap" class="sanLR04">T. Trabajador</td>
      <td nowrap="nowrap" class="sanLR04">T. Salario</td>
      <td nowrap="nowrap" class="sanLR04">Semana</td>
      <td nowrap="nowrap" class="sanLR04">F. Movimiento</td>
      <td nowrap="nowrap" class="sanLR04">U.M.F</td>
      <td nowrap="nowrap" class="sanLR04">Movimiento</td>
      <td nowrap="nowrap" class="sanLR04">Guia</td>
      <td nowrap="nowrap" class="sanLR04">C. Trabajador</td>
      <td nowrap="nowrap" class="sanLR04">Filler</td>
      <td nowrap="nowrap" class="sanLR04">CURP</td>      
	    <td nowrap="nowrap" class="sanLR04" width="100%">Identificador</td> <!--Ya quedo-->                    
	  </tr>   
    </thead>
    <tbody>
      <?php $i=1; while($rServicio=mysqli_fetch_array($rsServicios,MYSQLI_ASSOC)){
                        $salario=explode(".",str_replace(",", "", number_format($rServicio["Salario"], 2)));
                        if(strlen($salario[0])<=3)
                          $cadena="0".$salario[0].$salario[1];
                        else
                          $cadena=$salario[0].$salario[1];
                        $fech=$fechaayer; 
                        $fecha=explode("-",$fech); 
                        $abc=$fecha[2].$fecha[1].$fecha[0]; 
        ?>
      <tr>        
        <input type="hidden" id="id<?=$i?>" name="id<?=$i?>" value="<?=$rServicio["numeral"]?>">        
        <td nowrap="nowrap" class="sanLR04 columnB"><b><?=$rServicio{'rp'};?></b></td>                              
        <td nowrap="nowrap" class="sanLR04"><?= utf8_decode($rServicio["imss"]); ?></td>
        <td nowrap="nowrap" class="sanLR04 columnB"><?= utf8_decode($rServicio["pat"]); ?></td>
        <td nowrap="nowrap" class="sanLR04"><?= utf8_decode($rServicio["mat"]); ?></td>
        <td nowrap="nowrap" class="sanLR04 columnB"><?= utf8_decode($rServicio["nomb"]); ?></td>
<td nowrap="nowrap" class="sanLR04"><?= utf8_decode($cadena); ?></td>
        <td nowrap="nowrap" class="sanLR04 columnB">000000</td>
        <td nowrap="nowrap" class="sanLR04"><?=utf8_decode($rServicio["eCodTipoTrabajador"]);?></td>
        <td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_decode($rServicio["TipoSalario"]);?></td>
		<td nowrap="nowrap" class="sanLR04"><?= utf8_decode($rServicio["tipjo"]); ?></td>
<td nowrap="nowrap" class="sanLR04 columnB"><?= utf8_decode($abc); ?></td>
        <td nowrap="nowrap" class="sanLR04 "><?= utf8_decode($rServicio["clin"]); ?></td>
        <td nowrap="nowrap" class="sanLR04 columnB">07</td>
        <td nowrap="nowrap" class="sanLR04 ">03400</td>
        <td nowrap="nowrap" class="sanLR04 columnB">ESTIBADOR</td>
        <td nowrap="nowrap" class="sanLR04 "></td>
        <td nowrap="nowrap" class="sanLR04 columnB"><?= utf8_decode($rServicio["curp"]); ?></td>
        <td nowrap="nowrap" class="sanLR04 " width="100%">9</td>
        
      </tr>
      <?php $i++; } ?>
    </tbody>
  </table>
</div>
<?php }else{ ?>
<script type="text/javascript">
function generarBatch(){
	var UrlBatch = "php/batch/2.5.4.2.php?"+
    (dojo.byId('fecha').value ? "&fecha="+dojo.byId('fecha').value : "");    
    var Datos = document.createElement("FORM");
    document.body.appendChild(Datos);
    Datos.name='Datos';
    Datos.method = "POST";
    Datos.action = UrlBatch;
    Datos.submit();
}



dojo.addOnLoad(function(){filtrarConsulta();});
</script>
<form name="Datos" id="Datos" method="post" action="" onsubmit="return false;">
  <input type="hidden" value="0" name="ePagina" id="ePagina" />
  <input type="hidden" value="" name="eAccion" id="eAccion" />
  <table width="900px" border="0">
    <tr>
      <td colpan="3" width="100%"></td>
      <td align="right" nowrap="nowrap"><a class="fntBR11" href="javaScript:mostrarFiltros('Busqueda');">Busqueda de Registros</a></td>
    </tr>
    <tr id="trBusqueda" style="display:none">
      <td colspan="4"><table width="900px" bgcolor="#f9f9f9">
          <tr>
            <td class="sanLR04" height="5"></td>
          </tr>
          <tr>
            <td class="sanLR04" height="20">Fecha</td>
            <td class="sanLR04" width="50%"><input type="date" name="fecha" dojoType="dijit.form.TextBox" id="fecha" value="<?=$_POST["fecha"] ? $_POST["fecha"] : date("Y-m-d")?>" style="width:140px"></td>
            <td class="sanLR04"></td>
            <td class="sanLR04" width="50%"></td>
          </tr>
          <tr>
            <td class="sanLR04" height="5"></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td colspan="4"><div id="dvCNS"></div></td>
    </tr>
  </table>
</form>
<?php } ?>
