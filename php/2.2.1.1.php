<?php
require_once("conexion/soluciones-mysql.php"); 
$link = getLink();
if($_POST){
	if($_POST['eAccion']==1){
		foreach($_POST as $k => $valor){
			$nombre = strval($k);
			$campo = "eCodUsuario";
			if(strstr($nombre,$campo) && (int)$valor>0){
				$update=" UPDATE catusuarios SET tCodEstatus='CA' WHERE eCodUsuario=".$valor;
				mysqli_query($link,$update);
			}
		}
	}

	$select=" SELECT t2.tSiglas, t1.* ".
			" FROM catusuarios t1 inner join catentidades t2 on t1.eCodEntidad=t2.eCodEntidad".
			" WHERE 1=1".
			($_POST['eCodUsuario'] ? " AND t1.eCodUsuario=".$_POST['eCodUsuario'] : "").
			($_POST['tNombre'] ? " AND CONCAT(t1.tNombre, t1.tApellidos) LIKE '%".$_POST['tNombre']."%'" : "").
			($_POST['eCodEntidad'] ? " AND t1.eCodEntidad=".$_POST['eCodEntidad'] : "").
			($_POST['Estatus'] ? " AND t1.tCodEstatus='".$_POST['Estatus']."'" : "").	
			($_POST['tUsuario'] ? " AND t1.tUsuario LIKE '%".$_POST['tUsuario']."%'" : "");
			
	$rsUsuarios=mysqli_query($link,$select);
	$registros=(int)mysqli_num_rows($rsUsuarios);
	?>
    <table cellspacing="0" border="0" width="980px">
		<tr>
			<td width="50%"><hr color="#666666" /></td>
			<td class="sanLR04" nowrap="nowrap"><b>Registros Encontrados: ( <?=$registros;?> )</b></td>
			<td width="50%"><hr color="#666666" /></td>
		</tr>
    </table>
	<div style="display:block; top:0; left:0; width:980px; z-index=1; overflow: auto;">    
	<table cellspacing="0" border="0" width="980px">
		<thead>
			<tr class="thEncabezado">
				<td nowrap="nowrap" class="sanLR04" height="20" align="center">C</td>
				<td nowrap="nowrap" class="sanLR04" align="center">E</td>
				<td nowrap="nowrap" class="sanLR04">C&oacute;digo</td>                
				<td nowrap="nowrap" class="sanLR04">Nombre</td>
                <td nowrap="nowrap" class="sanLR04">Entidad</td>
				<td nowrap="nowrap" class="sanLR04" width="100%">Usuario</td>
                <td nowrap="nowrap" class="sanLR04" width="100%">Contrase&ntilde;a</td>
			</tr>
		</thead>
		<tbody>
			<?php $i=1; while($rUsuario=mysqli_fetch_array($rsUsuarios,MYSQLI_ASSOC)){ ?>
				<tr>
					<td nowrap="nowrap" class="sanLR04" height="20" align="center"><input type="checkbox" name="eCodUsuario<?=$i?>" id="eCodUsuario<?=$i?>" value="<?=$rUsuario{'eCodUsuario'};?>" ></td>
					<td nowrap="nowrap" class="sanLR04" height="20" align="center"><img width="16" height="16" alt="Usuario" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-<?=$rUsuario{'tCodEstatus'};?>.png"></td>
					<td nowrap="nowrap" class="sanLR04 colmenu" ><a  href="?ePagina=2.2.1.1.1.php&eCodUsuario=<?=$rUsuario{'eCodUsuario'};?>"><b><?=sprintf("%07d",$rUsuario{'eCodUsuario'});?></b></a></td>                    
					<td nowrap="nowrap" class="sanLR04" ><a class="txtCO12" href="?ePagina=2.2.1.1.2.php&eCodUsuario=<?=$rUsuario{'eCodUsuario'};?>"><?=utf8_encode($rUsuario{'tNombre'}." ".$rUsuario{'tApellidos'});?></a></td>
                    <td nowrap="nowrap" class="sanLR04 columnB" width="100%"><?=utf8_encode($rUsuario{'tSiglas'});?></td>
					<td nowrap="nowrap" class="sanLR04 " width="100%"><?=utf8_encode($rUsuario{'tUsuario'});?></td>
                    <td nowrap="nowrap" class="sanLR04 columnB" width="100%"><?=((int)$_POST['eUs']==1 ? utf8_encode($rUsuario{'tContrasena'}) : '***');?></td>
				</tr>
			<?php $i++; } ?>
		</tbody>
	</table>
	</div>
<?php }else{ ?>
	<script type="text/javascript">
	function nuevo(){
		document.location = "?ePagina=2.2.1.1.1.php";
	}

	function eliminar(){
		var eChk = 0;
		dojo.byId('eAccion').value = 1;	
		dojo.query("[id*=\"eCodUsuario\"]:checked").forEach(function(nodo, index, array){eChk++;});
	
		if(eChk!=0){
			if(confirm("¿Desea eliminar los usuarios?")){
				dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+'.php', handleAs: "text", load: function(tRespuesta, ioArgs){
					dojo.byId('dvCNS').innerHTML = tRespuesta; 
					alert("¡Los registros se han eliminado exitosamente!");	
				}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});
			}
		}else{
			alert("No ha seleccionado ningún usuario");
		}
		dojo.byId('eAccion').value = "";	
	}
	dojo.addOnLoad(function(){filtrarConsulta();});
	</script>
	<form name="Datos" id="Datos" method="post" action="" onsubmit="return false;">
		<input type="hidden" value="0" name="ePagina" id="ePagina" />
		<input type="hidden" value="" name="eAccion" id="eAccion" />
		<input type="hidden" value="<?=(int)$_SESSION['sesionUsuario']['eCodUsuario'];?>" name="eUs" id="eUs" />
		<table width="980px" border="0">
			<tr>
				<td colpan="3" width="100%"></td>
				<td align="right" nowrap="nowrap"><a class="fntBR11" href="javaScript:mostrarFiltros('Busqueda');">Busqueda de Registros</a></td>
			</tr>
			<tr id="trBusqueda" style="display:none">
				<td colspan="4">
					<table width="100%" bgcolor="#f9f9f9">
						<tr><td class="sanLR04" height="5"></td></tr>
						<tr>
							<td class="sanLR04" height="20">C&oacute;digo</td>
							<td class="sanLR04" width="50%"><input type="text" name="eCodUsuario" dojoType="dijit.form.TextBox" id="eCodUsuario" value="" style="width:80px"></td>
							<td class="sanLR04">Nombre</td>
							<td class="sanLR04" width="50%"><input type="text" name="tNombre" dojoType="dijit.form.TextBox" id="tNombre" value="" style="width:175px"></td>
						</tr>
						<tr>
							<td class="sanLR04" height="20">Usuario</td>
							<td class="sanLR04" width="50%"><input type="text" name="tUsuario" dojoType="dijit.form.TextBox" id="tUsuario" value="" style="width:175px"></td>
							<td class="sanLR04">Entidad</td>
							<td class="sanLR04" width="50%"><select name='eCodEntidad' id='eCodEntidad' style="width:175px">
                  			<option value='0'></option>
                  			<?php $sel = mysqli_query($link,"SELECT eCodEntidad as id, tSiglas as Siglas FROM catentidades where tCodEstatus='AC' and eCodTipoEntidad=1 ");                
                              while($row = mysqli_fetch_array($sel,MYSQLI_ASSOC)){ ?>
                  				<option value='<?php echo $row["id"]; ?>'> <?php echo $row["Siglas"]; ?> </option>
                  			<?php } ?>
                			</select></td>
						</tr>	
						<tr>
							<td class="sanLR04" height="20">Estatus</td>
							<td class="sanLR04" width="50%"><select name='Estatus' id='Estatus' style="width:175px">
                				<option value='0'></option>
                				<option value='AC'>ACTIVO</option>
                				<option value='CA'>CANCELADO</option>            				
              				</select>
            				</td>
							<td class="sanLR04"></td>
							<td class="sanLR04" width="50%"></td>
						</tr>					
					</table>
				</td>
			</tr>
			<tr><td colspan="4"><div id="dvCNS"></div></td></tr>
		</table>
	</form>
<?php } ?>