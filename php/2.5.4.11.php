﻿<?php require_once("conexion/soluciones-mysql.php"); 
$link = getLink();
if($_POST){
    date_default_timezone_set('America/Mexico_City');
    $varfecha=isset($_POST["fecha"]) ? $_POST["fecha"] : date("Y-m-d");
    //if($_POST['eCodTipoEntidad']==2){
        $fechaayer = date('Y-m-d',strtotime($varfecha));
        $fechaantier = date('Y-m-d',strtotime('-1 days', strtotime($varfecha)));
    /*}else{
        $fechaayer = date('Y-m-d',strtotime('-1 days', strtotime($varfecha)));
        $fechaantier = date('Y-m-d',strtotime('-2 days', strtotime($varfecha)));
    }*/
    $idUsuario = ($_POST['eUsuario'] ? $_POST['eUsuario'] : "NULL");
    
    $select=" SELECT * FROM sisconfiguracion ";
    $configura=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
    
    //////Inicio Continuaciones-->
    $fechaayerC = date('Y-m-d',strtotime('-1 days', strtotime($varfecha)));
    $fechaantierC = date('Y-m-d',strtotime('-2 days', strtotime($varfecha)));
    $select=" SELECT * ".
            " FROM categorias ".
            " WHERE indice=11 ";
    $salarioMin=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
    
    $select=" SELECT idEmpleado ".
            " FROM incapacidades ".
            " WHERE Estado='AC' AND FechaInicial<='".$fechaayerC."' AND FechaFinal>='".$fechaayerC."' ";
    $incapacidades=mysqli_query($link,$select);
    $UsuariosInc="0";
    while($incapacidad=mysqli_fetch_array($incapacidades,MYSQLI_ASSOC)){
        $UsuariosInc.=", ".$incapacidad{'idEmpleado'};
    }
    $select=" SELECT assi.id ".
            " FROM movimientosafiliatorios mos ".
            " INNER JOIN relasistenciamovimientos rassi ON rassi.eCodMovimiento=mos.eCodMovimiento ".
            " INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia ".
            " LEFT JOIN empleados AS emp ON assi.idEmpleado = emp.id ".
            " WHERE assi.idEmpresa=".(int)$_POST['eCodEntidad'].
            " AND assi.Fecha Between '".$fechaantierC."' AND '".$fechaayerC."' ".
            ((int)$_POST['eCodTipoEntidad']>0 ? " AND emp.eCodTipo=".(int)$_POST['eCodTipoEntidad'] : "");
    $afiliatorios=mysqli_query($link,$select);
    $UsuariosAfil="0";
    while($afiliatorio=mysqli_fetch_array($afiliatorios,MYSQLI_ASSOC)){
        $UsuariosAfil.=", ".$afiliatorio{'id'};
    }
    $select=" SELECT assihh.idEmpleado ".
            " FROM movimientosafiliatorios moshh ".
            " INNER JOIN relasistenciamovimientos rassihh ON rassihh.eCodMovimiento=moshh.eCodMovimiento ".
            " INNER JOIN asistencias assihh ON assihh.id=rassihh.eCodAsistencia ".
            " LEFT JOIN empleados AS emp ON assihh.idEmpleado=emp.id ".
            " WHERE moshh.fhFecha='".$fechaantierC."' AND moshh.tCodTipoMovimiento NOT IN ('02') AND moshh.eCodEstatus='AC' ".
            " AND assihh.idEmpresa=".(int)$_POST['eCodEntidad'].
            ((int)$_POST['eCodTipoEntidad']>0 ? " AND emp.eCodTipo=".(int)$_POST['eCodTipoEntidad'] : "");
    $movimientos=mysqli_query($link,$select);
    $UsiariosMovi="0";
    while($movimiento=mysqli_fetch_array($movimientos,MYSQLI_ASSOC)){
        $UsiariosMovi.=", ".$movimiento{'idEmpleado'};
    }

    $select=" SELECT DISTINCT emple.id AS numeral, emple.IMSS AS imss, cent.tRegistroPatronal AS rp, emple.Paterno AS pat,
            Materno AS mat, Nombre AS nomb, ctiemp.eCodTipoJornada AS tipjo, '".$fechaayerC."' AS Fecha, emple.Clinica AS clin, emple.CURP AS curp,
            CASE 
                WHEN (SELECT MAX(asish.Salario+asish.dBono)
                    FROM asistencias asish 
                    WHERE asish.Fecha Between '".$fechaantierC."' AND '".$fechaayerC."' AND asish.idEmpresa=".(int)$_POST['eCodEntidad']." AND asish.Estado='AC' 
                    AND asish.idEmpleado NOT IN (".$UsuariosInc.") AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
                    AND asish.id NOT IN (".$UsuariosAfil.") AND asish.idEmpleado IN (".$UsiariosMovi.") AND asish.idEmpleado=asis.idEmpleado)>".(float)$configura{'dTopeSalario'}." 
                THEN ".(float)$configura{'dTopeSalario'}." 
                ELSE 
                    CASE 
                        WHEN (SELECT MAX(asish.Salario+asish.dBono)
                            FROM asistencias asish 
                            WHERE asish.Fecha Between '".$fechaantierC."' AND '".$fechaayerC."' AND asish.idEmpresa=".(int)$_POST['eCodEntidad']." AND asish.Estado='AC' 
                            AND asish.idEmpleado NOT IN (".$UsuariosInc.") AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
                            AND asish.id NOT IN (".$UsuariosAfil.") AND asish.idEmpleado IN (".$UsiariosMovi.") 
                            AND asish.idEmpleado=asis.idEmpleado)=0 THEN FORMAT((".(float)$salarioMin{'Turno1'}."*ctiemp.dFactorIntegracion),2)
                        ELSE CASE WHEN (SELECT MAX(asish.Salario+asish.dBono)
            FROM asistencias asish 
            WHERE asish.Fecha Between '".$fechaantierC."' AND '".$fechaayerC."'
            AND asish.idEmpresa=".(int)$_POST['eCodEntidad']."
            AND asish.Estado='AC' 
            AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
            AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
            AND asish.id NOT IN (".$UsuariosAfil.")
            AND asish.idEmpleado IN (".$UsiariosMovi.")
            AND asish.idEmpleado=asis.idEmpleado)<".$configura{'dSalarioMinimo'}." THEN ".$configura{'dSalarioMinimo'}." 
            ELSE (SELECT MAX(asish.Salario+asish.dBono)
                FROM asistencias asish 
                WHERE asish.Fecha Between '".$fechaantierC."' AND '".$fechaayerC."'
                AND asish.idEmpresa=".(int)$_POST['eCodEntidad']."
                AND asish.Estado='AC' 
                AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
                AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
                AND asish.id NOT IN (".$UsuariosAfil.")
                AND asish.idEmpleado IN (".$UsiariosMovi.")
                AND asish.idEmpleado=asis.idEmpleado)
            END 
        END
    END AS tSalario ".
    " FROM asistencias asis ".
    " INNER JOIN empleados emple ON emple.id=asis.idEmpleado ".
    " INNER JOIN cattipoempleado ctiemp ON ctiemp.eCodTipoEntidad=emple.eCodTipo ".
    " LEFT JOIN catentidades cent ON cent.eCodEntidad=asis.idEmpresa ".
    " WHERE asis.Estado='AC' AND asis.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asis.id) ".
    " AND asis.idEmpresa=".(int)$_POST['eCodEntidad'].
    " AND asis.idEmpleado NOT IN (SELECT assi.idEmpleado FROM movimientosafiliatorios mos INNER JOIN relasistenciamovimientos rassi ON ".
    " rassi.eCodMovimiento=mos.eCodMovimiento INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia WHERE assi.id=asis.id AND assi.idEmpresa=".(int)$_POST['eCodEntidad'].") ".
    " AND asis.Fecha Between '".$fechaantierC."' AND '".$fechaayerC."' AND asis.idCategorias NOT IN (33) ".((int)$_POST['eCodTipoEntidad']>0 ? " AND emple.eCodTipo=".(int)$_POST['eCodTipoEntidad'] : "")." AND asis.idEmpleado NOT IN (".$UsuariosInc.") ".
    "AND asis.idEmpleado IN (SELECT assi.idEmpleado FROM movimientosafiliatorios mos INNER JOIN relasistenciamovimientos rassi ON rassi.eCodMovimiento=mos.eCodMovimiento ".
    " INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia WHERE assi.idEmpleado=asis.idEmpleado AND mos.fhFecha='".$fechaantierC."' AND assi.idEmpresa=".(int)$_POST['eCodEntidad'].
    " AND mos.tCodTipoMovimiento NOT IN ('02') AND mos.eCodEstatus='AC') AND ".
" 
IFNULL((SELECT mosa.dImporte
FROM movimientosafiliatorios mosa
INNER JOIN relasistenciamovimientos rassia ON rassia.eCodMovimiento=mosa.eCodMovimiento
INNER JOIN asistencias assia ON assia.id=rassia.eCodAsistencia
WHERE assia.idEmpleado=asis.idEmpleado AND assia.idEmpresa=".(int)$_POST['eCodEntidad'].
" AND mosa.fhFecha='".$fechaantierC."' AND mosa.tCodTipoMovimiento NOT IN ('02') AND mosa.eCodEstatus='AC' LIMIT 1),0)
= 
CASE WHEN (SELECT MAX(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantierC."' AND '".$fechaayerC."'
AND asish.idEmpresa=".(int)$_POST['eCodEntidad']."
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)>".(float)$configura{'dTopeSalario'}." THEN ".(float)$configura{'dTopeSalario'}." ELSE ".
    " CASE WHEN (SELECT MAX(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantierC."' AND '".$fechaayerC."'
AND asish.idEmpresa=".(int)$_POST['eCodEntidad']."
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)=0 THEN FORMAT((".(float)$salarioMin{'Turno1'}."*ctiemp.dFactorIntegracion),2) ELSE ".
    " CASE WHEN (SELECT MAX(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantierC."' AND '".$fechaayerC."'
AND asish.idEmpresa=".(int)$_POST['eCodEntidad']."
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)<".$configura{'dSalarioMinimo'}." THEN ".$configura{'dSalarioMinimo'}." ELSE (SELECT MAX(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantierC."' AND '".$fechaayerC."'
AND asish.idEmpresa=".(int)$_POST['eCodEntidad']."
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado) END ".
    " END ".
    " END
UNION 
SELECT DISTINCT emple.id AS numeral, emple.IMSS AS imss, cent.tRegistroPatronal AS rp, emple.Paterno AS pat, Materno AS mat, Nombre AS nomb, ctiemp.eCodTipoJornada AS tipjo, '".$fechaayerC."' AS Fecha, emple.Clinica AS clin, emple.CURP AS curp, 
CASE WHEN (
SELECT mosa.dImporte
FROM movimientosafiliatorios mosa
INNER JOIN relasistenciamovimientos rassia ON rassia.eCodMovimiento=mosa.eCodMovimiento
INNER JOIN asistencias assia ON assia.id=rassia.eCodAsistencia
WHERE assia.idEmpleado=asis.idEmpleado AND mosa.fhFecha='".$fechaantierC."' AND assia.idEmpresa=".(int)$_POST['eCodEntidad'].
" AND mosa.tCodTipoMovimiento NOT IN ('02') AND mosa.eCodEstatus='AC' LIMIT 1)>".(float)$configura{'dTopeSalario'}." THEN ".(float)$configura{'dTopeSalario'}." ELSE (
SELECT mosa.dImporte
FROM movimientosafiliatorios mosa
INNER JOIN relasistenciamovimientos rassia ON rassia.eCodMovimiento=mosa.eCodMovimiento
INNER JOIN asistencias assia ON assia.id=rassia.eCodAsistencia
WHERE assia.idEmpleado=asis.idEmpleado AND mosa.fhFecha='".$fechaantierC."' AND assia.idEmpresa=".(int)$_POST['eCodEntidad'].
" AND mosa.tCodTipoMovimiento NOT IN ('02') AND mosa.eCodEstatus='AC' LIMIT 1) END AS tSalario
FROM asistencias asis
INNER JOIN empleados emple ON emple.id=asis.idEmpleado
INNER JOIN cattipoempleado ctiemp ON ctiemp.eCodTipoEntidad=emple.eCodTipo
LEFT JOIN catentidades cent ON cent.eCodEntidad=asis.idEmpresa
WHERE asis.Fecha Between '".$fechaantierC."' AND '".$fechaayerC."' 
AND asis.idEmpresa=".(int)$_POST['eCodEntidad'].
" AND asis.Estado='AC' 
AND asis.idEmpleado NOT IN (".$UsuariosInc.") 
AND asis.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asis.id) 
AND asis.idEmpleado NOT IN (SELECT assi.idEmpleado 
FROM movimientosafiliatorios mos 
INNER JOIN relasistenciamovimientos rassi ON rassi.eCodMovimiento=mos.eCodMovimiento 
INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia
WHERE assi.id=asis.id AND assi.idEmpresa=".(int)$_POST['eCodEntidad'].")
AND asis.idEmpleado IN (SELECT assi.idEmpleado 
FROM movimientosafiliatorios mos 
INNER JOIN relasistenciamovimientos rassi ON rassi.eCodMovimiento=mos.eCodMovimiento 
INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia
WHERE assi.idEmpleado=asis.idEmpleado AND mos.fhFecha='".$fechaantierC."' AND assi.idEmpresa=".(int)$_POST['eCodEntidad']." AND mos.tCodTipoMovimiento NOT IN ('02') AND mos.eCodEstatus='AC')
AND asis.idEmpleado IN (SELECT eCodEmpleado FROM ausentismo WHERE fhFecha='".$fechaayerC."' AND eCodEstatus='AC' AND eCodEmpleado=asis.idEmpleado)
AND asis.idCategorias NOT IN (33) ".((int)$_POST['eCodTipoEntidad']>0 ? " AND emple.eCodTipo=".$_POST['eCodTipoEntidad'] : "");
/*if($_POST['eAccion']==888){
    print $select;
}
$vContinua=$select;
    $continuaciones=mysql_query($select);
    $select=" SELECT * FROM sisconfiguracion ";
    $configura2=mysql_fetch_array(mysql_query($select));
    while($continua=mysql_fetch_array($continuaciones)){
        $dSalarioR=((float)$continua{'tSalario'}>(float)$configura2{'dTopeSalario'} ? (float)$configura2{'dTopeSalario'} : (float)$continua{'tSalario'});
        //print "Empleado: ".$idUsuario." val->".$dSalarioR."==".(float)$configura2{'dTopeSalarioMinimoDescanso'}."<br>";
        //if($dSalarioR==(float)$configura2{'dTopeSalarioMinimoDescanso'}){
            //" VALUES (".$idUsuario.", '".$fechaayerC."', '01', 'AC', ".(float)$rAsistencia{'tSalario'}.", 'NULL', CURRENT_TIMESTAMP) ";
            $insert=" INSERT INTO movimientosafiliatorios (eCodUsuario, fhFecha, tCodTipoMovimiento,  eCodEstatus, dImporte, tFolio, fhFechaRegistro) ".                        
                    " VALUES (".$idUsuario.", '".$fechaayerC."', '01', 'AC', ".$dSalarioR." , 'NA', CURRENT_TIMESTAMP) ";
            if($res=mysql_query($insert)){
                $exito=1;
                $select=" select last_insert_id() AS Llave ";
                $rCodigo=mysql_fetch_array(mysql_query($select));
                $eCodMovimiento=(int)$rCodigo['Llave'];

                $select=" SELECT asish.id ".
                        " FROM asistencias asish ".
                        " WHERE asish.Fecha Between '".$fechaantierC."' AND '".$fechaayerC."' ".
                        " AND asish.idEmpresa=".(int)$_POST['eCodEntidad'].
                        " AND asish.Estado='AC' ".
                        " AND asish.idEmpleado NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayerC."' AND FechaFinal>='".$fechaayerC."') ".
                        " AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) ".
                        " AND asish.idEmpleado NOT IN (SELECT assih.idEmpleado ".
                        "                              FROM movimientosafiliatorios mosh ".
                        "                              INNER JOIN relasistenciamovimientos rassih ON rassih.eCodMovimiento=mosh.eCodMovimiento ".
                        "                              INNER JOIN asistencias assih ON assih.id=rassih.eCodAsistencia ".
                        "                              WHERE assih.id=asish.id AND assih.idEmpresa=".(int)$_POST['eCodEntidad'].") ".
                        " AND asish.idEmpleado IN (SELECT assihh.idEmpleado ".
                        "                          FROM movimientosafiliatorios moshh ".
                        "                          INNER JOIN relasistenciamovimientos rassihh ON rassihh.eCodMovimiento=moshh.eCodMovimiento ".
                        "                          INNER JOIN asistencias assihh ON assihh.id=rassihh.eCodAsistencia ".
                        "                          WHERE assihh.idEmpleado=asish.idEmpleado AND moshh.fhFecha='".$fechaantierC."' ".
                        "                          AND assihh.idEmpresa=".(int)$_POST['eCodEntidad']." AND moshh.tCodTipoMovimiento NOT IN ('02') AND moshh.eCodEstatus='AC') ".
                        " AND asish.idEmpleado=".$continua{'numeral'};
                $asistenciasEmp=mysql_query($select);
                while($asisEmp=mysql_fetch_array($asistenciasEmp)){
                    $insertt=" INSERT INTO relasistenciamovimientos (eCodAsistencia, eCodMovimiento) ".
                            " VALUES (".$asisEmp{'id'}.", ".$eCodMovimiento.") ";
                    if($res=mysql_query($insertt)){
                        $exito=1;
                    }else{
                        $exito=0;
                    }
                }
            }else{
                $exito=0;
            }
        //}
    } */
    //////<--Fin Continuaciones
    
    $select=" SELECT * ".
            " FROM categorias ".
            " WHERE indice=11 ";
    $salarioMin=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

    $select=" SELECT assi.id, assi.idEmpleado ".
            " FROM movimientosafiliatorios mos ".
            " INNER JOIN relasistenciamovimientos rassi ON rassi.eCodMovimiento=mos.eCodMovimiento ".
            " INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia ".
            " INNER JOIN empleados emple ON emple.id=assi.idEmpleado ".
            " WHERE (assi.Fecha='".$fechaayer."' OR mos.fhFecha='".$fechaayer."') AND mos.eCodEstatus='AC' AND assi.idEmpresa=".(int)$_POST['eCodEntidad'].
            ((int)$_POST['eCodTipoEntidad']>0 ? " AND emple.eCodTipo=".(int)$_POST['eCodTipoEntidad'] : "");
    $afiliatorios=mysqli_query($link,$select);
    $noUsuario="0";
    while($afiliatorio=mysqli_fetch_array($afiliatorios,MYSQLI_ASSOC)){
        $noUsuario.=", ".$afiliatorio{'idEmpleado'};
    }
    //
    $select=" SELECT assi.id, assi.idEmpleado ".
            " FROM movimientosafiliatorios mos ".
            " INNER JOIN relasistenciamovimientos rassi ON rassi.eCodMovimiento=mos.eCodMovimiento ".
            " INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia ".
            " INNER JOIN empleados emple ON emple.id=assi.idEmpleado ".
            " WHERE assi.Fecha Between '".$fechaantier."' AND '".$fechaayer."' AND assi.idEmpresa=".(int)$_POST['eCodEntidad'].
            ((int)$_POST['eCodTipoEntidad']>0 ? " AND emple.eCodTipo=".(int)$_POST['eCodTipoEntidad'] : "");
    $afiliatorios=mysqli_query($link,$select);
    $UsuariosAfil="0";
    while($afiliatorio=mysqli_fetch_array($afiliatorios,MYSQLI_ASSOC)){
        $UsuariosAfil.=", ".$afiliatorio{'id'};
    }
    //
    $select=" SELECT assihh.idEmpleado ".
            " FROM movimientosafiliatorios moshh ".
            " INNER JOIN relasistenciamovimientos rassihh ON rassihh.eCodMovimiento=moshh.eCodMovimiento ".
            " INNER JOIN asistencias assihh ON assihh.id=rassihh.eCodAsistencia ".
            " INNER JOIN empleados emple ON emple.id=assihh.idEmpleado ".
            " WHERE moshh.fhFecha='".$fechaantier."' AND moshh.tCodTipoMovimiento NOT IN ('02') AND assihh.idEmpresa=".(int)$_POST['eCodEntidad'].
            " AND moshh.eCodEstatus='AC' ".((int)$_POST['eCodTipoEntidad']>0 ? " AND emple.eCodTipo=".(int)$_POST['eCodTipoEntidad'] : "");
    $movimientos=mysqli_query($link,$select);
    $UsiariosMovi="0";
    while($movimiento=mysqli_fetch_array($movimientos,MYSQLI_ASSOC)){
        $UsiariosMovi.=", ".$movimiento{'idEmpleado'};
    }

   $select=" SELECT DISTINCT emple.id AS numeral, emple.IMSS AS imss, cent.tRegistroPatronal AS rp, emple.Paterno AS pat, Materno AS mat, Nombre AS nomb, ctj.tNombre AS tipjo, '".$fechaayer."' AS Fecha, emple.Clinica AS clin, emple.CURP AS curp, ctiemp.tNombreCorto AS TipoTrabajador, cts.tNombre AS TipoSalario, 
    CASE WHEN (SELECT MAX(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.idEmpresa=".(int)$_POST['eCodEntidad']."
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."') 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)>".(float)$configura{'dTopeSalario'}." THEN ".(float)$configura{'dTopeSalario'}." ELSE 
    CASE WHEN (SELECT MAX(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.idEmpresa=".(int)$_POST['eCodEntidad']."
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."') 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)=0 THEN FORMAT((".(float)$salarioMin{'Turno1'}."*ctiemp.dFactorIntegracion),2) ELSE ".
    " CASE WHEN (SELECT MAX(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.idEmpresa=".(int)$_POST['eCodEntidad']."
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."') 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)<".$configura{'dSalarioMinimo'}." THEN ".$configura{'dSalarioMinimo'}." ELSE (SELECT MAX(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.idEmpresa=".(int)$_POST['eCodEntidad']."
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."') 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado) END ".
    " END
    END AS Salario
FROM asistencias asis
INNER JOIN empleados emple ON emple.id=asis.idEmpleado
INNER JOIN cattipoempleado ctiemp ON ctiemp.eCodTipoEntidad=emple.eCodTipo
LEFT JOIN catentidades cent ON cent.eCodEntidad=asis.idEmpresa
LEFT JOIN cattiposalario cts ON cts.eCodSalario=ctiemp.eCodTipoSalario
LEFT JOIN cattipojornada ctj ON ctj.eCodJornada=ctiemp.eCodTipoJornada
WHERE asis.Fecha Between '".$fechaantier."' AND '".$fechaayer."' AND asis.idEmpresa=".(int)$_POST['eCodEntidad'].
((int)$_POST['eCodTipoEntidad']>0 ? " AND emple.eCodTipo=".(int)$_POST['eCodTipoEntidad'] : "")."
AND asis.Estado='AC' 
AND asis.idEmpleado NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."') 
AND asis.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asis.id) 
AND asis.id NOT IN (".$UsuariosAfil.") 
AND asis.idEmpleado NOT IN (".$noUsuario.")
AND asis.idEmpleado IN (SELECT assi.idEmpleado 
FROM movimientosafiliatorios mos 
INNER JOIN relasistenciamovimientos rassi ON rassi.eCodMovimiento=mos.eCodMovimiento 
INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia
WHERE assi.idEmpleado=asis.idEmpleado AND mos.fhFecha='".$fechaantier."' AND assi.idEmpresa=".(int)$_POST['eCodEntidad'].
" AND mos.tCodTipoMovimiento NOT IN ('02') AND mos.eCodEstatus='AC')
AND 
IFNULL((SELECT mosa.dImporte
FROM movimientosafiliatorios mosa
INNER JOIN relasistenciamovimientos rassia ON rassia.eCodMovimiento=mosa.eCodMovimiento
INNER JOIN asistencias assia ON assia.id=rassia.eCodAsistencia
WHERE assia.idEmpleado=asis.idEmpleado AND mosa.fhFecha='".$fechaantier."' AND assia.idEmpresa=".(int)$_POST['eCodEntidad']." AND mosa.tCodTipoMovimiento NOT IN ('02') AND mosa.eCodEstatus='AC' LIMIT 1),0)
<>
CASE WHEN (
SELECT MAX(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.idEmpresa=".(int)$_POST['eCodEntidad']."
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."') 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)>".(float)$configura{'dTopeSalario'}." THEN ".(float)$configura{'dTopeSalario'}." ELSE 
CASE WHEN (SELECT MAX(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.idEmpresa=".(int)$_POST['eCodEntidad']."
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."') 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)=0 THEN FORMAT((".(float)$salarioMin{'Turno1'}."*ctiemp.dFactorIntegracion),2) ELSE ".
    " CASE WHEN (SELECT MAX(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.idEmpresa=".(int)$_POST['eCodEntidad']."
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."') 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)<".$configura{'dSalarioMinimo'}." THEN ".$configura{'dSalarioMinimo'}." ELSE (SELECT MAX(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.idEmpresa=".(int)$_POST['eCodEntidad']."
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."') 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado) END ".
    " END
END ";
if($_POST['eAccion']==999){
    print $select;
}
$rsServicios=mysqli_query($link,$select);
$cont=0;
$registros=(int)mysqli_num_rows($rsServicios);
?>
<table cellspacing="0" border="0" width="980px">
  <tr>
    <td width="50%"><hr color="#666666" /></td>
    <td class="sanLR04" nowrap="nowrap"><b>Registros Encontrados: ( <?=$registros;?> )</b></td>
    <td width="50%"><hr color="#666666" /></td>
  </tr>
</table>
<div style="display:block; top:0; left:0; width:980px; z-index=1; overflow: auto;">
    <table cellspacing="0" border="0" width="980px">
        <thead>
            <tr class="thEncabezado">	      	
                <td nowrap="nowrap" class="sanLR04" height="20">R. Patronal</td>
                <td nowrap="nowrap" class="sanLR04">NSS</td>
                <td nowrap="nowrap" class="sanLR04">C. Empleado</td>
                <td nowrap="nowrap" class="sanLR04">Paterno</td> 
                <td nowrap="nowrap" class="sanLR04">Materno</td>
                <td nowrap="nowrap" class="sanLR04">Nombre(s)</td>
                <td nowrap="nowrap" class="sanLR04">S.D.I.</td>
                <td nowrap="nowrap" class="sanLR04">S. Infornavit</td>
                <td nowrap="nowrap" class="sanLR04">T. Trabajador</td>
                <td nowrap="nowrap" class="sanLR04">T. Salario</td>
                <td nowrap="nowrap" class="sanLR04">Semana</td>
                <td nowrap="nowrap" class="sanLR04">F. Movimiento</td>
                <td nowrap="nowrap" class="sanLR04">U.M.F</td>
                <td nowrap="nowrap" class="sanLR04">Movimiento</td>
                <td nowrap="nowrap" class="sanLR04">Guia</td>
                <td nowrap="nowrap" class="sanLR04">C. Trabajador</td>
                <td nowrap="nowrap" class="sanLR04">Filler</td>
                <td nowrap="nowrap" class="sanLR04">CURP</td>      
                <td nowrap="nowrap" class="sanLR04" width="100%">Identificador</td>
            </tr>   
        </thead>
        <tbody>
        	<?php $i=1; 
			while($rServicio=mysqli_fetch_array($rsServicios,MYSQLI_ASSOC)){
				$salario=explode(".",str_replace(",", "", number_format($rServicio["Salario"], 2)));
				if(strlen($salario[0])<=3)
					$cadena="0".$salario[0].$salario[1];
				else
					$cadena=$salario[0].$salario[1];
				$fech=$fechaayer; 
				$fecha=explode("-",$fech); 
				$abc=$fecha[2].$fecha[1].$fecha[0]; ?>
                <tr>        
                    <td nowrap="nowrap" class="sanLR04 columnB"><b><?=$rServicio{'rp'};?></b></td>                              
                    <td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rServicio["imss"]); ?></td>
                    <td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rServicio["numeral"]); ?></td>
                    <td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rServicio["pat"]); ?></td>
                    <td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rServicio["mat"]); ?></td>
                    <td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rServicio["nomb"]); ?></td>
                    <td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($cadena); ?></td>
                    <td nowrap="nowrap" class="sanLR04">000000</td>
                    <td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rServicio["TipoTrabajador"]);?></td>
                    <td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rServicio["TipoSalario"]);?></td>
                    <td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rServicio["tipjo"]); ?></td>
                    <td nowrap="nowrap" class="sanLR04"><?=utf8_encode($abc); ?></td>
                    <td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rServicio["clin"]); ?></td>
                    <td nowrap="nowrap" class="sanLR04">(07) Modificación</td>
                    <td nowrap="nowrap" class="sanLR04 columnB">03400</td>
                    <td nowrap="nowrap" class="sanLR04">ESTIBADOR</td>
                    <td nowrap="nowrap" class="sanLR04 columnB"></td>
                    <td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rServicio["curp"]); ?></td>
                    <td nowrap="nowrap" class="sanLR04 columnB" width="100%">9</td>
                </tr>
				<?php $i++; 
			} ?>
        </tbody>
    </table>
</div>
<?php }else{ 
$select=" SELECT * ".
		" FROM cattipoempleado ".
		" WHERE tCodEstatus='AC' ".
		" ORDER BY tNombreCorto ";
$rsTiposEmpleados=mysqli_query($link,$select);

$select=" SELECT * ".
        " FROM catentidades ".
        " WHERE tCodEstatus='AC' ".
        " ORDER BY tSiglas ";
$rsEntidades=mysqli_query($link,$select); ?>
<script type="text/javascript">
function generarBatch(){
	var UrlBatch = "php/batch/2.5.4.5.php?f=1"+
    (dojo.byId('fecha').value           ? "&fecha="+dojo.byId('fecha').value : "")+
    (dojo.byId('eAccion').value         ? "&eAccion="+dojo.byId('eAccion').value : "")+
    (dojo.byId('eCodEntidad').value     ? "&eCodEntidad="+dojo.byId('eCodEntidad').value : "")+
    (dojo.byId('eCodTipoEntidad').value ? "&eCodTipoEntidad="+dojo.byId('eCodTipoEntidad').value : "");
    var Datos = document.createElement("FORM");
    document.body.appendChild(Datos);
    Datos.name='Datos';
    Datos.method = "POST";
    Datos.action = UrlBatch;
    Datos.submit();
    filtrarConsulta();
}

dojo.addOnLoad(function(){
    filtrarConsulta();
});
</script>
<form name="Datos" id="Datos" method="post" action="" onsubmit="return false;">
    <input type="hidden" name="eUsuario" id="eUsuario" value="<?=(int)$_SESSION['sesionUsuario']['eCodUsuario'];?>" />
    <input type="hidden" value="0" name="ePagina" id="ePagina" />
    <input type="hidden" value="" name="eAccion" id="eAccion" />
    <table width="980px" border="0">
        <tr>
            <td colpan="3" width="100%"></td>
            <td align="right" nowrap="nowrap"><a class="fntBR11" href="javaScript:mostrarFiltros('Busqueda');">Busqueda de Registros</a></td>
        </tr>
        <tr id="trBusqueda" style="display:none">
        	<td colspan="4"><table width="980px" bgcolor="#f9f9f9">
                <tr>
                	<td class="sanLR04" height="5"></td>
                </tr>
                <tr>
                	<td class="sanLR04" height="20">Fecha</td>
                	<td class="sanLR04" width="50%"><input type="date" name="fecha" dojoType="dijit.form.TextBox" id="fecha" value="<?=$_POST["fecha"] ? $_POST["fecha"] : date("Y-m-d")?>" style="width:140px"></td>
                	<td class="sanLR04" height="20" nowrap>Tipo de Trabajador</td>
                	<td class="sanLR04" width="50%">
                        <select name="eCodTipoEntidad" id="eCodTipoEntidad" style="width:180px; height:25px;">
                            <option value="">Seleccione...</option>
							<?php while($rTipoEmpleado=mysqli_fetch_array($rsTiposEmpleados,MYSQLI_ASSOC)){ ?>
                                <option value="<?=$rTipoEmpleado{'eCodTipoEntidad'}?>"><?=utf8_encode($rTipoEmpleado{'tNombreCorto'})?></option>
                            <?php } ?>
                        </select>
	                </td>
                </tr>
                <tr>
                    <td class="sanLR04" height="20" nowrap>Entidad</td>
                    <td class="sanLR04" width="50%">
                        <select name="eCodEntidad" id="eCodEntidad" style="width:180px; height:25px;">
                            <option value="">Seleccione...</option>
                            <?php while($rEntidad=mysqli_fetch_array($rsEntidades,MYSQLI_ASSOC)){ ?>
                                <option value="<?=$rEntidad{'eCodEntidad'}?>" <?=($rEntidad{'bPrincipal'}==1 ? "selected" : "");?> ><?=utf8_encode($rEntidad{'tSiglas'})?></option>
                            <?php } ?>
                        </select>
                    </td>
                    <td class="sanLR04" height="20"></td>
                    <td class="sanLR04" width="50%"></td>
                </tr>
                <tr>
    	            <td class="sanLR04" height="5"></td>
                </tr>
        	</table></td>
        </tr>
        <tr>
        	<td colspan="4"><div id="dvCNS"></div></td>
        </tr>
    </table>
</form>
<?php } ?>