<?php
require_once("conexion/soluciones-mysql.php");
$link = getLink();
if($_POST){
	if ($_POST['fhFechaIncapacidadInicio']!=""){
  		$fhFechaIncapacidadInicio = $_POST['fhFechaIncapacidadInicio'].' 00:00:00';
  		$fhFechaIncapacidadFin = ($_POST['fhFechaIncapacidadFin']!="" ? $_POST['fhFechaIncapacidadFin'] : $_POST['fhFechaIncapacidadInicio']).' 23:59:59';
 	}
	$select=" SELECT inc.*, emp.Empleado AS Empleado, CONCAT_WS(' ', usr.tNombre, usr.tApellidos) AS Usuario, 
			CONCAT_WS(' ', usrc.tNombre, usrc.tApellidos) AS UCancelacion, CURRENT_DATE AS Hoy
			FROM incapacidades inc
			LEFT JOIN empleados emp ON emp.id=inc.idEmpleado
			INNER JOIN catusuarios usr ON usr.eCodUsuario=inc.idUsuario
			LEFT JOIN catusuarios usrc ON usrc.eCodUsuario=inc.eCodUsuarioCancelacion
			WHERE 1=1 ".
			($_POST['eCodIncapacidad'] 			? " AND inc.id=".$_POST['eCodIncapacidad'] 				: "").			
			($_POST['Empleado'] 				? " AND emp.Empleado like '%".$_POST['Empleado']."%'" 	: "").
			($_POST['fhFechaIncapacidadInicio']	? " AND inc.FechaInicial >='".$fhFechaIncapacidadInicio. "' AND inc.FechaInicial<='" .$fhFechaIncapacidadFin."' OR inc.FechaFinal <= '".$fhFechaIncapacidadInicio."' AND inc.FechaFinal >='".$fhFechaIncapacidadFin."'" : "" ).						
			($_POST['EcodEstatus'] 				? " AND inc.Estado='".$_POST['EcodEstatus']."'"			: "").			
			($_POST['folio'] 					? " AND inc.Folio like '%".$_POST['folio']."%'" 	: "").
			($_POST['chXVencer'] 				? " AND inc.FechaFinal=CURRENT_DATE AND inc.Estado='AC' " 	: "").
			($_POST['chVigentes'] 				? " AND inc.FechaInicial<=CURRENT_DATE AND inc.FechaFinal>=CURRENT_DATE AND inc.Estado='AC' " 	: "").
			" Order by inc.id ".($_POST['orden'] ? $_POST['orden']  : "DESC" )." LIMIT ".( $_POST['limite'] ? $_POST['limite']  : "1000" );
	$rsSolicitudes=mysqli_query($link,$select);
	$rsUsuarios=mysqli_query($link,$select);
	$registros=(int)mysqli_num_rows($link,$rsUsuarios); ?>
    <table cellspacing="0" border="0" width="965px">
		<tr>
			<td width="50%"><hr color="#666666" /></td>
			<td class="sanLR04" nowrap="nowrap"><input type="hidden" value="<?=$_POST['orden'];?>" name="tOrden" id="tOrden" /><b>Registros Encontrados: ( <?=$registros;?> )</b></td>
			<td width="50%"><hr color="#666666" /></td>
		</tr>
    </table>
	<div style="display:block; top:0; left:0; width:965px; z-index=1; overflow: auto; height:400px;">
	<table cellspacing="0" border="0" width="965px">
		<thead>
			<tr class="thEncabezado">
				<td nowrap="nowrap" class="sanLR04" height="20" align="center">E</td>
				<td nowrap="nowrap" class="sanLR04" height="20" align="center" title="Previa">P</td>
				<td nowrap="nowrap" class="sanLR04">C&oacute;digo</td>
				<td nowrap="nowrap" class="sanLR04">Empleado</td>
				<td nowrap="nowrap" class="sanLR04">F. Inicia</td>
				<td nowrap="nowrap" class="sanLR04">F. Finaliza</td>
				<td nowrap="nowrap" class="sanLR04">Folio</td>
				<td nowrap="nowrap" class="sanLR04">Usuario</td>
				<td nowrap="nowrap" class="sanLR04">F. Registro</td>
				<td nowrap="nowrap" class="sanLR04">Usuario Cancelaci&oacute;n</td>
				<td nowrap="nowrap" class="sanLR04">F. Cancelaci&oacute;n</td>
				<td nowrap="nowrap" class="sanLR04" width="100%">Motivo de Cancelaci&oacute;n</td>
			</tr>
		</thead>
		<tbody>
			<?php $i=1;
			while($rSolicitud=mysqli_fetch_array($rsSolicitudes, MYSQLI_ASSOC)){ ?>
				<tr>
					<td nowrap="nowrap" class="sanLR04" height="20" align="center"><img width="16" height="16" alt="Empresa" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-<?=$rSolicitud{'Estado'};?>.png"></td>
					<td nowrap="nowrap" class="sanLR04"><?=($rSolicitud{'Hoy'}==$rSolicitud{'FechaFinal'} && $rSolicitud{'Estado'}=='AC' ? "<a href=\"?ePagina=2.4.2.1.php&ePrevia=".$rSolicitud{'idEmpleado'}."\"><b>[+]</b></a>" : "");?></td>
					<td nowrap="nowrap" class="sanLR04 colmenu"><a href="?ePagina=2.4.2.1.php&eCodIncapacidad=<?=$rSolicitud{'id'};?>"><b><?=sprintf("%07d",$rSolicitud{'id'});?></b></a></td>
					<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rSolicitud{'Empleado'});?></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=date("d/m/Y", strtotime($rSolicitud{'FechaInicial'}));?></td>
					<td nowrap="nowrap" class="sanLR04"><?=date("d/m/Y", strtotime($rSolicitud{'FechaFinal'}));?></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rSolicitud{'Folio'});?></td>
					<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rSolicitud{'Usuario'});?></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=date("d/m/Y H:i", strtotime($rSolicitud{'fechaRegistro'}));?></td>
					<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rSolicitud{'UCancelacion'});?></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=($rSolicitud{'fhFechaCancelacion'} ? date("d/m/Y H:i", strtotime($rSolicitud{'fhFechaCancelacion'})) : "");?></td>
					<td nowrap="nowrap" class="sanLR04" width="100%"><?=utf8_encode($rSolicitud{'tMotivoCancelacion'});?></td>
				</tr>
				<?php $i++; 
			} ?>
		</tbody>
	</table>
	</div>
<?php }else{ 
$select=" SELECT * ".
		" FROM categorias ".
		" WHERE tCodEstatus='AC' ";
$rsTiposCategorias=mysqli_query($link,$select); ?>
<script type="text/javascript">
dojo.require("dojo.number");
dojo.require("dijit.form.NumberTextBox");
dojo.require("dijit.form.DateTextBox");
dojo.require("dojo.io.iframe");

function nuevo(){
	document.location = "?ePagina=2.4.2.1.php";
}

function generaExcel(){
	var UrlExcel = "php/excel/2.5.2.1.php?"+
	(dojo.byId('eCodIncapacidad').value ? "&eCodIncapacidad="+dojo.byId('eCodIncapacidad').value : "")+
	(dojo.byId('Empleado').value ? "&Empleado="+dojo.byId('Empleado').value : "")+
	(dojo.byId('fhFechaIncapacidadInicio').value ? "&fhFechaIncapacidadInicio="+dojo.byId('fhFechaIncapacidadInicio').value : "")+
	(dojo.byId('EcodEstatus').value ? "&EcodEstatus="+dojo.byId('EcodEstatus').value : "")+
	(dojo.byId('folio').value ? "&folio="+dojo.byId('folio').value : "")+
	(dojo.byId('chXVencer').checked==true ? "&chXVencer=1 " : "")+		
	(dojo.byId('chVigentes').checked==true ? "&chVigentes=1 " : "")+
	(dojo.byId('tOrden').value ? "&orden="+dojo.byId('tOrden').value : "")+
	(dojo.byId('limite').value ? "&limite="+dojo.byId('limite').value : "");		
	var Datos = document.createElement("FORM");
	document.body.appendChild(Datos);
	Datos.name='Datos';
	Datos.method = "POST";
	Datos.action = UrlExcel;
	Datos.submit();
}

dojo.addOnLoad(function(){filtrarConsulta();});
</script>
	<form name="Datos" id="Datos" method="post" action="" onsubmit="return false;">
		<input type="hidden" value="0" name="ePagina" id="ePagina" />
		<input type="hidden" value="" name="eAccion" id="eAccion" />
		<input type="hidden" value="<?=(int)$_SESSION['sesionUsuario']['eCodEntidad'];?>" name="eEntidad" id="eEntidad" />
		<table width="965px" border="0">
			<tr>
				<td colpan="3" width="100%"></td>
				<td align="right" nowrap="nowrap"><a class="fntBR11" href="javaScript:mostrarFiltros('Busqueda');">Busqueda de Registros</a></td>
			</tr>
			<tr id="trBusqueda" style="display:none">
				<td colspan="4">
					<table width="100%"  bgcolor="#f9f9f9">
						<tr><td class="sanLR04" height="5"></td></tr>
						<tr>
							<td class="sanLR04" height="20">C&oacute;digo</td>
							<td class="sanLR04" width="40%"><input type="text" name="eCodIncapacidad" dojoType="dijit.form.TextBox" id="eCodIncapacidad" value="<?=$_GET['eCodIncapacidad'];?>" style="width:80px; height:25px;"></td>
							<td class="sanLR04" nowrap="nowrap">Fecha de Incapacidad</td>
							<td class="sanLR04" width="50%">
							<input name="fhFechaIncapacidadInicio" id="fhFechaIncapacidadInicio" type="date"  required="false"  /> -
							<input name="fhFechaIncapacidadFin" id="fhFechaIncapacidadFin" type="date"  required="false"  />
							</td>
						</tr>						
						<tr>
							<td class="sanLR04" height="20">Empleado</td>
							<td class="sanLR04" width="50%"><input type="text" name="Empleado" dojoType="dijit.form.TextBox" id="Empleado" value="" style="width:180px; height:25px;"></td>
							<td class="sanLR04" height="20">Folio</td>
							<td class="sanLR04" width="50%"><input type="text" name="folio" dojoType="dijit.form.TextBox" id="folio" value="" style="width:142px; height:25px;"></td>
						</tr>						
						<tr>
							<td class="sanLR04" height="20">Estatus</td>
							<td class="sanLR04" width="50%"><select name="EcodEstatus" id="EcodEstatus" style="width:142px; height:25px;">
									<option value="">Seleccione...</option>
									<option value="AC">Activo</option>
									<option value="CA">Cancelado</option>									
								</select></td>
							<td class="sanLR04" height="20" colspan="2"><label><input type="checkbox" name="chXVencer" id="chXVencer" value="1">X vencer</label></td>
							
						</tr>
						<tr>
							<td class="sanLR04" height="20" colspan="2"><label><input type="checkbox" name="chVigentes" id="chVigentes" value="1">Vigentes</label></td>
							<td class="sanLR04" height="20" colspan="2"><label></td>
							
						</tr>
						<tr>
			              <td nowrap="nowrap" class="sanLR04" height="5"><b>[Ordenamiento]</b></td>
			              <td nowrap="nowrap" class="sanLR04" width="50%"><label><input type="radio" name="orden" id="orden"  value="ASC" > ASCENDENTE</label> / <label><input type="radio" name="orden" id="orden" checked="checked" value="DESC" > DESENDENTE</label></td>
			              <td nowrap="nowrap" class="sanLR04"><b>[Maximos Registros]:</b></td>
			              <td nowrap="nowrap" class="sanLR04" width="50%">
			              	<select name="limite" id="limite" style="width:142px; height:25px;">
                                  <option value="100">100</option>
                                  <option value="1000" selected>1,000</option>
                                  <option value="10000">10,000</option>
                                  <option value="100000">100,000</option>
                          	</select>
                          </td>
            			</tr>
						<tr><td class="sanLR04" height="5"></td></tr>
					</table>
				</td>
			</tr>
			<tr><td colspan="4"><div id="dvCNS"></div></td></tr>
		</table>
	</form>
<?php } ?>