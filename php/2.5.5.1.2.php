<?php require_once("conexion/soluciones-mysql.php");
require_once("conexion/sistema.php");
$link = getLink();
if($_POST){
	if($_POST['eProceso']==1){
		$eCodEIR		= ($_POST['eCodEIR']		? (int)$_POST['eCodEIR']		: "NULL");
		$eCodUsuario	= ($_POST['eCodUsuario']	? (int)$_POST['eCodUsuario']	: "NULL");

		$update=" UPDATE biteirs ".
				" SET	eCodUsuarioAutorizacion=".$eCodUsuario.",".
				"		fhFechaAutorizacion=CURRENT_TIMESTAMP ".
				" WHERE eCodEIR=".(int)$eCodEIR;
		if($res=mysqli_query($link,$update)){
			$exito=1;
		}else{
			$exito=0;
		}
		print "<input type=\"text\" value=\"".($exito==1 ? $eCodEIR : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}
//
}else{
$pagina = new sistema();
$select=" SELECT oss.eCodEntrada, oss.fhFechaEntrada, bei.eCodEIR, oss.tCodContenedor, ".
		" ctc.tNombreCorto AS TipoContenedor, cu.tNombre, cu.tApellidos, bei.fhFechaAutorizacion, ".
		" bei.eCodUsuarioAutorizacion ".
		" FROM biteirs bei ".
		" INNER JOIN opeentradasmercancias oss ON oss.eCodEntrada=bei.eCodEntrada ".
		" LEFT JOIN cattiposcontenedores ctc ON ctc.eCodTipoContenedor=oss.eCodTipoContenedor ".
		" LEFT JOIN catusuarios cu ON cu.eCodUsuario=bei.eCodUsuarioAutorizacion ".
		" WHERE bei.eCodEIR=".(int)$_GET['eCodEIR'];
$rSolicitud=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC); ?>
<script type="text/javascript">
function autorizar(){
	dojo.byId('eProceso').value=1;
	var bandera = false;
	if (dojo.byId("eCodEIRAutoriza").value){
		var mensaje="* EIR Autorizado\n";
		bandera = true;		
	}
	if (bandera==true){
		alert(mensaje);		
	}else{
		if (confirm("¿Desea Autorizar el EIR?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				if(dojo.byId("eCodigo").value>0){
					alert("¡La información se guardó correctamente!");
					tURL = './?ePagina=2.5.5.1.2.php&eCodEIR='+dojo.byId("eCodigo").value;	
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
		}
	}
}
function consultar(){
	document.location = './?ePagina=2.5.5.1.php';
}
function generarPDF(){
	var urlPDF ='php/pdf/2.5.5.1.2.php?eCodEIR='+<?=$rSolicitud{'eCodEIR'}?>;
	var submitForm =document.createElement("FORM");
	document.body.appendChild(submitForm);
	submitForm.method="POST";
	submitForm.action=urlPDF;
	submitForm.target="_blank";
	submitForm.submit();	 
}
</script>
<div id="dvCNS" style="display:none;"></div>
<form action="" method="post" name="Datos" id="Datos" onsubmit="return false;">
<input type="hidden" name="eCodEIR" id="eCodEIR" value="<?=$rSolicitud{'eCodEIR'};?>" />
<input type="hidden" name="eCodEIRAutoriza" id="eCodEIRAutoriza" value="<?=$rSolicitud{'eCodUsuarioAutorizacion'};?>" />
<input type="hidden" name="eProceso" id="eProceso" value="" />
<input type="hidden" name="eCodUsuario" id="eCodUsuario" value="<?=(int)$_SESSION['sesionUsuario']['eCodUsuario'];?>" />
<table border="0" cellpadding="0" cellspacing="0" width="965px">
    <tr><td height="20"></td></tr>
	<tr>		
	    <td height="23" nowrap class="sanLR04"> C&oacute;digo</td>
	    <td width="50%" nowrap class="sanLR04"><?=sprintf("%07d",$rSolicitud{'eCodEIR'});?></td>
		<td nowrap class="sanLR04"></td>
	    <td width="50%" nowrap class="sanLR04"></td>
    </tr>
    <?php if((int)$rSolicitud{'eCodUsuarioAutorizacion'}>0){ ?>
        <tr>
            <td height="23" nowrap class="sanLR04"> Usuario Autorizaci&oacute;n</td>
            <td nowrap class="sanLR04"><?=($rSolicitud{'tNombre'}." ".$rSolicitud{'tApellidos'});?></td>
            <td height="23" nowrap class="sanLR04"> Fecha de Autorizaci&oacute;n</td>
            <td nowrap class="sanLR04"><?=date("d/m/Y", strtotime($rSolicitud{'fhFechaAutorizacion'}));?></td>
        </tr>
	<?php } ?>
	<tr>
		<td height="23" nowrap class="sanLR04"> Entrada</td>
	    <td nowrap class="sanLR04"><?=sprintf("%07d",$rSolicitud{'eCodEntrada'});?></td>
		<td height="23" nowrap class="sanLR04"> Fecha de Entrada</td>
	    <td nowrap class="sanLR04"><?=date("d/m/Y", strtotime($rSolicitud{'fhFechaEntrada'}));?></td>
    </tr>
		<tr>
		<td height="23" nowrap class="sanLR04"> Contenedor</td>
	    <td nowrap class="sanLR04"><?=($rSolicitud{'tCodContenedor'});?></td>
		<td height="23" nowrap class="sanLR04"> Tipo</td>
	    <td nowrap class="sanLR04"><?=($rSolicitud{'TipoContenedor'});?></td>
    </tr>
	</tr>
		<tr>
		<td height="10" nowrap class="sanLR04"> </td>
	    <td nowrap class="sanLR04" colspan="3"></td>
    </tr>
	<tr>
		<td height="20" valign="top" nowrap="nowrap" class="sanLR04">EIR</td>
        <td valign="top"><table border="0" cellspacing="0" cellpadding="0" class="tbConsulta">
			<?php $i=0; 
			$select=" SELECT * ".
					" FROM catvistascontenedores ".
					" WHERE eCodVista IN (1,2,3) ";
			$rsVistas = mysqli_query($link,$select);
            while($rVista=mysqli_fetch_array($rsVistas,MYSQLI_ASSOC)){
				$select=" SELECT rvc.*, cc.tNombre AS Cara ".
						" FROM relvistascontenedorescarascontenedores rvc ".
						" INNER JOIN catcarascontenedores cc ON cc.eCodCara = rvc.eCodCara ".
						" WHERE rvc.eCodVista=".$rVista{'eCodVista'};
				$rsCaras = mysqli_query($link,$select);
				$ePix = 10;
				$tImagen = $pagina->imagenDanios(array('eir'=>$rSolicitud{'eCodEIR'},'vista'=>$rVista{'eCodVista'},'pix'=>$ePix)); 
				if($i){ ?>
					<tr>
						<td height="40" colspan="3" align="center"></td>
					</tr>
				<?php } ?>
				<tr class="thEncabezado">
					<td height="20" colspan="3" align="center"><?=($rVista{'tNombre'});?></td>
				</tr>
				<tr>
					<td ><span class="fntA11S">
						<?=$rVista{'tCostadoI'};?>
					</span></td>
					<td align="center" ><span class="fntA11S">
						<?=$rVista{'tCostadoT'};?>
					</span></td>
					<td height="20" align="right" >&nbsp;</td>
				</tr>
				<tr>
					<td height="20" colspan="3" align="center"><div id="div<?=$rVista{'eCodVista'};?>" style="width:<?=$rVista{'eAncho'}+($ePix*2);?>px;height:<?=$rVista{'eAlto'}+($ePix*2);?>px;">
					<img src="data:image/jpeg;base64,<?=base64_encode($tImagen['contenido']);?>" width="<?=$rVista{'eAncho'}+($ePix*2);?>" height="<?=$rVista{'eAlto'}+($ePix*2);?>" id="img<?=$rVista{'eCodVista'};?>" usemap="#mapa<?=$rVista{'eCodVista'};?>" border="0" />
					</div></td>
				</tr>
				<tr>
					<td align="center">&nbsp;</td>
					<td align="center"><span class="fntA11S">
						<?=$rVista{'tCostadoB'};?>
					</span></td>
					<td height="20" align="right"><span class="fntA11S">
						<?=$rVista{'tCostadoD'};?>
					</span></td>
				</tr>
				<?php $i++;
            }
			$pagina->eliminarImagenes(); ?>
    	</table></td>
        <td valign="top" nowrap="nowrap" class="sanLR04">Da&ntilde;os Reportados</td>
        <td valign="top" class="sanLR04">
        	<table border="0" cellspacing="0" cellpadding="0">
        	<?php 
			$select=" SELECT re.*, cv.tNombre AS Vista, cc.tNombre AS Cara, cd.tCodDanio, cd.tNombre AS Danio ".
					" FROM releirsdanios re ".
					" LEFT JOIN catvistascontenedores cv ON re.eCodVista=cv.eCodVista ".
					" LEFT JOIN catcarascontenedores cc ON re.eCodCara=cc.eCodCara ".
					" LEFT JOIN catdanioscontenedores cd ON re.eCodDanio=cd.eCodDanio ".
					" WHERE re.eCodEIR=".$rSolicitud{'eCodEIR'};
			$rsDanosReportados = mysqli_query($link,$select);
			if(mysqli_num_rows($rsDanosReportados)>0){
				while($rDanoReportado=mysqli_fetch_array($rsDanosReportados,MYSQLI_ASSOC)){ ?>
                <tr>
                	<td height="20" class="fntG10S"><?="(".(trim($rDanoReportado{'Vista'})).")";?></td>
                    <td class="sanLR04 fntG10S"><?="(".(trim($rDanoReportado{'Cara'})).")";?></td>
                    <td class="sanLR04 fntR11S"><?="(".trim($rDanoReportado{'tCodDanio'}).")";?> </td>
                    <td class="sanLR04 fntR11S"><?=$rDanoReportado{'Danio'};?></td>
				</tr>
				<?php }
			}else{
				print "Ninguno";
			} ?>
            </table>
        </td>
    </tr>
</table>
</form>
<? } ?>