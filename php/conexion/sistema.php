<?php
require_once("soluciones-mysql.php");

class sistema{
	var $ePagina;	
	public function sistema($ePagina){
		global $cxsis, $bdsis;
		$this->iniciarSistema($ePagina ? $ePagina : $this->ePagina);
	}

	public function redirigirInicio(){
		if(!$_GET['ePagina']){
			unset($_SESSION['sesionUsuario']);
			session_destroy();
			$this->redireccionar("?ePagina=1.1.php");
		}
	}

	public function redireccionar($url){
		header('Location: '.$url);
	}

	public function mostrarContenido($eUsuarioSeccion){
		if((int)$eUsuarioSeccion>=2 && !isset($_SESSION['sesionUsuario'])){
			unset($_SESSION['sesionUsuario']);
			session_destroy();
			$this->redireccionar("?ePagina=1.1.php");
		}else{
			$pagina = new sistema($this->ePagina);
			if(file_exists('php/'.$this->ePagina)){
				include('php/'.$this->ePagina);
			}else{
				unset($_SESSION['sesionUsuario']);
				session_destroy();
				$this->redireccionar("?ePagina=1.1.php");
			}
		}
    }

	private function iniciarSistema($ePagina){
		$this->tSeccion = $ePagina;
		if(!$ePagina || (!empty($eUsuarioSeccion) && (int)$eUsuarioSeccion>=2 && !isset($_SESSION['sesionUsuario']))){
			$ePagina='1.1.php';
		}
		$this->ePagina =$ePagina;
	}
	
	public function ingresos($datos){
		$link = getLink();
		if(isset($_SESSION['sesionServicios'])){
			$insert = " INSERT INTO sisaccesos (eCodUsuario, fhFecha, tIP) VALUES (".$_SESSION['sesionUsuario']['eCodUsuario'].", GETDATE(), '".$_SERVER['REMOTE_ADDR']."')";
			mysql_query($link,$insert);
		}
	}
	
	public function crearBotones(){
		$link = getLink();
		$html="";
		$tSeccion=$this->quitarExtencion($this->ePagina);
		$select=" SELECT ssb.tCodSeccion, ssb.eCodBoton, sb.tFuncion AS Funcion, ".
				" sb.tNombre, ssb.ePosicion, sb.tIcono ".
				" FROM sisseccionesbotones ssb ".
				" INNER JOIN sisbotones sb ON sb.eCodBoton=ssb.eCodBoton ".
				" WHERE ssb.tCodSeccion ='".$tSeccion."' ".
				" ORDER BY ssb.ePosicion ASC ";
		$rsBotones=mysqli_query($link,$select);
		if((int)mysqli_num_rows($rsBotones)>0){
			$html.="<table border=\"0\" width=\"980px\">";
			$html.="<tr><td colspan=\"2\" width=\"100%\"><hr width=\"99%\" size=\"0\" align=\"center\" color=\"#CACACA\"></td></tr><tr><td class=\"\">";
			/*while($rBoton=mysqli_fetch_array($rsBotones,MYSQLI_NUM)){
				$html.="&nbsp;<button id=\"btn".$rBoton{'eCodBoton'}."_".$rBoton{'ePosicion'}."\" onClick=\"".$rBoton{'Funcion'}."\">". //dojoType=\"dijit.form.Button\"
					   "<img width=\"16\" height=\"16\" src=\"https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/".$rBoton{'tIcono'}."\">".
					   "<span>&nbsp;&nbsp;".utf8_encode($rBoton{'tNombre'})."</span>".
					   "</button>";
			}*/
			$rBoton=mysqli_fetch_array($rsBotones,MYSQLI_NUM);
			foreach ($rsBotones as $key => $value) {
				$html.="&nbsp;<button id=\"btn".$value['eCodBoton']."_".$value['ePosicion']."\" onClick=\"".$value['Funcion']."\">". //dojoType=\"dijit.form.Button\"
					   "<img width=\"16\" height=\"16\" src=\"https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/".$value['tIcono']."\">".
					   "<span>&nbsp;&nbsp;".utf8_encode($value['tNombre'])."</span>".
					   "</button>";
			}
			$html.="</td><td ></td></tr>";
			$html.="<span style=\"display:none;\" id=\"spn\">&nbsp;&nbsp;<img src=\"../../gif/ic-cargador-16x16.gif\" width=\"16\" height=\"16\" align=\"absmiddle\" /></span>";
			$html.="</table>";
		}
		print $html;
	}
	
	public function tituloSeccion(){
		$link = getLink();
		$tSeccion=$this->quitarExtencion($this->ePagina);
		$rTitulo=mysqli_fetch_array(mysqli_query($link,"SELECT tNombre FROM sissecciones WHERE tCodSeccion='".$tSeccion."' AND tCodPadre!=1"),MYSQLI_NUM);
		print utf8_encode($rTitulo[0]);
	}
	
	public function quitarExtencion($tSeccion){
		return str_replace(".php", "",$tSeccion);
	}
	
	public function cargarFecha(){
		date_default_timezone_set('America/Mexico_City'); 
		$hoy=getdate();		
		switch ($hoy['wday']){case 0: $dia="Domingo";break; case 1: $dia="Lunes";break; case 2: $dia="Martes";break; case 3: $dia="Miercoles";break; case 4: $dia="Jueves";break; case 5: $dia="Viernes";break; case 6: $dia="Sabado";break;}	
		switch ($hoy['mon']){case 1: $mes="Enero";break; case 2: $mes="Febrero";break; case 3: $mes="Marzo";break; case 4: $mes="Abril";break; case 5: $mes="Mayo";break; case 6: $mes="Junio";break; case 7: $mes="Julio";break; case 8: $mes="Agosto";break; case 9: $mes="Septiembre";break; case 10: $mes="Octubre";break; case 11: $mes="Noviembre";break; case 12: $mes="Diciembre";break;} 
		if ($hoy["hours"]<10)
		$hora="0".$hoy["hours"];
		else
		$hora=$hoy["hours"];

		if ($hoy["minutes"]<10)
		$minutos="0".$hoy["minutes"];
		else
		$minutos=$hoy["minutes"];

		$fecha=$dia.", ".$hoy["mday"]." de ".$mes. " del ". $hoy["year"]. " (".$hora.":".$minutos." Horas)"; 	
		return $fecha;
	}
	
	public function imagenDanios($datos){
		$tExt = "png";
		$pMas = $datos['pix'];
		
		$select=" SELECT rd.*, cd.tCodDanio".
				" FROM releirsdanios rd ".
				" INNER JOIN catvistascontenedores cv ON cv.eCodVista = rd.eCodVista".
				" INNER JOIN catdanioscontenedores cd ON cd.eCodDanio = rd.eCodDanio".
				" WHERE rd.eCodEIR = ".$datos['eir']." AND rd.eCodVista=".$datos['vista'];
		$rsDanios = mysql_query($select);
		$select=" SELECT * FROM catvistascontenedores WHERE eCodVista=".$datos['vista'];
		$rVista=mysql_fetch_array(mysql_query($select));
		//print  "<img width=\"16\" height=\"16\" src=\"https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/".$rVista{'tNombreCorto'}.".".$tExt\">";
		list($ancho, $alto) = getimagesize($datos['nivel']."https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/".$rVista{'tNombreCorto'}.".".$tExt);
		$nuevo_ancho = $ancho +($pMas*2);
		$nuevo_alto = $alto +($pMas*2);
		$imagen_p = imagecreatetruecolor($nuevo_ancho, $nuevo_alto);

		if($tExt=="png"){
			$tImagen = imagecreatefrompng($datos['nivel']."https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/".$rVista{'tNombreCorto'}.".".$tExt);
			imagealphablending($tImagen, true);
			imagesavealpha($tImagen, true);
			imagealphablending($imagen_p, false);
			$color = imagecolortransparent($imagen_p, imagecolorallocatealpha($imagen_p, 0, 0, 0, 127));
			imagefill($imagen_p, 0, 0, $color);
			imagesavealpha($imagen_p, true);
		}else{
			$tImagen = imagecreatefromjpeg($datos['nivel']."https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/".$rVista{'tNombreCorto'}.".".$tExt);
			$background = imagecolorallocate($imagen_p, 255, 255, 255); 
			imagefill($imagen_p, 0, 0, $background); 
		}
		imagecopyresampled($imagen_p, $tImagen, $pMas, $pMas, 0, 0, $nuevo_ancho-($pMas*2), $nuevo_alto-($pMas*2), $ancho, $alto);
		while ($rDanio=mysql_fetch_array($rsDanios)){
			$ia = imagecolorallocate($imagen_p, 192, 0, 0);
			imagestring($imagen_p,3,$rDanio{'ePosX'}+$pMas,$rDanio{'ePosY'}+$pMas,$rDanio{'tCodDanio'},$ia);
		}
		$tNombre = $datos['nivel']."archivos/2.4.5.1/".uniqid().".".$tExt;
		if($tExt=="png"){
			imagepng($imagen_p,$tNombre);
		}else{
			imagejpeg($imagen_p,$tNombre,100);
		}
		$this->rArchivos[] = $tNombre;
		$tArchivo = $tNombre;
		$tMedio = fopen($tArchivo, "rb");
		$tContenido = fread($tMedio, filesize($tArchivo));
		fclose($tMedio);
		imagedestroy($tImagen);
		return array('archivo'=>$tArchivo,'contenido'=>$tContenido,'imagen_p'=>$ancho, 'directorio'=>$tNombre);
	}
	
	public function eliminarImagenes(){
	  foreach($this->rArchivos as $rArchivo){
			unlink($rArchivo);
		}
	}

//
} ?>