﻿<?php require_once("soluciones-mysql.php");
require_once("sistema.php");
$link = getLink();

class cFacturacion{
	public function cFacturacion(){
		
	}
	
	public function facturar(){
		
		$eCodSolicitud=$_POST['eCodSolicitud'];
		$eCodUsuario=$_POST['eUsuario'] ? $_POST['eUsuario'] : "NULL";
		
		$select=" SELECT tNombre ".
				" FROM catmetodospago ".
				" WHERE eCodMetodoPago=".(int)$_POST['eCodMetodoPago'];
		$rMetodoPago=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
		
		$select=" SELECT tNombre ".
				" FROM catformaspago ".
				" WHERE eCodFormaPago=".(int)$_POST['eCodFormaPago'];
		$rFormaPago=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
		
		$select=" SELECT cc.*, ce.tNombre AS Emisor, ce.tRFC, ce.tDireccion, ce.tColonia, ".
				" ce.tNumeroExterior, cci.tNombre AS Ciudad, ces.tNombre AS Estado, ".
				" cp.tNombre AS Pais, ce.tCodigoPostal, ce.tReferencia ".
				" FROM catcomprobantes cc ".
				" INNER JOIN catentidades ce ON ce.eCodEntidad=cc.eCodEntidad ".
				" LEFT JOIN catciudades cci ON cci.eCodCiudad=ce.eCodCiudad ".
				" LEFT JOIN catestados ces ON ces.eCodEstado=ce.eCodEstado ".
				" LEFT JOIN catpaises cp ON cp.eCodPais=ce.eCodPais ".
				" WHERE bFacturacion IS NOT NULL ";
		$rComprobante=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
		
		$select=" SELECT CASE WHEN MAX(eFolio) IS NULL THEN 1 ELSE MAX(eFolio+1) END AS Folio ".
				" FROM opecfds ".
				" WHERE tSerie='".$rComprobante{'tSerie'}."'";
		$rFolio=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
		
		$insert=" INSERT INTO opecfds (eCodComprobante, eCodEmisor, eCodNaviera, eCodCliente, eCodClienteFacturacion, eCodFormaPago, eCodUsuario, tCodEstatus, ".
				" eFolio, tSerie, fhFecha, fhFechaCreacion, tNombreEmisor, tRFCEmisor, tCalleEmisor, tColoniaEmisor, tNumeroExteriorEmisor, tMunicipioEmisor, ".
				" tEstadoEmisor, tPaisEmisor, tCPEmisor, tReferenciaEmisor, tNombreReceptor, tRFCReceptor, tCalleReceptor, tColoniaReceptor, ".
				" tNumeroExteriorReceptor, tNumeroInteriorReceptor, tReferenciaReceptor, tMunicipioReceptor, tEstadoReceptor, tPaisReceptor, tCPReceptor, ".
				" tFormaPago, tMetodoPago, tCondicionesPago, dTipoCambio, tDivisa, tReferencia) ".
				" SELECT ".$rComprobante{'eCodComprobante'}.", ".$rComprobante{'eCodEntidad'}.", os.eCodNaviera, os.eCodCliente, os.eCodFacturarA, ".
				(int)$_POST['eCodFormaPago'].", ".$eCodUsuario.", 'PR', ".$rFolio{'Folio'}.", '".$rComprobante{'tSerie'}."', CURRENT_TIMESTAMP, ".
				" CURRENT_TIMESTAMP, '".$rComprobante{'Emisor'}."', '".$rComprobante{'tRFC'}."', '".$rComprobante{'tDireccion'}."', '".$rComprobante{'tColonia'}."', ".
				" '".$rComprobante{'tNumeroExterior'}."', '".$rComprobante{'Ciudad'}."', '".$rComprobante{'Estado'}."', '".$rComprobante{'Pais'}."', ".
				" '".$rComprobante{'tCodigoPostal'}."', '".$rComprobante{'tReferencia'}."', ce.tNombre, ce.tRFC, ce.tDireccion, ce.tColonia, ".
				" ce.tNumeroExterior, ce.tNumeroInterior, ce.tReferencia, cci.tNombre, ces.tNombre, cp.tNombre, ce.tCodigoPostal, '".$rFormaPago{'tNombre'}."', ".
				" '".$rMetodoPago{'tNombre'}."', '".$_POST['tCondicionesPago']."', 1.0, 'MXP', '".$_POST['tReferencia']."' ".
				" FROM opesolicitudesservicios os ".
				" LEFT JOIN catentidades ce ON ce.eCodEntidad=os.eCodFacturarA ".
				" LEFT JOIN catciudades cci ON cci.eCodCiudad=ce.eCodCiudad ".
				" LEFT JOIN catestados ces ON ces.eCodEstado=ce.eCodEstado ".
				" LEFT JOIN catpaises cp ON cp.eCodPais=ce.eCodPais ".
				" WHERE os.eCodSolicitud=".(int)$eCodSolicitud;
		if($res=mysqli_query($link,$insert)){
			$exito=1;
			$dSubtotalF=0;
			$tInserts=$insert;
			$select=" select last_insert_id() AS Llave ";
			$rCodigo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
			$eCodCFD=(int)$rCodigo['Llave'];
			$select=" SELECT rs.*, cs.tNombre AS Servicio ".
					" FROM relsolicitudesserviciosservicios rs ".
					" INNER JOIN catservicios cs ON rs.eCodServicio=cs.eCodServicio ".
					" WHERE rs.eCodSolicitud=".(int)$eCodSolicitud;
			$rsServicios=mysqli_query($link,$select);
			while($rServicio=mysqli_fetch_array($rsServicios,MYSQLI_ASSOC)){
				$insert=" INSERT INTO relcfdsconceptos (eCodCFD, eCodServicio, tUnidad, tIdentificacion, tDescripcion, dCantidad, dValorUnitario, ".
						" dImpuestosTrasladados, dImpuestosRetenidos, dImporte) ".
						" VALUES (".$eCodCFD.", ".$rServicio{'eCodServicio'}.", 'No Aplica', NULL, '".$rServicio{'Servicio'}."', ".$rServicio{'eCantidad'}.", ".$rServicio{'dImporte'}.", ".
						" ".$rServicio{'dSubtotal'}*.16.", NULL, ".$rServicio{'dSubtotal'}.")";
				$dSubtotalF+=(float)$rServicio{'dSubtotal'};
				if($res=mysqli_query($link,$insert)){
					$select=" select last_insert_id() AS Llave ";
					$rCodigo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
					$eCodConcepto=(int)$rCodigo['Llave'];
					$tInserts.=$insert;
				}else{
					$exito=0;
					$tInserts.=$insert;
				}
			}
			$insert=" INSERT INTO relcfdsdescripcionesconceptos (eCodConcepto, eCodCFD, tNombre, tValor) ".
					" SELECT ".$eCodConcepto.", ".$eCodCFD.", 'Adicional Concepto', tCodContenedor ".
					" FROM relsolicitudesserviciosmercancias ".
					" WHERE eCodSolicitud=".(int)$eCodSolicitud;
			mysqli_query($link,$insert);
			$tInserts.=$insert;
			
			$insert=" INSERT INTO relcfdssolicitudesservicios (eCodCFD, eCodSolicitud) ".
					" VALUES (".$eCodCFD.", ".(int)$eCodSolicitud.")";
			mysqli_query($link,$insert);
			$tInserts.=$insert;
			
			$update=" UPDATE opecfds ".
					" SET dSubtotal=".(float)$dSubtotalF.",".
					" dImpuestosTrasladados=".(float)$dSubtotalF*.16.",".
					" dTotal=".(float)$dSubtotalF*1.16.
					" WHERE eCodCFD=".$eCodCFD;
			mysqli_query($link,$update);
			$tInserts.=$update;
		}else{
			$exito=0;
			$tInserts.=$insert;
		}
		return array('exito'=>$exito, 'codigo'=>$eCodCFD, 'inserts'=>$tInserts);
	}
	
	public function generarCFDi(){
		
		$eCodCFD=$_POST['eCodCFD'];
		$eCodUsuario=$_POST['eUsuario'] ? $_POST['eUsuario'] : "NULL";
		
		$select=" SELECT * ".
				" FROM sisconfiguracion ".
				" WHERE tNombre='tRegimenFiscal' ";
		$rRegimen=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
		
		$select=" SELECT oc.eCodCFD, oc.eCodCliente, oc.tMetodoPago, ctc.tNombre AS TipoComprobante, oc.dTotal, oc.tDivisa, oc.dTipoCambio, oc.tRFCEmisor, ".
				" oc.dSubtotal, oc.tCondicionesPago, ccd.tCer, cfp.tNombre AS FormaPago, oc.fhFechaCreacion, ccd.tCodCertificado, ".
				" oc.eFolio, oc.tSerie, oc.tNombreEmisor, oc.tCPEmisor, oc.tPaisEmisor, oc.tEstadoEmisor, oc.tMunicipioEmisor, oc.tColoniaEmisor, ".
				" oc.tLocalidadEmisor, oc.tNumeroInteriorEmisor, oc.tNumeroExteriorEmisor, oc.tCalleEmisor, oc.tNombreReceptor, oc.tReferencia, ".
				" oc.tRFCReceptor, oc.tCPReceptor, oc.tPaisReceptor, oc.tEstadoReceptor, oc.tMunicipioReceptor, oc.tColoniaReceptor, ".
				" oc.tNumeroInteriorReceptor, oc.tNumeroExteriorReceptor, oc.tCalleReceptor, oc.dImpuestosTrasladados, rcs.eCodSolicitud ".
				" FROM opecfds oc ".
				" LEFT JOIN catcomprobantes cc ON cc.eCodComprobante=oc.eCodComprobante ".
				" LEFT JOIN cattiposcomprobantes ctc ON ctc.eCodTipoComprobante=cc.eCodTipoComprobante ".
				" LEFT JOIN catcertificadosdigitales ccd ON ccd.eCodCertificado=cc.eCodCertificado ".
				" LEFT JOIN catformaspago cfp ON cfp.eCodFormaPago=oc.eCodFormaPago ".
				" LEFT JOIN relcfdssolicitudesservicios rcs ON rcs.eCodCFD=oc.eCodCFD ".
				" WHERE oc.eCodCFD=".(int)$eCodCFD;
		$rCFD=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
		
		$select=" SELECT rcc.eCodConcepto, rcc.dImporte, rcc.dValorUnitario, rcc.tDescripcion, rcc.dCantidad ".
				" FROM relcfdsconceptos rcc ".
				" WHERE rcc.eCodCFD=".(int)$eCodCFD;
		$rsConceptos=mysqli_query($link,$select);
		
		$cadena="01".//Tipo de registro
				"|".$rCFD{'eCodCFD'}.//Id comprobante
				"|WCY".//$rCFD{'tSerie'}.//Serie
				"|".$rCFD{'eFolio'}.//Folio
				"|".date("Y/m/d H:i:s", strtotime($rCFD{'fhFechaCreacion'})).//Fecha y hora de emision
				"|".str_replace(",", "", number_format($rCFD{'dSubtotal'},2)).//Subtotal
				"|".str_replace(",", "", number_format($rCFD{'dImpuestosTrasladados'},2)).//Total impuestos trasladados
				"|".str_replace(",", "", number_format($rCFD{'dImpuestosRetenidos'},2)).//Total impuestos retenidos
				"|0.00".//str_replace(",", "", number_format($rCFD{''},2)).//Descuentos/Bonificaciones
				"|".str_replace(",", "", number_format($rCFD{'dTotal'},2)).//Total
				"|".//Total con letra
				"|".//Motivo de descuento
				"|PAGO EN UNA SOLA EXHIBICION".//Forma de Pago
				"|".$rCFD{'tCondicionesPago'}.//Condiciones de Pago
				"|".$rCFD{'tMetodoPago'}.//Metodo de pago
				"|MXN".//$rCFD{'tDivisa'}.Moneda
				"|".str_replace(",", "", number_format($rCFD{'dTipoCambio'},2)).//Tipo de cambio
				"|".//Referencia de pago
				"|".//Folio fiscal original
				"|".//Fecha Folio fiscal original
				"|"."|";//Monto Folio fiscal original
				
		$cadena.="\n02|".$rCFD{'eCodCliente'}."|".$rCFD{'tRFCReceptor'}."|".$rCFD{'tNombreReceptor'}."|".$rCFD{'tPaisReceptor'}."|".$rCFD{'tCalleReceptor'}."|".$rCFD{'tNumeroExteriorReceptor'}."|".$rCFD{'tNumeroInteriorReceptor'}."|".$rCFD{'tColoniaReceptor'}."|||".$rCFD{'tMunicipioReceptor'}."|".$rCFD{'tEstadoReceptor'}."|".$rCFD{'tCPReceptor'}."|";
		$eConcepto=1;
		while($rConcepto=mysqli_fetch_array($rsConceptos,MYSQLI_ASSOC)){
			$cadena.="\n04|".$eConcepto."|".$rConcepto{'eCodConcepto'}."|".$rConcepto{'dCantidad'}."|".$rConcepto{'tDescripcion'}."|".str_replace(",", "", number_format($rConcepto{'dValorUnitario'},2))."|".str_replace(",", "", number_format($rConcepto{'dImporte'},2))."|PZA|";
			$select=" SELECT eCodConcepto, eCodCFD, tNombre, tValor ".
					" FROM relcfdsdescripcionesconceptos ".
					" WHERE eCodCFD=".(int)$eCodCFD." AND eCodConcepto=".(int)$rConcepto{'eCodConcepto'};
			$rsConceptosCFD=mysqli_query($link,$select);
			while($rConceptoCFD=mysqli_fetch_array($rsConceptosCFD,MYSQLI_ASSOC)){
				$cadena.="\n05|".$eConcepto."|".$rConceptoCFD{'tNombre'}."|".$rConceptoCFD{'tValor'}."|";
			}
			$eConcepto++;
		}
		$cadena.="\n10|IVA|16|".str_replace(",", "", number_format($rCFD{'dImpuestosTrasladados'},2))."|";
		if((int)$rCFD{'eCodSolicitud'}>0){
			$cadena.="\n12|Informacion Adicional|Número de Solicitud:".$rCFD{'eCodSolicitud'}."|";
		}
		if(trim($rCFD{'tReferencia'})!=""){
			$cadena.="\n12|Informacion Adicional|Referencia Aduanal:".$rCFD{'tReferencia'}."|";
		}
		
		ini_set("soap.wsdl_cache_enabled", "0");
		header('Content-Type: text/plain');
		$cliente = new SoapClient('https://apt.webinvoice.mx/wsv/wsv/', array('trace'=>1, 'style'=>SOAP_DOCUMENT, 'use'=>SOAP_LITERAL));
		$usuario=array('usuario'=>'fernando.mendez', 'password'=>'FMFM.2015');
		$facturacion=array('datosusuario'=>$usuario, 
							'identificador'=>'ae23d10469aa11e3ba5102fa016ccf30',
							'rfc'=>'SUL010720JN8',
							'comprobante'=>'FACTURA-WCY',
							'contenido'=>$cadena,
							'regresardocumentos'=>1);
		$contestacion=$cliente->generarCFD($facturacion);

		if((int)$contestacion->exito==1){
			$tOk=1;
			$tUUID="'".$contestacion->informacion->uuid."'";
			$update=" UPDATE opecfds ".
					" SET tUUID=".$tUUID.",".
					" bTimbrado=1,".
					" tCodEstatus='AU'".
					" WHERE eCodCFD=".(int)$eCodCFD;
			mysqli_query($link,$update);

			foreach($contestacion->detalles as $valor){
				$insert=" INSERT INTO relcfdsarchivos(eCodCFD, tArchivo, tTipo) ".
						" VALUES (".(int)$eCodCFD.", '".$valor->valor."', '".$valor->nombre."') ";
				mysqli_query($link,$insert);
			}
		}else{
			$tOk=0;
			$tError=$contestacion->errores[0]->descripcion;
		}
		return array('Ok'=>$tOk, 'eCodCFD'=>(int)$eCodCFD, 'tError'=>$tError);
	}
	
	function cancelarCFDi(){
		$eCodCFD=$_POST['eCodCFD'];
		$eCodUsuario=$_POST['eUsuario'] ? $_POST['eUsuario'] : "NULL";

		$select=" SELECT * ".
				" FROM opecfds ".
				" WHERE eCodCFD=".(int)$eCodCFD;
		$rCFD=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
		
		ini_set("soap.wsdl_cache_enabled", "0");
		header('Content-Type: text/plain');
		$cliente = new SoapClient('https://apt.webinvoice.mx/wsv/wsv/', array('trace'=>1, 'style'=>SOAP_DOCUMENT, 'use'=>SOAP_LITERAL));
		$usuario=array('usuario'=>'fernando.mendez', 'password'=>'FMFM.2015');
		$cancelacion=array('datosusuario'=>$usuario, 
							'identificador'=>'ae23d10469aa11e3ba5102fa016ccf30',
							'rfc'=>'SUL010720JN8',
							'serie'=>$rCFD{'tSerie'},
							'folio'=>$rCFD{'eFolio'},
							'uuid'=>$rCFD{'tUUID'});
		$contestacion=$cliente->cancelarCFD($cancelacion);
		print_r($contestacion);
		if((int)$contestacion->exito==1){
			$tOk=1;
			$update=" UPDATE opecfds ".
					" SET fhFechaCancelacion=CURRENT_TIMESTAMP".$tUUID.",".
					" eCodUsuarioCancelacion=".$eCodUsuario.",".
					" tCodEstatus='CA' ".
					" WHERE eCodCFD=".(int)$eCodCFD;
			mysqli_query($link,$update);
		}else{
			$tOk=0;
			$tError=$contestacion->errores[0]->descripcion;
		}
		return array('Ok'=>$tOk, 'eCodCFD'=>(int)$eCodCFD, 'tError'=>$tError);
	}
}
?>