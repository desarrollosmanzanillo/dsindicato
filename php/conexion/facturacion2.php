﻿<?php require_once("soluciones-mysql.php");
require_once("sistema.php");
$link = getLink();

class cFacturacion{
	public function cFacturacion(){
		
	}
	
	public function facturar(){
		
		$eCodSolicitud=$_POST['eCodSolicitud'];
		$eCodUsuario=$_POST['eUsuario'] ? $_POST['eUsuario'] : "NULL";
		
		$select=" SELECT tNombre ".
				" FROM catmetodospago ".
				" WHERE eCodMetodoPago=".(int)$_POST['eCodMetodoPago'];
		$rMetodoPago=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
		
		$select=" SELECT tNombre ".
				" FROM catformaspago ".
				" WHERE eCodFormaPago=".(int)$_POST['eCodFormaPago'];
		$rFormaPago=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
		
		$select=" SELECT cc.*, ce.tNombre AS Emisor, ce.tRFC, ce.tDireccion, ce.tColonia, ".
				" ce.tNumeroExterior, cci.tNombre AS Ciudad, ces.tNombre AS Estado, ".
				" cp.tNombre AS Pais, ce.tCodigoPostal, ce.tReferencia ".
				" FROM catcomprobantes cc ".
				" INNER JOIN catentidades ce ON ce.eCodEntidad=cc.eCodEntidad ".
				" LEFT JOIN catciudades cci ON cci.eCodCiudad=ce.eCodCiudad ".
				" LEFT JOIN catestados ces ON ces.eCodEstado=ce.eCodEstado ".
				" LEFT JOIN catpaises cp ON cp.eCodPais=ce.eCodPais ".
				" WHERE bFacturacion IS NOT NULL ";
		$rComprobante=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
		
		$select=" SELECT CASE WHEN MAX(eFolio) IS NULL THEN 1 ELSE MAX(eFolio+1) END AS Folio ".
				" FROM opecfds ".
				" WHERE tSerie='".$rComprobante{'tSerie'}."'";
		$rFolio=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
		
		$insert=" INSERT INTO opecfds (eCodComprobante, eCodEmisor, eCodNaviera, eCodCliente, eCodClienteFacturacion, eCodFormaPago, eCodUsuario, tCodEstatus, ".
				" eFolio, tSerie, fhFecha, fhFechaCreacion, tNombreEmisor, tRFCEmisor, tCalleEmisor, tColoniaEmisor, tNumeroExteriorEmisor, tMunicipioEmisor, ".
				" tEstadoEmisor, tPaisEmisor, tCPEmisor, tReferenciaEmisor, tNombreReceptor, tRFCReceptor, tCalleReceptor, tColoniaReceptor, ".
				" tNumeroExteriorReceptor, tNumeroInteriorReceptor, tReferenciaReceptor, tMunicipioReceptor, tEstadoReceptor, tPaisReceptor, tCPReceptor, ".
				" tFormaPago, tMetodoPago, tCondicionesPago, dTipoCambio, tDivisa) ".
				" SELECT ".$rComprobante{'eCodComprobante'}.", ".$rComprobante{'eCodEntidad'}.", os.eCodNaviera, os.eCodCliente, os.eCodFacturarA, ".
				(int)$_POST['eCodFormaPago'].", ".$eCodUsuario.", 'PR', ".$rFolio{'Folio'}.", '".$rComprobante{'tSerie'}."', CURRENT_TIMESTAMP, ".
				" CURRENT_TIMESTAMP, '".$rComprobante{'Emisor'}."', '".$rComprobante{'tRFC'}."', '".$rComprobante{'tDireccion'}."', '".$rComprobante{'tColonia'}."', ".
				" '".$rComprobante{'tNumeroExterior'}."', '".$rComprobante{'Ciudad'}."', '".$rComprobante{'Estado'}."', '".$rComprobante{'Pais'}."', ".
				" '".$rComprobante{'tCodigoPostal'}."', '".$rComprobante{'tReferencia'}."', ce.tNombre, ce.tRFC, ce.tDireccion, ce.tColonia, ".
				" ce.tNumeroExterior, ce.tNumeroInterior, ce.tReferencia, cci.tNombre, ces.tNombre, cp.tNombre, ce.tCodigoPostal, '".$rFormaPago{'tNombre'}."', ".
				" '".$rMetodoPago{'tNombre'}."', '".$_POST['tCondicionesPago']."', 1.0, 'MXP' ".
				" FROM opesolicitudesservicios os ".
				" LEFT JOIN catentidades ce ON ce.eCodEntidad=os.eCodFacturarA ".
				" LEFT JOIN catciudades cci ON cci.eCodCiudad=ce.eCodCiudad ".
				" LEFT JOIN catestados ces ON ces.eCodEstado=ce.eCodEstado ".
				" LEFT JOIN catpaises cp ON cp.eCodPais=ce.eCodPais ".
				" WHERE os.eCodSolicitud=".(int)$eCodSolicitud;
		if($res=mysqli_query($link,$insert)){
			$exito=1;
			$dSubtotalF=0;
			$tInserts=$insert;
			$select=" select last_insert_id() AS Llave ";
			$rCodigo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
			$eCodCFD=(int)$rCodigo['Llave'];
			$select=" SELECT rs.*, cs.tNombre AS Servicio ".
					" FROM relsolicitudesserviciosservicios rs ".
					" INNER JOIN catservicios cs ON rs.eCodServicio=cs.eCodServicio ".
					" WHERE rs.eCodSolicitud=".(int)$eCodSolicitud;
			$rsServicios=mysqli_query($link,$select);
			while($rServicio=mysqli_fetch_array($rsServicios,MYSQLI_ASSOC)){
				$insert=" INSERT INTO relcfdsconceptos (eCodCFD, eCodServicio, tUnidad, tIdentificacion, tDescripcion, dCantidad, dValorUnitario, ".
						" dImpuestosTrasladados, dImpuestosRetenidos, dImporte) ".
						" VALUES (".$eCodCFD.", ".$rServicio{'eCodServicio'}.", 'No Aplica', NULL, '".$rServicio{'Servicio'}."', ".$rServicio{'eCantidad'}.", ".$rServicio{'dImporte'}.", ".
						" ".$rServicio{'dSubtotal'}*.16.", NULL, ".$rServicio{'dSubtotal'}.")";
				$dSubtotalF+=(float)$rServicio{'dSubtotal'};
				if($res=mysqli_query($link,$insert)){
					$select=" select last_insert_id() AS Llave ";
					$rCodigo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
					$eCodConcepto=(int)$rCodigo['Llave'];
					$tInserts.=$insert;
				}else{
					$exito=0;
					$tInserts.=$insert;
				}
			}
			$insert=" INSERT INTO relcfdsdescripcionesconceptos (eCodConcepto, eCodCFD, tNombre, tValor) ".
					" SELECT ".$eCodConcepto.", ".$eCodCFD.", 'Adicional Concepto', tCodContenedor ".
					" FROM relsolicitudesserviciosmercancias ".
					" WHERE eCodSolicitud=".(int)$eCodSolicitud;
			mysqli_query($link,$insert);
			$tInserts.=$insert;
			
			$insert=" INSERT INTO relcfdssolicitudesservicios (eCodCFD, eCodSolicitud) ".
					" VALUES (".$eCodCFD.", ".(int)$eCodSolicitud.")";
			mysqli_query($link,$insert);
			$tInserts.=$insert;
			
			$update=" UPDATE opecfds ".
					" SET dSubtotal=".(float)$dSubtotalF.",".
					" dImpuestosTrasladados=".(float)$dSubtotalF*.16.",".
					" dTotal=".(float)$dSubtotalF*1.16.
					" WHERE eCodCFD=".$eCodCFD;
			mysqli_query($link,$update);
			$tInserts.=$update;
		}else{
			$exito=0;
			$tInserts.=$insert;
		}
		return array('exito'=>$exito, 'codigo'=>$eCodCFD, 'inserts'=>$tInserts);
	}
	
	public function generarDiverza(){
		
		$eCodCFD=$_POST['eCodCFD'];
		$eCodUsuario=$_POST['eUsuario'] ? $_POST['eUsuario'] : "NULL";
		
		$select=" SELECT * ".
				" FROM sisconfiguracion ".
				" WHERE tNombre='tRegimenFiscal' ";
		$rRegimen=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
		
		$select=" SELECT oc.tMetodoPago, 
					ctc.tNombre AS TipoComprobante, 
					oc.dTotal, oc.tDivisa, oc.dTipoCambio, oc.tRFCEmisor, 
					oc.dSubtotal, oc.tCondicionesPago, ccd.tCer, cfp.tNombre AS FormaPago, oc.fhFechaCreacion, ccd.tCodCertificado, 
					oc.eFolio, oc.tSerie, oc.tNombreEmisor, oc.tCPEmisor, oc.tPaisEmisor, oc.tEstadoEmisor, oc.tMunicipioEmisor, oc.tColoniaEmisor, 
					oc.tLocalidadEmisor, oc.tNumeroInteriorEmisor, oc.tNumeroExteriorEmisor, oc.tCalleEmisor, oc.tNombreReceptor, 
					oc.tRFCReceptor, oc.tCPReceptor, oc.tPaisReceptor, oc.tEstadoReceptor, oc.tMunicipioReceptor, oc.tColoniaReceptor, 
					oc.tNumeroInteriorReceptor, oc.tNumeroExteriorReceptor, oc.tCalleReceptor, oc.dImpuestosTrasladados 
				FROM opecfds oc 
				LEFT JOIN catcomprobantes cc ON cc.eCodComprobante=oc.eCodComprobante 
				LEFT JOIN cattiposcomprobantes ctc ON ctc.eCodTipoComprobante=cc.eCodTipoComprobante 
				LEFT JOIN catcertificadosdigitales ccd ON ccd.eCodCertificado=cc.eCodCertificado 
				LEFT JOIN catformaspago cfp ON cfp.eCodFormaPago=oc.eCodFormaPago 
				WHERE oc.eCodCFD=".(int)$eCodCFD;
		$rCFD=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
		
		$select=" SELECT 
					rcc.dImporte, 
					rcc.dValorUnitario, 
					rcc.tDescripcion, 
					rcc.dCantidad 
				FROM relcfdsconceptos rcc 
				WHERE rcc.eCodCFD=".(int)$eCodCFD;
		$rsConceptos=mysqli_query($link,$select);

		$cfd='<?xml version="1.0" encoding="UTF-8"?>
		<cfdi:Comprobante 
			NumCtaPago="1234" 
			LugarExpedicion="Manzanillo - Colima" 
			metodoDePago="'.$rCFD{'tMetodoPago'}.'"
			tipoDeComprobante="'.$rCFD{'TipoComprobante'}.'"
			total="'.str_replace(",", "", number_format($rCFD{'dTotal'},2)).'"
			subTotal="'.str_replace(",", "", number_format($rCFD{'dSubtotal'},2)).'"
			certificado="'.$rCFD{'tCer'}.'"
			noCertificado="'.$rCFD{'tCodCertificado'}.'"
			formaDePago="PAGO EN UNA SOLA EXHIBICION"
			sello="SaXzW85n5yn3X2/+G02GXA+SRM4Y+v9SN7BRm1pbT41/PYu13HoTcvNYOLP8G8V9OUd0XhAY6y+mkQLGH7p8h6BSC2V1G3WVfzyIQ76LM8E5wtmJElbRY3J2N4940tI1x/61dE9wz59EovJpZ05+pygbsjO9hBpXqk6WIEt8NRY="
			fecha="'.str_replace(" ", "T", $rCFD{'fhFechaCreacion'}).'"
			folio="'.$rCFD{'eFolio'}.'" 
			version="3.2"
			xmlns:cfdi="http://www.sat.gob.mx/cfd/3" 
			xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
			xsi:schemaLocation="http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd" 
			Moneda="'.$rCFD{'tDivisa'}.'" 
			TipoCambio="'.str_replace(",", "", number_format($rCFD{'dTipoCambio'},2)).'" 		
			condicionesDePago="'.$rCFD{'tCondicionesPago'}.'"
			serie="'.$rCFD{'tSerie'}.'" 
			>
			<cfdi:Emisor nombre="'.$rCFD{'tNombreEmisor'}.'" rfc="AAA010101AAA">
				<cfdi:DomicilioFiscal 
					codigoPostal="'.$rCFD{'tCPEmisor'}.'" 
					pais="'.$rCFD{'tPaisEmisor'}.'" 
					estado="'.$rCFD{'tEstadoEmisor'}.'"
					municipio="'.$rCFD{'tMunicipioEmisor'}.'" 
					colonia="'.$rCFD{'tColoniaEmisor'}.'" 
					noInterior="'.$rCFD{'tNumeroInteriorEmisor'}.'" 
					noExterior="'.$rCFD{'tNumeroExteriorEmisor'}.'"
					calle="'.$rCFD{'tCalleEmisor'}.'"/>
				<cfdi:ExpedidoEn 
					codigoPostal="'.$rCFD{'tCPEmisor'}.'" 
					pais="'.$rCFD{'tPaisEmisor'}.'" 
					estado="'.$rCFD{'tEstadoEmisor'}.'"
					municipio="'.$rCFD{'tMunicipioEmisor'}.'" 
					colonia="'.$rCFD{'tColoniaEmisor'}.'" 
					noInterior="'.$rCFD{'tNumeroInteriorEmisor'}.'" 
					noExterior="'.$rCFD{'tNumeroExteriorEmisor'}.'"
					calle="'.$rCFD{'tCalleEmisor'}.'"/>
				<cfdi:RegimenFiscal Regimen="'.$rRegimen{'tValor'}.'"/>
			</cfdi:Emisor>
			<cfdi:Receptor nombre="'.$rCFD{'tNombreReceptor'}.'" rfc="'.$rCFD{'tRFCReceptor'}.'">
				<cfdi:Domicilio 
					codigoPostal="'.$rCFD{'tCPReceptor'}.'" 
					pais="'.$rCFD{'tPaisReceptor'}.'" 
					estado="'.$rCFD{'tEstadoReceptor'}.'"
					municipio="'.$rCFD{'tMunicipioReceptor'}.'" 
					colonia="'.$rCFD{'tColoniaReceptor'}.'" 
					noInterior="'.$rCFD{'tNumeroInteriorReceptor'}.'" 
					noExterior="'.$rCFD{'tNumeroExteriorReceptor'}.'"
					calle="'.$rCFD{'tCalleReceptor'}.'"/>
			</cfdi:Receptor>
			<cfdi:Conceptos>';
				while($rConcepto=mysqli_fetch_array($rsConceptos,MYSQLI_ASSOC)){
					$cfd .='<cfdi:Concepto 
							importe="'.str_replace(",", "", number_format($rConcepto{'dImporte'},2)).'" 
							valorUnitario="'.str_replace(",", "", number_format($rConcepto{'dValorUnitario'},2)).'" 
							descripcion="'.$rConcepto{'tDescripcion'}.'" 
							unidad="PZA" 
							cantidad="'.$rConcepto{'dCantidad'}.'"/>';
				}
			$cfd .='</cfdi:Conceptos>
			<cfdi:Impuestos totalImpuestosTrasladados="'.str_replace(",", "", number_format($rCFD{'dImpuestosTrasladados'},2)).'">
				<cfdi:Traslados>
					<cfdi:Traslado importe="'.str_replace(",", "", number_format($rCFD{'dImpuestosTrasladados'},2)).'" tasa="16" impuesto="IVA"/>
				</cfdi:Traslados>
			</cfdi:Impuestos>
		</cfdi:Comprobante>';
		print $cfd;
		$request_options = array(
							'http' => array(
										'method'		=> "POST",
										'header'		=> "x-auth-token: ABCD1234",
										'content-type'	=> "application/vnd.sat.cfdi+xml; charset=UTF-8",
										'x-refid'		=> "A1",
										'content'		=> $cfd,
										'ignore_errors'	=> true
									)
						);
	
		$stream_context = stream_context_create($request_options);
		$response = file_get_contents('https://staging.diverza.com/api/v1/emitir', false, $stream_context);
		
		$response_code = $http_response_header[0];
		$response_message = $response;
		echo "Codigo HTTP: $response_code\r\n";
		echo "Timbre: $response_message\r\n";
	}
}
?>
