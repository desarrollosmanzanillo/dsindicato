<?php require_once("conexion/soluciones-mysql.php"); 
$link = getLink(); 
if($_POST){
	if($_POST['eProceso']==1){
		$exito				= 0;
		$tIMO				= ($_POST['tIMO']	? "'".trim(utf8_decode($_POST['tIMO']))."'"	: "NULL");
		$tNombre			= ($_POST['tNombre']		? "'".trim(utf8_decode($_POST['tNombre']))."'"		: "NULL");
		$eCodBuque			= ($_POST['eCodBuque']		? (int)$_POST['eCodBuque']							: "NULL");
		$eCodTipoBuque		= ($_POST['eCodTipoBuque']	? (int)$_POST['eCodTipoBuque']						: "NULL");
		$tIndicativoLlamada	= ($_POST['tIndicativoLlamada']	? "'".trim(utf8_decode($_POST['tIndicativoLlamada']))."'"	: "NULL");
		

		if((int)$eCodBuque>0){
			$update=" UPDATE catbuques ".
					" SET tNombre=".$tNombre.",".
					" tIndicativoLlamada=".$tIndicativoLlamada.",".
					" tIMO=".$tIMO.",".
					" eCodTipoBuque=".$eCodTipoBuque.
					" WHERE eCodBuque=".$eCodBuque;
			if($res=mysqli_query($link,$update)){
				$exito=1;
			}else{
				$exito=0;
			}
		}else{
			$insert=" INSERT INTO catbuques(tCodEstatus, tNombre, tIndicativoLlamada, tIMO, eCodTipoBuque) ".
					" VALUES ('AC', ".$tNombre.", ".$tIndicativoLlamada.", ".$tIMO.", ".$eCodTipoBuque.")";
			if($res=mysqli_query($link,$insert)){
				$exito=1;
				$select=" select last_insert_id() AS Llave ";
				$rCodigo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
				$eCodBuque=(int)$rCodigo['Llave'];
			}else{
				$exito=0;
			}
		}
		print "<input type=\"text\" value=\"".($exito==1 ? $eCodBuque : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}
	
	if($_POST['eProceso']==2){
		$rBuque=mysqli_fetch_array(mysqli_query($link,"SELECT 1 AS Buque FROM catbuques WHERE tIndicativoLlamada='".trim($_POST['tIndicativoLlamada'])."' AND eCodBuque!=".(int)$_POST['eCodBuque']),MYSQLI_ASSOC);
		print "<input name=\"eBuque\" id=\"eBuque\" value=\"".(int)$rBuque{'Buque'}."\">";
	}
}
if(!$_POST){
	
$select=" SELECT * ".
		" FROM catbuques ".
		" WHERE eCodBuque=".$_GET['eCodBuque'];
$rBuque = mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC); 

$select=" SELECT * ".
		" FROM cattiposbuques ".
		" WHERE tCodEstatus='AC' ".
		" ORDER BY tNombre ";
$rsTiposBuques = mysqli_query($link,$select); 

$select=" SELECT * ".
		" FROM catestatus ".
		" WHERE tCodEstatus IN ('AC', 'IN', 'CA') ".
		" ORDER BY tNombre ";
$rsEstatus = mysqli_query($link,$select); ?>
<script type="text/javascript">
function guardar(){
	dojo.byId('eProceso').value=1;
	bandera = false;
	mensaje = "¡Verifique lo siguiente!\n";
	if (!dojo.byId("tNombre").value){
		mensaje+="* Nombre\n";
		bandera = true;		
	}
	if (!dojo.byId("eCodTipoBuque").value){
		mensaje+="* Tipo de Buque\n";
		bandera = true;		
	}
	if (!dojo.byId("tCodEstatus").value){
		mensaje+="* Estatus\n";
		bandera = true;		
	}
	if (!dojo.byId("tIndicativoLlamada").value){
		mensaje+="* Indicativo de Llamada\n";
		bandera = true;		
	}

	if (bandera==true){
		alert(mensaje);		
	}else{
		if (confirm("¿Desea Guardar la Información?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				if(dojo.byId("eCodigo").value>0){
					alert("¡La información se guardó correctamente!");
					tURL = './?ePagina=2.3.4.1.2.php&eCodBuque='+dojo.byId("eCodigo").value;
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
		}
	}
}	

function verificarIndicativo(){
	if(dojo.byId('tIndicativoLlamada').value){
		dojo.byId('eProceso').value=2;
		dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
			dojo.byId("dvCNS").innerHTML = tRespuesta;
			if(dojo.byId('eBuque').value==1){
				alert("¡Este indicativo de llamada ya existe!");
				dojo.byId('tIndicativoLlamada').value="";
			}
		}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
	}
}

function consultar(){
	document.location = './?ePagina=2.3.4.1.php';
}
</script>
<div id="dvCNS" style="display:none;"></div>
<form action="" method="post" name="Datos" id="Datos" onsubmit="return false;">
<input type="hidden" name="eProceso" id="eProceso" value="" />
<input type="hidden" name="eCodBuque" id="eCodBuque" value="<?=$_GET['eCodBuque'];?>" />
<table border="0" cellpadding="0" cellspacing="0">
    <tr><td height="20"></td></tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Nombre </td>
	    <td width="50%" nowrap class="sanLR04">
           	<input name="tNombre" type="text" dojoType="dijit.form.TextBox" id="tNombre" value="<?=utf8_encode($rBuque{'tNombre'});?>" style="width:175px">
        </td>
		<td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Tipo de Buque </td>
	    <td width="50%" nowrap class="sanLR04">
			<select name="eCodTipoBuque" id="eCodTipoBuque" style="width:175px">
				<option value="">Seleccione...</option>
				<?php while($rTipoBuque=mysqli_fetch_array($rsTiposBuques,MYSQLI_ASSOC)){ ?>
					<option value="<?=$rTipoBuque{'eCodTipoBuque'}?>" <?=($rTipoBuque{'eCodTipoBuque'}==$rBuque{'eCodTipoBuque'} ? "selected='selected'" : "");?> ><?=utf8_encode($rTipoBuque{'tNombre'})?></option>
				<?php } ?>
			</select>
        </td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Indicativo de Llamada </td>
	    <td width="50%" nowrap class="sanLR04">
           	<input name="tIndicativoLlamada" type="text" dojoType="dijit.form.TextBox" id="tIndicativoLlamada" value="<?=utf8_encode($rBuque{'tIndicativoLlamada'});?>" style="width:175px" onchange="verificarIndicativo();">
        </td>
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" height="0" width="16" align="absmiddle"> IMO </td>
	    <td width="50%" nowrap class="sanLR04">
           	<input name="tIMO" type="text" dojoType="dijit.form.TextBox" id="tIMO" value="<?=utf8_encode($rBuque{'tIMO'});?>" style="width:175px">
        </td>
	</tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Estatus </td>
	    <td width="50%" nowrap class="sanLR04">
			<select name="tCodEstatus" id="tCodEstatus" style="width:175px">
				<option value="">Seleccione...</option>
				<?php while($rEstatu=mysqli_fetch_array($rsEstatus,MYSQLI_ASSOC)){ ?>
					<option value="<?=$rEstatu{'tCodEstatus'}?>" <?=($rEstatu{'tCodEstatus'}==$rBuque{'tCodEstatus'} ? "selected='selected'" : "");?> ><?=utf8_encode($rEstatu{'tNombre'})?></option>
				<?php } ?>
			</select>
        </td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle" /> <font class="fntG10K">Indica que es obligatorio llenar el campo.
    </font></td>
    </tr>
</table>
</form>
<?php } ?>