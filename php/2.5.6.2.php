<?php
require_once("conexion/soluciones-mysql.php"); 
$link = getLink();
if($_POST){

	if($_POST['fhFechaFacturacionInicio']!=""){
  		$fhFechaFacturacionInicio = $_POST['fhFechaFacturacionInicio'].' 00:00:00';
  		$fhFechaFacturacionFin = ($_POST['fhFechaFacturacionFin']!="" ? $_POST['fhFechaFacturacionFin'] : $_POST['fhFechaFacturacionInicio']).' 23:59:59';
 	}
	if($_POST['fhFechaCancelaInicio']!=""){
  		$fhFechaCancelaInicio = $_POST['fhFechaCancelaInicio'].' 00:00:00';
  		$fhFechaCancelaFin = ($_POST['fhFechaCancelaFin']!="" ? $_POST['fhFechaCancelaFin'] : $_POST['fhFechaCancelaInicio']).' 23:59:59';
 	}
	
	$select=" SELECT ono.tCodEstatus, ono.eCodNotificacionPago, ono.fhFecha, ono.fhFechaMovimiento, ".
			" ono.eCodTipoMovimiento, cmp.tNombre AS MetodoPago, ono.tReferencia, ono.dImporte, ".
			" ce.tNombre AS Agencia ".
			" FROM openotificacionespagos ono ".
			" LEFT JOIN catmetodospago cmp ON cmp.eCodMetodoPago=ono.eCodMetodoPago ".
			" LEFT JOIN catentidades ce ON ce.eCodEntidad=ono.eCodAgenciaAduanal ".
			" WHERE 1=1 ".
			($_POST['eFolio'] ? " AND oc.eFolio=".$_POST['eFolio'] : "").
			($_POST['Cliente']       ? " AND oc.tNombreReceptor   LIKE '%".$_POST['Cliente']."%'" : "").
			($_POST['eSolicitud']      ? " AND rcs.eCodSolicitud=  ".$_POST['eSolicitud'] : "").
			($_POST['Agencia']       ? " AND cea.tNombre   LIKE '%".$_POST['Agencia']."%'" : "").
			($_POST['Estatus'] ? " AND oc.tCodEstatus LIKE '%".$_POST['Estatus']."%'" : "").
			($_POST['MetodoPago']  ? " AND oc.tMetodoPago LIKE '%".$_POST['MetodoPago']."%'" : "").			
			($_POST['fhFechaFacturacionInicio']	? " AND oc.fhFecha			BETWEEN    '".$fhFechaFacturacionInicio. "' AND '" .$fhFechaFacturacionFin."'" : "" ).
			($_POST['fhFechaCancelaInicio']		? " AND oc.fhFechaCancelacion	BETWEEN    '".$fhFechaCancelaInicio. "' AND '" .$fhFechaCancelaFin."'" : "" ).
			((int)$_POST['eEntidad']==1 ? "" : " AND oc.eCodCliente=".(int)$_POST['eEntidad']).
			" ORDER BY ono.eCodNotificacionPago DESC ";
	$rsCFDs=mysqli_query($link,$select);
	$rsUsuarios=mysqli_query($link,$select);
	$registros=(int)mysqli_num_rows($rsUsuarios); ?>
<table cellspacing="0" border="0" width="965px">
  <tr>
    <td width="50%"><hr color="#666666" /></td>
    <td class="sanLR04" nowrap="nowrap"><b>Registros Encontrados: (
      <?=$registros;?>
      )</b></td>
    <td width="50%"><hr color="#666666" /></td>
  </tr>
</table>
<div style="display:block; top:0; left:0; width:965px; z-index=1; overflow: auto; height:400px;">
  <table cellspacing="0" border="0" width="965px">
    <thead>
      <tr class="thEncabezado">
        <td nowrap="nowrap" class="sanLR04" height="20" align="center">E</td>
        <td nowrap="nowrap" class="sanLR04">C&oacute;digo</td>
        <td nowrap="nowrap" class="sanLR04">Fecha</td>
        <td nowrap="nowrap" class="sanLR04">Fecha de Movimiento</td>
        <td nowrap="nowrap" class="sanLR04">Tipo de Movimiento</td>
        <td nowrap="nowrap" class="sanLR04">M&eacute;todo de Pago</td>
        <td nowrap="nowrap" class="sanLR04">Agencia Aduanal</td>
        <td nowrap="nowrap" class="sanLR04">Referencia</td>
        <td nowrap="nowrap" class="sanLR04" width="100%">Importe</td>
      </tr>
    </thead>
    <tbody>
      <?php $i=1; while($rCFD=mysqli_fetch_array($rsCFDs,MYSQLI_ASSOC)){ ?>
		  <tr>
			<td nowrap="nowrap" class="sanLR04" height="20" align="center">
            	<img width="16" height="16" alt="CFDs" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-<?=$rCFD{'tCodEstatus'};?>.png">
            </td>
			<td nowrap="nowrap" class="sanLR04 colmenu"><a href="?ePagina=2.5.6.2.1.php&eCodNotificacionPago=<?=$rCFD{'eCodNotificacionPago'};?>"><b><?=sprintf("%07d",$rCFD{'eCodNotificacionPago'});?></b></a></td>
			<td nowrap="nowrap" class="sanLR04"><?=date("d/m/Y H:i", strtotime($rCFD{'fhFecha'}));?></td>
			<td nowrap="nowrap" class="sanLR04 columnB"><?=date("d/m/Y", strtotime($rCFD{'fhFechaMovimiento'}));?></td>
			<td nowrap="nowrap" class="sanLR04"><?=utf8_encode((int)$rCFD{'eCodTipoMovimiento'}==1 ? "Nuevo" : "Saldo a Favor");?></td>
			<td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rCFD{'MetodoPago'});?></td>
			<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rCFD{'Agencia'});?></td>
			<td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rCFD{'tReferencia'});?></td>
			<td nowrap="nowrap" class="sanLR04" align="right" width="100%"><?=number_format($rCFD{'dImporte'}, 2);?></td>
		  </tr>
      <?php $i++; } ?>
    </tbody>
  </table>
</div>
<?php }else{ ?>
<script type="text/javascript">
dojo.require("dojo.number");
dojo.require("dijit.form.NumberTextBox");
dojo.require("dijit.form.DateTextBox");
dojo.require("dojo.io.iframe");
function nuevo(){
	document.location = "?ePagina=2.4.6.3.php";
}

dojo.addOnLoad(function(){filtrarConsulta();});
</script>
<form name="Datos" id="Datos" method="post" action="" onsubmit="return false;">
  <input type="hidden" value="0" name="ePagina" id="ePagina" />
  <input type="hidden" value="" name="eAccion" id="eAccion" />
  <input type="hidden" value="<?=(int)$_SESSION['sesionUsuario']['eCodEntidad'];?>" name="eEntidad" id="eEntidad" />
  <table width="965px" border="0">
    <tr>
      <td colpan="3" width="100%"></td>
      <td align="right" nowrap="nowrap"><a class="fntBR11" href="javaScript:mostrarFiltros('Busqueda');">Busqueda de Registros</a></td>
    </tr>
    <tr id="trBusqueda" style="display:none">
      <td colspan="4"><table  width="965px" bgcolor="#f9f9f9">
          <tr>
            <td class="sanLR04" height="5"></td>
          </tr>
          <tr>
            <td class="sanLR04" height="20">Folio</td>
            <td class="sanLR04" width="50%"><input type="text" name="eFolio" dojoType="dijit.form.TextBox" id="eFolio" value="" style="width:80px"></td>
            <td class="sanLR04">Cliente</td>
            <td class="sanLR04" width="50%"><input type="text" name="Cliente" dojoType="dijit.form.TextBox" id="Cliente" value="" style="width:175px"></td>
          </tr>
          <tr>
            <td nowrap="nowrap" class="sanLR04" height="20">Fecha de Facturaci&oacute;n</td>
            <td class="sanLR04" width="50%" >
            <input name="fhFechaFacturacionInicio" id="fhFechaFacturacionInicio" type="date" required="false" style="width:140px;" hasDownArrow="false" displayMessage="false" value="" /> -
             <input name="fhFechaFacturacionFin" id="fhFechaFacturacionFin" type="date"  required="false" style="width:140px;" hasDownArrow="false" displayMessage="false" value=""  />
              </td>
            <td nowrap="nowrap"class="sanLR04">Fecha de Cancelaci&oacute;n</td>
            <td class="sanLR04" width="50%"><input name="fhFechaCancelaInicio" id="fhFechaCancelaInicio" type="date"required="false" style="width:140px;" hasDownArrow="false" displayMessage="false" value="" />
              -
              <input name="fhFechaCancelaFin" id="fhFechaCancelaFin" type="date"  required="false" style="width:140px;" hasDownArrow="false" displayMessage="false" value="" />
            </td>
          </tr>
          <tr>
            <td class="sanLR04" height="20">Solicitud</td>
            <td class="sanLR04" width="50%"><input type="text" name="eSolicitud" dojoType="dijit.form.TextBox" id="eSolicitud" value="" style="width:80px"></td>
            <td class="sanLR04">Agencia Aduanal</td>
            <td class="sanLR04" width="50%"><input type="text" name="Agencia" dojoType="dijit.form.TextBox" id="Agencia" value="" style="width:175px"></td>
          </tr>
          <tr>
            <td nowrap="nowrap" class="sanLR04" height="20">Estatus</td>
            <td class="sanLR04" width="50%">
              <select name='Estatus' id='Estatus' style="width:175px">
                <option value=''>Seleccione...</option>                
                <option value='AU'> Activo </option>
                <option value='CA'> Cancelado </option>
                <option value='PR'> Proceso </option>                
              </select>
            </td>
            <td nowrap="nowrap" class="sanLR04">Metodo de Pago</td>
            <td class="sanLR04" width="50%"><select name='MetodoPago' id='MetodoPago' style="width:175px">
                <option value=''>Seleccione...</option>
                <?php $sel = mysqli_query($link,"SELECT eCodMetodoPago, tNombre FROM catmetodospago where tCodEstatus='AC'");                
                              while($row = mysqli_fetch_array($sel,MYSQLI_ASSOC)){ ?>
                <option value='<?php echo $row["tNombre"]; ?>'> <?php echo $row["tNombre"]; ?> </option>
                <?php } ?>
              </select></td>
          </tr>          
          <tr>
            <td class="sanLR04" height="5"></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td colspan="4"><div id="dvCNS"></div></td>
    </tr>
  </table>
</form>
<?php } ?>