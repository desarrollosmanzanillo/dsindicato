﻿<?php require_once("conexion/soluciones-mysql.php");
require_once("conexion/facturacion.php");
$link = getLink();
$facturacion = new cFacturacion();
if($_POST){
	if($_POST['eProceso']==1){
		$exito				= 0;
		//
		
		$eUsuario				= ($_POST['eUsuario']			? (int)$_POST['eUsuario']										: "NULL");
		$dImporte				= ($_POST['dImporte']			? str_replace(",", "", (float)$_POST['dImporte'])				: "NULL");
		$tReferencia			= ($_POST['tReferencia']			? "'".trim(utf8_decode(addslashes($_POST['tReferencia'])))."'"	: "NULL");
		$eCodMetodoPago			= ($_POST['eCodMetodoPago']		? (int)$_POST['eCodMetodoPago']									: "NULL");
		$eCodTipoMovimiento		= ($_POST['chkTipoMovimiento']	? (int)$_POST['chkTipoMovimiento']								: "NULL");
		$eCodAgenciaAduanal		= ($_POST['eCodAgenciaAduanal']	? (int)$_POST['eCodAgenciaAduanal']								: "NULL");
		$eCodCuentaBancaria		= ($_POST['eCodCuentaBancaria']	? (int)$_POST['eCodCuentaBancaria']								: "NULL");
		$fhFechaNotificacion	= ($_POST['fhFechaNotificacion']	? "'".trim(utf8_decode(addslashes($_POST['fhFechaNotificacion'])))."'"	: "NULL");
		
		$insert=" INSERT INTO openotificacionespagos (tCodEstatus, eCodUsuario, fhFecha, fhFechaMovimiento, eCodTipoMovimiento, ".
				" eCodAgenciaAduanal, eCodMetodoPago, eCodCuentaBancaria, dImporte, tReferencia) ".
				" VALUES ('PA', ".$eUsuario.", CURRENT_TIMESTAMP, ".$fhFechaNotificacion.", ".$eCodTipoMovimiento.", ".
				$eCodAgenciaAduanal.", ".$eCodMetodoPago.", ".$eCodCuentaBancaria.", ".$dImporte.", ".$tReferencia.") ";
		if($res=mysqli_query($link,$insert)){
			$exito=1;
			$select=" select last_insert_id() AS Llave ";
			$rCodigo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
			$eCodNotificacionPago=(int)$rCodigo['Llave'];
			foreach($_POST as $k => $v){
				$nombre = strval($k);
				if(strstr($nombre,"eCodCFD") && $v){
					$dAbonado	= ($_POST['dPagado'.$v]	? str_replace(",", "", (float)$_POST['dPagado'.$v])	: "NULL");
					$update=" UPDATE opecfds ".
							" SET dSaldo=dSaldo-".$dAbonado.
							" WHERE eCodCFD=".(int)$v;
					if($res=mysqli_query($link,$update)){
						$insert=" INSERT INTO relnotificacionespagoscfds (eCodNotificacionPago, eCodCFD, dAbonado) ".
								" VALUES (".$eCodNotificacionPago.", ".$v.", ".$dAbonado.") ";
						if($res=mysqli_query($link,$insert)){
						}else{
							$exito=0;
						}
					}else{
						$exito=0;
					}
				}
			}
		}else{
			$exito=0;
		}
		
		print "<input type=\"text\" value=\"".((int)$exito>0 ? $eCodNotificacionPago : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}

	if($_POST['eProceso']==2){ 
		$select=" SELECT oc.*, cea.tNombre AS Agente ".
				" FROM opecfds oc ".
				" INNER JOIN catentidades cea ON cea.eCodEntidad = oc.eCodCliente ".
				" WHERE oc.tCodEstatus='AU' AND oc.dSaldo>0 ".
				" AND oc.eCodCliente=".(int)$_POST['eCodAgenciaAduanal'].
				" ORDER BY oc.dSaldo ";
		$rsFacturas=mysqli_query($link,$select);
		if((int)mysqli_num_rows($rsFacturas)>0){ ?>
            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                <thead>
                    <tr class="thEncabezado">
                        <td nowrap="nowrap" class="sanLR04" height="20" align="center">S</td>
                        <td nowrap="nowrap" class="sanLR04">C&oacute;digo</td>
                        <td nowrap="nowrap" class="sanLR04">Fecha</td>
                        <td nowrap="nowrap" class="sanLR04" width="100%">Cliente</td>				
                        <td nowrap="nowrap" class="sanLR04">Importe</td>
                        <td nowrap="nowrap" class="sanLR04">Saldo</td>
                    </tr>
                </thead>
                <tbody>
                    <? while($rCFD=mysqli_fetch_array($rsFacturas,MYSQLI_ASSOC)){
                        $i=$rCFD{'eCodCFD'}; ?>
                        <tr>
                            <td height="20" nowrap="nowrap" class="sanLR04" align="center" valign="top">
                                <input type="checkbox" name="eCodCFD<?=$i?>" id="eCodCFD<?=$i?>" value="<?=$i?>" onClick="pagarCFD(<?=$i?>);">
                                <input type="hidden" id="dPagado<?=$i?>" name="dPagado<?=$i?>" value="">
                                <input type="hidden" id="dSaldo<?=$i?>" name="dSaldo<?=$i?>" value="<?=$rCFD{'dSaldo'};?>">
                            </td>
                            <td nowrap="nowrap" class="sanLR04 columnB" valign="top"><b><?=$rCFD{'tSerie'};?><?=sprintf("%07d",$rCFD{'eFolio'});?></b></a></td>
                            <td nowrap="nowrap" class="sanLR04" valign="top"><?=date("d/m/Y H:i", strtotime($rCFD{'fhFecha'}));?></td>
                            <td class="sanLR04 columnB" valign="top"><?=utf8_encode($rCFD{'Agente'});?></td>
                            <td nowrap="nowrap" class="sanLR04" align="right" valign="top"><?=number_format($rCFD{'dTotal'}, 2);?></td>
                            <td nowrap="nowrap" class="sanLR04 columnB" align="right" valign="top"><label id="lblSaldo<?=$i?>"><?=number_format($rCFD{'dSaldo'}, 2);?></label></td>
                        </tr>
                    <? } ?>
                </tbody>
            </table>
		<? }else{
			print "No existen facturas";
		}
	}
//	
}
if(!$_POST){ 
$select=" SELECT * ".
		" FROM catmetodospago ".
		" WHERE tCodEstatus='AC' ".
		" ORDER BY tNombre ";
$rsMetodosPagos=mysqli_query($link,$select);

$select=" SELECT * ".
		" FROM catentidades ".
		" WHERE tCodEstatus='AC' ".
		" AND eCodTipoEntidad=1 ".
		" ORDER BY tNombre ";
$rsClientes=mysqli_query($link,$select);

$select=" SELECT * ".
		" FROM catcuentasbancarias ".
		" ORDER BY tNombre "; 
$rsCuentasBancarias=mysqli_query($link,$select); ?>
<style>
	.eNumero{
		text-align:right;
	}
</style>
<script type="text/javascript">
dojo.require("dojo.number");
dojo.require("dijit.form.NumberTextBox");
dojo.require("dijit.form.DateTextBox");
dojo.require("dojo.io.iframe");

function guardar(){
	dojo.byId('eProceso').value=1;
	var bandera = false;
	var bServicios = false;
	var bMercancias = false;
	var eTipoSolicitud = 0;
	mensaje = "¡Verifique lo siguiente!\n";

	var ultimafila=0;
	dojo.query("[id*=\"eCodCFD\"]").forEach(function(nodo, index, array){
		ultimafila++;
	});
	
	if (!dojo.byId("fhFechaNotificacion").value){
		mensaje+="* Fecha de Movimiento\n";
		bandera = true;		
	}
	if (!dojo.byId("eCodAgenciaAduanal").value){
		mensaje+="* Agencia Aduanal\n";
		bandera = true;		
	}
	if (!dojo.byId("eCodMetodoPago").value){
		mensaje+="* Método de Pago\n";
		bandera = true;		
	}
	if (!dojo.byId("eCodCuentaBancaria").value){
		mensaje+="* Cuenta\n";
		bandera = true;		
	}
	if (!dojo.byId("dImporte").value){
		mensaje+="* Importe\n";
		bandera = true;		
	}
	if(parseInt(ultimafila)>0){
		ultimafila=0;
		dojo.query("[id*=\"eCodCFD\"]:checked").forEach(function(nodo, index, array){
			ultimafila++;
		});
		if(parseInt(ultimafila)<1){
			mensaje+="* Seleccione Facturas\n";
			bandera = true;
		}
	}else{
		mensaje+="* No existen facturas\n";
		bandera = true;
	}

	if (bandera==true){
		alert(mensaje);
	}else{
		if (confirm("¿Desea Guardar la Información?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				if(dojo.byId("eCodigo").value>0){
					alert("¡La información se guardó correctamente!");
					tURL = './?ePagina=2.5.6.2.1.php&eCodNotificacionPago='+dojo.byId("eCodigo").value;
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
		}
	}
}

function cargarCFDs(){
	dojo.byId('eProceso').value=2;
	dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
		dojo.byId("dvFacturas").innerHTML = tRespuesta;
	}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});
}

function pagarCFD(eFilaCFD){
	var eFilaCFD	= (parseInt(eFilaCFD)>0 ? eFilaCFD : 0);
	var dImporte	= dojo.byId('dImporte').value.replace(/,/ig,'');
	var dAsignado	= 0;
	var dPorAsignar	= 0;
	var dDisponible	= 0;
	if(parseInt(eFilaCFD)>0 && parseFloat(dImporte)>0){
		dojo.query("[id*=\"eCodCFD\"]:checked").forEach(function(nodo, index, array){
			if(nodo.value!=eFilaCFD){
				dAsignado+=parseFloat(dojo.byId('dPagado'+nodo.value).value);
			}
		});
		dPorAsignar=parseFloat(dImporte)-parseFloat(dAsignado);
		if(parseFloat(dPorAsignar)>0){
			if(dojo.byId('eCodCFD'+eFilaCFD).checked==true){
				if(dojo.byId('dSaldo'+eFilaCFD).value<dPorAsignar){
					dojo.byId('dPagado'+eFilaCFD).value=dojo.byId('dSaldo'+eFilaCFD).value;
					dDisponible=parseFloat(dPorAsignar)-parseFloat(dojo.byId('dSaldo'+eFilaCFD).value);
					dojo.byId('lblSaldo'+eFilaCFD).innerHTML=dojo.number.format(0,{places:2,round:-1});
				}else{
					dojo.byId('dPagado'+eFilaCFD).value=parseFloat(dPorAsignar);
					dDisponible=0;
					dojo.byId('lblSaldo'+eFilaCFD).innerHTML=dojo.number.format(parseFloat(dojo.byId('dSaldo'+eFilaCFD).value)-parseFloat(dPorAsignar),{places:2,round:-1});
				}
			}else{
				dojo.byId('lblSaldo'+eFilaCFD).innerHTML=dojo.number.format(parseFloat(dojo.byId('dSaldo'+eFilaCFD).value),{places:2,round:-1});
				dDisponible=dPorAsignar;
			}
		}else{
			dDisponible=0;
			dojo.byId('eCodCFD'+eFilaCFD).checked=false;
			alert("¡Se ha asignado el total del importe!");
		}
	}else{
		if(parseFloat(dojo.byId('dImporte').value.replace(/,/ig,''))>0){
			dojo.byId('dImporte').value=dojo.number.format(parseFloat(dojo.byId('dImporte').value.replace(/,/ig,'')),{places:2,round:-1});
			dImporte=parseFloat(dojo.byId('dImporte').value.replace(/,/ig,''));
		}else{
			dojo.byId('dImporte').value=dojo.number.format(0,{places:2,round:-1});
			dImporte=0;
		}
		
		dDisponible=dImporte;
		dojo.query("[id*=\"eCodCFD\"]").forEach(function(nodo, index, array){
			nodo.checked=false;
			dojo.byId('lblSaldo'+nodo.value).innerHTML=dojo.number.format(dojo.byId('dSaldo'+nodo.value).value,{places:2,round:-1});
		});
	}
	dojo.byId('dRestante').value=dDisponible;
	dojo.byId('lblRestante').innerHTML=dojo.number.format(dDisponible,{places:2,round:-1});
}

/*function sumar(filaConcepto){
	var eNumeroPiezas=0;
	var dSubtotal=0;
	var dIVA=0;
	dojo.query("[id^=\"eCantidad\"]").forEach(function(nodo, index, array){
		var bNumero=0;
		var bPeso=0;
		var eNumero=parseInt(nodo.value.replace(/,/ig,''));
		var eFilaConcepto=parseInt(nodo.id.replace('eCantidad',''));
		if(eNumero>=0){
			nodo.value=dojo.number.format(eNumero,{places:0,round:0});
			bNumero=1;
		}else{
			nodo.value="";
			eNumero=0;
		}
		var dPrecio=parseFloat(dojo.byId('dPrecio'+eFilaConcepto).value.replace(/,/ig,''));
		if(dPrecio>=0){
			dojo.byId('dPrecio'+eFilaConcepto).value=dojo.number.format(dPrecio,{places:2,round:0});
			bPeso=1;
		}else{
			dPrecio=0;
		}
		if(bNumero==1 && bPeso==1){
			var dTotalConcepto=(parseFloat(eNumero)*parseFloat(dPrecio));
			dojo.byId('dImporte'+eFilaConcepto).value=dojo.number.format(dTotalConcepto,{places:2,round:0});
			dSubtotal+=parseFloat(dTotalConcepto);
		}
	});
	dIVA=(parseFloat(dSubtotal)*.16);
	dojo.byId('lblSubtotal').innerHTML=dojo.number.format(dSubtotal,{places:2,round:0});
	dojo.byId('lblIVA').innerHTML=dojo.number.format(dIVA,{places:2,round:0});
	dojo.byId('lblTotal').innerHTML=dojo.number.format(parseFloat(dIVA)+parseFloat(dSubtotal),{places:2,round:0});
}*/

function consultar(){
	document.location = './?ePagina=2.5.6.2.php';
}

dojo.addOnLoad(function(){
});
</script>
<div id="dvCNS" style="display:none;"></div>
<form id="Datos" name="Datos" method="post" onSubmit="return false;" enctype="multipart/form-data">
<input type="hidden" name="eProceso" id="eProceso" value="" />
<input type="hidden" name="eServicio" id="eServicio" value="" />
<input type="hidden" name="eUsuario" id="eUsuario" value="<?=(int)$_SESSION['sesionUsuario']['eCodUsuario'];?>" />
<input type="hidden" name="eCodEntidad" id="eCodEntidad" value="<?=(int)$_SESSION['sesionUsuario']['eCodEntidad'];?>" />
<input type="hidden" name="mail" id="mail" value="<?=$_SESSION['sesionUsuario']['Correo'];?>" />
<input type="hidden" name="nombreusuario" id="nombreusuario" value="<?=$_SESSION['sesionUsuario']['Usuario'];?>" />
<input type="hidden" name="dRestante" id="dRestante" value="">
<table border="0" cellpadding="0" cellspacing="0" width="980px">
	 <tr>
        <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" width="16" height="16" align="absmiddle"> Fecha de Movimiento</td>
        <td nowrap class="sanLR04" colspan="3" width="700px">
        	<input name="fhFechaNotificacion" id="fhFechaNotificacion" type="date" value="<?=date("Y-m-d");?>" />
        </td>
    </tr>
    <tr>
        <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" width="16" height="16" align="absmiddle"> Tipo de Movimiento</td>
        <td nowrap class="sanLR04" colspan="3" width="700px">
        	<label><input type="radio" id="chkTipoMovimiento" name="chkTipoMovimiento" value="1" checked/>Nuevo</label>
            <label><input type="radio" id="chkTipoMovimiento" name="chkTipoMovimiento" value="2"/>Saldo a Favor</label>
        </td>
    </tr>
     <tr>
        <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Agencia Aduanal</td>
        <td nowrap class="sanLR04" colspan="3">
        	<select name="eCodAgenciaAduanal" id="eCodAgenciaAduanal" style="width:175px" onchange="cargarCFDs();">
                <option value="">Seleccione...</option>
                <?php while($rCliente=mysqli_fetch_array($rsClientes,MYSQLI_ASSOC)){ ?>
                    <option value="<?=$rCliente{'eCodEntidad'}?>"><?=utf8_encode($rCliente{'tNombre'})?></option>
                <?php } ?>
            </select>
        </td>
    </tr>
    <tr>
        <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" width="16" height="16" align="absmiddle"> M&eacute;todo de Pago</td>
        <td nowrap class="sanLR04" colspan="3">
			<select name="eCodMetodoPago" id="eCodMetodoPago" style="width:175px">
                <option value="">Seleccione...</option>
                <?php while($rMetodoPago=mysqli_fetch_array($rsMetodosPagos,MYSQLI_ASSOC)){ ?>
                    <option value="<?=$rMetodoPago{'eCodMetodoPago'}?>"><?=($rMetodoPago{'tNombre'})?></option>
                <?php } ?>
            </select>
        </td>
    </tr>
    <tr>
        <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" width="16" align="absmiddle"> Cuenta</td>
        <td nowrap class="sanLR04" colspan="3">
        	<select name="eCodCuentaBancaria" id="eCodCuentaBancaria" style="width:175px">
                <option value="">Seleccione...</option>
                <?php while($rCuentaBancaria=mysqli_fetch_array($rsCuentasBancarias,MYSQLI_ASSOC)){ ?>
                    <option value="<?=$rCuentaBancaria{'eCodCuentaBancaria'}?>"><?=utf8_encode($rCuentaBancaria{'tNombre'})?></option>
                <?php } ?>
            </select>
        </td>
    </tr>
    <tr>
        <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" width="16" align="absmiddle"> Importe</td>
        <td nowrap class="sanLR04">
        	<input name="dImporte" type="text" dojoType="dijit.form.TextBox" id="dImporte" value="" style="width:175px" class="numero" onBlur="pagarCFD();">
        </td>
        <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Disponible</td>
        <td nowrap class="sanLR04">
        	<label id="lblRestante">0.00</label>
        </td>
    </tr>
    <tr>
        <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Referencia</td>
        <td nowrap class="sanLR04" colspan="3">
        	<input name="tReferencia" type="text" dojoType="dijit.form.TextBox" id="tReferencia" value="" style="width:175px">	
        </td>
    </tr>
    <tr><td height="10"></td></tr>
	<?php mysqli_data_seek($rsServicios,0);?>
    <tr>
    	<td height="23" nowrap class="sanLR04" valign="top"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Facturas</td>
        <td colspan="3" id="dvFacturas">No existen facturas</td>
    </tr>
	<tr><td height="20"></td></tr>
    <tr><td valign="top" nowrap="nowrap" class="sanLR04" colspan="4"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle" /> <font class="fntG10K">Indica que es obligatorio llenar el campo.</font></td></tr>
</table>
</form>
<?php } ?>