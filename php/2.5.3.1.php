<style>
#divgral {
     overflow:scroll;
     height:450px;
     width:100%;
}
</style>
<?php require_once("conexion/soluciones-mysql.php"); 
$link = getLink();
if($_POST){
	if ($_POST['fhFechaAsistenciaInicio']!=""){
  		$fhFechaAsistenciaInicio = $_POST['fhFechaAsistenciaInicio'].' 00:00:00';
  		$fhFechaAsistenciaFin = ($_POST['fhFechaAsistenciaFin']!="" ? $_POST['fhFechaAsistenciaFin'] : $_POST['fhFechaAsistenciaInicio']).' 23:59:59';
 	}

	$select=" SELECT chek.id AS numeral, chek.Estado, chek.Turno, chek.Fecha, chek.idEmpleado, CAT.indice, chek.eCodMotivoAsistencia, empl.eCodCategoriaVacacion
		   FROM asistencias as chek
		   INNER JOIN categorias AS CAT ON chek.idCategorias = CAT.indice
		   INNER JOIN empleados AS empl ON empl.id=chek.idEmpleado
		   WHERE chek.Estado='AC' AND CAT.bOmitirIncidencia IS NULL ".
			($_POST['eCodIncidencia']			? " AND chek.idEmpleado=".$_POST['eCodIncidencia']				: "").
			($_POST['eCodEntidad'] 				? " AND chek.idEmpresa=".(int)$_POST['eCodEntidad'] 			: "").
			($_POST['fhFechaAsistenciaInicio']	? " AND chek.Fecha BETWEEN '".$fhFechaAsistenciaInicio. "' AND '" .$fhFechaAsistenciaFin."'" : "" ).
			" Order by chek.id ".($_POST['orden'] ? $_POST['orden'] : "ASC").
			" LIMIT ".( $_POST['limite'] ? $_POST['limite']  : "100" );
	if($_POST['eAccion']==888){
		print $select;
	}
	$rsEntidades=mysqli_query($link,$select); 
	$registros=(int)mysqli_num_rows($rsEntidades); ?>
    <table cellspacing="0" border="0" width="100%">
    <tr>
	    <td width="50%"><hr color="#666666" /></td>
	    <td class="sanLR04" nowrap="nowrap"><b>Registros Encontrados: ( <?=$registros;?> )</b></td>
		<td width="50%"><hr color="#666666" /></td>
    </tr>
    </table>
	<div id="divgral" style="display:block; top:0; left:0; width:100%; z-index=1; overflow: auto;">
	<table cellspacing="0" border="0" width="100%" >		
		<thead>
			<tr class="thEncabezado">				
				<td nowrap="nowrap" class="sanLR04">E</td>
				<td nowrap="nowrap" class="sanLR04">Empleado</td>
                <td nowrap="nowrap" class="sanLR04">Fecha</td>
				<td nowrap="nowrap" class="sanLR04" width="100%">Cat/Turno</td>
			</tr>
		</thead>
		<tbody>
			<?php $i=1; while($rEntidad=mysqli_fetch_array($rsEntidades,MYSQLI_ASSOC)){ 
				$cve_catturno='';
	          	$cve_catturno=sprintf("%02d",($rEntidad{'eCodMotivoAsistencia'}==3 ? $rEntidad{'eCodCategoriaVacacion'} : $rEntidad{'indice'})).$rEntidad{'Turno'}; ?>
				<tr>					
					<td nowrap="nowrap" class="sanLR04"><img width="16" height="16" alt="Usuario" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-<?=$rEntidad{'Estado'};?>.png"></td>
					<td nowrap="nowrap" class="sanLR04 colmenu" ><b><?=sprintf("%07d",$rEntidad{'idEmpleado'});?></b></td>
					<td nowrap="nowrap" class="sanLR04"><?=date("d/m/Y", strtotime($rEntidad{'Fecha'}));?></td>
                    <td nowrap="nowrap" class="sanLR04 columnB" width="100%"><?=utf8_encode($cve_catturno);?></a></td>					
				</tr>
			<?php $i++; } ?>
		</tbody>
	</table>
	</div>
<?php }else{ 
$select=" SELECT tSiglas, eCodEntidad
        FROM catentidades
        WHERE bPrincipal IS NOT NULL
        ORDER BY tSiglas ASC ";
$rsEmpresas=mysqli_query($link,$select);

	?>	
<script type="text/javascript">
	dojo.require("dojo.number");
	dojo.require("dijit.form.NumberTextBox");
	dojo.require("dijit.form.DateTextBox");
	dojo.require("dojo.io.iframe");
	
	function generaExcel(){
		var UrlExcel = "php/excel/2.5.3.1.php?"+
		(dojo.byId('eCodIncidencia').value ? "&eCodIncidencia="+dojo.byId('eCodIncidencia').value : "")+
		(dojo.byId('eCodEntidad').value ? "&eCodEntidad="+dojo.byId('eCodEntidad').value : "")+
		(dojo.byId('fhFechaAsistenciaInicio').value ? "&fhFechaAsistenciaInicio="+dojo.byId('fhFechaAsistenciaInicio').value : "")+
		(dojo.byId('fhFechaAsistenciaFin').value ? "&fhFechaAsistenciaFin="+dojo.byId('fhFechaAsistenciaFin').value : "")+		
		(dojo.byId('orden').value ? "&orden="+dojo.byId('orden').value : "")+
		(dojo.byId('limite').value ? "&limite="+dojo.byId('limite').value : "");		
		var Datos = document.createElement("FORM");
		document.body.appendChild(Datos);
		Datos.name='Datos';
		Datos.method = "POST";
		Datos.action = UrlExcel;
		Datos.submit();
		}

	dojo.addOnLoad(function(){ filtrarConsulta();});
</script>
<form name="Datos" id="Datos" method="post" action="" onsubmit="return false;">
	<input type="hidden" value="0" name="ePagina" id="ePagina" />
	<input type="hidden" value="" name="eAccion" id="eAccion" />
	<table width="100%" border="0">
		<tr>
			<td colpan="3" width="100%"></td>
			<td align="right" nowrap="nowrap"><a class="fntBR11" href="javaScript:mostrarFiltros('Busqueda');">Busqueda de Registros</a></td>
		</tr>
		<tr id="trBusqueda" style="display:none">
			<td colspan="4">
				<table width="100%" bgcolor="#f9f9f9">
					<tr><td class="sanLR04" height="5"></td></tr>
					<tr>
						<td class="sanLR04" height="20">C&oacute;digo</td>
						<td class="sanLR04" width="40%"><input type="text" name="eCodIncidencia" dojoType="dijit.form.TextBox" id="eCodIncidencia" value="" style="width:80px; height:25px;"></td>
						<td class="sanLR04" nowrap="nowrap">Fecha</td>
						<td class="sanLR04" width="50%" nowrap="nowrap"><input name="fhFechaAsistenciaInicio" id="fhFechaAsistenciaInicio" type="date" required="false"/> -	<input name="fhFechaAsistenciaFin" id="fhFechaAsistenciaFin" type="date" required="false"/></td>
					</tr>
					<tr>
						<td class="sanLR04" height="20" nowrap>Empresa</td>
			            <td class="sanLR04" width="50%">
			              <select name="eCodEntidad" id="eCodEntidad" style="width:180px; height:25px;">
			                  <option value="">Seleccione...</option>
			                  <?php while($rEmpresa=mysqli_fetch_array($rsEmpresas,MYSQLI_ASSOC)){ ?>
			                      <option value="<?=$rEmpresa{'eCodEntidad'}?>"><?=utf8_encode($rEmpresa{'tSiglas'})?></option>
			                  <?php } ?>
			                </select>
			            </td>
			            <td class="sanLR04" height="20"></td>
						<td class="sanLR04" width="50%"></td>
					</tr>					
			        <tr>
		              <td nowrap="nowrap" class="sanLR04" height="5"><b>[Ordenamiento]</b></td>
		              <td nowrap="nowrap" class="sanLR04" width="50%"><input type="radio" name="orden" id="orden" value="ASC" > ASCENDENTE / <input type="radio" name="orden" id="orden" value="DESC" checked="checked" > DESENDENTE</td>
		              <td nowrap="nowrap" class="sanLR04"><b>[Maximos Registros]:</b></td>
		              <td nowrap="nowrap" class="sanLR04" width="50%">
		              	<select name="limite" id="limite" style="width:175px">
                              <option value="100">100</option>
                              <option value="1000">1,000</option>
                              <option value="10000">10,000</option>
                              <option value="100000">100,000</option>
                      	</select>
                      </td>
        			</tr>
					<tr><td class="sanLR04" height="5"></td></tr>
				</table>
			</td>
		</tr>
		<tr><td colspan="4"><div id="dvCNS"></div></td></tr>
	</table>
</form>	
<?php } ?>