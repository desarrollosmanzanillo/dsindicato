

<!-- Bootstrap core CSS -->
<link href="dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="dist/css/carousel.css" rel="stylesheet">
<link href="dist/css/dashboard.css" rel="stylesheet">
<link href="dist/css/cover.css" rel="stylesheet">


<style type="text/css">
<!--
.style1 {
	font-size: 24px;
	COLOR: #2A3F55;
	TEXT-ALIGN: center
}
.style2 {
	font-size: 20px;
	COLOR: #F96F09;
	TEXT-ALIGN: center;
	font: Arial, Helvetica, sans-serif
}
.style3 {font-size: 24px; 
background-color:F96F09;
  color:FFF;
  text-align: justify
}

.style4 {

font-size: 16px;
color: #000000


}
-->
</style>
<br>
	
<div class="container">
<!-- Carousel
		================================================== -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
    <li data-target="#myCarousel" data-slide-to="3"></li>
    <li data-target="#myCarousel" data-slide-to="4"></li>
    <li data-target="#myCarousel" data-slide-to="5"></li>
    <li data-target="#myCarousel" data-slide-to="6"></li>
    <li data-target="#myCarousel" data-slide-to="7"></li>
    <li data-target="#myCarousel" data-slide-to="8"></li>


  </ol>
  <div class="carousel-inner">
    <div class="item active">
      <img src="img/1.jpg" alt="First slide"  />
      <div class="container">
          <div class="carousel-caption">
            <h5>
            <span>Carga y Descarga de Contenedores Llenos</span>
            </h5>            
          </div>
      </div>
    </div>
    <div class="item"> <img src="img/2.jpg" alt="Second slide" />
        <div class="container">
          <div class="carousel-caption">
          <h5>
            <span>Carga y Descarga de Contenedores Vac&iacute;os </span>
            </h5>
          </div>
        </div>
    </div>
    <div class="item"> <img src="img/3.jpg" alt="Third slide" />
        <div class="container">
          <div class="carousel-caption">
            <h5>
            <span>Almacenaje de contenedores vac&iacute;os</span>
            </h5>
          </div>
        </div>
    </div>
    <div class="item"> <img src="img/4.jpg" alt="Fourth slide" />
        <div class="container">
          <div class="carousel-caption">
            <h5>
            <span>Reparaci&oacute;n de contenedores bajo norma IICL Y UCIRC</span>
            </h5>
          </div>
        </div>
    </div>
    <div class="item"> <img src="img/5.jpg" alt="fifth slide" />
        <div class="container">
          <div class="carousel-caption">
            <h5>
            <span>Lavado de contenedores bajo norma IICL Y UCIRC</span>
            </h5>
          </div>
        </div>
    </div>
    <div class="item"> <img src="img/6.jpg" alt="sixth slide" />
        <div class="container">
          <div class="carousel-caption">
            <h5>
            <span>Desconsolidaci&oacute;n y Consolidaci&oacute;n de Contenedores</span>
            </h5>
          </div>
        </div>
    </div>
    <div class="item"> <img src="img/7.jpg" alt="septimo slide" />
        <div class="container">
          <div class="carousel-caption">
            <h5>
            <span>Maniobras de Carga y  Descarga de Carga General</span>
            </h5>
          </div>
        </div>
    </div>
    <div class="item"> <img src="img/8.jpg" alt="octavo slide" />
        <div class="container">
          <div class="carousel-caption">
            <h5>
            <span>Almacenaje de Contenedores Llenos</span>
            </h5>
          </div>
        </div>
    </div>
	<div class="item"> <img src="img/9.jpg" alt="noveno slide" />
        <div class="container">
          <div class="carousel-caption">
            <h5>
            <span>Almacenaje de Carga General</span>
            </h5>
          </div>
        </div>
    </div>
    
  </div>
<a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a> <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a> </div>
<!-- /.carousel -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="dist/js/bootstrap.min.js"></script>
<script src="dist/js/docs.min.js"></script>