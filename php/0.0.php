<!doctype html>  
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="icon" href="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/favicon.ico" type="image/x-icon"/> 
    <link rel="stylesheet" type="text/css" href="../css/styles.css" />
    <link type="text/css" href="../css/css3.css" rel="stylesheet" />
    <script type="text/javascript" src="../js/jquery-1.6.js"></script>
    <script type="text/javascript" src="../js/jquery.pikachoose.js"></script>
    <script language="javascript">
        $(document).ready(
            function (){
                //$("#pikame").PikaChoose();
            });
    </script>
</head>
<body>
    <div id="container">
        <header>
            <a href="#" id="logo"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/LogoEmpresa.png" width="221" height="100" alt=""/></a>    
            <nav>
            	<div id='cssmenu' style="position:absolute; z-index:10;">
                <ul>
                    <li><a href="1.1.php" class="current">[Bienvenido]</a></li>
              <li><a href="1.2.php">Historia</a>
                    	<ul>
                            <li><a href='1.2.1.php'><span>¿Quienes somos?</span></a></li>
                            <li><a href='1.2.2.php' class="middle"><span>Misi&oacute;n/Visi&oacute;n</span></a></li>
                            <li class="caption"><a href='1.2.3.php' class='last'><span>Pol&iacute;tica de Calidad</span></a></li>
						</ul>
                    </li>
              <li class='has-sub'><a href="1.3.php">Servicios</a>
                    	<ul>
                            <li><a href='1.3.1.php'><span>Deposito</span></a></li>
                            <li><a href='1.3.2.php' class='last'><span>Transporte</span></a></li>
						</ul>
                    </li>
              <li><a href="1.4.php">Infraestructura</a>
                    	<ul>
                        	<li><a href="1.4.1.php"><span>Ubicaci&oacute;n</span></a></li>
                            <li><a href="1.4.2.php" class="middle"><span>Equipo</span></a></li>
                            <li><a href="1.4.3.php" class='last'><span>Instalaciones</span></a></li>
                        </ul>
                    </li>
                    
                  <li><a href="1.5.php">Servicios en L&iacute;nea</a></li>
                </ul>
                </div>
            </nav>
        </header>
</div>
    <!--end container-->
   
   <!--start footer-->
</body>
</html>
