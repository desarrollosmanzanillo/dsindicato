<?php require_once("conexion/soluciones-mysql.php");  
$link = getLink();
if($_POST){
	if($_POST['eProceso']==1){
		$exito			   = 0;
		$tNombre		   = ($_POST['tNombre']			? "'".trim(utf8_decode($_POST['tNombre']))."'"		: "NULL");
		$tNombreCorto	   = ($_POST['tNombreCorto']	? "'".trim(utf8_decode($_POST['tNombreCorto']))."'"	: "NULL");
		$eCodTipoEntidad   = ($_POST['eCodTipoEntidad'] ? (int)$_POST['eCodTipoEntidad']					: "NULL");
		$eCodCausa  	   = ($_POST['eCodCausabaja'] 		? (int)$_POST['eCodCausabaja']							: "NULL");
		$dFactorIntegracion= $_POST['dFactorIntegracion'];
		$eCodTipoSalario   = $_POST['eCodTipoSalario'];
		$eCodTipoJornada   = $_POST['eCodTipoJornada'];
		$eCodTipoTrabajador= $_POST['eCodTipoTrabajador'];

		if((int)$eCodTipoEntidad>0){
			$update=" UPDATE cattipoempleado ".
					" SET tNombre=".$tNombre.",".
					" tNombreCorto=".$tNombreCorto.",".
					" dFactorIntegracion=".$dFactorIntegracion.",".
					" eCodTipoSalario=".$eCodTipoSalario.",".
					" eCodTipoJornada=".$eCodTipoJornada.",".
					" eCodCausaBaja=".$eCodCausa.",".					
					" eCodTipoTrabajador=".$eCodTipoTrabajador.					
					" WHERE eCodTipoEntidad=".$eCodTipoEntidad;
			if($res=mysqli_query($link,$update)){
				$exito=1;
			}else{
				$exito=0;
			}
		}else{
			$insert=" INSERT INTO cattipoempleado(tCodEstatus, tNombre, tNombreCorto, dFactorIntegracion,  eCodTipoSalario ,  eCodTipoJornada , eCodCausaBaja, eCodTipoTrabajador ) ".
					" VALUES ('AC', ".$tNombre.", ".$tNombreCorto." , ".$dFactorIntegracion." ,".$eCodTipoSalario." ,".$eCodTipoJornada." , ".$eCodCausa." , ".$eCodTipoTrabajador.")";
			if($res=mysqli_query($link,$insert)){
				$exito=1;
				$select=" select last_insert_id() AS Llave ";
				$rCodigo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
				$eCodTipoEntidad=(int)$rCodigo['Llave'];
			}else{
				$exito=0;
			}
		}
		print "<input type=\"text\" value=\"".($exito==1 ? $eCodTipoEntidad : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}
	
	if($_POST['eProceso']==2){
		$rTipo=mysqli_fetch_array(mysqli_query($link,"SELECT 1 AS TipoEntidad FROM cattipoempleado WHERE tNombre='".trim($_POST['tNombre'])."' AND eCodTipoEntidad!=".(int)$_POST['eCodTipoEntidad']),MYSQLI_ASSOC);
		print "<input name=\"eTipoEntidad\" id=\"eTipoEntidad\" value=\"".(int)$rTipo{'TipoEntidad'}."\">";
	}
}
if(!$_POST){
	
$select=" SELECT * ".
		" FROM cattipoempleado ".
		" WHERE eCodTipoEntidad=".$_GET['eCodTipoEntidad'];
$rTipoEntidad = mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC); 

$select=" SELECT * ".
		" FROM cattiposalario ";
$rsTipoSalario = mysqli_query($link,$select);

$select=" SELECT * ".
		" FROM cattipojornada ";
$rsTipoJornada = mysqli_query($link,$select);

$select=" SELECT * ".
		" FROM cattipotrabajador ";
$rsTipoTrabajador = mysqli_query($link,$select);

$select=" SELECT * ".
		" FROM catcausabaja ";
$rsTipocausa = mysqli_query($link,$select);

?>
<script type="text/javascript">
function guardar(){
	dojo.byId('eProceso').value=1;
	bandera = false;
	mensaje = "¡Verifique lo siguiente!\n";
	if (!dojo.byId("tNombre").value){
		mensaje+="* Nombre\n";
		bandera = true;		
	}
	if (!dojo.byId("tNombreCorto").value){
		mensaje+="* Nombre Corto\n";
		bandera = true;		
	}
	

	if (!dojo.byId("eCodTipoSalario").value){
		mensaje+="* Tipo de Salario\n";
		bandera = true;		
	}

	if (!dojo.byId("eCodTipoJornada").value){
		mensaje+="* Tipo de Jornada\n";
		bandera = true;		
	}

	if (!dojo.byId("eCodTipoTrabajador").value){
		mensaje+="* Tipo de Trabajador\n";
		bandera = true;		
	}

	if (!dojo.byId("eCodCausabaja").value){
		mensaje+="* Causa de Baja\n";
		bandera = true;		
	}

	if (!dojo.byId("dFactorIntegracion").value){
		mensaje+="* Factor de Integracion\n";
		bandera = true;		
	}


	if (bandera==true){
		alert(mensaje);		
	}else{
		if (confirm("¿Desea Guardar la Información?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				if(dojo.byId("eCodigo").value>0){
					alert("¡La información se guardó correctamente!");
					tURL = './?ePagina=2.3.2.3.2.php&eCodTipoEntidad='+dojo.byId("eCodigo").value;
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
		}
	}
}	

function verificarTipo(){
	if(dojo.byId('tNombre').value){
		dojo.byId('eProceso').value=2;
		dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
			dojo.byId("dvCNS").innerHTML = tRespuesta;
			if(dojo.byId('eTipoEntidad').value==1){
				alert("¡Este tipo de Empleado ya existe!");
				dojo.byId('tNombre').value="";
			}
		}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
	}
}

function consultar(){
	document.location = './?ePagina=2.3.2.3.php';
}
</script>
<div id="dvCNS" style="display:none;"></div>
<form action="" method="post" name="Datos" id="Datos" onsubmit="return false;">
<input type="hidden" name="eProceso" id="eProceso" value="" />
<input type="hidden" name="eCodTipoEntidad" id="eCodTipoEntidad" value="<?=$_GET['eCodTipoEntidad'];?>" />
<table border="0" cellpadding="0" cellspacing="0">
    <tr><td height="20"></td></tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Nombre </td>
	    <td width="50%" nowrap class="sanLR04">
           	<input name="tNombre" type="text" dojoType="dijit.form.TextBox" id="tNombre" value="<?=utf8_encode($rTipoEntidad{'tNombre'});?>" style="width:175px" onblur="verificarTipo();">
        </td>
     </tr>
     <tr>
		<td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Nombre Corto </td>
	    <td width="50%" nowrap class="sanLR04">
           	<input name="tNombreCorto" type="text" dojoType="dijit.form.TextBox" id="tNombreCorto" value="<?=utf8_encode($rTipoEntidad{'tNombreCorto'});?>" style="width:175px;">
        </td>
     </tr>
     <tr>
        <td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Tipo de Salario </td>
	    <td width="50%" nowrap class="sanLR04">
	    	<select name="eCodTipoSalario" id="eCodTipoSalario" style="width:175px">
				<option value="">Seleccione...</option>
				<?php while($rTipoSalario=mysqli_fetch_array($rsTipoSalario,MYSQLI_ASSOC)){ ?>
					<option value="<?=$rTipoSalario{'eCodSalario'}?>" <?=($rTipoSalario{'eCodSalario'}==$rTipoEntidad{'eCodTipoSalario'} ? "selected='selected'" : "");?> ><?=utf8_encode($rTipoSalario{'tNombre'})?></option>
				<?php } ?>
			</select>           	
        </td>
    </tr>
    <tr>
        <td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Tipo De Jornada </td>
	    <td width="50%" nowrap class="sanLR04">
	    	<select name="eCodTipoJornada" id="eCodTipoJornada" style="width:175px">
				<option value="">Seleccione...</option>
				<?php while($rTipoJornada=mysqli_fetch_array($rsTipoJornada,MYSQLI_ASSOC)){ ?>
					<option value="<?=$rTipoJornada{'eCodJornada'}?>" <?=($rTipoJornada{'eCodJornada'}==$rTipoEntidad{'eCodTipoJornada'} ? "selected='selected'" : "");?> ><?=utf8_encode($rTipoJornada{'tNombre'})?></option>
				<?php } ?>
			</select>           	
        </td>
    </tr>
    <tr>
        <td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Tipo de Trabajador </td>
	    <td width="50%" nowrap class="sanLR04">
	    	<select name="eCodTipoTrabajador" id="eCodTipoTrabajador" style="width:175px">
				<option value="">Seleccione...</option>
				<?php while($rTipoTrabajador=mysqli_fetch_array($rsTipoTrabajador,MYSQLI_ASSOC)){ ?>
					<option value="<?=$rTipoTrabajador{'eCodTrabajador'}?>" <?=($rTipoTrabajador{'eCodTrabajador'}==$rTipoEntidad{'eCodTipoTrabajador'} ? "selected='selected'" : "");?> ><?=utf8_encode($rTipoTrabajador{'tNombre'})?></option>
				<?php } ?>
			</select>           	
        </td>
    </tr>
    <tr>
        <td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Causa para Baja </td>
	    <td width="50%" nowrap class="sanLR04">
	    	<select name="eCodCausabaja" id="eCodCausabaja" style="width:175px">
				<option value="">Seleccione...</option>
				<?php while($rTipocausa=mysqli_fetch_array($rsTipocausa,MYSQLI_ASSOC)){ ?>
					<option value="<?=$rTipocausa{'eCodCausa'}?>" <?=($rTipocausa{'eCodCausa'}==$rTipoEntidad{'eCodCausaBaja'} ? "selected='selected'" : "");?> ><?=utf8_encode($rTipocausa{'tNombre'})?></option>
				<?php } ?>
			</select>           	
        </td>
    </tr>
    <tr>
        <td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Factor de Integraci&oacute;n </td>
	    <td width="50%" nowrap class="sanLR04">
           	<input name="dFactorIntegracion" type="text" dojoType="dijit.form.TextBox" id="dFactorIntegracion" value="<?=utf8_encode($rTipoEntidad{'dFactorIntegracion'});?>" style="width:175px;">
        </td>
    </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle" /> <font class="fntG10K">Indica que es obligatorio llenar el campo.
    </font></td>
    </tr>
</table>
</form>
<?php } ?>