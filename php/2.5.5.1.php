<?php
require_once("conexion/soluciones-mysql.php");
$link = getLink();
if($_POST){

	if ($_POST['fhFechaEntradaInicio']!=""){
  		$fhFechaEntradaInicio = $_POST['fhFechaEntradaInicio'].' 00:00:00';
  		$fhFechaEntradaFin = ($_POST['fhFechaEntradaFin']!="" ? $_POST['fhFechaEntradaFin'] : $_POST['fhFechaEntradaInicio']).' 23:59:59';
 	}

	$select=" SELECT oss.eCodEntrada, oss.eCodSolicitud, oss.fhFechaEntrada, oss.tBL, oss.tCodContenedor, ".
			" cen.tNombre AS Naviera, oss.tPatente, cec.tNombre AS Cliente, cef.tNombre AS FacturarA, ".
			" cbu.tNombre AS Buque, oss.tNumeroViaje, cto.tNombre AS TipoServicio, cto.eCodTipoServicio AS CodTipoServ, ".
			" bei.eCodEIR, oss.eCodTipoServicio, oss.tCodEstatus, ctc.tNombreCorto AS TipoContenedor, ctc.eCodTipoContenedor, ".
			" oss.tObservaciones, osm.fhFechaSalida ".
			" FROM opeentradasmercancias oss ".
			" INNER JOIN catentidades cen ON cen.eCodEntidad=oss.eCodNaviera ".
			" INNER JOIN catentidades cec ON cec.eCodEntidad=oss.eCodCliente ".
			" INNER JOIN catentidades cef ON cef.eCodEntidad=oss.eCodFacturarA ".
			" LEFT JOIN catbuques cbu ON cbu.eCodBuque=oss.eCodBuque ".
			" INNER JOIN cattiposservicios cto ON cto.eCodTipoServicio=oss.eCodTipoServicio ".
			" LEFT JOIN cattiposcontenedores ctc ON ctc.eCodTipoContenedor=oss.eCodTipoContenedor ".
			" LEFT JOIN biteirs bei ON bei.eCodEntrada=oss.eCodEntrada AND bei.eCodTipoEIR=1 ".
			" LEFT JOIN opesalidasmercancias osm ON osm.eCodSalida=oss.eCodSalida ".
			" WHERE 1=1 ".
			($_POST['eCodEntrada'] ? " AND oss.eCodEntrada=".$_POST['eCodEntrada'] : "").
			($_POST['fhFechaEntradaInicio']	? " AND oss.fhFechaEntrada			BETWEEN    '".$fhFechaEntradaInicio. "' AND '" .$fhFechaEntradaFin."'" : "" ).
			($_POST['Cliente']       ? " AND cec.tNombre   LIKE '%".$_POST['Cliente']."%'" : "").
			($_POST['tPatente']      ? " AND oss.tPatente  LIKE '%".$_POST['tPatente']."%'" : "").
			($_POST['Naviera']       ? " AND cen.tNombre   LIKE '%".$_POST['Naviera']."%'" : "").
			($_POST['eCodEIR'] ? " AND bei.eCodEIR=".$_POST['eCodEIR'] : "").
			($_POST['tBL']       ? " AND oss.tBL   LIKE '%".$_POST['tBL']."%'" : "").
			($_POST['TipoServicio']  ? " AND cto.eCodTipoServicio=".$_POST['TipoServicio'] : "").
			($_POST['Contenedor']       ? " AND oss.tCodContenedor   LIKE '%".$_POST['Contenedor']."%'" : "").
			($_POST['TipoContenedor'] ? " AND ctc.eCodTipoContenedor=".$_POST['TipoContenedor'] : "").			
			($_POST['Buque']       ? " AND cbu.tNombre   LIKE '%".$_POST['Buque']."%'" : "").
			($_POST['tNumeroViaje']      ? " AND oss.tNumeroViaje  LIKE '%".$_POST['tNumeroViaje']."%'" : "").
			((int)$_POST['eEntidad']==1 ? "" : " AND (oss.eCodNaviera=".(int)$_POST['eEntidad']." OR oss.eCodCliente=".(int)$_POST['eEntidad'].")").
			" ORDER BY oss.eCodEntrada DESC ";
	$rsSolicitudes=mysqli_query($link,$select);

	$rsSolicitudes=mysqli_query($link,$select);
	$rsUsuarios=mysqli_query($link,$select);
	$registros=(int)mysqli_num_rows($rsUsuarios);
	?> 
<table cellspacing="0" border="0" width="965px">
  <tr>
    <td width="50%"><hr color="#666666" /></td>
    <td class="sanLR04" nowrap="nowrap"><b>Registros Encontrados: (
      <?=$registros;?>
      )</b>
    <a href="excel/2.5.5.1.php">Haz</a>
  	</td>
    <td width="50%"><hr color="#666666" /></td>
  </tr>
</table>
	<div style="display:block; top:0; left:0; width:965px; z-index=1; overflow: auto; height:400px;">
	<table cellspacing="0" border="0" width="965px">
		<thead>
			<tr class="thEncabezado">
				<td nowrap="nowrap" class="sanLR04" height="20" align="center">E</td>
				<td nowrap="nowrap" class="sanLR04">C&oacute;digo</td>
				<td nowrap="nowrap" class="sanLR04">T. Servicio</td>
				<td nowrap="nowrap" class="sanLR04">F. Entrada</td>
				<td nowrap="nowrap" class="sanLR04">F. Salida</td>
				<td nowrap="nowrap" class="sanLR04">EIR</td>
				<td nowrap="nowrap" class="sanLR04">BL</td>
				<td nowrap="nowrap" class="sanLR04">Contenedor</td>
				<td nowrap="nowrap" class="sanLR04">T. Contenedor</td>
				<td nowrap="nowrap" class="sanLR04">Naviera</td>
				<td nowrap="nowrap" class="sanLR04">Patente</td>
				<td nowrap="nowrap" class="sanLR04">Cliente</td>
				<td nowrap="nowrap" class="sanLR04">Buque</td>
				<td nowrap="nowrap" class="sanLR04">Viaje</td>
				<td nowrap="nowrap" class="sanLR04" width="100%">Observaciones</td>
			</tr>
		</thead>
		<tbody>
			<?php $i=1; while($rSolicitud=mysqli_fetch_array($rsSolicitudes,MYSQLI_ASSOC)){ ?>
				<tr>
					<td nowrap="nowrap" class="sanLR04" height="20" align="center"><img width="16" height="16" alt="Entrada" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-<?=$rSolicitud{'tCodEstatus'};?>.png"></td>
					<td nowrap="nowrap" class="sanLR04 colmenu"><a href="?ePagina=2.4.5.1.1.php&eCodEntrada=<?=$rSolicitud{'eCodEntrada'};?>"><b><?=sprintf("%07d",$rSolicitud{'eCodEntrada'});?></b></a></td>
					<td nowrap="nowrap" class="sanLR04"><a class="txtCO12" href="?ePagina=2.5.5.1.1.php&eCodEntrada=<?=$rSolicitud{'eCodEntrada'};?>"><?=utf8_encode($rSolicitud{'TipoServicio'});?></a></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=date("d/m/Y", strtotime($rSolicitud{'fhFechaEntrada'}));?></td>
					<td nowrap="nowrap" class="sanLR04"><?=($rSolicitud{'fhFechaSalida'} ? date("d/m/Y", strtotime($rSolicitud{'fhFechaSalida'})) : "");?></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=($rSolicitud{'eCodEIR'} ? "<a class='txtCO12' href='?ePagina=2.5.5.1.2.php&eCodEIR=".$rSolicitud{'eCodEIR'}."'>".sprintf("%07d",$rSolicitud{'eCodEIR'})."</a>" : "");?></td>
					<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rSolicitud{'tBL'});?></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rSolicitud{'tCodContenedor'});?></td>
					<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rSolicitud{'TipoContenedor'});?></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rSolicitud{'Naviera'});?></td>
					<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rSolicitud{'tPatente'});?></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rSolicitud{'Cliente'});?></td>
					<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rSolicitud{'Buque'});?></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rSolicitud{'tNumeroViaje'});?></td>
					<td nowrap="nowrap" class="sanLR04" width="100%"><?=utf8_encode($rSolicitud{'tObservaciones'});?></td>
				</tr>
			<?php $i++; } ?>
		</tbody>
	</table>
	</div>
<?php }else{ ?>
	<script type="text/javascript">
	dojo.require("dojo.number");
	dojo.require("dijit.form.NumberTextBox");
	dojo.require("dijit.form.DateTextBox");
	dojo.require("dojo.io.iframe");
	function nuevo(){
		document.location = "?ePagina=2.4.5.1.php";
	}
	function generaExcel(){
		var UrlExcel = "php/excel/2.5.5.1.php";
		var submitForm = document.createElement("FORM");
		document.body.appendChild(submitForm);
		submitForm.method = "POST";
		submitForm.action = UrlExcel;
		submitForm.submit();
	}
	dojo.addOnLoad(function(){filtrarConsulta();});
	</script>
	<form name="Datos" id="Datos" method="post" action="" onsubmit="return false;">
		<input type="hidden" value="0" name="ePagina" id="ePagina" />
		<input type="hidden" value="" name="eAccion" id="eAccion" />
		<input type="hidden" value="<?=(int)$_SESSION['sesionUsuario']['eCodEntidad'];?>" name="eEntidad" id="eEntidad" />
		<table width="965px" border="0">
			<tr>
				<td colpan="3" width="100%"></td>
      		<td align="right" nowrap="nowrap"><a class="fntBR11" href="javaScript:mostrarFiltros('Busqueda');">Busqueda de Registros</a></td>
			</tr>
			<tr id="trBusqueda" style="display:none">
				<td colspan="4">
					<table width="965px" bgcolor="#f9f9f9">
						<tr>
							<td class="sanLR04" height="5"></td>
						</tr>
						<tr>
							<td class="sanLR04" height="20">C&oacute;digo</td>
							<td class="sanLR04" width="50%">
								<input type="text" name="eCodEntrada" dojoType="dijit.form.TextBox" id="eCodEntrada" value="" style="width:80px"></td>
							<td nowrap="nowrap" class="sanLR04" height="20">Fecha de Entrada</td>
            				<td class="sanLR04" width="50%" ><input name="fhFechaEntradaInicio" id="fhFechaEntradaInicio" type="date" required="false" style="width:140px;" hasDownArrow="false" displayMessage="false" value="" /> -
             				<input name="fhFechaEntradaFin" id="fhFechaEntradaFin" type="date"  required="false" style="width:140px;" hasDownArrow="false" displayMessage="false" value=""  /></td>           				
						</tr>
						<tr>
            				<td nowrap="nowrap"class="sanLR04">Cliente</td>
            				<td class="sanLR04" width="50%"><input type="text" name="Cliente" dojoType="dijit.form.TextBox" id="Cliente" value="" style="width:175px"></td>
            				<td class="sanLR04" height="20">Patente</td>
            				<td class="sanLR04" width="50%"><input type="text" name="tPatente" dojoType="dijit.form.TextBox" id="tPatente" value="" style="width:80px"></td>  
            				            				
          				</tr>
          				<tr>
            				<td class="sanLR04">Naviera</td>
            				<td class="sanLR04" width="50%"><input type="text" name="Naviera" dojoType="dijit.form.TextBox" id="Naviera" value="" style="width:175px"></td>
            				<td class="sanLR04">EIR</td>
            				<td class="sanLR04" width="50%"><input type="text" name="eCodEIR" dojoType="dijit.form.TextBox" id="eCodEIR" value="" style="width:80px"></td>          				
          				</tr>
          				<tr>
            				<td class="sanLR04">BL</td>
            				<td class="sanLR04" width="50%"><input type="text" name="tBL" dojoType="dijit.form.TextBox" id="tBL" value="" style="width:175px"></td>
            				<td nowrap="nowrap" class="sanLR04">Tipo de Servicio</td>
            				<td class="sanLR04" width="50%">
            					<select name='TipoServicio' id='TipoServicio' style="width:175px">
                					<option value='0'></option>
                						<?php $sel = mysqli_query($link,"SELECT eCodTipoServicio, tNombre FROM cattiposservicios where tCodEstatus='AC'");                
                            			  while($row = mysqli_fetch_array($sel,MYSQLI_ASSOC)){ ?>
                					<option value='<?php echo $row["eCodTipoServicio"]; ?>'> <?php echo $row["tNombre"]; ?> </option>
                						<?php } ?>
              					</select></td>
          				</tr>
          				<tr>
            				<td class="sanLR04" height="20">Contenedor</td>
            				<td class="sanLR04" width="50%"><input type="text" name="Contenedor" dojoType="dijit.form.TextBox" id="Contenedor" value="" style="width:175px"></td>
            				<td nowrap="nowrap" class="sanLR04" height="20">Tipo de Contenedor</td>
            				<td class="sanLR04" width="50%">
            					<select name='TipoContenedor' id='TipoContenedor' style="width:175px">
                					<option value='0'></option>
                						<?php $sel = mysqli_query($link,"SELECT eCodTipoContenedor, tNombreCorto FROM cattiposcontenedores where tCodEstatus='AC' order by tNombreCorto");                
                            			  while($row = mysqli_fetch_array($sel,MYSQLI_ASSOC)){ ?>
                					<option value='<?php echo $row["eCodTipoContenedor"]; ?>'> <?php echo $row["tNombreCorto"]; ?> </option>
                						<?php } ?>
              					</select>
            				</td>
          				</tr>
          				<tr>
          					<td class="sanLR04" height="20">Buque</td>
            				<td class="sanLR04" width="50%"><input type="text" name="Buque" dojoType="dijit.form.TextBox" id="Buque" value="" style="width:175px"></td>
            				<td class="sanLR04">Viaje</td>
            				<td class="sanLR04" width="50%"><input type="text" name="tNumeroViaje" dojoType="dijit.form.TextBox" id="tNumeroViaje" value="" style="width:80px"></td>           				
            				
          				</tr>
						<tr>
							<td class="sanLR04" height="5">
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr><td colspan="4"><div id="dvCNS"></div></td></tr>
		</table>
	</form>
<?php } ?>