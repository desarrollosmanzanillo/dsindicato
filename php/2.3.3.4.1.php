<?php require_once("conexion/soluciones-mysql.php");  
$link = getLink();
if($_POST){
	if($_POST['eProceso']==1){
		$exito				= 0;
		$tNombre	= ($_POST['tNombre']	? "'".trim(utf8_decode(addslashes($_POST['tNombre'])))."'"	: "NULL");
		$tCodDanio	= ($_POST['tCodDanio']	? "'".trim(utf8_decode(addslashes($_POST['tCodDanio'])))."'"	: "NULL");
		$eCodDanio	= ($_POST['eCodDanio']	? (int)$_POST['eCodDanio']									: "NULL");

		if((int)$eCodDanio>0){
			$update=" UPDATE catdanioscontenedores ".
					" SET tNombre=".$tNombre.", tCodDanio=".$tCodDanio.
					" WHERE eCodDanio=".$eCodDanio;
			if($res=mysqli_query($link,$update)){
				$exito=1;
			}else{
				$exito=0;
			}
		}else{
			$insert=" INSERT INTO catdanioscontenedores (tCodEstatus, tNombre, tCodDanio) ".
					" VALUES ('AC', ".$tNombre.", ".$tCodDanio.")";
			if($res=mysqli_query($link,$insert)){
				$exito=1;
				$select=" select last_insert_id() AS Llave ";
				$rCodigo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
				$eCodDanio=(int)$rCodigo['Llave'];
			}else{
				$exito=0;
			}
		}
		print "<input type=\"text\" value=\"".($exito==1 ? $eCodDanio : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}
	
	if($_POST['eProceso']==2){
		$rDano=mysqli_fetch_array(mysqli_query($link,"SELECT 1 AS Dano FROM catdanioscontenedores WHERE tNombre='".trim($_POST['tNombre'])."' AND tCodDanio='".trim($_POST['tCodDanio'])."' AND eCodDanio!=".(int)$_POST['eCodDanio']),MYSQLI_ASSOC);
		print "<input name=\"eDano\" id=\"eDano\" value=\"".(int)$rDano{'Dano'}."\">";
	}
}
if(!$_POST){
	
$select=" SELECT * ".
		" FROM catdanioscontenedores ".
		" WHERE eCodDanio=".$_GET['eCodDanio'];
$rDano = mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC); ?>
<script type="text/javascript">
dojo.require("dojo.number");
dojo.require("dijit.form.NumberTextBox");
function guardar(){
	dojo.byId('eProceso').value=1;
	bandera = false;
	mensaje = "¡Verifique lo siguiente!\n";
	if (!dojo.byId("tNombre").value){
		mensaje+="* Nombre\n";
		bandera = true;		
	}
	if (!dojo.byId("tCodDanio").value){
		mensaje+="* Codigo Daño\n";
		bandera = true;		
	}
	
	if (bandera==true){
		alert(mensaje);		
	}else{
		if (confirm("¿Desea Guardar la Información?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				if(dojo.byId("eCodigo").value>0){
					alert("¡La información se guardó correctamente!");
					tURL = './?ePagina=2.3.3.4.2.php&eCodDanio='+dojo.byId("eCodigo").value;
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
		}
	}
}	

function verificarDano(){
	if(dojo.byId('tNombre').value){
		dojo.byId('eProceso').value=2;
		dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
			dojo.byId("dvCNS").innerHTML = tRespuesta;
			if(dojo.byId('eDano').value==1){
				alert("¡Este daño de contenedor ya existe!");
				dojo.byId('tNombre').value="";
			}
		}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
	}
	if(dojo.byId('tCodDanio').value){
		dojo.byId('eProceso').value=2;
		dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
			dojo.byId("dvCNS").innerHTML = tRespuesta;
			if(dojo.byId('eDano').value==1){
				alert("¡Este Codigo de daño de contenedor ya existe!");
				dojo.byId('tCodDanio').value="";
			}
		}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
	}
}

function consultar(){
	document.location = './?ePagina=2.3.3.4.php';
}
</script>
<div id="dvCNS" style="display:none;"></div>
<form action="" method="post" name="Datos" id="Datos" onsubmit="return false;">
<input type="hidden" name="eProceso" id="eProceso" value="" />
<input type="hidden" name="eCodDanio" id="eCodDanio" value="<?=$_GET['eCodDanio'];?>" />
<table border="0" cellpadding="0" cellspacing="0">
    <tr><td height="20"></td></tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Nombre </td>
	    <td width="100%" nowrap class="sanLR04">
           	<input name="tNombre" type="text" dojoType="dijit.form.TextBox" id="tNombre" value="<?=utf8_encode($rDano{'tNombre'});?>" style="width:175px" onblur="verificarDano();">
        </td>
    </tr>
    <tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> C&oacute;digo Da&ntilde;o </td>
	    <td width="100%" nowrap class="sanLR04">
           	<input name="tCodDanio" type="text" dojoType="dijit.form.TextBox" id="tCodDanio" value="<?=utf8_encode($rDano{'tCodDanio'});?>" style="width:175px" onblur="verificarDano();">
        </td>
    </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle" /> <font class="fntG10K">Indica que es obligatorio llenar el campo.
    </font></td>
    </tr>
</table>
</form>
<?php } ?>