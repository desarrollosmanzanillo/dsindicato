<?php require_once("conexion/soluciones-mysql.php");  
$link = getLink();
if($_POST){
	if($_POST['eProceso']==1){
		$exito			= 0;
		$tNombre		= ($_POST['tNombre']	? "'".trim(utf8_decode($_POST['tNombre']))."'"	: "NULL");
		$eCodCredito	= ($_POST['eCodCredito']	? (int)$_POST['eCodCredito']						: "NULL");

		

		if((int)$eCodCredito>0)
		{
			$update=" UPDATE cattipocredito ".
					" SET tNombre=".$tNombre.
					" , tCodEstatus='AC' ".
					" WHERE eCodCredito=".$eCodCredito;
			if($res=mysqli_query($link,$update))
			{
				$exito=1;
			}
			else
			{
				$exito=0;
			}
		}
		else
		{
			$insert=" INSERT INTO cattipocredito (tCodEstatus, tNombre) ".
					" VALUES ('AC', ".$tNombre.")";
			if($res=mysqli_query($link,$insert))
			{
				$exito=1;
				$select=" select last_insert_id() AS Llave ";
				$rCodigo=mysqli_fetch_array(mysqli_query($link,$select), MYSQLI_ASSOC);
				$eCodCredito=(int)$rCodigo['Llave'];
			}
			else
			{
				$exito=0;
			}
		}
	
		print "<input type=\"text\" value=\"".($exito==1 ? $eCodCredito : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}
	
	if($_POST['eProceso']==2){
		$rArea=mysqli_fetch_array(mysqli_query($link,"SELECT 1 AS Area FROM cattipocredito WHERE tNombre='".trim($_POST['tNombre'])."' AND eCodCredito!=".(int)$_POST['eCodCredito']),MYSQLI_ASSOC);
		print "<input name=\"eArea\" id=\"eArea\" value=\"".(int)$rArea{'Area'}."\">";
	}
}
if(!$_POST){
	
$select=" SELECT * ".
		" FROM cattipocredito ".
		" WHERE eCodCredito=".$_GET['eCodCredito'];
$rArea = mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC); ?>
<script type="text/javascript">
function guardar(){
	dojo.byId('eProceso').value=1;
	bandera = false;
	mensaje = "¡Verifique lo siguiente!\n";
	if (!dojo.byId("tNombre").value){
		mensaje+="* Nombre\n";
		bandera = true;		
	}

	if (bandera==true){
		alert(mensaje);		
	}else{
		if (confirm("¿Desea Guardar la Información?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				if(dojo.byId("eCodigo").value>0){
					alert("¡La información se guardó correctamente!");
					tURL = './?ePagina=2.3.2.5.2.php&eCodCredito='+dojo.byId("eCodigo").value;
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
		}
	}
}	

function verificarTipo(){
	if(dojo.byId('tNombre').value){
		dojo.byId('eProceso').value=2;
		dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
			dojo.byId("dvCNS").innerHTML = tRespuesta;
			if(dojo.byId('eArea').value==1){
				alert("¡Este Nombre de Credito ya existe!");
				dojo.byId('tNombre').value="";
			}
		}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
	}
}

function consultar(){
	document.location = './?ePagina=2.3.2.5.php';
}
</script>
<div id="dvCNS" style="display:none;"></div>
<form action="" method="post" name="Datos" id="Datos" onsubmit="return false;">
<input type="hidden" name="eProceso" id="eProceso" value="" />
<input type="hidden" name="eCodCredito" id="eCodCredito" value="<?=$_GET['eCodCredito'];?>" />
<table border="0" cellpadding="0" cellspacing="0">
    <tr><td height="20"></td></tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Nombre </td>
	    <td width="50%" nowrap class="sanLR04">
           	<input name="tNombre" type="text" dojoType="dijit.form.TextBox" id="tNombre" value="<?=utf8_encode($rArea{'tNombre'});?>" style="width:175px" onblur="verificarTipo();" >
        </td>
    </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle" /> <font class="fntG10K">Indica que es obligatorio llenar el campo.
    </font></td>
    </tr>
</table>
</form>
<?php } ?>