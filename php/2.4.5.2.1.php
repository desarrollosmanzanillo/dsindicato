<?php
require_once("conexion/soluciones-mysql.php");
$link = getLink(); 
if($_POST){
	if($_POST['eProceso']==1){
		$exito				= 0;
		$tBL 				= ($_POST['tBL']					? "'".trim(utf8_decode(addslashes($_POST['tBL'])))."'"			: "NULL");
		$tIP				= ($_SERVER['REMOTE_ADDR']		? "'".trim(utf8_decode(addslashes($_SERVER['REMOTE_ADDR'])))."'"	: "NULL");
		$eCodArea			= ($_POST['eCodArea']			? (int)$_POST['eCodArea']										: "NULL");
		$tPatente			= ($_POST['tPatente']			? "'".trim(utf8_decode(addslashes($_POST['tPatente'])))."'"		: "NULL");
		$eCodSalida			= ($_POST['eCodSalida']			? (int)$_POST['eCodSalida']										: "NULL");
		$eCodCliente		= ($_POST['eCodCliente']			? (int)$_POST['eCodCliente']										: "NULL");
		$eCodNaviera		= ($_POST['eCodNaviera']			? (int)$_POST['eCodNaviera']										: "NULL");
		$eCodUsuario		= ($_POST['eCodUsuario']			? (int)$_POST['eCodUsuario']										: "NULL");
		$tURLArchivo		= ($_POST['tURLArchivo3']		? "'".trim(utf8_decode(addslashes($_POST['tURLArchivo3'])))."'"	: "NULL");
		$eCodSolicitud		= ($_POST['eCodSolicitud']		? (int)$_POST['eCodSolicitud']									: "NULL");
		$eCodFacturarA		= ($_POST['eCodFacturarA']		? (int)$_POST['eCodFacturarA']									: "NULL");
		$fhFechaSalida		= ($_POST['fhFechaSalida']		? "'".$_POST['fhFechaSalida']."'"								: "NULL");
		$tCodContenedor		= ($_POST['tMercancia']			? "'".$_POST['tMercancia']."'"									: "NULL");
		$tObservaciones		= ($_POST['tObservaciones']		? "'".$_POST['tObservaciones']."'"								: "NULL");
		$eCodTipoContenedor	= ($_POST['eCodTipoContenedor']	? (int)$_POST['eCodTipoContenedor']								: "NULL");
		
		
		$insert=" UPDATE opesalidasmercancias ".
				" SET	eCodNaviera=".$eCodNaviera.",".
				"		tPatente=".$tPatente.",".
				"		eCodCliente=".$eCodCliente.",".
				"		eCodFacturarA=".$eCodFacturarA.",".
				"		eCodUsuario=".$eCodUsuario.",".
				"		tObservaciones=".$tObservaciones.",".
				"		fhFechaSalida=".$fhFechaSalida.",".
				"		tIP=".$tIP.
				" WHERE eCodSalida=".(int)$eCodSalida; 
		if($res=mysqli_query($link,$insert)){
			$exito=1;
		}else{
			$exito=0;
		}
		print "<input type=\"text\" value=\"".($exito==1 ? $eCodSalida : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}

	if($_POST['eProceso']==7){
		$select=" SELECT * FROM( ".
				" SELECT see.* ".
				" FROM catentidades see ".
				" WHERE eCodEntidad=".(int)(int)$_POST['eCodCliente'].
				" UNION ".
				" SELECT ce.* ".
				" FROM catentidades ce ".
				" INNER JOIN relclientesagentes rc ON rc.eCodCliente=ce.eCodEntidad AND rc.eCodAgente=".(int)$_POST['eCodCliente'].
				" WHERE ce.tCodEstatus='AC' AND ce.eCodTipoEntidad=3 ".
				" ) AS R1 ".
				" ORDER BY tNombre ";
		$rsFacturarA=mysqli_query($link,$select); ?>
		<select name="eCodFacturarA" id="eCodFacturarA" style="width:175px">
			<option value="">Seleccione...</option>
			<?php while($rFacturarA=mysqli_fetch_array($link,$rsFacturarA),MYSQLI_ASSOC){ ?>
				<option value="<?=$rFacturarA{'eCodEntidad'}?>"><?=utf8_encode($rFacturarA{'tNombre'})?></option>
			<?php } ?>
		</select>
	<?php }
	
	if($_POST['eProceso']==8){ 
		$select=" SELECT * ".
				" FROM opeentradasmercancias ".
				" WHERE tCodContenedor='".$_POST['tMercancia']."' AND eCodEntrada<>".(int)$_POST['eCodEntrada']." AND eCodSalida IS NULL ";
		$rEntrada=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
		print $rEntrada{'eCodEntrada'};
		?>
	<? }
}
if(!$_POST){ 
$select=" SELECT oss.eCodSalida, oss.eCodSolicitud, ces.tNombre AS Estatus, oss.fhFechaSalida, ".
		" cen.tNombre AS Naviera, oss.tPatente, cec.tNombre AS Cliente, cef.tNombre AS FacturarA, ".
		" cbu.tNombre AS Buque, oem.tNumeroViaje, cto.tNombre AS TipoServicio, oss.tObservaciones, ".
		" oem.eCodTipoServicio, car.tNombre AS Area, oem.tCodContenedor, oss.eCodNaviera, oem.eCodArea, ".
		" ctc.tNombreCorto AS TipoContenedor, oss.eCodFacturarA, oss.eCodCliente, oem.tMercancia, ".
		" oem.eCodTipoContenedor, oss.tObservaciones, oem.tBL ".
		" FROM opesalidasmercancias oss ".
		" INNER JOIN opeentradasmercancias oem ON oem.eCodSalida=oss.eCodSalida ".
		" INNER JOIN catestatus ces ON ces.tCodEstatus=oss.tCodEstatus ".
		" INNER JOIN catentidades cen ON cen.eCodEntidad=oss.eCodNaviera ".
		" INNER JOIN catentidades cec ON cec.eCodEntidad=oss.eCodCliente ".
		" INNER JOIN catentidades cef ON cef.eCodEntidad=oss.eCodFacturarA ".
		" LEFT JOIN catbuques cbu ON cbu.eCodBuque=oem.eCodBuque ".
		" INNER JOIN cattiposservicios cto ON cto.eCodTipoServicio=oem.eCodTipoServicio ".
		" LEFT JOIN catareas car ON car.eCodArea=oem.eCodArea ".
		" LEFT JOIN cattiposcontenedores ctc ON ctc.eCodTipoContenedor=oem.eCodTipoContenedor ".
		" WHERE oss.eCodSalida=".(int)$_GET['eCodSalida'];
$rSolicitud=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

$select=" SELECT ca.*, cta.tNombre AS TipoArea ".
		" FROM catareas ca ".
		" INNER JOIN cattiposareas cta ON cta.eCodTipoArea=ca.eCodTipoArea ".
		" WHERE ca.tCodEstatus='AC' ".
		" ORDER BY ca.tNombre ASC ";
$rsAreas=mysqli_query($link,$select);

$select=" SELECT * ".
		" FROM catentidades ".
		" WHERE tCodEstatus='AC' ".
		" ORDER BY tNombre ";
$rsNavieras=mysqli_query($link,$select);

$select=" SELECT * ".
		" FROM catentidades ".
		" WHERE tCodEstatus='AC' ".
		" ORDER BY tNombre ";
$rsClientes=mysqli_query($link,$select);

$select=" SELECT * FROM( ".
		" SELECT see.* ".
		" FROM catentidades see ".
		" WHERE eCodEntidad=".(int)$rSolicitud{'eCodCliente'}.
		" UNION ".
		" SELECT ce.* ".
		" FROM catentidades ce ".
		" INNER JOIN relclientesagentes rc ON rc.eCodCliente=ce.eCodEntidad AND rc.eCodAgente=".(int)$rSolicitud{'eCodCliente'}.
		" WHERE ce.tCodEstatus='AC' AND ce.eCodTipoEntidad=3 ".
		" ) AS R1 ".
		" ORDER BY tNombre ";
$rsFacturarA=mysqli_query($link,$select);
?>
<script type="text/javascript">
dojo.require("dojo.number");
dojo.require("dijit.form.NumberTextBox");
dojo.require("dijit.form.DateTextBox");
dojo.require("dojo.io.iframe");
function guardar(){
	dojo.byId('eProceso').value=1;
	var bandera = false;
	var bServicios = false;
	var bMercancias = false;
	mensaje = "¡Verifique lo siguiente!\n";
	if (!dojo.byId("fhFechaSalida").value){
		mensaje+="* Fecha de Salida\n";
		bandera = true;		
	}
	if (!dojo.byId("eCodNaviera").value){
		mensaje+="* Naviera\n";
		bandera = true;		
	}
	if (!dojo.byId("eCodCliente").value){
		mensaje+="* Cliente\n";
		bandera = true;		
	}
	if (!dojo.byId("eCodFacturarA").value){
		mensaje+="* Facturar a\n";
		bandera = true;		
	}
	if (bandera==true){
		alert(mensaje);		
	}else{
		if (confirm("¿Desea Guardar la Información?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				if(dojo.byId("eCodigo").value>0){
					alert("¡La información se guardó correctamente!");
					tURL = './?ePagina=2.5.5.2.1.php&eCodSalida='+dojo.byId("eCodigo").value;	
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
		}
	}
}	

function subirarchivos(indice){
	dojo.byId('eArchivoIndice').value = indice;
	if(dojo.byId("tArchivo"+indice).value){
		dojo.byId("eProceso").value = 2;
		dojo.io.iframe.send({url: "php/"+dojo.byId('ePagina').value+".php", method: "post", handleAs: "text", form: 'Datos', handle: function(tRespuesta, ioArgs){
			dojo.byId("divArchivo"+indice).innerHTML = tRespuesta;
		}});
	}
}

function consultar(){
	document.location = "?ePagina=2.5.5.1.php";
}

function validarContenedor(){
	dojo.byId('eProceso').value=8;
	dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
		if(tRespuesta>0){
			alert('¡El contenedor ya existe en la entrada '+tRespuesta+'!');
			dojo.byId('tMercancia').value="";
		}
	}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});
}

function cargarFacturarA(){
	dojo.byId('eProceso').value=7;
	dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
		dojo.byId("tdFacturarA").innerHTML = tRespuesta;
	}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});
}

dojo.addOnLoad(function(){
});
</script>
<div id="dvCNS" style="display:none;"></div>
<form method="post" name="Datos" id="Datos" onsubmit="return false;" enctype="multipart/form-data">
<input type="hidden" name="eProceso" id="eProceso" value="" />
<input type="hidden" name="eCodSalida" id="eCodSalida" value="<?=$rSolicitud{'eCodSalida'};?>" />
<input type="hidden" name="eArchivoIndice" id="eArchivoIndice" value="" />
<input type="hidden" name="eCodUsuario" id="eCodUsuario" value="<?=(int)$_SESSION['sesionUsuario']['eCodUsuario'];?>" />
<input type="hidden" name="ePosX" id="ePosX" />
<input type="hidden" name="tCara" id="tCara" />
<input type="hidden" name="ePosY" id="ePosY" />
<input type="hidden" name="tImagen" id="tImagen" />  
<input type="hidden" name="eCodCara" id="eCodCara" />
<table border="0" cellpadding="0" cellspacing="0" width="980px">
    <tr><td height="20"></td></tr>
    <tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Codigo </td>
	    <td width="50%" nowrap class="sanLR04"><?=sprintf("%07d",$_GET['eCodSalida']);?></td>
		<td height="23" nowrap class="sanLR04"></td>
	    <td width="50%" nowrap class="sanLR04"></td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Fecha de Salida </td>
	    <td width="50%" nowrap class="sanLR04">
           	<input type="date" required  name="fhFechaSalida" id="fhFechaSalida"  value="<?=($rSolicitud{'fhFechaSalida'} ? date("Y-m-d", strtotime($rSolicitud{'fhFechaSalida'})) : date("Y-m-d"));?>" />
        </td>
		<td height="23" nowrap class="sanLR04"></td>
	    <td width="50%" nowrap class="sanLR04"></td>
    </tr>
    <tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Naviera </td>
	    <td width="50%" nowrap class="sanLR04">
           	<select name="eCodNaviera" id="eCodNaviera" style="width:175px">
				<option value="">Seleccione...</option>
				<?php while($rNaviera=mysqli_fetch_array($rsNavieras,MYSQLI_ASSOC)){ ?>
					<option value="<?=$rNaviera{'eCodEntidad'}?>" <?=($rNaviera{'eCodEntidad'}==$rSolicitud{'eCodNaviera'} ? "selected='selected'" : "");?> ><?=utf8_encode($rNaviera{'tNombre'})?></option>
				<?php } ?>
			</select>
        </td>
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Patente </td>
	    <td width="50%" nowrap class="sanLR04">
			<input name="tPatente" type="text" dojoType="dijit.form.TextBox" id="tPatente" value="<?=utf8_encode($rSolicitud{'tPatente'});?>" style="width:80px" UpperCase="true">
        </td>
    </tr>
    <tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Cliente </td>
	    <td width="50%" nowrap class="sanLR04">
           	<select name="eCodCliente" id="eCodCliente" style="width:175px" onchange="cargarFacturarA();">
				<option value="">Seleccione...</option>
				<?php while($rCliente=mysqli_fetch_array($rsClientes,MYSQLI_ASSOC)){ ?>
					<option value="<?=$rCliente{'eCodEntidad'}?>" <?=($rCliente{'eCodEntidad'}==$rSolicitud{'eCodCliente'} ? "selected='selected'" : "");?> ><?=utf8_encode($rCliente{'tNombre'})?></option>
				<?php } ?>
			</select>
        </td>
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Facturar a </td>
	    <td width="50%" nowrap class="sanLR04" id="tdFacturarA">
			<select name="eCodFacturarA" id="eCodFacturarA" style="width:175px">
				<option value="">Seleccione...</option>
				<?php while($rFacturarA=mysqli_fetch_array($rsFacturarA,MYSQLI_ASSOC)){ ?>
					<option value="<?=$rFacturarA{'eCodEntidad'}?>" <?=($rFacturarA{'eCodEntidad'}==$rSolicitud{'eCodFacturarA'} ? "selected='selected'" : "");?> ><?=utf8_encode($rFacturarA{'tNombre'})?></option>
				<?php } ?>
			</select>
        </td>
    </tr>
    
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Buque </td>
	    <td width="50%" nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'Buque'} ? $rSolicitud{'Buque'} : "N/D");?></td>
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Viaje </td>
	    <td width="50%" nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'tNumeroViaje'} ? $rSolicitud{'tNumeroViaje'} : "N/D");?></td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Tipo de Solicitud </td>
	    <td width="50%" nowrap class="sanLR04">Salida Directa</td>
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Tipo de Servicio </td>
	    <td width="50%" nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'TipoServicio'} ? $rSolicitud{'TipoServicio'} : "Contenedor");?></td>
    </tr>
	<tr><td colspan="4" align="center"><hr width="95%" size="0" align="center" color="#CACACA"></td></tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="" align="absmiddle"> BL </td>
	    <td nowrap class="sanLR04" colspan="3"><?=utf8_encode($rSolicitud{'tBL'});?></td>
    </tr>
    <tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> <?=($rSolicitud{'eCodTipoServicio'}==1 ? "Contenedor" : "Mercanc&iacute;a");?> </td>
	    <td nowrap class="sanLR04"><?=($rSolicitud{'eCodTipoServicio'}==1 ? $rSolicitud{'tCodContenedor'} : $rSolicitud{'tMercancia'});?></td>
        <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> <?=($rSolicitud{'eCodTipoServicio'}==1 ? "Tipo" : "Embalaje");?></td>
	    <td width="50%" nowrap class="sanLR04">
		<?=$rSolicitud{'TipoContenedor'};?>
        </td>
    </tr>
    <tr>
    	<td height="23" nowrap class="sanLR04" valign="top"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="" align="absmiddle"> Observaciones</td>
        <td nowrap="nowrap" class="sanLR04" colspan="3">
        	<textarea name="tObservaciones" id="tObservaciones" cols="84" rows="5"><?=$rSolicitud{'tObservaciones'}?></textarea>
        </td>
    </tr>
    
	<tr><td height="10" nowrap class="sanLR04" colspan="4"></td></tr>
	<?php /*if(((int)$_GET['eCodSolicitud']>0 && $_GET['tCodContenedor']!="") || (int)$_GET['eCodSolicitud']==0){ 
		$select=" SELECT * FROM catvistascontenedores WHERE eCodVista IN(1,2,3) ";
		$rsVistas=mysql_query($select);
		$select=" SELECT * FROM catdanioscontenedores WHERE tCodEstatus='AC' ";
		$rsDanios = mysql_query($select); ?> 
		<tr>
			<td height="10" nowrap class="sanLR04" colspan="4"></td>
		</tr>
		<tr>
			<td nowrap="nowrap" class="sanLR04 fntN11B" colspan="3" height="23"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> EIR</td>
			<td><input type="hidden" id="bEIR" name="bEIR" value="1" /></td>
		</tr>
		<tr>
			<td rowspan="2" height="23" valign="top" nowrap="nowrap" class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" width="16" height="16" align="absmiddle"> Inspecci&oacute;n</td>
			<td rowspan="2" valign="top"><table border="0" cellspacing="0" cellpadding="0" class="tbConsulta">
				<?php $i=0;
				while($rVista=mysql_fetch_array($rsVistas)){
					$select=" SELECT rvc.*, cc.tNombre AS Cara ".
							" FROM relvistascontenedorescarascontenedores rvc ".
							" INNER JOIN catcarascontenedores cc ON cc.eCodCara=rvc.eCodCara ".
							" WHERE rvc.eCodVista=".$rVista{'eCodVista'};
					$rsCaras = mysql_query($select);
					if($i) { ?>
						<tr>
							<td height="40" colspan="3" align="center"></td>
						</tr>
					<?php } ?>
					<tr class="thEncabezado">
						<td height="20" colspan="3" align="center"><?=($rVista{'tNombre'});?></td>
					</tr>
					<tr>
						<td nowrap="nowrap" ><span class="fntA11S"><?=$rVista{'tCostadoI'};?></span></td>
						<td align="center" nowrap="nowrap" ><span class="fntA11S"><?=$rVista{'tCostadoT'};?></span></td>
						<td height="20" align="right" nowrap="nowrap" >&nbsp;</td>
					</tr>
					<tr>
						<td height="20" colspan="3" align="center">    
						<div id="divImagen<?=$rVista{'eCodVista'};?>" style="width:<?=$rVista{'eAncho'};?>px;height:<?=$rVista{'eAlto'};?>px; position:relative; z-index:<?=$rVista{'eCodVista'};?>;">
						<div id="divVista<?=$rVista{'eCodVista'};?>" style="position:static;"></div>
						<img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/<?=$rVista{'tNombreCorto'};?>.png" width="<?=$rVista{'eAncho'};?>" height="<?=$rVista{'eAlto'};?>" id="img<?=$rVista{'eCodVista'};?>" usemap="#mapa<?=$rVista{'eCodVista'};?>" border="0" />
						<?php if(mysql_num_rows($rsCaras)) { ?>
							<map name="mapa<?=$rVista{'eCodVista'};?>">
							<?php while ($rCara=mysql_fetch_array($rsCaras)) { ?>
								<area onclick="asignarCara(event,this,'<?=$rVista{'eCodVista'};?>','<?=$rCara{'eCodCara'};?>')" shape="poly" alt="<?=$rCara{'Cara'};?>" coords="<?=$rCara{'tCoordenadas'};?>" nohref="nohref" />
							<?php } ?>
							</map>
						<?php } ?>
						</div>
						</td>
					</tr>
					<tr>
						<td align="center" nowrap="nowrap">&nbsp;</td>
						<td align="center" nowrap="nowrap"><span class="fntA11S"><?=$rVista{'tCostadoB'};?></span></td>
						<td height="20" align="right" nowrap="nowrap"><span class="fntA11S"><?=$rVista{'tCostadoD'};?></span></td>
					</tr>
					<?php $i++;
				} ?>
			</table></td>
			<td valign="top" nowrap="nowrap" class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" width="16" height="16" align="absmiddle"> Da&ntilde;os</td>
			<td valign="top" nowrap="nowrap" class="sanLR04"><table border="0" cellspacing="0" cellpadding="0">
				<?php $i= 0;
				while($rDanio=mysql_fetch_array($rsDanios)){ ?>
					<tr>
						<td height="20" nowrap="nowrap"><label>
						<input type="radio" name="rdDanio" id="rdDanio" value="<?=trim($rDanio{'tCodDanio'});?>" <?=(!$i ? "checked=\"checked\"" : "")?> />
						<input type="hidden" name="D-<?=trim($rDanio{'tCodDanio'});?>" id="D-<?=trim($rDanio{'tCodDanio'});?>" value="<?=trim($rDanio{'eCodDanio'});?>" />
						<?="(".trim($rDanio{'tCodDanio'}).")";?> / <?=utf8_encode($rDanio{'tNombre'});?>
						</label></td>
					</tr>
					<?php $i++;
				} ?>
			</table></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" width="16" height="16" align="absmiddle"> Da&ntilde;os</td>
			<td valign="top" class="sanLR04" style="max-width:220px; min-width:220px"><div id="divDaniosRegistrados">Ninguno</div></td>
		</tr>
		<tr>
			<td height="10" nowrap class="sanLR04" colspan="4"></td>
		</tr>
		<tr>
			<td nowrap="nowrap" class="" height="23"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> EIR</td>
			<td nowrap="nowrap" class="sanLR04" width="100%">
				<input id="tArchivo3" name="tArchivo3" type="file" class="uploadBtn" onkeyup="this.value=''" style="width:325px" onchange="subirarchivos(3);" />
			</td>
			<td nowrap="nowrap" class="sanLR04" width="100%">
				<div id="divArchivo3"></div>
			</td>
		</tr>
	<?php }*/ ?>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle" /> <font class="fntG10K">Indica que es obligatorio llenar el campo.
    </font></td>
    </tr>
</table>
</form>
<?php } ?>