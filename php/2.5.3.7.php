<style>
#divgral {
     overflow:scroll;
     height:450px;
     width:900px;
}
</style>
<?php
require_once("conexion/soluciones-mysql.php"); 
$link = getLink();

if($_POST){
	if($_POST['fhFechaAsistenciaInicio']!=""){
  		$fhFechaAsistenciaInicio = $_POST['fhFechaAsistenciaInicio'].' 00:00:00';
  		$fhFechaAsistenciaFin = ($_POST['fhFechaAsistenciaFin']!="" ? $_POST['fhFechaAsistenciaFin'] : $_POST['fhFechaAsistenciaInicio']).' 23:59:59';
 	}

	$select=" SELECT *
			FROM catchecadoras as emp
			WHERE emp.Estado='AC' ".
			($_POST['eCodEmpleado'] ? " AND emp.id=".$_POST['eCodEmpleado'] : "").
			($_POST['fhFechaAsistenciaInicio']	? " AND emp.FechaRegistro BETWEEN '".$fhFechaAsistenciaInicio. "' AND '" .$fhFechaAsistenciaFin."'" : "" ).
			" Order by emp.id ".($_POST['orden'] ? $_POST['orden'] : "DESC" )." LIMIT ".( $_POST['limite'] ? $_POST['limite']  : "100" );
	$rsEntidades=mysqli_query($link,$select);
	$rsUsuarios=mysqli_query($link,$select);
	$registros=(int)mysqli_num_rows($rsUsuarios); ?>
	<input type="hidden" value="<?=($_POST['orden'] ? $_POST['orden'] : "DESC");?>" name="tOrden" id="tOrden" />
    <table cellspacing="0" border="0" width="900px">
	    <tr>
		    <td width="50%"><hr color="#666666" /></td>
		    <td class="sanLR04" nowrap="nowrap"><b>Registros Encontrados: ( <?=$registros;?> )</b></td>
			<td width="50%"><hr color="#666666" /></td>
	    </tr>
    </table>
	<div id="divgral" style="display:block; top:0; left:0; width:900px; z-index=1; overflow: auto;">
	<table cellspacing="0" border="0" width="900px">
		<thead>
			<tr class="thEncabezado">
				<td nowrap="nowrap" class="sanLR04">E</td>
				<td nowrap="nowrap" class="sanLR04">C&oacute;digo</td>
				<td nowrap="nowrap" class="sanLR04">Paterno</td>
                <td nowrap="nowrap" class="sanLR04">Materno</td>
                <td nowrap="nowrap" class="sanLR04">Nombre(s)</td>
                <td nowrap="nowrap" class="sanLR04">Nacimiento</td>
                <td nowrap="nowrap" class="sanLR04">RFC</td>
                <td nowrap="nowrap" class="sanLR04">CURP</td>
                <td nowrap="nowrap" class="sanLR04">NSS</td>
                <td nowrap="nowrap" class="sanLR04">F.Ingreso</td>
                <td nowrap="nowrap" class="sanLR04">T.Nomina</td>
                <td nowrap="nowrap" class="sanLR04">Depto</td>
                <td nowrap="nowrap" class="sanLR04">Puesto</td>
                <td nowrap="nowrap" class="sanLR04">T. Pago</td>
                <td nowrap="nowrap" class="sanLR04">Cta. Bancaria</td>
                <td nowrap="nowrap" class="sanLR04">T. Credito</td>
                <td nowrap="nowrap" class="sanLR04">N&uacute;mero Credito</td>
                <td nowrap="nowrap" class="sanLR04">F. Inicio Credito</td>
                <td nowrap="nowrap" class="sanLR04">F. Fin Credito</td>
                <td nowrap="nowrap" class="sanLR04" width="100%">% Monto o Descuento</td>
			</tr>
		</thead>
		<tbody>
			<?php $i=1; while($rEntidad=mysqli_fetch_array($rsEntidades,MYSQLI_ASSOC)){ 
				$nacimiento=$rEntidad["CURP"];
				$abc=$nacimiento[8].$nacimiento[9]."/".$nacimiento[6].$nacimiento[7]."/".$nacimiento[4].$nacimiento[5]; ?>
				<tr>
					<td nowrap="nowrap" class="sanLR04"><img width="16" height="16" alt="Usuario" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-<?=$rEntidad{'Estado'};?>.png"></td>
					<td nowrap="nowrap" class="sanLR04 colmenu" ><b>7<?=sprintf("%04d",$rEntidad{'id'});?></b></td>
					<td nowrap="nowrap" class="sanLR04 "><?=utf8_encode($rEntidad{'Paterno'});?></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rEntidad{'Materno'});?></td>
					<td nowrap="nowrap" class="sanLR04 "><?=utf8_encode($rEntidad{'Nombre'});?></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($abc);?></td>
					<td nowrap="nowrap" class="sanLR04 "><?=utf8_encode($rEntidad{'RFC'});?></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rEntidad{'CURP'});?></td>
					<td nowrap="nowrap" class="sanLR04 "><?=utf8_encode($rEntidad{'IMSS'});?></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=date("d/m/Y", strtotime($rEntidad{'FechaRegistro'}));?></td>
					<td nowrap="nowrap" class="sanLR04 "></td>
					<td nowrap="nowrap" class="sanLR04 columnB"></td>
					<td nowrap="nowrap" class="sanLR04 "></td>
					<td nowrap="nowrap" class="sanLR04 columnB"></td>
					<td nowrap="nowrap" class="sanLR04 "></td>
					<td nowrap="nowrap" class="sanLR04 columnB"></td>
					<td nowrap="nowrap" class="sanLR04 "></td>
					<td nowrap="nowrap" class="sanLR04 columnB"></td>
					<td nowrap="nowrap" class="sanLR04 "></td>
                    <td nowrap="nowrap" class="sanLR04 columnB" width="100%"></td>
				</tr>
			<?php $i++; } ?>
		</tbody>
	</table>
	</div>
<?php }else{ ?>
	<script type="text/javascript">
	dojo.require("dojo.number");
	dojo.require("dijit.form.NumberTextBox");
	dojo.require("dijit.form.DateTextBox");
	dojo.require("dojo.io.iframe");

	function generaExcel(){
		var UrlExcel = "php/excel/2.5.3.7.php?"+
		(dojo.byId('eCodEmpleado').value ? "&eCodEmpleado="+dojo.byId('eCodEmpleado').value : "")+
		(dojo.byId('fhFechaAsistenciaInicio').value ? "&fhFechaAsistenciaInicio="+dojo.byId('fhFechaAsistenciaInicio').value : "")+
		(dojo.byId('fhFechaAsistenciaFin').value ? "&fhFechaAsistenciaFin="+dojo.byId('fhFechaAsistenciaFin').value : "")+
		(dojo.byId('tOrden').value ? "&orden="+dojo.byId('tOrden').value : "")+
		(dojo.byId('limite').value ? "&limite="+dojo.byId('limite').value : "");
		var Datos = document.createElement("FORM");
		document.body.appendChild(Datos);
		Datos.name='Datos';
		Datos.method = "POST";
		Datos.action = UrlExcel;
		Datos.submit();
	}

	dojo.addOnLoad(function(){ 
		filtrarConsulta();
	});
	</script>
	<form name="Datos" id="Datos" method="post" action="" onsubmit="return false;">
		<input type="hidden" value="0" name="ePagina" id="ePagina" />
		<input type="hidden" value="" name="eAccion" id="eAccion" />
		<table width="900px" border="0">
			<tr>
				<td colpan="3" width="100%"></td>
				<td align="right" nowrap="nowrap"><a class="fntBR11" href="javaScript:mostrarFiltros('Busqueda');">Busqueda de Registros</a></td>
			</tr>
			<tr id="trBusqueda" style="display:none">
				<td colspan="4">
					<table width="100%" bgcolor="#f9f9f9">
						<tr><td class="sanLR04" height="5"></td></tr>
						<tr>
							<td class="sanLR04" height="20">C&oacute;digo</td>
							<td class="sanLR04" width="40%"><input type="text" name="eCodEmpleado" dojoType="dijit.form.TextBox" id="eCodEmpleado" value="" style="width:80px; height:25px;"></td>
							<td class="sanLR04" nowrap="nowrap">Fecha</td>
							<td class="sanLR04" width="50%" nowrap="nowrap"><input name="fhFechaAsistenciaInicio" id="fhFechaAsistenciaInicio" type="date"  required="false"  /> -	<input name="fhFechaAsistenciaFin" id="fhFechaAsistenciaFin" type="date"  required="false"  /></td>
						</tr>						
				        <tr>
			              <td nowrap="nowrap" class="sanLR04" height="5"><b>[Ordenamiento]</b></td>
			              <td nowrap="nowrap" class="sanLR04" width="50%"><input type="radio" name="orden"  id="orden" value="ASC" > ASCENDENTE / <input type="radio" name="orden" checked="checked"  id="orden" value="DESC" > DESENDENTE</td>
			              
			              <td nowrap="nowrap" class="sanLR04"><b>[Maximos Registros]:</b></td>
			              <td nowrap="nowrap" class="sanLR04" width="50%">
			              	<select name="limite" id="limite" style="width:175px">
                                  <option value="100">100</option>
                                  <option value="1000">1,000</option>
                                  <option value="10000">10,000</option>
                                  <option value="100000">100,000</option>
                          	</select>
                          </td>
            			</tr>
						<tr><td class="sanLR04" height="5"></td></tr>
					</table>
				</td>
			</tr>
			<tr><td colspan="4"><div id="dvCNS"></div></td></tr>
		</table>
	</form>	
<?php } ?>