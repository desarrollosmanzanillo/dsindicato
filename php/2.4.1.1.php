<?php require_once("conexion/soluciones-mysql.php");
$link = getLink();

if($_POST){
	if($_POST['eProceso']==1){
		$exito				= 1;
		$asistencias		= 0;
		$Turno				= ($_POST['turno']				? $_POST['turno']						: "NULL");
		$idEmpresa			= ($_POST['eCodEmisor']			? $_POST['eCodEmisor']					: "NULL");
		$idUsuario			= ($_POST['eUsuario']			? $_POST['eUsuario']					: "NULL");
		$eCodBuque			= ($_POST['eCodBuque']			? $_POST['eCodBuque']					: "NULL");
		$fhFechaAsistencia	= ($_POST['fhFechaAsistencia']	? "'".$_POST['fhFechaAsistencia']."'"	: "NULL");

		$select=" SELECT * ".
				" FROM sisconfiguracion ";
		$configura=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
		$fechaasis= date('Y-m-d',strtotime('+1 days', strtotime($_POST['fhFechaAsistencia'])));
		$domingo=date("w",strtotime($fechaasis));

		$select=" SELECT * ".
				" FROM catentidades ".
				" WHERE bPrincipal=1 ";
		$rEmisor=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

		$select=" SELECT eCodDia ".
				" FROM catdiasfestivos ".
				" WHERE ecodEstatus='AC' AND fhFecha=".$fhFechaAsistencia;
		$rFestivo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

		if((int)$_POST['eAccion']==100){
			if((int)$Turno==1){
				$pContinuacion=0;
				$diasAtras=7;
				$varfecha=isset($_POST["fhFechaAsistencia"]) ? $_POST["fhFechaAsistencia"] : date("Y-m-d");
				$fechaayer = date('Y-m-d',strtotime('-7 days', strtotime($varfecha)));
				$select=" SELECT * FROM sisconfiguracion ";
				$configura=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
				$select=" SELECT Turno3 AS Salario FROM categorias WHERE indice=".(int)$configura{'dCategoriaDescansos'};
				$salario=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

				while($diasAtras>1){
					$fechaAntier = date('Y-m-d',strtotime($fechaayer));
					$fechaayer   = date('Y-m-d',strtotime('+1 days', strtotime($fechaayer)));
					$fechahoy    = date('Y-m-d',strtotime('+2 days', strtotime($fechaAntier)));

					$select=" SELECT eCodDia ".
							" FROM catdiasfestivos ".
							" WHERE ecodEstatus='AC' AND fhFecha='".$fechaayer."'";
					$rFestivo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

					//////Asistencia Vacaciones//////
					$select=" SELECT empl.id, IFNULL(cv.dFactorIntegracion,ctipo.dFactorIntegracion) AS dFactorIntegracion, cat.Turno1, empl.eCodCategoriaVacacion, empl.eCodCategoriaFestivo, 
							empl.eCodEntidad
							FROM empleados empl ".
							" INNER JOIN cattipoempleado ctipo ON ctipo.eCodTipoEntidad=empl.eCodTipo ".
							" LEFT JOIN catvacaciones cv ON cv.eCodVacaciones=CASE WHEN DATE_ADD(empl.fhFechaIngreso, INTERVAL ROUND(DATEDIFF('".$fechaayer."', empl.fhFechaIngreso)/365) YEAR)<CURRENT_DATE OR ROUND(DATEDIFF('".$fechaayer."', empl.fhFechaIngreso)/365)=0 THEN ROUND(DATEDIFF('".$fechaayer."', empl.fhFechaIngreso)/365)+1 ELSE ROUND(DATEDIFF('".$fechaayer."', empl.fhFechaIngreso)/365) END AND 1=empl.eCodTipo ".
							" LEFT JOIN categorias cat ON cat.indice=IF(".(int)$rFestivo{'eCodDia'}.">0, IF(empl.eCodCategoriaFestivo>0, empl.eCodCategoriaFestivo, empl.eCodCategoriaVacacion), empl.eCodCategoriaVacacion)".
							" WHERE empl.eCodTipo=1 AND empl.Estado='AC' AND empl.FechaRegistro<='".$fechahoy."' ".
							" AND CASE WHEN empl.descanso=4 AND 4=".(date("w",strtotime($fechahoy))+1)." THEN 3 ELSE empl.descanso END<>".(date("w",strtotime($fechahoy))+1).
							" AND empl.id IN (SELECT eCodEmpleado FROM bitvacaciones WHERE fhFecha='".$fechahoy."' AND tCodEstatus='AC') ".
							" AND empl.id NOT IN (SELECT idEmpleado FROM asistencias WHERE Fecha='".$fechahoy."' AND Estado='AC') ".
							" AND empl.id NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechahoy."' AND FechaFinal>='".$fechahoy."') ".
							" AND empl.id NOT IN (SELECT eCodEmpleado FROM ausentismo WHERE fhFecha='".$fechahoy."' AND eCodEstatus='AC' AND eCodEmpleado=empl.id) ";
					print "<br>Asistencia Vacaciones- ".$select."<br>fechaayer".$fechaayer."<br>fechahoy".$fechahoy."<br>eCodDia".(int)$rFestivo{'eCodDia'};
					$rsVacaciones=mysqli_query($link,$select);
					while($rVacacion=mysqli_fetch_array($rsVacaciones,MYSQLI_ASSOC)){
						$insert=" INSERT INTO asistencias (idEmpleado, idCategorias, idEmpresa, area, Turno, Salario, Fecha, ".
								" fechaRegistro, idUsuario, Estado, dFactorIntegracion, eCodMotivoAsistencia) ".
								" SELECT ".$rVacacion{'id'}.", ".((int)$rFestivo{'eCodDia'}>0 ? ((int)$rVacacion{'eCodCategoriaFestivo'}>0 ? $rVacacion{'eCodCategoriaFestivo'} : $rVacacion{'eCodCategoria'}) : 13).", ".(int)$rVacacion{'eCodEntidad'}.", 0, 1, ".((float)$rVacacion{'Turno1'}*$rVacacion{'dFactorIntegracion'}).", '".$fechahoy."', ".
								" CURRENT_TIMESTAMP, ".$idUsuario.", 'AC', ".(float)$rVacacion{'dFactorIntegracion'}.", 3";
						print $insert."<br>";
					}
					//
					$diasAtras--;
				}
			}
			//
		}else{
			if((int)$_POST['eAccion']==0){
				foreach($_POST as $k => $v){
					$nombre = strval($k);
					if(strstr($nombre,"eRegistro") && $v && (int)$_POST['idEmpleado'.$v]>0){
						$eFila=str_replace("eRegistro", "", $nombre);
						$area 			= ($_POST['eCodCosto'.$eFila]	? $_POST['eCodCosto'.$eFila]	: "NULL");
						$idEmpleado 	= ($_POST['idEmpleado'.$eFila]	? $_POST['idEmpleado'.$eFila]	: "NULL");
						$idCategorias 	= ($_POST['idCategoria'.$eFila] ? $_POST['idCategoria'.$eFila]	: "NULL");
						$select=" SELECT Turno".$Turno." AS Salario, TurnoE".$Turno." AS SalarioE FROM categorias WHERE indice=".(int)$idCategorias;
						$salariocategoria=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

						$select=" SELECT empl.descanso, IFNULL(cv.dFactorIntegracion,ctipo.dFactorIntegracion) AS dFactorIntegracion, ctipo.eCodTipoEntidad, empl.bSinDescanso, rac.tURLArchivo ".
								" FROM empleados empl ".
								" INNER JOIN cattipoempleado ctipo ON ctipo.eCodTipoEntidad=empl.eCodTipo ".
								" LEFT JOIN relarchivoscsf rac ON rac.eCodEmpleado=empl.id ".
								" LEFT JOIN catvacaciones cv ON cv.eCodVacaciones=CASE WHEN DATE_ADD(empl.fhFechaIngreso, INTERVAL ROUND(DATEDIFF(".$fhFechaAsistencia.", empl.fhFechaIngreso)/365) YEAR)<CURRENT_DATE OR ROUND(DATEDIFF(".$fhFechaAsistencia.", empl.fhFechaIngreso)/365)=0 THEN ROUND(DATEDIFF(".$fhFechaAsistencia.", empl.fhFechaIngreso)/365)+1 ELSE ROUND(DATEDIFF(".$fhFechaAsistencia.", empl.fhFechaIngreso)/365) END AND 1=empl.eCodTipo ".
								" WHERE empl.id=".(int)$idEmpleado.
								" AND empl.id NOT IN (SELECT idEmpleado ".
								"					FROM asistencias ".
								"					WHERE Estado='AC' ".
								"					AND Turno=".(int)$Turno." AND Fecha=".$fhFechaAsistencia." AND idEmpleado=".(int)$idEmpleado.") ";
						$factor = mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
						$salario = ($factor{'eCodTipoEntidad'}==1 ? (float)$salariocategoria{'Salario'} : (float)$salariocategoria{'SalarioE'});
						$descanso = (int)$factor{'descanso'};
						if((int)$idCategorias>0){
							$SalarioNeto=((int)$factor{'bSinDescanso'}==0 && $descanso==$domingo && $domingo==1 //Día de Descanso y Domingo
								? ($salario*$factor{'dFactorIntegracion'})+($salario*((float)$configura{'dPrimaDominical'}/100))+($factor{'eCodTipoEntidad'}==1 ? $salario : 0)
								: ((int)$factor{'bSinDescanso'}==0 && $descanso==$domingo && $domingo!=1 //Día de Descanso y NO Domingo
									? ($salario*$factor{'dFactorIntegracion'})+($factor{'eCodTipoEntidad'}==1 ? $salario : 0) 
									: ($descanso!=$domingo && $domingo==1 //NO Día Descanso y Domingo
										? ($salario*$factor{'dFactorIntegracion'})+($salario*((float)$configura{'dPrimaDominical'}/100))
										: $factor{'dFactorIntegracion'}*$salario)))+((int)$rFestivo{'eCodDia'}>0 ? (float)$salario*2 : 0); //Normal

							$insert=" INSERT INTO asistencias (idEmpleado, idCategorias, idEmpresa, area, Turno, Salario, Fecha, ".
									" fechaRegistro, idUsuario, Estado, eCodCosto, dFactorIntegracion, eCodMotivoAsistencia, eCodBuque) ".
									" VALUES (".$idEmpleado.", ".$idCategorias.", ".$idEmpresa.", ".$area.", ".$Turno.", ".(float)$SalarioNeto.", 
									".$fhFechaAsistencia.", CURRENT_TIMESTAMP, ".$idUsuario.", 'AC', ".$area.", ".(float)$factor{'dFactorIntegracion'}.", 1, ".(int)$eCodBuque.") ";
							if($res=mysqli_query($link,$insert)){
								$cadena.=$insert;
								$asistencias++;
							}else{
								$cadena.=$insert;
								$exito=0;
							}
						}
					}
					if(strstr($nombre,"eRegistro") && $v && (int)$_POST['idEmpleado'.$v]>0){
					}
				}
			}
			if((int)$Turno==1){
				$pContinuacion=0;
				$diasAtras=7;
				$varfecha=isset($_POST["fhFechaAsistencia"]) ? $_POST["fhFechaAsistencia"] : date("Y-m-d");
				$fechaayer = date('Y-m-d',strtotime('-7 days', strtotime($varfecha)));
				$select=" SELECT * FROM sisconfiguracion ";
				$configura=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
				$select=" SELECT Turno3 AS Salario FROM categorias WHERE indice=".(int)$configura{'dCategoriaDescansos'};
				$salario=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

				while($diasAtras>1){
					$fechaAntier = date('Y-m-d',strtotime($fechaayer));
					$fechaayer   = date('Y-m-d',strtotime('+1 days', strtotime($fechaayer)));
					$fechahoy    = date('Y-m-d',strtotime('+2 days', strtotime($fechaAntier)));

					$select=" SELECT eCodDia ".
							" FROM catdiasfestivos ".
							" WHERE ecodEstatus='AC' AND fhFecha='".$fechaayer."'";
					$rFestivo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

					//Asistencia Garantia
					//Asistencia Falta
					if((int)$_POST['eAccion']==888){
						print "fechaAntier-".$fechaAntier." fechaayer-".$fechaayer." w-".(date("w",strtotime($fechaayer))+1);
					}
					$select=" SELECT empl.id, IFNULL(cv.dFactorIntegracion,ctipo.dFactorIntegracion) AS dFactorIntegracion, cat.Turno1, empl.eCodCategoria, empl.eCodCategoriaFestivo, 
							empl.eCodEntidad
							FROM empleados empl ".
							" INNER JOIN cattipoempleado ctipo ON ctipo.eCodTipoEntidad=empl.eCodTipo ".
							" LEFT JOIN catvacaciones cv ON cv.eCodVacaciones=CASE WHEN DATE_ADD(empl.fhFechaIngreso, INTERVAL ROUND(DATEDIFF('".$fechaAntier."', empl.fhFechaIngreso)/365) YEAR)<CURRENT_DATE OR ROUND(DATEDIFF('".$fechaAntier."', empl.fhFechaIngreso)/365)=0 THEN ROUND(DATEDIFF('".$fechaAntier."', empl.fhFechaIngreso)/365)+1 ELSE ROUND(DATEDIFF('".$fechaAntier."', empl.fhFechaIngreso)/365) END AND 1=empl.eCodTipo ".
							" LEFT JOIN categorias cat ON cat.indice=IF(".(int)$rFestivo{'eCodDia'}.">0, IF(empl.eCodCategoriaFestivo>0, empl.eCodCategoriaFestivo, empl.eCodCategoria), empl.eCodCategoria)".
							" WHERE empl.eCodTipo=1 AND empl.Estado='AC' AND empl.FechaRegistro<='".$fechaayer."' ".
							" AND CASE WHEN empl.descanso=4 AND 4=".(date("w",strtotime($fechaayer))+1)." THEN 3 ELSE empl.descanso END<>".(date("w",strtotime($fechaayer))+1).
							" AND empl.id NOT IN (SELECT eCodEmpleado FROM bitvacaciones WHERE fhFecha='".$fechaayer."' AND tCodEstatus='AC') ".
							" AND empl.id NOT IN (SELECT idEmpleado FROM asistencias WHERE Fecha='".$fechaayer."' AND Estado='AC') ".
							" AND empl.id NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."') ".
							" AND empl.id NOT IN (SELECT eCodEmpleado FROM ausentismo WHERE fhFecha='".$fechaayer."' AND eCodEstatus='AC' AND eCodEmpleado=empl.id) ";
					if((int)$_POST['eAccion']==888){
						print "Asistencia Falta- ".$select."<br>";
					}
					$rsGarantias=mysqli_query($link,$select);
					while($rGarantia=mysqli_fetch_array($rsGarantias,MYSQLI_ASSOC)){
						$insert=" INSERT INTO asistencias (idEmpleado, idCategorias, idEmpresa, area, Turno, Salario, Fecha, ".
								" fechaRegistro, idUsuario, Estado, dFactorIntegracion, eCodMotivoAsistencia) ".
								" SELECT ".$rGarantia{'id'}.", ".((int)$rFestivo{'eCodDia'}>0 ? ((int)$rGarantia{'eCodCategoriaFestivo'}>0 ? $rGarantia{'eCodCategoriaFestivo'} : $rGarantia{'eCodCategoria'}) : 13).", ".(int)$rGarantia{'eCodEntidad'}.", 0, 1, ".((float)$rGarantia{'Turno1'}*$rGarantia{'dFactorIntegracion'}).", '".$fechaayer."', ".
								" CURRENT_TIMESTAMP, ".$idUsuario.", 'AC', ".(float)$rGarantia{'dFactorIntegracion'}.", 2";
						if($res=mysqli_query($link,$insert)){
							$pContinuacion++;
							$cadena.=$insert;
							$asistencias++;
						}else{
							$cadena.=$insert;
							$exito=0;
						}
					}

					//Asistencia Ausentismo
					$select=" SELECT Turno3 AS Salario FROM categorias WHERE indice=".(int)$configura{'dCategoriaAusentismo'};
					$salarioau=mysqli_fetch_array(mysqli_query($link,$select));
					$select=" SELECT empl.id, IFNULL(cv.dFactorIntegracion,ctipo.dFactorIntegracion) AS dFactorIntegracion, cat.Turno1, empl.eCodEntidad,
							(SELECT eCodTipoAusentismo FROM ausentismo WHERE fhFecha='".$fechaayer."' AND eCodEstatus='AC' AND eCodEmpleado=empl.id LIMIT 1) AS eCodTipoAusentismo ".
							" FROM empleados empl ".
							" INNER JOIN cattipoempleado ctipo ON ctipo.eCodTipoEntidad=empl.eCodTipo ".
							" LEFT JOIN catvacaciones cv ON cv.eCodVacaciones=CASE WHEN DATE_ADD(empl.fhFechaIngreso, INTERVAL ROUND(DATEDIFF('".$fechaAntier."', empl.fhFechaIngreso)/365) YEAR)<CURRENT_DATE OR ROUND(DATEDIFF('".$fechaAntier."', empl.fhFechaIngreso)/365)=0 THEN ROUND(DATEDIFF('".$fechaAntier."', empl.fhFechaIngreso)/365)+1 ELSE ROUND(DATEDIFF('".$fechaAntier."', empl.fhFechaIngreso)/365) END AND 1=empl.eCodTipo ".
							" LEFT JOIN categorias cat ON cat.indice=IFNULL(empl.eCodCategoria,11) ".
							" WHERE empl.eCodTipo=1 AND empl.Estado='AC' AND empl.FechaRegistro<='".$fechaayer."' ".
							" AND empl.id NOT IN (SELECT eCodEmpleado FROM bitvacaciones WHERE fhFecha='".$fechaayer."' AND tCodEstatus='AC') ".
							" AND empl.id NOT IN (SELECT idEmpleado FROM asistencias WHERE Fecha='".$fechaayer."' AND Estado='AC') ".
							" AND empl.id NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."') ".
							" AND empl.id IN (SELECT eCodEmpleado FROM ausentismo WHERE fhFecha='".$fechaayer."' AND eCodEstatus='AC' AND eCodEmpleado=empl.id) ";
					if((int)$_POST['eAccion']==888){
						print "Asistencia Ausentismo- ".$select."<br>";
					}
					$ausentismos=mysqli_query($link,$select);
					while($rausentismo=mysqli_fetch_array($ausentismos,MYSQLI_ASSOC)){
						$insert=" INSERT INTO asistencias (idEmpleado, idCategorias, idEmpresa, area, Turno, Salario, Fecha, ".
								" fechaRegistro, idUsuario, Estado, dFactorIntegracion, eCodMotivoAsistencia) ".
								" SELECT ".$rausentismo{'id'}.", ".((int)$rausentismo{'eCodTipoAusentismo'}==1 ? 14 : 15).", ".(int)$rausentismo{'eCodEntidad'}.", 0, 1, ".((float)$rausentismo{'Turno1'}*$rausentismo{'dFactorIntegracion'}).", ".
								" '".$fechaayer."', CURRENT_TIMESTAMP, ".$idUsuario.", 'AC', ".(float)$rausentismo{'dFactorIntegracion'}.", 5";
						if($res=mysqli_query($link,$insert)){
							$pContinuacion++;
							$cadena.=$insert;
							$asistencias++;
						}else{
							$cadena.=$insert;
							$exito=0;
						}
					}

					//Asistencia Incapacidad
					$select=" SELECT Turno3 AS Salario FROM categorias WHERE indice=".(int)$configura{'dCategoriaIncapacidad'};
					$salarioin=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
					$select=" SELECT empl.id, IFNULL(cv.dFactorIntegracion,ctipo.dFactorIntegracion) AS dFactorIntegracion, empl.eCodEntidad
							FROM empleados empl
							INNER JOIN cattipoempleado ctipo ON ctipo.eCodTipoEntidad=empl.eCodTipo
							LEFT JOIN catvacaciones cv ON cv.eCodVacaciones=CASE WHEN DATE_ADD(empl.fhFechaIngreso, INTERVAL ROUND(DATEDIFF('".$fechaAntier."', empl.fhFechaIngreso)/365) YEAR)<CURRENT_DATE OR ROUND(DATEDIFF('".$fechaAntier."', empl.fhFechaIngreso)/365)=0 THEN ROUND(DATEDIFF('".$fechaAntier."', empl.fhFechaIngreso)/365)+1 ELSE ROUND(DATEDIFF('".$fechaAntier."', empl.fhFechaIngreso)/365) END AND 1=empl.eCodTipo
							WHERE empl.Estado='AC' AND empl.FechaRegistro<='".$fechaayer."' ".
							//" AND empl.descanso<>".(date("w",strtotime($fechaayer))+1).
							" AND empl.id NOT IN (SELECT eCodEmpleado FROM bitvacaciones WHERE fhFecha='".$fechaayer."' AND tCodEstatus='AC') ".
							" AND empl.id NOT IN (SELECT idEmpleado FROM asistencias WHERE Fecha='".$fechaayer."' AND Estado='AC') ".
							" AND empl.id IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."') ".
							" AND empl.id NOT IN (SELECT eCodEmpleado FROM ausentismo WHERE fhFecha='".$fechaayer."' AND eCodEstatus='AC' AND eCodEmpleado=empl.id) ";
					if((int)$_POST['eAccion']==888){
						print "Asistencia Incapacidad- ".$select."<br>";
					}
					$incapacidades=mysqli_query($link,$select);
					while($incapacidad=mysqli_fetch_array($incapacidades,MYSQLI_ASSOC)){
						$select=" SELECT ma.dImporte ".
								" FROM movimientosafiliatorios ma ".
								" INNER JOIN relasistenciamovimientos ras ON ras.eCodMovimiento=ma.eCodMovimiento ".
								" INNER JOIN asistencias asi ON asi.id=ras.eCodAsistencia ".
								" WHERE ma.eCodEstatus='AC' AND ma.fhFecha<'".$varfecha."' AND asi.idEmpleado=".(int)$incapacidad{'id'}.
								" ORDER BY ma.eCodMovimiento DESC LIMIT 1 ";
						$rSalario=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

						$insert=" INSERT INTO asistencias (idEmpleado, idCategorias, idEmpresa, area, Turno, Salario, Fecha,
								fechaRegistro, idUsuario, Estado, dFactorIntegracion, eCodMotivoAsistencia)
								SELECT ".$incapacidad{'id'}.", 17, ".(int)$incapacidad{'eCodEntidad'}.", 0, 3, ".(float)$rSalario{'dImporte'}.", '".$fechaayer."', ".
								" CURRENT_TIMESTAMP, ".$idUsuario.", 'AC', ".$incapacidad{'dFactorIntegracion'}.", 4";
						if($res=mysqli_query($link,$insert)){
							$cadena.=$insert;
							$asistencias++;
							$select=" select last_insert_id() AS Llave ";
							$rCodigo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
							$eCodAsistencia=(int)$rCodigo['Llave'];

							$insert=" INSERT INTO movimientosafiliatorios (eCodUsuario, fhFecha, tCodTipoMovimiento, eCodEstatus, dImporte, tFolio, fhFechaRegistro) ".
									" VALUES (".$idUsuario.", '".$fechaayer."', '01', 'AC', ".(float)$rSalario{'dImporte'}.", 'NA', CURRENT_TIMESTAMP) ";
							if($res=mysqli_query($link,$insert)){
								$cadena.=$insert;
								$select=" select last_insert_id() AS Llave ";
								$rCodigo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
								$eCodMovimiento=(int)$rCodigo['Llave'];
								$insertt=" INSERT INTO relasistenciamovimientos (eCodAsistencia, eCodMovimiento) ".
										" VALUES (".$eCodAsistencia.", ".$eCodMovimiento.") ";
								if($res=mysqli_query($link,$insertt)){
									$exito=1;
								}else{
									$exito=0;
								}
							}else{
								$cadena.=$insert;
								$exito=0;
							}
						}else{
							$cadena.=$insert;
							$exito=0;
						}
					}

					//////Asistencia Vacaciones//////
					$select=" SELECT empl.id, IFNULL(cv.dFactorIntegracion,ctipo.dFactorIntegracion) AS dFactorIntegracion, cat.Turno1, empl.eCodCategoriaVacacion, empl.eCodCategoriaFestivo, 
							empl.eCodEntidad
							FROM empleados empl ".
							" INNER JOIN cattipoempleado ctipo ON ctipo.eCodTipoEntidad=empl.eCodTipo ".
							" LEFT JOIN catvacaciones cv ON cv.eCodVacaciones=CASE WHEN DATE_ADD(empl.fhFechaIngreso, INTERVAL ROUND(DATEDIFF('".$fechaayer."', empl.fhFechaIngreso)/365) YEAR)<CURRENT_DATE OR ROUND(DATEDIFF('".$fechaayer."', empl.fhFechaIngreso)/365)=0 THEN ROUND(DATEDIFF('".$fechaayer."', empl.fhFechaIngreso)/365)+1 ELSE ROUND(DATEDIFF('".$fechaayer."', empl.fhFechaIngreso)/365) END AND 1=empl.eCodTipo ".
							" LEFT JOIN categorias cat ON cat.indice=IF(".(int)$rFestivo{'eCodDia'}.">0, IF(empl.eCodCategoriaFestivo>0, empl.eCodCategoriaFestivo, empl.eCodCategoriaVacacion), empl.eCodCategoriaVacacion)".
							" WHERE empl.eCodTipo=1 AND empl.Estado='AC' AND empl.FechaRegistro<='".$fechahoy."' ".
							" AND CASE WHEN empl.descanso=4 AND 4=".(date("w",strtotime($fechahoy))+1)." THEN 3 ELSE empl.descanso END<>".(date("w",strtotime($fechahoy))+1).
							" AND empl.id IN (SELECT eCodEmpleado FROM bitvacaciones WHERE fhFecha='".$fechahoy."' AND tCodEstatus='AC') ".
							" AND empl.id NOT IN (SELECT idEmpleado FROM asistencias WHERE Fecha='".$fechahoy."' AND Estado='AC') ".
							" AND empl.id NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechahoy."' AND FechaFinal>='".$fechahoy."') ".
							" AND empl.id NOT IN (SELECT eCodEmpleado FROM ausentismo WHERE fhFecha='".$fechahoy."' AND eCodEstatus='AC' AND eCodEmpleado=empl.id) ";
					print "<br>Asistencia Vacaciones- ".$select."<br>fechaayer".$fechaayer."<br>fechahoy".$fechahoy."<br>eCodDia".(int)$rFestivo{'eCodDia'};
					$rsVacaciones=mysqli_query($link,$select);
					while($rVacacion=mysqli_fetch_array($rsVacaciones,MYSQLI_ASSOC)){
						$insert=" INSERT INTO asistencias (idEmpleado, idCategorias, idEmpresa, area, Turno, Salario, Fecha, ".
								" fechaRegistro, idUsuario, Estado, dFactorIntegracion, eCodMotivoAsistencia) ".
								" SELECT ".$rVacacion{'id'}.", 34, ".(int)$rVacacion{'eCodEntidad'}.", 0, 1, ".(float)$rVacacion{'Turno1'}*$rVacacion{'dFactorIntegracion'}.", '".$fechahoy."', ".
								" CURRENT_TIMESTAMP, ".$idUsuario.", 'AC', ".(float)$rVacacion{'dFactorIntegracion'}.", 3";
						//print $insert."<br>";
						if($res=mysqli_query($link,$insert)){
							$pContinuacion++;
							$cadena.=$insert;
							$asistencias++;
						}else{
							$cadena.=$insert;
							$exito=0;
						}
					}
					//
					$diasAtras--;
				}

				if((int)$pContinuacion>0 || (int)$_POST['eAccion']==20){
					//$fechaayer = date('Y-m-d',strtotime($_POST['fhFechaAsistencia']));
	        		//$fechaantier = date('Y-m-d',strtotime('-1 days', strtotime($_POST['fhFechaAsistencia'])));
	        		$fechaayer = date('Y-m-d',strtotime('-1 days', strtotime($_POST['fhFechaAsistencia'])));
	        		$fechaantier = date('Y-m-d',strtotime('-2 days', strtotime($_POST['fhFechaAsistencia'])));
					//////Inicio Continuaciones-->
				    $select=" SELECT * ".
				            " FROM categorias ".
				            " WHERE indice=11 ";
				    $salarioMin=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

				    $select=" SELECT idEmpleado ".
				            " FROM incapacidades ".
				            " WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."' ";
				    $incapacidades=mysqli_query($link,$select);
				    $UsuariosInc="0";
				    while($incapacidad=mysqli_fetch_array($incapacidades,MYSQLI_ASSOC)){
				        $UsuariosInc.=", ".$incapacidad{'idEmpleado'};
				    }
				    $select=" SELECT assi.id ".
				            " FROM movimientosafiliatorios mos ".
				            " INNER JOIN relasistenciamovimientos rassi ON rassi.eCodMovimiento=mos.eCodMovimiento ".
				            " INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia ".
				            " LEFT JOIN empleados AS emp ON assi.idEmpleado = emp.id ".
				            " WHERE 1=1 ".
				            " AND assi.Fecha Between '".$fechaantier."' AND '".$fechaayer."' ".
				            ((int)$_POST['eCodTipoEntidad']>0 ? " AND emp.eCodTipo=".(int)$_POST['eCodTipoEntidad'] : "");
				    $afiliatorios=mysqli_query($link,$select);
				    $UsuariosAfil="0";
				    while($afiliatorio=mysqli_fetch_array($afiliatorios,MYSQLI_ASSOC)){
				        $UsuariosAfil.=", ".$afiliatorio{'id'};
				    }
				    $select=" SELECT assihh.idEmpleado ".
				            " FROM movimientosafiliatorios moshh ".
				            " INNER JOIN relasistenciamovimientos rassihh ON rassihh.eCodMovimiento=moshh.eCodMovimiento ".
				            " INNER JOIN asistencias assihh ON assihh.id=rassihh.eCodAsistencia ".
				            " LEFT JOIN empleados AS emp ON assihh.idEmpleado=emp.id ".
				            " WHERE moshh.fhFecha='".$fechaantier."' AND moshh.tCodTipoMovimiento NOT IN ('02') AND moshh.eCodEstatus='AC' ".
				            ((int)$_POST['eCodTipoEntidad']>0 ? " AND emp.eCodTipo=".(int)$_POST['eCodTipoEntidad'] : "");
				    $movimientos=mysqli_query($link,$select);
				    $UsiariosMovi="0";
				    while($movimiento=mysqli_fetch_array($movimientos,MYSQLI_ASSOC)){
				        $UsiariosMovi.=", ".$movimiento{'idEmpleado'};
				    }

				    $select=" SELECT DISTINCT emple.id AS numeral, emple.IMSS AS imss, cent.tRegistroPatronal AS rp, emple.Paterno AS pat, ".
				            " Materno AS mat, Nombre AS nomb, ctiemp.eCodTipoJornada AS tipjo, '".$fechaayer."' AS Fecha, emple.Clinica AS clin, emple.CURP AS curp, ".
				" CASE WHEN (SELECT MAX(asish.Salario+asish.dBono)
				    FROM asistencias asish 
				    WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
				    AND asish.Estado='AC' 
				    AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
				    AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
				    AND asish.id NOT IN (".$UsuariosAfil.")
				    AND asish.idEmpleado IN (".$UsiariosMovi.")
				    AND asish.idEmpleado=asis.idEmpleado)>".(float)$configura{'dTopeSalario'}." THEN ".(float)$configura{'dTopeSalario'}.
				" ELSE CASE WHEN (SELECT MAX(asish.Salario+asish.dBono)
				        FROM asistencias asish 
				        WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
				        AND asish.Estado='AC' 
				        AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
				        AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
				        AND asish.id NOT IN (".$UsuariosAfil.")
				        AND asish.idEmpleado IN (".$UsiariosMovi.")
				        AND asish.idEmpleado=asis.idEmpleado)=0 THEN FORMAT((".(float)$salarioMin{'Turno1'}."*ctiemp.dFactorIntegracion),2) ".
				        " ELSE CASE WHEN (SELECT MAX(asish.Salario+asish.dBono)
				            FROM asistencias asish 
				            WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
				            AND asish.Estado='AC' 
				            AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
				            AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
				            AND asish.id NOT IN (".$UsuariosAfil.")
				            AND asish.idEmpleado IN (".$UsiariosMovi.")
				            AND asish.idEmpleado=asis.idEmpleado)<".$configura{'dSalarioMinimo'}." THEN ".$configura{'dSalarioMinimo'}.
				            " ELSE (SELECT MAX(asish.Salario+asish.dBono)
				                FROM asistencias asish 
				                WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
				                AND asish.Estado='AC' 
				                AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
				                AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
				                AND asish.id NOT IN (".$UsuariosAfil.")
				                AND asish.idEmpleado IN (".$UsiariosMovi.")
				                AND asish.idEmpleado=asis.idEmpleado)
				            END 
				        END
				    END AS tSalario ".
				    " FROM asistencias asis ".
				    " INNER JOIN empleados emple ON emple.id=asis.idEmpleado ".
				    " INNER JOIN cattipoempleado ctiemp ON ctiemp.eCodTipoEntidad=emple.eCodTipo ".
				    " LEFT JOIN catentidades cent ON cent.eCodEntidad=asis.idEmpresa ".
				    " WHERE asis.Estado='AC' AND asis.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asis.id) ".
				    " AND asis.idEmpleado NOT IN (SELECT assi.idEmpleado FROM movimientosafiliatorios mos INNER JOIN relasistenciamovimientos rassi ON ".
				    " rassi.eCodMovimiento=mos.eCodMovimiento INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia WHERE assi.id=asis.id) ".
				    " AND asis.Fecha Between '".$fechaantier."' AND '".$fechaayer."' AND asis.idCategorias NOT IN (33) ".((int)$_POST['eCodTipoEntidad']>0 ? " AND emple.eCodTipo=".(int)$_POST['eCodTipoEntidad'] : "")." AND asis.idEmpleado NOT IN (".$UsuariosInc.") ".
				    "AND asis.idEmpleado IN (SELECT assi.idEmpleado FROM movimientosafiliatorios mos INNER JOIN relasistenciamovimientos rassi ON rassi.eCodMovimiento=mos.eCodMovimiento ".
				    " INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia WHERE assi.idEmpleado=asis.idEmpleado AND mos.fhFecha='".$fechaantier."' ".
				    " AND mos.tCodTipoMovimiento NOT IN ('02') AND mos.eCodEstatus='AC') AND ".
				" IFNULL((SELECT mosa.dImporte
				FROM movimientosafiliatorios mosa
				INNER JOIN relasistenciamovimientos rassia ON rassia.eCodMovimiento=mosa.eCodMovimiento
				INNER JOIN asistencias assia ON assia.id=rassia.eCodAsistencia
				WHERE assia.idEmpleado=asis.idEmpleado AND mosa.fhFecha='".$fechaantier."' AND mosa.tCodTipoMovimiento NOT IN ('02') AND mosa.eCodEstatus='AC' LIMIT 1),0)
				= 
				CASE WHEN (SELECT MAX(asish.Salario+asish.dBono)
				FROM asistencias asish 
				WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
				AND asish.Estado='AC' 
				AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
				AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
				AND asish.id NOT IN (".$UsuariosAfil.")
				AND asish.idEmpleado IN (".$UsiariosMovi.")
				AND asish.idEmpleado=asis.idEmpleado)>".(float)$configura{'dTopeSalario'}." THEN ".(float)$configura{'dTopeSalario'}." ELSE ".
				    " CASE WHEN (SELECT MAX(asish.Salario+asish.dBono)
				FROM asistencias asish 
				WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
				AND asish.Estado='AC' 
				AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
				AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
				AND asish.id NOT IN (".$UsuariosAfil.")
				AND asish.idEmpleado IN (".$UsiariosMovi.")
				AND asish.idEmpleado=asis.idEmpleado)=0 THEN FORMAT((".(float)$salarioMin{'Turno1'}."*ctiemp.dFactorIntegracion),2) ELSE ".
				    " CASE WHEN (SELECT MAX(asish.Salario+asish.dBono)
				FROM asistencias asish 
				WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
				AND asish.Estado='AC' 
				AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
				AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
				AND asish.id NOT IN (".$UsuariosAfil.")
				AND asish.idEmpleado IN (".$UsiariosMovi.")
				AND asish.idEmpleado=asis.idEmpleado)<".$configura{'dSalarioMinimo'}." THEN ".$configura{'dSalarioMinimo'}." ELSE (SELECT MAX(asish.Salario+asish.dBono)
				FROM asistencias asish 
				WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
				AND asish.Estado='AC' 
				AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
				AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
				AND asish.id NOT IN (".$UsuariosAfil.")
				AND asish.idEmpleado IN (".$UsiariosMovi.")
				AND asish.idEmpleado=asis.idEmpleado) END ".
				    " END ".
				    " END
				UNION 
				SELECT DISTINCT emple.id AS numeral, emple.IMSS AS imss, cent.tRegistroPatronal AS rp, emple.Paterno AS pat, Materno AS mat, Nombre AS nomb, ctiemp.eCodTipoJornada AS tipjo, '".$fechaayer."' AS Fecha, emple.Clinica AS clin, emple.CURP AS curp, CASE WHEN (SELECT mosa.dImporte
				FROM movimientosafiliatorios mosa 
				INNER JOIN relasistenciamovimientos rassia ON rassia.eCodMovimiento=mosa.eCodMovimiento 
				INNER JOIN asistencias assia ON assia.id=rassia.eCodAsistencia
				WHERE assia.idEmpleado=asis.idEmpleado AND mosa.fhFecha='".$fechaantier."' AND mosa.tCodTipoMovimiento NOT IN ('02') AND mosa.eCodEstatus='AC' LIMIT 1)>".(float)$configura{'dTopeSalario'}." THEN ".(float)$configura{'dTopeSalario'}." ELSE (SELECT mosa.dImporte
				FROM movimientosafiliatorios mosa 
				INNER JOIN relasistenciamovimientos rassia ON rassia.eCodMovimiento=mosa.eCodMovimiento 
				INNER JOIN asistencias assia ON assia.id=rassia.eCodAsistencia
				WHERE assia.idEmpleado=asis.idEmpleado AND mosa.fhFecha='".$fechaantier."' AND mosa.tCodTipoMovimiento NOT IN ('02') AND mosa.eCodEstatus='AC' LIMIT 1) END AS tSalario
				FROM asistencias asis
				INNER JOIN empleados emple ON emple.id=asis.idEmpleado
				INNER JOIN cattipoempleado ctiemp ON ctiemp.eCodTipoEntidad=emple.eCodTipo
				LEFT JOIN catentidades cent ON cent.eCodEntidad=asis.idEmpresa
				WHERE asis.Fecha Between '".$fechaantier."' AND '".$fechaayer."' 
				AND asis.Estado='AC' 
				AND asis.idEmpleado NOT IN (".$UsuariosInc.") 
				AND asis.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asis.id) 
				AND asis.idEmpleado NOT IN (SELECT assi.idEmpleado 
				FROM movimientosafiliatorios mos 
				INNER JOIN relasistenciamovimientos rassi ON rassi.eCodMovimiento=mos.eCodMovimiento 
				INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia
				WHERE assi.id=asis.id)
				AND asis.idEmpleado IN (SELECT assi.idEmpleado 
				FROM movimientosafiliatorios mos 
				INNER JOIN relasistenciamovimientos rassi ON rassi.eCodMovimiento=mos.eCodMovimiento 
				INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia
				WHERE assi.idEmpleado=asis.idEmpleado AND mos.fhFecha='".$fechaantier."' AND mos.tCodTipoMovimiento NOT IN ('02') AND mos.eCodEstatus='AC')
				AND asis.idEmpleado IN (SELECT eCodEmpleado FROM ausentismo WHERE fhFecha='".$fechaayer."' AND eCodEstatus='AC' AND eCodEmpleado=asis.idEmpleado)
				AND asis.idCategorias NOT IN (33) ".((int)$_POST['eCodTipoEntidad']>0 ? " AND emple.eCodTipo=".$_POST['eCodTipoEntidad'] : "");
				$vContinua=$select;
				if((int)$_POST['eAccion']==20){
					print "Continuacion-".$select."<br>";
				}
			    $continuaciones=mysqli_query($link,$select);
			    $select=" SELECT * FROM sisconfiguracion ";
			    $configura2=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
			    while($continua=mysqli_fetch_array($continuaciones,MYSQLI_ASSOC)){
			        $dSalarioR=((float)$continua{'tSalario'}>(float)$configura2{'dTopeSalario'} ? (float)$configura2{'dTopeSalario'} : (float)$continua{'tSalario'});
			        //print "Empleado: ".$idUsuario." val->".$dSalarioR."==".(float)$configura2{'dTopeSalarioMinimoDescanso'}."<br>";
			        //if($dSalarioR==(float)$configura2{'dTopeSalarioMinimoDescanso'}){
			            //" VALUES (".$idUsuario.", '".$fechaayer."', '01', 'AC', ".(float)$rAsistencia{'tSalario'}.", 'NULL', CURRENT_TIMESTAMP) ";
			            $insert=" INSERT INTO movimientosafiliatorios (eCodUsuario, fhFecha, tCodTipoMovimiento,  eCodEstatus, dImporte, tFolio, fhFechaRegistro) ".                        
			                    " VALUES (".$idUsuario.", '".$fechaayer."', '01', 'AC', ".$dSalarioR." , 'NA', CURRENT_TIMESTAMP) ";
			            if($res=mysqli_query($link,$insert)){
			                $exito=1;
			                $select=" select last_insert_id() AS Llave ";
			                $rCodigo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
			                $eCodMovimiento=(int)$rCodigo['Llave'];

			                $select=" SELECT asish.id
				FROM asistencias asish 
				WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
				AND asish.Estado='AC' 
				AND asish.idEmpleado NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."') 
				AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
				AND asish.idEmpleado NOT IN (SELECT assih.idEmpleado 
				FROM movimientosafiliatorios mosh 
				INNER JOIN relasistenciamovimientos rassih ON rassih.eCodMovimiento=mosh.eCodMovimiento 
				INNER JOIN asistencias assih ON assih.id=rassih.eCodAsistencia
				WHERE assih.id=asish.id)
				AND asish.idEmpleado IN (SELECT assihh.idEmpleado 
				FROM movimientosafiliatorios moshh 
				INNER JOIN relasistenciamovimientos rassihh ON rassihh.eCodMovimiento=moshh.eCodMovimiento 
				INNER JOIN asistencias assihh ON assihh.id=rassihh.eCodAsistencia
				WHERE assihh.idEmpleado=asish.idEmpleado AND moshh.fhFecha='".$fechaantier."' AND moshh.tCodTipoMovimiento NOT IN ('02') AND moshh.eCodEstatus='AC')
				AND asish.idEmpleado=".$continua{'numeral'};
				                $asistenciasEmp=mysqli_query($link,$select);
				                while($asisEmp=mysqli_fetch_array($asistenciasEmp,MYSQLI_ASSOC)){
				                    $insertt=" INSERT INTO relasistenciamovimientos (eCodAsistencia, eCodMovimiento) ".
				                            " VALUES (".$asisEmp{'id'}.", ".$eCodMovimiento.") ";
				                    if($res=mysqli_query($link,$insertt)){
				                        $exito=1;
				                    }else{
				                        $exito=0;
				                    }
				                }
				            }else{
				                $exito=0;
				            }
				        //}
				    } 
				    //////<--Fin Continuaciones idCategorias
				}
				///
			}
		}
		print "<input type=\"hidden\" value=\"".((int)$exito>0 && (int)$asistencias>0 && (int)$_POST['eAccion']==0 ? 1 : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}

	if($_POST['eProceso']==2){
		$i 			= 1;
		$eTurno 	= (int)$_POST['turno'];
		$eDobletes 	= 0;
		$fFechaTurno= $_POST['fhFechaAsistencia'];
		while($i>0 && $eTurno>0){
			if($eTurno==1){
				$eTurno=3;
				$select=" SELECT DATE_ADD('".$fFechaTurno."' ,INTERVAL -1 DAY) AS fecha ";
				$rFecha=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
				$fFechaTurno=$rFecha{'fecha'};
			}else{
				$eTurno--;
			}
			$select=" SELECT id ".
					" FROM asistencias ".
					" WHERE Estado LIKE 'AC' AND Fecha='".$fFechaTurno."' AND Turno=".(int)$eTurno.
					" AND idEmpleado=".(int)$_POST['idEmpleado'.(int)$_POST['eFilaEmp']];
			$rDoblete=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
			if((int)$_POST['eUsuario']==1){
				
			}

			$i=0;
			if((int)$rDoblete{'id'}>0){
				$i=1;
				$eDobletes++;
			}
			//
		}
		$select=" SELECT eCodVacacion ".
				" FROM bitvacaciones ".
				" WHERE tCodEstatus='AC' AND fhFecha='".$_POST['fhFechaAsistencia']."' ".
				" AND eCodEmpleado=".(int)$_POST['idEmpleado'.(int)$_POST['eFilaEmp']];

		$eVacacion=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

		$select=" SELECT IFNULL(eCodigoPostal, 0) eCodigoPostal ".
				" FROM empleados ".
				" WHERE Estado='AC' ".
				" AND id=".(int)$_POST['idEmpleado'.(int)$_POST['eFilaEmp']];

		$eCodigoPostal=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

		$select=" SELECT id ".
				" FROM asistencias ".
				" WHERE Estado LIKE 'AC' AND Fecha='".$_POST['fhFechaAsistencia']."' AND Turno=".(int)$_POST['turno'].
				" AND idEmpleado=".(int)$_POST['idEmpleado'.(int)$_POST['eFilaEmp']];

		$asistencia=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

		$select=" SELECT id ".
				" FROM incapacidades ".
				" WHERE Estado LIKE 'AC' AND idEmpleado=".(int)$_POST['idEmpleado'.(int)$_POST['eFilaEmp']].
				" AND FechaInicial<='".$_POST['fhFechaAsistencia']."' AND FechaFinal>='".$_POST['fhFechaAsistencia']."'";

		$incapacidad=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

		$select=" SELECT id ".
				" FROM asistencias ".
				" WHERE Estado LIKE 'AC' AND idEmpleado=".(int)$_POST['idEmpleado'.(int)$_POST['eFilaEmp']].
				" AND Fecha='".$_POST['fhFechaAsistencia']."' AND idCategorias=16 ";

		$descanso=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
		print "<input type=\"hidden\" name=\"eVacacion\" id=\"eVacacion\" value=\"".(int)$eVacacion."\">";
		print "<input type=\"hidden\" name=\"eCodigoPostal\" id=\"eCodigoPostal\" value=\"".(int)$eCodigoPostal{'eCodigoPostal'}."\">";
		print "<input type=\"hidden\" name=\"eDobletes\" id=\"eDobletes\" value=\"".(int)$eDobletes."\">";
		print "<input type=\"hidden\" name=\"eEmpleadoBD\" id=\"eEmpleadoBD\" value=\"".((int)$_POST['eAccion']==0 ? (int)$asistencia{'id'} : "")."\">";
		print "<input type=\"hidden\" name=\"eIncapacidad\" id=\"eIncapacidad\" value=\"".((int)$_POST['eAccion']==0 ? (int)$incapacidad{'id'} : "")."\">";
		print "<input type=\"hidden\" name=\"eDescanso\" id=\"eDescanso\" value=\"".((int)$_POST['eAccion']==0 ? (int)$descanso{'id'} : "")."\">";
	}
//
}
if(!$_POST){
//
$select=" SELECT * ".
		" FROM catentidades ".
		" WHERE bPrincipal=1 ";
$rsEmisores=mysqli_query($link,$select);

$select=" SELECT * ".
		" FROM catcomprobantes WHERE tCodEstatus='AC' ".
		" ORDER BY tNombre ASC ";
$rsComprobantes=mysqli_query($link,$select); 

$select=" SELECT * ".
		" FROM catformaspago ".
		" WHERE tCodEstatus='AC' ".
		" ORDER BY tNombre ";
$rsFormasPagos=mysqli_query($link,$select);

$select=" SELECT * ".
		" FROM catmetodospago ".
		" WHERE tCodEstatus='AC' ".
		" ORDER BY tNombre ";
$rsMetodosPagos=mysqli_query($link,$select);

$select=" SELECT * ".
		" FROM catentidades ".
		" WHERE tCodEstatus='AC' ".
		" AND eCodTipoEntidad=1 ".
		" ORDER BY tNombre ";
$rsClientes=mysqli_query($link,$select); 

$select=" SELECT * ".
		" FROM catcostos WHERE tCodEstatus='AC' ".
		" ORDER BY tSiglas ";
$rsCostos=mysqli_query($link,$select); 

$select=" SELECT * 
		FROM categorias 
		WHERE tCodEstatus='AC' 
		AND indice NOT IN (14, 15) 
		ORDER BY Categoria ASC ";
$rsCategorias=mysqli_query($link,$select); ?>
<style>
.eNumero{
	text-align:right;
}
</style>
<script src="../js/dojo/dijit/form/FilteringSelect.js" type="text/javascript"></script> 
<script src="../js/dojo/dojo/data/ItemFileReadStore.js" type="text/javascript"></script> 
<script src="../js/dojo/dijit/form/FilteringSelect.js" type="text/javascript"></script> 
<script src="../js/dojo/dijit/TooltipDialog.js" type="text/javascript"></script> 
<script type="text/javascript">
dojo.require("dojo.number");
dojo.require("dijit.form.NumberTextBox");
dojo.require("dijit.form.DateTextBox");
dojo.require("dojo.io.iframe");

function guardar(){
	dojo.byId('eProceso').value=1;
	var bandera = false;
	var bServicios = false;
	var bMercancias = false;
	var eTipoSolicitud = 0;
	mensaje = "¡Verifique lo siguiente!\n";

	var ultimafila=0;
	var ultimafilaV=0;

	dojo.query("[id^=\"eRegistro\"]").forEach(function(nodo, index, array){
		ultimafila=parseInt(nodo.value);
		ultimafilaV++;
	});
	if (!dojo.byId("eCodEmisor").value){
		mensaje+="* Registro Patronal\n";
		bandera = true;		
	}
	if (!dojo.byId("turno").value){
		mensaje+="* Turno\n";
		bandera = true;
	}
	if (!dojo.byId("fhFechaAsistencia").value){
		mensaje+="* Fecha de Asistencia\n";
		bandera = true;		
	}else{
		if(parseFloat(dojo.byId("fhFechaAsistencia").value.replace(/-/gi, ""))>parseFloat(dojo.byId('eFDiaria').value)){
			mensaje+="* No Puede Guardar Asistencias Anticipadas\n";
			bandera = true;
		}
	}
	if(parseInt(ultimafilaV)<=1){
		mensaje+="* Asistencias\n";
		bandera = true;		
	}else{
		var bIncompleto=false;
		var bEmpleado=false;
		var bCategoria=false;
		var bCosto=false;
		dojo.query("[id^=\"eRegistro\"]").forEach(function(nodo, index, array){
			var eFilaValida=parseInt(nodo.value);
			if(ultimafila>eFilaValida){
				if(!dijit.byId('idEmpleado'+eFilaValida).value){
					bEmpleado=true;
					bIncompleto=true;
				}
				if(!dojo.byId('idCategoria'+eFilaValida).value){
					bIncompleto=true;
					bCategoria=true;
				}
				/*if(!dojo.byId('eCodCosto'+eFilaValida).value){
					bCosto=true;
					bIncompleto=true;
				}*/
			}else{
				if(dijit.byId('idEmpleado'+eFilaValida).value){
					bEmpleado=true;
					bIncompleto=true;
				}
				if(dojo.byId('idCategoria'+eFilaValida).value){
					bIncompleto=true;
					bCategoria=true;
				}
				/*if(dojo.byId('eCodCosto'+eFilaValida).value){
					bCosto=true;
					bIncompleto=true;
				}*/
			}
		});
		if(bIncompleto==true){
			mensaje+="* Conceptos incompletos\n";
			if(bEmpleado==true){
				mensaje+="   Empleado\n";
			}
			if(bCategoria==true){
				mensaje+="   Categoría\n";
			}
			if(bCosto==true){
				mensaje+="   Centro de Costo\n";
			}
			bandera = true;
		}
	}

	if (bandera==true){
		alert(mensaje);
	}else{
		if (confirm("¿Desea Guardar la Información?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				if(dojo.byId("eCodigo").value>0){
					alert("¡La información se guardó correctamente!");
					tURL = './?ePagina=2.5.1.1.php';
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
		}
	}
}

function borrarAsistencia(eFilaAsistencia){
	var ultimafila=0;
	dojo.query("[id^=\"eRegistro\"]").forEach(function(nodo, index, array){
		ultimafila=parseInt(nodo.id.replace('eRegistro',''));
	});

	if(eFilaAsistencia!=ultimafila){
		if(confirm("¿Seguro de eliminar la asistencia?")){
			dojo.destroy(dojo.byId('filaAsistencia'+eFilaAsistencia));
		}
	}
}

function verificarEvtos(){

}

function verificarEmpleados(){
	dojo.query("[id^=\"idEmpleado\"]").forEach(function(nodo, index, array){
		var filaEmpleado=nodo.id.replace('idEmpleado','');
		if(parseInt(filaEmpleado)>0){
			validarEmpleado(filaEmpleado);
		}
	});
}

function limpiarFila(fila){
	dojo.byId('idEmpleado'+fila).value="";
	dijit.byId('idEmpleado'+fila).value="";
	dojo.byId('dvTurnosS'+fila).innerHTML="";
	dojo.byId('idEmpleado'+fila).focus();
}

function validarEmpleado(fila){
	dojo.byId('eProceso').value=2; 
	dojo.byId('eFilaEmp').value=fila;
	dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
		dojo.byId("dvCNS").innerHTML = tRespuesta;
		dojo.byId('dvTurnosS'+fila).innerHTML=parseInt(dojo.byId('eDobletes').value);
		if(parseInt(dijit.byId('idEmpleado'+fila).value)>0){
			if(parseInt(dojo.byId('eCodigoPostal').value)==0){
				alert("¡El empleado no cuenta con Codigo postal\n");
			}
			if(parseInt(dojo.byId('eVacacion').value)>0){
				alert("¡El empleado esta de vacaciones!");
				limpiarFila(fila);
			}else{
				if(parseInt(dojo.byId('eIncapacidad').value)>0){
					alert("¡El empleado cuenta con registro de incapacidad!");
					limpiarFila(fila);
				}else{
					if(parseInt(dojo.byId("eEmpleadoBD").value)>0){
						alert("¡El empleado ya tiene asistencia en el turno!");
						limpiarFila(fila);
					}else{
						if(parseInt(dojo.byId("eDobletes").value)==2 || parseInt(dojo.byId("eDobletes").value)>2){
							alert("¡El empleado ya va por el tercer turno!");
							limpiarFila(fila);
						}else{
							if(parseInt(dojo.byId('eDescanso').value)>0){
								if (confirm("¡El empleado cuenta con registro de descanso! ¿Desea Continuar?")){
									validarFila(fila);
								}else{
									limpiarFila(fila);
								}
							}else{
								validarFila(fila);
							}
						}
					}
				}
			}
		}
	}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
}

function agregarConcepto(filaAsistencia){ 
	var tb = dojo.byId('tbAsistencias');
	var tr = null;
	var td = null;
	var fila = 0;
	fila = parseInt(dojo.byId("eFilas").value)+1;
	var rows = document.getElementById('tablaAsistencias').getElementsByTagName('tbody')[0].getElementsByTagName('tr');
    for (i = 0; i < rows.length; i++) {
		if(rows[i].id == "filaAsistencia"+filaAsistencia){
			tr = tb.insertRow(rows[i].rowIndex);
		}
    }

	tr.id = "filaAsistencia"+fila;
	td = tr.insertCell(0);
	td.height = '23px';
	td.className = "sanLR04";
	td.innerHTML="<img width=\"16\" align=\"absmiddle\" height=\"16\" onclick=\"borrarAsistencia("+fila+");\" id=\"filaConcepto"+fila+"\" src=\"https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-borrar.png\" title=\"Eliminar Concepto\">";
	td.noWrap = "true";

	td = tr.insertCell(1);
	td.className = "sanLR04";
	td.innerHTML='<b><div id="dvTurnosS'+fila+'"></div></b>';
	td.noWrap = "true";

	td = tr.insertCell(2);
	td.className = "sanLR04";
	td.innerHTML='<input type="hidden" name="eRegistro'+fila+'" id="eRegistro'+fila+'" value="'+fila+'"><input type="hidden" name="eTurnosS'+fila+'" id="eTurnosS'+fila+'" value="'+fila+'"><div dojoType="dojo.data.ItemFileReadStore" jsId="acEmpledos'+fila+'" url="php/auto/empleados.php" clearOnClose="true" urlPreventCache="true"></div><input dojoType="dijit.form.FilteringSelect" value="" jsId="cmbEmpleados'+fila+'" store="acEmpledos'+fila+'" searchAttr="nombre" hasDownArrow="false" name="idEmpleado'+fila+'" id="idEmpleado'+fila+'" autoComplete="false" searchDelay="300" highlightMatch="none" placeHolder="Empleados" style="width:380px;" displayMessage="false" required="false" queryExpr = "*" pageSize="10" onChange="validarEmpleado('+fila+')">';
	td.noWrap = "true";

	td = tr.insertCell(3);
	td.className = "sanLR04";
	td.innerHTML="<select name=\"idCategoria"+fila+"\" id=\"idCategoria"+fila+"\" style=\"width:250px\" onchange=\"validarFila("+fila+");\"><option value=\"\">Seleccione...</option><?php foreach($rsCategorias as $key => $rCategoria){ ?><option value=\"<?=$rCategoria['indice']?>\" ><?=($rCategoria['Categoria'])?></option><?php } ?></select>";
	td.noWrap = "true";
	
	td = tr.insertCell(4);
	td.className = "sanLR04";
	td.innerHTML="";
	td.innerHTML="<select name=\"eCodCosto"+fila+"\" id=\"eCodCosto"+fila+"\" style=\"width:250px\" onchange=\"validarFila("+fila+");\"><option value=\"\">Seleccione...</option><?php foreach($rsCostos as $key => $rCosto){ ?><option value=\"<?=$rCosto['eCodCosto']?>\" ><?=("(".$rCosto['tSiglas'].") ".$rCosto['tArea'])?></option><?php } ?></select>";
	td.noWrap = "true";

	dojo.byId("eFilas").value++;
	dojo.parser.parse('tablaAsistencias');
	asignaEmpleados();
}

function validarFila(filaEmpleado){
	var ultimafila=0;
	var bDistinto=true;
	dojo.query("[id^=\"idEmpleado\"]").forEach(function(nodo, index, array){
		ultimafila=nodo.id.replace('idEmpleado','');
		if(ultimafila>0){
			if(ultimafila!=filaEmpleado && dijit.byId(nodo.id).value){
				if(dijit.byId('idEmpleado'+filaEmpleado).value==dijit.byId(nodo.id).value){
					alert('¡Este empleado ya se encuentra seleccionado!');
					dojo.byId('idEmpleado'+filaEmpleado).value="";
					dijit.byId('idEmpleado'+filaEmpleado).value="";
					dojo.byId('idEmpleado'+filaEmpleado).focus();
					bDistinto=false;
				}
			}
		}
	});

	if(bDistinto==true){
		var ultimafila=0;
		var bGenerarLinea=true;
		var eCodTipoServicio=0;
		
		dojo.query("[id^=\"eRegistro\"]").forEach(function(nodo, index, array){
			ultimafila=parseInt(nodo.id.replace('eRegistro',''));
		});

		if(filaEmpleado==ultimafila){
			/*if(dojo.byId('eCodCosto'+filaEmpleado)){
				if(!dojo.byId('eCodCosto'+filaEmpleado).value){
					bGenerarLinea=false;
				}
			}*/
			if(dojo.byId('idCategoria'+filaEmpleado)){
				if(!dojo.byId('idCategoria'+filaEmpleado).value){
					bGenerarLinea=false;
				}
			}
			if(dojo.byId('idEmpleado'+filaEmpleado)){
				if(!dijit.byId('idEmpleado'+filaEmpleado).value){
					bGenerarLinea=false;
				}
			}
			if(bGenerarLinea==true){
				agregarConcepto(filaEmpleado);
			}
		}
	}
}

function consultar(){
	document.location = './?ePagina=2.5.1.1.php';
}

function listaempleados(codigo){
	var id = codigo.replace("idEmpleado","")
	var busqueda = dojo.byId(codigo).value;
	var acEmp='acEmpledos'+id;
	var cmbEmp='cmbEmpleados'+id;
	eval(acEmp).url = "php/auto/empleados.php?busqueda="+busqueda+"&eCodEmisor="+dojo.byId('eCodEmisor').value;
	eval(acEmp).close();
	eval(cmbEmp).store=eval(acEmp);
}

function asignaEmpleados(){
	dojo.query("[id*=\"idEmpleado\"]").forEach(function(nodo, index, array){
		dojo.connect(dijit.byId(nodo.id), "onKeyUp", function(event){
			if(event.keyCode!=40&&event.keyCode!=38){
				listaempleados(nodo.id);
			}
		});
	});
}

function listabuques(codigo){
	var busqueda = dojo.byId(codigo).value;
	var acBuq='acBuques';
	var cmbBuq='cmbBuques';
	eval(acBuq).url = "php/auto/buques.php?busqueda="+busqueda;
	eval(acBuq).close();
	eval(cmbBuq).store=eval(acBuq);
}

function asignaBuques(){
	dojo.query("[id*=\"eCodBuque\"]").forEach(function(nodo, index, array){
		dojo.connect(dijit.byId(nodo.id), "onKeyUp", function(event){
			if(event.keyCode!=40&&event.keyCode!=38){
				listabuques(nodo.id);
			}
		});
	});
}

dojo.addOnLoad(function(){
	dojo.parser.parse('tablaAsistencias');
	asignaEmpleados();
	asignaBuques();
});
</script>
<div id="dvCNS" style="display:none;"></div>
<form id="Datos" name="Datos" method="post" onSubmit="return false;" enctype="multipart/form-data">
<input type="hidden" name="eProceso" id="eProceso" value="" />
<input type="hidden" name="eAccion" id="eAccion" value="" />
<input type="hidden" name="eServicio" id="eServicio" value="" />
<input type="hidden" name="eUsuario" id="eUsuario" value="<?=(int)$_SESSION['sesionUsuario']['eCodUsuario'];?>" />
<input type="hidden" name="eCodEntidad" id="eCodEntidad" value="<?=(int)$_SESSION['sesionUsuario']['eCodEntidad'];?>" />
<input type="hidden" name="mail" id="mail" value="<?=$_SESSION['sesionUsuario']['Correo'];?>" />
<input type="hidden" name="nombreusuario" id="nombreusuario" value="<?=$_SESSION['sesionUsuario']['Usuario'];?>" />
<input type="hidden" name="eFilaEmp" id="eFilaEmp" value="" />
<input type="hidden" name="eFDiaria" id="eFDiaria" value="<?=date('Ymd');?>" />
<div id="dvFacturacion"></div>
<table border="0" cellpadding="0" cellspacing="0" width="980px">
	 <tr>
        <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Registro Patronal </td>
        <td nowrap class="sanLR04" colspan="3">
        	<select name="eCodEmisor" id="eCodEmisor" style="width:300px" onChange="verificarEvtos();">
                <?php foreach($rsEmisores as $key => $rEmisor){ ?>
                    <option value="<?=($rEmisor{'eCodEntidad'});?>" ><?=utf8_encode($rEmisor['tNombre'])." - ".$rEmisor['tRegistroPatronal'];?></option>
                <?php } ?>
            </select>
        </td>
    </tr>     
    <tr>
        <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Turno </td>
        <td width="50%" nowrap class="sanLR04">
        	<select name="turno" id="turno" style="width:175px" onChange="verificarEmpleados();verificarEvtos();">
                <option value="">Seleccione...</option>
                <option value="1">Turno 1</option>
                <option value="2">Turno 2</option>
                <option value="3">Turno 3</option>
            </select>
        </td>
        <td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" width="16" height="16" align="absmiddle"> Fecha de Asistencia </td>
        <td width="50%" nowrap class="sanLR04">
			<input name="fhFechaAsistencia" type="date" dojoType="dijit.form.TextBox" id="fhFechaAsistencia" value="<?=date('Y-m-d');?>" style="width:140px" onChange="verificarEmpleados();">	
        </td>
    </tr>
    <tr>
        <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" align="absmiddle"> Buque</td>
        <td width="50%" nowrap class="sanLR04">
        	<div dojoType="dojo.data.ItemFileReadStore" jsId="acBuques" url="php/auto/buques.php" clearOnClose="true" urlPreventCache="true"></div>
            <input dojoType="dijit.form.FilteringSelect" 
                value=""
                jsId="cmbBuques"
                store="acBuques"
                searchAttr="nombre"
                hasDownArrow="false"
                name="eCodBuque"
                id="eCodBuque"
                autoComplete="false"
                searchDelay="300"
                highlightMatch="none"
                placeHolder="Buques" 
                style="width:175px;"
                displayMessage="false"
                required="false"
                queryExpr = "*"
                pageSize="10"
                onChange="asignaBuques();"
                >
        </td>
        <td nowrap class="sanLR04"></td>
        <td width="50%" nowrap class="sanLR04"></td>
    </tr>
    <tr>
        <td height="23" nowrap class="sanLR04"></td>
        <td width="50%" nowrap class="sanLR04"></td>
        <td nowrap class="sanLR04"></td>
        <td width="50%" nowrap class="sanLR04"></td>
    </tr>

    <tr>
        <td colspan="4"><div id="dvRegistrosExtras"></div></td>
    </tr>
    <?php mysqli_data_seek($rsCostos,0);?>
    <?php mysqli_data_seek($rsCategorias,0);?>
    <tr>
    	<td colspan="4">
            <table cellspacing="0" border="0" width="100%" id="tablaAsistencias" name="tablaAsistencias">
                <thead>
                    <tr class="thEncabezado">
                        <td nowrap="nowrap" class="sanLR04" height="23"> </td>
                        <td nowrap="nowrap" class="sanLR04" height="23" title="Turnos Trabajados"> TT </td>
                        <td nowrap="nowrap" class="sanLR04" height="23"> Empleado</td>
                        <td nowrap="nowrap" class="sanLR04" height="23"> Categor&Iacute;a</td>
                        <td nowrap="nowrap" class="sanLR04" width="100%"> Centro de Costo</td>
                    </tr>
                </thead>
                <tbody id="tbAsistencias" name="tbAsistencias">
                    <tr id="filaAsistencia1" name="filaAsistencia1">
                        <td nowrap="nowrap" class="sanLR04" height="23"><img width="16" align="absmiddle" height="16" onclick="borrarAsistencia(1);" id="filaConcepto1" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-borrar.png" title="Eliminar Concepto"></td>
                        <td nowrap="nowrap" class="sanLR04"><b><div id="dvTurnosS1"></div></b></td>
                        <td nowrap="nowrap" class="sanLR04" height="23"><input type="hidden" name="eRegistro1" id="eRegistro1" value="1"><input type="hidden" name="eTurnosS1" id="eTurnosS1" value="1">
                        <div dojoType="dojo.data.ItemFileReadStore" jsId="acEmpledos1" url="php/auto/empleados.php" clearOnClose="true" urlPreventCache="true"></div>
						<input type="hidden" name="tArchivo1" id="tArchivo1" value="">
                        <input dojoType="dijit.form.FilteringSelect" 
                            value=""
                            jsId="cmbEmpleados1"
                            store="acEmpledos1"
                            searchAttr="nombre"
                            hasDownArrow="false"
                            name="idEmpleado1"
                            id="idEmpleado1"
                            autoComplete="false"
                            searchDelay="300"
                            highlightMatch="none"
                            placeHolder="Empleados" 
                            style="width:380px;"
                            displayMessage="false"
                            required="false"
                            queryExpr = "*"
                            pageSize="10"
                            onChange="validarEmpleado(1)"
                            >
                        </td>
                        <td nowrap="nowrap" class="sanLR04">
                        <select name="idCategoria1" id="idCategoria1" style="width:250px" onchange="validarFila(1);">
                        	<option value="">Seleccione...</option>
							<?php while($rCategoria=mysqli_fetch_array($rsCategorias,MYSQLI_ASSOC)){ ?>
                            	<option value="<?=$rCategoria{'indice'}?>"><?=($rCategoria{'Categoria'})?></option>
							<?php } ?>
                        </select>
                        </td>
                        <td nowrap="nowrap" class="sanLR04">
                        <select name="eCodCosto1" id="eCodCosto1" style="width:250px" onchange="validarFila(1);">
                        	<option value="">Seleccione...</option>
							<?php while($rCosto=mysqli_fetch_array($rsCostos,MYSQLI_ASSOC)){ ?>
                            	<option value="<?=$rCosto{'eCodCosto'}?>"><?=("(".$rCosto{'tSiglas'}.") ".$rCosto{'tArea'})?></option>
							<?php } ?>
                        </select>
                        </td>
                    </tr>
                </tbody>
            </table>
            <input name="eFilas" id="eFilas" value="1" type="hidden">
        </td>
    </tr>
	<tr><td height="20"></td></tr>
    <tr><td valign="top" nowrap="nowrap" class="sanLR04" colspan="4"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle" /> <font class="fntG10K">Indica que es obligatorio llenar el campo.</font></td></tr>
</table>
</form>
<?php } ?>
