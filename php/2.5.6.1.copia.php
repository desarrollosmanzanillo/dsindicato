<?php
require_once("conexion/soluciones-mysql.php"); 
$link = getLink();
if($_POST){

	if($_POST['fhFechaProgramacionInicio']!=""){
  		$fhFechaProgramacionInicio = $_POST['fhFechaProgramacionInicio'].' 00:00:00';
  		$fhFechaProgramacionFin = ($_POST['fhFechaProgramacionFin']!="" ? $_POST['fhFechaProgramacionFin'] : $_POST['fhFechaProgramacionInicio']).' 23:59:59';
 	}
	if($_POST['fhFechaServicioInicio']!=""){
  		$fhFechaServicioInicio = $_POST['fhFechaServicioInicio'].' 00:00:00';
  		$fhFechaServicioFin = ($_POST['fhFechaServicioFin']!="" ? $_POST['fhFechaServicioFin'] : $_POST['fhFechaServicioInicio']).' 23:59:59';
 	}
	
	$select=" SELECT oc.tCodEstatus, oc.fhFecha, oc.tSerie, oc.eFolio, cc.tNombre AS Comprobante, ".
			" ctc.tNombre AS TipoComprobante, oc.tNombreEmisor, oc.tNombreReceptor, oc.tRFCReceptor, ".
			" cea.tNombre AS Agente, rcs.eCodSolicitud, cfp.tNombre AS FormaPago, oc.tMetodoPago, ".
			" oc.dSubtotal, oc.dImpuestosTrasladados, oc.dTotal, oc.eCodCFD, oc.bTimbrado ".
			" FROM opecfds oc ".
			" INNER JOIN catcomprobantes cc ON cc.eCodComprobante=oc.eCodComprobante ".
			" INNER JOIN cattiposcomprobantes ctc ON ctc.eCodTipoComprobante=cc.eCodTipoComprobante ".
			//" INNER JOIN catentidades cea ON cea.eCodEntidad = oc.eCodNaviera ".
      " INNER JOIN catentidades cea ON cea.eCodEntidad = oc.eCodCliente ".
			" LEFT JOIN relcfdssolicitudesservicios rcs ON rcs.eCodCFD = oc.eCodCFD ".
			" LEFT JOIN catformaspago cfp ON cfp.eCodFormaPago = oc.eCodFormaPago ".
			" WHERE 1=1 ".
			($_POST['eFolio'] ? " AND oc.eFolio=".$_POST['eFolio'] : "").
			($_POST['Cliente']       ? " AND oc.tNombreReceptor   LIKE '%".$_POST['Cliente']."%'" : "").
			($_POST['tPatente']      ? " AND oss.tPatente  LIKE '%".$_POST['tPatente']."%'" : "").
			($_POST['Naviera']       ? " AND cen.tNombre   LIKE '%".$_POST['Naviera']."%'" : "").
			($_POST['TipoSolicitud'] ? " AND cts.eCodTipoSolicitud=".$_POST['TipoSolicitud'] : "").
			($_POST['TipoServicio']  ? " AND cto.eCodTipoServicio=".$_POST['TipoServicio'] : "").
			($_POST['Buque']       ? " AND cbu.tNombre   LIKE '%".$_POST['Buque']."%'" : "").
			($_POST['tNumeroViaje']      ? " AND oss.tNumeroViaje  LIKE '%".$_POST['tNumeroViaje']."%'" : "").
			($_POST['fhFechaProgramacionInicio']	? " AND oss.fhFecha			BETWEEN    '".$fhFechaProgramacionInicio. "' AND '" .$fhFechaProgramacionFin."'" : "" ).
			($_POST['fhFechaServicioInicio']		? " AND oss.fhFechaServicio	BETWEEN    '".$fhFechaServicioInicio. "' AND '" .$fhFechaServicioFin."'" : "" ).
			((int)$_POST['eEntidad']==1 ? "" : " AND oc.eCodCliente=".(int)$_POST['eEntidad']).//(oss.eCodNaviera=".(int)$_POST['eEntidad']." OR .")"
			" ORDER BY oc.eCodCFD DESC ";
	$rsCFDs=mysqli_query($link,$select);
	$rsUsuarios=mysqli_query($link,$select);
	$registros=(int)mysqli_num_rows($rsUsuarios); ?>
<table cellspacing="0" border="0" width="965px">
  <tr>
    <td width="50%"><hr color="#666666" /></td>
    <td class="sanLR04" nowrap="nowrap"><b>Registros Encontrados: (
      <?=$registros;?>
      )</b></td>
    <td width="50%"><hr color="#666666" /></td>
  </tr>
</table>
<div style="display:block; top:0; left:0; width:965px; z-index=1; overflow: auto; height:400px;">
  <table cellspacing="0" border="0" width="965px">
    <thead>
      <tr class="thEncabezado">
        <td nowrap="nowrap" class="sanLR04" height="20" align="center">E</td>
        <td nowrap="nowrap" class="sanLR04" height="20" align="center">PDF</td>
        <td nowrap="nowrap" class="sanLR04" height="20" align="center">XML</td>
        <td nowrap="nowrap" class="sanLR04">C&oacute;digo</td>
        <td nowrap="nowrap" class="sanLR04">Fecha</td>
        <td nowrap="nowrap" class="sanLR04">Comprobante</td>
        <td nowrap="nowrap" class="sanLR04">Tipo</td>
        <td nowrap="nowrap" class="sanLR04">Emisor</td>
        <td nowrap="nowrap" class="sanLR04">Cliente</td>
        <td nowrap="nowrap" class="sanLR04">RFC</td>
        <td nowrap="nowrap" class="sanLR04">Agencia Aduanal</td>
        <td nowrap="nowrap" class="sanLR04">No. Solicitud</td>
        <td nowrap="nowrap" class="sanLR04">Forma de Pago</td>
        <td nowrap="nowrap" class="sanLR04">Metodo de Pago</td>
		<td nowrap="nowrap" class="sanLR04">Subtotal</td>
        <td nowrap="nowrap" class="sanLR04">Traslados</td>
        <td nowrap="nowrap" class="sanLR04">Total</td>
		<td nowrap="nowrap" class="sanLR04" width="100%">Saldo</td>
      </tr>
    </thead>
    <tbody>
      <?php $i=1; while($rCFD=mysqli_fetch_array($rsCFDs,MYSQLI_ASSOC)){ ?>
		  <tr>
			<td nowrap="nowrap" class="sanLR04" height="20" align="center">
            	<img width="16" height="16" alt="CFDs" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-<?=$rCFD{'tCodEstatus'};?>.png">
            </td>
            <td nowrap="nowrap" class="sanLR04" height="20" align="center">
            <?php if((int)$rCFD{'bTimbrado'}>0 && $rCFD{'tCodEstatus'}=='AU'){ ?>
              <img width="16" height="16" alt="CFDs" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-pdf.png" onClick="descargarComprobante('pdf','<?=$rCFD{'tSerie'};?>',<?=$rCFD{'eFolio'};?>);">
            <?php } ?>
            </td>
            <td nowrap="nowrap" class="sanLR04" height="20" align="center">
            <?php if((int)$rCFD{'bTimbrado'}>0 && $rCFD{'tCodEstatus'}=='AU'){ ?>
              <img width="16" height="16" alt="CFDs" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-xml.png" onClick="descargarComprobante('xml','<?=$rCFD{'tSerie'};?>',<?=$rCFD{'eFolio'};?>);">
            <?php } ?>
            </td>
			<td nowrap="nowrap" class="sanLR04 colmenu"><a href="?ePagina=2.5.6.1.1.php&eCodCFD=<?=$rCFD{'eCodCFD'};?>"><b><?=$rCFD{'tSerie'};?><?=sprintf("%07d",$rCFD{'eFolio'});?></b></a></td>
			<td nowrap="nowrap" class="sanLR04"><?=date("d/m/Y H:i", strtotime($rCFD{'fhFecha'}));?></td>
			<td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rCFD{'Comprobante'});?></td>
			<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rCFD{'TipoComprobante'});?></td>
			<td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rCFD{'tNombreEmisor'});?></td>
			<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rCFD{'tNombreReceptor'});?></td>
			<td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rCFD{'tRFCReceptor'});?></td>
			<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rCFD{'Agente'});?></td>
			<td nowrap="nowrap" class="sanLR04 columnB"><b><a href="?ePagina=2.5.3.1.1.php&amp;eCodSolicitud=<?=($rCFD{'eCodSolicitud'})?>" class="txtCO12" target="_blank"><?=sprintf("%07d",$rCFD{'eCodSolicitud'})?></a></b></td>
			<td nowrap="nowrap" class="sanLR04"><?=($rCFD{'FormaPago'});?></td>
			<td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rCFD{'tMetodoPago'});?></td>
			<td nowrap="nowrap" class="sanLR04" align="right"><?=number_format($rCFD{'dSubtotal'}, 2);?></td>
            <td nowrap="nowrap" class="sanLR04 columnB" align="right"><?=number_format($rCFD{'dImpuestosTrasladados'}, 2);?></td>
            <td nowrap="nowrap" class="sanLR04" align="right"><?=number_format($rCFD{'dTotal'}, 2);?></td>
			<td nowrap="nowrap" class="sanLR04 columnB" align="right" width="100%"><?=number_format($rCFD{'dTotal'}, 2);?></td>
		  </tr>
      <?php $i++; } ?>
    </tbody>
  </table>
</div>
<?php }else{ ?>
<script type="text/javascript">
dojo.require("dojo.number");
dojo.require("dijit.form.NumberTextBox");
dojo.require("dijit.form.DateTextBox");
dojo.require("dojo.io.iframe");
function nuevo(){
	document.location = "?ePagina=2.4.6.2.php";
}

function generaExcel(){
	var UrlExcel = "php/excel/2.5.6.1.php?eEntidad="+dojo.byId('eEntidad').value+
	(dojo.byId('eCodSolicitud').value ? "&eCodSolicitud="+dojo.byId('eCodSolicitud').value : "")+
	(dojo.byId('fhFechaProgramacionInicio').value ? "&fhFechaProgramacionInicio="+dojo.byId('fhFechaProgramacionInicio').value : "")+
	(dojo.byId('fhFechaProgramacionFin').value ? "&fhFechaProgramacionFin="+dojo.byId('fhFechaProgramacionFin').value : "")+
	(dojo.byId('tPatente').value ? "&tPatente="+dojo.byId('tPatente').value : "")+
	(dojo.byId('Naviera').value ? "&Naviera="+dojo.byId('Naviera').value : "")+
	(dojo.byId('TipoSolicitud').value ? "&TipoSolicitud="+dojo.byId('TipoSolicitud').value : "")+
	(dojo.byId('TipoServicio').value ? "&TipoServicio="+dojo.byId('TipoServicio').value : "")+
	(dojo.byId('Buque').value ? "&Buque="+dojo.byId('Buque').value : "")+
	(dojo.byId('tNumeroViaje').value ? "&tNumeroViaje="+dojo.byId('tNumeroViaje').value : "");
	var Datos = document.createElement("FORM");
	document.body.appendChild(Datos);
	Datos.name='Datos';
	Datos.method = "POST";
	Datos.action = UrlExcel;
	Datos.submit();
}

function descargarComprobante(tComprobante, serie, folio){
	var UrlPdf = "https://app.webinvoice.mx/qry/docs/?tProceso=descargarCFD&tFormato="+tComprobante+"&tIdentificadorEmpresa=bbecb4e2715811e5b80602fa016ccf30&tRFC=SAT140208L69&tSerie=F&eFolio="+folio;
	var submitForm = document.createElement("FORM");
	document.body.appendChild(submitForm);
	submitForm.method = "POST";
	submitForm.action = UrlPdf;
	submitForm.submit();	
}

dojo.addOnLoad(function(){filtrarConsulta();});
</script>
<form name="Datos" id="Datos" method="post" action="" onsubmit="return false;">
  <input type="hidden" value="0" name="ePagina" id="ePagina" />
  <input type="hidden" value="" name="eAccion" id="eAccion" />
  <input type="hidden" value="<?=(int)$_SESSION['sesionUsuario']['eCodEntidad'];?>" name="eEntidad" id="eEntidad" />
  <table width="965px" border="0">
    <tr>
      <td colpan="3" width="100%"></td>
      <td align="right" nowrap="nowrap"><a class="fntBR11" href="javaScript:mostrarFiltros('Busqueda');">Busqueda de Registros</a></td>
    </tr>
    <tr id="trBusqueda" style="display:none">
      <td colspan="4"><table  width="965px" bgcolor="#f9f9f9">
          <tr>
            <td class="sanLR04" height="5"></td>
          </tr>
          <tr>
            <td class="sanLR04" height="20">Folio</td>
            <td class="sanLR04" width="50%"><input type="text" name="eFolio" dojoType="dijit.form.TextBox" id="eFolio" value="" style="width:80px"></td>
            <td class="sanLR04">Cliente</td>
            <td class="sanLR04" width="50%"><input type="text" name="Cliente" dojoType="dijit.form.TextBox" id="Cliente" value="" style="width:175px"></td>
          </tr>
          <tr>
            <td nowrap="nowrap" class="sanLR04" height="20">Fecha de Solicitud</td>
            <td class="sanLR04" width="50%" >
            <input name="fhFechaProgramacionInicio" id="fhFechaProgramacionInicio" type="text" dojoType="dijit.form.DateTextBox" required="false" style="width:80px;" hasDownArrow="false" displayMessage="false" value="" constraints="{datePattern:'dd/MM/yyyy'}"/> -
             <input name="fhFechaProgramacionFin" id="fhFechaProgramacionFin" type="text" dojoType="dijit.form.DateTextBox" required="false" style="width:80px;" hasDownArrow="false" displayMessage="false" value="" constraints="{datePattern:'dd/MM/yyyy'}" />
              </td>
            <td nowrap="nowrap"class="sanLR04">Fecha de Servicio</td>
            <td class="sanLR04" width="50%"><input name="fhFechaServicioInicio" id="fhFechaServicioInicio" type="text" dojoType="dijit.form.DateTextBox" required="false" style="width:80px;" hasDownArrow="false" displayMessage="false" value="" constraints="{datePattern:'dd/MM/yyyy'}"/>
              -
              <input name="fhFechaServicioFin" id="fhFechaServicioFin" type="text" dojoType="dijit.form.DateTextBox" required="false" style="width:80px;" hasDownArrow="false" displayMessage="false" value="" constraints="{datePattern:'dd/MM/yyyy'}"/>
            </td>
          </tr>
          <tr>
            <td class="sanLR04" height="20">Patente</td>
            <td class="sanLR04" width="50%"><input type="text" name="tPatente" dojoType="dijit.form.TextBox" id="tPatente" value="" style="width:80px"></td>
            <td class="sanLR04">Naviera</td>
            <td class="sanLR04" width="50%"><input type="text" name="Naviera" dojoType="dijit.form.TextBox" id="Naviera" value="" style="width:175px"></td>
          </tr>
          <tr>
            <td nowrap="nowrap" class="sanLR04" height="20">Tipo de Solicitud</td>
            <td class="sanLR04" width="50%"><select name='TipoSolicitud' id='TipoSolicitud' style="width:175px">
                <option value=''>Seleccione...</option>
                <?php $sel = mysqli_query($link,"SELECT eCodTipoSolicitud, tNombre FROM cattipossolicitudes where tCodEstatus='AC'");                
                              while($row = mysqli_fetch_array($sel,MYSQLI_ASSOC)){ ?>
                <option value='<?php echo $row["eCodTipoSolicitud"]; ?>'> <?php echo $row["tNombre"]; ?> </option>
                <?php } ?>
              </select>
            </td>
            <td nowrap="nowrap" class="sanLR04">Tipo de Servicio</td>
            <td class="sanLR04" width="50%"><select name='TipoServicio' id='TipoServicio' style="width:175px">
                <option value=''>Seleccione...</option>
                <?php $sel = mysqli_query($link,"SELECT eCodTipoServicio, tNombre FROM cattiposservicios where tCodEstatus='AC'");                
                              while($row = mysqli_fetch_array($sel,MYSQLI_ASSOC)){ ?>
                <option value='<?php echo $row["eCodTipoServicio"]; ?>'> <?php echo $row["tNombre"]; ?> </option>
                <?php } ?>
              </select></td>
          </tr>
          <tr>
            <td class="sanLR04" height="20">Buque</td>
            <td class="sanLR04" width="50%"><input type="text" name="Buque" dojoType="dijit.form.TextBox" id="Buque" value="" style="width:175px"></td>
            <td class="sanLR04">Viaje</td>
            <td class="sanLR04" width="50%"><input type="text" name="tNumeroViaje" dojoType="dijit.form.TextBox" id="tNumeroViaje" value="" style="width:80px"></td>
          </tr>
          <tr>
            <td class="sanLR04" height="5"></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td colspan="4"><div id="dvCNS"></div></td>
    </tr>
  </table>
</form>
<?php } ?>