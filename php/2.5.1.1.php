<?php require_once("conexion/soluciones-mysql.php");
$link = getLink();
if($_POST){
	if ($_POST['fhFechaAsistenciaInicio']!=""){
  		$fhFechaAsistenciaInicio = $_POST['fhFechaAsistenciaInicio'].' 00:00:00';
  		$fhFechaAsistenciaFin = ($_POST['fhFechaAsistenciaFin']!="" ? $_POST['fhFechaAsistenciaFin'] : $_POST['fhFechaAsistenciaInicio']).' 23:59:59';
 	}
 	if ($_POST['fhFechaMovimientoInicio']!=""){
  		$fhFechaMovimientoInicio = $_POST['fhFechaMovimientoInicio'].' 00:00:00';
  		$fhFechaMovimientoFin = ($_POST['fhFechaMovimientoFin']!="" ? $_POST['fhFechaMovimientoFin'] : $_POST['fhFechaMovimientoInicio']).' 23:59:59';
 	}
	 $select="SELECT DISTINCT
	 			ast.id, 
				ast.Turno, 
				ast.Fecha, 
				ast.dBono, 
				ast.Estado, 
				mov.tFolio, 
				ast.Salario, 
				ent.tSiglas, 
				mov.dImporte, 
				ast.idEmpresa, 
				ast.eCodBuque, 
				ast.idEmpleado, 
				ast.idCategorias, 
				tpo.tNombreCorto, 
				ast.bModificacion, 
				tpo.eCodTipoEntidad, 
				ctm.tNombre AS tMovi, 
				cbu.tNombre AS Buque,
				mov.tCodTipoMovimiento, 
				emp.Empleado AS tNombre, 
				bon.tNombre AS tipobono, 
				ast.eCodMotivoAsistencia, 
				ast.Estado as tCodEstatus, 
				cat.Categoria AS categoria, 
				cco.tSiglas AS CentroCosto, 
				mov.fhFecha AS fhMovimiento, 
				cma.tNombre AS MotivoAsistencia, 
				emp.eCodigoPostal
			FROM asistencias ast 
			LEFT JOIN relmaniobrasbonosasistencias rbn ON rbn.eCodAsistencia=ast.id 
			LEFT JOIN opemaniobrabono omb ON omb.eCodManiobra=rbn.eCodManiobra 
			LEFT JOIN cattipobono bon ON bon.eCodBono=omb.eCodBono 
			LEFT JOIN relasistenciamovimientos rel ON rel.eCodAsistencia=ast.id 
			LEFT JOIN movimientosafiliatorios mov ON mov.eCodMovimiento=rel.eCodMovimiento AND mov.eCodEstatus='AC' 
			LEFT JOIN categorias cat ON cat.indice=ast.idCategorias 
			LEFT JOIN empleados emp ON emp.id=ast.idEmpleado 
			LEFT JOIN cattipoempleado tpo ON emp.eCodTipo=tpo.eCodTipoEntidad 
			LEFT JOIN catentidades ent ON ent.eCodEntidad=ast.idEmpresa 
			LEFT JOIN cattipomovimientos ctm ON ctm.eCodMovimiento=mov.tCodTipoMovimiento 
			LEFT JOIN catcostos cco ON cco.eCodCosto=ast.eCodCosto
			LEFT JOIN catmotivosasistencias cma ON cma.eCodMotivoAsistencia=ast.eCodMotivoAsistencia
			LEFT JOIN catbuques cbu ON cbu.eCodBuque=ast.eCodBuque
			WHERE 1=1 ".
			($_POST['baja'] 					? " AND ast.folioBaja=".$_POST['baja'] 							                                                   : "").
			($_POST['folio'] 					? " AND mov.tFolio like '%".$_POST['folio']."%'" 				                                                   : "").
			($_POST['Empleado'] 				? " AND (emp.Empleado like '%".utf8_decode($_POST['Empleado'])."%' OR ast.idEmpleado=".(int)$_POST['Empleado'].")" : "").
			($_POST['eCodCosto'] 				? " AND ast.eCodCosto=".$_POST['eCodCosto']						                                                   : "").
			($_POST['eCodTurno'] 				? " AND ast.Turno=".$_POST['eCodTurno'] 						                                                   : "").
			($_POST['eCodEntidad'] 				? " AND ast.idEmpresa=".(int)$_POST['eCodEntidad'] 			                                                       : "").
			($_POST['EcodEstatus'] 				? " AND ast.Estado='".$_POST['EcodEstatus']."'"					                                                   : "").
			($_POST['eCodAsistencia'] 			? " AND ast.id=".$_POST['eCodAsistencia'] 						                                                   : "").
			($_POST['tipoMovimiento']==10		? " AND mov.tFolio IS NULL "								                                                       : ($_POST['tipoMovimiento'] ? " AND mov.tCodTipoMovimiento='".$_POST['tipoMovimiento']."'" : "")).
			($_POST['eCodTipoEntidad'] 			? " AND emp.eCodTipo=".$_POST['eCodTipoEntidad'] 				                                                   : "").
			($_POST['eCodTipoCategoria'] 		? " AND ast.idCategorias=".$_POST['eCodTipoCategoria'] 			                                                   : "").
			($_POST['eCodMotivoAsistencia'] 	? " AND ast.eCodMotivoAsistencia=".$_POST['eCodMotivoAsistencia'] 			                                       : "").
			($_POST['fhFechaAsistenciaInicio']	? " AND ast.Fecha BETWEEN '".$fhFechaAsistenciaInicio. "' AND '" .$fhFechaAsistenciaFin."'"                        : "" ).
			($_POST['fhFechaMovimientoInicio']	? " AND mov.fhFecha BETWEEN '".$fhFechaMovimientoInicio. "' AND '" .$fhFechaMovimientoFin."'"                      : "" ).
			($_POST['chkModificado']			? " AND ast.bModificacion IS NOT NULL "                                                                            : "").
			($_POST['chkBuque']					? " AND (ast.eCodBuque IS NOT NULL AND ast.eCodBuque>0) "                                                                            : "").
			
			($_POST['chkDoble']					? " AND (SELECT COUNT(id)
													FROM asistencias 
													WHERE idEmpleado=ast.idEmpleado AND Fecha=ast.Fecha)>1 "                                                       : "").
			" ORDER BY ast.Fecha ".($_POST['orden'] ? $_POST['orden'] : "DESC").", ast.id DESC, ast.Turno DESC, mov.eCodMovimiento DESC 
			LIMIT ".($_POST['limite'] ? $_POST['limite'] : "100");
	if($_POST['eAccion']==999){
		print $select;
	}
	$rsSolicitudes=mysqli_query($link,$select);
	$soli = mysqli_fetch_array($rSolicitudes,MYSQLI_NUM);
	$registros=(int)mysqli_num_rows($rsSolicitudes); ?>
    <table cellspacing="0" border="0" width="965px">
		<tr>
			<td width="50%"><hr color="#666666" /></td>
			<td class="sanLR04" nowrap="nowrap"><b>Registros Encontrados: ( <?=$registros;?> )</b></td>
			<td width="50%"><hr color="#666666" /></td>
		</tr>
    </table>
	<div style="display:block; top:0; left:0; width:965px; z-index=1; overflow: auto; height:400px;">
	<table cellspacing="0" border="0" width="965px">
		<thead>
			<tr class="thEncabezado">
				<td nowrap="nowrap" class="sanLR04" height="20" align="center" title="Estatus">E</td>
				<td nowrap="nowrap" class="sanLR04" height="20" align="center" title="Motivo de Asistencia">MA</td>
				<td nowrap="nowrap" class="sanLR04" height="20" align="center" title="Buque">B</td>
				<td nowrap="nowrap" class="sanLR04">C&oacute;digo</td>				
				<td nowrap="nowrap" class="sanLR04">Fecha</td>
				<td nowrap="nowrap" class="sanLR04" title="Turno">T</td>
				<td nowrap="nowrap" class="sanLR04" title="Centro de Costo">C. Costo</td>
				<td nowrap="nowrap" class="sanLR04" >Categoria</td>
				<td nowrap="nowrap" class="sanLR04" >Salario</td>
				<td nowrap="nowrap" class="sanLR04" title="Clave de Empleado">#</td>
				<td nowrap="nowrap" class="sanLR04" >Empleado</td>
				<td nowrap="nowrap" class="sanLR04" title="Tipo Empleado">Tipo</td>
				<td nowrap="nowrap" class="sanLR04" >Buque</td>
				<td nowrap="nowrap" class="sanLR04" title="Empresa">Empresa</td>
				<td nowrap="nowrap" class="sanLR04" >Movimiento</td>
				<td nowrap="nowrap" class="sanLR04" >Fecha</td>
				<td nowrap="nowrap" class="sanLR04" >Folio</td>
				<td nowrap="nowrap" class="sanLR04" width="100%">S.D.I.</td>
				<td nowrap="nowrap" class="sanLR04" >C. Postal</td>
			</tr>
		</thead>
		<tbody>
			<?php $i=1; foreach($rsSolicitudes as $key => $rSolicitud){ ?>
				<tr>
					<td nowrap="nowrap" class="sanLR04" height="20" align="center"><img width="16" height="16" title="Estatus" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-<?=$rSolicitud['Estado'];?>.png"><?=((int)$rSolicitud['bModificacion']>0 ? "<img width=\"16\" height=\"16\" title=\"Modificado\" src=\"../images/png/modificar.png\">" : "");?></td>
					<td nowrap="nowrap" class="sanLR04" height="20" align="center"><img width="16" height="16" title="<?=($rSolicitud['eCodMotivoAsistencia']>0 ? $rSolicitud['MotivoAsistencia'] : "");?>" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/<?=($rSolicitud['eCodMotivoAsistencia']>0 ? "ic-".$rSolicitud['eCodMotivoAsistencia'] : "noobligatorio");?>.png"></td>
					<td nowrap="nowrap" class="sanLR04" height="20" align="center"><img width="16" height="16" title="Estatus" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/<?=($rSolicitud['eCodBuque']>0 ? "ic-buque" : "noobligatorio");?>.png"></td>

					<td nowrap="nowrap" class="sanLR04 colmenu"><b><?if(is_null($rSolicitud['tCodTipoMovimiento']) AND ($rSolicitud['Estado']=='AC')){?><a href="?ePagina=2.5.1.1.1.php&eCodAsistencia=<?=$rSolicitud['id'];?>"><?}?><?=sprintf("%07d",$rSolicitud['id']);?></b><?if (is_null($rSolicitud['tCodTipoMovimiento'])){?></a><?}?></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><a href="?ePagina=2.5.1.1.2.php&eCodAsistencia=<?=$rSolicitud['id'];?>"><?=date("d/m/Y", strtotime($rSolicitud['Fecha']));?></a></td>
					<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rSolicitud['Turno']);?></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rSolicitud['CentroCosto']);?></td>
					<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rSolicitud['categoria']);?></td>
					<td nowrap="nowrap" class="sanLR04 columnB" align="right"><?=number_format($rSolicitud['Salario'],2);?></td>
					<td nowrap="nowrap" class="sanLR04" ><?=utf8_encode($rSolicitud['idEmpleado']);?></td>
					<td nowrap="nowrap" class="sanLR04 columnB" ><?=utf8_encode($rSolicitud['tNombre']);?></td>
					<td nowrap="nowrap" class="sanLR04" ><?=utf8_encode($rSolicitud['tNombreCorto']);?></td>
					<td nowrap="nowrap" class="sanLR04 columnB" ><?=utf8_encode($rSolicitud['Buque']);?></td>
					<td nowrap="nowrap" class="sanLR04" ><?=utf8_encode($rSolicitud['tSiglas']);?></td>
					<td nowrap="nowrap" class="sanLR04 columnB" ><?=utf8_encode($rSolicitud['tMovi']);?></td>
					<td nowrap="nowrap" class="sanLR04" ><?=($rSolicitud['fhMovimiento'] ? date("d/m/Y", strtotime($rSolicitud['fhMovimiento'])) : "");?></td>
					<td nowrap="nowrap" class="sanLR04 columnB" ><?=utf8_encode($rSolicitud['tFolio']);?></td>
					<td nowrap="nowrap" class="sanLR04" align="right"><?=number_format($rSolicitud['dImporte'],2);?></td>
					<td nowrap="nowrap" class="sanLR04 columnB" ><?=utf8_encode($rSolicitud['eCodigoPostal']);?></td>
				</tr>
			<?php $i++; } ?>
		</tbody>
	</table>
	</div>
<?php }else{ 
	$select=" SELECT * 
			FROM categorias 
			WHERE tCodEstatus='AC' 
			order by Categoria ";
	$rsTiposCategorias=mysqli_query($link,$select);

	$select=" SELECT * 
			FROM catcostos 
			WHERE tCodEstatus='AC' 
			order by tSiglas ";
	$rsCentrosCostos=mysqli_query($link,$select);

	$select=" SELECT * 
			FROM cattipoempleado 
			WHERE tCodEstatus='AC' 
			ORDER BY tNombreCorto ";
	$rsTiposEmpleados=mysqli_query($link,$select);

	$select=" SELECT tSiglas, eCodEntidad
	        FROM catentidades
	        WHERE bPrincipal IS NOT NULL
	        ORDER BY tSiglas ASC ";
	$rsEmpresas=mysqli_query($link,$select);

	$select=" SELECT * 
			FROM catmotivosasistencias
			ORDER BY tNombre ";
	$rsMotivosAsistencias=mysqli_query($link,$select);
	
	$FechaHoy=date("Y-m-d");
	$fechaayer=date('Y-m-d',strtotime('-5 days', strtotime($FechaHoy)));
	?>
	<script type="text/javascript">
	dojo.require("dojo.number");
	dojo.require("dijit.form.NumberTextBox");
	dojo.require("dijit.form.DateTextBox");
	dojo.require("dojo.io.iframe");
	
	function nuevo(){
		document.location = "?ePagina=2.4.1.1.php";
	}

	function generaExcel(){
		var UrlExcel = "php/excel/2.5.1.1.php?"+
		(dojo.byId('eCodAsistencia').value          ? "&eCodAsistencia="+dojo.byId('eCodAsistencia').value : "")+
		(dojo.byId('eCodMotivoAsistencia').value    ? "&eCodMotivoAsistencia="+dojo.byId('eCodMotivoAsistencia').value : "")+
		(dojo.byId('fhFechaAsistenciaInicio').value ? "&fhFechaAsistenciaInicio="+dojo.byId('fhFechaAsistenciaInicio').value : "")+
		(dojo.byId('fhFechaAsistenciaFin').value    ? "&fhFechaAsistenciaFin="+dojo.byId('fhFechaAsistenciaFin').value : "")+
		(dojo.byId('fhFechaMovimientoInicio').value ? "&fhFechaMovimientoInicio="+dojo.byId('fhFechaMovimientoInicio').value : "")+
		(dojo.byId('fhFechaMovimientoFin').value    ? "&fhFechaMovimientoFin="+dojo.byId('fhFechaMovimientoFin').value : "")+
		(dojo.byId('eCodTipoCategoria').value       ? "&eCodTipoCategoria="+dojo.byId('eCodTipoCategoria').value : "")+
		(dojo.byId('eCodTurno').value               ? "&eCodTurno="+dojo.byId('eCodTurno').value : "")+
		(dojo.byId('Empleado').value                ? "&Empleado="+dojo.byId('Empleado').value : "")+		
		(dojo.byId('eCodTipoEntidad').value         ? "&eCodTipoEntidad="+dojo.byId('eCodTipoEntidad').value : "")+		
		(dojo.byId('folio').value                   ? "&folio="+dojo.byId('folio').value                   : "")+		
		(dojo.byId('tipoMovimiento').value          ? "&tipoMovimiento="+dojo.byId('tipoMovimiento').value : "")+
		(dojo.byId('EcodEstatus').value             ? "&EcodEstatus="+dojo.byId('EcodEstatus').value : "")+
		(dojo.byId('eCodEntidad').value             ? "&eCodEntidad="+dojo.byId('eCodEntidad').value : "")+
		(dojo.byId('eCodCosto').value               ? "&eCodCosto="+dojo.byId('eCodCosto').value     : "")+
		(dojo.byId('eAccion').value                 ? "&eAccion="+dojo.byId('eAccion').value         : "")+
		(dojo.byId('orden').value                   ? "&orden="+dojo.byId('orden').value             : "")+
		(dojo.byId('limite').value                  ? "&limite="+dojo.byId('limite').value           : "")+
		(dojo.byId('chkDoble').checked==true        ? "&chkDoble=1"                                  : "");
		var Datos = document.createElement("FORM");
		document.body.appendChild(Datos);
		Datos.name='Datos';
		Datos.method = "POST";
		Datos.action = UrlExcel;
		Datos.submit();
	}

	function rDiaria(){
		var UrlExcel = "php/excel/2.5.1.1.a.php?"+
		(dojo.byId('eCodAsistencia').value ? "&eCodAsistencia="+dojo.byId('eCodAsistencia').value : "")+
		(dojo.byId('eCodMotivoAsistencia').value ? "&eCodMotivoAsistencia="+dojo.byId('eCodMotivoAsistencia').value : "")+
		(dojo.byId('fhFechaAsistenciaInicio').value ? "&fhFechaAsistenciaInicio="+dojo.byId('fhFechaAsistenciaInicio').value : "")+
		(dojo.byId('fhFechaAsistenciaFin').value ? "&fhFechaAsistenciaFin="+dojo.byId('fhFechaAsistenciaFin').value : "")+
		(dojo.byId('fhFechaMovimientoInicio').value ? "&fhFechaMovimientoInicio="+dojo.byId('fhFechaMovimientoInicio').value : "")+
		(dojo.byId('fhFechaMovimientoFin').value ? "&fhFechaMovimientoFin="+dojo.byId('fhFechaMovimientoFin').value : "")+
		(dojo.byId('eCodTipoCategoria').value ? "&eCodTipoCategoria="+dojo.byId('eCodTipoCategoria').value : "")+
		(dojo.byId('eCodTurno').value ? "&eCodTurno="+dojo.byId('eCodTurno').value : "")+
		(dojo.byId('Empleado').value ? "&Empleado="+dojo.byId('Empleado').value : "")+		
		(dojo.byId('eCodTipoEntidad').value ? "&eCodTipoEntidad="+dojo.byId('eCodTipoEntidad').value : "")+		
		(dojo.byId('folio').value ? "&folio="+dojo.byId('folio').value : "")+		
		(dojo.byId('tipoMovimiento').value ? "&tipoMovimiento="+dojo.byId('tipoMovimiento').value : "")+
		(dojo.byId('EcodEstatus').value ? "&EcodEstatus="+dojo.byId('EcodEstatus').value : "")+
		(dojo.byId('eCodEntidad').value ? "&eCodEntidad="+dojo.byId('eCodEntidad').value : "")+
		(dojo.byId('eCodCosto').value ? "&eCodCosto="+dojo.byId('eCodCosto').value : "")+
		(dojo.byId('eAccion').value ? "&eAccion="+dojo.byId('eAccion').value : "")+
		(dojo.byId('orden').value ? "&orden="+dojo.byId('orden').value : "")+
		(dojo.byId('limite').value ? "&limite="+dojo.byId('limite').value : "")+
		(dojo.byId('chkDoble').checked==true ? "&chkDoble=1" : "")+
		(dojo.byId('chkModificado').checked==true ? "&chkModificado=1" : "")+
		(dojo.byId('chkBuque').checked==true ? "&chkBuque=1" : "");
		var Datos = document.createElement("FORM");
		document.body.appendChild(Datos);
		Datos.name='Datos';
		Datos.method = "POST";
		Datos.action = UrlExcel;
		Datos.submit();
	}
	
	dojo.addOnLoad(function(){filtrarConsulta();});
	</script>
	<form name="Datos" id="Datos" method="post" action="" onsubmit="return false;">
		<input type="hidden" value="0" name="ePagina" id="ePagina" />
		<input type="hidden" value="" name="eAccion" id="eAccion" />
		<input type="hidden" value="<?=(int)$_SESSION['sesionUsuario']['eCodEntidad'];?>" name="eEntidad" id="eEntidad" />
		<table width="965px" border="0">
			<tr>
				<td colpan="3" width="100%"></td>
				<td align="right" nowrap="nowrap"><a class="fntBR11" href="javaScript:mostrarFiltros('Busqueda');">Busqueda de Registros</a></td>
			</tr>
			<tr id="trBusqueda" style="display:none">
				<td colspan="4">
					<table width="100%"  bgcolor="#f9f9f9">
						<tr><td class="sanLR04" height="5"></td></tr>
						<tr>
							<td class="sanLR04" height="20">C&oacute;digo</td>
							<td class="sanLR04" width="40%"><input type="text" name="eCodAsistencia" dojoType="dijit.form.TextBox" id="eCodAsistencia" value="" style="width:80px; height:25px;"></td>
							<td class="sanLR04" nowrap="nowrap">Fecha de Asistencia</td>
							<td class="sanLR04" width="50%">
							<input name="fhFechaAsistenciaInicio" id="fhFechaAsistenciaInicio" type="date"  required="false" value="<?=$fechaayer;?>" /> -
							<input name="fhFechaAsistenciaFin" id="fhFechaAsistenciaFin" type="date"  required="false" value="<?=$FechaHoy;?>"  />
							</td>
						</tr>
						<tr>
							<td class="sanLR04" height="20" nowrap="nowrap">Tipo de Categoria</td>
							<td class="sanLR04" width="50%">
								<select name="eCodTipoCategoria" id="eCodTipoCategoria" style="width:180px; height:25px;">
									<option value="">Seleccione...</option>
									<?php foreach($rsTiposCategorias as $key => $rTipoCategoria){ ?>
										<option value="<?=$rTipoCategoria{'indice'}?>"><?=utf8_encode($rTipoCategoria{'Categoria'})?></option>
									<?php } ?>
								</select>
							</td>
							<td class="sanLR04" nowrap="nowrap">Fecha de Movimiento</td>
							<td class="sanLR04" width="50%">
							<input name="fhFechaMovimientoInicio" id="fhFechaMovimientoInicio" type="date"  required="false" value="" /> -
							<input name="fhFechaMovimientoFin" id="fhFechaMovimientoFin" type="date"  required="false" value=""  />
							</td>
						</tr>
						<tr>
							<td class="sanLR04" height="20">Empleado</td>
							<td class="sanLR04" width="50%"><input type="text" name="Empleado" dojoType="dijit.form.TextBox" id="Empleado" value="" style="width:180px; height:25px;"></td>
							<td class="sanLR04" height="20">Turno</td>
							<td class="sanLR04" width="50%">
								<select name="eCodTurno" id="eCodTurno" style="width:142px; height:25px;">
									<option value="">Seleccione...</option>
									<option value="1">Turno 1</option>
									<option value="2">Turno 2</option>
									<option value="3">Turno 3</option>
								</select>
							</td>
						</tr>
						<tr>
							<td class="sanLR04" height="20">Estatus</td>
							<td class="sanLR04" width="50%"><select name="EcodEstatus" id="EcodEstatus" style="width:142px; height:25px;">
									<option value="">Seleccione...</option>
									<option value="AC" selected>Activo</option>
									<option value="CA">Cancelado</option>									
								</select></td>
							<td class="sanLR04" height="20">Folio</td>
							<td class="sanLR04" width="50%"><input type="text" name="folio" dojoType="dijit.form.TextBox" id="folio" value="" style="width:142px; height:25px;"></td>
						</tr>
						<tr>
                            <td class="sanLR04" height="20" nowrap="nowrap">Tipo de Empleado</td>
                            <td class="sanLR04" width="50%">
                                <select name="eCodTipoEntidad" id="eCodTipoEntidad" style="width:180px; height:25px;">
                                	<option value="">Seleccione...</option>
                                    <?php foreach($rsTiposEmpleados as $key => $rTipoEmpleado){ ?>
                                        <option value="<?=$rTipoEmpleado{'eCodTipoEntidad'}?>"><?=utf8_encode($rTipoEmpleado{'tNombreCorto'})?></option>
                                    <?php } ?>
                                </select>
                            </td>
                            <td class="sanLR04" height="20">Tipo Movimiento</td>
							<td class="sanLR04" width="50%">
								<select name="tipoMovimiento" id="tipoMovimiento" style="width:142px; height:25px;">
									<option value="">Seleccione...</option>
									<option value="10">Sin movimiento</option>
									<option value="02">Baja</option>
									<option value="01">Continuacion</option>
									<option value="07">Modificacion</option>
									<option value="08">Reingreso</option>
								</select>
							</td>
						</tr>
						<tr>
							<td class="sanLR04" height="20">Centro de Costo</td>
							<td class="sanLR04" width="50%"><select name="eCodCosto" id="eCodCosto" style="width:142px; height:25px;">
									<option value="">Seleccione...</option>
									<?php foreach($rsCentrosCostos as $key => $rCentroCosto){ ?>
										<option value="<?=$rCentroCosto{'eCodCosto'}?>"><?=utf8_encode($rCentroCosto{'tSiglas'})?></option>
									<?php } ?>
								</select></td>
							<td class="sanLR04" height="20" nowrap>Empresa</td>
				            <td class="sanLR04" width="50%">
				              <select name="eCodEntidad" id="eCodEntidad" style="width:180px; height:25px;">
				                  <option value="">Seleccione...</option>
				                  <?php foreach($rsEmpresas as $key => $rEmpresa){ ?>
				                      <option value="<?=$rEmpresa{'eCodEntidad'}?>"><?=utf8_encode($rEmpresa{'tSiglas'})?></option>
				                  <?php } ?>
				                </select>
				            </td>
						</tr>
						<tr>
							<td class="sanLR04" height="20" nowrap>Motivo</td>
				            <td class="sanLR04" width="50%">
				            	<select name="eCodMotivoAsistencia" id="eCodMotivoAsistencia" style="width:180px; height:25px;">
				                  <option value="">Seleccione...</option>
				                  <?php foreach($rsMotivosAsistencias as $key => $rMotivoAsistencia){ ?>
				                      <option value="<?=$rMotivoAsistencia{'eCodMotivoAsistencia'}?>"><?=utf8_encode($rMotivoAsistencia{'tNombre'})?></option>
				                  <?php } ?>
				                </select>
				            </td>
							<td class="sanLR04" height="20"></td>
							<td class="sanLR04" width="50%"><label><input type="checkbox" name="chkDoble" id="chkDoble" value="1">Solo Turnos Dobles</label></td>
						</tr>
						<tr>
							<td class="sanLR04" height="20"></td>
							<td class="sanLR04" width="50%"><label><input type="checkbox" name="chkModificado" id="chkModificado" value="1">Solo Modificadas</label></td>
							<td class="sanLR04" height="20" nowrap></td>
				            <td class="sanLR04" width="50%"><label><input type="checkbox" name="chkBuque" id="chkBuque" value="1">Con Buque</label></td>
						</tr>					
						<tr>
			              <td nowrap="nowrap" class="sanLR04" height="5"><b>[Ordenamiento]</b></td>
			              <td nowrap="nowrap" class="sanLR04" width="50%"><input type="radio" name="orden" id="orden"  value="ASC" > ASCENDENTE / <input type="radio" name="orden" id="orden" checked="checked" value="DESC" > DESENDENTE</td>
			              
			              <td nowrap="nowrap" class="sanLR04"><b>[Maximos Registros]:</b></td>
			              <td nowrap="nowrap" class="sanLR04" width="50%">
			              	<select name="limite" id="limite" style="width:142px; height:25px;">
                                  <option value="100" selected>100</option>
                                  <option value="1000" >1,000</option>
                                  <option value="10000" >10,000</option>
                                  <option value="100000">100,000</option>
                          	</select>
                          </td>
            			</tr>
						<tr><td class="sanLR04" height="5"></td></tr>
					</table>
				</td>
			</tr>
			<tr><td colspan="4"><div id="dvCNS"></div></td></tr>
		</table>
	</form>
<?php } ?>