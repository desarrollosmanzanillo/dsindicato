<!doctype html>  
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="icon" href="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/favicon.ico" type="image/x-icon"/> 
    <link rel="stylesheet" type="text/css" href="../css/styles.css" />
    <link type="text/css" href="../css/css3.css" rel="stylesheet" />
    <script type="text/javascript" src="../js/jquery-1.6.js"></script>
    <script type="text/javascript" src="../js/jquery.pikachoose.js"></script>
    <script language="javascript">
        $(document).ready(
            function (){
                $("#pikame").PikaChoose();
            });
    </script>
</head>
<body>
    <div id="container">
        <header>
            <a href="#" id="logo"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/LogoEmpresa.png" width="221" height="100" alt=""/></a>    
            <nav>
            	<div id='cssmenu' style="position:absolute; z-index:10;">
                <ul>
                    <li><a href="2.1.php" class="current">[Inicio]</a></li>
                    <li><a href="2.2.php">Opciones</a>
                    	<ul>
							<li><a href='2.2.1.php'><span>Configuraci&oacute;n</span></a>
								<ul><li><a href='2.2.1.1.php'><span>Usuarios</span></a></li></ul>
							</li>
						</ul>
                    </li>
                    <li class='has-sub'><a href="2.3.php">Cat&aacute;logos</a>
                    	<ul>
							<li><a href='2.3.1.php'><span>Empresas</span></a>
								<ul>
									<li><a href='2.3.1.1.php'><span>Entidades</span></a></li>
									<li><a href='2.3.1.2.php'><span>Sucursales</span></a></li>
									<li><a href='2.3.1.3.php'><span>Tipos de Entidades</span></a></li>
									<li><a href='2.3.1.4.php'><span>Almacenes</span></a></li>
									<li><a href='2.3.1.5.php'><span>Patios</span></a></li>
								</ul>
							</li>
							<li><a href='2.3.2.php'><span>Servicios</span></a>
								<ul>
									<li><a href='2.3.2.1.php'><span>Servicios</span></a></li>
									<li><a href='2.3.2.2.php'><span>Tarifas</span></a></li>
									<li><a href='2.3.2.3.php'><span>Tipos de Contenedores</span></a></li>
									<li><a href='2.3.2.4.php'><span>Daños</span></a></li>
								</ul>
							</li>
							<li><a href='2.3.3.php'><span>Buques</span></a>
								<ul>
									<li><a href='2.3.3.1.php'><span>Buques</span></a></li>
									<li><a href='2.3.3.2.php'><span>Tipos de Buques</span></a></li>
								</ul>
							</li>
						</ul>
                    </li>
                    <li><a href="2.4.php">Operaci&oacute;n</a>
                    	<ul>
							<li><a href='2.3.4.1.php'><span>Empresas</span></a></li>
							<li><a href='2.3.4.2.php'><span>Servicios</span></a></li>
							<li><a href='2.3.4.3.php'><span>Maniobras</span></a></li>
						</ul>
                    </li>
                    <li><a href="2.5.php">Consultas</a>
						<ul>
							<li><a href='2.3.5.1.php'><span>Empresas</span></a></li>
							<li><a href='2.3.5.2.php'><span>Servicios</span></a></li>
							<li><a href='2.3.5.3.php'><span>Maniobras</span></a></li>
						</ul>
					</li>
                </ul>
                </div>
            </nav>
        </header>
        <div id="intro">
        	
        </div>
        <div class="holder_content"></div>
    </div>
   <!--end container-->
   
   <!--start footer-->
   <footer>
   <div class="container">  
   <div id="FooterTwo"> Think simple </div>
   <div id="FooterTree"> </div> 
   </div>
   </footer>
</body>
</html>
