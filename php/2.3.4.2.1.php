<?php require_once("conexion/soluciones-mysql.php");  
$link = getLink();
if($_POST){
	if($_POST['eProceso']==1){
		$exito			= 0;
		$tNombre		= ($_POST['tNombre']		? "'".trim(utf8_decode($_POST['tNombre']))."'"		: "NULL");
		$eCodTipoBuque	= ($_POST['eCodTipoBuque']	? (int)$_POST['eCodTipoBuque']						: "NULL");

		if((int)$eCodTipoBuque>0){
			$update=" UPDATE cattiposbuques ".
					" SET tNombre=".$tNombre.
					" WHERE eCodTipoBuque=".$eCodTipoBuque;
			if($res=mysqli_query($link,$update)){
				$exito=1;
			}else{
				$exito=0;
			}
		}else{
			$insert=" INSERT INTO cattiposbuques(tCodEstatus, tNombre) ".
					" VALUES ('AC', ".$tNombre.")";
			if($res=mysqli_query($link,$insert)){
				$exito=1;
				$select=" select last_insert_id() AS Llave ";
				$rCodigo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
				$eCodTipoBuque=(int)$rCodigo['Llave'];
			}else{
				$exito=0;
			}
		}
		print "<input type=\"text\" value=\"".($exito==1 ? $eCodTipoBuque : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}
	
	if($_POST['eProceso']==2){
		$rTipoBuque=mysqli_fetch_array(mysqli_query($link,"SELECT 1 AS TipoBuque FROM cattiposbuques WHERE tNombre='".trim($_POST['tNombre'])."' AND eCodTipoBuque!=".(int)$_POST['eCodTipoBuque']),MYSQLI_ASSOC);
		print "<input name=\"eTipoBuque\" id=\"eTipoBuque\" value=\"".(int)$rTipoBuque{'TipoBuque'}."\">";
	}
}
if(!$_POST){
	
$select=" SELECT * ".
		" FROM cattiposbuques ".
		" WHERE eCodTipoBuque=".$_GET['eCodTipoBuque'];
$rTipoBuque = mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC); ?>
<script type="text/javascript">
function guardar(){
	dojo.byId('eProceso').value=1;
	bandera = false;
	mensaje = "¡Verifique lo siguiente!\n";
	if (!dojo.byId("tNombre").value){
		mensaje+="* Nombre\n";
		bandera = true;		
	}

	if (bandera==true){
		alert(mensaje);		
	}else{
		if (confirm("¿Desea Guardar la Información?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				if(dojo.byId("eCodigo").value>0){
					alert("¡La información se guardó correctamente!");
					tURL = './?ePagina=2.3.4.2.2.php&eCodTipoBuque='+dojo.byId("eCodigo").value;
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
		}
	}
}	

function verificarTipoBuque(){
	if(dojo.byId('tNombre').value){
		dojo.byId('eProceso').value=2;
		dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
			dojo.byId("dvCNS").innerHTML = tRespuesta;
			if(dojo.byId('ePais').value==1){
				alert("¡Este tipo de buque ya existe!");
				dojo.byId('tNombre').value="";
			}
		}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
	}
}

function consultar(){
	document.location = './?ePagina=2.3.4.2.php';
}
</script>
<div id="dvCNS" style="display:none;"></div>
<form action="" method="post" name="Datos" id="Datos" onsubmit="return false;">
<input type="hidden" name="eProceso" id="eProceso" value="" />
<input type="hidden" name="eCodTipoBuque" id="eCodTipoBuque" value="<?=$_GET['eCodTipoBuque'];?>" />
<table border="0" cellpadding="0" cellspacing="0">
    <tr><td height="20"></td></tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Nombre </td>
	    <td width="100%" nowrap class="sanLR04">
           	<input name="tNombre" type="text" dojoType="dijit.form.TextBox" id="tNombre" value="<?=utf8_encode($rTipoBuque{'tNombre'});?>" style="width:175px" onchange="verificarTipoBuque();">
        </td>
    </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle" /> <font class="fntG10K">Indica que es obligatorio llenar el campo.
    </font></td>
    </tr>
</table>
</form>
<?php } ?>