<?php
require_once("conexion/soluciones-mysql.php"); 
$link = getLink();
if($_POST){
	if($_POST['eProceso']==1){
		$exito				= 0;
		$tIP				= ($_SERVER['REMOTE_ADDR']		? "'".trim(utf8_decode(addslashes($_SERVER['REMOTE_ADDR'])))."'"	: "NULL");
		$eCodArea			= ($_POST['eCodArea']			? (int)$_POST['eCodArea']											: "NULL");
		$tPatente			= ($_POST['tPatente']			? "'".trim(utf8_decode(addslashes($_POST['tPatente'])))."'"			: "NULL");
		$eCodCliente		= ($_POST['eCodCliente']		? (int)$_POST['eCodCliente']										: "NULL");
		$eCodNaviera		= ($_POST['eCodNaviera']		? (int)$_POST['eCodNaviera']										: "NULL");
		$eCodUsuario		= ($_POST['eCodUsuario']		? (int)$_POST['eCodUsuario']										: "NULL");
		$tURLArchivo		= ($_POST['tURLArchivo3']		? "'".trim(utf8_decode(addslashes($_POST['tURLArchivo3'])))."'"		: "NULL");
		$eCodSolicitud		= ($_POST['eCodSolicitud']		? (int)$_POST['eCodSolicitud']										: "NULL");
		$eCodFacturarA		= ($_POST['eCodFacturarA']		? (int)$_POST['eCodFacturarA']										: "NULL");
		$fhFechaEntrada		= ($_POST['fhFechaEntrada']		? "'".$_POST['fhFechaEntrada']."'"									: "NULL");
		$tCodContenedor		= ($_POST['tCodContenedor']		? "'".$_POST['tCodContenedor']."'"									: "NULL");
		
		$insert=" INSERT INTO opeentradasmercancias (eCodSolicitud, eCodTipoEntrada, eCodTipoServicio, eCodBuque, eCodNaviera, eCodCliente, eCodFacturarA, ".
				" eCodUsuario, tBL, tCodContenedor, tMercancia, eCodTipoContenedor, eCodEmbalaje, tObservaciones, tCodEstatus, fhFechaEntrada, tNumeroViaje, ".
				" tPatente, tIP, eCodArea) ".
				" SELECT ".$eCodSolicitud.", 1, bs.eCodTipoServicio, bs.eCodBuque, ".$eCodNaviera.", ".$eCodCliente.", ".$eCodFacturarA.", ".
				$eCodUsuario.", bs.tBL, rs.tCodContenedor, rs.tMercancia, rs.eCodTipoContenedor, rs.eCodEmbalaje, rs.tObservaciones, 'FI', ".
				$fhFechaEntrada.", bs.tNumeroViaje, ".$tPatente.", ".$tIP.", ".$eCodArea.
				" FROM opesolicitudesservicios bs ".
				" INNER JOIN relsolicitudesserviciosmercancias rs ON rs.eCodSolicitud=bs.eCodSolicitud ".
				" WHERE bs.eCodSolicitud=".$eCodSolicitud." ".($tCodContenedor<>'NULL' ? " AND rs.tCodContenedor=".$tCodContenedor : '');
		if($res=mysqli_query($link,$insert)){
			$exito=1;
			$select=" select last_insert_id() AS Llave ";
			$rCodigo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
			$eCodEntrada=(int)$rCodigo['Llave'];
			$update=" UPDATE relsolicitudesserviciosmercancias ".
					" SET eCodEntrada=".$eCodEntrada.
					" WHERE eCodSolicitud=".$eCodSolicitud.($tCodContenedor<>"NULL" ? " AND tCodContenedor=".$tCodContenedor : "");
			mysqli_query($link,$update);
			
			$insert=" INSERT INTO relentradasarchivos (eCodEntrada, eCodTipoArchivo, tURL) ".
					" VALUES (".$eCodEntrada.", 3, ".$tURLArchivo.")";
			mysqli_query($link,$insert);
			
			if((int)$_POST['bEIR']>0){
				$insert=" INSERT INTO bitEIRs (eCodEntrada, tCodEstatus, fhFecha) ".
						" VALUES (".$eCodEntrada.", 'AC', CURRENT_TIMESTAMP)";
				if($res=mysqli_query($link,$insert)){
					$select=" select last_insert_id() AS Llave ";
					$rCodigo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
					$eCodEIR=(int)$rCodigo['Llave'];
					foreach($_POST as $campo => $valor){
						if(strstr($campo,'eCodDanio') && $valor && $exito){
							$ePosX 		= $_POST[str_replace('eCodDanio','ePosX',$campo)];
							$ePosY 		= $_POST[str_replace('eCodDanio','ePosY',$campo)];
							$tImagen 	= basename($_POST[str_replace('eCodDanio','tImagen',$campo)]);
							$eCodCara 	= $_POST[str_replace('eCodDanio','eCodCara',$campo)];
							$eCodDanio 	= $valor;
							$eCodVista 	= $_POST[str_replace('eCodDanio','eCodVista',$campo)];

							$insert=" INSERT INTO relEIRsDanios (eCodEIR, eCodVista, eCodCara, eCodDanio, ePosX, ePosY, fhFecha) ".
									" VALUES (".$eCodEIR.", ".$eCodVista.", ".$eCodCara.", ".$eCodDanio.", ".$ePosX.", ".$ePosY.", CURRENT_TIMESTAMP) ";
							if($res=mysqli_query($link,$insert)){
							}else{
								$exito=0;
							}
						}
					}
				}else{
					$exito=0;
				}
			}
			
		}else{
			$exito=0;
		}
		print "<input type=\"text\" value=\"".($exito==1 ? $eCodEntrada : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}
	
	if($_POST['eProceso']==2){	
		header('Content-Type: text/html; charset=windows-1250');		
		$tRespuesta ="<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">";
		$indice	= $_POST['eArchivoIndice'];		
		$tTMP	= $_FILES['tArchivo'.$indice]['tmp_name'];
		$tNombreArchivo="EIR";
		if(is_uploaded_file($tTMP)){
			$extension = pathinfo($_FILES['tArchivo'.$indice]['name'], PATHINFO_EXTENSION);
			$tArchivo = "../archivos/2.4.5.1/(".date("Ymds").")".strtoupper(uniqid()).".".$extension;
			if(move_uploaded_file($tTMP, $tArchivo)){
				$tRespuesta.="<tr><td class=\"sanLR04\"><img width=\"16\" height=\"16\" src=\"https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-CA.png\" style=\"cursor:pointer;\"></td>".
						 "<td class=\"sanLR04\" width=\"100%\"><a href=\"Patio/".utf8_encode($tArchivo)."\" target=\"_blank\" class=\"sanT12\" >".utf8_encode($tNombreArchivo)."</a>".
						 "<input type=\"hidden\" name=\"tURLArchivo".$indice."\" id=\"tURLArchivo".$indice."\" value=\"".utf8_encode($tArchivo)."\" /></td></tr>";
			}else{
				$tRespuesta.="Fallo al subir el archivo";
			}
		}else{
			$tRespuesta.="Fallo al subir el archivo";
		}
		$tRespuesta.="</table>";	
		print "<textarea>".$tRespuesta."</textarea>";
	}

	if($_POST['eProceso']==3){
		$i=0;
		foreach($_POST as $campo => $valor){
			if(strstr($campo,'eCodDanio') && $valor){
				$eCodDanio = $valor;
				$eCodCara = $_POST[str_replace('eCodDanio','eCodCara',$campo)];
				$eCodVista = $_POST[str_replace('eCodDanio','eCodVista',$campo)];
				$tDanios.=($tDanios ? " UNION " : "")." SELECT ".$i." AS eIndice, ".$eCodVista." AS eCodVista, ".$eCodDanio." AS eCodDanio, ".$eCodCara." AS eCodCara";
				$i++;
			}
		}
		///
		$select=" SELECT N1.eIndice, cv.tNombre AS Vista, cc.tNombre AS Cara, cd.tCodDanio, cd.tNombre AS Danio ".
				" FROM (".$tDanios.") AS N1 ".
				" INNER JOIN catCarasContenedores cc ON cc.eCodCara = N1.eCodCara ".
				" INNER JOIN catDaniosContenedores cd ON cd.eCodDanio = N1.eCodDanio ".
				" INNER JOIN catVistasContenedores cv ON cv.eCodVista = N1.eCodVista ".
				" ORDER BY N1.eIndice ";
		$rsDanios = mysqli_query($link,$select);
		if(mysqli_num_rows($rsDanios)){
			$html="<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
			while($rDanio = mysqli_fetch_array($rsDanios,MYSQLI_ASSOC)){
				$html.="<tr><td class=\"sanLR04\" height=\"23\" nowrap=\"nowrap\"><span class=\"fntG10S\">(".utf8_encode($rDanio{'Vista'}).")</span></td><td class=\"sanLR04\" height=\"20\"><span class=\"fntG10S\">".utf8_encode($rDanio{'Cara'})."</span></td><td class=\"sanLR04\"><span class=\"fntR11S\">".utf8_encode("(".trim($rDanio{'tCodDanio'}).")")."</span></td><td class=\"sanLR04\"><span class=\"fntR11S\">".utf8_encode($rDanio{'Danio'})."</span></td></tr>";
			}
			$html.="</table>";
		}else{
			$html="Ninguno";
		}
		print $html;
	}
}
if(!$_POST){ 
$select=" SELECT oss.fhFechaServicio, cbu.tNombre AS Buque, oss.tNumeroViaje, cts.tNombre AS TipoServicio, ".
		" oss.eCodNaviera, oss.tPatente, oss.eCodCliente, oss.eCodFacturarA, oss.tBL, oss.eCodTipoServicio, ".
		" oss.eCodSolicitud ".
		" FROM opesolicitudesservicios oss ".
		" INNER JOIN catbuques cbu ON cbu.eCodBuque=oss.eCodBuque ".
		" INNER JOIN cattiposservicios cts ON cts.eCodTipoServicio=oss.eCodTipoServicio ".
		" WHERE oss.eCodSolicitud=".$_GET['eCodSolicitud'];
$rSolicitud=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

$select=" SELECT ca.*, cta.tNombre AS TipoArea ".
		" FROM catareas ca ".
		" INNER JOIN cattiposareas cta ON cta.eCodTipoArea=ca.eCodTipoArea ".
		" WHERE ca.tCodEstatus='AC' ".
		" ORDER BY ca.tNombre ASC ";
$rsAreas=mysqli_query($link,$select);

$select=" SELECT * ".
		" FROM catentidades ".
		" WHERE tCodEstatus='AC' ".
		" ORDER BY tNombre ";
$rsNavieras=mysqli_query($link,$select);

$select=" SELECT * ".
		" FROM catentidades ".
		" WHERE tCodEstatus='AC' ".
		" ORDER BY tNombre ";
$rsClientes=mysqli_query($link,$select);

$select=" SELECT * ".
		" FROM catentidades ".
		" WHERE tCodEstatus='AC' ".
		" ORDER BY tNombre ";
$rsFacturarA=mysqli_query($link,$select);
?>
<script type="text/javascript">
dojo.require("dojo.number");
dojo.require("dijit.form.NumberTextBox");
dojo.require("dijit.form.DateTextBox");
dojo.require("dojo.io.iframe");
function guardar(){
	dojo.byId('eProceso').value=1;
	var bandera = false;
	var bServicios = false;
	var bMercancias = false;
	mensaje = "¡Verifique lo siguiente!\n";
	if (!dojo.byId("fhFechaEntrada").value){
		mensaje+="* Fecha de Servicio\n";
		bandera = true;		
	}
	if (!dojo.byId("eCodArea").value){
		mensaje+="* Area\n";
		bandera = true;		
	}
	
	if (!dojo.byId("eCodNaviera").value){
		mensaje+="* Naviera\n";
		bandera = true;		
	}
	if (!dojo.byId("eCodCliente").value){
		mensaje+="* Cliente\n";
		bandera = true;		
	}
	if (!dojo.byId("eCodFacturarA").value){
		mensaje+="* Facturar a\n";
		bandera = true;		
	}

	if (bandera==true){
		alert(mensaje);		
	}else{
		if (confirm("¿Desea Guardar la Información?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				if(dojo.byId("eCodigo").value>0){
					alert("¡La información se guardó correctamente!");
					if(dojo.byId("eRegistros").value>1){
						tURL = './?ePagina=2.5.5.1.php';
					}else{
						tURL = './?ePagina=2.5.5.1.1.php&eCodEntrada='+dojo.byId("eCodigo").value;
					}
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
		}
	}
}	

function subirarchivos(indice){
	dojo.byId('eArchivoIndice').value = indice;
	if(dojo.byId("tArchivo"+indice).value){
		dojo.byId("eProceso").value = 2;
		dojo.io.iframe.send({url: "php/"+dojo.byId('ePagina').value+".php", method: "post", handleAs: "text", form: 'Datos', handle: function(tRespuesta, ioArgs){
			dojo.byId("divArchivo"+indice).innerHTML = tRespuesta;
		}});
	}
}

function consultar(){
	document.location = "?ePagina=2.5.5.1.php";
}

//EIR's
eTotal=0;
var bCargando=false;
function agregarDanio(ev,el,p,v,c){
	posm=(mousePos(ev));
	mouX = posm.x;
	mouY = posm.y;
	posd = dojo.position(dojo.byId('divImagen'+v),true);
	divX = posd.x;
	divY = posd.y;
	posX=posm.x-posd.x;
	posY=posm.y-posd.y;
	var eDiv=document.createElement("divPunto"+eTotal);
	eDiv.innerHTML+= "<span class=\"fntR11S\">"+valorRadio(document.Datos.rdDanio)+"</span>";
	eDiv.innerHTML+= "<input type=\"hidden\" name=\"eCodDanio"+eTotal+"\" id=\"eCodDanio"+eTotal+"\" value=\""+dojo.byId("D-"+valorRadio(document.Datos.rdDanio)).value+"\" />";
	eDiv.innerHTML+= "<input type=\"hidden\" name=\"tCodDanio"+eTotal+"\" id=\"tCodDanio"+eTotal+"\" value=\""+valorRadio(document.Datos.rdDanio)+"\" />";
	eDiv.innerHTML+= "<input type=\"hidden\" name=\"eCodVista"+eTotal+"\" id=\"eCodVista"+eTotal+"\" value=\""+v+"\"/>";
	eDiv.innerHTML+= "<input type=\"hidden\" name=\"eCodCara"+eTotal+"\" id=\"eCodCara"+eTotal+"\" value=\""+c+"\"/>";
	eDiv.innerHTML+= "<input type=\"hidden\" name=\"tImagen"+eTotal+"\" id=\"tImagen"+eTotal+"\" value=\""+dojo.byId('img'+v).src+"\"/>";
	eDiv.innerHTML+= "<input type=\"hidden\" name=\"ePosX"+eTotal+"\" id=\"ePosX"+eTotal+"\" value=\""+(posX-6)+"\"/>";
	eDiv.innerHTML+= "<input type=\"hidden\" name=\"ePosY"+eTotal+"\" id=\"ePosY"+eTotal+"\" value=\""+(posY-6)+"\"/>";
	eDiv.id = "divPunto"+eTotal;
	dojo.style(eDiv, 'position','absolute');
	dojo.style(eDiv, 'left', posX-6+'px');
	dojo.style(eDiv, 'top', posY-6+'px');
	dojo.style(eDiv, 'zindex', eTotal * 1000);
	var cDiv=document.createElement("cer"+eTotal);
	cDiv.innerHTML = "<img src=\"https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-ca.png\" width=\"16\" height=\"16\" />";
	dojo.style(cDiv, 'position','absolute');
	dojo.style(cDiv, 'left', '-12px');
	dojo.style(cDiv, 'top', '-12px');
	eDiv.appendChild(cDiv);
	cDiv.onclick = function(){p = (cDiv.parentElement ? cDiv.parentElement : cDiv.parentNode); dojo.byId('divVista'+v).removeChild(p);mostrarDanios();}
	dojo.byId('divVista'+v).appendChild(eDiv);
	dojo.byId('eCodCara').value = "";
	eTotal++;
	mostrarDanios();
}

function asignarCara(ev,el,v,c){
	if(!bCargando){
		p = (el.parentElement.parentElement ? el.parentElement.parentElement : el.parentNode.parentNode);
		agregarDanio(ev,el,p,v,c);
	}
}

function mostrarDanios(){
	bCargando = true;
	dojo.byId("eProceso").value = 3;
	dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
		dojo.byId("divDaniosRegistrados").innerHTML = tRespuesta;
	}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
	bCargando = false;
}
// Fin EIR's

dojo.addOnLoad(function(){
});
</script>
<div id="dvCNS" style="display:none;"></div>
<form method="post" name="Datos" id="Datos" onsubmit="return false;" enctype="multipart/form-data">
<input type="hidden" name="eProceso" id="eProceso" value="" />
<input type="hidden" name="eCodSolicitud" id="eCodSolicitud" value="<?=$rSolicitud{'eCodSolicitud'};?>" />
<input type="hidden" name="tCodContenedor" id="tCodContenedor" value="<?=$_GET['tCodContenedor'];?>" />
<input type="hidden" name="eArchivoIndice" id="eArchivoIndice" value="" />
<input type="hidden" name="ePosX" id="ePosX" />
<input type="hidden" name="tCara" id="tCara" />
<input type="hidden" name="ePosY" id="ePosY" />
<input type="hidden" name="tImagen" id="tImagen" />  
<input type="hidden" name="eCodCara" id="eCodCara" />
<table border="0" cellpadding="0" cellspacing="0" width="980px">
    <tr><td height="20"></td></tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Fecha de Entrada </td>
	    <td width="50%" nowrap class="sanLR04">
           	<input name="fhFechaEntrada"
            id="fhFechaEntrada"
            type="text"
            dojoType="dijit.form.DateTextBox"
            required="false"
            style="width:80px;"
            hasDownArrow="false"
            displayMessage="false"
            value="<?=date("Y-m-d", strtotime($rSolicitud{'fhFechaServicio'}));?>"
            constraints="{datePattern:'dd/MM/yyyy'}"
            />
        </td>
		<td height="23" nowrap class="sanLR04"></td>
	    <td width="50%" nowrap class="sanLR04"></td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Buque </td>
	    <td width="50%" nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'Buque'});?></td>
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Viaje </td>
	    <td width="50%" nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'tNumeroViaje'});?></td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Tipo de Solicitud  </td>
	    <td width="50%" nowrap class="sanLR04">Entrada Directa</td>
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Tipo de Servicio </td>
	    <td width="50%" nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'TipoServicio'});?></td>
    </tr>
	<tr><td colspan="4" align="center"><hr width="95%" size="0" align="center" color="#CACACA"></td></tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Area </td>
	    <td width="50%" nowrap class="sanLR04">
           	<select name="eCodArea" id="eCodArea" style="width:175px">
				<option value="">Seleccione...</option>
				<?php while($rArea=mysqli_fetch_array($rsAreas,MYSQLI_ASSOC)){ ?>
					<option value="<?=$rArea{'eCodArea'}?>"><?=utf8_encode($rArea{'tNombre'}." ".($rArea{'TipoArea'} ? "(".$rArea{'TipoArea'}.")" : ""))?></option>
				<?php } ?>
			</select>
        </td>
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Solicitud</td>
	    <td width="50%" nowrap class="sanLR04"><?=sprintf("%07d",$rSolicitud{'eCodSolicitud'});?></td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Naviera </td>
	    <td width="50%" nowrap class="sanLR04">
           	<select name="eCodNaviera" id="eCodNaviera" style="width:175px">
				<option value="">Seleccione...</option>
				<?php while($rNaviera=mysqli_fetch_array($rsNavieras,MYSQLI_ASSOC)){ ?>
					<option value="<?=$rNaviera{'eCodEntidad'}?>" <?=($rNaviera{'eCodEntidad'}==$rSolicitud{'eCodNaviera'} ? "selected='selected'" : "");?> ><?=utf8_encode($rNaviera{'tNombre'})?></option>
				<?php } ?>
			</select>
        </td>
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Patente </td>
	    <td width="50%" nowrap class="sanLR04">
			<input name="tPatente" type="text" dojoType="dijit.form.TextBox" id="tPatente" value="<?=utf8_encode($rSolicitud{'tPatente'});?>" style="width:80px" UpperCase="true">
        </td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Cliente </td>
	    <td width="50%" nowrap class="sanLR04">
           	<select name="eCodCliente" id="eCodCliente" style="width:175px">
				<option value="">Seleccione...</option>
				<?php while($rCliente=mysqli_fetch_array($rsClientes,MYSQLI_ASSOC)){ ?>
					<option value="<?=$rCliente{'eCodEntidad'}?>" <?=($rCliente{'eCodEntidad'}==$rSolicitud{'eCodCliente'} ? "selected='selected'" : "");?> ><?=utf8_encode($rCliente{'tNombre'})?></option>
				<?php } ?>
			</select>
        </td>
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Facturar a </td>
	    <td width="50%" nowrap class="sanLR04">
			<select name="eCodFacturarA" id="eCodFacturarA" style="width:175px">
				<option value="">Seleccione...</option>
				<?php while($rFacturarA=mysqli_fetch_array($rsFacturarA,MYSQLI_ASSOC)){ ?>
					<option value="<?=$rFacturarA{'eCodEntidad'}?>" <?=($rFacturarA{'eCodEntidad'}==$rSolicitud{'eCodFacturarA'} ? "selected='selected'" : "");?> ><?=utf8_encode($rFacturarA{'tNombre'})?></option>
				<?php } ?>
			</select>
        </td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> BL </td>
	    <td nowrap class="sanLR04" colspan="3"><?=utf8_encode($rSolicitud{'tBL'});?></td>
    </tr>
	<tr><td height="10" nowrap class="sanLR04" colspan="4"></td></tr>
	<tr>
		<td height="23" nowrap class="sanLR04" valign="top" colspan="4">
			<?php
			$eTipoServicio	= (int)$rSolicitud['eCodTipoServicio'];
			$select=" SELECT rs.*, ctc.tNombreCorto AS TipoContenedor, ce.tNombre AS Embalaje ".
					" FROM relsolicitudesserviciosmercancias rs ".
					" LEFT JOIN cattiposcontenedores ctc ON ctc.eCodTipoContenedor=rs.eCodTipoContenedor ".
					" LEFT JOIN catembalajes ce ON ce.eCodEmbalaje=rs.eCodEmbalaje ".
					" WHERE rs.eCodSolicitud=".(int)$rSolicitud['eCodSolicitud'].($_GET['tCodContenedor']!="" ? " AND rs.tCodContenedor='".$_GET['tCodContenedor']."'" : "");
			$rsMercancias=mysqli_query($link,$select); ?>
			<table cellspacing="0" border="0" width="100%" id="tablaMercancias" name="tablaMercancias">
				<thead>
					<tr class="thEncabezado">
						<td nowrap="nowrap" class="sanLR04" height="23"> <?=($eTipoServicio==1 ? "Contenedor" : "Mercanc&iacute;a");?></td>
						<td nowrap="nowrap" class="sanLR04"> <?=($eTipoServicio==1 ? "Tipo" : "Embalaje");?></td>
						<td nowrap="nowrap" class="sanLR04"> Observaciones</td>
						<td nowrap="nowrap" class="sanLR04" width="100%"></td>
					</tr>
				</thead>
				<tbody id="tbMercancias" name="tbMercancias">
					<?php $eFila=0; while($rMercancia=mysqli_fetch_array($rsMercancias,MYSQLI_ASSOC)){ ?>
					<tr id="filaMercancia1" name="filaMercancia1">
						<td nowrap="nowrap" class="sanLR04" height="23">
							<?=($eTipoServicio==1 ? $rMercancia{'tCodContenedor'} : $rMercancia{'tMercancia'});?>
						</td>
						<td nowrap="nowrap" class="sanLR04">
						<?=utf8_encode($eTipoServicio==1 ? $rMercancia{'TipoContenedor'} : $rMercancia{'Embalaje'})?>
						</td>
						<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rMercancia{'tObservaciones'});?></td>
					</tr>
					<?php $eFila++; } ?>
					<input name="eRegistros" id="eRegistros" value="<?=$eFila;?>" type="hidden">
				</tbody>
			</table>
		</td>
	</tr>
	<?php if($_GET['tCodContenedor']!=""){ 
		$select=" SELECT * FROM catVistasContenedores WHERE eCodVista IN(1,2,3) ";
		$rsVistas=mysqli_query($link,$select);
		$select=" SELECT * FROM catDaniosContenedores WHERE tCodEstatus='AC' ";
		$rsDanios = mysqli_query($link,$select); ?> 
		<tr>
			<td height="10" nowrap class="sanLR04" colspan="4"></td>
		</tr>
		<tr>
			<td nowrap="nowrap" class="sanLR04 fntN11B" colspan="3" height="23"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> EIR</td>
			<td><input type="hidden" id="bEIR" name="bEIR" value="1" /></td>
		</tr>
		<tr>
			<td rowspan="2" height="23" valign="top" nowrap="nowrap" class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" width="16" height="16" align="absmiddle"> Inspecci&oacute;n</td>
			<td rowspan="2" valign="top"><table border="0" cellspacing="0" cellpadding="0" class="tbConsulta">
				<? $i=0;
				while($rVista=mysqli_fetch_array($rsVistas,MYSQLI_ASSOC)){
					$select=" SELECT rvc.*, cc.tNombre AS Cara ".
							" FROM relVistasContenedoresCarasContenedores rvc ".
							" INNER JOIN catCarasContenedores cc ON cc.eCodCara=rvc.eCodCara ".
							" WHERE rvc.eCodVista=".$rVista{'eCodVista'};
					$rsCaras = mysqli_query($link,$select);
					if($i) { ?>
						<tr>
							<td height="40" colspan="3" align="center"></td>
						</tr>
					<? } ?>
					<tr class="thEncabezado">
						<td height="20" colspan="3" align="center"><?=utf8_encode($rVista{'tNombre'});?></td>
					</tr>
					<tr>
						<td nowrap="nowrap" ><span class="fntA11S"><?=$rVista{'tCostadoI'};?></span></td>
						<td align="center" nowrap="nowrap" ><span class="fntA11S"><?=$rVista{'tCostadoT'};?></span></td>
						<td height="20" align="right" nowrap="nowrap" >&nbsp;</td>
					</tr>
					<tr>
						<td height="20" colspan="3" align="center">    
						<div id="divImagen<?=$rVista{'eCodVista'};?>" style="width:<?=$rVista{'eAncho'};?>px;height:<?=$rVista{'eAlto'};?>px; position:relative; z-index:<?=$rVista{'eCodVista'};?>;">
						<div id="divVista<?=$rVista{'eCodVista'};?>" style="position:static;"></div>
						<img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/<?=$rVista{'tNombreCorto'};?>.png" width="<?=$rVista{'eAncho'};?>" height="<?=$rVista{'eAlto'};?>" id="img<?=$rVista{'eCodVista'};?>" usemap="#mapa<?=$rVista{'eCodVista'};?>" border="0" />
						<? if(mysqli_num_rows($rsCaras)) { ?>
							<map name="mapa<?=$rVista{'eCodVista'};?>">
							<? while ($rCara=mysqli_fetch_array($rsCaras,MYSQLI_ASSOC)) { ?>
								<area onclick="asignarCara(event,this,'<?=$rVista{'eCodVista'};?>','<?=$rCara{'eCodCara'};?>')" shape="poly" alt="<?=$rCara{'Cara'};?>" coords="<?=$rCara{'tCoordenadas'};?>" nohref="nohref" />
							<? } ?>
							</map>
						<? } ?>
						</div>
						</td>
					</tr>
					<tr>
						<td align="center" nowrap="nowrap">&nbsp;</td>
						<td align="center" nowrap="nowrap"><span class="fntA11S"><?=$rVista{'tCostadoB'};?></span></td>
						<td height="20" align="right" nowrap="nowrap"><span class="fntA11S"><?=$rVista{'tCostadoD'};?></span></td>
					</tr>
					<? $i++;
				} ?>
			</table></td>
			<td valign="top" nowrap="nowrap" class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" width="16" height="16" align="absmiddle"> Da&ntilde;os</td>
			<td valign="top" nowrap="nowrap" class="sanLR04"><table border="0" cellspacing="0" cellpadding="0">
				<? $i= 0;
				while($rDanio=mysqli_fetch_array($rsDanios,MYSQLI_ASSOC)){ ?>
					<tr>
						<td height="20" nowrap="nowrap"><label>
						<input type="radio" name="rdDanio" id="rdDanio" value="<?=trim($rDanio{'tCodDanio'});?>" <?=(!$i ? "checked=\"checked\"" : "")?> />
						<input type="hidden" name="D-<?=trim($rDanio{'tCodDanio'});?>" id="D-<?=trim($rDanio{'tCodDanio'});?>" value="<?=trim($rDanio{'eCodDanio'});?>" />
						<?="(".trim($rDanio{'tCodDanio'}).")";?> / <?=utf8_encode($rDanio{'tNombre'});?>
						</label></td>
					</tr>
					<? $i++;
				} ?>
			</table></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" width="16" height="16" align="absmiddle"> Da&ntilde;os Reportados</td>
			<td valign="top" class="sanLR04" style="max-width:220px; min-width:220px"><div id="divDaniosRegistrados">Ninguno</div></td>
		</tr>
		<tr>
			<td height="10" nowrap class="sanLR04" colspan="4"></td>
		</tr>
		<tr>
			<td nowrap="nowrap" class="" height="23"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> EIR</td>
			<td nowrap="nowrap" class="sanLR04" width="100%">
				<input id="tArchivo3" name="tArchivo3" type="file" class="uploadBtn" onkeyup="this.value=''" style="width:325px" onchange="subirarchivos(3);" />
			</td>
			<td nowrap="nowrap" class="sanLR04" width="100%">
				<div id="divArchivo3"></div>
			</td>
		</tr>
	<?php } ?>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle" /> <font class="fntG10K">Indica que es obligatorio llenar el campo.
    </font></td>
    </tr>
</table>
</form>
<?php } ?>