<?php require_once("conexion/soluciones-mysql.php"); 
$link = getLink(); 
if($_POST){
	if($_POST['eProceso']==1){
		$exito= 0;
		$tCLABE= ($_POST['tCLABE']	? "'".trim(utf8_decode($_POST['tCLABE']))."'"	: "NULL");
		$tNombre= ($_POST['tNombre']	? "'".trim(utf8_decode($_POST['tNombre']))."'"	: "NULL");
		$eCodBanco= ($_POST['eCodBanco']	? (int)$_POST['eCodBanco']	: "NULL");
		$tNumeroCuenta= ($_POST['tNumeroCuenta']	? "'".trim(utf8_decode($_POST['tNumeroCuenta']))."'"	: "NULL");
		$eCodCuentaBancaria= ($_POST['eCodCuentaBancaria']	? (int)$_POST['eCodCuentaBancaria']	: "NULL");

		if((int)$eCodCuentaBancaria>0){
			$update=" UPDATE catcuentasbancarias ".
					" SET tNombre=".$tNombre.",".
					" eCodBanco=".$eCodBanco.",".
					" tNumeroCuenta=".$tNumeroCuenta.",".
					" tCLABE=".$tCLABE.
					" WHERE eCodCuentaBancaria=".$eCodCuentaBancaria;
			print $update;
			if($res=mysqli_query($link,$update)){
				$exito=1;
			}else{
				$exito=0;
			}
		}else{
			$insert=" INSERT INTO catcuentasbancarias(tNombre, eCodBanco, tCodEstatus, tNumeroCuenta, tCLABE) ".
					" VALUES (".$tNombre.", ".$eCodBanco.", 'AC', ".$tNumeroCuenta.", ".$tCLABE.")";
			print $insert;
			if($res=mysqli_query($link,$insert)){
				$exito=1;
				$select=" select last_insert_id() AS Llave ";
				$rCodigo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
				$eCodCuentaBancaria=(int)$rCodigo['Llave'];
			}else{
				$exito=0;
			}
		}
		print "<input type=\"text\" value=\"".($exito==1 ? $eCodCuentaBancaria : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}

}
if(!$_POST){
$select=" SELECT * ".
		" FROM catcuentasbancarias ".
		" WHERE eCodCuentaBancaria=".(int)$_GET['eCodCuentaBancaria'];
$rCuentaBancaria=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC); 

$select=" SELECT * ".
		" FROM catbancos ".
		" ORDER BY tNombre "; 
$rsBancos=mysqli_query($link,$select); ?>
<script type="text/javascript">
function guardar(){
	dojo.byId('eProceso').value=1;
	bandera = false;
	mensaje = "¡Verifique lo siguiente!\n";
	if (!dojo.byId("tNombre").value){
		mensaje+="* Nombre\n";
		bandera = true;		
	}

	if (bandera==true){
		alert(mensaje);		
	}else{
		if (confirm("¿Desea Guardar la Información?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				if(dojo.byId("eCodigo").value>0){
					alert("¡La información se guardó correctamente!");
					tURL = './?ePagina=2.3.6.3.2.php&eCodCuentaBancaria='+dojo.byId("eCodigo").value;
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
		}
	}
}	

function consultar(){
	document.location = './?ePagina=2.3.6.3.php';
}
</script>
<div id="dvCNS" style="display:none;"></div>
<form action="" method="post" name="Datos" id="Datos" onsubmit="return false;">
<input type="hidden" name="eProceso" id="eProceso" value="" />
<input type="hidden" name="eCodCuentaBancaria" id="eCodCuentaBancaria" value="<?=$_GET['eCodCuentaBancaria'];?>" />
<table border="0" cellpadding="0" cellspacing="0" width="980px">
    <tr><td height="20"></td></tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" height="16" width="16" align="absmiddle"> Nombre</td>
	    <td width="50%" nowrap class="sanLR04">
			<input name="tNombre" type="text" dojoType="dijit.form.TextBox" id="tNombre" value="<?=utf8_encode($rCuentaBancaria{'tNombre'});?>" style="width:175px">
        </td>
        <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" height="0" width="16" align="absmiddle"> Banco</td>
	    <td width="50%" nowrap class="sanLR04">
           	<select name="eCodBanco" id="eCodBanco" style="width:175px">
				<option value="">Seleccione...</option>
				<?php while($rBanco=mysqli_fetch_array($rsBancos,MYSQLI_ASSOC)){ ?>
					<option value="<?=$rBanco{'eCodBanco'}?>" <?=($rBanco{'eCodBanco'}==$rCuentaBancaria{'eCodBanco'} ? "selected='selected'" : "");?> >
						<?=utf8_encode($rBanco{'tNombre'})?>
                    </option>
				<?php } ?>
			</select>
        </td>
		
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" height="0" width="16" align="absmiddle"> N&oacute;. Cuenta</td>
	    <td width="50%" nowrap class="sanLR04">
           	<input name="tNumeroCuenta" type="text" dojoType="dijit.form.TextBox" id="tNumeroCuenta" value="<?=utf8_encode($rCuentaBancaria{'tNumeroCuenta'});?>" style="width:175px">
        </td>
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" height="0" width="16" align="absmiddle"> CLABE </td>
	    <td width="50%" nowrap class="sanLR04">
           	<input name="tCLABE" type="text" dojoType="dijit.form.TextBox" id="tCLABE" value="<?=utf8_encode($rCuentaBancaria{'tCLABE'});?>" style="width:175px">
        </td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle" /> <font class="fntG10K">Indica que es obligatorio llenar el campo.
    </font></td>
    </tr>
</table>
</form>
<?php } ?>