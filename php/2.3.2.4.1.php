<?php require_once("conexion/soluciones-mysql.php");
$link = getLink();
if($_POST){
	if($_POST['eProceso']==1){
		$exito			= 0;
		$eCodDia		= ($_POST['eCodDia']		? (int)$_POST['eCodDia']							: "NULL");
		$fhFecha		= ($_POST['fhFecha']		? "'".$_POST['fhFecha']."'"							: "NULL");
		$tObservacion	= ($_POST['tObservacion']	? "'".trim(utf8_decode($_POST['tObservacion']))."'"	: "NULL");

		if((int)$eCodDia>0){
			$update=" UPDATE catdiasfestivos ".
					" SET fhFecha=".$fhFecha.",".
					" tObservacion=".$tObservacion.
					" WHERE eCodDia=".$eCodDia;
			if($res=mysqli_query($link,$update)){
				$exito=1;
			}else{
				$exito=0;
			}
		}else{
			$insert=" INSERT INTO catdiasfestivos(ecodEstatus, fhFecha, tObservacion) ".
					" VALUES ('AC', ".$fhFecha.", ".$tObservacion.")";
			if($res=mysqli_query($link,$insert)){
				$exito=1;
				$select=" select last_insert_id() AS Llave ";
				$rCodigo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
				$eCodDia=(int)$rCodigo['Llave'];
			}else{
				$exito=0;
			}
		}
		print "<input type=\"text\" value=\"".($exito==1 ? $eCodDia : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}
	
	/*if($_POST['eProceso']==2){
		$rDiaFestivo=mysql_fetch_array(mysql_query("SELECT 1 AS Area FROM catdiasfestivos WHERE tNombre='".trim($_POST['tNombre'])."' AND eCodDia!=".(int)$_POST['eCodDia']));
		print "<input name=\"eArea\" id=\"eArea\" value=\"".(int)$rDiaFestivo{'Area'}."\">";
	}*/
}
if(!$_POST){
	
$select=" SELECT * ".
		" FROM catdiasfestivos ".
		" WHERE eCodDia=".$_GET['eCodDia'];
$rDiaFestivo = mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC); ?>
<script type="text/javascript">
function guardar(){
	dojo.byId('eProceso').value=1;
	bandera = false;
	mensaje = "¡Verifique lo siguiente!\n";
	if (!dojo.byId("fhFecha").value){
		mensaje+="* Fecha\n";
		bandera = true;		
	}
	if (!dojo.byId("tObservacion").value){
		mensaje+="* Observaciones\n";
		bandera = true;		
	}

	if (bandera==true){
		alert(mensaje);		
	}else{
		if (confirm("¿Desea Guardar la Información?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				if(dojo.byId("eCodigo").value>0){
					alert("¡La información se guardó correctamente!");
					tURL = './?ePagina=2.3.2.4.2.php&eCodDia='+dojo.byId("eCodigo").value;
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
		}
	}
}	

/*function verificarTipo(){
	if(dojo.byId('tNombre').value){
		dojo.byId('eProceso').value=2;
		dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
			dojo.byId("dvCNS").innerHTML = tRespuesta;
			if(dojo.byId('eArea').value==1){
				alert("¡Esta area ya existe!");
				dojo.byId('tNombre').value="";
			}
		}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
	}
}*/

function consultar(){
	document.location = './?ePagina=2.3.2.4.php';
}
</script>
<div id="dvCNS" style="display:none;"></div>
<form action="" method="post" name="Datos" id="Datos" onsubmit="return false;">
<input type="hidden" name="eProceso" id="eProceso" value="" />
<input type="hidden" name="eCodDia" id="eCodDia" value="<?=$_GET['eCodDia'];?>" />
<table border="0" cellpadding="0" cellspacing="0">
    <tr><td height="20"></td></tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Fecha </td>
	    <td width="50%" nowrap class="sanLR04">
           	<input name="fhFecha" type="date" dojoType="dijit.form.TextBox" id="fhFecha" value="<?=$rDiaFestivo{'fhFecha'};?>" style="width:130px" >
        </td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Observaciones </td>
	    <td width="50%" nowrap class="sanLR04">
           	<input name="tObservacion" type="text" dojoType="dijit.form.TextBox" id="tObservacion" value="<?=utf8_encode($rDiaFestivo{'tObservacion'});?>" style="width:175px">
        </td>
    </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle" /> <font class="fntG10K">Indica que es obligatorio llenar el campo.
    </font></td>
    </tr>
</table>
</form>
<?php } ?>