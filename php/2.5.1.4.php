<?php
require_once("conexion/soluciones-mysql.php");
$link = getLink();
if($_POST){

	if ($_POST['fhFechaAsistenciaInicio']!=""){
  		$fhFechaAsistenciaInicio = $_POST['fhFechaAsistenciaInicio'].' 00:00:00';
  		$fhFechaAsistenciaFin = ($_POST['fhFechaAsistenciaFin']!="" ? $_POST['fhFechaAsistenciaFin'] : $_POST['fhFechaAsistenciaInicio']).' 23:59:59';
 	}
	$select=" SELECT aus.*, emp.Empleado AS tNombre, tpo.eCodTipoEntidad,  tpo.tNombreCorto, usr.tNombre AS tUsuario, 
			usr.tApellidos, cta.tNombre AS Tipo, CONCAT_WS(' ', usrc.tNombre, usrc.tApellidos) AS UCancelacion
			FROM ausentismo aus
			LEFT JOIN empleados emp ON emp.id=aus.eCodEmpleado
			LEFT JOIN catusuarios usr ON usr.eCodUsuario=aus.eCodUsuario
			LEFT JOIN cattipoempleado tpo ON emp.eCodTipo=tpo.eCodTipoEntidad
			LEFT JOIN cattipoausentismo cta ON cta.eCodTipoAusentismo=aus.eCodTipoAusentismo 
			LEFT JOIN catusuarios usrc ON usrc.eCodUsuario=aus.eCodUsuarioCancelacion
			WHERE 1=1 ".
			($_POST['Empleado'] 				? " AND emp.Empleado like '%".$_POST['Empleado']."%'" 			: "").
			($_POST['EcodEstatus'] 				? " AND aus.eCodEstatus='".$_POST['EcodEstatus']."'"			: "").
			($_POST['eCodAsistencia'] 			? " AND aus.eCodAusentismo=".$_POST['eCodAsistencia'] 			: "").
			($_POST['eCodTipoAusentismo'] 		? " AND aus.eCodTipoAusentismo=".$_POST['eCodTipoAusentismo']	: "").
			($_POST['fhFechaAsistenciaInicio']	? " AND aus.fhFecha BETWEEN '".$fhFechaAsistenciaInicio. "' AND '" .$fhFechaAsistenciaFin."'" : "" ).
			" ORDER BY aus.fhRegistro ".($_POST['orden'] ? $_POST['orden'] : "DESC" ).", emp.Empleado ASC ".
			" LIMIT ".($_POST['limite'] ? $_POST['limite'] : "1000");
	$rsSolicitudes=mysqli_query($link,$select);
	$rsUsuarios=mysqli_query($link,$select);
	$registros=(int)mysqli_num_rows($rsUsuarios);
	?>
    <table cellspacing="0" border="0" width="965px">
		<tr>
			<td width="50%"><hr color="#666666" /></td>
			<td class="sanLR04" nowrap="nowrap"><b>Registros Encontrados: ( <?=$registros;?> )</b></td>
			<td width="50%"><hr color="#666666" /></td>
		</tr>
    </table>
	<div style="display:block; top:0; left:0; width:965px; z-index=1; overflow: auto; height:400px;">
	<table cellspacing="0" border="0" width="965px">
		<thead>
			<tr class="thEncabezado">
				<td nowrap="nowrap" class="sanLR04" height="20" align="center">E</td>
				<td nowrap="nowrap" class="sanLR04" >C&oacute;digo</td>
				<td nowrap="nowrap" class="sanLR04" >Fecha</td>
				<td nowrap="nowrap" class="sanLR04" >Tipo</td>
				<td nowrap="nowrap" class="sanLR04" >Empleado</td>
				<td nowrap="nowrap" class="sanLR04" >T. Empleado</td>
				<td nowrap="nowrap" class="sanLR04" >Usuario</td>
				<td nowrap="nowrap" class="sanLR04" >F. Registro</td>
				<td nowrap="nowrap" class="sanLR04" >Usuario Cancelaci&oacute;n</td>
				<td nowrap="nowrap" class="sanLR04" >F. Cancelaci&oacute;n</td>
				<td nowrap="nowrap" class="sanLR04" width="100%">Motivo de Cancelaci&oacute;n</td>
			</tr>
		</thead>
		<tbody>
			<?php $i=1; while($rSolicitud=mysqli_fetch_array($rsSolicitudes, MYSQLI_ASSOC)){ ?>
				<tr>
					<td nowrap="nowrap" class="sanLR04" height="20" align="center"><img width="16" height="16" alt="Empresa" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-<?=$rSolicitud['eCodEstatus'];?>.png"></td>
					<td nowrap="nowrap" class="sanLR04 colmenu"><a href="?ePagina=2.4.1.2.php&eCodAusentismo=<?=$rSolicitud['eCodAusentismo'];?>"><b><?=sprintf("%07d",$rSolicitud['eCodAusentismo']);?></b></a></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=date("d/m/Y", strtotime($rSolicitud['fhFecha']));?></td>
					<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rSolicitud['Tipo']);?></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rSolicitud['tNombre']);?></td>
					<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rSolicitud['tNombreCorto']);?></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rSolicitud['tUsuario']." ".$rSolicitud['tApellidos']);?></td>
					<td nowrap="nowrap" class="sanLR04"><?=date("d/m/Y H:i", strtotime($rSolicitud['fhRegistro']));?></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rSolicitud['UCancelacion']);?></td>
					<td nowrap="nowrap" class="sanLR04"><?=($rSolicitud['fhFechaCancelacion'] ? date("d/m/Y H:i", strtotime($rSolicitud['fhFechaCancelacion'])) : "");?></td>
					<td nowrap="nowrap" class="sanLR04 columnB" width="100%"><?=utf8_encode($rSolicitud['tMotivoCancelacion']);?></td>
				</tr>
			<?php $i++; } ?>
		</tbody>
	</table>
	</div>
<?php }else{ ?>
<script type="text/javascript">
dojo.require("dojo.number");
dojo.require("dijit.form.NumberTextBox");
dojo.require("dijit.form.DateTextBox");
dojo.require("dojo.io.iframe");

function nuevo(){
	document.location = "?ePagina=2.4.1.2.php";
}

function generaExcel(){
	var UrlExcel = "php/excel/2.5.1.4.php?"+
	(dojo.byId('eCodAsistencia').value ? "&eCodAsistencia="+dojo.byId('eCodAsistencia').value : "")+
	(dojo.byId('fhFechaAsistenciaInicio').value ? "&fhFechaAsistenciaInicio="+dojo.byId('fhFechaAsistenciaInicio').value : "")+
	(dojo.byId('fhFechaAsistenciaFin').value ? "&fhFechaAsistenciaFin="+dojo.byId('fhFechaAsistenciaFin').value : "")+		
	(dojo.byId('Empleado').value ? "&Empleado="+dojo.byId('Empleado').value : "")+				
	(dojo.byId('EcodEstatus').value ? "&EcodEstatus="+dojo.byId('EcodEstatus').value : "")+
	(dojo.byId('eCodTipoAusentismo').value ? "&eCodTipoAusentismo="+dojo.byId('eCodTipoAusentismo').value : "")+
	(dojo.byId('orden').value ? "&orden="+dojo.byId('orden').value : "")+
	(dojo.byId('limite').value ? "&limite="+dojo.byId('limite').value : "");		
	var Datos = document.createElement("FORM");
	document.body.appendChild(Datos);
	Datos.name='Datos';
	Datos.method = "POST";
	Datos.action = UrlExcel;
	Datos.submit();
	}

dojo.addOnLoad(function(){filtrarConsulta();});
</script>
<form name="Datos" id="Datos" method="post" action="" onsubmit="return false;">
	<input type="hidden" value="0" name="ePagina" id="ePagina" />
	<input type="hidden" value="" name="eAccion" id="eAccion" />
	<input type="hidden" value="<?=(int)$_SESSION['sesionUsuario']['eCodEntidad'];?>" name="eEntidad" id="eEntidad" />
	<table width="965px" border="0">
		<tr>
			<td colpan="3" width="100%"></td>
			<td align="right" nowrap="nowrap"><a class="fntBR11" href="javaScript:mostrarFiltros('Busqueda');">Busqueda de Registros</a></td>
		</tr>
		<tr id="trBusqueda" style="display:none">
			<td colspan="4">
				<table width="100%"  bgcolor="#f9f9f9">
					<tr><td class="sanLR04" height="5"></td></tr>
					<tr>
						<td class="sanLR04" height="20">C&oacute;digo</td>
						<td class="sanLR04" width="40%"><input type="text" name="eCodAsistencia" dojoType="dijit.form.TextBox" id="eCodAsistencia" value="" style="width:80px; height:25px;"></td>
						<td class="sanLR04" nowrap="nowrap">Fecha de Ausentismo</td>
						<td class="sanLR04" width="50%">
						<input name="fhFechaAsistenciaInicio" id="fhFechaAsistenciaInicio" type="date"  required="false"  /> -
						<input name="fhFechaAsistenciaFin" id="fhFechaAsistenciaFin" type="date"  required="false"  />
						</td>
					</tr>						
					<tr>
						<td class="sanLR04" height="20">Empleado</td>
						<td class="sanLR04" width="50%"><input type="text" name="Empleado" dojoType="dijit.form.TextBox" id="Empleado" value="" style="width:180px; height:25px;"></td>
						<td class="sanLR04" height="20">Estatus</td>
						<td class="sanLR04" width="50%"><select name="EcodEstatus" id="EcodEstatus" style="width:142px; height:25px;">
								<option value="">Seleccione...</option>
								<option value="AC">Activo</option>
								<option value="CA">Cancelado</option>									
							</select></td>
					</tr>
					<tr>
						<td class="sanLR04" height="20">Tipo</td>
						<td class="sanLR04" width="50%">
							<select name="eCodTipoAusentismo" id="eCodTipoAusentismo" style="width:142px; height:25px;">
								<option value="" selected>Seleccione...</option>
								<option value="2">Castigo</option>
								<option value="1">Permiso</option>									
							</select>
						</td>
						<td class="sanLR04" height="20"></td>
						<td class="sanLR04" width="50%"></td>
					</tr>												
					<tr>
		              <td nowrap="nowrap" class="sanLR04" height="5"><b>[Ordenamiento]</b></td>
		              <td nowrap="nowrap" class="sanLR04" width="50%"><input type="radio" name="orden" id="orden"  value="ASC" > ASCENDENTE / <input type="radio" name="orden" id="orden" checked="checked" value="DESC" > DESENDENTE</td>
		              <td nowrap="nowrap" class="sanLR04"><b>[Maximos Registros]:</b></td>
		              <td nowrap="nowrap" class="sanLR04" width="50%">
		              	<select name="limite" id="limite" style="width:142px; height:25px;">
                              <option value="100">100</option>
                              <option value="1000" selected>1,000</option>
                              <option value="10000" >10,000</option>
                              <option value="100000">100,000</option>
                      	</select>
                      </td>
        			</tr>
					<tr><td class="sanLR04" height="5"></td></tr>
				</table>
			</td>
		</tr>
		<tr><td colspan="4"><div id="dvCNS"></div></td></tr>
	</table>
</form>
<?php } ?>