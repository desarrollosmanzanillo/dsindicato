<?php 
require_once("conexion/soluciones-mysql.php"); 
$link = getLink();
if($_POST){
}else{
$select=" SELECT ono.tCodEstatus, ono.eCodNotificacionPago, ono.fhFecha, ono.fhFechaMovimiento, ".
		" ono.eCodTipoMovimiento, cmp.tNombre AS MetodoPago, ono.tReferencia, ono.dImporte, ".
		" ce.tNombre AS Agencia, ces.tNombre AS Estatus, cu.tNombre AS Usuario, ".
		" cu.tApellidos AS UsuarioA, ccb.tNombre AS CuentaBancaria ".
		" FROM openotificacionespagos ono ".
		" LEFT JOIN catmetodospago cmp ON cmp.eCodMetodoPago=ono.eCodMetodoPago ".
		" LEFT JOIN catentidades ce ON ce.eCodEntidad=ono.eCodAgenciaAduanal ".
		" LEFT JOIN catestatus ces ON ces.tCodEstatus=ono.tCodEstatus ".
		" LEFT JOIN catusuarios cu ON cu.eCodUsuario=ono.eCodUsuario ".
		" LEFT JOIN catcuentasbancarias ccb ON ccb.eCodCuentaBancaria=ono.eCodCuentaBancaria ".
		" WHERE eCodNotificacionPago=".(int)$_GET['eCodNotificacionPago'];
$rNotificacion=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

$select=" SELECT oc.tSerie, oc.eFolio, cc.tNombre AS Comprobante, oc.dTotal, oc.dSaldo, rn.dAbonado ".
		" FROM relnotificacionespagoscfds rn ".
		" LEFT JOIN opecfds oc ON oc.eCodCFD=rn.eCodCFD ".
		" LEFT JOIN catcomprobantes cc ON cc.eCodComprobante=oc.eCodComprobante ".
		" WHERE rn.eCodNotificacionPago=".(int)$_GET['eCodNotificacionPago'];
$rsCFDs=mysqli_query($link,$select); ?>
<script type="text/javascript">	

function consultar(){
	document.location = './?ePagina=2.5.6.2.php';
}
function nuevo(){
	document.location = './?ePagina=2.4.6.3.php';
}

function generarPDF(){
	var urlPDF ='php/pdf/2.5.6.2.1.php?eCodNotificacionPago='+<?=$rNotificacion{'eCodNotificacionPago'}?>;
	var submitForm =document.createElement("FORM");
	document.body.appendChild(submitForm);
	submitForm.method="POST";
	submitForm.action=urlPDF;
	submitForm.target="_blank";
	submitForm.submit();	 
}

</script>
<div id="dvCNS" style="display:none;"></div>
<form id="Datos" name="Datos" method="post" onSubmit="return false;" enctype="multipart/form-data">
<input type="hidden" id="eCodCFD" name="eCodCFD" value="<?=(int)$_GET['eCodNotificacionPago'];?>">
<input type="hidden" id="eUsuario" name="eUsuario" value="<?=(int)$_SESSION['sesionUsuario']['eCodUsuario'];?>">
<input type="hidden" value="<?=(int)$_SESSION['sesionUsuario']['eCodEntidad'];?>" name="eEntidad" id="eEntidad" />
<table border="0" cellpadding="0" cellspacing="0" width="980px">
	<tr><td height="20"></td></tr>    
	<tr>
	    <td height="23" nowrap class="sanLR04"> C&oacute;digo</td>
	    <td width="50%" nowrap class="sanLR04"><?=sprintf("%07d",$rNotificacion{'eCodNotificacionPago'});?></td>
		<td nowrap class="sanLR04"> Estatus</td>
	    <td width="50%" nowrap class="sanLR04"><?=($rNotificacion{'Estatus'})?></td>
    </tr>
	<tr>
		<td height="23" nowrap class="sanLR04"> Fecha de Movimiento</td>
	    <td nowrap class="sanLR04"><?=date("d/m/Y", strtotime($rNotificacion{'fhFechaMovimiento'}));?></td>
		<td nowrap class="sanLR04">Tipo de Movimiento</td>
	    <td nowrap class="sanLR04"><?=((int)$rCFD{'eCodTipoMovimiento'}==1 ? "Nuevo" : "Saldo a Favor");?></td>
    </tr>
	<tr>
		<td height="23" nowrap class="sanLR04"> Usuario</td>
	    <td nowrap class="sanLR04"><?=($rNotificacion{'Usuario'}." ".$rNotificacion{'UsuarioA'})?></td>
		<td nowrap class="sanLR04"> Fecha</td>
	    <td nowrap class="sanLR04"><?=date("d/m/Y H:i", strtotime($rNotificacion{'fhFecha'}));?></td>
    </tr>
    <tr>
		<td height="23" nowrap class="sanLR04" valign="top"> Metodo de Pago</td>
	    <td class="sanLR04"><?=($rNotificacion{'MetodoPago'})?></td>
		<td nowrap class="sanLR04" valign="top"> Cuenta</td>
	    <td class="sanLR04"><?=($rNotificacion{'CuentaBancaria'})?></td>
    </tr>
    <tr>
		<td height="23" nowrap class="sanLR04" valign="top"> Agencia Aduanal</td>
	    <td class="sanLR04"><?=($rNotificacion{'Agencia'})?></td>
		<td nowrap class="sanLR04" valign="top"> Importe</td>
	    <td class="sanLR04">$<?=number_format($rNotificacion{'dImporte'},2)?></td>
    </tr>
	<tr>
		<td height="23" nowrap class="sanLR04" valign="top"> Referencia</td>
	    <td class="sanLR04"><?=($rNotificacion{'tReferencia'})?></td>
		<td nowrap class="sanLR04" valign="top"> </td>
	    <td class="sanLR04"></td>
    </tr>
    <tr>
		<td height="23" nowrap class="sanLR04" valign="top" colspan="4"> </td>
    </tr>
	<tr>
		<td height="23" nowrap class="sanLR04" valign="top">Facturas Abonadas</td>
        <td height="23" nowrap class="sanLR04" colspan="3"> 
			<table cellspacing="0" border="0" width="100%">
				<thead>
					<tr class="thEncabezado">
						<td nowrap="nowrap" class="sanLR04" height="23">C&oacute;digo</td>
                        <td nowrap="nowrap" class="sanLR04" width="100%">Comprobante</td>
                        <td nowrap="nowrap" class="sanLR04" align="right">Total</td>
						<td nowrap="nowrap" class="sanLR04" align="right">Saldo</td>
						<td nowrap="nowrap" class="sanLR04" align="right">Asignado</td>
					</tr>
				</thead>
				<tbody>
					<?php 
					while($rCFD=mysqli_fetch_array($rsCFDs,MYSQLI_ASSOC)){ ?>
						<tr>
							<td nowrap="nowrap" class="sanLR04" align="right"><?=$rCFD{'tSerie'};?><?=sprintf("%07d",$rCFD{'eFolio'});?></td>
							<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rCFD{'Comprobante'});?></td>
                            <td nowrap="nowrap" class="sanLR04" align="right"><?=number_format($rCFD{'dTotal'}, 2);?></td>
                            <td nowrap="nowrap" class="sanLR04" align="right"><?=number_format($rCFD{'dSaldo'}, 2);?></td>
							<td nowrap="nowrap" class="sanLR04" align="right"><?=number_format($rCFD{'dAbonado'}, 2);?></td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</td>
    </tr>
</table>
</form>
<?php } ?>