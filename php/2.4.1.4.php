﻿<?php require_once("conexion/soluciones-mysql.php");
$link = getLink();
if($_POST){
	
	if($_POST['eProceso']==1){
		$exito = 1;
		$bonos = 0;
		$eUsuario=(int)$_POST['eUsuario'];

		foreach($_POST as $k => $v){
			$nombre = strval($k);
			if(strstr($nombre,"eRegistro") && $v){
				$eFila=str_replace("eRegistro", "", $nombre);
				$eCodBono = ($_POST['eCodBono'.$eFila] ? (int)$_POST['eCodBono'.$eFila] : "NULL");
				$dPeso = ($_POST['dPeso'.$eFila] ? (float)$_POST['dPeso'.$eFila] : "NULL");
				$fhFecha = ($_POST['fhFecha'.$eFila] ? "'".$_POST['fhFecha'.$eFila]."'" : "NULL");
				$turno = ($_POST['turno'.$eFila] ? (int)$_POST['turno'.$eFila] : "NULL");
				
				if((int)$eCodBono>0){
					$insert=" INSERT INTO opemaniobrabono (eCodBono, tCodEstatus, dPeso, fhFecha, dTurno, eCodUsuario, fhFechaRegistro) 
							SELECT ".(int)$eCodBono.", 'AC', ".$dPeso.", ".$fhFecha.", ".$turno.", ".$eUsuario.", CURRENT_TIMESTAMP ";
					if($res=mysqli_query($link,$insert)){
						$cadena.=$insert;
						$bonos++;
					}else{
						$cadena.=$insert;
						$exito=0;
					}
				}
			}
		}
		print "<input type=\"text\" value=\"".((int)$exito>0 && (int)$bonos>0 ? 1 : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}

	if($_POST['eProceso']==2){
		$select=" SELECT eCodManiobra ".
				" FROM opemaniobrabono ".
				" WHERE fhFecha='".$_POST['fhFecha'.(int)$_POST['eFilaBono']]."'".
				" AND dTurno=".(int)$_POST['turno'.(int)$_POST['eFilaBono']].
				" AND eCodBono=".(int)$_POST['eCodBono'.(int)$_POST['eFilaBono']].
				" AND tCodEstatus='AC'";
		$bono=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
		print (int)$bono{'eCodManiobra'};
	}
//	
}
if(!$_POST){
$select=" SELECT * ".
		" FROM cattipobono ".
		" WHERE tCodEstatus='AC' ".
		" ORDER BY tNombre ";
$TiposBonos=mysqli_query($link,$select);

$select=" SELECT * ".
		" FROM catcostos WHERE tCodEstatus='AC' ".
		" ORDER BY tSiglas ";
$rsCostos=mysqli_query($link,$select); 

$select=" SELECT * FROM categorias WHERE tCodEstatus='AC' ORDER BY Categoria ASC ";
$rsCategorias=mysqli_query($link,$select);
?>
<style>
.eNumero{
	text-align:right;
}
</style>
<script src="../js/dojo/dijit/form/FilteringSelect.js" type="text/javascript"></script> 
<script src="../js/dojo/dojo/data/ItemFileReadStore.js" type="text/javascript"></script> 
<script src="../js/dojo/dijit/form/FilteringSelect.js" type="text/javascript"></script> 
<script src="../js/dojo/dijit/TooltipDialog.js" type="text/javascript"></script> 
<script type="text/javascript">
dojo.require("dojo.number");
dojo.require("dijit.form.NumberTextBox");
dojo.require("dijit.form.DateTextBox");
dojo.require("dojo.io.iframe");

function guardar(){
	dojo.byId('eProceso').value=1;
	var bandera = false;
	var bServicios = false;
	var bMercancias = false;
	var eTipoSolicitud = 0;
	mensaje = "¡Verifique lo siguiente!\n";

	var ultimafila=0;

	dojo.query("[id^=\"eRegistro\"]").forEach(function(nodo, index, array){
		ultimafila++;
	});

	if(parseInt(ultimafila)<=1){
		mensaje+="* Maniobras\n";
		bandera = true;		
	}else{
		var bIncompleto=false;
		var bManiobra=false;
		var bToneladas=false;
		var bFecha=false;
		var bTurno=false;
		dojo.query("[id^=\"eRegistro\"]").forEach(function(nodo, index, array){
			var eFilaValida=parseInt(nodo.value);
			if(ultimafila>eFilaValida){
				if(!dojo.byId('eCodBono'+eFilaValida).value){
					bManiobra=true;
					bIncompleto=true;
				}
				if(!dojo.byId("dPeso"+eFilaValida).value){
					bToneladas=true;
					bIncompleto=true;
				}
				if(!dojo.byId("fhFecha"+eFilaValida).value){
					bFecha=true;
					bIncompleto=true;
				}
				if(!dojo.byId("turno"+eFilaValida).value){
					bTurno=true;
					bIncompleto=true;
				}
			}else{
				if(dojo.byId('eCodBono'+eFilaValida).value){
					bManiobra=true;
					bIncompleto=true;
				}
				if(dojo.byId("dPeso"+eFilaValida).value){
					bToneladas=true;
					bIncompleto=true;
				}
				if(dojo.byId("fhFecha"+eFilaValida).value){
					bFecha=true;
					bIncompleto=true;
				}
				if(dojo.byId("turno"+eFilaValida).value){
					bTurno=true;
					bIncompleto=true;
				}
			}
		});
		if(bIncompleto==true){
			mensaje+="* Conceptos incompletos\n";
			if(bManiobra==true){
				mensaje+="   Maniobra\n";
			}
			if(bToneladas==true){
				mensaje+="   Toneladas\n";
			}
			if(bFecha==true){
				mensaje+="   Fecha\n";
			}
			if(bTurno==true){
				mensaje+="   Turno\n";
			}
			bandera = true;
		}
	}

	if (bandera==true){
		alert(mensaje);
	}else{
		if (confirm("¿Desea Guardar la Información?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				if(dojo.byId("eCodigo").value>0){
					alert("¡La información se guardó correctamente!");
					tURL = './?ePagina=2.5.1.5.php';
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
		}
	}
}

function borrarBono(efilaBono){
	var ultimafila=0;
	dojo.query("[id^=\"eRegistro\"]").forEach(function(nodo, index, array){
		ultimafila=parseInt(nodo.id.replace('eRegistro',''));
	});

	if(efilaBono!=ultimafila){
		if(confirm("¿Seguro de eliminar la incapacidad?")){
			dojo.destroy(dojo.byId('filaBono'+efilaBono));
		}
	}
}

function agregarBono(filaBono){ 
	var tb = dojo.byId('tbBonos');
	var tr = null;
	var td = null;
	var fila = 0;
	fila = parseInt(dojo.byId("eFilas").value)+1;
	var rows = document.getElementById('tablaBonos').getElementsByTagName('tbody')[0].getElementsByTagName('tr');
    for (i = 0; i < rows.length; i++) {
		if(rows[i].id == "filaBono"+filaBono){
			tr = tb.insertRow(rows[i].rowIndex);
		}
    }

	tr.id = "filaBono"+fila;
	td = tr.insertCell(0);
	td.height = '23px';
	td.className = "sanLR04";
	td.innerHTML="<img width=\"16\" align=\"absmiddle\" height=\"16\" onclick=\"borrarBono("+fila+");\" id=\"filaConcepto"+fila+"\" src=\"https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-borrar.png\" title=\"Eliminar Concepto\">";	
	td.noWrap = "true";

	td = tr.insertCell(1);
	td.className = "sanLR04";
	td.innerHTML="<input type=\"hidden\" name=\"eRegistro"+fila+"\" id=\"eRegistro"+fila+"\" value=\""+fila+"\"><select name=\"eCodBono"+fila+"\" id=\"eCodBono"+fila+"\" style=\"width:150px\" onchange=\"verBonos("+fila+");\"><option value=\"\">Seleccione...</option><?php while($TipoBono=mysqli_fetch_array($TiposBonos,MYSQLI_ASSOC)){ ?><option value=\"<?=$TipoBono{'eCodBono'}?>\" ><?=($TipoBono{'tNombre'})?></option><?php } ?></select>";
	td.noWrap = "true";
	
	td = tr.insertCell(2);
	td.className = "sanLR04";
	td.innerHTML="<input name=\"dPeso"+fila+"\" type=\"text\" dojoType=\"dijit.form.TextBox\" id=\"dPeso"+fila+"\" value=\"\" style=\"width:140px\" onChange=\"verBonos("+fila+");\">";
	
	td = tr.insertCell(3);
	td.className = "sanLR04";
	td.innerHTML="<input name=\"fhFecha"+fila+"\" type=\"date\" dojoType=\"dijit.form.TextBox\" id=\"fhFecha"+fila+"\" value=\"\" style=\"width:140px\" onChange=\"verBonos("+fila+");\">";
	
	td = tr.insertCell(4);
	td.className = "sanLR04";
	td.innerHTML="<select name=\"turno"+fila+"\" id=\"turno"+fila+"\" style=\"width:175px\" onChange=\"verBonos("+fila+");\"><option value=\"\">Seleccione...</option><option value=\"1\">Turno 1</option><option value=\"2\">Turno 2</option><option value=\"3\">Turno 3</option></select>";

	dojo.byId("eFilas").value++;
	dojo.parser.parse('tablaBonos');
}

function validarEmpleado(fila){
	var ultimafila=0;
	var bDistinto=true;
	dojo.query("[id^=\"idEmpleado\"]").forEach(function(nodo, index, array){
		ultimafila=nodo.id.replace('idEmpleado','');
		if(ultimafila>0){
			if(ultimafila!=fila && dijit.byId(nodo.id).value){
				if(dijit.byId('idEmpleado'+fila).value==dijit.byId(nodo.id).value){
					alert('¡Este empleado ya se encuentra seleccionado!');
					dojo.byId('idEmpleado'+fila).value="";
					dijit.byId('idEmpleado'+fila).value="";
					dojo.byId('idEmpleado'+fila).focus();
					bDistinto=false;
				}
			}
		}
	});
	if(bDistinto==true){
		dojo.byId('eProceso').value=2;
		dojo.byId('eFilaEmp').value=fila;
		dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
			dojo.byId("dvCNS").innerHTML = tRespuesta;
			if(parseInt(dijit.byId("idEmpleado"+fila).value)>0 && dojo.byId("fhFechaInicio"+fila).value && dojo.byId("fhFechaTermino"+fila).value){
				if(parseInt(dojo.byId('fFechaMayoMen').value)>0){
					alert("¡La Fecha Término, no puede ser menor a Fecha Inicio!");
					dojo.byId('fhFechaTermino'+fila).value="";
					dijit.byId('fhFechaTermino'+fila).value="";
					dojo.byId('fhFechaTermino'+fila).focus();
				}else{
					if(parseInt(dojo.byId('eIncapacidad').value)>0){
						alert("¡El empleado cuenta con registro de incapacidad!");
						dojo.byId('idEmpleado'+fila).value="";
						dijit.byId('idEmpleado'+fila).value="";
						dojo.byId('idEmpleado'+fila).focus();
					}else{
						if(parseInt(dojo.byId("eEmpleadoBD").value)>0){
							alert("¡El empleado ya tiene asistencia en el dia!");
							dojo.byId('idEmpleado'+fila).value="";
							dijit.byId('idEmpleado'+fila).value="";
							dojo.byId('idEmpleado'+fila).focus();
						}else{
							validarFila(fila);
						}
					}
				}
			}
		}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});
	}
}

function verBonos(filaBono){
	dojo.byId('eProceso').value=2;
	dojo.byId('eFilaBono').value=filaBono;
	dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
		if(parseInt(tRespuesta)>0){
			alert("Ya existe la combinación");
			dojo.byId('turno'+filaBono).value="";
		}else{
			var ultimafila=0;
			var bDistinto=true;
			dojo.query("[id^=\"eCodBono\"]").forEach(function(nodo, index, array){
				ultimafila=nodo.id.replace('eCodBono','');
				if(filaBono!=ultimafila){
					if(dojo.byId('eCodBono'+ultimafila).value && dojo.byId('fhFecha'+ultimafila).value && dojo.byId('turno'+ultimafila).value){
						if((dojo.byId('eCodBono'+filaBono).value==dojo.byId('eCodBono'+ultimafila).value) && 
							(dojo.byId('fhFecha'+filaBono).value==dojo.byId('fhFecha'+ultimafila).value) && 
							(dojo.byId('turno'+filaBono).value==dojo.byId('turno'+ultimafila).value)){
							bDistinto=false;
						}
					}
				}
			});
			if(bDistinto==false){
				alert("Ya existe la combinación");
				dojo.byId('turno'+filaBono).value="";
			}
			if(filaBono==ultimafila && bDistinto==true){
				var bGenerarLinea= true;
				if(dojo.byId('eCodBono'+filaBono)){
					if(!dojo.byId('eCodBono'+filaBono).value){
						bGenerarLinea=false;
					}
				}
				if(dojo.byId('dPeso'+filaBono)){
					if(!dojo.byId('dPeso'+filaBono).value){
						bGenerarLinea=false;
					}
				}
				if(dojo.byId('fhFecha'+filaBono)){
					if(!dojo.byId('fhFecha'+filaBono).value){
						bGenerarLinea=false;
					}
				}
				if(dojo.byId('turno'+filaBono)){
					if(!dojo.byId('turno'+filaBono).value){
						bGenerarLinea=false;
					}
				}
				if(bGenerarLinea==true){
					
					agregarBono(filaBono);
				}
			}
		}
	}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});
}

function consultar(){
	document.location = './?ePagina=2.5.1.5.php';
}

dojo.addOnLoad(function(){
	dojo.parser.parse('tablaBonos');
});
</script>
<div id="dvCNS" style="display:none;"></div>
<form id="Datos" name="Datos" method="post" onSubmit="return false;" enctype="multipart/form-data">
<input type="hidden" name="eProceso" id="eProceso" value="" />
<input type="hidden" name="eServicio" id="eServicio" value="" />
<input type="hidden" name="eUsuario" id="eUsuario" value="<?=(int)$_SESSION['sesionUsuario']['eCodUsuario'];?>" />
<input type="hidden" name="eCodEntidad" id="eCodEntidad" value="<?=(int)$_SESSION['sesionUsuario']['eCodEntidad'];?>" />
<input type="hidden" name="mail" id="mail" value="<?=$_SESSION['sesionUsuario']['Correo'];?>" />
<input type="hidden" name="nombreusuario" id="nombreusuario" value="<?=$_SESSION['sesionUsuario']['Usuario'];?>" />
<input type="hidden" name="eFilaBono" id="eFilaBono" value="" />
<input type="hidden" name="eFDiaria" id="eFDiaria" value="<?=date('Ymd');?>" />
<div id="dvFacturacion"></div>
<table border="0" cellpadding="0" cellspacing="0" width="980px">
    <tr>
        <td height="23" nowrap class="sanLR04"></td>
        <td width="50%" nowrap class="sanLR04"></td>
        <td nowrap class="sanLR04"></td>
        <td width="50%" nowrap class="sanLR04"></td>
    </tr>    
    <?php mysqli_data_seek($TiposBonos,0);?>
    <tr>
    	<td colspan="4">
            <table cellspacing="0" border="0" width="100%" id="tablaBonos" name="tablaBonos">
                <thead>
                    <tr class="thEncabezado">
                        <td nowrap="nowrap" class="sanLR04" height="23"> </td>
                        <td nowrap="nowrap" class="sanLR04"> Maniobra</td>
                        <td nowrap="nowrap" class="sanLR04"> Toneladas</td>
                        <td nowrap="nowrap" class="sanLR04"> Fecha</td>
                        <td nowrap="nowrap" class="sanLR04"> Turno</td>
                        <td nowrap="nowrap" class="sanLR04" width="100%"></td>
                    </tr>
                </thead>
                <tbody id="tbBonos" name="tbBonos">
                    <tr id="filaBono1" name="filaBono1">
                        <td nowrap="nowrap" class="sanLR04" height="23"><img width="16" align="absmiddle" height="16" onclick="borrarBono(1);" id="filaConcepto1" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-borrar.png" title="Eliminar Concepto"></td>
                        <td nowrap="nowrap" class="sanLR04" height="23"><input type="hidden" name="eRegistro1" id="eRegistro1" value="1">
                        <select id="eCodBono1" name="eCodBono1" style="width:150px" onChange="verBonos(1);">
                        	<option value="">Seleccione...</option>
                            <?php while($TipoBono=mysqli_fetch_array($TiposBonos,MYSQLI_ASSOC)){ ?>
                            	<option value="<?=$TipoBono{'eCodBono'};?>"><?=$TipoBono{'tNombre'};?></option>
                            <?php } ?>
                        </select>
                        </td>
                        <td nowrap="nowrap" class="sanLR04"><input name="dPeso1" type="text" dojoType="dijit.form.TextBox" id="dPeso1" value="" style="width:140px" onChange="verBonos(1);"></td>
                        <td nowrap="nowrap" class="sanLR04"><input name="fhFecha1" type="date" dojoType="dijit.form.TextBox" id="fhFecha1" value="" style="width:140px" onChange="verBonos(1);"></td>
                        <td nowrap="nowrap" class="sanLR04">
                        	<select name="turno1" id="turno1" style="width:175px" onChange="verBonos(1);">
                                <option value="">Seleccione...</option>
                                <option value="1">Turno 1</option>
                                <option value="2">Turno 2</option>
                                <option value="3">Turno 3</option>                
                            </select>
                        </td>
                        <td nowrap="nowrap" class="sanLR04"></td>
                        
                    </tr>
                </tbody>
            </table>
            <input name="eFilas" id="eFilas" value="1" type="hidden">
        </td>
    </tr>
	<tr><td height="20"></td></tr>
    <tr><td valign="top" nowrap="nowrap" class="sanLR04" colspan="4"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle" /> <font class="fntG10K">Indica que es obligatorio llenar el campo.</font></td></tr>
</table>
</form>
<?php } ?>