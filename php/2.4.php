<table cellspacing="0" border="0" width="900px">
	<tr><td width="100%" colspan="2"><hr width="99%" size="0" align="center" color="#CACACA"></td></tr>
	<tr><td height="20" colspan="2"></td></tr>
	<tr>
		<td height="20" width="50%">
			<table cellspacing="0" border="0" width="100%">
				<tbody>
					<tr>
						<td valign="top" width="128" class="sanLR04"><img width="128" height="128" alt="Empresa" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/trabajador.png"></td>
						<td valign="top">
							<table cellspacing="0" border="0" width="100%">
								<tbody>
									<tr><td class="sanLR04"><a href='?ePagina=2.4.1.1.php'><label class="fonN12S">Registro de Asistencias</label></a></td></tr>
									<tr><td class="sanLR04"><a href='?ePagina=2.4.1.6.php'><label class="fonN12S">Registro de Asistencias Checador@s</label></a></td></tr>
									<tr><td align="left" height="2"><hr width="200" size="0" color="#666666" align="left"></td></tr>
                                    <tr><td class="sanLR04"><a href='?ePagina=2.4.1.2.php'><label class="fonN12S">Registro de Ausentismos</label></a></td></tr>
                                    <tr><td class="sanLR04"><a href='?ePagina=2.4.1.3.php'><label class="fonN12S">Registro dia Descanso</label></a></td></tr>
                                    <!--tr><td class="sanLR04"><a href='?ePagina=2.4.1.4.php'><label class="fonN12S">Registro de Maniobras con Bono</label></a></td></tr-->
									<tr><td align="left" height="2"><hr width="200" size="0" color="#666666" align="left"></td></tr>
									<tr><td class="sanLR04"><a href='?ePagina=2.4.1.5.php'><label class="fonN12S">Registro de Vacaciones</label></a></td></tr>
									<!--<tr><td class="sanLR04"><a href='#'><label class="fonN12S">Sincronizaci&oacute;n con Biom&eacute;tricos</label></a></td></tr>-->
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</td>
		<td width="50%">
			<table cellspacing="0" border="0" width="100%">
				<tbody>
					<tr>
						<td valign="top" width="128" class="sanLR04"><img width="128" height="128" alt="Maniobras" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/imss.png"></td>
						<td valign="top">
							<table cellspacing="0" border="0" width="100%">
								<tbody>
									<tr><td class="sanLR04"><a href='?ePagina=2.4.3.4.php'><label class="fonN12S">Reingresos</label></a></td></tr>
                                    <tr><td class="sanLR04"><a href='?ePagina=2.4.3.5.php'><label class="fonN12S">Modificaciones</label></a></td></tr>
                                    <tr><td class="sanLR04"><a href='?ePagina=2.4.3.6.php'><label class="fonN12S">Bajas</label></a></td></tr>
                                    <tr><td align="left" height="2"><hr width="200" size="0" color="#666666" align="left"></td></tr>
                                    <tr><td class="sanLR04"><a href='?ePagina=2.4.3.7.php'><label class="fonN12S">Reingresos Checador@s</label></a></td></tr>
                                    <tr><td class="sanLR04"><a href='?ePagina=2.4.3.8.php'><label class="fonN12S">Bajas Checador@s</label></a></td></tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
	<tr><td height="20" colspan="2"></td></tr>
	<tr>
		<td height="20" width="50%">
			<table cellspacing="0" border="0" width="100%">
				<tbody>
					<tr>
						<td valign="top" width="128" class="sanLR04"><img width="128" height="128" alt="Maniobras" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/incapacidad.png"></td>
						<td valign="top">
							<table cellspacing="0" border="0" width="100%">
								<tbody>
									<tr><td class="sanLR04"><a href='?ePagina=2.4.2.1.php'><label class="fonN12S">Registros de Incapacidades</label></a></td></tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</td>
	<tr><td height="20" colspan="2"></td></tr>
</table>
