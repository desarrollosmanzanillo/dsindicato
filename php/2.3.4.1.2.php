<?php require_once("conexion/soluciones-mysql.php");  
$link = getLink();
	
$select=" SELECT cb.*, ctb.tNombre AS TipoBuque, ce.tNombre AS Estatus ".
		" FROM catbuques cb ".
		" INNER JOIN cattiposbuques ctb ON ctb.eCodTipoBuque=cb.eCodTipoBuque ".
		" INNER JOIN catestatus ce ON ce.tCodEstatus=cb.tCodEstatus ".
		" WHERE eCodBuque=".$_GET['eCodBuque'];
$rBuque = mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC); ?>
<script type="text/javascript">
function consultar(){
	document.location = './?ePagina=2.3.4.1.php';
}
function nuevo(){
	document.location = './?ePagina=2.3.4.1.1.php';
}
</script>
<div id="dvCNS" style="display:none;"></div>
<form action="" method="post" name="Datos" id="Datos" onsubmit="return false;">
<table width="900px" border="0" cellpadding="0" cellspacing="0">
    <tr><td height="20"></td></tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"> C&oacute;digo</td>
	    <td width="50%" nowrap class="sanLR04"><?=sprintf("%07d",$rBuque{'eCodBuque'});?></td>
		<td nowrap class="sanLR04"> Estatus</td>
	    <td width="50%" nowrap class="sanLR04"><?=utf8_encode($rBuque{'Estatus'})?></td>
    </tr>
	<tr>
	    <td nowrap class="sanLR04"> Nombre</td>
	    <td width="50%" nowrap class="sanLR04"><?=utf8_encode($rBuque{'tNombre'})?></td>
		<td height="23" nowrap class="sanLR04"> Tipo de Buque</td>
	    <td width="50%" nowrap class="sanLR04"><?=utf8_encode($rBuque{'TipoBuque'});?></td>
    </tr>
	<tr>
	    <td nowrap class="sanLR04"> Indicativo de Llamada</td>
	    <td width="50%" nowrap class="sanLR04"><?=utf8_encode($rBuque{'tIndicativoLlamada'})?></td>
		<td height="23" nowrap class="sanLR04"> IMO</td>
	    <td width="50%" nowrap class="sanLR04"><?=utf8_encode($rBuque{'tIMO'});?></td>
    </tr>
</table>
</form>