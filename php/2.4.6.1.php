﻿<?php require_once("conexion/soluciones-mysql.php");
require_once("conexion/facturacion.php");
$link = getLink();
$facturacion = new cFacturacion();
if($_POST){
	if($_POST['eProceso']==1){
		$res=$facturacion->facturar();
		print_r($res);
		print "<input type=\"text\" value=\"".($res['exito']>0 ? $res['codigo'] : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}

	if($_POST['eProceso']==2){ 
		$select=" SELECT oss.eCodSolicitud, ces.tNombre AS Estatus, oss.fhFecha, oss.fhFechaServicio, oss.tReferencia, ".
				" cen.tNombre AS Naviera, oss.tPatente, cec.tNombre AS Cliente, cef.tNombre AS FacturarA, oss.tCodEstatus, ".
				" cbu.tNombre AS Buque, oss.tNumeroViaje, cts.tNombre AS TipoSolicitud, cto.tNombre AS TipoServicio, ".
				" oss.eCodTipoServicio, oss.eCodTipoSolicitud, oss.tBL, oss.tMotivoCancelacion, oss.eCodMetodoPago ".
				" FROM opesolicitudesservicios oss ".
				" INNER JOIN catestatus ces ON ces.tCodEstatus=oss.tCodEstatus ".
				" INNER JOIN catentidades cen ON cen.eCodEntidad=oss.eCodNaviera ".
				" INNER JOIN catentidades cec ON cec.eCodEntidad=oss.eCodCliente ".
				" INNER JOIN catentidades cef ON cef.eCodEntidad=oss.eCodFacturarA ".
				" INNER JOIN catbuques cbu ON cbu.eCodBuque=oss.eCodBuque ".
				" INNER JOIN cattipossolicitudes cts ON cts.eCodTipoSolicitud=oss.eCodTipoSolicitud ".
				" INNER JOIN cattiposservicios cto ON cto.eCodTipoServicio=oss.eCodTipoServicio ".
				" WHERE oss.eCodSolicitud=".(int)$_POST['eSolicitud'];
		$rSolicitud=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
		$select=" SELECT * ".
				" FROM relcfdssolicitudesservicios ".
				" WHERE eCodSolicitud=".(int)$_POST['eSolicitud'];
		$rFactura=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC); ?>
        <input type="hidden" name="eFactura" id="eFactura" value="<?=(int)$rFactura{'eCodCFD'};?>"/>
        <input type="hidden" name="tCodEstatus" id="tCodEstatus" value="<?=$rSolicitud{'tCodEstatus'};?>"/>
		<table border="0" cellpadding="0" cellspacing="0" width="980px">
        	<tr>
                <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle" /> Solicitud de Servicio</td>
                <td width="50%" nowrap class="sanLR04"><input name="eSolicitud" type="text" dojoType="dijit.form.TextBox" id="eSolicitud" style="width:80px" value="<?=((int)$_POST['eSolicitud']>0 ? (int)$_POST['eSolicitud'] : "");?>" onchange="servicios();"></td>
                <td nowrap class="sanLR04"></td>
                <td width="50%" nowrap class="sanLR04"></td>
            </tr>
            <?php if($rSolicitud{'eCodSolicitud'}>0 && trim($rSolicitud{'tCodEstatus'})=='AU' && (int)$rFactura{'eCodCFD'}==0){ 
				$select=" SELECT rs.eCantidad, rs.dImporte, rs.dSubtotal, cs.tNombre AS Servicio ".
						" FROM relsolicitudesserviciosservicios rs ".
						" INNER JOIN catservicios cs ON cs.eCodServicio=rs.eCodServicio ".
						" WHERE rs.eCodSolicitud=".(int)$rSolicitud['eCodSolicitud'];
				$rsServicios=mysqli_query($link,$select);
				$select=" SELECT rsi.dValorImpuesto, rsi.dImpuesto, ci.tNombre AS Impuesto ".
						" FROM relsolicitudesserviciosimpuestos rsi ".
						" INNER JOIN catimpuestos ci ON ci.eCodImpuesto=rsi.eCodImpuesto ".
						" WHERE rsi.eCodSolicitud=".(int)$rSolicitud['eCodSolicitud'];
				$rsImpuestos=mysqli_query($link,$select);
				if((int)$rSolicitud{'eCodTipoSolicitud'}==1){
					$select=" SELECT ".((int)$rSolicitud{'eCodTipoServicio'}==1 ? "rs.tCodContenedor" : "")." AS tMercancia, ".
							((int)$rSolicitud{'eCodTipoServicio'}==1 ? "ctc.tNombreCorto" : "")." AS TipoEmbalaje, rs.tObservaciones ".
							" FROM relsolicitudesserviciosmercancias rs ".
							" LEFT JOIN cattiposcontenedores ctc ON ctc.eCodTipoContenedor=rs.eCodTipoContenedor ".
							" WHERE rs.eCodSolicitud=".(int)$rSolicitud['eCodSolicitud'];
				}else{
					$select=" SELECT ".((int)$rSolicitud{'eCodTipoServicio'}==1 ? "rs.tCodContenedor" : "")." AS tMercancia, ".
							((int)$rSolicitud{'eCodTipoServicio'}==1 ? "ctc.tNombreCorto" : "")." AS TipoEmbalaje, rs.tObservaciones ".
							" FROM opeentradasmercancias rs ".
							" INNER JOIN relsolicitudesserviciosmercanciassalidas rse ON rse.eCodEntrada=rs.eCodEntrada ".
							" LEFT JOIN cattiposcontenedores ctc ON ctc.eCodTipoContenedor=rs.eCodTipoContenedor ".
							" WHERE rse.eCodSolicitud=".(int)$rSolicitud['eCodSolicitud'];
				}
				$rsMercancias=mysqli_query($link,$select);
				$select=" SELECT rs.*, ct.tNombre AS Archivo ".
						" FROM relsolicitudesserviciosmercanciasarchivos rs ".
						" LEFT JOIN cattiposarchivos ct ON ct.eCodTipoArchivo=rs.eCodTipoArchivo ".
						" WHERE rs.eCodSolicitud=".(int)$rSolicitud['eCodSolicitud'];
				$rsArchivos=mysqli_query($link,$select); 
				$select=" SELECT * ".
						" FROM catformaspago ".
						" WHERE tCodEstatus='AC' ".
						" ORDER BY tNombre ";
				$rsFormasPagos=mysqli_query($link,$select);
				$select=" SELECT * ".
						" FROM catmetodospago ".
						" WHERE tCodEstatus='AC' ".
						" ORDER BY tNombre ";
				$rsMetodosPagos=mysqli_query($link,$select); ?>
                <input type="hidden" name="eCodSolicitud" id="eCodSolicitud" value="<?=(int)$rSolicitud['eCodSolicitud'];?>"/>
                <tr><td height="20"></td></tr>    
                <tr>
                    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> C&oacute;digo</td>
                    <td width="50%" nowrap class="sanLR04"><?=sprintf("%07d",$rSolicitud{'eCodSolicitud'});?></td>
                    <td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Estatus</td>
                    <td width="50%" nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'Estatus'})?></td>
                </tr>
                <tr>
                    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Fecha de Solicitud</td>
                    <td nowrap class="sanLR04"><?=date("d/m/Y H:i", strtotime($rSolicitud{'fhFecha'}));?></td>
                    <td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Fecha de Servicio</td>
                    <td nowrap class="sanLR04"><?=date("d/m/Y", strtotime($rSolicitud{'fhFechaServicio'}));?></td>
                </tr>
                <tr>
                    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Naviera</td>
                    <td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'Naviera'})?></td>
                    <td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Patente</td>
                    <td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'tPatente'} ? $rSolicitud{'tPatente'} : "N/D")?></td>
                </tr>
                <tr>
                    <td height="23" nowrap class="sanLR04" valign="top"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Cliente</td>
                    <td class="sanLR04"><?=utf8_encode($rSolicitud{'Cliente'})?></td>
                    <td nowrap class="sanLR04" valign="top"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Facturar a</td>
                    <td class="sanLR04"><?=utf8_encode($rSolicitud{'FacturarA'})?></td>
                </tr>
                <tr>
                    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> BL</td>
                    <td nowrap class="sanLR04"><b><?=utf8_encode($rSolicitud{'tBL'})?></b></td>
                    <td nowrap class="sanLR04"></td>
                    <td nowrap class="sanLR04"></td>
                </tr>
                <tr>
                    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Buque</td>
                    <td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'Buque'})?></td>
                    <td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Viaje</td>
                    <td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'tNumeroViaje'})?></td>
                </tr>
                <tr>
                    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Tipo de Solicitud</td>
                    <td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'TipoSolicitud'})?></td>
                    <td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Tipo de Servicio</td>
                    <td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'TipoServicio'})?></td>
                </tr>
                <tr>
                    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" width="16" height="16" align="absmiddle"> Forma de Pago</td>
                    <td nowrap class="sanLR04">
						<select name="eCodFormaPago" id="eCodFormaPago" style="width:175px">
                            <option value="">Seleccione...</option>
                            <?php while($rFormaPago=mysqli_fetch_array($rsFormasPagos,MYSQLI_ASSOC)){ ?>
                                <option value="<?=$rFormaPago{'eCodFormaPago'};?>" <?=($rFormaPago{'eCodFormaPago'}==1 ? "selected" : "");?>><?=($rFormaPago{'tNombre'})?></option>
                            <?php } ?>
                        </select>
                    </td>
                    <td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" width="16" height="16" align="absmiddle"> M&eacute;todo de Pago</td>
                    <td nowrap class="sanLR04">
						<select name="eCodMetodoPago" id="eCodMetodoPago" style="width:175px">
                            <option value="">Seleccione...</option>
                            <?php while($rMetodoPago=mysqli_fetch_array($rsMetodosPagos,MYSQLI_ASSOC)){ ?>
                                <option value="<?=$rMetodoPago{'eCodMetodoPago'}?>" <?=($rMetodoPago{'eCodMetodoPago'}==$rSolicitud{'eCodMetodoPago'} ? "selected" : "");?>><?=($rMetodoPago{'tNombre'})?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Referencia</td>
                    <td nowrap class="sanLR04"> 
                        <input name="tReferencia" type="text" dojoType="dijit.form.TextBox" id="tReferencia" value="<?=$rSolicitud{'tReferencia'};?>" style="width:175px">
                    </td>
                </tr>
                <tr>
                    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/nobligatorio.png" width="16" height="0" align="absmiddle"> Condiciones de Pago</td>
                    <td nowrap class="sanLR04" colspan="3">
                    	<input name="tCondicionesPago" type="text" dojoType="dijit.form.TextBox" id="tCondicionesPago" style="width:655px">
                    </td>
                </tr>
                <tr>
                    <td height="23" nowrap class="sanLR04" colspan="4"> 
                        <table cellspacing="0" border="0" width="965px">
                            <thead>
                                <tr class="thEncabezado">
                                    <td nowrap="nowrap" class="sanLR04" height="23">Cantidad</td>
                                    <td nowrap="nowrap" class="sanLR04" width="100%">Descripci&oacute;n</td>
                                    <td nowrap="nowrap" class="sanLR04">Precio Unitario</td>
                                    <td nowrap="nowrap" class="sanLR04" align="right">Total</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $dSubtotal=0;
                                $dTotal=0;
                                while($rServicio=mysqli_fetch_array($rsServicios,MYSQLI_ASSOC)){ ?>
                                    <tr>
                                        <td nowrap="nowrap" class="sanLR04" align="right"><?=sprintf("%02d",$rServicio{'eCantidad'});?></td>
                                        <td nowrap="nowrap" class="sanLR04" width="100%"><?=utf8_encode($rServicio{'Servicio'});?></a></td>
                                        <td nowrap="nowrap" class="sanLR04" align="right"><?=number_format($rServicio{'dImporte'},2);?></td>
                                        <td nowrap="nowrap" class="sanLR04" align="right"><?=number_format($rServicio{'dSubtotal'},2);?></td>
                                    </tr>
                                    <?php $dSubtotal=(float)$dSubtotal+(float)$rServicio{'dSubtotal'};
                                } 
                                $dTotal=$dSubtotal; ?>
                                <tr>
                                    <td nowrap="nowrap" class="sanLR04" height="20" colspan="3" align="right"><b>Subtotal</b></td>
                                    <td nowrap="nowrap" class="sanLR04" align="right"><?=number_format($dSubtotal, 2);?></td>
                                </tr>
                                <?php while($rImpuesto=mysqli_fetch_array($rsImpuestos,MYSQLI_ASSOC)){ ?>
                                <tr>
                                    <td nowrap="nowrap" class="sanLR04" height="20" colspan="3" align="right">
                                    <b><?=$rImpuesto{'Impuesto'}." ".number_format($rImpuesto{'dValorImpuesto'},2);?>%</b>
                                    </td>
                                    <td nowrap="nowrap" class="sanLR04" align="right"><?=number_format($rImpuesto{'dImpuesto'}, 2);?></td>
                                </tr>
                                <?php $dTotal=(float)$dTotal+(float)$rImpuesto{'dImpuesto'}; } ?>
                                <tr>
                                    <td nowrap="nowrap" class="sanLR04" height="20" colspan="3" align="right"><b>Total</b></td>
                                    <td nowrap="nowrap" class="sanLR04" align="right"><?=number_format($dTotal, 2);?></td>
                                </tr>
                                <tr><td nowrap="nowrap" class="sanLR04" height="20" colspan="5"></td></tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <table cellspacing="0" border="0" width="965px" id="tablaMercancias" name="tablaMercancias">
                            <thead>
                                <tr class="thEncabezado">
                                    <td nowrap="nowrap" class="sanLR04" height="23"> <?=((int)$rSolicitud{'eCodTipoServicio'}==1 ? "Contenedor" : "Mercanc&iacute;a");?></td>
                                    <td nowrap="nowrap" class="sanLR04"> <?=((int)$rSolicitud{'eCodTipoServicio'}==1 ? "Tipo" : "Embalaje");?></td>
                                    <td nowrap="nowrap" class="sanLR04"> Observaciones</td>
                                    <td nowrap="nowrap" class="sanLR04" width="100%"></td>
                                </tr>
                            </thead>
                            <tbody id="tbMercancias" name="tbMercancias">
                                <?php while($rMercancia=mysqli_fetch_array($rsMercancias,MYSQLI_ASSOC)){ ?>
                                    <tr>
                                        <td nowrap="nowrap" class="sanLR04" height="23"><?=utf8_encode($rMercancia{'tMercancia'});?></td>
                                        <td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rMercancia{'TipoEmbalaje'});?></td>
                                        <td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rMercancia{'tObservaciones'});?></td>
                                        <td nowrap="nowrap" class="sanLR04"></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <?php if((int)mysqli_num_rows($rsArchivos)>0){ ?>
                    <tr><td height="23"></td></tr>
                    <?php $eArchivo=1;
                    while($rArchivo=mysqli_fetch_array($rsArchivos,MYSQLI_ASSOC)){ ?>
                        <tr>
                            <td height="23" nowrap class="sanLR04"><?=($eArchivo==1 ?  "Archivos" : "");?></td>
                            <td nowrap class="sanLR04" colspan="3"><a href="Patio/<?=$rArchivo{'tURL'};?>" target="_blank" class="sanT12" ><?=$rArchivo{'Archivo'};?></a></td>
                        </tr>
                        <?php $eArchivo++;
                    }
                } ?>
            </table>
		<?php }
	}
	
//	
}
if(!$_POST){ ?>
<script type="text/javascript">
dojo.require("dojo.number");
dojo.require("dijit.form.NumberTextBox");
dojo.require("dijit.form.DateTextBox");
dojo.require("dojo.io.iframe");

function guardar(){
	dojo.byId('eProceso').value=1;
	var bandera = false;
	var bServicios = false;
	var bMercancias = false;
	var eTipoSolicitud = 0;
	mensaje = "¡Verifique lo siguiente!\n";

	if (!dojo.byId("eCodSolicitud")){
		mensaje+="* Solicitud de Servicio\n";
		bandera = true;		
	}else{
		if (!dojo.byId("eCodFormaPago").value){
			mensaje+="* Forma de Pago\n";
			bandera = true;		
		}
		if (!dojo.byId("eCodMetodoPago").value){
			mensaje+="* Método de Pago\n";
			bandera = true;		
		}
	}
	if (bandera==true){
		alert(mensaje);
	}else{
		if (confirm("¿Desea Guardar la Información?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				if(dojo.byId("eCodigo").value>0){
					alert("¡La información se guardó correctamente!");
					tURL = './?ePagina=2.5.6.1.1.php&eCodCFD='+dojo.byId("eCodigo").value;
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
		}
	}
}	

function servicios(){
	dojo.byId('eProceso').value=2;
	dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
		dojo.forEach(dijit.findWidgets(dojo.byId('dvFacturacion')), function(nodo) {nodo.destroyRecursive();});
		dojo.byId("dvFacturacion").innerHTML = tRespuesta;
		dojo.parser.parse(dojo.byId("dvFacturacion").id);
		if(dojo.byId('eSolicitud').value>0){
			if(dojo.byId('tCodEstatus').value=='AU'){
				if(dojo.byId('eFactura').value>0){
					alert('¡Solicitud Facturada!');
				}
			}else{
				alert('¡Solicitud NO Autorizada!');
			}
		}
	}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});
}

function consultar(){
	document.location = './?ePagina=2.5.3.1.php';
}

dojo.addOnLoad(function(){servicios();});
</script>
<div id="dvCNS" style="display:none;"></div>
<form id="Datos" name="Datos" method="post" onSubmit="return false;" enctype="multipart/form-data">
<input type="hidden" name="eProceso" id="eProceso" value="" />
<input type="hidden" name="eUsuario" id="eUsuario" value="<?=(int)$_SESSION['sesionUsuario']['eCodUsuario'];?>" />
<input type="hidden" name="eCodEntidad" id="eCodEntidad" value="<?=(int)$_SESSION['sesionUsuario']['eCodEntidad'];?>" />
<input type="hidden" name="mail" id="mail" value="<?=$_SESSION['sesionUsuario']['Correo'];?>" />
<input type="hidden" name="nombreusuario" id="nombreusuario" value="<?=$_SESSION['sesionUsuario']['Usuario'];?>" />
<div id="dvFacturacion"></div>
<table border="0" cellpadding="0" cellspacing="0" width="980px">
	<tr><td height="20"></td></tr>
    <tr><td valign="top" nowrap="nowrap" class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle" /> <font class="fntG10K">Indica que es obligatorio llenar el campo.</font></td></tr>
</table>
</form>
<?php } ?>