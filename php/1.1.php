<script language="javascript">
function validar(){
	var mensaje="Favor de Revisar lo siguiente: \n";
	var bError=false;
	//obteniendo el valor que se puso en el campo text del formulario
	var miCampoTexto = document.getElementById("usuario").value;	 
	if (miCampoTexto.length == 0 || /^\s+$/.test(miCampoTexto)){
		mensaje +=" * Usuario \n";
		bError=true;
	}
	//obteniendo el valor que se puso en el campo text del formulario
	var miCampoTexto = document.getElementById("password").value;	 
	if (miCampoTexto.length == 0 || /^\s+$/.test(miCampoTexto)){
		mensaje +=" * Contraseña \n";
		bError=true;		
	}
	
	if(bError==true){
		alert(mensaje);
		return false;
	}else{
		return true;
	}
}
</script>
<div id="intro">
	<div class="pikachoose" align="center">
			<img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/logosistema.png" width="450" height="280" alt="picture"/></a>
	</div>
</div>
<div class="holder_content">    
	<section class="group6">
        <p><b>Bienvenid@ a su plataforma de Servicios en Linea &quot;Sindicato V3.0 &copy;&quot;</b><br/>
        </p>
      <p>Este sistema es de <span class="style1">uso exclusivo para usuarios registrados</span>. Si tiene alguna duda sobre el uso o acceso del sistema en l&iacute;nea, por favor p&oacute;ngase en contacto con <a href="mailto: mzomicrosystems@outlook.com" class="style2">nosotros:</a></p>
  </section>
  <form method="post" action="?ePagina=1.6.php" onsubmit="return validar()">
	<section class="group3">
		<table>
			<tr>
				<td nowrap="nowrap"><b>Correo Electr&oacute;nico</b></td>
				<td><input type="text" dojoType="dijit.form.TextBox" name="usuario" id="usuario" placeholder="usuario@dominio"></td>
			</tr>
			<tr>
				<td nowrap="nowrap"><b>Contrase&ntilde;a</b></td>
				<td><input type="password" dojoType="dijit.form.TextBox" name="password" id="password" placeholder="Contrase&ntilde;a"></td>
			</tr>
			<tr>
				<td></td>
				<td><button><b>Iniciar Sesi&oacute;n</b></button></td>
			</tr>
		</table>
	</section>
	</form>
</div>