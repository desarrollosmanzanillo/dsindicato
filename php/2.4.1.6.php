<?php require_once("conexion/soluciones-mysql.php");
$link = getLink();
if($_POST){
	if($_POST['eProceso']==1){
		$exito				= 1;
		$asistencias		= 0;
		$idEmpresa			= ($_POST['eCodEmisor']			? $_POST['eCodEmisor']					: "NULL");
		$idUsuario			= ($_POST['eUsuario']			? $_POST['eUsuario']					: "NULL");
		$fhFechaAsistencia	= ($_POST['fhFechaAsistencia']	? "'".$_POST['fhFechaAsistencia']."'"	: "NULL");

		$select=" SELECT * ".
				" FROM sisconfiguracion ";
		$configura=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
		$fechaasis= date('Y-m-d',strtotime('+1 days', strtotime($_POST['fhFechaAsistencia'])));
		$domingo=date("w",strtotime($fechaasis));

		$select=" SELECT * ".
				" FROM catentidades ".
				" WHERE bPrincipal=1 ";
		$rEmisor=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

		$select=" SELECT eCodDia ".
				" FROM catdiasfestivos ".
				" WHERE ecodEstatus='AC' AND fhFecha=".$fhFechaAsistencia;
		$rFestivo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

		if((int)$_POST['eAccion']==100){
			foreach($_POST as $k => $v){
				$nombre = strval($k);
				if(strstr($nombre,"eRegistro") && $v && (int)$_POST['idEmpleado'.$v]>0){
					$eFila=str_replace("eRegistro", "", $nombre);
					$area 			= ($_POST['eCodCosto'.$eFila]	? $_POST['eCodCosto'.$eFila]	: "NULL");
					$idEmpleado 	= ($_POST['idEmpleado'.$eFila]	? $_POST['idEmpleado'.$eFila]	: "NULL");
					
					if((int)$idEmpleado>0){
						$insert=" INSERT INTO asistenciaschecadoras (idEmpleado, idCategorias, idEmpresa, area, Salario, Fecha, ".
								" fechaRegistro, idUsuario, Estado, eCodCosto, Turno) ".
								" VALUES (".$idEmpleado.", 4, ".$idEmpresa.", ".$area.", 0,
								".$fhFechaAsistencia.", CURRENT_TIMESTAMP, ".$idUsuario.", 'AC', ".$area.", 1) ";
						if($res=mysqli_query($link,$insert)){
							$cadena.=$insert;
							$asistencias++;
						}else{
							$cadena.=$insert;
							$exito=0;
						}
					}
				}
			}
		}else{
			if((int)$_POST['eAccion']==0){
				foreach($_POST as $k => $v){
					$nombre = strval($k);
					if(strstr($nombre,"eRegistro") && $v && (int)$_POST['idEmpleado'.$v]>0){
						$eFila=str_replace("eRegistro", "", $nombre);
						$area 			= ($_POST['eCodCosto'.$eFila]	? $_POST['eCodCosto'.$eFila]	: "NULL");
						$idEmpleado 	= ($_POST['idEmpleado'.$eFila]	? $_POST['idEmpleado'.$eFila]	: "NULL");
						
						if((int)$idEmpleado>0){
							$insert=" INSERT INTO asistenciaschecadoras (idEmpleado, idCategorias, idEmpresa, area, Salario, Fecha, ".
									" fechaRegistro, idUsuario, Estado, eCodCosto, Turno) ".
									" VALUES (".$idEmpleado.", 4, ".$idEmpresa.", ".$area.", 0,
									".$fhFechaAsistencia.", CURRENT_TIMESTAMP, ".$idUsuario.", 'AC', ".$area.", 1) ";
							print $insert;
							if($res=mysqli_query($link,$insert)){
								$cadena.=$insert;
								$asistencias++;
							}else{
								$cadena.=$insert;
								$exito=0;
							}
						}
					}
				}
			}
		}
		print "<input type=\"hidden\" value=\"".((int)$exito>0 && (int)$asistencias>0 && (int)$_POST['eAccion']==0 ? 1 : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}

	if($_POST['eProceso']==2){
		$select=" SELECT id ".
				" FROM asistenciaschecadoras ".
				" WHERE Estado LIKE 'AC' AND Fecha='".$_POST['fhFechaAsistencia']."' 
				AND idEmpleado=".(int)$_POST['idEmpleado'.(int)$_POST['eFilaEmp']];
		print $select;
		$asistencia=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
		$select=" SELECT id ".
				" FROM incapacidades ".
				" WHERE Estado LIKE 'AC' AND idEmpleado=".(int)$_POST['idEmpleado'.(int)$_POST['eFilaEmp']].
				" AND FechaInicial<='".$_POST['fhFechaAsistencia']."' AND FechaFinal>='".$_POST['fhFechaAsistencia']."'";
		$incapacidad=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
		print "<input type=\"hidden\" name=\"eEmpleadoBD\" id=\"eEmpleadoBD\" value=\"".((int)$_POST['eAccion']==0 ? (int)$asistencia{'id'} : "")."\">";
//              print "<input type=\"hidden\" name=\"eIncapacidad\" id=\"eIncapacidad\" value=\"".((int)$_POST['eAccion']==0 ? (int)$incapacidad{'id'} : "")."\">";
                print "<input type=\"hidden\" name=\"eIncapacidad\" id=\"eIncapacidad\" value=\"\">";
	}
//
}
if(!$_POST){
//
$select=" SELECT * ".
		" FROM catentidades ".
		" WHERE bPrincipal=1 ";
$rsEmisores=mysqli_query($link,$select);

$select=" SELECT * ".
		" FROM catcomprobantes WHERE tCodEstatus='AC' ".
		" ORDER BY tNombre ASC ";
$rsComprobantes=mysqli_query($link,$select);

$select=" SELECT * ".
		" FROM catformaspago ".
		" WHERE tCodEstatus='AC' ".
		" ORDER BY tNombre ";
$rsFormasPagos=mysqli_query($link,$select);

$select=" SELECT * ".
		" FROM catmetodospago ".
		" WHERE tCodEstatus='AC' ".
		" ORDER BY tNombre ";
$rsMetodosPagos=mysqli_query($link,$select);

$select=" SELECT * ".
		" FROM catentidades ".
		" WHERE tCodEstatus='AC' ".
		" AND eCodTipoEntidad=1 ".
		" ORDER BY tNombre ";
$rsClientes=mysqli_query($link,$select);

$select=" SELECT *
		FROM catcostoschecadoras
		WHERE tCodEstatus='AC'
		ORDER BY tArea ";
$rsCostos=mysqli_query($link,$select); ?>
<style>
.eNumero{
	text-align:right;
}
</style>
<script src="../js/dojo/dijit/form/FilteringSelect.js" type="text/javascript"></script>
<script src="../js/dojo/dojo/data/ItemFileReadStore.js" type="text/javascript"></script>
<script src="../js/dojo/dijit/form/FilteringSelect.js" type="text/javascript"></script>
<script src="../js/dojo/dijit/TooltipDialog.js" type="text/javascript"></script>
<script type="text/javascript">
dojo.require("dojo.number");
dojo.require("dijit.form.NumberTextBox");
dojo.require("dijit.form.DateTextBox");
dojo.require("dojo.io.iframe");

function guardar(){
	dojo.byId('eProceso').value=1;
	var bandera = false;
	var bServicios = false;
	var bMercancias = false;
	var eTipoSolicitud = 0;
	mensaje = "¡Verifique lo siguiente!\n";

	var ultimafila=0;
	var ultimafilaV=0;

	dojo.query("[id^=\"eRegistro\"]").forEach(function(nodo, index, array){
		ultimafila=parseInt(nodo.value);
		ultimafilaV++;
	});
	if (!dojo.byId("eCodEmisor").value){
		mensaje+="* Registro Patronal\n";
		bandera = true;
	}
	if (!dojo.byId("fhFechaAsistencia").value){
		mensaje+="* Fecha de Asistencia\n";
		bandera = true;
	}else{
		if(parseFloat(dojo.byId("fhFechaAsistencia").value.replace(/-/gi, ""))>parseFloat(dojo.byId('eFDiaria').value)){
			mensaje+="* No Puede Guardar Asistencias Anticipadas\n";
			bandera = true;
		}
	}
	if(parseInt(ultimafilaV)<=1){
		mensaje+="* Asistencias\n";
		bandera = true;
	}else{
		var bIncompleto=false;
		var bEmpleado=false;
		var bCategoria=false;
		var bCosto=false;
		dojo.query("[id^=\"eRegistro\"]").forEach(function(nodo, index, array){
			var eFilaValida=parseInt(nodo.value);
			if(ultimafila>eFilaValida){
				if(!dijit.byId('idEmpleado'+eFilaValida).value){
					bEmpleado=true;
					bIncompleto=true;
				}
				if(!dojo.byId('eCodCosto'+eFilaValida).value){
					bCosto=true;
					bIncompleto=true;
				}
			}else{
				if(dijit.byId('idEmpleado'+eFilaValida).value){
					bEmpleado=true;
					bIncompleto=true;
				}
				if(dojo.byId('eCodCosto'+eFilaValida).value){
					bCosto=true;
					bIncompleto=true;
				}
			}
		});
		if(bIncompleto==true){
			mensaje+="* Conceptos incompletos\n";
			if(bEmpleado==true){
				mensaje+="   Empleado\n";
			}
			if(bCosto==true){
				mensaje+="   Centro de Costo\n";
			}
			bandera = true;
		}
	}

	if (bandera==true){
		alert(mensaje);
	}else{
		if (confirm("¿Desea Guardar la Información?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				if(dojo.byId("eCodigo").value>0){
					alert("¡La información se guardó correctamente!");
					tURL = './?ePagina=2.5.1.6.php';
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});
		}
	}
}

function borrarAsistencia(eFilaAsistencia){
	var ultimafila=0;
	dojo.query("[id^=\"eRegistro\"]").forEach(function(nodo, index, array){
		ultimafila=parseInt(nodo.id.replace('eRegistro',''));
	});

	if(eFilaAsistencia!=ultimafila){
		if(confirm("¿Seguro de eliminar la asistencia?")){
			dojo.destroy(dojo.byId('filaAsistencia'+eFilaAsistencia));
		}
	}
}

function verificarEmpleados(){
	dojo.query("[id^=\"idEmpleado\"]").forEach(function(nodo, index, array){
		var filaEmpleado=nodo.id.replace('idEmpleado','');
		if(parseInt(filaEmpleado)>0){
			validarEmpleado(filaEmpleado);
		}
	});
}

function validarEmpleado(fila){
	dojo.byId('eProceso').value=2;
	dojo.byId('eFilaEmp').value=fila;
	dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
		dojo.byId("dvCNS").innerHTML = tRespuesta;
		if(parseInt(dijit.byId('idEmpleado'+fila).value)>0){
			if(parseInt(dojo.byId('eIncapacidad').value)>0){
				alert("¡El empleado cuenta con registro de incapacidad!");
				dojo.byId('idEmpleado'+fila).value="";
				dijit.byId('idEmpleado'+fila).value="";
				dojo.byId('idEmpleado'+fila).focus();
			}else{
				if(parseInt(dojo.byId("eEmpleadoBD").value)>0){
					alert("¡El empleado ya tiene asistencia en el turno!");
					dojo.byId('idEmpleado'+fila).value="";
					dijit.byId('idEmpleado'+fila).value="";
					dojo.byId('idEmpleado'+fila).focus();
				}else{
					validarFila(fila);
				}
			}
		}
	}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});
}

function agregarConcepto(filaAsistencia){
	var tb = dojo.byId('tbAsistencias');
	var tr = null;
	var td = null;
	var fila = 0;
	fila = parseInt(dojo.byId("eFilas").value)+1;
	var rows = document.getElementById('tablaAsistencias').getElementsByTagName('tbody')[0].getElementsByTagName('tr');
    for (i = 0; i < rows.length; i++) {
		if(rows[i].id == "filaAsistencia"+filaAsistencia){
			tr = tb.insertRow(rows[i].rowIndex);
		}
    }

	tr.id = "filaAsistencia"+fila;
	td = tr.insertCell(0);
	td.height = '23px';
	td.className = "sanLR04";
	td.innerHTML="<img width=\"16\" align=\"absmiddle\" height=\"16\" onclick=\"borrarAsistencia("+fila+");\" id=\"filaConcepto"+fila+"\" src=\"https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-borrar.png\" title=\"Eliminar Concepto\">";
	td.noWrap = "true";

	td = tr.insertCell(1);
	td.className = "sanLR04";
	td.innerHTML='<input type="hidden" name="eRegistro'+fila+'" id="eRegistro'+fila+'" value="'+fila+'"><div dojoType="dojo.data.ItemFileReadStore" jsId="acEmpledos'+fila+'" url="php/auto/catchecadoras.php" clearOnClose="true" urlPreventCache="true"></div><input dojoType="dijit.form.FilteringSelect" value="" jsId="cmbEmpleados'+fila+'" store="acEmpledos'+fila+'" searchAttr="nombre" hasDownArrow="false" name="idEmpleado'+fila+'" id="idEmpleado'+fila+'" autoComplete="false" searchDelay="300" highlightMatch="none" placeHolder="Empleados" style="width:380px;" displayMessage="false" required="false" queryExpr = "*" pageSize="10" onChange="validarEmpleado('+fila+');">';
	td.noWrap = "true";

	td = tr.insertCell(2);
	td.className = "sanLR04";
	td.innerHTML="Checador@";
	td.noWrap = "true";

	td = tr.insertCell(3);
	td.className = "sanLR04";
	td.innerHTML="";
	td.innerHTML="<select name=\"eCodCosto"+fila+"\" id=\"eCodCosto"+fila+"\" style=\"width:250px\" onchange=\"validarFila("+fila+");\"><option value=\"\">Seleccione...</option><?php while($rCosto=mysqli_fetch_array($rsCostos,MYSQLI_ASSOC)){ ?><option value=\"<?=$rCosto{'eCodCosto'}?>\" ><?=("(".$rCosto{'tSiglas'}.") ".$rCosto{'tArea'})?></option><?php } ?></select>";
	td.noWrap = "true";

	dojo.byId("eFilas").value++;
	dojo.parser.parse('tablaAsistencias');
	asignaEmpleados();
}

function validarFila(filaEmpleado){
	var ultimafila=0;
	var bDistinto=true;
	dojo.query("[id^=\"idEmpleado\"]").forEach(function(nodo, index, array){
		ultimafila=nodo.id.replace('idEmpleado','');
		if(ultimafila>0){
			if(ultimafila!=filaEmpleado && dijit.byId(nodo.id).value){
				if(dijit.byId('idEmpleado'+filaEmpleado).value==dijit.byId(nodo.id).value){
					alert('¡Este empleado ya se encuentra seleccionado!');
					dojo.byId('idEmpleado'+filaEmpleado).value="";
					dijit.byId('idEmpleado'+filaEmpleado).value="";
					dojo.byId('idEmpleado'+filaEmpleado).focus();
					bDistinto=false;
				}
			}
		}
	});

	if(bDistinto==true){
		var ultimafila=0;
		var bGenerarLinea=true;
		var eCodTipoServicio=0;

		dojo.query("[id^=\"eRegistro\"]").forEach(function(nodo, index, array){
			ultimafila=parseInt(nodo.id.replace('eRegistro',''));
		});

		if(filaEmpleado==ultimafila){
			if(dojo.byId('idEmpleado'+filaEmpleado)){
				if(!dijit.byId('idEmpleado'+filaEmpleado).value){
					bGenerarLinea=false;
				}
			}
			if(dojo.byId('eCodCosto'+filaEmpleado)){
				if(!dojo.byId('eCodCosto'+filaEmpleado).value){
					bGenerarLinea=false;
				}
			}
			if(bGenerarLinea==true){
				agregarConcepto(filaEmpleado);
			}
		}
	}
}

function consultar(){
	document.location = './?ePagina=2.5.1.6.php';
}

function listaempleados(codigo){
	var id = codigo.replace("idEmpleado","")
	var busqueda = dojo.byId(codigo).value;
	var acEmp='acEmpledos'+id;
	var cmbEmp='cmbEmpleados'+id;
	eval(acEmp).url = "php/auto/checadoras.php?busqueda="+busqueda;
	eval(acEmp).close();
	eval(cmbEmp).store=eval(acEmp);
}

function asignaEmpleados(){
	dojo.query("[id*=\"idEmpleado\"]").forEach(function(nodo, index, array){
		dojo.connect(dijit.byId(nodo.id), "onKeyUp", function(event){
			if(event.keyCode!=40&&event.keyCode!=38){
				listaempleados(nodo.id);
			}
		});
	});
}

dojo.addOnLoad(function(){
	dojo.parser.parse('tablaAsistencias');
	asignaEmpleados();
});
</script>
<div id="dvCNS" style="display:none;"></div>
<form id="Datos" name="Datos" method="post" onSubmit="return false;" enctype="multipart/form-data">
<input type="hidden" name="eProceso" id="eProceso" value="" />
<input type="hidden" name="eAccion" id="eAccion" value="" />
<input type="hidden" name="eServicio" id="eServicio" value="" />
<input type="hidden" name="eUsuario" id="eUsuario" value="<?=(int)$_SESSION['sesionUsuario']['eCodUsuario'];?>" />
<input type="hidden" name="eCodEntidad" id="eCodEntidad" value="<?=(int)$_SESSION['sesionUsuario']['eCodEntidad'];?>" />
<input type="hidden" name="mail" id="mail" value="<?=$_SESSION['sesionUsuario']['Correo'];?>" />
<input type="hidden" name="nombreusuario" id="nombreusuario" value="<?=$_SESSION['sesionUsuario']['Usuario'];?>" />
<input type="hidden" name="eFilaEmp" id="eFilaEmp" value="" />
<input type="hidden" name="eFDiaria" id="eFDiaria" value="<?=date('Ymd');?>" />
<div id="dvFacturacion"></div>
<table border="0" cellpadding="0" cellspacing="0" width="980px">
	 <tr>
        <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Registro Patronal </td>
        <td nowrap class="sanLR04" colspan="3">
        	<select name="eCodEmisor" id="eCodEmisor" style="width:300px">
                <?php while($rEmisor=mysqli_fetch_array($rsEmisores,MYSQLI_ASSOC)){ ?>
                    <option value="<?=($rEmisor{'eCodEntidad'});?>" ><?=utf8_encode($rEmisor{'tNombre'})." - ".$rEmisor{'tRegistroPatronal'};?></option>
                <?php } ?>
            </select>
        </td>
    </tr>
    <tr>
        <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Fecha de Asistencia</td>
        <td width="50%" nowrap class="sanLR04">
        	<input name="fhFechaAsistencia" type="date" dojoType="dijit.form.TextBox" id="fhFechaAsistencia" value="<?=date('Y-m-d');?>" style="width:140px" onChange="verificarEmpleados();">
        </td>
        <td nowrap class="sanLR04"></td>
        <td width="50%" nowrap class="sanLR04"></td>
    </tr>
    <tr>
        <td height="23" nowrap class="sanLR04"></td>
        <td width="50%" nowrap class="sanLR04"></td>
        <td nowrap class="sanLR04"></td>
        <td width="50%" nowrap class="sanLR04"></td>
    </tr>
    <?php mysqli_data_seek($rsCostos,0);?>
    <tr>
    	<td colspan="4">
            <table cellspacing="0" border="0" width="100%" id="tablaAsistencias" name="tablaAsistencias">
                <thead>
                    <tr class="thEncabezado">
                        <td nowrap="nowrap" class="sanLR04" height="23"> </td>
                        <td nowrap="nowrap" class="sanLR04" height="23"> Empleado</td>
                        <td nowrap="nowrap" class="sanLR04" height="23"> Categor&Iacute;a</td>
                        <td nowrap="nowrap" class="sanLR04" width="100%"> Centro de Costo</td>
                    </tr>
                </thead>
                <tbody id="tbAsistencias" name="tbAsistencias">
                    <tr id="filaAsistencia1" name="filaAsistencia1">
                        <td nowrap="nowrap" class="sanLR04" height="23"><img width="16" align="absmiddle" height="16" onclick="borrarAsistencia(1);" id="filaConcepto1" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-borrar.png" title="Eliminar Concepto"></td>
                        <td nowrap="nowrap" class="sanLR04" height="23"><input type="hidden" name="eRegistro1" id="eRegistro1" value="1">
                        <div dojoType="dojo.data.ItemFileReadStore" jsId="acEmpledos1" url="php/auto/catchecadoras.php" clearOnClose="true" urlPreventCache="true"></div>
                        <input dojoType="dijit.form.FilteringSelect"
                            value="<?=$rDepositoMovimiento['eCodSucursalCliente'];?>"
                            jsId="cmbEmpleados1"
                            store="acEmpledos1"
                            searchAttr="nombre"
                            hasDownArrow="false"
                            name="idEmpleado1"
                            id="idEmpleado1"
                            autoComplete="false"
                            searchDelay="300"
                            highlightMatch="none"
                            placeHolder="Empleados"
                            style="width:380px;"
                            displayMessage="false"
                            required="false"
                            queryExpr = "*"
                            pageSize="10"
                            onChange="validarEmpleado(1);"
                            >
                        </td>
                        <td nowrap="nowrap" class="sanLR04">Checador@</td>
                        <td nowrap="nowrap" class="sanLR04">
                        <select name="eCodCosto1" id="eCodCosto1" style="width:250px" onchange="validarFila(1);">
                        	<option value="">Seleccione...</option>
							<?php while($rCosto=mysqli_fetch_array($rsCostos,MYSQLI_ASSOC)){ ?>
                            	<option value="<?=$rCosto{'eCodCosto'}?>"><?=("(".$rCosto{'tSiglas'}.") ".$rCosto{'tArea'})?></option>
							<?php } ?>
                        </select>
                        </td>
                    </tr>
                </tbody>
            </table>
            <input name="eFilas" id="eFilas" value="1" type="hidden">
        </td>
    </tr>
	<tr><td height="20"></td></tr>
    <tr><td valign="top" nowrap="nowrap" class="sanLR04" colspan="4"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle" /> <font class="fntG10K">Indica que es obligatorio llenar el campo.</font></td></tr>
</table>
</form>
<?php } ?>
