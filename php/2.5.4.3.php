﻿<?php 
require_once("conexion/soluciones-mysql.php"); 
$link = getLink();
if($_POST){
	
date_default_timezone_set('America/Mexico_City');
$varfecha=isset($_POST["fecha"]) ? $_POST["fecha"] : date("Y-m-d");
$fechabaja = date('Y-m-d',strtotime('-2 days', strtotime($varfecha)));
$fechaayer = date('Y-m-d',strtotime('-1 days', strtotime($varfecha)));
//$fechavalidanext = date('Y-m-d',strtotime('-3 days', strtotime($varfecha)));

//Consulta para validar si tienen continuacion de asistencia dia siguiente mismo empleado.
$dianext="SELECT asi.idEmpleado ".
" FROM asistencias AS asi ".
" LEFT JOIN relasistenciamovimientos AS rel ON rel.eCodAsistencia=asi.id ".
" LEFT JOIN movimientosafiliatorios AS mov ON mov.eCodMovimiento=rel.eCodMovimiento AND mov.eCodEstatus='AC' ".
" WHERE asi.Estado='AC' AND mov.fhFecha='".$fechaayer."' ".
" group by asi.idEmpleado order by asi.idEmpleado ";  

$continua="";
if($Rsdianext=mysqli_query($link,$dianext))
{
  while($rdianext=mysqli_fetch_array($Rsdianext,MYSQLI_ASSOC))
  {
    $continua=$continua.$rdianext["idEmpleado"]." , ";
  }
}
  $continua=$continua." 0 ";

  $dianext="SELECT asi.idEmpleado ".
" FROM asistencias AS asi ".
" LEFT JOIN relasistenciamovimientos AS rel ON rel.eCodAsistencia=asi.id ".
" LEFT JOIN movimientosafiliatorios AS mov ON mov.eCodMovimiento=rel.eCodMovimiento AND mov.eCodEstatus='AC' ".
" WHERE asi.Estado='AC' AND mov.tCodTipoMovimiento='02' AND mov.fhFecha='".$fechabaja."' ".
" group by asi.idEmpleado order by asi.idEmpleado ";  

$baja="";
if($Rsdianext=mysqli_query($link,$dianext))
{
  while($rdianext=mysqli_fetch_array($Rsdianext,MYSQLI_ASSOC))
  {
    $baja=$baja.$rdianext["idEmpleado"]." , ";
  }
}
  $baja=$baja." 0 ";


$select=" SELECT asi.id AS numeral, asi.*, cat.eCodEntidad AS iden, cat.tRegistroPatronal AS rp, emp.IMSS, emp.Paterno , emp.materno, emp.nombre, ".
        " mov.tFolio, mov.fhFecha, mov.tCodTipoMovimiento ".
        " FROM asistencias AS asi ".
        " LEFT JOIN relasistenciamovimientos AS rel ON rel.eCodAsistencia=asi.id ".
        " LEFT JOIN movimientosafiliatorios AS mov ON mov.eCodMovimiento=rel.eCodMovimiento AND mov.eCodEstatus='AC' ".
        " LEFT JOIN catentidades AS cat ON asi.idEmpresa = cat.eCodEntidad ".
        " LEFT JOIN empleados AS emp ON asi.idEmpleado = emp.id ".
        " LEFT JOIN categorias AS ser ON ser.indice = asi.idCategorias ".
        " WHERE asi.Estado='AC' AND mov.fhFecha='".$fechabaja."' AND asi.idCategorias not in ( 35 ) AND mov.tFolio IS NOT NULL ".
        " AND emp.id not in ( ".$continua." , ".$baja." ) AND emp.eCodTipo=1 ".        
        " group by asi.idEmpleado order by asi.id ";

    //print($select);
    $rsServicios=mysqli_query($link,$select);     
    $cont=0;
    $registros=(int)mysqli_num_rows($rsServicios);
	?>

<table cellspacing="0" border="0" width="980px">
  <tr>
    <td width="50%"><hr color="#666666" /></td>
    <td class="sanLR04" nowrap="nowrap"><b>Registros Encontrados: (<?=$registros;?>)</b></td>
    <td width="50%"><hr color="#666666" /></td>
  </tr>
</table>
<div style="display:block; top:0; left:0; width:965px; z-index=1; overflow: auto; height:400px;">
  <table cellspacing="0" border="0" width="980px">
    <thead>
      <tr class="thEncabezado">	      	
      <td nowrap="nowrap" class="sanLR04">R. Patronal</td>
	    <td nowrap="nowrap" class="sanLR04">NSS</th>
	    <td nowrap="nowrap" class="sanLR04">Paterno</td> 
	    <td nowrap="nowrap" class="sanLR04">Materno</td>
      <td nowrap="nowrap" class="sanLR04">Nombre(s)</td>
      <td nowrap="nowrap" class="sanLR04">Filler</td>
      <td nowrap="nowrap" class="sanLR04">Fecha</td>
      <td nowrap="nowrap" class="sanLR04">Filler</td>
      <td nowrap="nowrap" class="sanLR04">Movimiento</td>      
      <td nowrap="nowrap" class="sanLR04">Guia</td>
      <td nowrap="nowrap" class="sanLR04">C. Trabajador</td>
      <td nowrap="nowrap" class="sanLR04">Causa de Baja</td>
      <td nowrap="nowrap" class="sanLR04">Filler</td>      
	    <td nowrap="nowrap" class="sanLR04" width="100%">Identificador</td> <!--Ya quedo-->                    
	  </tr>   
    </thead>
    <tbody>
      <?php $i=1; while($rServicio=mysqli_fetch_array($rsServicios,MYSQLI_ASSOC))
      {                     
                        $fech=$rServicio["fhFecha"]; 
                        $fecha=explode("-",$fech); 
                        $abc=$fecha[2].$fecha[1].$fecha[0]; 
        ?>
      <tr>        
        <input type="hidden" id="id<?=$i?>" name="id<?=$i?>" value="<?=$rServicio["asi.id"]?>">        
        <td nowrap="nowrap" class="sanLR04 columnB"><b><?=$rServicio{'rp'};?></b></td>                              
        <td nowrap="nowrap" class="sanLR04"><?= utf8_decode($rServicio["IMSS"]); ?></td>
        <td nowrap="nowrap" class="sanLR04 columnB"><?= utf8_decode($rServicio["Paterno"]); ?></td>
        <td nowrap="nowrap" class="sanLR04"><?= utf8_decode($rServicio["materno"]); ?></td>
        <td nowrap="nowrap" class="sanLR04 columnB"><?= utf8_decode($rServicio["nombre"]); ?></td>
        <td nowrap="nowrap" class="sanLR04"></td>        
        <td nowrap="nowrap" class="sanLR04 columnB"><?= utf8_decode($abc); ?></td>
        <td nowrap="nowrap" class="sanLR04 "></td>
        <td nowrap="nowrap" class="sanLR04 columnB">02</td>
        <td nowrap="nowrap" class="sanLR04 ">03400</td>
        <td nowrap="nowrap" class="sanLR04 columnB">ESTIBADOR</td>
        <td nowrap="nowrap" class="sanLR04 ">TERMINO DE CONTRATO</td>
        <td nowrap="nowrap" class="sanLR04 columnB"></td>
        <td nowrap="nowrap" class="sanLR04 " width="100%">9</td>
        
      </tr>
      <?php $i++; } ?>
    </tbody>
  </table>
</div>
<?php }else{ ?>
<script type="text/javascript">
function generarBatch(){
	var UrlBatch = "php/batch/2.5.4.3.php?"+
    (dojo.byId('fecha').value ? "&fecha="+dojo.byId('fecha').value : "");    
    var Datos = document.createElement("FORM");
    document.body.appendChild(Datos);
    Datos.name='Datos';
    Datos.method = "POST";
    Datos.action = UrlBatch;
    Datos.submit();
}

dojo.addOnLoad(function(){filtrarConsulta();});
</script>
<form name="Datos" id="Datos" method="post" action="" onsubmit="return false;">
  <input type="hidden" value="0" name="ePagina" id="ePagina" />
  <input type="hidden" value="" name="eAccion" id="eAccion" />
  <table width="900px" border="0">
    <tr>
      <td colpan="3" width="100%"></td>
      <td align="right" nowrap="nowrap"><a class="fntBR11" href="javaScript:mostrarFiltros('Busqueda');">Busqueda de Registros</a></td>
    </tr>
    <tr id="trBusqueda" style="display:none">
      <td colspan="4"><table width="900px" bgcolor="#f9f9f9">
          <tr>
            <td class="sanLR04" height="5"></td>
          </tr>
          <tr>
            <td class="sanLR04" height="20">Fecha</td>
            <td class="sanLR04" width="50%"><input type="date" name="fecha" dojoType="dijit.form.TextBox" id="fecha" value="<?=$_POST["fecha"] ? $_POST["fecha"] : date("Y-m-d")?>" style="width:140px"></td>
            <td class="sanLR04"></td>
            <td class="sanLR04" width="50%"></td>
          </tr>
          <tr>
            <td class="sanLR04" height="5"></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td colspan="4"><div id="dvCNS"></div></td>
    </tr>
  </table>
</form>
<?php } ?>
