﻿<?php 
require_once("conexion/soluciones-mysql.php");
$link = getLink();
if($_POST){
	if($_POST['eProceso']==1){
		$exito = 1;
		$asistencias = 0;
		$idUsuario = ($_POST['eUsuario'] ? $_POST['eUsuario'] : "NULL");

		if((int)$_POST['edIncapacidad']>0){
			$tFolio 	= ($_POST['tFolio1'] 			? "'".$_POST['tFolio1']."'"			: "NULL");
			$fhFechaI 	= ($_POST['fhFechaInicio1'] 	? "'".$_POST['fhFechaInicio1']."'" 	: "NULL");
			$fhFechaT 	= ($_POST['fhFechaTermino1']	? "'".$_POST['fhFechaTermino1']."'" : "NULL");

			$insert=" UPDATE incapacidades ".
					" SET 	FechaInicial=".$fhFechaI.",".
					"		FechaFinal=".$fhFechaT.", ".
					"		Folio=".$tFolio.
					" WHERE id=".(int)$_POST['edIncapacidad'];
			if($res=mysqli_query($link,$insert)){
				$cadena.=$insert;
				$asistencias++;
				$exito=1;
			}else{
				$cadena.=$insert;
				$exito=0;
			}
		}else{
			foreach($_POST as $k => $v){
				$nombre = strval($k);
				if(strstr($nombre,"eRegistro") && $v){
					$eFila=str_replace("eRegistro", "", $nombre);
					$tFolio 	= ($_POST['tFolio'.$eFila]			? "'".$_POST['tFolio'.$eFila]."'"			: "NULL");
					$fhFechaI 	= ($_POST['fhFechaInicio'.$eFila]	? "'".$_POST['fhFechaInicio'.$eFila]."'"	: "NULL");
					$fhFechaT 	= ($_POST['fhFechaTermino'.$eFila]	? "'".$_POST['fhFechaTermino'.$eFila]."'"	: "NULL");
					$idEmpleado = ($_POST['idEmpleado'.$eFila]		? $_POST['idEmpleado'.$eFila]				: ($_POST['edPrevia'.$eFila] ? $_POST['edPrevia'.$eFila] : "NULL"));
					if((int)$idEmpleado>0){
						$insert=" INSERT INTO incapacidades (idEmpleado, FechaInicial, FechaFinal, Folio, idUsuario, Estado, fechaRegistro) 
								SELECT ".(int)$idEmpleado.", ".$fhFechaI.", ".$fhFechaT.", ".$tFolio.", ".$idUsuario.", 'AC', CURRENT_TIMESTAMP ";
						if($res=mysqli_query($link,$insert)){
							$cadena.=$insert;
							$asistencias++;
						}else{
							$cadena.=$insert;
							$exito=0;
						}
					}
				}
			}
		}
		print "<input type=\"text\" value=\"".((int)$exito>0 && (int)$asistencias>0 ? 1 : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}

	if($_POST['eProceso']==2){
		$select=" SELECT id ".
				" FROM asistencias ".
				" WHERE Fecha='".$_POST['fhFechaAsistencia']."'".
				" AND idEmpleado=".(int)$_POST['idEmpleado'.(int)$_POST['eFilaEmp']];
		$asistencia=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

		$select=" SELECT id
				FROM incapacidades
				WHERE idEmpleado=".(int)$_POST['idEmpleado'.(int)$_POST['eFilaEmp']].
				" AND Estado='AC'
				AND FechaInicial<='".$_POST['fhFechaInicio'.(int)$_POST['eFilaEmp']]."' 
				AND FechaFinal>='".$_POST['fhFechaInicio'.(int)$_POST['eFilaEmp']]."'".
				($_POST['edIncapacidad']>0 ? " AND id NOT IN (".$_POST['edIncapacidad'].")" : "");
		$incapacidad=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

		$select=" SELECT eCodAusentismo ".
				" FROM ausentismo ".
				" WHERE eCodEmpleado=".(int)$_POST['idEmpleado'.(int)$_POST['eFilaEmp']].
				" AND fhFecha='".$_POST['fhFechaAsistencia']."' ".
				" AND eCodEstatus='AC' ";
		$ausentis=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
		
		$select=" SELECT CASE WHEN '".$_POST['fhFechaTermino'.(int)$_POST['eFilaEmp']]."'<'".$_POST['fhFechaInicio'.(int)$_POST['eFilaEmp']]."' THEN 1 END AS fechFiMenor ";
		$fechaFiMenor=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
		print "<input type=\"hidden\" name=\"eAusentismoBD\" id=\"eAusentismoBD\" value=\"".(int)$ausentis{'eCodAusentismo'}."\">";
		print "<input type=\"hidden\" name=\"eEmpleadoBD\" id=\"eEmpleadoBD\" value=\"".(int)$asistencia{'id'}."\">";
		print "<input type=\"hidden\" name=\"eIncapacidad\" id=\"eIncapacidad\" value=\"".(int)$incapacidad{'id'}."\">";
		print "<input type=\"hidden\" name=\"fFechaMayoMen\" id=\"fFechaMayoMen\" value=\"".(int)$fechaFiMenor{'fechFiMenor'}."\">";
	}

	if($_POST['eProceso']==3){
		$exito = 1;
		$idUsuario 			= ($_POST['eUsuario'] 			? $_POST['eUsuario'] 								: "NULL");
		$tMotivoCancelacion	= ($_POST['tMotivoCancelacion']	? "'".utf8_decode($_POST['tMotivoCancelacion'])."'"	: "NULL");

		$insert=" UPDATE incapacidades ".
				" SET 	Estado='CA',".
				"		tMotivoCancelacion=".$tMotivoCancelacion.", ".
				"		fhFechaCancelacion=CURRENT_TIMESTAMP,".
				"		eCodUsuarioCancelacion=".$idUsuario.
				" WHERE id=".(int)$_POST['edIncapacidad'];
		if($res=mysqli_query($link,$insert)){
			$cadena.=$insert;
			$asistencias++;
			$exito=1;
		}else{
			$cadena.=$insert;
			$exito=0;
		}
		print "<input type=\"text\" value=\"".((int)$exito>0 ? 1 : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}
//
}else{
$select=" SELECT * ".
		" FROM incapacidades ".
		" WHERE id=".(int)$_GET['eCodIncapacidad'];
$rIncapacidad=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC); 
if((int)$_GET['ePrevia']>0){
	$select=" SELECT id AS idEmpleado 
			FROM empleados 
			WHERE id=".(int)$_GET['ePrevia'];
	$rIncapacidad=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC); 
}

?>
<style>
.eNumero{
	text-align:right;
}
</style>
<script src="../js/dojo/dijit/form/FilteringSelect.js" type="text/javascript"></script> 
<script src="../js/dojo/dojo/data/ItemFileReadStore.js" type="text/javascript"></script> 
<script src="../js/dojo/dijit/form/FilteringSelect.js" type="text/javascript"></script> 
<script src="../js/dojo/dijit/TooltipDialog.js" type="text/javascript"></script> 
<script type="text/javascript">
dojo.require("dojo.number");
dojo.require("dijit.form.NumberTextBox");
dojo.require("dijit.form.DateTextBox");
dojo.require("dojo.io.iframe");

function guardar(){
	dojo.byId('eProceso').value=1;
	var bandera = false;
	var bServicios = false;
	var bMercancias = false;
	var eTipoSolicitud = 0;
	mensaje = "¡Verifique lo siguiente!\n";

	var ultimafila=0;

	dojo.query("[id^=\"eRegistro\"]").forEach(function(nodo, index, array){
		ultimafila++;
	});

	if(parseInt(ultimafila)<=1){
		if(dojo.byId('edIncapacidad').value>0){
			if(!dojo.byId("fhFechaInicio1").value){
				bFechaI=true;
				bIncompleto=true;
			}
			if(!dojo.byId("fhFechaTermino1").value){
				bFechaT=true;
				bIncompleto=true;
			}
			if(!dojo.byId("tFolio1").value){
				bFolio=true;
				bIncompleto=true;
			}
		}else{
			mensaje+="* Ausentismos\n";
			bandera = true;
		}
	}else{
		var bIncompleto=false;
		var bEmpleado=false;
		var bCategoria=false;
		var bCosto=false;
		var bFechaI=false;
		var bFechaT=false;
		var bFolio=true;
		dojo.query("[id^=\"eRegistro\"]").forEach(function(nodo, index, array){
			var eFilaValida=parseInt(nodo.value);
			if(ultimafila>eFilaValida){
				if(!dijit.byId('idEmpleado'+eFilaValida).value){
					bEmpleado=true;
					bIncompleto=true;
				}
				if(!dojo.byId("fhFechaInicio"+eFilaValida).value){
					bFechaI=true;
					bIncompleto=true;
				}
				if(!dojo.byId("fhFechaTermino"+eFilaValida).value){
					bFechaT=true;
					bIncompleto=true;
				}
				if(!dojo.byId("tFolio"+eFilaValida).value){
					bFolio=true;
					bIncompleto=true;
				}
			}else{
				if(dijit.byId('idEmpleado'+eFilaValida).value){
					bEmpleado=true;
					bIncompleto=true;
				}
				if(dojo.byId("fhFechaInicio"+eFilaValida).value){
					bFechaI=true;
					bIncompleto=true;
				}
				if(dojo.byId("fhFechaTermino"+eFilaValida).value){
					bFechaT=true;
					bIncompleto=true;
				}
				if(dojo.byId("tFolio"+eFilaValida).value){
					bFolio=true;
					bIncompleto=true;
				}
			}
		});
		if(bIncompleto==true){
			mensaje+="* Conceptos incompletos\n";
			if(bEmpleado==true){
				mensaje+="   Empleado\n";
			}
			if(bFechaI==true){
				mensaje+="   Fecha Inicio\n";
			}
			if(bFechaT==true){
				mensaje+="   Fecha Término\n";
			}
			if(bFolio==true){
				mensaje+="   Folio\n";
			}
			bandera = true;
		}
	}

	if (bandera==true){
		alert(mensaje);
	}else{
		if (confirm("¿Desea Guardar la Información?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				if(dojo.byId("eCodigo").value>0){
					alert("¡La información se guardó correctamente!");
					tURL = './?ePagina=2.5.2.1.php'+(dojo.byId("edIncapacidad").value>0 ? "&eCodIncapacidad="+dojo.byId("edIncapacidad").value : "");
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
		}
	}
}

function cancelar(){
	dojo.byId('eProceso').value=3;
	var bandera = false;
	var bServicios = false;
	var bMercancias = false;
	var eTipoSolicitud = 0;
	mensaje = "¡Verifique lo siguiente!\n";

	if(!dojo.byId('tMotivoCancelacion').value){
		bandera = true;
		mensaje+="* Motivo de Cancelación\n";
	}

	if (bandera==true){
		alert(mensaje);
	}else{
		if (confirm("¿Desea Cancelar la Información?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				if(dojo.byId("eCodigo").value>0){
					alert("¡La información se guardó correctamente!");
					tURL = './?ePagina=2.5.2.1.php&eCodIncapacidad='+dojo.byId("edIncapacidad").value;
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
		}
	}
}

function borrarAsistencia(eFilaAsistencia){
	var ultimafila=0;
	dojo.query("[id^=\"eRegistro\"]").forEach(function(nodo, index, array){
		ultimafila=parseInt(nodo.id.replace('eRegistro',''));
	});

	if(eFilaAsistencia!=ultimafila){
		if(confirm("¿Seguro de eliminar la incapacidad?")){
			dojo.destroy(dojo.byId('filaAsistencia'+eFilaAsistencia));
		}
	}
}

function verificarEmpleados(){
	dojo.query("[id^=\"idEmpleado\"]").forEach(function(nodo, index, array){
		var filaEmpleado=nodo.id.replace('idEmpleado','');
		if(parseInt(filaEmpleado)>0){
			validarEmpleado(filaEmpleado);
		}
	});
}

function validarEmpleado(fila){
	var ultimafila=0;
	var bDistinto=true;
	dojo.query("[id^=\"idEmpleado\"]").forEach(function(nodo, index, array){
		ultimafila=nodo.id.replace('idEmpleado','');
		if(ultimafila>0){
			if(ultimafila!=fila && dijit.byId(nodo.id).value){
				if(dijit.byId('idEmpleado'+fila).value==dijit.byId(nodo.id).value){
					alert('¡Este empleado ya se encuentra seleccionado!');
					dojo.byId('idEmpleado'+fila).value="";
					dijit.byId('idEmpleado'+fila).value="";
					dojo.byId('idEmpleado'+fila).focus();
					bDistinto=false;
				}
			}
		}
	});
	if(bDistinto==true){
		dojo.byId('eProceso').value=2;
		dojo.byId('eFilaEmp').value=fila;
		dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
			dojo.byId("dvCNS").innerHTML = tRespuesta;
			if(parseInt(dijit.byId("idEmpleado"+fila).value)>0 && dojo.byId("fhFechaInicio"+fila).value && dojo.byId("fhFechaTermino"+fila).value){
				if(parseInt(dojo.byId('fFechaMayoMen').value)>0){
					alert("¡La Fecha Término, no puede ser menor a Fecha Inicio!");
					dojo.byId('fhFechaTermino'+fila).value="";
					dijit.byId('fhFechaTermino'+fila).value="";
					dojo.byId('fhFechaTermino'+fila).focus();
				}else{
					if(parseInt(dojo.byId('eIncapacidad').value)>0){
						alert("¡El empleado cuenta con registro de incapacidad!");
						dojo.byId('idEmpleado'+fila).value="";
						dijit.byId('idEmpleado'+fila).value="";
						dojo.byId('idEmpleado'+fila).focus();
					}else{
						if(parseInt(dojo.byId("eEmpleadoBD").value)>0){
							alert("¡El empleado ya tiene asistencia en el dia!");
							dojo.byId('idEmpleado'+fila).value="";
							dijit.byId('idEmpleado'+fila).value="";
							dojo.byId('idEmpleado'+fila).focus();
						}else{
							validarFila(fila);
						}
					}
				}
			}
		}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});
	}
}

function agregarConcepto(filaAsistencia){ 
	var tb = dojo.byId('tbAsistencias');
	var tr = null;
	var td = null;
	var fila = 0;
	fila = parseInt(dojo.byId("eFilas").value)+1;
	var rows = document.getElementById('tablaAsistencias').getElementsByTagName('tbody')[0].getElementsByTagName('tr');
    for (i = 0; i < rows.length; i++) {
		if(rows[i].id == "filaAsistencia"+filaAsistencia){
			tr = tb.insertRow(rows[i].rowIndex);
		}
    }

	tr.id = "filaAsistencia"+fila;
	td = tr.insertCell(0);
	td.height = '23px';
	td.className = "sanLR04";
	td.innerHTML="<img width=\"16\" align=\"absmiddle\" height=\"16\" onclick=\"borrarAsistencia("+fila+");\" id=\"filaConcepto"+fila+"\" src=\"https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-borrar.png\" title=\"Eliminar Concepto\">";	
	td.noWrap = "true";

	td = tr.insertCell(1);
	td.className = "sanLR04";
	td.innerHTML='<input type="hidden" name="eRegistro'+fila+'" id="eRegistro'+fila+'" value="'+fila+'"><div dojoType="dojo.data.ItemFileReadStore" jsId="acEmpledos'+fila+'" url="php/auto/empleados.php" clearOnClose="true" urlPreventCache="true"></div><input dojoType="dijit.form.FilteringSelect" value="" jsId="cmbEmpleados'+fila+'" store="acEmpledos'+fila+'" searchAttr="nombre" hasDownArrow="false" name="idEmpleado'+fila+'" id="idEmpleado'+fila+'" autoComplete="false" searchDelay="300" highlightMatch="none" placeHolder="Empleados" style="width:380px;" displayMessage="false" required="false" queryExpr = "*" pageSize="10" onChange="validarEmpleado('+fila+');">';
	td.noWrap = "true";
	
	td = tr.insertCell(2);
	td.className = "sanLR04";
	td.innerHTML='<input name="fhFechaInicio'+fila+'" type="date" dojoType="dijit.form.TextBox" id="fhFechaInicio'+fila+'" value="" style="width:140px" onChange="validarEmpleado('+fila+');">';
	
	td = tr.insertCell(3);
	td.className = "sanLR04";
	td.innerHTML='<input name="fhFechaTermino'+fila+'" type="date" dojoType="dijit.form.TextBox" id="fhFechaTermino'+fila+'" value="" style="width:140px" onChange="validarEmpleado('+fila+');">';
	
	td = tr.insertCell(4);
	td.className = "sanLR04";
	td.innerHTML='<input name="tFolio'+fila+'" type="text" dojoType="dijit.form.TextBox" id="tFolio'+fila+'" value="" style="width:140px" onChange="validarEmpleado('+fila+');">';

	dojo.byId("eFilas").value++;
	dojo.parser.parse('tablaAsistencias');
	asignaEmpleados();
}

function validarFila(filaEmpleado){
	var ultimafila=0;
	var bDistinto=true;
	dojo.query("[id^=\"idEmpleado\"]").forEach(function(nodo, index, array){
		ultimafila=nodo.id.replace('idEmpleado','');
		if(ultimafila>0){
			if(ultimafila!=filaEmpleado && dijit.byId(nodo.id).value){
				if(dijit.byId('idEmpleado'+filaEmpleado).value==dijit.byId(nodo.id).value){
					alert('¡Este empleado ya se encuentra seleccionado!');
					dojo.byId('idEmpleado'+filaEmpleado).value="";
					dijit.byId('idEmpleado'+filaEmpleado).value="";
					dojo.byId('idEmpleado'+filaEmpleado).focus();
					bDistinto=false;
				}
			}
		}
	});

	if(bDistinto==true && dojo.byId('edIncapacidad').value==0){
		var ultimafila=0;
		var bGenerarLinea=true;
		var eCodTipoServicio=0;
		
		dojo.query("[id^=\"eRegistro\"]").forEach(function(nodo, index, array){
			ultimafila=parseInt(nodo.id.replace('eRegistro',''));
		});
			
		if(filaEmpleado==ultimafila){
			if(dojo.byId('idEmpleado'+filaEmpleado)){
				if(!dijit.byId('idEmpleado'+filaEmpleado).value){
					bGenerarLinea=false;
				}
			}
			if(dojo.byId('fhFechaInicio'+filaEmpleado)){
				if(!dojo.byId('fhFechaInicio'+filaEmpleado).value){
					bGenerarLinea=false;
				}
			}
			if(dojo.byId('fhFechaTermino'+filaEmpleado)){
				if(!dojo.byId('fhFechaTermino'+filaEmpleado).value){
					bGenerarLinea=false;
				}
			}
			if(dojo.byId('tFolio'+filaEmpleado)){
				if(!dojo.byId('tFolio'+filaEmpleado).value){
					bGenerarLinea=false;
				}
			}
			if(bGenerarLinea==true){
				agregarConcepto(filaEmpleado);
			}
		}
	}
}

function consultar(){
	document.location = './?ePagina=2.5.2.1.php';
}

function listaempleados(codigo){
	var id = codigo.replace("idEmpleado","")
	var busqueda = dojo.byId(codigo).value;
	var acEmp='acEmpledos'+id;
	var cmbEmp='cmbEmpleados'+id;
	eval(acEmp).url = "php/auto/empleados.php?busqueda="+busqueda;
	eval(acEmp).close();
	eval(cmbEmp).store=eval(acEmp);
}

function asignaEmpleados(){
	dojo.query("[id*=\"idEmpleado\"]").forEach(function(nodo, index, array){
		dojo.connect(dijit.byId(nodo.id), "onKeyUp", function(event){
			if(event.keyCode!=40&&event.keyCode!=38){
				listaempleados(nodo.id);
			}
		});
	});
}

dojo.addOnLoad(function(){
	dojo.parser.parse('tablaAsistencias');
	asignaEmpleados();
	if(dojo.byId('edIncapacidad').value==""){
		dojo.destroy("btn8_2");
	}
});
</script>
<div id="dvCNS" style="display:none;"></div>
<form id="Datos" name="Datos" method="post" onSubmit="return false;" enctype="multipart/form-data">
<input type="hidden" name="eProceso" id="eProceso" value="" />
<input type="hidden" name="eServicio" id="eServicio" value="" />
<input type="hidden" name="eUsuario" id="eUsuario" value="<?=(int)$_SESSION['sesionUsuario']['eCodUsuario'];?>" />
<input type="hidden" name="eCodEntidad" id="eCodEntidad" value="<?=(int)$_SESSION['sesionUsuario']['eCodEntidad'];?>" />
<input type="hidden" name="mail" id="mail" value="<?=$_SESSION['sesionUsuario']['Correo'];?>" />
<input type="hidden" name="nombreusuario" id="nombreusuario" value="<?=$_SESSION['sesionUsuario']['Usuario'];?>" />
<input type="hidden" name="edIncapacidad" id="edIncapacidad" value="<?=$rIncapacidad{'id'};?>" />
<input type="hidden" name="eFilaEmp" id="eFilaEmp" value="" />
<input type="hidden" name="eFDiaria" id="eFDiaria" value="<?=date('Ymd');?>" />
<div id="dvFacturacion"></div>
<table border="0" cellpadding="0" cellspacing="0" width="980px">
    <tr>
        <td height="23" nowrap class="sanLR04"></td>
        <td width="50%" nowrap class="sanLR04"></td>
        <td nowrap class="sanLR04"></td>
        <td width="50%" nowrap class="sanLR04"></td>
    </tr>
    <tr>
    	<td colspan="4">
            <table cellspacing="0" border="0" width="100%" id="tablaAsistencias" name="tablaAsistencias">
                <thead>
                    <tr class="thEncabezado">
                        <td nowrap="nowrap" class="sanLR04" height="23"> </td>
                        <td nowrap="nowrap" class="sanLR04"> Empleado</td>
                        <td nowrap="nowrap" class="sanLR04"> Fecha Inicio</td>
                        <td nowrap="nowrap" class="sanLR04"> Fecha T&eacute;rmino</td>
                        <td nowrap="nowrap" class="sanLR04"> Folio</td>
                        <td nowrap="nowrap" class="sanLR04" width="100%"></td>
                    </tr>
                </thead>
                <tbody id="tbAsistencias" name="tbAsistencias">
                    <tr id="filaAsistencia1" name="filaAsistencia1">
                        <td nowrap="nowrap" class="sanLR04" height="23">
                        	<?php if((int)$rIncapacidad{'idEmpleado'}>0 && (int)$_GET['ePrevia']==0){ ?>
                        		<?=sprintf("%07d",$rIncapacidad{'id'});?>
                        		<input type="hidden" name="eCodIncapacidad1" id="eCodIncapacidad1" value="<?=$rIncapacidad{'id'};?>">
                        	<?php }else{ ?>
	                        	<img width="16" align="absmiddle" height="16" onclick="borrarAsistencia(1);" id="filaConcepto1" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-borrar.png" title="Eliminar Concepto">
	                        	<input type="hidden" name="eRegistro1" id="eRegistro1" value="1">
	                        	<input type="hidden" name="edPrevia1" id="edPrevia1" value="<?=(int)$_GET['ePrevia']?>">
	                        <?php } ?>
                        </td>
                        <td nowrap="nowrap" class="sanLR04" height="23">
	                        <div dojoType="dojo.data.ItemFileReadStore" jsId="acEmpledos1" url="php/auto/empleados.php?idEmpleado=<?=$rIncapacidad{'idEmpleado'} ? $rIncapacidad{'idEmpleado'} : "";?>" clearOnClose="true" urlPreventCache="true"></div>
	                        <input dojoType="dijit.form.FilteringSelect" 
	                        	<?=((int)$rIncapacidad{'idEmpleado'}>0 ? "disabled" : "");?>
	                            value="<?=(int)$rIncapacidad{'idEmpleado'}>0 ? $rIncapacidad{'idEmpleado'} : "";?>"
	                            jsId="cmbEmpleados1"
	                            store="acEmpledos1"
	                            searchAttr="nombre"
	                            hasDownArrow="false"
	                            name="idEmpleado1"
	                            id="idEmpleado1"
	                            autoComplete="false"
	                            searchDelay="300"
	                            highlightMatch="none"
	                            placeHolder="Empleados" 
	                            style="width:380px;"
	                            displayMessage="false"
	                            required="false"
	                            queryExpr = "*"
	                            pageSize="10"
	                            onChange="validarEmpleado(1);"
	                            >
                        </td>
                        <td nowrap="nowrap" class="sanLR04">
                        	<input name="fhFechaInicio1" type="date" dojoType="dijit.form.TextBox" id="fhFechaInicio1" value="<?=($rIncapacidad{'FechaInicial'} ? $rIncapacidad{'FechaInicial'} : "");?>" style="width:140px" onChange="validarEmpleado(1);">
                        </td>
                        <td nowrap="nowrap" class="sanLR04">
                        	<input name="fhFechaTermino1" type="date" dojoType="dijit.form.TextBox" id="fhFechaTermino1" value="<?=($rIncapacidad{'FechaFinal'} ? $rIncapacidad{'FechaFinal'} : "");?>" style="width:140px" onChange="validarEmpleado(1);">
                        </td>
                        <td nowrap="nowrap" class="sanLR04">
                        	<input name="tFolio1" type="text" dojoType="dijit.form.TextBox" id="tFolio1" value="<?=$rIncapacidad{'Folio'};?>" style="width:140px" onChange="validarEmpleado(1);">
                        </td>
                        <td nowrap="nowrap" class="sanLR04"></td>
                        
                    </tr>
                </tbody>
            </table>
            <input name="eFilas" id="eFilas" value="1" type="hidden">
        </td>
    </tr>
    <?php if((int)$rIncapacidad{'idEmpleado'}>0){ ?>
    	<tr><td height="20"></td></tr>
    	<tr>
	        <td height="23" nowrap="" class="sanLR04" valign="top">Motivo de Cancelación</td>
	        <td colspan="3" nowrap="" class="sanLR04"><textarea name="tMotivoCancelacion" id="tMotivoCancelacion" cols="100" rows="4"></textarea></td>
	    </tr>
    <?php } ?>
	<tr><td height="20"></td></tr>
    <tr><td valign="top" nowrap="nowrap" class="sanLR04" colspan="4"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle" /> <font class="fntG10K">Indica que es obligatorio llenar el campo.</font></td></tr>
</table>
</form>
<?php } ?>