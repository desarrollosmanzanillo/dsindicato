<?php require_once("conexion/soluciones-mysql.php");
$link = getLink();
if($_POST){
	if($_POST['eProceso']==1){
		$exito				= 0;
		$tIP				= ($_SERVER['REMOTE_ADDR']	? "'".trim(utf8_decode(addslashes($_SERVER['REMOTE_ADDR'])))."'"	: "NULL");
		$eCodArea			= ($_POST['eCodArea']		? (int)$_POST['eCodArea']										: "NULL");
		$tPatente			= ($_POST['tPatente']		? "'".trim(utf8_decode(addslashes($_POST['tPatente'])))."'"		: "NULL");
		$eCodCliente		= ($_POST['eCodCliente']		? (int)$_POST['eCodCliente']										: "NULL");
		$eCodNaviera		= ($_POST['eCodNaviera']		? (int)$_POST['eCodNaviera']										: "NULL");
		$eCodUsuario		= ($_POST['eCodUsuario']		? (int)$_POST['eCodUsuario']										: "NULL");
		$eCodSolicitud		= ($_POST['eCodSolicitud']	? (int)$_POST['eCodSolicitud']									: "NULL");
		$eCodFacturarA		= ($_POST['eCodFacturarA']	? (int)$_POST['eCodFacturarA']									: "NULL");
		$fhFechaSalida		= ($_POST['fhFechaSalida']	? "'".$_POST['fhFechaSalida']."'"								: "NULL");
		$tCodContenedor		= ($_POST['tCodContenedor']	? "'".$_POST['tCodContenedor']."'"								: "NULL");
		$tObservaciones		= ($_POST['tObservaciones1']	? "'".$_POST['tObservaciones1']."'"								: "NULL");

		if((int)$_POST['eCodSolicitud']>0){
			$insert=" INSERT INTO opesalidasmercancias (eCodSolicitud, eCodTipoSalida, eCodNaviera, eCodCliente, eCodFacturarA, ".
					" eCodUsuario, tCodEstatus, fhFecha, fhFechaSalida, tPatente, tIP) ".
					" SELECT ".$eCodSolicitud.", 1, ".$eCodNaviera.", ".$eCodCliente.", ".$eCodFacturarA.", ".
					$eCodUsuario.", 'FI', CURRENT_TIMESTAMP, ".$fhFechaSalida.", ".$tPatente.", ".$tIP.
					" FROM opesolicitudesservicios bs ".
					" INNER JOIN relsolicitudesserviciosmercanciassalidas rss ON rss.eCodSolicitud=bs.eCodSolicitud ".
					" INNER JOIN opeentradasmercancias rs ON rs.eCodEntrada=rss.eCodEntrada ".
					" WHERE bs.eCodSolicitud=".$eCodSolicitud." ".($tCodContenedor<>'NULL' ? " AND rs.tCodContenedor=".$tCodContenedor : '');
		}else{
			$insert=" INSERT INTO opesalidasmercancias (eCodTipoSalida, eCodUsuario, eCodNaviera, eCodCliente, eCodFacturarA, ".
					" tCodEstatus, tPatente, fhFecha, fhFechaSalida, tIP, tObservaciones) ".
					" SELECT 1, ".$eCodUsuario.", ".$eCodNaviera.", ".$eCodCliente.", ".$eCodFacturarA.", ".
					" 'FI', ".$tPatente.", CURRENT_TIMESTAMP, ".$fhFechaSalida.", ".$tIP.", ".$tObservaciones;
		}
		if($res=mysqli_query($link,$insert)){
			$exito=1;
			$select=" select last_insert_id() AS Llave ";
			$rCodigo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
			$eCodSalida=(int)$rCodigo['Llave'];
			if((int)$_POST['eCodSolicitud']>0){
				$select=" SELECT rs.* ".
						" FROM relsolicitudesserviciosmercanciassalidas rs ".
						" INNER JOIN opeentradasmercancias oe ON oe.eCodEntrada=rs.eCodEntrada ".
						" WHERE rs.eCodSolicitud=".$eCodSolicitud.($tCodContenedor<>"NULL" ? " AND oe.tCodContenedor=".$tCodContenedor : "");
				$rsEntradas=mysqli_query($link,$select);
				while($rEntrada=mysqli_fetch_array($rsEntradas,MYSQLI_ASSOC)){
					$update=" UPDATE opeentradasmercancias ".
							" SET eCodSalida=".$eCodSalida.
							" WHERE eCodEntrada=".$rEntrada{'eCodEntrada'};
					mysqli_query($link,$update);
					$eCodEntrada=$rEntrada{'eCodEntrada'};
				}
			}else{
				$update=" UPDATE opeentradasmercancias ".
						" SET eCodSalida=".$eCodSalida.
						" WHERE eCodEntrada=".$_POST['eCodEntrada1'];
				mysqli_query($link,$update);
				$eCodEntrada=$_POST['eCodEntrada1'];
			}
			
			if((int)$_POST['bEIR']>0){
				$insert=" INSERT INTO biteirs (eCodEntrada, tCodEstatus, fhFecha, eCodTipoEIR) ".
						" VALUES (".$eCodEntrada.", 'AC', CURRENT_TIMESTAMP, 2)";
				if($res=mysqli_query($link,$insert)){
					$select=" select last_insert_id() AS Llave ";
					$rCodigo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
					$eCodEIR=(int)$rCodigo['Llave'];
					foreach($_POST as $campo => $valor){
						if(strstr($campo,'eCodDanio') && $valor && $exito){
							$ePosX 		= $_POST[str_replace('eCodDanio','ePosX',$campo)];
							$ePosY 		= $_POST[str_replace('eCodDanio','ePosY',$campo)];
							$tImagen 	= basename($_POST[str_replace('eCodDanio','tImagen',$campo)]);
							$eCodCara 	= $_POST[str_replace('eCodDanio','eCodCara',$campo)];
							$eCodDanio 	= $valor;
							$eCodVista 	= $_POST[str_replace('eCodDanio','eCodVista',$campo)];

							$insert=" INSERT INTO releirsdanios (eCodEIR, eCodVista, eCodCara, eCodDanio, ePosX, ePosY, fhFecha) ".
									" VALUES (".$eCodEIR.", ".$eCodVista.", ".$eCodCara.", ".$eCodDanio.", ".$ePosX.", ".$ePosY.", CURRENT_TIMESTAMP) ";
							if($res=mysqli_query($link,$insert)){
							}else{
								$exito=0;
							}
						}
					}
				}else{
					$exito=0;
				}
			}
		}else{
			$exito=0;
		}
		print "<input type=\"text\" value=\"".($exito==1 ? $eCodSalida : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}

	if($_POST['eProceso']==2){
		header('Content-Type: text/html; charset=windows-1250');	
		$tRespuesta = "<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">";
		$i=0;

		//$tTMP = $_FILES['tArchivo']['tmp_name'];

		/*if (is_uploaded_file($tTMP)){
			$tArchivo = /*"../../upl/02.05.01.01/".* /uniqid(date('YmdHis'),true);
			$tExt = pathinfo($_FILES['tArchivo']['name'], PATHINFO_EXTENSION);	
			$tMIME = $_FILES['tArchivo']['type'];											

			//if(
			move_uploaded_file($tTMP, $tArchivo)//){
				$tRespuesta.="<tr>".
							 "<td class=\"sanLR04\" width=\"100%\">".
							 "<a href=\"../qry/html-descargararchivo.php?eCodArchivo=".$eCodArchivo."\" target=\"_blank\" class=\"fntN11S\" >Ver Archivo</a>".
							 "<input type=\"hidden\" name=\"eArchivoBD".$indice."\" id=\"eArchivoBD".$indice."\" value=\"".$eCodArchivo."\" />".
							 "</td></tr>";
			}else{*/
				$tRespuesta ="<tr>".
							 "<td class=\"sanLR04\" width=\"100%\">".
							 "<span class=\"fntG10K\">&iexcl;Ha ocurrido un error a la subida del archivo, intente de nuevo!</span>".
							 "</td></tr>";
			//}
		//}
				
		$tRespuesta.="</table>";	
		print "<textarea>".$tRespuesta."</textarea>";
	}

	if($_POST['eProceso']==3){
		$i=0;
		foreach($_POST as $campo => $valor){
			if(strstr($campo,'eCodDanio') && $valor){
				$eCodDanio = $valor;
				$eCodCara = $_POST[str_replace('eCodDanio','eCodCara',$campo)];
				$eCodVista = $_POST[str_replace('eCodDanio','eCodVista',$campo)];
				$tDanios.=($tDanios ? " UNION " : "")." SELECT ".$i." AS eIndice, ".$eCodVista." AS eCodVista, ".$eCodDanio." AS eCodDanio, ".$eCodCara." AS eCodCara";
				$i++;
			}
		}
		///
		$select=" SELECT N1.eIndice, cv.tNombre AS Vista, cc.tNombre AS Cara, cd.tCodDanio, cd.tNombre AS Danio ".
				" FROM (".$tDanios.") AS N1 ".
				" INNER JOIN catcarascontenedores cc ON cc.eCodCara = N1.eCodCara ".
				" INNER JOIN catdanioscontenedores cd ON cd.eCodDanio = N1.eCodDanio ".
				" INNER JOIN catvistascontenedores cv ON cv.eCodVista = N1.eCodVista ".
				" ORDER BY N1.eIndice ";
		$rsDanios = mysqli_query($link,$select);
		if(mysqli_num_rows($rsDanios)){
			$html="<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">";
			while($rDanio = mysqli_fetch_array($rsDanios,MYSQLI_ASSOC)){
				$html.="<tr><td class=\"sanLR04\" height=\"23\" nowrap=\"nowrap\"><span class=\"fntG10S\">(".($rDanio{'Vista'}).")</span></td><td class=\"sanLR04\" height=\"20\"><span class=\"fntG10S\">".utf8_encode($rDanio{'Cara'})."</span></td><td class=\"sanLR04\" nowrap=\"nowrap\"><span class=\"fntR11S\">".utf8_encode("(".trim($rDanio{'tCodDanio'}).")")."</span></td><td class=\"sanLR04\" ><span class=\"fntR11S\">".utf8_encode($rDanio{'Danio'})."</span></td></tr>";
			}
			$html.="</table>";
		}else{
			$html="Ninguno";
		}
		print $html;
	}
	
	if($_POST['eProceso']==4){
		$select=" SELECT * FROM opeentradasmercancias WHERE tCodContenedor='".$_POST['tMercancia1']."' AND eCodSalida IS NULL ";
		$rEntrada=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
		print $rEntrada{'eCodEntrada'}."|".$rEntrada{'eCodNaviera'}."|".$rEntrada{'eCodCliente'}."|".$rEntrada{'eCodFacturarA'}."|".$rEntrada{'tBL'}."|".$rEntrada{'eCodTipoContenedor'};
	}
	
	if($_POST['eProceso']==5){
		$select=" SELECT * FROM opeentradasmercancias WHERE tBooking='".$_POST['tBL']."' AND eCodSalida IS NULL ";
		$rBooking=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
		print $rBooking{'tCodContenedor'};
	}

	if($_POST['eProceso']==7){
		$select=" SELECT * FROM( ".
				" SELECT see.* ".
				" FROM catentidades see ".
				" WHERE eCodEntidad=".(int)(int)$_POST['eCodCliente'].
				" UNION ".
				" SELECT ce.* ".
				" FROM catentidades ce ".
				" INNER JOIN relclientesagentes rc ON rc.eCodCliente=ce.eCodEntidad AND rc.eCodAgente=".(int)$_POST['eCodCliente'].
				" WHERE ce.tCodEstatus='AC' AND ce.eCodTipoEntidad=3 ".
				" ) AS R1 ".
				" ORDER BY tNombre ";
		$rsFacturarA=mysqli_query($link,$select); ?>
		<select name="eCodFacturarA" id="eCodFacturarA" style="width:175px">
			<option value="">Seleccione...</option>
			<?php while($rFacturarA=mysqli_fetch_array($rsFacturarA,MYSQLI_ASSOC)){ ?>
				<option value="<?=$rFacturarA{'eCodEntidad'}?>"><?=utf8_encode($rFacturarA{'tNombre'})?></option>
			<?php } ?>
		</select>
	<?php }
//
}
if(!$_POST){ 
$select=" SELECT oss.fhFechaServicio, cbu.tNombre AS Buque, oss.tNumeroViaje, cts.tNombre AS TipoServicio, ".
		" oss.eCodNaviera, oss.tPatente, oss.eCodCliente, oss.eCodFacturarA, oss.tBL, oss.eCodTipoServicio, ".
		" oss.eCodSolicitud ".
		" FROM opesolicitudesservicios oss ".
		" INNER JOIN relsolicitudesserviciosmercanciassalidas rss ON rss.eCodSolicitud=oss.eCodSolicitud ".
		" INNER JOIN catbuques cbu ON cbu.eCodBuque=oss.eCodBuque ".
		" INNER JOIN cattiposservicios cts ON cts.eCodTipoServicio=oss.eCodTipoServicio ".
		" LEFT JOIN opeentradasmercancias oem ON oem.eCodEntrada=rss.eCodEntrada ".
		" WHERE oss.eCodSolicitud=".$_GET['eCodSolicitud'].($_GET['tCodContenedor']!="" ? " AND oem.tCodContenedor='".$_GET['tCodContenedor']."'" : "").
		" AND oem.eCodSalida IS NULL ";
$rSolicitud=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

$select=" SELECT ca.*, cta.tNombre AS TipoArea ".
		" FROM catareas ca ".
		" INNER JOIN cattiposareas cta ON cta.eCodTipoArea=ca.eCodTipoArea ".
		" WHERE ca.tCodEstatus='AC' ".
		" ORDER BY ca.tNombre ASC ";
$rsAreas=mysqli_query($link,$select);

$select=" SELECT * ".
		" FROM catentidades ".
		" WHERE eCodTipoEntidad=2 AND tCodEstatus='AC' ".
		" ORDER BY tNombre ";
$rsNavieras=mysqli_query($link,$select);

$select=" SELECT * ".
		" FROM catentidades ".
		" WHERE eCodTipoEntidad=1 AND tCodEstatus='AC' ".
		" ORDER BY tNombre ";
$rsClientes=mysqli_query($link,$select);

$select=" SELECT * ".
		" FROM catentidades ".
		" WHERE tCodEstatus='AC' ".
		" ORDER BY tNombre ";
$rsFacturarA=mysqli_query($link,$select);
?>
<script type="text/javascript">
dojo.require("dojo.number");
dojo.require("dijit.form.NumberTextBox");
dojo.require("dijit.form.DateTextBox");
dojo.require("dojo.io.iframe");
function guardar(){
	dojo.byId('eProceso').value=1;
	var bandera = false;
	var bServicios = false;
	var bMercancias = false;
	mensaje = "¡Verifique lo siguiente!\n";
	if (!dojo.byId("fhFechaSalida").value){
		mensaje+="* Fecha de Salida\n";
		bandera = true;		
	}
	if (!dojo.byId("eCodNaviera").value){
		mensaje+="* Naviera\n";
		bandera = true;		
	}
	if (!dojo.byId("eCodCliente").value){
		mensaje+="* Cliente\n";
		bandera = true;		
	}
	if (!dojo.byId("eCodFacturarA").value){
		mensaje+="* Facturar a\n";
		bandera = true;		
	}
	if (parseInt(dojo.byId("eRegistros").value)==0 && !dojo.byId('eCodEntrada1')){
		mensaje+="* Mercancía\n";
		bandera = true;
	}
	if(dojo.byId('eCodEntrada1')){
		if(!dojo.byId('eCodEntrada1').value){
			mensaje+="* Mercancía\n";
			bandera = true;
		}
	}

	if (bandera==true){
		alert(mensaje);		
	}else{
		if (confirm("¿Desea Guardar la Información?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				if(dojo.byId("eCodigo").value>0){
					alert("¡La información se guardó correctamente!");
					if(dojo.byId("eRegistros").value>1){
						tURL = './?ePagina=2.5.5.2.php';
					}else{
						tURL = './?ePagina=2.5.5.2.1.php&eCodSalida='+dojo.byId("eCodigo").value;
					}
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
		}
	}
}	

function subirarchivo(){
	if(dojo.byId("tArchivo").value){
		dojo.byId("eProceso").value = 2;
		dojo.byId("divArchivo").innerHTML = "Subiendo...";
		dojo.io.iframe.send({url: "php/"+dojo.byId('ePagina').value+".php", method: "post", handleAs: "text", form: 'Datos', handle: function(tRespuesta, ioArgs){
			dojo.byId("divArchivo").innerHTML = tRespuesta;
		}});
	}
}

function obtenerEntrada(){
	if(dojo.byId('eCodSolicitud').value==""){
		dojo.byId("eProceso").value = 4;
		dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
			var arrEntrada=tRespuesta.split("|");
			if(arrEntrada[0]>0){
				dojo.byId('eCodEntrada1').value=arrEntrada[0];
				dojo.byId('eCodNaviera').value=(dojo.byId('eCodNaviera').value	? dojo.byId('eCodNaviera').value	: arrEntrada[1]);
				dojo.byId('eCodCliente').value=(dojo.byId('eCodCliente').value	? dojo.byId('eCodCliente').value	: arrEntrada[2]);
				dojo.byId('eCodFacturarA').value=(dojo.byId('eCodFacturarA').value	? dojo.byId('eCodFacturarA').value	: arrEntrada[3]);
				dojo.byId('tBL').value=(dojo.byId('tBL').value	? dojo.byId('tBL').value	: arrEntrada[4]);
				dojo.byId('eCodTipoContenedor1').value=arrEntrada[5];
			}else{
				if(dojo.byId('tMercancia1').value){
					alert('¡No existe el contenedor!');
					dojo.byId('tMercancia1').value="";
				}
				dojo.byId('eCodEntrada1').value="";
			}
		}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});
	}
}

//EIR's
eTotal=0;
var bCargando=false;
function agregarDanio(ev,el,p,v,c){
	posm=(mousePos(ev));
	mouX = posm.x;
	mouY = posm.y;
	posd = dojo.position(dojo.byId('divImagen'+v),true);
	divX = posd.x;
	divY = posd.y;
	posX=posm.x-posd.x;
	posY=posm.y-posd.y;
	var eDiv=document.createElement("divPunto"+eTotal);
	eDiv.innerHTML+= "<span class=\"fntR11S\">"+valorRadio(document.Datos.rdDanio)+"</span>";
	eDiv.innerHTML+= "<input type=\"hidden\" name=\"eCodDanio"+eTotal+"\" id=\"eCodDanio"+eTotal+"\" value=\""+dojo.byId("D-"+valorRadio(document.Datos.rdDanio)).value+"\" />";
	eDiv.innerHTML+= "<input type=\"hidden\" name=\"tCodDanio"+eTotal+"\" id=\"tCodDanio"+eTotal+"\" value=\""+valorRadio(document.Datos.rdDanio)+"\" />";
	eDiv.innerHTML+= "<input type=\"hidden\" name=\"eCodVista"+eTotal+"\" id=\"eCodVista"+eTotal+"\" value=\""+v+"\"/>";
	eDiv.innerHTML+= "<input type=\"hidden\" name=\"eCodCara"+eTotal+"\" id=\"eCodCara"+eTotal+"\" value=\""+c+"\"/>";
	eDiv.innerHTML+= "<input type=\"hidden\" name=\"tImagen"+eTotal+"\" id=\"tImagen"+eTotal+"\" value=\""+dojo.byId('img'+v).src+"\"/>";
	eDiv.innerHTML+= "<input type=\"hidden\" name=\"ePosX"+eTotal+"\" id=\"ePosX"+eTotal+"\" value=\""+(posX-6)+"\"/>";
	eDiv.innerHTML+= "<input type=\"hidden\" name=\"ePosY"+eTotal+"\" id=\"ePosY"+eTotal+"\" value=\""+(posY-6)+"\"/>";
	eDiv.id = "divPunto"+eTotal;
	dojo.style(eDiv, 'position','absolute');
	dojo.style(eDiv, 'left', posX-6+'px');
	dojo.style(eDiv, 'top', posY-6+'px');
	dojo.style(eDiv, 'zindex', eTotal * 1000);
	var cDiv=document.createElement("cer"+eTotal);
	cDiv.innerHTML = "<img src=\"https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-ca.png\" width=\"16\" height=\"16\" />";
	dojo.style(cDiv, 'position','absolute');
	dojo.style(cDiv, 'left', '-12px');
	dojo.style(cDiv, 'top', '-12px');
	eDiv.appendChild(cDiv);
	cDiv.onclick = function(){p = (cDiv.parentElement ? cDiv.parentElement : cDiv.parentNode); dojo.byId('divVista'+v).removeChild(p);mostrarDanios();}
	dojo.byId('divVista'+v).appendChild(eDiv);
	dojo.byId('eCodCara').value = "";
	eTotal++;
	mostrarDanios();
}

function asignarCara(ev,el,v,c){
	if(!bCargando){
		p = (el.parentElement.parentElement ? el.parentElement.parentElement : el.parentNode.parentNode);
		agregarDanio(ev,el,p,v,c);
	}
}

function mostrarDanios(){
	bCargando = true;
	dojo.byId("eProceso").value = 3;
	dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
		dojo.byId("divDaniosRegistrados").innerHTML = tRespuesta;
	}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
	bCargando = false;
}
// Fin EIR's

function validarBooking(){
	dojo.byId("eProceso").value = 5;
	dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
		if(tRespuesta!=""){
			if(dojo.byId('tMercancia1')){
				dojo.byId('tMercancia1').value=tRespuesta;
				setTimeout(obtenerEntrada, 1000);
			}
		}
	}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
}

function cargarFacturarA(){
	dojo.byId('eProceso').value=7;
	dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
		dojo.byId("tdFacturarA").innerHTML = tRespuesta;
	}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});
}

function consultar(){
	document.location = "?ePagina=2.5.5.2.php";
}

dojo.addOnLoad(function(){
});
</script>
<div id="dvCNS" style="display:none;"></div>
<form method="post" name="Datos" id="Datos" onsubmit="return false;" enctype="multipart/form-data">
<input type="hidden" name="eProceso" id="eProceso" value="" />
<input type="hidden" name="eCodSolicitud" id="eCodSolicitud" value="<?=$rSolicitud{'eCodSolicitud'};?>" />
<input type="hidden" name="tCodContenedor" id="tCodContenedor" value="<?=$_GET['tCodContenedor'];?>" />
<input type="hidden" name="eCodUsuario" id="eCodUsuario" value="<?=(int)$_SESSION['sesionUsuario']['eCodUsuario'];?>" />
<input type="hidden" name="ePosX" id="ePosX" />
<input type="hidden" name="tCara" id="tCara" />
<input type="hidden" name="ePosY" id="ePosY" />
<input type="hidden" name="tImagen" id="tImagen" />  
<input type="hidden" name="eCodCara" id="eCodCara" />
<table border="0" cellpadding="0" cellspacing="0" width="900px">
    <tr><td height="20"></td></tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Fecha de Salida </td>
	    <td width="50%" nowrap class="sanLR04">
           	<input name="fhFechaSalida"
            id="fhFechaSalida"
            type="date"            
            required="false"
            style="width:140px;"
            hasDownArrow="false"
            displayMessage="false"
            value="<?=date("Y-m-d", strtotime($rSolicitud{'fhFechaServicio'} ? $rSolicitud{'fhFechaServicio'} : date('Y-m-d')));?>"            
            />
        </td>
		<td height="23" nowrap class="sanLR04"></td>
	    <td width="50%" nowrap class="sanLR04"></td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Buque </td>
	    <td width="50%" nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'Buque'} ? $rSolicitud{'Buque'} : "N/D");?></td>
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Viaje </td>
	    <td width="50%" nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'tNumeroViaje'} ? $rSolicitud{'tNumeroViaje'} : "N/D");?></td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Tipo de Salida </td>
	    <td width="50%" nowrap class="sanLR04">Salida Directa</td>
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Tipo de Servicio </td>
	    <td width="50%" nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'TipoServicio'} ? $rSolicitud{'TipoServicio'} : "Contenedor");?></td>
    </tr>
	<tr><td colspan="4" align="center"><hr width="95%" size="0" align="center" color="#CACACA"></td></tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Naviera </td>
	    <td width="50%" nowrap class="sanLR04">
           	<select name="eCodNaviera" id="eCodNaviera" style="width:175px">
				<option value="">Seleccione...</option>
				<?php while($rNaviera=mysqli_fetch_array($rsNavieras,MYSQLI_ASSOC)){ ?>
					<option value="<?=$rNaviera{'eCodEntidad'}?>" <?=($rNaviera{'eCodEntidad'}==$rSolicitud{'eCodNaviera'} ? "selected='selected'" : "");?> ><?=utf8_encode($rNaviera{'tNombre'})?></option>
				<?php } ?>
			</select>
        </td>
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Patente </td>
	    <td width="50%" nowrap class="sanLR04">
			<input name="tPatente" type="text" dojoType="dijit.form.TextBox" id="tPatente" value="<?=utf8_encode($rSolicitud{'tPatente'});?>" style="width:80px" UpperCase="true">
        </td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Cliente </td>
	    <td width="50%" nowrap class="sanLR04">
           	<select name="eCodCliente" id="eCodCliente" style="width:175px" onchange="cargarFacturarA();">
				<option value="">Seleccione...</option>
				<?php while($rCliente=mysqli_fetch_array($rsClientes,MYSQLI_ASSOC)){ ?>
					<option value="<?=$rCliente{'eCodEntidad'}?>" <?=($rCliente{'eCodEntidad'}==$rSolicitud{'eCodCliente'} ? "selected='selected'" : "");?> ><?=utf8_encode($rCliente{'tNombre'})?></option>
				<?php } ?>
			</select>
        </td>
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Facturar a </td>
	    <td width="50%" nowrap class="sanLR04" id="tdFacturarA">
			<select name="eCodFacturarA" id="eCodFacturarA" style="width:175px">
				<option value="">Seleccione...</option>
				<?php while($rFacturarA=mysqli_fetch_array($rsFacturarA,MYSQLI_ASSOC)){ ?>
					<option value="<?=$rFacturarA{'eCodEntidad'}?>" <?=($rFacturarA{'eCodEntidad'}==$rSolicitud{'eCodFacturarA'} ? "selected='selected'" : "");?> ><?=utf8_encode($rFacturarA{'tNombre'})?></option>
				<?php } ?>
			</select>
        </td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> BL/Booking </td>
	    <td nowrap class="sanLR04" colspan="3"><?=($rSolicitud{'tBL'} ? utf8_encode($rSolicitud{'tBL'}) : "<input name=\"tBL\" type=\"text\" dojoType=\"dijit.form.TextBox\" id=\"tBL\" value=\"\" style=\"width:175px\" UpperCase=\"true\" onBlur=\"validarBooking();\">");?></td>
    </tr>
	<tr>
		<td height="23" nowrap class="sanLR04" valign="top" colspan="4">
			<?php
			$eTipoServicio	= (int)$rSolicitud['eCodTipoServicio'];
			$select=" SELECT rs.*, ctc.tNombreCorto AS TipoContenedor, ce.tNombre AS Embalaje ".
					" FROM opeentradasmercancias rs ".
					" INNER JOIN relsolicitudesserviciosmercanciassalidas rss ON rss.eCodEntrada= rs.eCodEntrada ".
					" LEFT JOIN cattiposcontenedores ctc ON ctc.eCodTipoContenedor=rs.eCodTipoContenedor ".
					" LEFT JOIN catembalajes ce ON ce.eCodEmbalaje=rs.eCodEmbalaje ".
					" WHERE rss.eCodSolicitud=".(int)$rSolicitud['eCodSolicitud'].($_GET['tCodContenedor']!="" ? " AND rs.tCodContenedor='".$_GET['tCodContenedor']."'" : "");
			$rsMercancias=mysqli_query($link,$select); ?>
			<table cellspacing="0" border="0" width="100%" id="tablaMercancias" name="tablaMercancias">
				<thead>
					<tr class="thEncabezado">
						<td nowrap="nowrap" class="sanLR04" height="23"> <?=($eTipoServicio==2 ? "Mercanc&iacute;a" : "Contenedor" );?></td>
						<td nowrap="nowrap" class="sanLR04"> <?=($eTipoServicio==1 ? "Tipo" : "Embalaje");?></td>
						<td nowrap="nowrap" class="sanLR04"> Observaciones</td>
						<td nowrap="nowrap" class="sanLR04" width="100%"></td>
					</tr>
				</thead>
				<tbody id="tbMercancias" name="tbMercancias">
					<?php $eFila=0; 
					if((int)mysqli_num_rows($rsMercancias)>0){
						while($rMercancia=mysqli_fetch_array($rsMercancias,MYSQLI_ASSOC)){ ?>
                            <tr id="filaMercancia1" name="filaMercancia1">
                                <td nowrap="nowrap" class="sanLR04" height="23"><?=($eTipoServicio==1 ? $rMercancia{'tCodContenedor'} : $rMercancia{'tMercancia'});?></td>
                                <td nowrap="nowrap" class="sanLR04"><?=utf8_encode($eTipoServicio==1 ? $rMercancia{'TipoContenedor'} : $rMercancia{'Embalaje'})?></td>
                                <td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rMercancia{'tObservaciones'});?></td>
                            </tr>
							<?php $eFila++; 
						}
					}else 
						$select=" SELECT * ".
								" FROM cattiposcontenedores ".
								" WHERE tCodEstatus='AC' ".
								" ORDER BY tNombre ";
						$rsTiposContenedores=mysqli_query($link,$select);
						$select=" SELECT * ".
								" FROM catembalajes ".
								" WHERE tCodEstatus='AC' ".
								" ORDER BY tNombre ";
						$rsEmbalajes=mysqli_query($link,$select); ?>
						<tr id="filaMercancia1" name="filaMercancia1">
							<td nowrap="nowrap" class="sanLR04" height="23">
								<input maxlength="15" name="tMercancia1" dojoType="dijit.form.TextBox" id="tMercancia1" style="width:165px" UpperCase="true" onBlur="obtenerEntrada();">
                                <input type="hidden" name="eCodEntrada1" id="eCodEntrada1">
							</td>
							<td nowrap="nowrap" class="sanLR04">
								<select name="eCodTipoContenedor1" id="eCodTipoContenedor1" style="width:175px">
									<option value="">Seleccione...</option>
									<?php while($rTipoContenedor=mysqli_fetch_array($rsTiposContenedores,MYSQLI_ASSOC)){ ?>
										<option value="<?=$rTipoContenedor{'eCodTipoContenedor'};?>"><?=utf8_encode($rTipoContenedor{'tNombreCorto'});?></option>
									<?php } ?>
								</select>
							</td>
							<td nowrap="nowrap" class="sanLR04">
								<input name="tObservaciones1" dojoType="dijit.form.TextBox" id="tObservaciones1" style="width:265px" maxlength="100" >
							</td>
						</tr>
					<?php  ?>
					<input name="eRegistros" id="eRegistros" value="<?=$eFila;?>" type="hidden">
				</tbody>
			</table>
		</td>
	</tr>
	<?php if(((int)$_GET['eCodSolicitud']>0 && $_GET['tCodContenedor']!="") || (int)$_GET['eCodSolicitud']==0){ 
		$select=" SELECT * FROM catvistascontenedores WHERE eCodVista IN(1,2,3) ";
		$rsVistas=mysqli_query($link,$select);
		$select=" SELECT * FROM catdanioscontenedores WHERE tCodEstatus='AC' ";
		$rsDanios = mysqli_query($link,$select); ?>
		<tr>
			<td height="10" nowrap class="sanLR04" colspan="4"></td>
		</tr>
        <tr>
			<td nowrap="nowrap" class="sanLR04 fntN11B" colspan="3" height="23"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> EIR</td>
			<td><input type="hidden" id="bEIR" name="bEIR" value="1" /></td>
		</tr>
        <tr>
			<td rowspan="2" height="23" valign="top" nowrap="nowrap" class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" width="16" height="16" align="absmiddle"> Inspecci&oacute;n</td>
			<td rowspan="2" valign="top"><table border="0" cellspacing="0" cellpadding="0" class="tbConsulta">
				<?php $i=0;
				while($rVista=mysqli_fetch_array($rsVistas,MYSQLI_ASSOC)){
					$select=" SELECT rvc.*, cc.tNombre AS Cara ".
							" FROM relvistascontenedorescarascontenedores rvc ".
							" INNER JOIN catcarascontenedores cc ON cc.eCodCara=rvc.eCodCara ".
							" WHERE rvc.eCodVista=".$rVista{'eCodVista'};
					$rsCaras = mysqli_query($link,$select);
					if($i) { ?>
						<tr>
							<td height="40" colspan="3" align="center"></td>
						</tr>
					<?php } ?>
					<tr class="thEncabezado">
						<td height="20" colspan="3" align="center"><?=($rVista{'tNombre'});?></td>
					</tr>
					<tr>
						<td nowrap="nowrap" ><span class="fntA11S"><?=$rVista{'tCostadoI'};?></span></td>
						<td align="center" nowrap="nowrap" ><span class="fntA11S"><?=$rVista{'tCostadoT'};?></span></td>
						<td height="20" align="right" nowrap="nowrap" >&nbsp;</td>
					</tr>
					<tr>
						<td height="20" colspan="3" align="center">    
						<div id="divImagen<?=$rVista{'eCodVista'};?>" style="width:<?=$rVista{'eAncho'};?>px;height:<?=$rVista{'eAlto'};?>px; position:relative; z-index:<?=$rVista{'eCodVista'};?>;">
						<div id="divVista<?=$rVista{'eCodVista'};?>" style="position:static;"></div>
						<img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/<?=$rVista{'tNombreCorto'};?>.png" width="<?=$rVista{'eAncho'};?>" height="<?=$rVista{'eAlto'};?>" id="img<?=$rVista{'eCodVista'};?>" usemap="#mapa<?=$rVista{'eCodVista'};?>" border="0" />
						<?php if(mysqli_num_rows($rsCaras)) { ?>
							<map name="mapa<?=$rVista{'eCodVista'};?>">
							<?php while ($rCara=mysqli_fetch_array($rsCaras,MYSQLI_ASSOC)) { ?>
								<area onclick="asignarCara(event,this,'<?=$rVista{'eCodVista'};?>','<?=$rCara{'eCodCara'};?>')" shape="poly" alt="<?=$rCara{'Cara'};?>" coords="<?=$rCara{'tCoordenadas'};?>" nohref="nohref" />
							<?php } ?>
							</map>
						<?php } ?>
						</div>
						</td>
					</tr>
					<tr>
						<td align="center" nowrap="nowrap">&nbsp;</td>
						<td align="center" nowrap="nowrap"><span class="fntA11S"><?=$rVista{'tCostadoB'};?></span></td>
						<td height="20" align="right" nowrap="nowrap"><span class="fntA11S"><?=$rVista{'tCostadoD'};?></span></td>
					</tr>
					<?php $i++;
				} ?>
			</table></td>
			<td valign="top" nowrap="nowrap" class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" width="16" height="16" align="absmiddle"> Da&ntilde;os</td>
			<td valign="top" nowrap="nowrap" class="sanLR04"><table border="0" cellspacing="0" cellpadding="0">
				<?php $i= 0;
				while($rDanio=mysqli_fetch_array($rsDanios,MYSQLI_ASSOC)){ ?>
					<tr>
						<td height="20" nowrap="nowrap"><label>
						<input type="radio" name="rdDanio" id="rdDanio" value="<?=trim($rDanio{'tCodDanio'});?>" <?=(!$i ? "checked=\"checked\"" : "")?> />
						<input type="hidden" name="D-<?=trim($rDanio{'tCodDanio'});?>" id="D-<?=trim($rDanio{'tCodDanio'});?>" value="<?=trim($rDanio{'eCodDanio'});?>" />
						<?="(".trim($rDanio{'tCodDanio'}).")";?> / <?=utf8_encode($rDanio{'tNombre'});?>
						</label></td>
					</tr>
					<?php $i++;
				} ?>
			</table></td>
		</tr>
		<tr>
			<td valign="top" nowrap="nowrap" class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" width="16" height="16" align="absmiddle"> Da&ntilde;os</td>
			<td valign="top" class="sanLR04" style="max-width:220px; min-width:220px"><div id="divDaniosRegistrados">Ninguno</div></td>
		</tr>
		<tr>
			<td height="10" nowrap class="sanLR04" colspan="4"></td>
		</tr>
	<?php } ?>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle" /> <font class="fntG10K">Indica que es obligatorio llenar el campo.
    </font></td>
    </tr>
</table>
</form>
<?php } ?>