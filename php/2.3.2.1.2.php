<?php require_once("conexion/soluciones-mysql.php");  
$link = getLink();
	
$select=" SELECT ce.*, cte.tNombre AS TipoEmpleado, cre.tNombre AS TipoCredito, 
		cat.Categoria, catf.Categoria AS CategoriaFestivo, cen.tNombre AS Entidad, 
		cen.tSiglas, catv.Categoria AS CategoriaVacacion, rac.tURLArchivo
		FROM empleados ce
		LEFT JOIN cattipoempleado cte ON cte.eCodTipoEntidad=ce.eCodTipo
		LEFT JOIN cattipocredito cre ON cre.eCodCredito=ce.eCodTipoCredito
		LEFT JOIN categorias cat ON cat.indice=ce.eCodCategoria
		LEFT JOIN categorias catf ON catf.indice=ce.eCodCategoriaFestivo
		LEFT JOIN categorias catv ON catv.indice=ce.eCodCategoriaVacacion
		LEFT JOIN catentidades cen ON cen.eCodEntidad=ce.eCodEntidad
		LEFT JOIN relarchivoscsf rac ON rac.eCodEmpleado=ce.id
		WHERE ce.id=".$_GET['eCodEntidad'];
$rEstado = mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

$select=" SELECT *
		FROM catdias
		WHERE eCodDia=".$rEstado['descanso'];
$rDias = mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

$select=" SELECT lee.fhFechaRegistro, cen.tNombre, cen.tSiglas, cus.tNombre AS Usuario, cus.tApellidos
		FROM logempleadosempresas lee
		INNER JOIN empleados emp ON emp.id=lee.eCodEmpleado
		INNER JOIN catentidades cen ON cen.eCodEntidad=lee.eCodEntidad
		LEFT JOIN catusuarios cus ON cus.eCodUsuario=lee.eCodUsuario
		WHERE lee.eCodEmpleado=".$_GET['eCodEntidad'];
$rsEdiciones=mysqli_query($link,$select); ?>
<script type="text/javascript">
function consultar(){
	document.location = './?ePagina=2.3.2.1.php';
}
function nuevo(){
	document.location = './?ePagina=2.3.2.1.1.php';
}
</script>
<div id="dvCNS" style="display:none;"></div>
<form action="" method="post" name="Datos" id="Datos" onsubmit="return false;">
<table border="0" cellpadding="0" cellspacing="0" width="980px">
    <tr><td height="20"></td></tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"> C&oacute;digo</td>
	    <td nowrap class="sanLR04"><b><?=sprintf("%07d",$rEstado{'id'});?></b></td>
	    <td nowrap class="sanLR04">Entidad</td>
	    <td nowrap class="sanLR04" colspan="3"><b><u><?=utf8_encode($rEstado{'Entidad'}." (".$rEstado{'tSiglas'}.")");?></u></b></td>
    </tr>
    <tr>
	    <td nowrap class="sanLR04">RFC</td>
	    <td width="33%" nowrap class="sanLR04"><b><?=utf8_encode($rEstado{'RFC'});?></b></td>
	    <td height="23" nowrap class="sanLR04"></td>
	    <td width="33%" nowrap class="sanLR04"></td>
	    <td nowrap class="sanLR04"></td>
	    <td width="33%" nowrap class="sanLR04"></td>
		
    </tr>
    <tr>
		<td height="23" nowrap class="sanLR04"> Nombre</td>
	    <td nowrap class="sanLR04" colspan="5"><b><?=utf8_encode($rEstado{'Empleado'});?></b></td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"> Tipo</td>
	    <td nowrap class="sanLR04"><b><?=utf8_encode($rEstado{'TipoEmpleado'});?></b></td>
		<td height="23" nowrap class="sanLR04"> NSS </td>
	    <td nowrap class="sanLR04"><b><?=utf8_encode($rEstado{'IMSS'});?></b></td>
	    <td height="23" nowrap class="sanLR04"> CURP</td>
	    <td nowrap class="sanLR04"><b><?=utf8_encode($rEstado{'CURP'});?></b></td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"> Clinica</td>
	    <td nowrap class="sanLR04"><b><?=utf8_encode(sprintf("%03d",$rEstado{'Clinica'}));?></b></td>
		<td height="23" nowrap class="sanLR04"> Registro</td>
	    <td nowrap class="sanLR04"><b><?=($rEstado{'FechaRegistro'} ? date("d/m/Y", strtotime($rEstado{'FechaRegistro'})) : "");?></b></td>
	    <td height="23" nowrap class="sanLR04"> Actualizaci&oacute;n</td>
	    <td nowrap class="sanLR04"><b><?=($rEstado{'fechaactualizacion'} ? date("d/m/Y", strtotime($rEstado{'fechaactualizacion'})) : "");?></b></td>
    </tr>	
	<hr>
    <tr>
	    <td height="23" nowrap class="sanLR04"> Dia de Descanso</td>
	    <td nowrap class="sanLR04"><b><?=utf8_encode($rDias{'tNombre'});?></b></td>
		<td height="23" nowrap class="sanLR04"> Tipo de Credito </td>
	    <td nowrap class="sanLR04"><b><?=utf8_encode($rEstado{'TipoCredito'});?></b></td>
	    <td height="23" nowrap class="sanLR04"> Calle </td>
		<td nowrap class="sanLR04"><b><?=utf8_encode($rEstado{'tCalle'});?></b></td>
	    <td nowrap class="sanLR04"><b></b></td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"> Número Interior</td>
	    <td nowrap class="sanLR04"><b><?=utf8_encode($rEstado{'eNumeroInt'});?></b></td>
		<td height="23" nowrap class="sanLR04"> Número Exterior </td>
	    <td nowrap class="sanLR04"><b><?=utf8_encode($rEstado{'eNumeroExt'});?></b></td>
	    <td height="23" nowrap class="sanLR04"> Código Postal </td>
		<td nowrap class="sanLR04"><b><?=utf8_encode($rEstado{'eCodigoPostal'});?></b></td>
	    <td nowrap class="sanLR04"><b></b></td>
    </tr>
	<tr>
		<td height="23" nowrap class="sanLR04"> Estado </td>
	    <td nowrap class="sanLR04"><b><?=utf8_encode($rEstado{'tEstado'});?></b></td>
		<td height="23" nowrap class="sanLR04"> Ciudad </td>
	    <td nowrap class="sanLR04"><b><?=utf8_encode($rEstado{'tCiudad'});?></b></td>
	    <td height="23" nowrap class="sanLR04"> Colonia </td>
		<td nowrap class="sanLR04"><b><?=utf8_encode($rEstado{'tColonia'});?></b></td>
	    <td nowrap class="sanLR04"><b></b></td>
    </tr>	
	<hr>
	<tr>
		<td height="23" nowrap class="sanLR04"> CSF </td>
		<td nowrap class="sanLR04"><strong><a href="<?=utf8_encode($rEstado{'tURLArchivo'});?>">Ver / descargar archivo</a></strong></td>
	</tr>
	<hr>
    <tr>
		
	    <td height="23" nowrap class="sanLR04"> Categoria</td>
	    <td nowrap class="sanLR04" colspan="2"><b><?=utf8_encode($rEstado{'Categoria'});?></b></td>
    </tr>
    <tr>
	    <td height="23" nowrap class="sanLR04"> Categoria Festivo</td>
	    <td nowrap class="sanLR04" colspan="2"><b><?=utf8_encode($rEstado{'CategoriaFestivo'});?></b></td>
    </tr>
    <tr>
	    <td height="23" nowrap class="sanLR04"> Categoria Vacaciones</td>
	    <td nowrap class="sanLR04" colspan="2"><b><?=utf8_encode($rEstado{'CategoriaVacacion'});?></b></td>
    </tr>
    <?php if((int)mysqli_num_rows($rsEdiciones)>0){ ?>
    	<tr>
		    <td height="23" nowrap class="sanLR04"> </td>
		    <td nowrap class="sanLR04"></td>
	    </tr>
	    <tr>
		    <td colspan="4" nowrap class="sanLR04">
		    	<table cellspacing="0" border="0" width="100%">
					<thead>
						<tr class="thEncabezado">
							<td nowrap="nowrap" class="sanLR04" height="20" align="center">#</td>
							<td nowrap="nowrap" class="sanLR04">F. Edici&oacute;n</td>                
							<td nowrap="nowrap" class="sanLR04">Entidad Anterior</td>
							<td nowrap="nowrap" class="sanLR04" width="100%">Usuario</td>
						</tr>
					</thead>
					<tbody>
						<?php $i=1; while($rEdicion=mysqli_fetch_array($rsEdiciones,MYSQLI_ASSOC)){ ?>
							<tr>
								<td nowrap="nowrap" class="sanLR04" height="20" align="center"><b><?=$i?></b></td>
			                    <td nowrap="nowrap" class="sanLR04 columnB"><?=date("d/m/Y H:i", strtotime($rEdicion{'fhFechaRegistro'}));?></td>
								<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rEdicion{'tNombre'}." (".$rEdicion{'tSiglas'}.")");?></td>
			                    <td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rEdicion{'Usuario'}." ".$rEdicion{'tApellidos'});?></td>
							</tr>
						<?php $i++; } ?>
					</tbody>
				</table>
		    </td>
	    </tr>
    <?php } ?>
</table>
</form>