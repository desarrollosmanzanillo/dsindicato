<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Log-In</title>
<!-- Bootstrap core CSS -->
<link href="dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Custom styles for this template -->
<link href="dist/css/carousel.css" rel="stylesheet">
<link href="dist/css/dashboard.css" rel="stylesheet">
<link href="dist/css/cover.css" rel="stylesheet">

</head>
<body>

<div class="container">
<!-- Carousel
		================================================== -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
    <li data-target="#myCarousel" data-slide-to="3"></li>
    <li data-target="#myCarousel" data-slide-to="4"></li>
  </ol>
  <div class="carousel-inner">
    <div class="item active"> <img src="img/1.jpg" alt="First slide">
      <div class="container">
        <div class="carousel-caption">
          <h1>Carga y Descarga de Contenedores LLenos o Vacios</h1>
        </div>
      </div>
    </div>
    <div class="item"> <img src="img/2.jpg" alt="Second slide">
      <div class="container">
        <div class="carousel-caption">
          <h1>Almacenaje de contenedores vacíos</h1>
        </div>
      </div>
    </div>
    <div class="item"> <img src="img/4.jpg" alt="Third slide">
      <div class="container">
        <div class="carousel-caption">
          <h1>Reparación de contenedores de acuerdo a la norma IICL Y UCIRC</h1>
        </div>
      </div>
    </div>
    <div class="item"> <img src="img/3.jpg" alt="Fourth slide">
      <div class="container">
        <div class="carousel-caption">
          <h1>Lavado de contenedores de acuerdo a la norma IICL Y UCIRC</h1>
        </div>
      </div>
    </div>
    <div class="item"> <img src="img/5.jpg" alt="fifth slide">
      <div class="container">
        <div class="carousel-caption">
          <h1>Desconsolidación y consolidación de contenedores</h1>
        </div>
      </div>
    </div>
  </div>
  <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a> 
  <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a> </div>

<!-- /.carousel -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="dist/js/bootstrap.min.js"></script>
<script src="dist/js/docs.min.js"></script>

</body>
</html>
