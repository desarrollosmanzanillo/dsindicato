<?php $eAdmin=($_SESSION['sesionUsuario']['bSindicato']==1  ? 1 : 0); ?>
<table cellspacing="0" border="0" width="900px">
	<tr><td width="100%" colspan="2"><hr width="99%" size="0" align="center" color="#CACACA"></td></tr>
	<tr><td height="20" colspan="2"></td></tr>
	<tr>
		<td height="20" width="50%">
			<table cellspacing="0" border="0" width="100%">
				<tbody>
					<tr>
						<td valign="top" width="128" class="sanLR04"><img width="128" height="128" alt="Empresa" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-empleados.png"></td>
						<td valign="top">
							<table cellspacing="0" border="0" width="100%">
								<tbody>
									<tr><td class="sanLR04"><a href='?ePagina=2.3.2.1.php'><label class="fonN12S">Empleados</label></a></td></tr>
									<tr><td class="sanLR04"><a href='?ePagina=2.3.2.7.php'><label class="fonN12S">Checador@s</label></a></td></tr>
									<tr><td height="2" align="left"><hr width="200" size="0" align="left" color="#666666"></td></tr>
									<tr><td class="sanLR04"><a href='?ePagina=2.3.2.3.php'><label class="fonN12S">Tipos de Empleados</label></a></td></tr>
									<tr><td class="sanLR04"><a href='?ePagina=2.3.2.5.php'><label class="fonN12S">Tipos de Creditos</label></a></td></tr>
									<tr><td class="sanLR04"><a href='?ePagina=2.3.2.6.php'><label class="fonN12S">Vacaciones</label></a></td></tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</td>
		<td width="50%">
			<table cellspacing="0" border="0" width="100%">
				<tbody>
					<tr>
						<td valign="top" width="128" class="sanLR04"><img width="128" height="128" alt="Servicios" title="Servicios" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-servicios.png"></td>
						<td valign="top">
							<table cellspacing="0" border="0" width="100%">
								<tbody>
									<tr><td class="sanLR04"><a href='?ePagina=2.3.3.1.php'><label class="fonN12S">Centro de Costos</label></a></td></tr>
									<tr><td class="sanLR04"><a href='?ePagina=2.3.3.2.php'><label class="fonN12S">Centro de Costos Checador@s</label></a></td></tr>
									<tr><td height="2" align="left"><hr width="200" size="0" align="left" color="#666666"></td></tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
	<tr><td height="20" colspan="2"></td></tr>
	<tr>
		<td height="20" width="50%">
			<table cellspacing="0" border="0" width="100%">
				<tbody>
					<tr>
						<td valign="top" width="128" class="sanLR04"><img width="128" height="128" alt="Buques" title="Buques" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-empresas.png"></td>
						<td valign="top">
							<table cellspacing="0" border="0" width="100%">
								<tbody>
									<tr><td class="sanLR04"><a href='?ePagina=2.3.2.2.php'><label class="fonN12S">Empresa</label></a></td></tr>
									<tr><td height="2" align="left"><hr width="200" size="0" align="left" color="#666666"></td></tr>
									<tr><td class="sanLR04"><a href='?ePagina=2.3.2.4.php'><label class="fonN12S">Dias Festivos</label></a></td></tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</td>
		<td width="50%">
        	<?php if((int)$eAdmin==1){ ?>
                <table cellspacing="0" border="0" width="100%">
                    <tbody>
                        <tr>
                            <td valign="top" width="128" class="sanLR04"><img width="128" height="128" alt="Finanzas" title="Finanzas" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-turnos.png"></td>
                            <td valign="top">
                                <table cellspacing="0" border="0" width="100%">
                                    <tbody>
                                        <tr><td class="sanLR04"><a href="?ePagina=2.3.6.1.php" style="cursor: pointer"><label class="fonN12S">Categorias y Turnos</label></a></td></tr>
                                        <tr><td height="2" align="left"><hr width="200" size="0" align="left" color="#666666"></td></tr>
                                        <tr><td class="sanLR04"><a href="?ePagina=2.3.6.2.php" style="cursor: pointer"><label class="fonN12S">Tipos de Bonos</label></a></td></tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
			<?php } ?>
        </td>
	</tr>
	<tr><td height="20" colspan="2"></td></tr>
</table>
