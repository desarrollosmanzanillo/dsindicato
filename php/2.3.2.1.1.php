<?php 

require_once("conexion/soluciones-mysql.php");  
require (dirname(__DIR__).'/vendor/autoload.php');
//print_r(dirname(__DIR__).'/vendor/autoload.php');
use Aws\S3\S3Client;
use Aws\Exception\AwsException;
$link = getLink();
if($_POST){
	if($_POST['eProceso']==1){
		$exito					= 0;
		$tRFC					= ($_POST['RFC']					? "'".trim(utf8_decode($_POST['RFC']))."'"				: "NULL");
		$tIMSS					= ($_POST['IMSS']					? "'".trim(utf8_decode($_POST['IMSS']))."'"				: "NULL");
		$tCURP					= ($_POST['CURP']					? "'".trim(utf8_decode($_POST['CURP']))."'"				: "NULL");
		$tNombre				= ($_POST['nombre']					? "'".trim(utf8_decode($_POST['nombre']))."'"			: "NULL");
		$tPaterno				= ($_POST['paterno']				? "'".trim(utf8_decode($_POST['paterno']))."'"			: "NULL");
		$tMaterno				= ($_POST['materno']				? "'".trim(utf8_decode($_POST['materno']))."'"			: "NULL");
		$tClinica				= ($_POST['clinica']				? "'".trim(utf8_decode($_POST['clinica']))."'"			: "NULL");
		$tCalle			    	= ($_POST['tCalle']			        ? "'".trim(utf8_decode($_POST['tCalle']))."'"			: "NULL");
		$eNumeroInt				= ($_POST['eNumeroInt']		        ? "'".trim(utf8_decode($_POST['eNumeroInt']))."'"		: "NULL");
		$eNumeroExt				= ($_POST['eNumeroExt']			    ? "'".trim(utf8_decode($_POST['eNumeroExt']))."'"		: "NULL");
		$eCodigoPostal			= ($_POST['eCodigoPostal']			? "'".trim(utf8_decode($_POST['eCodigoPostal']))."'"	: "NULL");
		$tEstado				= ($_POST['tEstado']			    ? "'".trim(utf8_decode($_POST['tEstado']))."'"			: "NULL");
		$tCiudad				= ($_POST['tCiudad']			    ? "'".trim(utf8_decode($_POST['tCiudad']))."'"			: "NULL");
		$tColonia				= ($_POST['tColonia']			    ? "'".trim(utf8_decode($_POST['tColonia']))."'"			: "NULL");
		$tURLArchivo			= ($_POST['tURLArchivo']			? "'".trim(utf8_decode($_POST['tURLArchivo']))."'"		: "NULL");
		$eCodEntidad			= ($_POST['eCodEntidad']			? (int)$_POST['eCodEntidad']							: "NULL");
		$eCodEmpresa			= ($_POST['eCodEmpresa']			? (int)$_POST['eCodEmpresa']							: "NULL");
		$eCodCategoria			= ($_POST['eCodCategoria']			? (int)$_POST['eCodCategoria']							: "NULL");
		$eCodTipoEmpleado		= ($_POST['eCodTipoEmpleado']		? (int)$_POST['eCodTipoEmpleado']						: "NULL");
		$eCodCategoriaFestivo 	= ($_POST['eCodCategoriaFestivo']	? (int)$_POST['eCodCategoriaFestivo']					: "NULL");
		$eCodCategoriaVacacion 	= ($_POST['eCodCategoriaVacacion']	? (int)$_POST['eCodCategoriaVacacion']					: "NULL");
		$eCodTipoCredito		= ($_POST['eCodTipoCredito']		? (int)$_POST['eCodTipoCredito']						: 1);
		$descanso           	= $_POST['descanso'];
		$tEmpleado="'".utf8_decode(trim($_POST['paterno']).(trim($_POST['materno']) ? " ".trim($_POST['materno']) : "")." ".trim($_POST['nombre']))."'";
		
		if((int)$eCodEntidad>0){
			$update=" UPDATE empleados ".
					" SET Paterno=".$tPaterno.",".
					" IMSS=".$tIMSS.",".
					" CURP=".$tCURP.",".
					" RFC=".$tRFC.",".
					" Nombre=".$tNombre.",".
					" Estado= 'AC' ,".
					" Materno=".$tMaterno.",".
					" Clinica=".$tClinica.",".
					" tCalle=".$tCalle.",".
					" eNumeroInt=".$eNumeroInt.",".
					" eNumeroExt=".$eNumeroExt.",".
					" eCodigoPostal=".$eCodigoPostal.",".
					" tEstado=".$tEstado.",".
					" tCiudad=".$tCiudad.",".
					" tColonia=".$tColonia.",".
					" descanso= ".$descanso." ,".					
					" Empleado=".$tEmpleado.",".
					" eCodTipo=".$eCodTipoEmpleado.",".
					" eCodEntidad=".$eCodEmpresa.",".
					" eCodCategoria=".$eCodCategoria.",".
					" eCodTipoCredito=".$eCodTipoCredito.",".
					" fechaactualizacion=current_timestamp,".
					" eCodCategoriaFestivo=".$eCodCategoriaFestivo.",".
					" eCodCategoriaVacacion=".$eCodCategoriaVacacion.
					" WHERE id=".$eCodEntidad;
					print ($update);
			if($res=mysqli_query($link,$update)){
				$insertcsf=" INSERT INTO relarchivoscsf (tURLArchivo, eCodEmpleado) ".
				" VALUES (".$tURLArchivo.", ".$eCodEntidad.")";
					print($insertcsf);
				$updtCSF=mysqli_query($link,$insertcsf);
				$exito=1;
			}else{
				$exito=0;
			}
		}else{
			$insert=" INSERT INTO empleados (Estado, Paterno, Materno, Nombre, Clinica, IMSS, CURP, RFC, eCodEntidad,
					eCodTipo, eCodTipoCredito, descanso, Empleado, fechaactualizacion, FechaRegistro, eCodCategoria, 
					eCodCategoriaFestivo, eCodCategoriaVacacion, tCalle, eNumeroInt, eNumeroExt, eCodigoPostal,
					tEstado, tCiudad, tColonia) ".
					" VALUES ('AC', ".$tPaterno.", ".$tMaterno.", ".$tNombre.", ".$tClinica.", ".$tIMSS.", ".$tCURP.", ".$tRFC.", ".$eCodEmpresa.",".
					$eCodTipoEmpleado.", ".$eCodTipoCredito.", ".$descanso." , ".$tEmpleado.", current_timestamp , current_timestamp, ".$eCodCategoria.", ".
					$eCodCategoriaFestivo.", ".$eCodCategoriaVacacion.", ".$tCalle.", ".$eNumeroInt.", ".$eNumeroExt.", ".$eCodigoPostal.", ".$tEstado.", ".
					$tCiudad.", ".$tColonia.")";
			if($res=mysqli_query($link,$insert)){
				$exito=1;
				$select=" select last_insert_id() AS Llave ";
				$rCodigo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
				$eCodEntidad=(int)$rCodigo['Llave'];
				$insertcsf=" INSERT INTO relarchivoscsf (tURLArchivo, eCodEmpleado) ".
							" VALUES (".$tURLArchivo.", ".$eCodEntidad.")";
				$CSF=mysqli_query($link,$insertcsf);
			}else{
				$exito=0;
			}
		}
		print "<input type=\"text\" value=\"".($exito==1 ? (int)$eCodEntidad : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}
	
	if($_POST['eProceso']==2){
		$rRFC=mysqli_fetch_array(mysqli_query($link,"SELECT 1 AS RFC FROM empleados WHERE RFC='".trim($_POST['RFC'])."' AND id!=".(int)$_POST['eCodEntidad']),MYSQLI_ASSOC);
		print "<input name=\"eEntidad\" id=\"eEntidad\" value=\"".(int)$rRFC{'RFC'}."\">";
	}
	
	if($_POST['eProceso']==3){
		$select=" SELECT * ".
				" FROM catestados ".
				" WHERE tCodEstatus='AC' ".
				" AND eCodPais=".$_POST['eCodPais'];
		$rsEstados=mysqli_query($link,$select); ?>
		<select name="eCodEstado" id="eCodEstado" style="width:175px" onchange="cargarCiudad();">
			<option value="">Seleccione...</option>
			<?php while($rEstado=mysqli_fetch_array($rsEstados,MYSQLI_ASSOC)){ ?>
				<option value="<?=$rEstado{'eCodEstado'}?>"><?=utf8_encode($rEstado{'tNombre'})?></option>
			<?php } ?>
		</select>
	<?php }
	
	if($_POST['eProceso']==4){
		$select=" SELECT * ".
				" FROM catciudades ".
				" WHERE tCodEstatus='AC' ".
				" AND eCodEstado=".$_POST['eCodEstado'];
		$rsCiudades=mysqli_query($link,$select); ?>
		<select name="eCodCiudad" id="eCodCiudad" style="width:175px">
			<option value="">Seleccione...</option>
			<?php while($rCiudad=mysqli_fetch_array($rsCiudades,MYSQLI_ASSOC)){ ?>
				<option value="<?=$rCiudad{'eCodCiudad'}?>"><?=utf8_encode($rCiudad{'tNombre'})?></option>
			<?php } ?>
		</select>
	<?php }

	if($_POST['eProceso']==5){
		$rRFC=mysqli_fetch_array(mysqli_query($link,"SELECT 1 AS RFC FROM empleados WHERE IMSS='".trim($_POST['IMSS'])."' AND id!=".(int)$_POST['eCodEntidad']),MYSQLI_ASSOC);
		print "<input name=\"eEntidad\" id=\"eEntidad\" value=\"".(int)$rRFC{'RFC'}."\">";
	}

	if($_POST['eProceso']==6){
		$rRFC=mysqli_fetch_array(mysqli_query($link,"SELECT 1 AS RFC FROM empleados WHERE CURP='".trim($_POST['CURP'])."' AND id!=".(int)$_POST['eCodEntidad']),MYSQLI_ASSOC);
		print "<input name=\"eEntidad\" id=\"eEntidad\" value=\"".(int)$rRFC{'RFC'}."\">";
	}

	if($_POST['eProceso']==7){
		$tDias="";
		$eDescanso=($_POST['descanso'] ? $_POST['descanso'] : ($_POST['eDes'] ? $_POST['eDes'] : 1));

		$select=" SELECT * ".
				" FROM catdias ".
				" WHERE ".((int)$_POST['eCodTipoEmpleado']==1 ? "bBase" : "bEventual")."=1";
		$rsDias=mysqli_query($link,$select);
		while($rDia=mysqli_fetch_array($rsDias,MYSQLI_ASSOC)){
			$tDias.="<label><input type=\"radio\" name=\"descanso\" id=\"descanso\" value=\"".$rDia{'eCodDia'}."\" ".($eDescanso==$rDia{'eCodDia'} || $rDia{'eCodDia'}==1 ? "checked" : "").">".$rDia{'tNombre'}."</label><br>";
		}
		print $tDias;
	}

	if($_POST['eProceso']==8){	
		header('Content-Type: text/html; charset=windows-1250');		
		$tRespuesta ="<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">";
		//$indice	= $_POST['eArchivoIndice'];		
		$tTMP	= $_FILES['tArchivo3']['tmp_name'];
		//print_r($tRespuesta);
		$s3Client = new S3Client([
			'region'      => 'us-west-2',
            'version'     => 'latest',
			'credentials' => [
			'key'         => 'AKIASE4UHMSFQMCDMPMX',
			'secret'      => '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'
			]
		]);
	
		$fileName = $_FILES['tArchivo3']['name'];
		//obtenemos la direccion de donde viene el archivo
		$fileTempName = $_FILES['tArchivo3']['tmp_name'];
		//obtenemos la extension del archivo
		$fileNameCmps = explode(".", $fileName);
		$fileExtension = strtolower(end($fileNameCmps));
		
		//convertimos con diferentes digitos el nombre del archivo y agregamos extension del mismo
		$newFileName = md5(time() . $fileName) . '.' . $fileExtension;
		
		//creamos array para el envio del archivo a s3 de amazon
		try {
			$result = $s3Client->putObject([
				'Bucket' => 'sindicato-ec2',
				'Key' => 'docs/'.$newFileName,
				'SourceFile' => $fileTempName
			]);
			$success = true;
			$tArchivo=($result->get('ObjectURL'));
			$tRespuesta.="<tr><td class=\"sanLR04\"><img width=\"16\" height=\"16\" src=\"https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-borrar.png\" style=\"cursor:pointer;\"></td>".
						 "<td class=\"sanLR04\" width=\"100%\"><a href=\"".utf8_encode($tArchivo)."\" target=\"_blank\" class=\"sanT12\" >".utf8_encode($newFileName)."</a>".
						 "<input type=\"hidden\" name=\"tURLArchivo\" id=\"tURLArchivo\" value=\"".utf8_encode($tArchivo)."\" /></td></tr>";
		} catch (AwsException $e) {
			$success = false;
			echo('Error '.$e->getMessage());
		}
		//$tNombreArchivo="CSF";
		//if(is_uploaded_file($tTMP)){
		//	$extension = pathinfo($_FILES['tArchivo3']['name'], PATHINFO_EXTENSION);
		//	$tArchivo = "/var/www/html/sindicato/archivos/2.4.5.1/(".date("Ymds").")".strtoupper(uniqid()).".".$extension;
		//	if(move_uploaded_file($tTMP, $tArchivo)){
		//		$tRespuesta.="<tr><td class=\"sanLR04\"><img width=\"16\" height=\"16\" src=\"https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-CA.png\" style=\"cursor:pointer;\"></td>".
		//				 "<td class=\"sanLR04\" width=\"100%\"><a href=\"Patio/".utf8_encode($tArchivo)."\" target=\"_blank\" class=\"sanT12\" >".utf8_encode($tNombreArchivo)."</a>".
		//				 "<input type=\"hidden\" name=\"tURLArchivo\" id=\"tURLArchivo\" value=\"".utf8_encode($tArchivo)."\" /></td></tr>";
		//	}else{
		//		$tRespuesta.="Fallo al subir el archivo";
		//	}	
		//}else{
		//	$tRespuesta.="Fallo al subir el archivo";
		//}
		$tRespuesta.="</table>";	
		print "<textarea>".$tRespuesta."</textarea>";
	}
}else{
	
$select=" SELECT * ".
		" FROM empleados ".
		" WHERE id=".$_GET['eCodEntidad'];
$rEntidad = mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC); 

$select=" SELECT * ".
		" FROM cattipoempleado ".
		" WHERE tCodEstatus='AC' ".
		" ORDER BY tNombre ASC ";
$rsTiposEntidades = mysqli_query($link,$select);

$select=" SELECT * ".
		" FROM cattipocredito ".
		" WHERE tCodEstatus='AC' ".
		" ORDER BY eCodCredito ASC ";
$rsTiposCredito = mysqli_query($link,$select);

$select=" SELECT * ".
		" FROM categorias ".
		" WHERE tCodEstatus='AC' ".
		" ORDER BY Categoria ASC ";
$rsCategorias = mysqli_query($link,$select); 

$select=" SELECT *
		FROM catentidades 
		WHERE tCodEstatus='AC' 
		ORDER BY tNombre ";
$rsEntidades=mysqli_query($link,$select); ?>
<script type="text/javascript">
dojo.require("dojo.io.iframe");
function guardar(){
	dojo.byId('eProceso').value=1;
	bandera = false;
	mensaje = "¡Verifique lo siguiente!\n";
	if (!dojo.byId("eCodEmpresa").value){
		mensaje+="* Entidad\n";
		bandera = true;		
	}
	if (!dojo.byId("paterno").value){
		mensaje+="* Paterno\n";
		bandera = true;		
	}
	if (!dojo.byId("nombre").value){
		mensaje+="* Nombre(s)\n";
		bandera = true;		
	}
	if (!dojo.byId("RFC").value){
		mensaje+="* RFC\n";
		bandera = true;		
	}
	if (!dojo.byId("eCodTipoEmpleado").value){
		mensaje+="* Tipo\n";
		bandera = true;		
	}
	if (!dojo.byId("IMSS").value){
		mensaje+="* NSS\n";
		bandera = true;		
	}
	if (!dojo.byId("clinica").value){
		mensaje+="* Clinica\n";
		bandera = true;		
	}
	if (!dojo.byId("CURP").value){
		mensaje+="* CURP\n";
		bandera = true;		
	}
	if (!dojo.byId("eCodCategoria").value){
		mensaje+="* Categoria\n";
		bandera = true;		
	}
	
	if (!dojo.byId("eCodigoPostal").value){
		mensaje+="* Codigo postal\n";
		bandera = true;
	}

	if (bandera==true){
		alert(mensaje);		
	}else{
		if (confirm("¿Desea Guardar la Información?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				if(dojo.byId("eCodigo").value>0){
					alert("¡La información se guardó correctamente!");
					tURL = './?ePagina=2.3.2.1.2.php&eCodEntidad='+dojo.byId("eCodigo").value;
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
		}
	}
}	

function verificarRFC(){
	if(dojo.byId('RFC').value){
		dojo.byId('eProceso').value=2;
		dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
			dojo.byId("dvCNS").innerHTML = tRespuesta;
			if(dojo.byId('eEntidad').value==1){
				alert("¡El RFC ya existe!");
				dojo.byId('RFC').value="";
			}
		}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
	}
}

function verificarIMSS(){
	if(dojo.byId('IMSS').value){
		dojo.byId('eProceso').value=5;
		dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
			dojo.byId("dvCNS").innerHTML = tRespuesta;
			if(dojo.byId('eEntidad').value==1){
				alert("¡El NSS ya existe!");
				dojo.byId('IMSS').value="";
			}
		}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
	}
}

function verificarCURP(){
	if(dojo.byId('CURP').value){
		dojo.byId('eProceso').value=6;
		dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
			dojo.byId("dvCNS").innerHTML = tRespuesta;
			if(dojo.byId('eEntidad').value==1){
				alert("¡El CURP ya existe!");
				dojo.byId('CURP').value="";
			}
		}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
	}
}

function vDescanso(){
	if(dojo.byId('eCodTipoEmpleado').value){
		dojo.byId('eProceso').value=7;
		dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
			dojo.byId("tdDescanso").innerHTML = tRespuesta;
		}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});
	}
}

function consultar(){
	document.location = './?ePagina=2.3.2.1.php';
}

function subirArchivo(){
	//dojo.byId('eArchivoIndice').value = ;
	if(dojo.byId("tArchivo3").value){
		dojo.byId("eProceso").value = 8;
		dojo.io.iframe.send({url: "php/"+dojo.byId('ePagina').value+".php", method: "post", handleAs: "text", form: 'Datos', handle: function(tRespuesta, ioArgs){
			dojo.byId("divArchivo3").innerHTML = tRespuesta;
		}});
	}
}

dojo.addOnLoad(function(){ vDescanso();});
</script>
<div id="dvCNS" style="display:none;"></div>
<form  method="post" name="Datos" id="Datos" onsubmit="return false;" enctype="multipart/form-data">
<input type="hidden" name="eProceso" id="eProceso" value="" />
<input type="hidden" name="eCodEntidad" id="eCodEntidad" value="<?=$_GET['eCodEntidad'];?>" />
<input type="hidden" name="eDes" id="eDes" value="<?=$rEntidad{'descanso'};?>" />
<input type="hidden" name="eArchivoIndice" id="eArchivoIndice" value="" />
<table border="0" cellpadding="0" cellspacing="0">
    <tr><td height="20"></td></tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Empresa </td>
	    <td width="33%" nowrap class="sanLR04" colspan="5">
	    	<select name="eCodEmpresa" id="eCodEmpresa" style="width:510px">
                <option value=''>Seleccione</option>
                <?php while($sEntidad = mysqli_fetch_array($rsEntidades,MYSQLI_ASSOC)){ ?>
                	<option value='<?=$sEntidad["eCodEntidad"];?>' <?=($rEntidad{'eCodEntidad'}==$sEntidad{'eCodEntidad'} ? "selected='selected'" : "");?>> <?=$sEntidad["tNombre"]." (".$sEntidad["tSiglas"].")";?> </option>
                <?php } ?>
            </select>
	    </td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Paterno </td>
	    <td width="33%" nowrap class="sanLR04"><input name="paterno" type="text" dojoType="dijit.form.TextBox" id="paterno" value="<?=utf8_encode($rEntidad{'Paterno'});?>" style="width:175px">        </td>
		<td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Materno </td>
	    <td width="33%" nowrap class="sanLR04"><input name="materno" type="text" dojoType="dijit.form.TextBox" id="materno" value="<?=utf8_encode($rEntidad{'Materno'});?>" style="width:175px;">        </td>
        <td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Nombre(s) </td>
        <td width="33%" nowrap class="sanLR04"><input name="nombre" type="text" dojoType="dijit.form.TextBox" id="nombre" value="<?=utf8_encode($rEntidad{'Nombre'});?>" style="width:175px;">
        </td>
    </tr>
    <tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> RFC </td>
	    <td width="33%" nowrap class="sanLR04"><input name="RFC" type="text" dojoType="dijit.form.TextBox" id="RFC" value="<?=$rEntidad{'RFC'};?>" style="width:175px" onblur="verificarRFC();"></td>
		<td class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Tipo </td>
	    <td width="33%" class="sanLR04">
			<select name="eCodTipoEmpleado" id="eCodTipoEmpleado" style="width:175px" onchange="vDescanso();">
				<option value="">Seleccione...</option>
				<?php while($rTipoEntidad=mysqli_fetch_array($rsTiposEntidades,MYSQLI_ASSOC)){ ?>
					<option value="<?=$rTipoEntidad{'eCodTipoEntidad'}?>" <?=($rTipoEntidad{'eCodTipoEntidad'}==$rEntidad{'eCodTipo'} ? "selected='selected'" : "");?> ><?=utf8_encode($rTipoEntidad{'tNombre'})?></option>
				<?php } ?>
			</select>
		</td>
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Clinica</td>	    
        <td width="33%" nowrap class="sanLR04"><input name="clinica" type="text" dojoType="dijit.form.TextBox" id="clinica" value="<?=utf8_encode(sprintf("%03d",$rEntidad{'Clinica'}));?>" style="width:175px;">
        </td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> NSS </td>
	    <td width="33%" nowrap class="sanLR04"><input name="IMSS" type="text" dojoType="dijit.form.TextBox" id="IMSS" value="<?=utf8_encode($rEntidad{'IMSS'});?>" style="width:175px;" onblur="verificarIMSS();"></td>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> CURP </td>
	    <td width="33%" nowrap class="sanLR04"><input name="CURP" type="text" dojoType="dijit.form.TextBox" id="CURP" value="<?=utf8_encode($rEntidad{'CURP'});?>" style="width:175px;" onblur="verificarCURP();"></td>
	    <td class="sanLR04"><!--img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Cr&eacute;dito --></td>
	    <td width="33%" class="sanLR04">
			<!--select name="eCodTipoCredito" id="eCodTipoCredito" style="width:175px">
				< ?php while($rTipoCredito=mysql_fetch_array($rsTiposCredito)){ ?>
					<option value="< ?=$rTipoCredito{'eCodCredito'}?>" < ?=($rTipoCredito{'eCodCredito'}==$rEntidad{'eCodTipoCredito'} ? "selected='selected'" : "");?> >< ?=utf8_encode($rTipoCredito{'tNombre'})?></option>
				< ?php } ?>
			</select-->
		</td>	
    </tr>
    <tr>
	    <td class="sanLR04" nowrap><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Categoria </td>
	    <td width="33%" class="sanLR04">
			<select name="eCodCategoria" id="eCodCategoria" style="width:175px">				
				<option value="" >Seleccione...</option>
				<?php while($rCategoria=mysqli_fetch_array($rsCategorias,MYSQLI_ASSOC)){ ?>
					<option value="<?=$rCategoria{'indice'}?>" <?=($rCategoria{'indice'}==$rEntidad{'eCodCategoria'} ? "selected='selected'" : "");?> ><?=utf8_encode($rCategoria{'Categoria'})?></option>
				<?php } ?>
			</select>
		</td>
		<td class="sanLR04" nowrap><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Categoria Festivo</td>
	    <td width="33%" class="sanLR04">
			<select name="eCodCategoriaFestivo" id="eCodCategoriaFestivo" style="width:175px">				
				<option value="" >Seleccione...</option>
				<?php 
				mysqli_data_seek($rsCategorias,0);
				while($rCategoria=mysqli_fetch_array($rsCategorias,MYSQLI_ASSOC)){ ?>
					<option value="<?=$rCategoria{'indice'}?>" <?=($rCategoria{'indice'}==$rEntidad{'eCodCategoriaFestivo'} ? "selected='selected'" : "");?> ><?=utf8_encode($rCategoria{'Categoria'})?></option>
				<?php } ?>
			</select>
		</td>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" align="absmiddle"> Categoria Vacaciones</td>
	    <td width="33%" nowrap class="sanLR04">
	    	<select name="eCodCategoriaVacacion" id="eCodCategoriaVacacion" style="width:175px">				
				<option value="" >Seleccione...</option>
				<?php 
				mysqli_data_seek($rsCategorias,0);
				while($rCategoria=mysqli_fetch_array($rsCategorias,MYSQLI_ASSOC)){ ?>
					<option value="<?=$rCategoria{'indice'}?>" <?=($rCategoria{'indice'}==$rEntidad{'eCodCategoriaVacacion'} ? "selected='selected'" : "");?> ><?=utf8_encode($rCategoria{'Categoria'})?></option>
				<?php } ?>
			</select>
	    </td>
    </tr>
	<tr>
		<td height="23" nowrap class="sanLR04"> Calle </td>
		<td width="33%" nowrap class="sanLR04"><input name="tCalle" type="text" dojoType="dijit.form.TextBox" id="tCalle" value="<?=utf8_encode($rEntidad{'tCalle'});?>" style="width:175px;"></td>
		<td height="23" nowrap class="sanLR04"> Número interior </td>
		<td width="33%" nowrap class="sanLR04"><input name="eNumeroInt" type="number" dojoType="dijit.form.TextBox" id="eNumeroInt" value="<?=utf8_encode($rEntidad{'eNumeroInt'});?>" style="width:175px;"></td>
		<td height="23" nowrap class="sanLR04"> Número exterior </td>
		<td width="33%" nowrap class="sanLR04"><input name="eNumeroExt" type="number" dojoType="dijit.form.TextBox" id="eNumeroExt" value="<?=utf8_encode($rEntidad{'eNumeroExt'});?>" style="width:175px;"></td>
	</tr>
	<tr>
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Código postal </td>
		<td width="33%" nowrap class="sanLR04"><input name="eCodigoPostal" type="number" dojoType="dijit.form.TextBox" id="eCodigoPostal" value="<?=utf8_encode($rEntidad{'eCodigoPostal'});?>" style="width:175px;"></td>
		<td height="23" nowrap class="sanLR04"> Estado </td>
		<td width="33%" nowrap class="sanLR04"><input name="tEstado" type="text" dojoType="dijit.form.TextBox" id="tEstado" value="<?=utf8_encode($rEntidad{'tEstado'});?>" style="width:175px;"></td>
		<td height="23" nowrap class="sanLR04"> Ciudad </td>
		<td width="33%" nowrap class="sanLR04"><input name="tCiudad" type="text" dojoType="dijit.form.TextBox" id="tCiudad" value="<?=utf8_encode($rEntidad{'tCiudad'});?>" style="width:175px;"></td>
	</tr>
	<tr>
		<td height="23" nowrap class="sanLR04"> Colonia </td>
		<td width="33%" nowrap class="sanLR04"><input name="tColonia" type="text" dojoType="dijit.form.TextBox" id="tColonia" value="<?=utf8_encode($rEntidad{'tColonia'});?>" style="width:175px;"></td>
	</tr>
	<tr>
		<td height="23" nowrap class="sanLR04"> Archivo CSF </td>
		<td width="33%" nowrap class="sanLR04" colspan="2">
			<input id="tArchivo3" name="tArchivo3" type="file" class="uploadBtn" onkeyup="this.value=''" style="width:325px" onchange="subirArchivo();" />
		</td>
		<td width="33%" nowrap class="sanLR04" colspan="2">
			<div id="divArchivo3"></div>
		</td>
	</tr>
    <tr><td height="10"></td>
    </tr>    
    <tr>
	    <td height="23" nowrap class="sanLR04" valign="top"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Descanso</td>
	    <td width="33%" nowrap class="sanLR04" id="tdDescanso">
	    	<!--label><input type="radio" name="descanso" id="descanso" value="1" < ?=($rEntidad{'descanso'}==1 ? "checked" : "")? > > Domingo</label><br>
  			<label><input type="radio" name="descanso" id="descanso" value="2" < ?=($rEntidad{'descanso'}==2 ? "checked" : "")? > > Lunes</label><br>
  			<label><input type="radio" name="descanso" id="descanso" value="3" < ?=($rEntidad{'descanso'}==3 ? "checked" : "")? > > Martes</label><br>
  			<label><input type="radio" name="descanso" id="descanso" value="4" < ?=($rEntidad{'descanso'}==4 ? "checked" : "")? > > Miercoles</label><br>
  			<label><input type="radio" name="descanso" id="descanso" value="5" < ?=($rEntidad{'descanso'}==5 ? "checked" : "")? > > Jueves</label><br>
  			<label><input type="radio" name="descanso" id="descanso" value="6" < ?=($rEntidad{'descanso'}==6 ? "checked" : "")? > > Viernes</label><br>
  			<label><input type="radio" name="descanso" id="descanso" value="7" < ?=($rEntidad{'descanso'}==7 ? "checked" : "")? > > Sabado</label><br-->
  		</td>
	    <td height="23" nowrap class="sanLR04"></td>
	    <td width="33%" nowrap class="sanLR04"></td>
	    <td height="23" nowrap class="sanLR04"></td>
	    <td width="33%" nowrap class="sanLR04"></td>		
    </tr>
	
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle" /> <font class="fntG10K">Indica que es obligatorio llenar el campo.
    </font></td>
    </tr>
</table>
</form>
<?php } ?>
