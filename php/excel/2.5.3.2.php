<?php  require_once("excel/phpexcel.php");
require_once("../conexion/soluciones-mysql.php");
$link = getLink();
if ($_GET['fhFechaAsistenciaInicio']!=""){
	$fhFechaAsistenciaInicio = $_GET['fhFechaAsistenciaInicio'].' 00:00:00';
	$fhFechaAsistenciaFin = ($_GET['fhFechaAsistenciaFin']!="" ? $_GET['fhFechaAsistenciaFin'] : $_GET['fhFechaAsistenciaInicio']).' 23:59:59';
}
$select=" SELECT DISTINCT asis.idEmpleado AS numeral, mov.eCodEstatus AS Estado, mov.tCodTipoMovimiento,
		mov.fhFecha AS Fecha, asis.idEmpleado, mov.dImporte AS Salario, mov.tFolio, ent.tSiglas
		FROM asistencias asis
		INNER JOIN relasistenciamovimientos ram ON ram.eCodAsistencia=asis.id
		INNER JOIN movimientosafiliatorios mov ON mov.eCodMovimiento=ram.eCodMovimiento
		LEFT JOIN catentidades ent ON ent.eCodEntidad=asis.idEmpresa
		WHERE asis.Estado='AC' AND mov.tFolio IS NOT NULL AND mov.tCodTipoMovimiento NOT IN ('01','02') ".
		($_GET['eCodEntidad']				? " AND asis.idEmpresa=".$_GET['eCodEntidad'] : "").
		($_GET['tFolio']					? " AND mov.tFolio='".$_GET['tFolio']."'" : "").
		($_GET['eCodIncidencia']			? " AND asis.idEmpleado=".$_GET['eCodIncidencia'] : "").
		($_GET['fhFechaAsistenciaInicio']	? " AND mov.fhFecha BETWEEN '".$fhFechaAsistenciaInicio. "' AND '" .$fhFechaAsistenciaFin."'" : "" ).
		" Order by mov.fhFecha ".($_GET['orden'] ? $_GET['orden'] : "DESC").", mov.tCodTipoMovimiento DESC ".
		" LIMIT ".($_GET['limite'] ? $_GET['limite'] : "100");
$rsMovimientos=mysqli_query($link,$select); 
if($_GET['eAccion']==999){
	print $select."<br>";
}
//
$select=" SELECT DISTINCT asis.idEmpleado AS numeral, mov.eCodEstatus AS Estado, mov.tCodTipoMovimiento,
		mov.fhFecha AS Fecha, asis.idEmpleado, mov.dImporte AS Salario, mov.tFolio
		FROM asistencias asis
		INNER JOIN relasistenciamovimientos ram ON ram.eCodAsistencia=asis.id
		INNER JOIN movimientosafiliatorios mov ON mov.eCodMovimiento=ram.eCodMovimiento
		WHERE asis.Estado='AC' AND mov.tFolio IS NOT NULL AND mov.tCodTipoMovimiento='02' ".
		($_GET['eCodEntidad']				? " AND asis.idEmpresa=".$_GET['eCodEntidad'] : "").
		($_GET['tFolio']					? " AND mov.tFolio='".$_GET['tFolio']."'" : "").
		($_GET['eCodIncidencia']			? " AND asis.idEmpleado=".$_GET['eCodIncidencia'] : "").
		($_GET['fhFechaAsistenciaInicio']	? " AND mov.fhFecha BETWEEN '".$fhFechaAsistenciaInicio. "' AND '" .$fhFechaAsistenciaFin."'" : "" ).
		" Order by mov.fhFecha ".($_GET['orden'] ? $_GET['orden'] : "DESC").
		" LIMIT ".($_GET['limite'] ? $_GET['limite'] : "100");
$rsBajas=mysqli_query($link,$select);
if($_GET['eAccion']==999){
	print $select."<br>";
	dd();
}

// Create new PHPExcel object
$excel = new PHPExcel();

// Valores a Utilizar
$titulo = "Movimientos Afiliatorios Checadores";
$columna = 'A';
$fila = 5;

// Arreglo con titulos columnas
$titulos = array(
	"Código",
	"Movimiento",
	"Fecha",
	"Diario",
	"Variable",
	"Integrado"
);

// Ponemos las propiedades del documento
$excel->getProperties()->setCreator("Sindicato")
	  ->setLastModifiedBy("Sindicato")
	  ->setTitle("Sindicato")
	  ->setSubject("Sindicato")
	  ->setDescription("Sindicato")
	  ->setKeywords("Sindicato")
	  ->setCategory("Sindicato");

$select=" SELECT * ".
		" FROM catentidades WHERE eCodEntidad=1";		
$rEmpresa = mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
// Encabezados del documento
$excel->getActiveSheet()->setCellValue("A1", utf8_encode($rEmpresa{'tNombre'}));
$excel->getActiveSheet()->setCellValue("A2", utf8_encode("Fecha del Reporte: ".date("d/m/Y H:i:s")));
$excel->getActiveSheet()->setCellValue("A3", $titulo);

// Arreglo con formato para los titulos columnas
$frEnc = new PHPExcel_Style();
$frTit = new PHPExcel_Style();
$frSep = new PHPExcel_Style();

$frEnc->applyFromArray(array('fill'	=> array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color'	=> array('rgb' => 'd6d6d6')),'font' => array('bold'=> true)));
$frTit->applyFromArray(array('fill' => array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color'	=> array('rgb' => 'FFD200')),'font'	=> array('color'=> array('rgb' => 'FFFFFF'))));
$frSep->applyFromArray(array('fill' => array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color'	=> array('rgb' => 'FFFFFF'))));

$numTitulos = sizeof($titulos);

// Ajuste de anchos de columnas
for($z=1;$z<=$numTitulos;$z++){
	// Ponemos el ancho de la columna
	//$excel->getActiveSheet()->getColumnDimension($columna)->setWidth(14);
	$excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);

	// Pintamos el titulo
	$celda = $columna."".$fila;
	$excel->setActiveSheetIndex(0)->setCellValue($celda, $titulos[$z-1]);
	
	if($z < $numTitulos){
		$columna++;
	}
}

// Formato de celdas
$excel->getActiveSheet()->setSharedStyle($frEnc, "A".$fila.":".$columna.$fila);

// Agrupamos las celdas de los encabezados
for($i=1; $i<=3; $i++){
	$excel->getActiveSheet()->setSharedStyle($frEnc, "A".$i);
	$excel->getActiveSheet()->mergeCells('A'.$i.':'.$columna.''.$i);
}
	
$excel->getActiveSheet()->setSharedStyle($frTit, "A3");
$excel->getActiveSheet()->setSharedStyle($frSep, "A4");
$excel->getActiveSheet()->setSharedStyle($frEnc, "A5");
$excel->getActiveSheet()->getRowDimension('4')->setRowHeight(5);

// Ajustamos formatos
$excel->getActiveSheet()->getStyle("A1")->getFont()->setSize(14);
$excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);

$excel->getActiveSheet()->getStyle("A3")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$excel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true);

while($rSolicitud=mysqli_fetch_array($rsMovimientos,MYSQLI_ASSOC)){
	// aumentamos primero porque estabamos en fila de encabezados
	$fila++;
	$columna = 'A';	
	$excel->getActiveSheet()->setCellValue($columna.$fila, sprintf("%05d",$rSolicitud{'idEmpleado'}))->getStyle($columna++.$fila)->getNumberFormat()->setFormatCode('00000');
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'tCodTipoMovimiento'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, date("d/m/Y", strtotime($rSolicitud{'Fecha'})));	
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'Salario'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode("0.00"));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'Salario'}));
}
while($rSolicitud2=mysqli_fetch_array($rsBajas,MYSQLI_ASSOC)){
	// aumentamos primero porque estabamos en fila de encabezados
	$fila++;
	$columna = 'A';	
	$excel->getActiveSheet()->setCellValue($columna.$fila, sprintf("%05d",$rSolicitud2{'idEmpleado'}))->getStyle($columna++.$fila)->getNumberFormat()->setFormatCode('00000');
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode("02"));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, date("d/m/Y", strtotime($rSolicitud2{'Fecha'})));	
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode("0.00"));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode("0.00"));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode("0.00"));
}
// Cambiamos el nombre de la hoja
$excel->getActiveSheet()->setTitle('Sindicato');

// Ocultamos las lineas de division
$excel->getActiveSheet()->setShowGridlines(false);

// Asignamos la primer hoja como la activa
$excel->setActiveSheetIndex(0);

// Redirect output to a client's web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename=Movimientos_Afiliatorios.xls');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
$objWriter->save('php://output');
exit;
?>