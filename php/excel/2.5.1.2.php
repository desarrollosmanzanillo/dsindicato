<?php 
require_once("excel/phpexcel.php");
require_once("../conexion/soluciones-mysql.php");		
$link = getLink();				

	if ($_GET['fhFechaAsistenciaInicio']!=""){
  		$fhFechaAsistenciaInicio = $_GET['fhFechaAsistenciaInicio'].' 00:00:00';
  		$fhFechaAsistenciaFin = ($_GET['fhFechaAsistenciaFin']!="" ? $_GET['fhFechaAsistenciaFin'] : $_GET['fhFechaAsistenciaInicio']).' 23:59:59';
 	}
	$select=" SELECT mov.* , emp.Empleado AS empleado, ".			
			" tpo.eCodTipoEntidad,  tpo.tNombreCorto, usr.tNombre , usr.tApellidos ,  tmo.tNombre AS Movimiento  ".
			" FROM movimientosafiliatorios AS mov  ".
			" INNER JOIN relasistenciamovimientos AS rel ON rel.eCodMovimiento=mov.eCodMovimiento ".
			" LEFT JOIN cattipomovimientos AS tmo ON tmo.eCodMovimiento=mov.tCodTipoMovimiento ".
			" LEFT JOIN catusuarios AS usr ON usr.eCodUsuario=mov.eCodUsuario ".
			" LEFT JOIN asistencias AS ast ON rel.eCodAsistencia=ast.id	".			
			" LEFT JOIN empleados emp ON emp.id=ast.idEmpleado ".
			" LEFT JOIN cattipoempleado tpo ON emp.eCodTipo=tpo.eCodTipoEntidad ".			
			" WHERE 1=1 ".
			($_GET['eCodMovimiento'] 			? " AND mov.eCodMovimiento=".$_GET['eCodMovimiento'] 				: "").
			($_GET['folio'] 					? " AND mov.tFolio=".$_GET['folio'] 				: "").
			($_GET['Empleado'] 				? " AND emp.Empleado like '%".$_GET['Empleado']."%'" 	: "").
			($_GET['fhFechaAsistenciaInicio']	? " AND mov.fhFecha BETWEEN '".$fhFechaAsistenciaInicio. "' AND '" .$fhFechaAsistenciaFin."'" : "" ).			
			($_GET['tipoMovimiento'] 			? " AND mov.tCodTipoMovimiento=".$_GET['tipoMovimiento'] 	: "").			
			" Order by mov.fhFecha  ".( $_GET['orden'] ? $_GET['orden']  : "DESC" )." LIMIT ".( $_GET['limite'] ? $_GET['limite']  : "1000" );			

$rsSolicitudes=mysqli_query($link,$select);

$rsSolicitudes=mysqli_query($link,$select);
$rsUsuarios=mysqli_query($link,$select);
$registros=(int)mysqli_num_rows($rsUsuarios);

// Create new PHPExcel object
$excel = new PHPExcel();

// Valores a Utilizar
$titulo = "Reporte de Movimientos Afiliatorios";
$columna = 'A';
$fila = 5;

// Arreglo con titulos columnas
$titulos = array(
	"Código",
	"Fecha",
	"S.D.I.",
	"Empleado",
	"T. Empleado",
	"Folio",
	"Movimiento",
	"Usuario",
	"F. Registro"	
);

// Ponemos las propiedades del documento
$excel->getProperties()->setCreator("Sindicato - Mzomicrosystems.com")
	  ->setLastModifiedBy("Sindicato - Mzomicrosystems.com")
	  ->setTitle("Sindicato - Mzomicrosystems.com")
	  ->setSubject("Sindicato - Mzomicrosystems.com")
	  ->setDescription("Sindicato - Mzomicrosystems.com")
	  ->setKeywords("Sindicato - Mzomicrosystems.com")
	  ->setCategory("Sindicato - Mzomicrosystems.com");

$select=" SELECT * ".
		" FROM catentidades WHERE eCodEntidad=1";		
$rEmpresa = mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
// Encabezados del documento
$excel->getActiveSheet()->setCellValue("A1", utf8_encode($rEmpresa{'tNombre'}));
$excel->getActiveSheet()->setCellValue("A2", utf8_encode("Fecha del Reporte: ".date("d/m/Y H:i:s")));
$excel->getActiveSheet()->setCellValue("A3", $titulo);

// Arreglo con formato para los titulos columnas
$frEnc = new PHPExcel_Style();
$frTit = new PHPExcel_Style();
$frSep = new PHPExcel_Style();

$frEnc->applyFromArray(array('fill'	=> array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color'	=> array('rgb' => 'd6d6d6')),'font' => array('bold'=> true)));
$frTit->applyFromArray(array('fill' => array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color'	=> array('rgb' => 'FFD200')),'font'	=> array('color'=> array('rgb' => 'FFFFFF'))));
$frSep->applyFromArray(array('fill' => array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color'	=> array('rgb' => 'FFFFFF'))));

$numTitulos = sizeof($titulos);

// Ajuste de anchos de columnas
for($z=1;$z<=$numTitulos;$z++){
	// Ponemos el ancho de la columna
	//$excel->getActiveSheet()->getColumnDimension($columna)->setWidth(14);
	$excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);

	// Pintamos el titulo
	$celda = $columna."".$fila;
	$excel->setActiveSheetIndex(0)->setCellValue($celda, $titulos[$z-1]);
	
	if($z < $numTitulos){
		$columna++;
	}
}

// Formato de celdas
$excel->getActiveSheet()->setSharedStyle($frEnc, "A".$fila.":".$columna.$fila);

// Agrupamos las celdas de los encabezados
for($i=1; $i<=3; $i++){
	$excel->getActiveSheet()->setSharedStyle($frEnc, "A".$i);
	$excel->getActiveSheet()->mergeCells('A'.$i.':'.$columna.''.$i);
}
	
$excel->getActiveSheet()->setSharedStyle($frTit, "A3");
$excel->getActiveSheet()->setSharedStyle($frSep, "A4");
$excel->getActiveSheet()->setSharedStyle($frEnc, "A5");
$excel->getActiveSheet()->getRowDimension('4')->setRowHeight(5);

// Ajustamos formatos
$excel->getActiveSheet()->getStyle("A1")->getFont()->setSize(14);
$excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);

$excel->getActiveSheet()->getStyle("A3")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$excel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true);

while($rSolicitud=mysqli_fetch_array($rsSolicitudes,MYSQLI_ASSOC)){
	// aumentamos primero porque estabamos en fila de encabezados
	
	$fila++;
	$columna = 'A';
	$excel->getActiveSheet()->setCellValue($columna++.$fila, sprintf("%07d",$rSolicitud{'eCodMovimiento'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode(date("d/m/Y", strtotime($rSolicitud{'fhFecha'}))));	
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'dImporte'}));	
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'empleado'}));	
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'tNombreCorto'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'tFolio'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'Movimiento'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'tNombre'}." ".$rSolicitud{'tApellidos'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode(date("d/m/Y h:i:s", strtotime($rSolicitud{'fhFechaRegistro'}))));
}
// Cambiamos el nombre de la hoja
$excel->getActiveSheet()->setTitle('Sindicato - Mzomicrosystems.com');

// Ocultamos las lineas de division
$excel->getActiveSheet()->setShowGridlines(false);

// Asignamos la primer hoja como la activa
$excel->setActiveSheetIndex(0);

// Redirect output to a client's web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename=Reporte_Movimientos_Afiliatorios.xls');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
$objWriter->save('php://output');
exit;
?>