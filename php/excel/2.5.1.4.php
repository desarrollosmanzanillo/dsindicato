<?php 
require_once("excel/phpexcel.php");
require_once("../conexion/soluciones-mysql.php");
$link = getLink();

if ($_GET['fhFechaAsistenciaInicio']!=""){
  		$fhFechaAsistenciaInicio = $_GET['fhFechaAsistenciaInicio'].' 00:00:00';
  		$fhFechaAsistenciaFin = ($_GET['fhFechaAsistenciaFin']!="" ? $_GET['fhFechaAsistenciaFin'] : $_GET['fhFechaAsistenciaInicio']).' 23:59:59';
 	}
 	$select=" SELECT aus.*, emp.Empleado AS tNombre, ".			
			" tpo.eCodTipoEntidad,  tpo.tNombreCorto, usr.tNombre AS tUsuario, usr.tApellidos, cta.tNombre AS Tipo ".
			" FROM ausentismo aus ".
			" LEFT JOIN empleados emp ON emp.id=aus.eCodEmpleado ".
			" LEFT JOIN catusuarios usr ON usr.eCodUsuario=aus.eCodUsuario ".
			" LEFT JOIN cattipoempleado tpo ON emp.eCodTipo=tpo.eCodTipoEntidad ".
			" LEFT JOIN cattipoausentismo cta ON cta.eCodTipoAusentismo=aus.eCodTipoAusentismo ".
			" WHERE 1=1 ".
			($_GET['Empleado'] 					? " AND emp.Empleado like '%".$_GET['Empleado']."%'" 	: "").
			($_GET['EcodEstatus'] 				? " AND aus.eCodEstatus='".$_GET['EcodEstatus']."'"			: "").
			($_GET['eCodAsistencia'] 			? " AND aus.eCodAusentismo=".$_GET['eCodAsistencia'] 				: "").
			($_GET['eCodTipoAusentismo'] 		? " AND aus.eCodTipoAusentismo=".$_GET['eCodTipoAusentismo']	: "").
			($_GET['fhFechaAsistenciaInicio']	? " AND aus.fhFecha BETWEEN '".$fhFechaAsistenciaInicio. "' AND '" .$fhFechaAsistenciaFin."'" : "" ).
			" ORDER BY aus.fhFecha ".($_GET['orden'] ? $_GET['orden'] : "DESC").", emp.Empleado ASC ".
			" LIMIT ".( $_GET['limite'] ? $_GET['limite']  : "1000" );
$rsSolicitudes=mysqli_query($link,$select);

// Create new PHPExcel object
$excel = new PHPExcel();

// Valores a Utilizar
$titulo = "Reporte de Ausentismo";
$columna = 'A';
$fila = 5;

// Arreglo con titulos columnas
$titulos = array(
	"Estatus",	
	"Código",	
	"Fecha",
	"Tipo",
	"Empleado",
	"T. Empleado",
	"Usuario",
	"F. Registro"		
);

// Ponemos las propiedades del documento
$excel->getProperties()->setCreator("Sindicato - Mzomicrosystems.com")
	  ->setLastModifiedBy("Sindicato - Mzomicrosystems.com")
	  ->setTitle("Sindicato - Mzomicrosystems.com")
	  ->setSubject("Sindicato - Mzomicrosystems.com")
	  ->setDescription("Sindicato - Mzomicrosystems.com")
	  ->setKeywords("Sindicato - Mzomicrosystems.com")
	  ->setCategory("Sindicato - Mzomicrosystems.com");

$select=" SELECT * ".
		" FROM catentidades WHERE eCodEntidad=1";		
$rEmpresa = mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
// Encabezados del documento
$excel->getActiveSheet()->setCellValue("A1", utf8_encode($rEmpresa['tNombre']));
$excel->getActiveSheet()->setCellValue("A2", utf8_encode("Fecha del Reporte: ".date("d/m/Y H:i:s")));
$excel->getActiveSheet()->setCellValue("A3", $titulo);

// Arreglo con formato para los titulos columnas
$frEnc = new PHPExcel_Style();
$frTit = new PHPExcel_Style();
$frSep = new PHPExcel_Style();

$frEnc->applyFromArray(array('fill'	=> array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color'	=> array('rgb' => 'd6d6d6')),'font' => array('bold'=> true)));
$frTit->applyFromArray(array('fill' => array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color'	=> array('rgb' => 'FFD200')),'font'	=> array('color'=> array('rgb' => 'FFFFFF'))));
$frSep->applyFromArray(array('fill' => array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color'	=> array('rgb' => 'FFFFFF'))));

$numTitulos = sizeof($titulos);

// Ajuste de anchos de columnas
for($z=1;$z<=$numTitulos;$z++){
	// Ponemos el ancho de la columna
	//$excel->getActiveSheet()->getColumnDimension($columna)->setWidth(14);
	$excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);

	// Pintamos el titulo
	$celda = $columna."".$fila;
	$excel->setActiveSheetIndex(0)->setCellValue($celda, $titulos[$z-1]);
	
	if($z < $numTitulos){
		$columna++;
	}
}

// Formato de celdas
$excel->getActiveSheet()->setSharedStyle($frEnc, "A".$fila.":".$columna.$fila);

// Agrupamos las celdas de los encabezados
for($i=1; $i<=3; $i++){
	$excel->getActiveSheet()->setSharedStyle($frEnc, "A".$i);
	$excel->getActiveSheet()->mergeCells('A'.$i.':'.$columna.''.$i);
}
	
$excel->getActiveSheet()->setSharedStyle($frTit, "A3");
$excel->getActiveSheet()->setSharedStyle($frSep, "A4");
$excel->getActiveSheet()->setSharedStyle($frEnc, "A5");
$excel->getActiveSheet()->getRowDimension('4')->setRowHeight(5);

// Ajustamos formatos
$excel->getActiveSheet()->getStyle("A1")->getFont()->setSize(14);
$excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);

$excel->getActiveSheet()->getStyle("A3")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$excel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true);

while($rSolicitud=mysqli_fetch_array($rsSolicitudes,MYSQLI_ASSOC)){
	// aumentamos primero porque estabamos en fila de encabezados
		
	$fila++;
	$columna = 'A';
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'eCodEstatus'}));
	$excel->getActiveSheet()->setCellValue($columna.$fila, sprintf("%07d",$rSolicitud{'eCodAusentismo'}))->getStyle($columna++.$fila)->getNumberFormat()->setFormatCode('00000');	
	$excel->getActiveSheet()->setCellValue($columna++.$fila, date("d/m/Y", strtotime($rSolicitud{'fhFecha'})));	
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'Tipo'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'tNombre'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'tNombreCorto'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'tUsuario'}." ".$rSolicitud{'tApellidos'}));	
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode(date("d/m/Y H:i", strtotime($rSolicitud{'fhRegistro'}))));	
}
// Cambiamos el nombre de la hoja
$excel->getActiveSheet()->setTitle('Sindicato - Mzomicrosystems.com');

// Ocultamos las lineas de division
$excel->getActiveSheet()->setShowGridlines(false);

// Asignamos la primer hoja como la activa
$excel->setActiveSheetIndex(0);

// Redirect output to a client's web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename=Reporte_Ausentismo.xls');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
$objWriter->save('php://output');
exit;
?>