<?php 
require_once("excel/phpexcel.php");
require_once("../conexion/soluciones-mysql.php");
$link = getLink();

$select=" SELECT * ".
		" FROM catentidades WHERE eCodEntidad=1";		
$rEmpresa = mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

$select=" SELECT emp.* ,  tpe.eCodTipoEntidad,  tpe.tNombre as tipo , cre.tNombre as credito , dia.tNombre as tDescanso, cat.Categoria  ".
		" FROM empleados as emp ".
		" INNER JOIN cattipoempleado as tpe on tpe.eCodTipoEntidad=emp.eCodTipo ".
		" LEFT JOIN cattipocredito cre ON cre.eCodCredito=emp.eCodTipoCredito ".
		" LEFT JOIN catdias as dia on dia.eCodDia=emp.descanso ".			
		" LEFT JOIN categorias cat ON cat.indice=emp.eCodCategoria ".
		" WHERE 1=1".
		($_GET['tRFC']				? " AND emp.rfc LIKE '%".$_GET['tRFC']."%'" : "").
		($_GET['tNSS']				? " AND emp.IMSS LIKE '%".$_GET['tNSS']."%'" : "").
		($_GET['tCURP']				? " AND emp.curp LIKE '%".$_GET['tCURP']."%'" : "").
		($_GET['tNombre']			? " AND emp.Empleado LIKE '%".$_GET['tNombre']."%'" : "").
		($_GET['eCodEntidad']		? " AND emp.id=".$_GET['eCodEntidad'] : "").
		($_GET['tipoempleado']		? " AND tpe.eCodTipoEntidad= ".$_GET['tipoempleado'] : "").
		($_GET['eCodCategoria']		? " AND emp.eCodCategoria=".$_GET['eCodCategoria']		: "").
		($_GET['eCodTipoCredito']	? " AND emp.eCodTipoCredito= ".$_GET['eCodTipoCredito'] : "").
		" ORDER BY emp.id ".($_GET['orden'] ? $_GET['orden'] : "ASC").
		" LIMIT ".($_GET['limite'] ? $_GET['limite']  : "100");
$rsSolicitudes=mysqli_query($link,$select);

// Create new PHPExcel object
$excel = new PHPExcel();

// Valores a Utilizar
$titulo = "Catalogo de Categorias";
$columna = 'A';
$fila = 5;

// Arreglo con titulos columnas
$titulos = array(
	"Código",
	"RFC",
	"Tipo",
	"NSS",
	"Nombre",
	"Categoria",
	"CURP",
	"Fecha Alta",
	"Fecha Ingreso",
	"Dia Descanso"
);

// Ponemos las propiedades del documento
$excel->getProperties()->setCreator("Sindicato - Mzomicrosystems.com")
	  ->setLastModifiedBy("Sindicato - Mzomicrosystems.com")
	  ->setTitle("Sindicato - Mzomicrosystems.com")
	  ->setSubject("Sindicato - Mzomicrosystems.com")
	  ->setDescription("Sindicato - Mzomicrosystems.com")
	  ->setKeywords("Sindicato - Mzomicrosystems.com")
	  ->setCategory("Sindicato - Mzomicrosystems.com");

// Encabezados del documento
$excel->getActiveSheet()->setCellValue("A1", utf8_encode($rEmpresa{'tNombre'}));
$excel->getActiveSheet()->setCellValue("A2", utf8_encode("Fecha del Reporte: ".date("d/m/Y H:i:s")));
$excel->getActiveSheet()->setCellValue("A3", $titulo);

// Arreglo con formato para los titulos columnas
$frEnc = new PHPExcel_Style();
$frTit = new PHPExcel_Style();
$frSep = new PHPExcel_Style();

$frEnc->applyFromArray(array('fill'	=> array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color'	=> array('rgb' => 'd6d6d6')),'font' => array('bold'=> true)));
$frTit->applyFromArray(array('fill' => array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color'	=> array('rgb' => 'FFD200')),'font'	=> array('color'=> array('rgb' => 'FFFFFF'))));
$frSep->applyFromArray(array('fill' => array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color'	=> array('rgb' => 'FFFFFF'))));

$numTitulos = sizeof($titulos);

// Ajuste de anchos de columnas
for($z=1;$z<=$numTitulos;$z++){
	// Ponemos el ancho de la columna
	//$excel->getActiveSheet()->getColumnDimension($columna)->setWidth(14);
	$excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);

	// Pintamos el titulo
	$celda = $columna."".$fila;
	$excel->setActiveSheetIndex(0)->setCellValue($celda, $titulos[$z-1]);
	
	if($z < $numTitulos){
		$columna++;
	}
}

// Formato de celdas
$excel->getActiveSheet()->setSharedStyle($frEnc, "A".$fila.":".$columna.$fila);

// Agrupamos las celdas de los encabezados
for($i=1; $i<=3; $i++){
	$excel->getActiveSheet()->setSharedStyle($frEnc, "A".$i);
	$excel->getActiveSheet()->mergeCells('A'.$i.':'.$columna.''.$i);
}
	
$excel->getActiveSheet()->setSharedStyle($frTit, "A3");
$excel->getActiveSheet()->setSharedStyle($frSep, "A4");
$excel->getActiveSheet()->setSharedStyle($frEnc, "A5");
$excel->getActiveSheet()->getRowDimension('4')->setRowHeight(5);

// Ajustamos formatos
$excel->getActiveSheet()->getStyle("A1")->getFont()->setSize(14);
$excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);

$excel->getActiveSheet()->getStyle("A3")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$excel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true);

while($rSolicitud=mysqli_fetch_array($rsSolicitudes,MYSQLI_ASSOC)){
	// aumentamos primero porque estabamos en fila de encabezados
	$fila++;
	$columna = 'A';
	$excel->getActiveSheet()->setCellValue($columna.$fila, sprintf("%05d",$rSolicitud{'id'}))->getStyle($columna++.$fila)->getNumberFormat()->setFormatCode('00000');
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'RFC'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'tipo'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'IMSS'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'Empleado'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'Categoria'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'CURP'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, ($rSolicitud{'fhFechaIngreso'} ? date("d/m/Y", strtotime($rSolicitud{'fhFechaIngreso'})) : ""));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, ($rSolicitud{'fhFechaIngresoReal'} ? date("d/m/Y", strtotime($rSolicitud{'fhFechaIngresoReal'})) : ""));
	
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'tDescanso'}));
}
// Cambiamos el nombre de la hoja
$excel->getActiveSheet()->setTitle('Sindicato - Mzomicrosystems.com');

// Ocultamos las lineas de division
$excel->getActiveSheet()->setShowGridlines(false);

// Asignamos la primer hoja como la activa
$excel->setActiveSheetIndex(0);

// Redirect output to a client's web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename=Catalogo_Empleados.xls');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
$objWriter->save('php://output');
exit;
?>