<?php require_once("excel/phpexcel.php");
require_once("../conexion/soluciones-mysql.php");

$link = getLink();

if ($_GET['fhFechaIncapacidadInicio']!=""){
	$fhFechaIncapacidadInicio = $_GET['fhFechaIncapacidadInicio'].' 00:00:00';
	$fhFechaIncapacidadFin = ($_GET['fhFechaIncapacidadFin']!="" ? $_GET['fhFechaIncapacidadFin'] : $_GET['fhFechaIncapacidadInicio']).' 23:59:59';
}

$select=" SELECT inc.*, emp.Empleado AS Empleado, CONCAT_WS(' ', usr.tNombre, usr.tApellidos) AS Usuario, 
		CONCAT_WS(' ', usrc.tNombre, usrc.tApellidos) AS UCancelacion, CURRENT_DATE AS Hoy
		FROM incapacidades inc
		LEFT JOIN empleados emp ON emp.id=inc.idEmpleado
		INNER JOIN catusuarios usr ON usr.eCodUsuario=inc.idUsuario
		LEFT JOIN catusuarios usrc ON usrc.eCodUsuario=inc.eCodUsuarioCancelacion
		WHERE 1=1 ".
		($_GET['eCodIncapacidad'] 			? " AND inc.id=".$_GET['eCodIncapacidad'] 				: "").			
		($_GET['Empleado'] 				? " AND emp.Empleado like '%".$_GET['Empleado']."%'" 	: "").
		($_GET['fhFechaIncapacidadInicio']	? " AND inc.FechaInicial >='".$fhFechaIncapacidadInicio. "' AND inc.FechaInicial<='" .$fhFechaIncapacidadFin."' OR inc.FechaFinal <= '".$fhFechaIncapacidadInicio."' AND inc.FechaFinal >='".$fhFechaIncapacidadFin."'" : "" ).						
		($_GET['EcodEstatus'] 				? " AND inc.Estado='".$_GET['EcodEstatus']."'"			: "").			
		($_GET['folio'] 					? " AND inc.Folio like '%".$_GET['folio']."%'" 	: "").
		($_GET['chXVencer'] 				? " AND inc.FechaFinal=CURRENT_DATE AND inc.Estado='AC' " 	: "").
		($_GET['chVigentes'] 				? " AND inc.FechaInicial<=CURRENT_DATE AND inc.FechaFinal>=CURRENT_DATE AND inc.Estado='AC' " 	: "").
		" Order by inc.id ".($_GET['orden'] ? $_GET['orden'] : "DESC")." LIMIT ".($_GET['limite'] ? $_GET['limite'] : "1000");
if($_GET['eAccion']==999){
	print $select;
	die();
}
$rsSolicitudes=mysqli_query($link,$select);
// Create new PHPExcel object
$excel = new PHPExcel();

// Valores a Utilizar
$titulo = "Reporte de Incapacidades";
$columna = 'A';
$fila = 5;

// Arreglo con titulos columnas
$titulos = array(
	"Código",	
	"Empleado",
	"F. Inicia",
	"F. Finaliza",
	"Folio",
	"Usuario",
	"F. Registro",
	"Usuario Cancelación",
	"F. Cancelación",
	"Motivo de Cancelación"
);

// Ponemos las propiedades del documento
$excel->getProperties()->setCreator("Sindicato")
	  ->setLastModifiedBy("Sindicato")
	  ->setTitle("Sindicato")
	  ->setSubject("Sindicato")
	  ->setDescription("Sindicato")
	  ->setKeywords("Sindicato")
	  ->setCategory("Sindicato");

$select=" SELECT * ".
		" FROM catentidades WHERE eCodEntidad=1";		
$rEmpresa = mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
// Encabezados del documento
$excel->getActiveSheet()->setCellValue("A1", utf8_encode($rEmpresa['tNombre']));
$excel->getActiveSheet()->setCellValue("A2", utf8_encode("Fecha del Reporte: ".date("d/m/Y H:i:s")));
$excel->getActiveSheet()->setCellValue("A3", $titulo);

// Arreglo con formato para los titulos columnas
$frEnc = new PHPExcel_Style();
$frTit = new PHPExcel_Style();
$frSep = new PHPExcel_Style();

$frEnc->applyFromArray(array('fill'	=> array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color'	=> array('rgb' => 'd6d6d6')),'font' => array('bold'=> true)));
$frTit->applyFromArray(array('fill' => array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color'	=> array('rgb' => 'FFD200')),'font'	=> array('color'=> array('rgb' => 'FFFFFF'))));
$frSep->applyFromArray(array('fill' => array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color'	=> array('rgb' => 'FFFFFF'))));

$numTitulos = sizeof($titulos);

// Ajuste de anchos de columnas
for($z=1;$z<=$numTitulos;$z++){
	// Ponemos el ancho de la columna
	//$excel->getActiveSheet()->getColumnDimension($columna)->setWidth(14);
	$excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);

	// Pintamos el titulo
	$celda = $columna."".$fila;
	$excel->setActiveSheetIndex(0)->setCellValue($celda, $titulos[$z-1]);
	
	if($z < $numTitulos){
		$columna++;
	}
}

// Formato de celdas
$excel->getActiveSheet()->setSharedStyle($frEnc, "A".$fila.":".$columna.$fila);

// Agrupamos las celdas de los encabezados
for($i=1; $i<=3; $i++){
	$excel->getActiveSheet()->setSharedStyle($frEnc, "A".$i);
	$excel->getActiveSheet()->mergeCells('A'.$i.':'.$columna.''.$i);
}
	
$excel->getActiveSheet()->setSharedStyle($frTit, "A3");
$excel->getActiveSheet()->setSharedStyle($frSep, "A4");
$excel->getActiveSheet()->setSharedStyle($frEnc, "A5");
$excel->getActiveSheet()->getRowDimension('4')->setRowHeight(5);

// Ajustamos formatos
$excel->getActiveSheet()->getStyle("A1")->getFont()->setSize(14);
$excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);

$excel->getActiveSheet()->getStyle("A3")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$excel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true);

while($rSolicitud=mysqli_fetch_array($rsSolicitudes,MYSQLI_ASSOC)){
	// aumentamos primero porque estabamos en fila de encabezados
	$fila++;
	$columna = 'A';
	$excel->getActiveSheet()->setCellValue($columna.$fila, sprintf("%05d",$rSolicitud['id']))->getStyle($columna++.$fila)->getNumberFormat()->setFormatCode('00000');
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud['Empleado']));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, date("d/m/Y", strtotime($rSolicitud['FechaInicial'])));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, date("d/m/Y", strtotime($rSolicitud['FechaFinal'])));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud['Folio']));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud['Usuario']));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, date("d/m/Y H:i", strtotime($rSolicitud['fechaRegistro'])));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud['UCancelacion']));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, ($rSolicitud{'fhFechaCancelacion'} ? date("d/m/Y H:i", strtotime($rSolicitud{'fhFechaCancelacion'})) : ""));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud['tMotivoCancelacion']));
}
// Cambiamos el nombre de la hoja
$excel->getActiveSheet()->setTitle('Sindicato');

// Ocultamos las lineas de division
$excel->getActiveSheet()->setShowGridlines(false);

// Asignamos la primer hoja como la activa
$excel->setActiveSheetIndex(0);

// Redirect output to a client's web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename=Reporte_Incapacidades.xls');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
$objWriter->save('php://output');
exit; ?>