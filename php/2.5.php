<?php $eAdmin=($_SESSION['sesionUsuario']['bSindicato']==1  ? 1 : 0); ?>
<table cellspacing="0" border="0" width="900px">
	<tr><td width="100%" colspan="2"><hr width="99%" size="0" align="center" color="#CACACA"></td></tr>
	<tr><td height="20" colspan="2"></td></tr>
	<tr>
		<td height="20" width="50%">
			<table cellspacing="0" border="0" width="100%">
				<tbody>
					<tr>
						<td valign="top" width="128" class="sanLR04"><img width="128" height="128" alt="Empresas" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/trabajador.png"></td>
						<td valign="top">
							<table cellspacing="0" border="0" width="100%">
								<tbody>
									<tr><td class="sanLR04"><a href='?ePagina=2.5.1.1.php'><label class="fonN12S">Asistencias</label></a></td></tr>
									<tr><td class="sanLR04"><a href='?ePagina=2.5.1.6.php'><label class="fonN12S">Asistencias Checador@s</label></a></td></tr>
									<tr><td class="sanLR04"><a href='?ePagina=2.5.1.4.php'><label class="fonN12S">Ausentismos</label></a></td></tr>
									<tr><td align="left" height="2"><hr width="200" size="0" color="#666666" align="left"></td></tr>
									<tr><td class="sanLR04"><a href='?ePagina=2.5.1.2.php'><label class="fonN12S">Movimientos Afiliatorios</label></a></td></tr>
									<tr><td class="sanLR04"><a href='?ePagina=2.5.1.3.php'><label class="fonN12S">Listado Bajas</label></a></td></tr>
									<tr><td align="left" height="2"><hr width="200" size="0" color="#666666" align="left"></td></tr>
									<tr><td class="sanLR04"><a href='?ePagina=2.5.1.5.php'><label class="fonN12S">Vacaciones</label></a></td></tr>
								</tbody>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</td>
		<?php if($eAdmin==1){ ?>
			<td width="50%">
				<table cellspacing="0" border="0" width="100%">
					<tbody>
						<tr>
							<td valign="top" width="128" class="sanLR04"><img width="128" height="128" alt="Servicios" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/imss.png"></td>
							<td valign="top">
								<table cellspacing="0" border="0" width="100%">
									<tbody>
										<tr><td class="sanLR04"><a href="?ePagina=2.5.4.4.php" style="cursor: pointer"><label class="fonN12S">Reingresos </label></a></td></tr>
	                                    <tr><td class="sanLR04"><a href="?ePagina=2.5.4.5.php" style="cursor: pointer"><label class="fonN12S">Modificaci&oacute;n </label></a></td>
	                                    <tr><td class="sanLR04"><a href="?ePagina=2.5.4.6.php" style="cursor: pointer"><label class="fonN12S">Bajas </label></a></td></tr>
	                                    <tr><td align="left" height="2"><hr width="200" size="0" color="#666666" align="left"></td></tr>
	                                    <tr><td class="sanLR04"><a href="?ePagina=2.5.4.7.php" style="cursor: pointer"><label class="fonN12S">Reingresos Checador@s</label></a></td></tr>
	                                    <tr><td class="sanLR04"><a href="?ePagina=2.5.4.8.php" style="cursor: pointer"><label class="fonN12S">Bajas Checador@s</label></a></td></tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
		<?php } ?>
	</tr>
	<tr><td height="20" colspan="2"></td></tr>
	<?php if($eAdmin==1){ ?>
	    <tr>
	        <td height="20" width="50%">
	            <table cellspacing="0" border="0" width="100%">
	                <tbody>
	                    <tr>
	                        <td valign="top" width="128" class="sanLR04"><img width="128" height="128" alt="Maniobras" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/incapacidad.png"></td>
	                        <td valign="top">
	                            <table cellspacing="0" border="0" width="100%">
	                                <tbody>                                	
	                                    <tr><td class="sanLR04"><a href='?ePagina=2.5.2.1.php'><label class="fonN12S">Incapacidades</label></a></td></tr>
	                                </tbody>
	                            </table>
	                        </td>
	                    </tr>
	                </tbody>
	            </table>
	        </td>
	        <td width="50%">
				<table cellspacing="0" border="0" width="100%">
					<tbody>
						<tr>
							<td valign="top" width="128" class="sanLR04"><img width="128" height="128" alt="Servicios" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-export.png"></td>
							<td valign="top">
								<table cellspacing="0" border="0" width="100%">
									<tbody>
										<tr><td class="sanLR04"><a href="?ePagina=2.5.3.1.php" style="cursor: pointer"><label class="fonN12S">Incidencias</label></a></td></tr>
	                                    <tr><td align="left" height="2"><hr width="200" size="0" color="#666666" align="left"></td></tr>
	                                    <tr><td class="sanLR04"><a href="?ePagina=2.5.3.2.php" style="cursor: pointer"><label class="fonN12S">Movtos Afiliatorios</label></a></td>
	                                    <tr><td align="left" height="2"><hr width="200" size="0" color="#666666" align="left"></td></tr>
	                                    <tr><td class="sanLR04"><a href="?ePagina=2.5.3.3.php" style="cursor: pointer"><label class="fonN12S">Nuevos Empleados</label></a></td></tr>
	                                    <tr><td class="sanLR04"><a href="?ePagina=2.5.3.4.php" style="cursor: pointer"><label class="fonN12S">Prenomina Sindical</label></a></td></tr>
										<tr><td align="left" height="2"><hr width="200" size="0" color="#666666" align="left"></td></tr>
	                                    <tr><td class="sanLR04"><a href="?ePagina=2.5.3.5.php" style="cursor: pointer"><label class="fonN12S">Incidencias Checador@s</label></a></td></tr>
	                                    <tr><td class="sanLR04"><a href="?ePagina=2.5.3.6.php" style="cursor: pointer"><label class="fonN12S">Movtos Afiliatorios Checador@s</label></a></td>
	                                    <tr><td class="sanLR04"><a href="?ePagina=2.5.3.7.php" style="cursor: pointer"><label class="fonN12S">Nuevos Empleados Checador@s</label></a></td></tr>
	                                    <tr><td class="sanLR04"><a href="?ePagina=2.5.3.8.php" style="cursor: pointer"><label class="fonN12S">Prenomina Sindical Checador@s</label></a></td></tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</td>
	    </tr>
    <?php } ?>
	<tr><td height="20" colspan="2"></td></tr>
</table>
</table>