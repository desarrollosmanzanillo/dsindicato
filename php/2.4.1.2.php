﻿<?php require_once("conexion/soluciones-mysql.php");
$link = getLink();
if($_POST){
	
	if($_POST['eProceso']==1){
		$exito = 1;
		$asistencias = 0;
		$idEmpresa 			= ($_POST['eCodEmisor']			? (int)$_POST['eCodEmisor']				: "NULL");
		$idUsuario			= ($_POST['eUsuario']			? (int)$_POST['eUsuario']				: "NULL");
		$fhFechaAsistencia 	= ($_POST['fhFechaAsistencia']	? "'".$_POST['fhFechaAsistencia']."'"	: "NULL");
		$fhFechaAsistenciaF = ($_POST['fhFechaAsistencia1']	? "'".$_POST['fhFechaAsistencia1']."'"	: "NULL");
		$eCodTipoAusentismo	= ($_POST['eCodTipoAusentismo']	? (int)$_POST['eCodTipoAusentismo']		: "NULL");
		$fechaayer = date('Y-m-d',strtotime('-1 days', strtotime($fhFechaAsistencia)));

		$select=" SELECT * FROM sisconfiguracion ";
		$configura=mysqli_fetch_array(mysqli_query($link,$select), MYSQLI_ASSOC);
		$select=" SELECT Turno3 AS Salario FROM categorias WHERE indice=".(int)$configura['dCategoriaAusentismo'];
		$salarioau=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

		$select=" SELECT DATEDIFF('".$_POST['fhFechaAsistencia1']."', '".$_POST['fhFechaAsistencia']."') AS Dias ";
		$rDiferencia=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
		$eFor = ((int)$rDiferencia{'Dias'}+1);
		$i = 1;
		while ($i <= $eFor){
			$eFor--;
			$select=" SELECT DATE_ADD('".$_POST['fhFechaAsistencia1']."', INTERVAL -".$eFor." DAY) AS dActual ";
			$dActual=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
			$fhFechaAsistencia = $dActual{'dActual'};

			foreach($_POST as $k => $v){
				$nombre = strval($k);
				if(strstr($nombre,"eRegistro") && $v){
					$eFila=str_replace("eRegistro", "", $nombre);
					$area 				= ($_POST['eCodCosto'.$eFila]		? (int)$_POST['eCodCosto'.$eFila]		: "NULL");
					$idEmpleado 		= ($_POST['idEmpleado'.$eFila]		? (int)$_POST['idEmpleado'.$eFila]		: "NULL");
					$idCategorias 		= ($_POST['idCategoria'.$eFila]		? (int)$_POST['idCategoria'.$eFila]		: "NULL");

					if((int)$idEmpleado>0){
						$insert=" INSERT INTO ausentismo (eCodEmpleado, eCodTipoAusentismo, fhFecha, eCodUsuario, eCodEstatus, fhRegistro) 
								VALUES (".(int)$idEmpleado.", ".$eCodTipoAusentismo.", '".$fhFechaAsistencia."', ".$idUsuario.", 'AC', CURRENT_TIMESTAMP) ";
						if($res=mysqli_query($link,$insert)){
							$cadena.=$insert;
							$asistencias++;
						}else{
							$cadena.=$insert;
							$exito=0;
						}
					}
				}
			}
		}
		print "<input type=\"text\" value=\"".((int)$exito>0 && (int)$asistencias>0 ? 1 : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}

	if($_POST['eProceso']==2){
		$select=" SELECT DATEDIFF('".$_POST['fhFechaAsistencia1']."', '".$_POST['fhFechaAsistencia']."') AS Dias ";
		$select=" SELECT id ".
				" FROM asistencias ".
				" WHERE Estado LIKE 'AC' AND Fecha BETWEEN '".$_POST['fhFechaAsistencia']."' AND '".$_POST['fhFechaAsistencia1']."'  ".
				" AND idEmpleado=".(int)$_POST['idEmpleado'.(int)$_POST['eFilaEmp']];
		$asistencia=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

		$select=" SELECT id ".
				" FROM incapacidades ".
				" WHERE Estado LIKE 'AC' AND idEmpleado=".(int)$_POST['idEmpleado'.(int)$_POST['eFilaEmp']].
				" AND (FechaInicial BETWEEN '".$_POST['fhFechaAsistencia']."' AND '".$_POST['fhFechaAsistencia1']."' ".
				" OR FechaFinal BETWEEN '".$_POST['fhFechaAsistencia']."' AND '".$_POST['fhFechaAsistencia1']."') ";
		$incapacidad=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

		$select=" SELECT eCodAusentismo ".
				" FROM ausentismo ".
				" WHERE eCodEmpleado=".(int)$_POST['idEmpleado'.(int)$_POST['eFilaEmp']].
				" AND fhFecha BETWEEN '".$_POST['fhFechaAsistencia']."' AND '".$_POST['fhFechaAsistencia1']."' AND eCodEstatus='AC' ";
		$ausentis=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
		print "<input type=\"hidden\" name=\"eAusentismoBD\" id=\"eAusentismoBD\" value=\"".(int)$ausentis{'eCodAusentismo'}."\">";
		print "<input type=\"hidden\" name=\"eEmpleadoBD\" id=\"eEmpleadoBD\" value=\"".(int)$asistencia{'id'}."\">";
		print "<input type=\"hidden\" name=\"eIncapacidad\" id=\"eIncapacidad\" value=\"".(int)$incapacidad{'id'}."\">";
	}

	if($_POST['eProceso']==3){
		$exito = 1;
		$idUsuario 			= ($_POST['eUsuario'] 			? $_POST['eUsuario'] 								: "NULL");
		$tMotivoCancelacion	= ($_POST['tMotivoCancelacion']	? "'".utf8_decode($_POST['tMotivoCancelacion'])."'"	: "NULL");

		$insert=" UPDATE ausentismo ".
				" SET 	eCodEstatus='CA',".
				"		tMotivoCancelacion=".$tMotivoCancelacion.", ".
				"		fhFechaCancelacion=CURRENT_TIMESTAMP,".
				"		eCodUsuarioCancelacion=".$idUsuario.
				" WHERE eCodAusentismo=".(int)$_POST['edAusentismo'];
		if($res=mysqli_query($link,$insert)){
			$cadena.=$insert;
			$asistencias++;
			$exito=1;
		}else{
			$cadena.=$insert;
			$exito=0;
		}
		print "<input type=\"text\" value=\"".((int)$exito>0 ? 1 : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}
//	
}
if(!$_POST){
$select=" SELECT *
		FROM ausentismo
		WHERE eCodAusentismo=".$_GET['eCodAusentismo'];
$rAusentismo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

$select=" SELECT * ".
		" FROM cattipoausentismo ".
		" WHERE tCodEstatus='AC' ".
		" ORDER BY tNombre ";
$rsTiposAusentismos=mysqli_query($link,$select); ?>
<style>
.eNumero{
	text-align:right;
}
</style>
<script src="../js/dojo/dijit/form/FilteringSelect.js" type="text/javascript"></script> 
<script src="../js/dojo/dojo/data/ItemFileReadStore.js" type="text/javascript"></script> 
<script src="../js/dojo/dijit/form/FilteringSelect.js" type="text/javascript"></script> 
<script src="../js/dojo/dijit/TooltipDialog.js" type="text/javascript"></script> 
<script type="text/javascript">
dojo.require("dojo.number");
dojo.require("dijit.form.NumberTextBox");
dojo.require("dijit.form.DateTextBox");
dojo.require("dojo.io.iframe");

function guardar(){
	dojo.byId('eProceso').value=1;
	var bandera = false;
	var bServicios = false;
	var bMercancias = false;
	var eTipoSolicitud = 0;
	mensaje = "¡Verifique lo siguiente!\n";

	var ultimafila=0;

	dojo.query("[id^=\"eRegistro\"]").forEach(function(nodo, index, array){
		ultimafila++;
	});
	if (!dojo.byId("fhFechaAsistencia").value || !dojo.byId("fhFechaAsistencia1").value){
		mensaje+="* Fecha de Ausentismo\n";
		bandera = true;		
	}
	if (!dojo.byId("eCodTipoAusentismo").value){
		mensaje+="* Tipo\n";
		bandera = true;		
	}
	if(parseInt(ultimafila)<=1){
		mensaje+="* Ausentismos\n";
		bandera = true;		
	}else{
		var bIncompleto=false;
		var bEmpleado=false;
		var bCategoria=false;
		var bCosto=false;
		dojo.query("[id^=\"eRegistro\"]").forEach(function(nodo, index, array){
			var eFilaValida=parseInt(nodo.value);
			if(ultimafila>eFilaValida){
				if(!dijit.byId('idEmpleado'+eFilaValida).value){
					bEmpleado=true;
					bIncompleto=true;
				}
			}else{
				if(dijit.byId('idEmpleado'+eFilaValida).value){
					bEmpleado=true;
					bIncompleto=true;
				}
			}
		});
		if(bIncompleto==true){
			mensaje+="* Conceptos incompletos\n";
			if(bEmpleado==true){
				mensaje+="   Empleado\n";
			}
			bandera = true;
		}
	}

	if (bandera==true){
		alert(mensaje);
	}else{
		if (confirm("¿Desea Guardar la Información?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				if(dojo.byId("eCodigo").value>0){
					alert("¡La información se guardó correctamente!");
					tURL = './?ePagina=2.5.1.4.php';
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
		}
	}
}

function cancelar(){
	dojo.byId('eProceso').value=3;
	var bandera = false;
	var bServicios = false;
	var bMercancias = false;
	var eTipoSolicitud = 0;
	mensaje = "¡Verifique lo siguiente!\n";

	if(!dojo.byId('tMotivoCancelacion').value){
		bandera = true;
		mensaje+="* Motivo de Cancelación\n";
	}

	if (bandera==true){
		alert(mensaje);
	}else{
		if (confirm("¿Desea Cancelar la Información?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				if(dojo.byId("eCodigo").value>0){
					alert("¡La información se guardó correctamente!");
					tURL = './?ePagina=2.5.1.4.php&eCodAusentismo='+dojo.byId("edAusentismo").value;
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
		}
	}
}

function borrarAsistencia(eFilaAsistencia){
	var ultimafila=0;
	dojo.query("[id^=\"eRegistro\"]").forEach(function(nodo, index, array){
		ultimafila=parseInt(nodo.id.replace('eRegistro',''));
	});

	if(eFilaAsistencia!=ultimafila){
		if(confirm("¿Seguro de eliminar la asistencia?")){
			dojo.destroy(dojo.byId('filaAsistencia'+eFilaAsistencia));
		}
	}
}

function verificarEmpleados(){
	dojo.query("[id^=\"idEmpleado\"]").forEach(function(nodo, index, array){
		var filaEmpleado=nodo.id.replace('idEmpleado','');
		if(parseInt(filaEmpleado)>0){
			validarEmpleado(filaEmpleado);
		}
	});
}

function validarEmpleado(fila){
	dojo.byId('eProceso').value=2;
	dojo.byId('eFilaEmp').value=fila;
	dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
		dojo.byId("dvCNS").innerHTML = tRespuesta;
		if(parseInt(dijit.byId('idEmpleado'+fila).value)>0){
			if(parseInt(dojo.byId('eAusentismoBD').value)>0){
				alert("¡El empleado cuenta con registro de ausentismo en el periodo de dias!");
				dojo.byId('idEmpleado'+fila).value="";
				dijit.byId('idEmpleado'+fila).value="";
				dojo.byId('idEmpleado'+fila).focus();
			}else{
				if(parseInt(dojo.byId('eIncapacidad').value)>0){
					alert("¡El empleado cuenta con registro de incapacidad en el periodo de dias!");
					dojo.byId('idEmpleado'+fila).value="";
					dijit.byId('idEmpleado'+fila).value="";
					dojo.byId('idEmpleado'+fila).focus();
				}else{
					if(parseInt(dojo.byId("eEmpleadoBD").value)>0){
						alert("¡El empleado ya tiene asistencia en el periodo de dias!");
						dojo.byId('idEmpleado'+fila).value="";
						dijit.byId('idEmpleado'+fila).value="";
						dojo.byId('idEmpleado'+fila).focus();
					}else{
						validarFila(fila);
					}
				}
			}
		}
	}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
}

function agregarConcepto(filaAsistencia){ 
	var tb = dojo.byId('tbAsistencias');
	var tr = null;
	var td = null;
	var fila = 0;
	fila = parseInt(dojo.byId("eFilas").value)+1;
	var rows = document.getElementById('tablaAsistencias').getElementsByTagName('tbody')[0].getElementsByTagName('tr');
    for (i = 0; i < rows.length; i++) {
		if(rows[i].id == "filaAsistencia"+filaAsistencia){
			tr = tb.insertRow(rows[i].rowIndex);
		}
    }

	tr.id = "filaAsistencia"+fila;
	td = tr.insertCell(0);
	td.height = '23px';
	td.className = "sanLR04";
	td.innerHTML="<img width=\"16\" align=\"absmiddle\" height=\"16\" onclick=\"borrarAsistencia("+fila+");\" id=\"filaConcepto"+fila+"\" src=\"https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-borrar.png\" title=\"Eliminar Concepto\">";	
	td.noWrap = "true";

	td = tr.insertCell(1);
	td.className = "sanLR04";
	td.innerHTML='<input type="hidden" name="eRegistro'+fila+'" id="eRegistro'+fila+'" value="'+fila+'"><div dojoType="dojo.data.ItemFileReadStore" jsId="acEmpledos'+fila+'" url="php/auto/empleados.php" clearOnClose="true" urlPreventCache="true"></div><input dojoType="dijit.form.FilteringSelect" value="" jsId="cmbEmpleados'+fila+'" store="acEmpledos'+fila+'" searchAttr="nombre" hasDownArrow="false" name="idEmpleado'+fila+'" id="idEmpleado'+fila+'" autoComplete="false" searchDelay="300" highlightMatch="none" placeHolder="Empleados" style="width:380px;" displayMessage="false" required="false" queryExpr = "*" pageSize="10" onChange="validarEmpleado('+fila+');">';
	td.noWrap = "true";

	dojo.byId("eFilas").value++;
	dojo.parser.parse('tablaAsistencias');
	asignaEmpleados();
}

function validarFila(filaEmpleado){
	
	var ultimafila=0;
	var bDistinto=true;
	dojo.query("[id^=\"idEmpleado\"]").forEach(function(nodo, index, array){
		ultimafila=nodo.id.replace('idEmpleado','');
		if(ultimafila>0){
			if(ultimafila!=filaEmpleado && dijit.byId(nodo.id).value){
				if(dijit.byId('idEmpleado'+filaEmpleado).value==dijit.byId(nodo.id).value){
					alert('¡Este empleado ya se encuentra seleccionado!');
					dojo.byId('idEmpleado'+filaEmpleado).value="";
					dijit.byId('idEmpleado'+filaEmpleado).value="";
					dojo.byId('idEmpleado'+filaEmpleado).focus();
					bDistinto=false;
				}
			}
		}
	});

	if(bDistinto==true){
		var ultimafila=0;
		var bGenerarLinea=true;
		var eCodTipoServicio=0;
		
		dojo.query("[id^=\"eRegistro\"]").forEach(function(nodo, index, array){
			ultimafila=parseInt(nodo.id.replace('eRegistro',''));
		});
			
		if(filaEmpleado==ultimafila){
			if(dojo.byId('idEmpleado'+filaEmpleado)){
				if(!dijit.byId('idEmpleado'+filaEmpleado).value){
					bGenerarLinea=false;
				}
			}
			if(bGenerarLinea==true){
				agregarConcepto(filaEmpleado);
			}
		}
	}
}

function consultar(){
	document.location = './?ePagina=2.5.1.4.php';
}

function listaempleados(codigo){
	var id = codigo.replace("idEmpleado","")
	var busqueda = dojo.byId(codigo).value;
	var acEmp='acEmpledos'+id;
	var cmbEmp='cmbEmpleados'+id;
	eval(acEmp).url = "php/auto/empleados.php?busqueda="+busqueda;
	eval(acEmp).close();
	eval(cmbEmp).store=eval(acEmp);
}

function asignaEmpleados(){
	dojo.query("[id*=\"idEmpleado\"]").forEach(function(nodo, index, array){
		dojo.connect(dijit.byId(nodo.id), "onKeyUp", function(event){
			if(event.keyCode!=40&&event.keyCode!=38){
				listaempleados(nodo.id);
			}
		});
	});
}

dojo.addOnLoad(function(){
	dojo.parser.parse('tablaAsistencias');
	asignaEmpleados();
	if(dojo.byId('edAusentismo').value==""){
		dojo.destroy("btn8_2");
	}else{
		dojo.destroy("btn1_1");
	}
});
</script>
<div id="dvCNS" style="display:none;"></div>
<form id="Datos" name="Datos" method="post" onSubmit="return false;" enctype="multipart/form-data">
<input type="hidden" name="eProceso" id="eProceso" value="" />
<input type="hidden" name="eServicio" id="eServicio" value="" />
<input type="hidden" name="eUsuario" id="eUsuario" value="<?=(int)$_SESSION['sesionUsuario']['eCodUsuario'];?>" />
<input type="hidden" name="eCodEntidad" id="eCodEntidad" value="<?=(int)$_SESSION['sesionUsuario']['eCodEntidad'];?>" />
<input type="hidden" name="mail" id="mail" value="<?=$_SESSION['sesionUsuario']['Correo'];?>" />
<input type="hidden" name="nombreusuario" id="nombreusuario" value="<?=$_SESSION['sesionUsuario']['Usuario'];?>" />
<input type="hidden" name="edAusentismo" id="edAusentismo" value="<?=$rAusentismo{'eCodAusentismo'};?>" />
<input type="hidden" name="eFilaEmp" id="eFilaEmp" value="" />
<input type="hidden" name="eFDiaria" id="eFDiaria" value="<?=date('Ymd');?>" />
<div id="dvFacturacion"></div>
<table border="0" cellpadding="0" cellspacing="0" width="980px">
    <tr>
        <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" width="16" height="16" align="absmiddle"> Fecha de Ausentismo </td>
        <td width="50%" nowrap class="sanLR04">
			<input name="fhFechaAsistencia" type="date" dojoType="dijit.form.TextBox" id="fhFechaAsistencia" value="<?=($rAusentismo{'fhFecha'} ? $rAusentismo{'fhFecha'} : date('Y-m-d'));?>" style="width:140px" onChange="verificarEmpleados();" <?=((int)$rAusentismo{'eCodEmpleado'}>0 ? "disabled" : "");?>>	
			-
			<input name="fhFechaAsistencia1" type="date" dojoType="dijit.form.TextBox" id="fhFechaAsistencia1" value="<?=($rAusentismo{'fhFecha'} ? $rAusentismo{'fhFecha'} : date('Y-m-d'));?>" style="width:140px" onChange="verificarEmpleados();" <?=((int)$rAusentismo{'eCodEmpleado'}>0 ? "disabled" : "");?>>
        </td>
    </tr>
    <tr>
        <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" width="16" height="16" align="absmiddle"> Tipo </td>
        <td width="50%" nowrap class="sanLR04">
			<select name="eCodTipoAusentismo" id="eCodTipoAusentismo" style="width:142px; height:25px;" <?=((int)$rAusentismo{'eCodEmpleado'}>0 ? "disabled" : "");?>>
				<option value="">Seleccione...</option>
				<?php while($rTipoAusentismo=mysqli_fetch_array($rsTiposAusentismos,MYSQLI_ASSOC)){ ?>
					<option value="<?=$rTipoAusentismo{'eCodTipoAusentismo'}?>" <?=($rTipoAusentismo{'eCodTipoAusentismo'}==$rAusentismo{'eCodTipoAusentismo'} ? "selected" : "");?>><?=utf8_encode($rTipoAusentismo{'tNombre'})?></option>
				<?php } ?>
			</select>
        </td>
    </tr>
    <tr>
        <td height="23" nowrap class="sanLR04"></td>
        <td width="50%" nowrap class="sanLR04"></td>
        <td nowrap class="sanLR04"></td>
        <td width="50%" nowrap class="sanLR04"></td>
    </tr>
    <tr>
    	<td colspan="4">
            <table cellspacing="0" border="0" width="100%" id="tablaAsistencias" name="tablaAsistencias">
                <thead>
                    <tr class="thEncabezado">
                        <td nowrap="nowrap" class="sanLR04" height="23"> </td>
                        <td nowrap="nowrap" class="sanLR04" height="23"> Empleado</td>
                        <td nowrap="nowrap" class="sanLR04" width="100%"></td>                        
                    </tr>
                </thead>
                <tbody id="tbAsistencias" name="tbAsistencias">
                    <tr id="filaAsistencia1" name="filaAsistencia1">
                        <td nowrap="nowrap" class="sanLR04" height="23">
                        	<?php if((int)$rAusentismo{'eCodAusentismo'}>0){ ?>
                        		<?=sprintf("%07d",$rAusentismo{'eCodAusentismo'});?>
                        		<input type="hidden" name="eCodAusentismo1" id="eCodAusentismo1" value="<?=$rAusentismo{'eCodAusentismo'};?>">
                        	<?php }else{ ?>
                        		<img width="16" align="absmiddle" height="16" onclick="borrarAsistencia(1);" id="filaConcepto1" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-borrar.png" title="Eliminar Concepto">
                        	<?php } ?>
                        </td>
                        <td nowrap="nowrap" class="sanLR04" height="23"><input type="hidden" name="eRegistro1" id="eRegistro1" value="1">
                        <div dojoType="dojo.data.ItemFileReadStore" jsId="acEmpledos1" url="php/auto/empleados.php?idEmpleado=<?=$rAusentismo{'eCodEmpleado'};?>" clearOnClose="true" urlPreventCache="true"></div>
                        <input dojoType="dijit.form.FilteringSelect" 
                        	<?=((int)$rAusentismo{'eCodEmpleado'}>0 ? "disabled" : "");?>
                            value="<?=$rAusentismo['eCodEmpleado'];?>"
                            jsId="cmbEmpleados1"
                            store="acEmpledos1"
                            searchAttr="nombre"
                            hasDownArrow="false"
                            name="idEmpleado1"
                            id="idEmpleado1"
                            autoComplete="false"
                            searchDelay="300"
                            highlightMatch="none"
                            placeHolder="Empleados" 
                            style="width:380px;"
                            displayMessage="false"
                            required="false"
                            queryExpr = "*"
                            pageSize="10"
                            onChange="validarEmpleado(1);"
                            >
                        </td>
                        <td nowrap="nowrap" class="sanLR04"></td>
                    </tr>
                </tbody>
            </table>
            <input name="eFilas" id="eFilas" value="1" type="hidden">
        </td>
    </tr>
    <?php if((int)$rAusentismo{'eCodEmpleado'}>0){ ?>
    	<tr><td height="20"></td></tr>
    	<tr>
	        <td height="23" nowrap="" class="sanLR04" valign="top">Motivo de Cancelación</td>
	        <td colspan="3" nowrap="" class="sanLR04"><textarea name="tMotivoCancelacion" id="tMotivoCancelacion" cols="100" rows="4"></textarea></td>
	    </tr>
    <?php } ?>
	<tr><td height="20"></td></tr>
    <tr><td valign="top" nowrap="nowrap" class="sanLR04" colspan="4"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle" /> <font class="fntG10K">Indica que es obligatorio llenar el campo.</font></td></tr>
</table>
</form>
<?php } ?>