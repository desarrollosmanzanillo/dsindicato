<?php require_once("conexion/soluciones-mysql.php");  
$link = getLink();
if($_POST){
	if($_POST['eProceso']==1){
		$exito			= 0;
		$tNombre		= ($_POST['tNombre']		? "'".trim(utf8_decode($_POST['tNombre']))."'"		: "NULL");
		$eCodPais		= ($_POST['eCodPais']		? (int)$_POST['eCodPais']							: "NULL");
		$eCodCiudad		= ($_POST['eCodCiudad']		? (int)$_POST['eCodCiudad']							: "NULL");
		$eCodEstado		= ($_POST['eCodEstado']		? (int)$_POST['eCodEstado']							: "NULL");
		$tNombreCorto	= ($_POST['tNombreCorto']	? "'".trim(utf8_decode($_POST['tNombreCorto']))."'"	: "NULL");

		if((int)$eCodCiudad>0){
			$update=" UPDATE catciudades ".
					" SET tNombre=".$tNombre.",".
					" tNombreCorto=".$tNombreCorto.",".
					" eCodEstado=".$eCodEstado.
					" WHERE eCodCiudad=".$eCodCiudad;
			if($res=mysqli_query($link,$update)){
				$exito=1;
			}else{
				$exito=0;
			}
		}else{
			$insert=" INSERT INTO catciudades(tCodEstatus, tNombre, tNombreCorto, eCodEstado) ".
					" VALUES ('AC', ".$tNombre.", ".$tNombreCorto.", ".$eCodEstado.")";
			print $insert;
			if($res=mysqli_query($link,$insert)){
				$exito=1;
				$select=" select last_insert_id() AS Llave ";
				$rCodigo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
				$eCodCiudad=(int)$rCodigo['Llave'];
			}else{
				$exito=0;
			}
		}
		print "<input type=\"text\" value=\"".($exito==1 ? $eCodCiudad : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}
	
	if($_POST['eProceso']==2){
		$rCiudad=mysqli_fetch_array(mysqli_query($link,"SELECT 1 AS Ciudad FROM catestados WHERE tNombre='".trim($_POST['tNombre'])."' AND eCodPais=".(int)$_POST['eCodPais']." AND eCodEstado=".(int)$_POST['eCodEstado']." AND eCodCiudad!=".(int)$_POST['eCodCiudad']),MYSQLI_ASSOC);
		print "<input name=\"eCiudad\" id=\"eCiudad\" value=\"".(int)$rCiudad{'Ciudad'}."\">";
	}

	if($_POST['eProceso']==3){
		$select=" SELECT * ".
				" FROM catestados ".
				" WHERE tCodEstatus='AC' ".
				" AND eCodPais=".$_POST['eCodPais'];
		$rsEstados=mysqli_query($link,$select); ?>
		<select name="eCodEstado" id="eCodEstado" style="width:175px">
			<option value="">Seleccione...</option>
			<?php while($rEstado=mysqli_fetch_array($rsEstados,MYSQLI_ASSOC)){ ?>
				<option value="<?=$rEstado{'eCodEstado'}?>"><?=utf8_encode($rEstado{'tNombre'})?></option>
			<?php } ?>
		</select>
	<?php }
}
if(!$_POST){
$select=" SELECT cc.*, ce.eCodPais ".
		" FROM catciudades cc ".
		" LEFT JOIN catestados ce ON ce.eCodEstado=cc.eCodEstado ".
		" WHERE eCodCiudad=".$_GET['eCodCiudad'];
$rCiudad = mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
//Combo Pais
$select=" SELECT * ".
		" FROM catpaises ".
		" WHERE tCodEstatus='AC' ".
		" ORDER BY tNombre ASC ";
$rsPaises = mysqli_query($link,$select);
//Combo Estados
$select=" SELECT * ".
		" FROM catestados ".
		" WHERE tCodEstatus='AC' ".
		" AND eCodPais=".(int)$rCiudad{'eCodPais'}.
		" ORDER BY tNombre ASC ";
$rsEstados = mysqli_query($link,$select);
?>
<script type="text/javascript">
function guardar(){
	dojo.byId('eProceso').value=1;
	bandera = false;
	mensaje = "¡Verifique lo siguiente!\n";
	if (!dojo.byId("tNombre").value){
		mensaje+="* Nombre\n";
		bandera = true;		
	}
	if (!dojo.byId("eCodPais").value){
		mensaje+="* País\n";
		bandera = true;		
	}
	if (!dojo.byId("eCodEstado").value){
		mensaje+="* Estado\n";
		bandera = true;		
	}

	if (bandera==true){
		alert(mensaje);		
	}else{
		if (confirm("¿Desea Guardar la Información?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				if(dojo.byId("eCodigo").value>0){
					alert("¡La información se guardó correctamente!");
					tURL = './?ePagina=2.3.4.5.2.php&eCodCiudad='+dojo.byId("eCodigo").value;
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
		}
	}
}	

function verificarPais(){
	if(dojo.byId('tNombre').value){
		dojo.byId('eProceso').value=2;
		dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
			dojo.byId("dvCNS").innerHTML = tRespuesta;
			if(dojo.byId('eCiudad').value==1){
				alert("¡Esta ciudad ya existe!");
				dojo.byId('tNombre').value="";
			}
		}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
	}
}

function cargarEstados(){
	dojo.byId('eProceso').value=3;
	dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
		dojo.byId("tdEstado").innerHTML = tRespuesta;
	}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});
}

function consultar(){
	document.location = './?ePagina=2.3.4.5.php';
}
</script>
<div id="dvCNS" style="display:none;"></div>
<form action="" method="post" name="Datos" id="Datos" onsubmit="return false;">
<input type="hidden" name="eProceso" id="eProceso" value="" />
<input type="hidden" name="eCodCiudad" id="eCodCiudad" value="<?=$_GET['eCodCiudad'];?>" />
<table border="0" cellpadding="0" cellspacing="0">
    <tr><td height="20"></td></tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Nombre </td>
	    <td width="50%" nowrap class="sanLR04">
           	<input name="tNombre" type="text" dojoType="dijit.form.TextBox" id="tNombre" value="<?=utf8_encode($rCiudad{'tNombre'});?>" style="width:175px">
        </td>
		<td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" height="0" width="16" align="absmiddle"> Nombre Corto </td>
	    <td width="50%" nowrap class="sanLR04">
           	<input name="tNombreCorto" type="text" dojoType="dijit.form.TextBox" id="tNombreCorto" value="<?=utf8_encode($rCiudad{'tNombreCorto'});?>" style="width:175px;">
        </td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Pa&iacute;s </td>
	    <td nowrap class="sanLR04">
           	<select name="eCodPais" id="eCodPais" style="width:175px" onchange="cargarEstados();">
				<option value="">Seleccione...</option>
				<?php while($rPais=mysqli_fetch_array($rsPaises,MYSQLI_ASSOC)){ ?>
					<option value="<?=$rPais{'eCodPais'}?>" <?=($rPais{'eCodPais'}==$rCiudad{'eCodPais'} ? "selected=\"selected\"" : "");?> ><?=utf8_encode($rPais{'tNombre'});?></option>
				<?php } ?>
			</select>
        </td>
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Estados </td>
	    <td nowrap class="sanLR04" id="tdEstado">
           	<select name="eCodEstado" id="eCodEstado" style="width:175px">
				<option value="">Seleccione...</option>
				<?php while($rEstado=mysqli_fetch_array($rsEstados,MYSQLI_ASSOC)){ ?>
					<option value="<?=$rEstado{'eCodEstado'}?>" <?=($rEstado{'eCodEstado'}==$rCiudad{'eCodEstado'} ? "selected=\"selected\"" : "");?> ><?=utf8_encode($rEstado{'tNombre'});?></option>
				<?php } ?>
			</select>
        </td>
    </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle" /> <font class="fntG10K">Indica que es obligatorio llenar el campo.
    </font></td>
    </tr>
</table>
</form>
<?php } ?>