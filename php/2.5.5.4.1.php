<?php require_once("conexion/soluciones-mysql.php");  
$link = getLink();
$select=" SELECT oss.eCodSolicitud, oss.eCodEntrada, ces.tNombre AS Estatus, oss.fhFechaEntrada, ".
		" cen.tNombre AS Naviera, oss.tPatente, cec.tNombre AS Cliente, cef.tNombre AS FacturarA, ".
		" cbu.tNombre AS Buque, oss.tNumeroViaje, cto.tNombre AS TipoServicio, oss.tObservaciones, ".
		" oss.eCodTipoServicio, car.tNombre AS Area, oss.tCodContenedor, ctc.tNombreCorto AS TipoContenedor ".
		" FROM opeentradasmercancias oss ".
		" INNER JOIN catestatus ces ON ces.tCodEstatus=oss.tCodEstatus ".
		" LEFT JOIN catentidades cen ON cen.eCodEntidad=oss.eCodNaviera ".
		" INNER JOIN catentidades cec ON cec.eCodEntidad=oss.eCodCliente ".
		" INNER JOIN catentidades cef ON cef.eCodEntidad=oss.eCodFacturarA ".
		" LEFT JOIN catbuques cbu ON cbu.eCodBuque=oss.eCodBuque ".
		" INNER JOIN cattiposservicios cto ON cto.eCodTipoServicio=oss.eCodTipoServicio ".
		" LEFT JOIN catareas car ON car.eCodArea=oss.eCodArea ".
		" LEFT JOIN cattiposcontenedores ctc ON ctc.eCodTipoContenedor=oss.eCodTipoContenedor ".
		" WHERE oss.eCodEntrada=".(int)$_GET['eCodEntrada'];
$rSolicitud=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

$select=" SELECT rs.*, ct.tNombre AS Archivo ".
		" FROM relentradasarchivos rs ".
		" LEFT JOIN cattiposarchivos ct ON ct.eCodTipoArchivo=rs.eCodTipoArchivo ".
		" WHERE rs.eCodEntrada=".(int)$_GET['eCodEntrada'];
$rsArchivos=mysqli_query($link,$select);
?>
<script type="text/javascript">
function consultar(){
	document.location = './?ePagina=2.5.5.4.php';
}
</script>
<div id="dvCNS" style="display:none;"></div>
<form action="" method="post" name="Datos" id="Datos" onsubmit="return false;">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr><td height="20"></td></tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"> C&oacute;digo</td>
	    <td width="50%" nowrap class="sanLR04"><?=sprintf("%07d",$rSolicitud{'eCodEntrada'});?></td>
		<td nowrap class="sanLR04"> Estatus</td>
	    <td width="50%" nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'Estatus'})?></td>
    </tr>
	<tr>
		<td height="23" nowrap class="sanLR04"> Fecha de Entrada</td>
	    <td nowrap class="sanLR04"><?=date("d/m/Y", strtotime($rSolicitud{'fhFechaEntrada'}));?></td>
    </tr>
	<tr>
		<td height="23" nowrap class="sanLR04"> Cliente</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'Cliente'})?></td>
		<td nowrap class="sanLR04"> Facturar a</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'FacturarA'})?></td>
    </tr>
	<tr>
		<td height="23" nowrap class="sanLR04"> Buque</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'Buque'})?></td>
		<td nowrap class="sanLR04"> Viaje</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'tNumeroViaje'})?></td>
    </tr>
	<tr>
		<td height="23" nowrap class="sanLR04"> Tipo de Servicio</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'TipoServicio'})?></td>
		<td height="23" nowrap class="sanLR04"> Area</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'Area'})?></td>
    </tr>
	<?php if((int)$rSolicitud{'eCodTipoServicio'}==1){ ?>
		<tr>
			<td height="23" nowrap class="sanLR04"> Contenedor</td>
			<td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'tCodContenedor'})?></td>
			<td height="23" nowrap class="sanLR04"> Tipo de Contenedor</td>
			<td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'TipoContenedor'})?></td>
		</tr>
		<tr>
			<td height="23" nowrap class="sanLR04"> Observaciones</td>
			<td nowrap class="sanLR04" colspan="3"><?=utf8_encode($rSolicitud{'tObservaciones'})?></td>
		</tr>
	<?php } 
	if((int)mysqli_num_rows($rsArchivos)>0){
		$eArchivo=1;
		while($rArchivo=mysqli_fetch_array($rsArchivos,MYSQLI_ASSOC)){ ?>
			<tr>
				<td height="23" nowrap class="sanLR04"><?=($eArchivo==1 ?  "Archivos" : "");?></td>
				<td nowrap class="" colspan="3"><a href="Patio/<?=$rArchivo{'tURL'};?>" target="_blank" class="sanT12" ><?=$rArchivo{'Archivo'};?></a></td>
			</tr>
			<?php $eArchivo++;
		}
	} ?>
</table>
</form>