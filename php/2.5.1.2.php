<?php
require_once("conexion/soluciones-mysql.php");
$link = getLink();
if($_POST){

	if ($_POST['fhFechaAsistenciaInicio']!=""){
  		$fhFechaAsistenciaInicio = $_POST['fhFechaAsistenciaInicio'].' 00:00:00';
  		$fhFechaAsistenciaFin = ($_POST['fhFechaAsistenciaFin']!="" ? $_POST['fhFechaAsistenciaFin'] : $_POST['fhFechaAsistenciaInicio']).' 23:59:59';
 	}
	$select=" SELECT DISTINCT mov.eCodMovimiento, mov.eCodUsuario, mov.fhFecha, mov.tCodTipoMovimiento, mov.eCodEstatus, mov.dImporte, mov.tFolio, mov.fhFechaRegistro , emp.Empleado AS empleado, ".			
			" tpo.eCodTipoEntidad,  tpo.tNombreCorto, usr.tNombre , usr.tApellidos ,  tmo.tNombre AS Movimiento  ".
			" FROM movimientosafiliatorios AS mov  ".
			" INNER JOIN relasistenciamovimientos AS rel ON rel.eCodMovimiento=mov.eCodMovimiento ".
			" LEFT JOIN cattipomovimientos AS tmo ON tmo.eCodMovimiento=mov.tCodTipoMovimiento ".
			" LEFT JOIN catusuarios AS usr ON usr.eCodUsuario=mov.eCodUsuario ".
			" LEFT JOIN asistencias AS ast ON rel.eCodAsistencia=ast.id	".			
			" LEFT JOIN empleados emp ON emp.id=ast.idEmpleado ".
			" LEFT JOIN cattipoempleado tpo ON emp.eCodTipo=tpo.eCodTipoEntidad ".			
			" WHERE 1=1 ".
			($_POST['eCodMovimiento'] 			? " AND mov.eCodMovimiento=".$_POST['eCodMovimiento'] 				: "").
			($_POST['folio'] 					? " AND mov.tFolio=".$_POST['folio'] 				: "").
			($_POST['Empleado'] 				? " AND emp.Empleado like '%".$_POST['Empleado']."%'" 	: "").
			($_POST['fhFechaAsistenciaInicio']	? " AND mov.fhFecha BETWEEN '".$fhFechaAsistenciaInicio. "' AND '" .$fhFechaAsistenciaFin."'" : "" ).			
			($_POST['tipoMovimiento'] 			? " AND mov.tCodTipoMovimiento=".$_POST['tipoMovimiento'] 	: "").			
			" Order by mov.fhFecha  ".( $_POST['orden'] ? $_POST['orden']  : "DESC" )." LIMIT ".( $_POST['limite'] ? $_POST['limite']  : "1000" );			



			//print($select);
	$rsSolicitudes=mysqli_query($link,$select);
	$rsUsuarios=mysqli_query($link,$select);
	$registros=(int)mysqli_num_rows($rsUsuarios);
	?>
    <table cellspacing="0" border="0" width="965px">
		<tr>
			<td width="50%"><hr color="#666666" /></td>
			<td class="sanLR04" nowrap="nowrap"><b>Registros Encontrados: ( <?=$registros;?> )</b></td>
			<td width="50%"><hr color="#666666" /></td>
		</tr>
    </table>
	<div style="display:block; top:0; left:0; width:965px; z-index=1; overflow: auto; height:400px;">
	<table cellspacing="0" border="0" width="965px">
		<thead>
			<tr class="thEncabezado">
				<td nowrap="nowrap" class="sanLR04" height="20" align="center">E</td>
				<td nowrap="nowrap" class="sanLR04">C&oacute;digo</td>				
				<td nowrap="nowrap" class="sanLR04">Fecha</td>
				<td nowrap="nowrap" class="sanLR04" >S.D.I.</td>
				<td nowrap="nowrap" class="sanLR04" >Empleado</td>
				<td nowrap="nowrap" class="sanLR04" >T. Empleado</td>
				<td nowrap="nowrap" class="sanLR04" >Folio</td>
				<td nowrap="nowrap" class="sanLR04" >Movimiento</td>
				<td nowrap="nowrap" class="sanLR04" >Usuario</td>
				<td nowrap="nowrap" class="sanLR04" width="100%">F. Registro</td>
			</tr>
		</thead>
		<tbody>
			<?php $i=1; while($rSolicitud=mysqli_fetch_array($rsSolicitudes,MYSQLI_ASSOC)){ 				

				?>
				<tr>
					<td nowrap="nowrap" class="sanLR04" height="20" align="center"><img width="16" height="16" alt="Empresa" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-AC.png"></td>
					<td nowrap="nowrap" class="sanLR04 colmenu"><b><?=sprintf("%07d",$rSolicitud{'eCodMovimiento'});?></b></td>					
					<td nowrap="nowrap" class="sanLR04 columnB"><?=date("d/m/Y", strtotime($rSolicitud{'fhFecha'}));?></td>
					<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rSolicitud{'dImporte'});?></td>
					<td nowrap="nowrap" class="sanLR04 columnB" ><?=utf8_encode($rSolicitud{'empleado'});?></td>
					<td nowrap="nowrap" class="sanLR04 " ><?=utf8_encode($rSolicitud{'tNombreCorto'});?></td>
					<td nowrap="nowrap" class="sanLR04 columnB" ><?=utf8_encode($rSolicitud{'tFolio'});?></td>
					<td nowrap="nowrap" class="sanLR04 " ><?=utf8_encode($rSolicitud{'Movimiento'});?></td>
					<td nowrap="nowrap" class="sanLR04 columnB" ><?=utf8_encode($rSolicitud{'tNombre'}." ".$rSolicitud{'tApellidos'});?></td>					
					<td nowrap="nowrap" class="sanLR04 " width="100%"><?=date("d/m/Y h:i:s", strtotime($rSolicitud{'fhFechaRegistro'}));?></td>
				</tr>
			<?php $i++; } ?>
		</tbody>
	</table>
	</div>
<?php }else{ 
	$select=" SELECT * ".
			" FROM cattipomovimientos ";			
	$rsTiposMovimiento=mysqli_query($link,$select);
	?>
	<script type="text/javascript">
	dojo.require("dojo.number");
	dojo.require("dijit.form.NumberTextBox");
	dojo.require("dijit.form.DateTextBox");
	dojo.require("dojo.io.iframe");	
	

	function generaExcel(){
		var UrlExcel = "php/excel/2.5.1.2.php?"+
		(dojo.byId('eCodMovimiento').value ? "&eCodMovimiento="+dojo.byId('eCodMovimiento').value : "")+
		(dojo.byId('fhFechaAsistenciaInicio').value ? "&fhFechaAsistenciaInicio="+dojo.byId('fhFechaAsistenciaInicio').value : "")+
		(dojo.byId('fhFechaAsistenciaFin').value ? "&fhFechaAsistenciaFin="+dojo.byId('fhFechaAsistenciaFin').value : "")+		
		(dojo.byId('Empleado').value ? "&Empleado="+dojo.byId('Empleado').value : "")+		
		(dojo.byId('folio').value ? "&folio="+dojo.byId('folio').value : "")+		
		(dojo.byId('tipoMovimiento').value ? "&tipoMovimiento="+dojo.byId('tipoMovimiento').value : "")+		
		(dojo.byId('orden').value ? "&orden="+dojo.byId('orden').value : "")+
		(dojo.byId('limite').value ? "&limite="+dojo.byId('limite').value : "");		
		var Datos = document.createElement("FORM");
		document.body.appendChild(Datos);
		Datos.name='Datos';
		Datos.method = "POST";
		Datos.action = UrlExcel;
		Datos.submit();
		}
	
	dojo.addOnLoad(function(){filtrarConsulta();});
	</script>
	<form name="Datos" id="Datos" method="post" action="" onsubmit="return false;">
		<input type="hidden" value="0" name="ePagina" id="ePagina" />
		<input type="hidden" value="" name="eAccion" id="eAccion" />
		<input type="hidden" value="<?=(int)$_SESSION['sesionUsuario']['eCodEntidad'];?>" name="eEntidad" id="eEntidad" />
		<table width="965px" border="0">
			<tr>
				<td colpan="3" width="100%"></td>
				<td align="right" nowrap="nowrap"><a class="fntBR11" href="javaScript:mostrarFiltros('Busqueda');">Busqueda de Registros</a></td>
			</tr>
			<tr id="trBusqueda" style="display:none">
				<td colspan="4">
					<table width="100%"  bgcolor="#f9f9f9">
						<tr><td class="sanLR04" height="5"></td></tr>
						<tr>
							<td class="sanLR04" height="20">C&oacute;digo</td>
							<td class="sanLR04" width="40%"><input type="text" name="eCodMovimiento" dojoType="dijit.form.TextBox" id="eCodMovimiento" value="" style="width:80px; height:25px;"></td>
							<td class="sanLR04" nowrap="nowrap">Fecha de Movimiento</td>
							<td class="sanLR04" width="50%">
							<input name="fhFechaAsistenciaInicio" id="fhFechaAsistenciaInicio" type="date"  required="false"  /> -
							<input name="fhFechaAsistenciaFin" id="fhFechaAsistenciaFin" type="date"  required="false"  />
							</td>
						</tr>
						<tr>
							<td class="sanLR04" height="20" nowrap="nowrap">Tipo de Movimiento</td>
							<td class="sanLR04" width="50%">
								<select name="tipoMovimiento" id="tipoMovimiento" style="width:180px; height:25px;">
									<option value="">Seleccione...</option>
									<?php while($rTipoMovimiento=mysqli_fetch_array($rsTiposMovimiento,MYSQLI_ASSOC)){ ?>
										<option value="<?=$rTipoMovimiento{'eCodMovimiento'}?>"><?=utf8_encode($rTipoMovimiento{'tNombre'})?></option>
									<?php } ?>
								</select>
							</td>
							<td class="sanLR04" height="20">Folio</td>
							<td class="sanLR04" width="50%"><input type="text" name="folio" dojoType="dijit.form.TextBox" id="folio" value="" style="width:142px; height:25px;"></td>
						</tr>
						<tr>
							<td class="sanLR04" height="20">Empleado</td>
							<td class="sanLR04" width="50%"><input type="text" name="Empleado" dojoType="dijit.form.TextBox" id="Empleado" value="" style="width:180px; height:25px;"></td>
							<td class="sanLR04" height="20"></td>
							<td class="sanLR04" width="50%"></td>
						</tr>
						</tr>
						<tr>
			              <td nowrap="nowrap" class="sanLR04" height="5"><b>[Ordenamiento]</b></td>
			              <td nowrap="nowrap" class="sanLR04" width="50%"><input type="radio" name="orden" id="orden"  value="ASC" > ASCENDENTE / <input type="radio" name="orden" id="orden" checked="checked" value="DESC" > DESENDENTE</td>
			              
			              <td nowrap="nowrap" class="sanLR04"><b>[Maximos Registros]:</b></td>
			              <td nowrap="nowrap" class="sanLR04" width="50%">
			              	<select name="limite" id="limite" style="width:142px; height:25px;">
                                  <option value="100">100</option>
                                  <option value="1000" selected>1,000</option>
                                  <option value="10000" >10,000</option>
                                  <option value="100000">100,000</option>
                          	</select>
                          </td>
            			</tr>
						<tr><td class="sanLR04" height="5"></td></tr>
					</table>
				</td>
			</tr>
			<tr><td colspan="4"><div id="dvCNS"></div></td></tr>
		</table>
	</form>
<?php } ?>