<?php require_once("conexion/soluciones-mysql.php"); 
require_once("conexion/facturacion.php");
$link = getLink();
$facturacion = new cFacturacion();
if($_POST){
	if($_POST['eProceso']==1){
		$res=$facturacion->facturar();
		print_r($res);
	}
}else{
$select=" SELECT oss.eCodSolicitud, ces.tNombre AS Estatus, oss.fhFecha, oss.fhFechaServicio, ".
		" cen.tNombre AS Naviera, oss.tPatente, cec.tNombre AS Cliente, cef.tNombre AS FacturarA, oss.tCodEstatus, ".
		" cbu.tNombre AS Buque, oss.tNumeroViaje, cts.tNombre AS TipoSolicitud, cto.tNombre AS TipoServicio, ".
		" oss.eCodTipoServicio, oss.eCodTipoSolicitud, oss.tBL, oss.tMotivoCancelacion ".
		" FROM opesolicitudesservicios oss ".
		" INNER JOIN catestatus ces ON ces.tCodEstatus=oss.tCodEstatus ".
		" LEFT JOIN catentidades cen ON cen.eCodEntidad=oss.eCodNaviera ".
		" INNER JOIN catentidades cec ON cec.eCodEntidad=oss.eCodCliente ".
		" INNER JOIN catentidades cef ON cef.eCodEntidad=oss.eCodFacturarA ".
		" LEFT JOIN catbuques cbu ON cbu.eCodBuque=oss.eCodBuque ".
		" INNER JOIN cattipossolicitudes cts ON cts.eCodTipoSolicitud=oss.eCodTipoSolicitud ".
		" INNER JOIN cattiposservicios cto ON cto.eCodTipoServicio=oss.eCodTipoServicio ".
		" WHERE oss.eCodSolicitud=".(int)$_GET['eCodSolicitud'];
$rSolicitud=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

$select=" SELECT rs.eCantidad, rs.dImporte, rs.dSubtotal, cs.tNombre AS Servicio ".
		" FROM relsolicitudesserviciosservicios rs ".
		" INNER JOIN catservicios cs ON cs.eCodServicio=rs.eCodServicio ".
		" WHERE rs.eCodSolicitud=".(int)$_GET['eCodSolicitud'];
$rsServicios=mysqli_query($link,$select);

$select=" SELECT rsi.dValorImpuesto, rsi.dImpuesto, ci.tNombre AS Impuesto ".
		" FROM relsolicitudesserviciosimpuestos rsi ".
		" INNER JOIN catimpuestos ci ON ci.eCodImpuesto=rsi.eCodImpuesto ".
		" WHERE rsi.eCodSolicitud=".(int)$_GET['eCodSolicitud'];
$rsImpuestos=mysqli_query($link,$select);

if((int)$rSolicitud{'eCodTipoSolicitud'}==1){
	$select=" SELECT ".((int)$rSolicitud{'eCodTipoServicio'}==1 ? "rs.tCodContenedor" : "")." AS tMercancia, ".
			((int)$rSolicitud{'eCodTipoServicio'}==1 ? "ctc.tNombreCorto" : "")." AS TipoEmbalaje, rs.tObservaciones ".
			" FROM relsolicitudesserviciosmercancias rs ".
			" LEFT JOIN cattiposcontenedores ctc ON ctc.eCodTipoContenedor=rs.eCodTipoContenedor ".
			" WHERE rs.eCodSolicitud=".(int)$_GET['eCodSolicitud'];
}else{
	$select=" SELECT ".((int)$rSolicitud{'eCodTipoServicio'}==1 ? "rs.tCodContenedor" : "")." AS tMercancia, ".
			((int)$rSolicitud{'eCodTipoServicio'}==1 ? "ctc.tNombreCorto" : "")." AS TipoEmbalaje, rs.tObservaciones ".
			" FROM opeentradasmercancias rs ".
			" INNER JOIN relsolicitudesserviciosmercanciassalidas rse ON rse.eCodEntrada=rs.eCodEntrada ".
			" LEFT JOIN cattiposcontenedores ctc ON ctc.eCodTipoContenedor=rs.eCodTipoContenedor ".
			" WHERE rse.eCodSolicitud=".(int)$_GET['eCodSolicitud'];
}
$rsMercancias=mysqli_query($link,$select);
$select=" SELECT rs.*, ct.tNombre AS Archivo ".
		" FROM relsolicitudesserviciosmercanciasarchivos rs ".
		" LEFT JOIN cattiposarchivos ct ON ct.eCodTipoArchivo=rs.eCodTipoArchivo ".
		" WHERE rs.eCodSolicitud=".(int)$_GET['eCodSolicitud'];
$rsArchivos=mysqli_query($link,$select);
?>
<script type="text/javascript">
function facturar(){
	dojo.byId('eProceso').value=1;
	var bandera = false;
	var bServicios = false;
	var bMercancias = false;
	var eTipoSolicitud = 0;
	mensaje = "¡Verifique lo siguiente!\n";

	if (!dojo.byId("eCodSolicitud").value){
		mensaje+="* Solicitud\n";
		bandera = true;		
	}
		
	if (bandera==true){
		alert(mensaje);
	}else{
		if (confirm("¿Desea Guardar la Información?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				alert(tRespuesta);
				/*if(dojo.byId("eCodigo").value>0){
					alert("¡La información se guardó correctamente!");
					tURL = './?ePagina=2.5.3.1.1.php&eCodSolicitud='+dojo.byId("eCodigo").value;
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}*/
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
		}
	}
}	

function consultar(){
	document.location = './?ePagina=2.5.3.3.php';
}
function nuevo(){
	document.location = './?ePagina=2.4.3.3.php';
	}

function generarPDF(){
	var urlPDF ='php/pdf/2.5.3.3.1.php?eCodSolicitud='+<?=$rSolicitud{'eCodSolicitud'}?>;
	var submitForm =document.createElement("FORM");
	document.body.appendChild(submitForm);
	submitForm.method="POST";
	submitForm.action=urlPDF;
	submitForm.target="_blank";
	submitForm.submit();	 
}

dojo.addOnLoad(function(){
	if(dojo.byId('tCodEstatus').value!='AU'){
		dojo.destroy("btn9_3");
	}
	if(dojo.byId('eFactura').value==''){
		dojo.destroy("btn11_4");
	}
})
</script>
<div id="dvCNS" style="display:none;"></div>
<form id="Datos" name="Datos" method="post" onSubmit="return false;" enctype="multipart/form-data">
<input type="hidden" name="eProceso" id="eProceso" value="" />
<input id="tCodEstatus" name="tCodEstatus" type="hidden" value="<?=$rSolicitud['tCodEstatus'];?>">
<input id="eFactura" name="eFactura" type="hidden" value="<?=$_GET['eFactura'];?>">
<input id="eCodSolicitud" name="eCodSolicitud" type="hidden" value="<?=(int)$_GET['eCodSolicitud'];?>">
<input id="eCodUsuario" name="eCodUsuario" type="hidden" value="<?=(int)$_SESSION['sesionUsuario']['eCodUsuario'];?>">
<table border="0" cellpadding="0" cellspacing="0" width="980px">
	<input id="codigo" name="codigo" type="hidden" value="<?=(int)$_GET['eCodSolicitud'];?>">
	<tr><td height="20"></td></tr>    
	<tr>
	    <td height="23" nowrap class="sanLR04"> C&oacute;digo</td>
	    <td width="50%" nowrap class="sanLR04"><?=sprintf("%07d",$rSolicitud{'eCodSolicitud'});?></td>
		<td nowrap class="sanLR04"> Estatus</td>
	    <td width="50%" nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'Estatus'})?></td>
    </tr>
	<tr>
		<td height="23" nowrap class="sanLR04"> Fecha de Solicitud</td>
	    <td nowrap class="sanLR04"><?=date("d/m/Y H:i", strtotime($rSolicitud{'fhFecha'}));?></td>
		<td nowrap class="sanLR04"> Fecha de Servicio</td>
	    <td nowrap class="sanLR04"><?=date("d/m/Y", strtotime($rSolicitud{'fhFechaServicio'}));?></td>
    </tr>
	
	<tr>
		<td height="23" nowrap class="sanLR04" valign="top"> Cliente</td>
	    <td class="sanLR04"><?=utf8_encode($rSolicitud{'Cliente'})?></td>
		<td nowrap class="sanLR04" valign="top"> Facturar a</td>
	    <td class="sanLR04"><?=utf8_encode($rSolicitud{'FacturarA'})?></td>
    </tr>
	<tr>
		<td height="23" nowrap class="sanLR04"> BL</td>
	    <td nowrap class="sanLR04"><b><?=utf8_encode($rSolicitud{'tBL'})?></b></td>
		<td nowrap class="sanLR04"></td>
	    <td nowrap class="sanLR04"></td>
    </tr>
	
	<tr>
		<td height="23" nowrap class="sanLR04"> Tipo de Solicitud</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'TipoSolicitud'})?></td>
		<td nowrap class="sanLR04"> Tipo de Servicio</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'TipoServicio'})?></td>
    </tr>
    <? if($rSolicitud{'tCodEstatus'}=='CA'){ ?>
        <tr>
            <td height="23" nowrap class="sanLR04"> Motivo de Cancelaci&oacute;n</td>
            <td nowrap class="sanLR04" colspan="3"><?=utf8_encode($rSolicitud{'tMotivoCancelacion'})?></td>
        </tr>
    <? } ?>
	<tr>
		<td height="23" nowrap class="sanLR04" colspan="4"> 
			<table cellspacing="0" border="0" width="965px">
				<thead>
					<tr class="thEncabezado">
						<td nowrap="nowrap" class="sanLR04" height="23">Cantidad</td>
						<td nowrap="nowrap" class="sanLR04" width="100%">Descripci&oacute;n</td>
						<td nowrap="nowrap" class="sanLR04">Precio Unitario</td>
						<td nowrap="nowrap" class="sanLR04" align="right">Total</td>
					</tr>
				</thead>
				<tbody>
					<?php 
					$dSubtotal=0;
					$dTotal=0;
					while($rServicio=mysqli_fetch_array($rsServicios,MYSQLI_ASSOC)){ ?>
						<tr>
							<td nowrap="nowrap" class="sanLR04" align="right"><?=sprintf("%02d",$rServicio{'eCantidad'});?></td>
							<td nowrap="nowrap" class="sanLR04" width="100%"><?=utf8_encode($rServicio{'Servicio'});?></a></td>
							<td nowrap="nowrap" class="sanLR04" align="right"><?=number_format($rServicio{'dImporte'},2);?></td>
							<td nowrap="nowrap" class="sanLR04" align="right"><?=number_format($rServicio{'dSubtotal'},2);?></td>
						</tr>
						<?php $dSubtotal=(float)$dSubtotal+(float)$rServicio{'dSubtotal'};
					} 
					$dTotal=$dSubtotal; ?>
					<tr>
						<td nowrap="nowrap" class="sanLR04" height="20" colspan="3" align="right"><b>Subtotal</b></td>
						<td nowrap="nowrap" class="sanLR04" align="right"><?=number_format($dSubtotal, 2);?></td>
					</tr>
					<?php while($rImpuesto=mysqli_fetch_array($rsImpuestos,MYSQLI_ASSOC)){ ?>
					<tr>
						<td nowrap="nowrap" class="sanLR04" height="20" colspan="3" align="right">
						<b><?=$rImpuesto{'Impuesto'}." ".number_format($rImpuesto{'dValorImpuesto'},2);?>%</b>
						</td>
						<td nowrap="nowrap" class="sanLR04" align="right"><?=number_format($rImpuesto{'dImpuesto'}, 2);?></td>
					</tr>
					<?php $dTotal=(float)$dTotal+(float)$rImpuesto{'dImpuesto'}; } ?>
					<tr>
						<td nowrap="nowrap" class="sanLR04" height="20" colspan="3" align="right"><b>Total</b></td>
						<td nowrap="nowrap" class="sanLR04" align="right"><?=number_format($dTotal, 2);?></td>
					</tr>
					<tr><td nowrap="nowrap" class="sanLR04" height="20" colspan="5"></td></tr>
				</tbody>
			</table>
		</td>
    </tr>
	<tr>
		<td colspan="4">
			<table cellspacing="0" border="0" width="965px" id="tablaMercancias" name="tablaMercancias">
				<thead>
					<tr class="thEncabezado">
						<td nowrap="nowrap" class="sanLR04" height="23"> <?=((int)$rSolicitud{'eCodTipoServicio'}==1 ? "Contenedor" : "Mercanc&iacute;a");?></td>
						<td nowrap="nowrap" class="sanLR04"> <?=((int)$rSolicitud{'eCodTipoServicio'}==1 ? "Tipo" : "Embalaje");?></td>
						<td nowrap="nowrap" class="sanLR04"> Observaciones</td>
						<td nowrap="nowrap" class="sanLR04" width="100%"></td>
					</tr>
				</thead>
				<tbody id="tbMercancias" name="tbMercancias">
					<?php while($rMercancia=mysqli_fetch_array($rsMercancias,MYSQLI_ASSOC)){ ?>
						<tr>
							<td nowrap="nowrap" class="sanLR04" height="23"><?=utf8_encode($rMercancia{'tMercancia'});?></td>
							<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rMercancia{'TipoEmbalaje'});?></td>
							<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rMercancia{'tObservaciones'});?></td>
							<td nowrap="nowrap" class="sanLR04"></td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</td>
	</tr>
	<?php if((int)mysqli_num_rows($rsArchivos)>0){ ?>
		<tr><td height="23"></td></tr>
		<?php $eArchivo=1;
		while($rArchivo=mysqli_fetch_array($rsArchivos,MYSQLI_ASSOC)){ ?>
			<tr>
				<td height="23" nowrap class="sanLR04"><?=($eArchivo==1 ?  "Archivos" : "");?></td>
				<td nowrap class="sanLR04" colspan="3"><a href="Patio/<?=$rArchivo{'tURL'};?>" target="_blank" class="sanT12" ><?=$rArchivo{'Archivo'};?></a></td>
			</tr>
			<?php $eArchivo++;
		}
	} ?>
</table>
</form>
<?php } ?>