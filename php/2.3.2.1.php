<style>
#divgral {
     overflow:scroll;
     height:450px;
     width:900px;
}
</style>
<?php require_once("conexion/soluciones-mysql.php");
$link = getLink();
if($_POST){
	if($_POST['eAccion']==1){
		foreach($_POST as $k => $valor){
			$nombre = strval($k);
			$campo = "eCodEntidad";
			if(strstr($nombre,$campo) && (int)$valor>0){
				$update=" UPDATE empleados SET Estado='CA', thFechaBaja=current_timestamp WHERE id=".$valor;
				mysqli_query($link,$update);
			}
		}
	}

	if($_POST['eAccion']==2){
		foreach($_POST as $k => $valor){
			$nombre = strval($k);
			$campo = "eCodEntidad";
			if(strstr($nombre,$campo) && (int)$valor>0){
				$eFila=str_replace("eCodEntidad", "", $nombre);
				$eDescanso=(int)$_POST['eCodDia'.$eFila];

				$update=" UPDATE empleados SET descanso=".$eDescanso." WHERE id=".$valor;
				mysqli_query($link,$update);
			}
		}
	}

	if($_POST['eAccion']==3){
		foreach($_POST as $k => $valor){
			$nombre = strval($k);
			$campo = "eEntidad";
			if(strstr($nombre,$campo) && (int)$valor>0){
				$eFila=str_replace("eEntidad", "", $nombre);
				$eCodEntidad=(int)$_POST['eCodEmpresa'.$eFila];

				$insert=" INSERT INTO logempleadosempresas (eCodEmpleado, eCodEntidad, eCodUsuario, fhFechaRegistro)
						SELECT ".$valor.", eCodEntidad, ".(int)$_POST['eUsuario'].", CURRENT_TIMESTAMP
						FROM empleados
						WHERE id=".$valor;
				mysqli_query($link,$insert);

				$update=" UPDATE empleados SET eCodEntidad=".$eCodEntidad." WHERE id=".$valor;
				mysqli_query($link,$update);
			}
		}
	}

	$select=" SELECT emp.*, tpe.eCodTipoEntidad, tpe.tNombre as tipo, rac.tURLArchivo, cre.tNombre as credito, ".
			" dia.tNombre as dDescanso, cat.Categoria, catf.Categoria AS CategoriaFestivo,
			catv.Categoria AS CategoriaVacacion ".
			" FROM empleados as emp ".
			" INNER JOIN cattipoempleado as tpe on tpe.eCodTipoEntidad=emp.eCodTipo ".
			" LEFT JOIN cattipocredito cre ON cre.eCodCredito=emp.eCodTipoCredito ".
			" LEFT JOIN catdias as dia on dia.eCodDia=emp.descanso ".
			" LEFT JOIN categorias cat ON cat.indice=emp.eCodCategoria ".
			" LEFT JOIN categorias catf ON catf.indice=emp.eCodCategoriaFestivo ".
			" LEFT JOIN categorias catv ON catv.indice=emp.eCodCategoriaVacacion ".
			" LEFT JOIN relarchivoscsf rac ON rac.eCodEmpleado=emp.id ".
			" WHERE 1=1".
			
			($_POST['tRFC']						? " AND emp.rfc LIKE '%".$_POST['tRFC']."%'"						    : "").
			($_POST['tNSS']						? " AND emp.IMSS LIKE '%".$_POST['tNSS']."%'"					        : "").
			($_POST['tCURP']					? " AND emp.curp LIKE '%".$_POST['tCURP']."%'"					      	: "").
			($_POST['tNombre']					? " AND emp.Empleado LIKE '%".$_POST['tNombre']."%'"		          	: "").
			($_POST['tURLArchivo']          	? " AND rac.tURLArchivo LIKE '%".$_POST['tURLArchivo']."%'"             : "").
			($_POST['tCalle']               	? " AND emp.tCalle LIKE '%".$_POST['tCalle']."%'"                     	: "").
			($_POST['eNumeroInt']           	? " AND emp.eNumeroInt LIKE '%".$_POST['eNumeroInt']."%'"         		: "").
			($_POST['eNumeroExt']           	? " AND emp.eNumeroExt LIKE '%".$_POST['eNumeroExt']."%'"           	: "").
			($_POST['eCodigoPostal']        	? " AND emp.eCodigoPostal LIKE '%".$_POST['eCodigoPostal']."%'"  		: "").
			($_POST['tEstado']              	? " AND emp.tEstado LIKE '%".$_POST['tEstado']."%'"                		: "").
			($_POST['tCiudad']              	? " AND emp.tCiudad LIKE '%".$_POST['tCiudad']."%'"                		: "").
			($_POST['tColonia']             	? " AND emp.tColonia LIKE '%".$_POST['tColonia']."%'"                 	: "").
			($_POST['tCodEstatus']				? " AND emp.Estado='".$_POST['tCodEstatus']."'"					 		: "").
			($_POST['eCodEmpresa']				? " AND emp.eCodEntidad=".(int)$_POST['eCodEmpresa']		     		: "").
			($_POST['eCodEntidad']				? " AND emp.id=".$_POST['eCodEntidad']							 		: "").
			($_POST['tipoempleado']				? " AND tpe.eCodTipoEntidad=".$_POST['tipoempleado']			 		: "").
			($_POST['eCodCategoria']			? " AND emp.eCodCategoria=".$_POST['eCodCategoria']			 			: "").
			($_POST['eCodTipoCredito']			? " AND emp.eCodTipoCredito=".$_POST['eCodTipoCredito']			 		: "").
			($_POST['eCodCategoriaFestivo']		? " AND emp.eCodCategoriaFestivo=".$_POST['eCodCategoriaFestivo']	    : "").
			($_POST['eCodCategoriaVacacion']	? " AND emp.eCodCategoriaVacacion=".$_POST['eCodCategoriaVacacion']		: "").
			" ORDER BY emp.id ".($_POST['orden'] ? $_POST['orden'] : "ASC").
			" LIMIT ".( $_POST['limite'] ? $_POST['limite'] : "100");
	$rsEntidades=mysqli_query($link,$select); 
	$registros=(int)mysqli_num_rows($rsEntidades); ?>
    <table cellspacing="0" border="0" width="100%">
	    <tr>
		    <td width="50%"><hr color="#666666" /></td>
		    <td class="sanLR04" nowrap="nowrap"><b>Registros Encontrados: ( <?=$registros;?> )</b></td>
			<td width="50%"><hr color="#666666" /></td>
	    </tr>
    </table>
	<div id="divgral" style="display:block; top:0; left:0; width:970px; z-index=1; overflow: auto;">
	<table cellspacing="0" border="0" width="900px" >		
		<thead>
			<tr class="thEncabezado">
				<td nowrap="nowrap" class="sanLR04" height="20" align="center" title="D&iacute;a Descanso">D</td>
				<td nowrap="nowrap" class="sanLR04" height="20" align="center" title="Entidad">E</td>
				<td nowrap="nowrap" class="sanLR04">E</td>
				<td nowrap="nowrap" class="sanLR04">CSF</td>
				<td nowrap="nowrap" class="sanLR04">C&oacute;digo</td>
                <td nowrap="nowrap" class="sanLR04">RFC</td>
                <td nowrap="nowrap" class="sanLR04">Descanso</td>
                <td nowrap="nowrap" class="sanLR04">Empresa</td>
				<td nowrap="nowrap" class="sanLR04">Tipo</td>
				<td nowrap="nowrap" class="sanLR04">NSS</td>
				<td nowrap="nowrap" class="sanLR04">Nombre</td>
				<td nowrap="nowrap" class="sanLR04">Calle</td>
				<td nowrap="nowrap" class="sanLR04">Numero Interior</td>
				<td nowrap="nowrap" class="sanLR04">Numero Exterior</td>
				<td nowrap="nowrap" class="sanLR04">Codigo Postal</td>
				<td nowrap="nowrap" class="sanLR04">Estado</td>
				<td nowrap="nowrap" class="sanLR04">Ciudad</td>
				<td nowrap="nowrap" class="sanLR04">Colonia</td>
				<td nowrap="nowrap" class="sanLR04">Categoria</td>
				<td nowrap="nowrap" class="sanLR04">Categoria Festivo</td>
				<td nowrap="nowrap" class="sanLR04">Categoria Vacaciones</td>
				<td nowrap="nowrap" class="sanLR04">CURP</td>
				<td nowrap="nowrap" class="sanLR04">Fecha Alta</td>
				<td nowrap="nowrap" class="sanLR04" width="100%">Fecha Ingreso</td>
			</tr>
		</thead>
		<tbody>
			<?php $i=1; while($rEntidad=mysqli_fetch_array($rsEntidades,MYSQLI_ASSOC)){ 
				$select=" SELECT * ".
						" FROM catdias ".
						" WHERE 1=1 ".
						($rEntidad{'eCodTipo'}==2 ? " AND eCodDia=1 " : "").
						" ORDER BY eCodDia ASC ";
				$rsDescansos=mysqli_query($link,$select);

				$select=" SELECT * FROM catentidades WHERE tCodEstatus='AC' ";
				$rsEmpresas=mysqli_query($link,$select);
				?>
				<tr>
					<td nowrap="nowrap" class="sanLR04" height="20" align="center"><input type="checkbox" name="eCodEntidad<?=$i?>" id="eCodEntidad<?=$i?>" value="<?=$rEntidad{'id'};?>" ></td>
					<td nowrap="nowrap" class="sanLR04" height="20" align="center"><input type="checkbox" name="eEntidad<?=$i?>" id="eEntidad<?=$i?>" value="<?=$rEntidad{'id'};?>" ></td>
					<td nowrap="nowrap" class="sanLR04"><img width="16" height="16" alt="Usuario" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-<?=$rEntidad{'Estado'};?>.png"></td>
				<?php if($rEntidad{'tURLArchivo'} != '') { ?>
					<td nowrap="nowrap" class="sanLR04"><a href="<?php echo $rEntidad{'tURLArchivo'} ?>"><img width="16" height="16" alt="csf" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-pdf.png"></a></td>
				<?php } else {?>
					<td nowrap="nowrap" class="sanLR04"></td>
				<?php }?>
					<td nowrap="nowrap" class="sanLR04 colmenu" ><a href="?ePagina=2.3.2.1.1.php&eCodEntidad=<?=$rEntidad{'id'};?>"><b><?=sprintf("%07d",$rEntidad{'id'});?></b></a></td>
					<td nowrap="nowrap" class="sanLR04"><a class="txtCO12" href="?ePagina=2.3.2.1.2.php&eCodEntidad=<?=$rEntidad{'id'};?>"><?=utf8_encode($rEntidad{'RFC'});?></a></td>
					<td nowrap="nowrap" class="sanLR04 columnB">
						<select name="eCodDia<?=$i?>" id="eCodDia<?=$i?>" style="width:105px" onchange="mChk(<?=$i?>);">
							<?php while($rDescanso=mysqli_fetch_array($rsDescansos,MYSQLI_ASSOC)){ ?>
								<option value="<?=$rDescanso{'eCodDia'}?>" <?=($rDescanso{'eCodDia'}==$rEntidad{'descanso'} ? "selected=\"selected\"" : "");?> ><?=utf8_encode($rDescanso{'tNombre'})?></option>
							<?php } ?>
						</select>
					</td>
					<td nowrap="nowrap" class="sanLR04">
						<select name="eCodEmpresa<?=$i?>" id="eCodEmpresa<?=$i?>" style="width:105px" onchange="mChkEn(<?=$i?>);">
							<option value=''>Seleccione...</option>
							<?php while($rEntidade=mysqli_fetch_array($rsEmpresas,MYSQLI_ASSOC)){ ?>
								<option value="<?=$rEntidade{'eCodEntidad'}?>" <?=($rEntidade{'eCodEntidad'}==$rEntidad{'eCodEntidad'} ? "selected=\"selected\"" : "");?> ><?=utf8_encode($rEntidade{'tSiglas'})?></option>
							<?php } ?>
						</select>
					</td>
                    <td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rEntidad{'tipo'});?></a></td>
                    <td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rEntidad{'IMSS'});?></a></td>
                    <td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rEntidad{'Empleado'});?></a></td>
					<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rEntidad{'tCalle'});?></a></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rEntidad{'eNumeroInt'});?></a></td>
					<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rEntidad{'eNumeroExt'});?></a></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rEntidad{'eCodigoPostal'});?></a></td>
					<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rEntidad{'tEstado'});?></a></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rEntidad{'tCiudad'});?></a></td>
					<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rEntidad{'tColonia'});?></a></td>
                    <td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rEntidad{'Categoria'});?></a></td>
                    <td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rEntidad{'CategoriaFestivo'});?></a></td>
                    <td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rEntidad{'CategoriaVacacion'});?></a></td>
				<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rEntidad{'CURP'});?></td>
				<td nowrap="nowrap" class="sanLR04 columnB" width="100%"><?=($rEntidad{'fhFechaIngreso'} ? date("d/m/Y", strtotime($rEntidad{'fhFechaIngreso'})) : "");?></td>
				<td nowrap="nowrap" class="sanLR04" width="100%"><?=($rEntidad{'fhFechaIngresoReal'} ? date("d/m/Y", strtotime($rEntidad{'fhFechaIngresoReal'})) : "");?></td>
				</tr>
			<?php $i++; } ?>
		</tbody>
	</table>
	</div>
<?php }else{ 
$select=" SELECT * ".
		" FROM categorias ".
		" ORDER BY Categoria ASC ";
$rsCategorias = mysqli_query($link,$select);

$select=" SELECT *
		FROM catentidades 
		WHERE tCodEstatus='AC' 
		ORDER BY tSiglas ";
$rsEntidades=mysqli_query($link,$select); ?>	
<script type="text/javascript">
	dojo.require("dojo.number");
	dojo.require("dijit.form.NumberTextBox");
	dojo.require("dijit.form.DateTextBox");
	dojo.require("dojo.io.iframe");

	function nuevo(){
		document.location = "?ePagina=2.3.2.1.1.php";
	}

	function eliminar(){
		var eChk = 0;
		dojo.byId('eAccion').value = 1;
		dojo.query("[id*=\"eCodEntidad\"]:checked").forEach(function(nodo, index, array){eChk++;});
		if(eChk!=0){
			if(confirm("¿Desea eliminar el registro?")){
				dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+'.php', handleAs: "text", load: function(tRespuesta, ioArgs){
					dojo.byId('dvCNS').innerHTML = tRespuesta; 
					alert("¡Los registros se han eliminado exitosamente!");	
				}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});
			}
		}else{
			alert("No ha seleccionado ningun Empleado");
		}
		dojo.byId('eAccion').value = "";	
	}

	function mChk(fila){
		dojo.byId('eCodEntidad'+fila).checked=true;
	}

	function mChkEn(fila){
		dojo.byId('eEntidad'+fila).checked=true;
	}

	function descanso(){
		var eChk = 0;
		dojo.byId('eAccion').value = 2;
		dojo.query("[id*=\"eCodEntidad\"]:checked").forEach(function(nodo, index, array){eChk++;});
		if(eChk!=0){
			if(confirm("¿Desea actualizar el registro?")){
				dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+'.php', handleAs: "text", load: function(tRespuesta, ioArgs){
					dojo.byId('dvCNS').innerHTML = tRespuesta; 
					alert("¡Los registros se han actualizado exitosamente!");	
				}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});
			}
		}else{
			alert("Seleccionar empleado(s)!");
		}
		dojo.byId('eAccion').value = "";	
	}

	function entidad(){
		var eChk = 0;
		dojo.byId('eAccion').value = 3;
		dojo.query("[id*=\"eEntidad\"]:checked").forEach(function(nodo, index, array){eChk++;});
		if(eChk!=0){
			if(confirm("¿Desea actualizar el registro?")){
				dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+'.php', handleAs: "text", load: function(tRespuesta, ioArgs){
					dojo.byId('dvCNS').innerHTML = tRespuesta; 
					alert("¡Los registros se han actualizado exitosamente!");	
				}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});
			}
		}else{
			alert("Seleccionar empleado(s)!");
		}
		dojo.byId('eAccion').value = "";	
	}	

	function generaExcel(){
		var UrlExcel = "php/excel/2.3.2.1.php?"+
		(dojo.byId('tRFC').value			? "&tRFC="+dojo.byId('tRFC').value						: "")+
		(dojo.byId('tNSS').value			? "&tNSS="+dojo.byId('tNSS').value						: "")+
		(dojo.byId('tCURP').value		? "&tCURP="+dojo.byId('tCURP').value					: "")+
		(dojo.byId('orden').value		? "&orden="+dojo.byId('orden').value					: "")+
		(dojo.byId('limite').value		? "&limite="+dojo.byId('limite').value					: "")+
		(dojo.byId('tNombre').value 		? "&tNombre="+dojo.byId('tNombre').value				: "")+
		(dojo.byId('tCodEstatus').value	? "&tCodEstatus="+dojo.byId('tCodEstatus').value			: "")+
		(dojo.byId('eCodEntidad').value 	? "&eCodEntidad="+dojo.byId('eCodEntidad').value			: "")+
		(dojo.byId('tipoempleado').value	? "&tipoempleado="+dojo.byId('tipoempleado').value		: "")+
		(dojo.byId('eCodCategoria').value	? "&eCodCategoria="+dojo.byId('eCodCategoria').value		: "")+
		(dojo.byId('eCodTipoCredito').value ? "&eCodTipoCredito="+dojo.byId('eCodTipoCredito').value	: "");
		var Datos = document.createElement("FORM");
		document.body.appendChild(Datos);
		Datos.name='Datos';
		Datos.method = "POST";
		Datos.action = UrlExcel;
		Datos.submit();
	}

	dojo.addOnLoad(function(){ filtrarConsulta();});
	</script>
	<form name="Datos" id="Datos" method="post" action="" onsubmit="return false;">
		<input type="hidden" value="0" name="ePagina" id="ePagina" />
		<input type="hidden" value="" name="eAccion" id="eAccion" />
		<input type="hidden" value="<?=(int)$_SESSION['sesionUsuario']['eCodUsuario'];?>" name="eUsuario" id="eUsuario" />
		<table width="100%" border="0">
			<tr>
				<td colpan="3" width="100%"></td>
				<td align="right" nowrap="nowrap"><a class="fntBR11" href="javaScript:mostrarFiltros('Busqueda');">Busqueda de Registros</a></td>
			</tr>
			<tr id="trBusqueda" style="display:none">
				<td colspan="4">
					<table width="100%" bgcolor="#f9f9f9">
						<tr><td class="sanLR04" height="5"></td></tr>
						<tr>
							<td class="sanLR04" height="20">C&oacute;digo</td>
							<td class="sanLR04" width="50%"><input type="text" name="eCodEntidad" dojoType="dijit.form.TextBox" id="eCodEntidad" value="" style="width:80px"></td>
							<td class="sanLR04">RFC</td>
							<td class="sanLR04" width="50%"><input type="text" name="tRFC" dojoType="dijit.form.TextBox" id="tRFC" value="" style="width:175px"></td>
						</tr>
						<tr>
							<td class="sanLR04" height="20">Nombre</td>
							<td class="sanLR04" width="50%"><input type="text" name="tNombre" dojoType="dijit.form.TextBox" id="tNombre" value="" style="width:175px"></td>
							<td class="sanLR04" >CURP</td>
							<td class="sanLR04" width="50%"><input type="text" name="tCURP" dojoType="dijit.form.TextBox" id="tCURP" value="" style="width:175px"></td>
							
						</tr>
						<tr>
				            <td nowrap="nowrap" class="sanLR04" height="20">Tipo de Empleado</td>
				            <td class="sanLR04" width="50%"><select name='tipoempleado' id='tipoempleado' style="width:175px">
				                <option value=''>Seleccione...</option>
				                <?php $sel = mysqli_query($link,"SELECT eCodTipoEntidad, tNombre FROM cattipoempleado where tCodEstatus='AC'");                
				                              while($row = mysqli_fetch_array($sel,MYSQLI_ASSOC)){ ?>
				                <option value='<?php echo $row["eCodTipoEntidad"]; ?>'> <?php echo $row["tNombre"]; ?> </option>
				                <?php } ?>
				              </select>
				            </td>
				            <td class="sanLR04" nowrap="nowrap">NSS</td>
							<td class="sanLR04" width="50%"><input type="text" name="tNSS" dojoType="dijit.form.TextBox" id="tNSS" value="" style="width:175px"></td>
				        </tr>
				        <tr>
				            <td class="sanLR04" nowrap="nowrap">Categoria</td>
							<td class="sanLR04" width="50%">
								<select name='eCodCategoria' id='eCodCategoria' style="width:175px">
				                	<option value=''>Seleccione...</option>
				                	<?php while($rCategoria=mysqli_fetch_array($rsCategorias,MYSQLI_ASSOC)){ ?>
				                		<option value='<?=$rCategoria["indice"];?>'><?=$rCategoria["Categoria"];?></option>
				                	<?php } ?>
				              </select>
						  </td>
						  <td nowrap="nowrap" class="sanLR04" height="20">Tipo de Credito</td>
				            <td class="sanLR04" width="50%">
				            <select name='eCodTipoCredito' id='eCodTipoCredito' style="width:175px">
				                <option value=''>Seleccione...</option>
				                <?php   $select=" SELECT * ".
										" FROM cattipocredito ".
										" WHERE tCodEstatus='AC' ".
										" ORDER BY tNombre ASC ";
										$rsTiposCredito = mysqli_query($link,$select);				                
				                        while($row = mysqli_fetch_array($rsTiposCredito,MYSQLI_ASSOC)){ ?>
				                			<option value='<?php echo $row["eCodCredito"]; ?>'> <?php echo $row["tNombre"]; ?> </option>
				                <?php } ?>
				              </select>
				            </td>
				        </tr>
				        <tr>
				            <td class="sanLR04" nowrap="nowrap">Categoria Festivo</td>
							<td class="sanLR04" width="50%">
								<select name='eCodCategoriaFestivo' id='eCodCategoriaFestivo' style="width:175px">
				                	<option value=''>Seleccione...</option>
				                	<?php 
				                	mysqli_data_seek($rsCategorias,0);
				                	while($rCategoria=mysqli_fetch_array($rsCategorias,MYSQLI_ASSOC)){ ?>
				                		<option value='<?=$rCategoria["indice"];?>'><?=$rCategoria["Categoria"];?></option>
				                	<?php } ?>
				              </select>
						  </td>
				            <td nowrap="nowrap" class="sanLR04" height="20">Empresa</td>
				            <td class="sanLR04" width="50%">
					            <select name='eCodEmpresa' id='eCodEmpresa' style="width:175px">
					                <option value=''>Seleccione...</option>
					                <?php while($rEntidad = mysqli_fetch_array($rsEntidades,MYSQLI_ASSOC)){ ?>
					                	<option value='<?=$rEntidad["eCodEntidad"];?>'> <?=$rEntidad["tSiglas"]; ?> </option>
					                <?php } ?>
					            </select>
				            </td>
				        </tr>
				        <tr>
				            <td class="sanLR04" nowrap="nowrap">Categoria Vacaciones</td>
							<td class="sanLR04" width="50%">
								<select name='eCodCategoriaVacacion' id='eCodCategoriaVacacion' style="width:175px">
				                	<option value=''>Seleccione...</option>
				                	<?php 
				                	mysqli_data_seek($rsCategorias,0);
				                	while($rCategoria=mysqli_fetch_array($rsCategorias,MYSQLI_ASSOC)){ ?>
				                		<option value='<?=$rCategoria["indice"];?>'><?=$rCategoria["Categoria"];?></option>
				                	<?php } ?>
				              </select>
						  </td>
				            <td nowrap="nowrap" class="sanLR04" height="20">Estatus</td>
				            <td class="sanLR04" width="50%">
				            	<select name='tCodEstatus' id='tCodEstatus' style="width:175px">
				                	<option value=''>Seleccione...</option>
				                	<option value='AC'>Activo</option>
				                	<option value='CA'>Cancelado</option>
				              </select>
				            </td>
				        </tr>
				        <tr>
			              <td nowrap="nowrap" class="sanLR04" height="5"><b>[Ordenamiento]</b></td>
			              <td nowrap="nowrap" class="sanLR04" width="50%"><input type="radio" name="orden" id="orden" checked="checked" value="ASC" > ASCENDENTE / <input type="radio" name="orden" id="orden" value="DESC" > DESENDENTE</td>
			              
			              <td nowrap="nowrap" class="sanLR04"><b>[Maximos Registros]:</b></td>
			              <td nowrap="nowrap" class="sanLR04" width="50%">
			              	<select name="limite" id="limite" style="width:175px">
                                  <option value="100">100</option>
                                  <option value="1000">1,000</option>
                                  <option value="10000">10,000</option>
                                  <option value="100000">100,000</option>
                          	</select>
                          </td>
            			</tr>
						<tr><td class="sanLR04" height="5"></td></tr>
					</table>
				</td>
			</tr>
			<tr><td colspan="4"><div id="dvCNS"></div></td></tr>
		</table>
	</form>	
<?php } ?>