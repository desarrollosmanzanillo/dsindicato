﻿<?php require_once("conexion/soluciones-mysql.php");
$link = getLink();
if($_POST){
	date_default_timezone_set('America/Mexico_City');
	$fhHoy=isset($_POST["fecha"]) ? $_POST["fecha"] : date("Y-m-d");
	$fhAyer = date('Y-m-d',strtotime('-1 days', strtotime($fhHoy)));
  $select=" SELECT DISTINCT asis.idEmpleado, asis.idEmpresa, asis.folioIMSS, asis.Estado AS tCodEstatus, asis.Fecha, empl.id, ".
          " empl.Empleado, empl.IMSS AS imss, empl.Paterno AS pat, empl.Materno AS mat, empl.Nombre AS nomb, empl.Clinica AS clin, ".
          " empl.CURP AS curp, caen.tRegistroPatronal AS rp, ctemp.dFactorIntegracion AS FactorIntegracion, cts.tNombre AS TipoSalario, ".
          " ctemp.eCodTipoJornada, (SELECT MAX(asi1.Salario) FROM asistencias asi1 WHERE asi1.Estado='AC' AND asi1.Fecha='".$fhHoy."' AND asi1.idEmpleado=asis.idEmpleado) ".
          " AS Salario, ctemp.eCodTipoTrabajador, ctemp.tNombreCorto AS TipoEmpleado ".
          " FROM asistencias asis ".
          " INNER JOIN empleados empl ON empl.id=asis.idEmpleado ".
          " INNER JOIN cattipoempleado ctemp ON ctemp.eCodTipoEntidad=empl.eCodTipo ".
          " LEFT JOIN catentidades caen ON caen.eCodEntidad=asis.idEmpresa ".
          " LEFT JOIN cattiposalario cts ON cts.eCodSalario=ctemp.eCodTipoSalario ".
          " WHERE asis.Estado='AC' AND asis.Fecha='".$fhHoy."' ".((int)$_POST['eCodTipoEntidad']>0 ? " AND empl.eCodTipo=".(int)$_POST['eCodTipoEntidad'] : "").
          " AND asis.idEmpresa=".(int)$_POST['eCodEntidad'].
          " AND asis.idEmpleado NOT IN (SELECT idEmpleado FROM asistencias WHERE Estado='AC' AND Fecha='".$fhAyer."' AND idEmpleado=asis.idEmpleado AND idEmpresa=".(int)$_POST['eCodEntidad'].") ".
          " AND asis.idEmpleado NOT IN (SELECT DISTINCT asi.idEmpleado FROM asistencias AS asi
                                LEFT JOIN relasistenciamovimientos AS rel ON rel.eCodAsistencia = asi.id
                                LEFT JOIN movimientosafiliatorios AS mov ON mov.eCodMovimiento = rel.eCodMovimiento
                                WHERE asi.Estado = 'AC' AND asi.idEmpleado=asis.idEmpleado AND mov.fhFecha='".$fhHoy."' AND asi.idEmpresa=".(int)$_POST['eCodEntidad'].") ".
          " AND asis.idEmpleado NOT IN (SELECT DISTINCT asi.idEmpleado ".
          "   FROM asistencias AS asi ".
          "   LEFT JOIN relasistenciamovimientos AS rel ON rel.eCodAsistencia = asi.id ".
          "   LEFT JOIN movimientosafiliatorios AS mov ON mov.eCodMovimiento=rel.eCodMovimiento ".
          "   WHERE asi.Estado='AC' AND (asi.fecha='".$fhAyer."' OR mov.fhFecha='".$fhAyer."') AND mov.tFolio>'0' AND asi.idEmpresa=".(int)$_POST['eCodEntidad'].
          " AND asi.idEmpleado NOT IN (SELECT DISTINCT asi.idEmpleado ".
          "   FROM asistencias AS asi ".
          "   LEFT JOIN relasistenciamovimientos AS rel ON rel.eCodAsistencia = asi.id ".
          "   LEFT JOIN movimientosafiliatorios AS mov ON mov.eCodMovimiento=rel.eCodMovimiento ".
          "   WHERE asi.Estado='AC' AND (asi.fecha='".$fhAyer."' OR mov.fhFecha='".$fhAyer."') AND mov.tFolio>'0' ".
          "   AND mov.tCodTipoMovimiento='02' AND asi.idEmpresa=".(int)$_POST['eCodEntidad'].")) ";
  if($_POST['eAccion']==888){
      print $select;
  }
	$rsServicios=mysqli_query($link,$select);
	$registros=(int)mysqli_num_rows($rsServicios);
	$cont=0; ?>
<table cellspacing="0" border="0" width="980px">
  <tr>
    <td width="50%"><hr color="#666666" /></td>
    <td class="sanLR04" nowrap="nowrap"><b>Registros Encontrados: (     <?=$registros;?>     )</b></td>
    <td width="50%"><hr color="#666666" /></td>
  </tr>
</table>
<div style="display:block; top:0; left:0; width:965px; z-index=1; overflow: auto; height:400px;">
  <table cellspacing="0" border="0" width="980px">
    <thead>
      <tr class="thEncabezado">
      <td nowrap="nowrap" class="sanLR04">R. Patronal</td>
	    <td nowrap="nowrap" class="sanLR04">NSS</td>
      <td nowrap="nowrap" class="sanLR04">C. Empleado</td>
	    <td nowrap="nowrap" class="sanLR04">Paterno</td>
	    <td nowrap="nowrap" class="sanLR04">Materno</td>
      <td nowrap="nowrap" class="sanLR04">Nombre(s)</td>
      <td nowrap="nowrap" class="sanLR04">S.D.I.</td>
      <td nowrap="nowrap" class="sanLR04">S. Infornavit</td>
      <td nowrap="nowrap" class="sanLR04">T. Trabajador</td>
      <td nowrap="nowrap" class="sanLR04">T. Salario</td>
      <td nowrap="nowrap" class="sanLR04">Jornada</td>
      <td nowrap="nowrap" class="sanLR04">F. Movimiento</td>
      <td nowrap="nowrap" class="sanLR04">U.M.F</td>
      <td nowrap="nowrap" class="sanLR04">Movimiento</td>
      <td nowrap="nowrap" class="sanLR04">Guia</td>
      <td nowrap="nowrap" class="sanLR04">C. Trabajador</td>
      <td nowrap="nowrap" class="sanLR04">Filler</td>
      <td nowrap="nowrap" class="sanLR04">CURP</td>
	    <td nowrap="nowrap" class="sanLR04" width="100%">Identificador</td> <!--Ya quedo-->
	  </tr>
    </thead>
    <tbody>
      <?php $i=1;
	  $select=" SELECT * FROM sisconfiguracion ";
	  $configuracion=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
	  while($rServicio=mysqli_fetch_array($rsServicios,MYSQLI_ASSOC)){
      $salario=(float)$rServicio{'Salario'};
      $salario=number_format($salario,2);
      $salario=explode(".",$salario);
      if(strlen($salario[0])<=3)
        $cadena="0".$salario[0].$salario[1];
      else
        $cadena=$salario[0].$salario[1];


      $dTope=(float)$configuracion{'dTopeSalario'};
      $dTope=number_format($dTope,2);
      $dTope=explode(".",$dTope);

      $fech=$rServicio["Fecha"];
      $fecha=explode("-",$fech);
      $abc=$fecha[2].$fecha[1].$fecha[0]; ?>
      <tr>
        <!-- <input type="hidden" id="id< ?=$i?>" name="id< ?=$i?>" value="< ?=$rServicio["numeral"]?>">         -->
        <td nowrap="nowrap" class="sanLR04 columnB"><b><?=$rServicio{'rp'};?></b></td>
        <td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rServicio["imss"]); ?></td>
        <td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rServicio["idEmpleado"]); ?></td>
        <td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rServicio["pat"]); ?></td>
        <td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rServicio["mat"]); ?></td>
        <td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rServicio["nomb"]); ?></td>
        <td nowrap="nowrap" class="sanLR04 columnB">
<? //=(float)$rServicio{'Salario'}."(".(float)$configuracion{'dTopeSalario'}."<(float)(".str_replace(",", "", $salario[0])."+(".$salario[1]." / 100)) ? ".(float)$configuracion{'dTopeSalario'}." : ".(float)(str_replace(",", "", $salario[0])+($salario[1]/100)).")";?>
					<?=((float)$configuracion{'dTopeSalario'}<(float)(str_replace(",", "", $salario[0])+($salario[1]/100)) ? 
					(float)(str_replace(",", "", $dTope[0])+($dTope[1]/100)) : 
					(float)(str_replace(",", "", $salario[0])+($salario[1]/100)));?></td>
        <td nowrap="nowrap" class="sanLR04">000000</td>
        <td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rServicio["TipoEmpleado"]); ?></td>
        <td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rServicio["TipoSalario"]); ?></td>
        <td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rServicio["eCodTipoJornada"]); ?></td>
        <td nowrap="nowrap" class="sanLR04"><?=utf8_encode($abc); ?></td>
        <td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rServicio["clin"]); ?></td>
        <td nowrap="nowrap" class="sanLR04">08</td>
        <td nowrap="nowrap" class="sanLR04 columnB">03400</td>
        <td nowrap="nowrap" class="sanLR04">ESTIBADOR</td>
        <td nowrap="nowrap" class="sanLR04 columnB"></td>
        <td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rServicio["curp"]); ?></td>
        <td nowrap="nowrap" class="sanLR04 columnB" width="100%">9</td>
      </tr>
      <?php $i++; } ?>
    </tbody>
  </table>
</div>
<?php }else{
$select=" SELECT * ".
		" FROM cattipoempleado ".
		" WHERE tCodEstatus='AC' ".
		" ORDER BY tNombreCorto ";
$rsTiposEmpleados=mysqli_query($link,$select);

$select=" SELECT tSiglas, eCodEntidad
        FROM catentidades
        WHERE bPrincipal IS NOT NULL
        ORDER BY tSiglas ASC ";
$rsEmpresas=mysqli_query($link,$select); ?>
<script type="text/javascript">
function generarBatch(){
	var UrlBatch = "php/batch/2.5.4.4.php?"+
    (dojo.byId('fecha').value ? "&fecha="+dojo.byId('fecha').value : "")+
    (dojo.byId('eCodTipoEntidad').value ? "&eCodTipoEntidad="+dojo.byId('eCodTipoEntidad').value : "")+
    (dojo.byId('eCodEntidad').value ? "&eCodEntidad="+dojo.byId('eCodEntidad').value : "");
    var Datos = document.createElement("FORM");
    document.body.appendChild(Datos);
    Datos.name='Datos';
    Datos.method = "POST";
    Datos.action = UrlBatch;
    Datos.submit();
}

dojo.addOnLoad(function(){filtrarConsulta();});
</script>
<form name="Datos" id="Datos" method="post" action="" onsubmit="return false;">
  <input type="hidden" value="0" name="ePagina" id="ePagina" />
  <input type="hidden" value="" name="eAccion" id="eAccion" />
  <table width="980px" border="0">
    <tr>
      <td colpan="3" width="100%"></td>
      <td align="right" nowrap="nowrap"><a class="fntBR11" href="javaScript:mostrarFiltros('Busqueda');">Busqueda de Registros</a></td>
    </tr>
    <tr id="trBusqueda" style="display:none">
      <td colspan="4"><table width="100%" bgcolor="#f9f9f9">
          <tr>
            <td class="sanLR04" height="5"></td>
          </tr>
          <tr>
            <td class="sanLR04" height="20">Fecha</td>
            <td class="sanLR04" width="50%"><input type="date" name="fecha" dojoType="dijit.form.TextBox" id="fecha" value="<?php if(array_key_exists("fecha", $_POST)){ echo $_POST["fecha"] ? $_POST["fecha"] : date("Y-m-d"); } else { echo date("Y-m-d"); }?>" style="width:140px"></td>
            <td class="sanLR04" height="20" nowrap>Tipo de Trabajador</td>
            <td class="sanLR04" width="50%">
            	<select name="eCodTipoEntidad" id="eCodTipoEntidad" style="width:180px; height:25px;">
                  <option value="">Seleccione...</option>
                  <?php while($rTipoEmpleado=mysqli_fetch_array($rsTiposEmpleados,MYSQLI_ASSOC)){ ?>
                      <option value="<?=$rTipoEmpleado{'eCodTipoEntidad'}?>"><?=utf8_encode($rTipoEmpleado{'tNombreCorto'})?></option>
                  <?php } ?>
                </select>
            </td>
          </tr>
          <tr>
            <td class="sanLR04" height="20" nowrap>Empresa</td>
            <td class="sanLR04" width="50%">
              <select name="eCodEntidad" id="eCodEntidad" style="width:180px; height:25px;">
                  <option value="">Seleccione...</option>
                  <?php while($rEmpresa=mysqli_fetch_array($rsEmpresas,MYSQLI_ASSOC)){ ?>
                      <option value="<?=$rEmpresa{'eCodEntidad'}?>"><?=utf8_encode($rEmpresa{'tSiglas'})?></option>
                  <?php } ?>
                </select>
            </td>
            <td class="sanLR04" height="20"></td>
            <td class="sanLR04" width="50%"></td>
          </tr>

          <tr>
            <td class="sanLR04" height="5"></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td colspan="4"><div id="dvCNS"></div></td>
    </tr>
  </table>
</form>
<?php } ?>
