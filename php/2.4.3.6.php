﻿<?php require_once("conexion/soluciones-mysql.php");
$link = getLink();
if($_POST){
	if($_POST['eAccion']==1){
		date_default_timezone_set('America/Mexico_City');
		$varfecha	= (isset($_POST["fecha"])	? $_POST["fecha"]		: date("Y-m-d"));
		$fechaayer	= date('Y-m-d',strtotime('-1 days', strtotime($varfecha)));
		$idUsuario	= ($_POST['eUsuario']		? $_POST['eUsuario']	: "NULL");
		$fechaantier= date('Y-m-d',strtotime('-2 days', strtotime($varfecha)));

		foreach($_POST as $k => $valor){
			$nombre = strval($k);
			$campo = "chek";
			if(strstr($nombre,$campo) && (int)$valor>0){
				$insert=" INSERT INTO movimientosafiliatorios (eCodUsuario, fhFecha, tCodTipoMovimiento,  eCodEstatus, dImporte, tFolio, fhFechaRegistro) ".
						" VALUES (".$idUsuario.", '".$fechaantier."', '02', 'AC', 0.0, '".trim($_POST['folio'])."', CURRENT_TIMESTAMP) ";
				if($res=mysqli_query($link,$insert)){
					$exito=1;
					$select=" select last_insert_id() AS Llave ";
					$rCodigo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
					$eCodMovimiento=(int)$rCodigo['Llave'];
					$insertt=" INSERT INTO relasistenciamovimientos (eCodAsistencia, eCodMovimiento) ".
							" VALUES (".$valor.", ".$eCodMovimiento.") ";
					if($res=mysqli_query($link,$insertt)){
						$exito=1;
					}else{
						$exito=0;
					}
				}else{
					$exito=0;
				}
			}
		}
	}

	date_default_timezone_set('America/Mexico_City');
	$varfecha=isset($_POST["fecha"]) ? $_POST["fecha"] : date("Y-m-d");
	$fechabaja = date('Y-m-d',strtotime('-2 days', strtotime($varfecha)));
	$fechaayer = date('Y-m-d',strtotime('-1 days', strtotime($varfecha)));

	//Consulta para validar si tienen continuacion de asistencia dia siguiente mismo empleado.
	$dianext=" SELECT asi.idEmpleado ".
			" FROM asistencias asi ".
			" LEFT JOIN relasistenciamovimientos AS rel ON rel.eCodAsistencia=asi.id ".
			" LEFT JOIN movimientosafiliatorios AS mov ON mov.eCodMovimiento=rel.eCodMovimiento AND mov.eCodEstatus='AC' ".
			" LEFT JOIN empleados AS emp ON asi.idEmpleado=emp.id ".
			" WHERE asi.Estado='AC' AND mov.fhFecha='".$fechaayer."' AND asi.idEmpresa=".(int)$_POST['eCodEntidad'].
			($_POST['eCodTipoEntidad'] ? " AND emp.eCodTipo=".$_POST['eCodTipoEntidad'] : "").
			" group by asi.idEmpleado order by asi.idEmpleado ";
	$continua="";
	if($Rsdianext=mysqli_query($link,$dianext)){
		while($rdianext=mysqli_fetch_array($Rsdianext,MYSQLI_ASSOC)){
			$continua=$continua.$rdianext["idEmpleado"]." , ";
		}
	}
	$continua=$continua." 0 ";

	$dianext=" SELECT asi.idEmpleado ".
			" FROM asistencias asi ".
			" LEFT JOIN relasistenciamovimientos AS rel ON rel.eCodAsistencia=asi.id ".
			" LEFT JOIN movimientosafiliatorios AS mov ON mov.eCodMovimiento=rel.eCodMovimiento AND mov.eCodEstatus='AC' ".
			" LEFT JOIN empleados AS emp ON asi.idEmpleado=emp.id ".
			" WHERE asi.Estado='AC' AND mov.tCodTipoMovimiento='02' AND mov.fhFecha='".$fechabaja."' AND asi.idEmpresa=".(int)$_POST['eCodEntidad'].
			($_POST['eCodTipoEntidad'] ? " AND emp.eCodTipo=".$_POST['eCodTipoEntidad'] : "").
			" group by asi.idEmpleado order by asi.idEmpleado ";
	$baja="";
	if($Rsdianext=mysqli_query($link,$dianext)){
		while($rdianext=mysqli_fetch_array($Rsdianext,MYSQLI_ASSOC)){
			$baja=$baja.$rdianext["idEmpleado"]." , ";
		}
	}
	$baja=$baja." 0 ";

	$select=" SELECT idEmpleado ".
			" FROM incapacidades ".
			" WHERE Estado='AC' ".
			" AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."' ";
	$incap="";
	if($rsIncapacidades=mysqli_query($link,$select)){
		while($rIncapacidad=mysqli_fetch_array($rsIncapacidades,MYSQLI_ASSOC)){
			$incap=$incap.$rIncapacidad["idEmpleado"]." , ";
		}
	}
	$incap=$incap." 0 ";

	$select=" SELECT DISTINCT mov.fhFecha, asi.id AS numeral, cat.tRegistroPatronal AS rp, emp.IMSS, asi.idEmpleado, emp.Paterno, emp.materno, emp.nombre, asi.Turno ".
			" FROM asistencias asi ".
			" LEFT JOIN relasistenciamovimientos rel ON rel.eCodAsistencia=asi.id ".
			" LEFT JOIN movimientosafiliatorios mov ON mov.eCodMovimiento=rel.eCodMovimiento AND mov.eCodEstatus='AC' ".
			" LEFT JOIN catentidades cat ON asi.idEmpresa = cat.eCodEntidad ".
			" LEFT JOIN empleados emp ON asi.idEmpleado = emp.id ".
			" LEFT JOIN categorias ser ON ser.indice = asi.idCategorias ".
			" WHERE asi.Estado='AC' AND mov.fhFecha='".$fechabaja."' AND mov.tFolio IS NOT NULL AND asi.idEmpresa=".(int)$_POST['eCodEntidad'].//AND asi.idCategorias not in (17)
			" AND emp.id not in (".$continua." , ".$baja.", ".$incap.") ".
			($_POST['eCodTipoEntidad'] ? " AND emp.eCodTipo=".$_POST['eCodTipoEntidad'] : "").
			" order by emp.Paterno, emp.materno, emp.nombre ";
	if($_POST['eAccion']==999){
		print $select;
	}
    $rsServicios=mysqli_query($link,$select);
    $cont=0;
    $select=" SELECT DISTINCT mov.fhFecha, cat.tRegistroPatronal AS rp, emp.IMSS, asi.idEmpleado, emp.Paterno, emp.materno, emp.nombre ".
			" FROM asistencias asi ".
			" LEFT JOIN relasistenciamovimientos rel ON rel.eCodAsistencia=asi.id ".
			" LEFT JOIN movimientosafiliatorios mov ON mov.eCodMovimiento=rel.eCodMovimiento AND mov.eCodEstatus='AC' ".
			" LEFT JOIN catentidades cat ON asi.idEmpresa = cat.eCodEntidad ".
			" LEFT JOIN empleados emp ON asi.idEmpleado = emp.id ".
			" LEFT JOIN categorias ser ON ser.indice = asi.idCategorias ".
			" WHERE asi.Estado='AC' AND mov.fhFecha='".$fechabaja."' AND mov.tFolio IS NOT NULL AND asi.idEmpresa=".(int)$_POST['eCodEntidad'].//AND asi.idCategorias not in (17)
			" AND emp.id not in (".$continua." , ".$baja.", ".$incap.") ".
			($_POST['eCodTipoEntidad'] ? " AND emp.eCodTipo=".$_POST['eCodTipoEntidad'] : "").
			" order by emp.Paterno, emp.materno, emp.nombre ";
    $registros=(int)mysqli_num_rows(mysqli_query($link,$select)); ?>
<table cellspacing="0" border="0" width="980px">
  <tr>
    <td width="50%"><hr color="#666666" /></td>
    <td class="sanLR04" nowrap="nowrap"><b>Registros Encontrados: ( <?=$registros;?> )</b></td>
    <td width="50%"><hr color="#666666" /></td>
  </tr>
</table>
<div style="display:block; top:0; left:0; width:965px; z-index=1; overflow: auto; height:400px;">
  <table cellspacing="0" border="0" width="980px">
    <thead>
      <tr class="thEncabezado">
      <td nowrap="nowrap" class="sanLR04" height="20" align="center"><input type="checkbox" id="actMarca" name="actMarca" onclick="Seleccionar();"></td>
      <td nowrap="nowrap" class="sanLR04">R. Patronal</td>
      <td nowrap="nowrap" class="sanLR04">NSS</td>
      <td nowrap="nowrap" class="sanLR04" title="Turno">T</td>
      <td nowrap="nowrap" class="sanLR04">C. Empleado</td>
      <td nowrap="nowrap" class="sanLR04">Paterno</td>
      <td nowrap="nowrap" class="sanLR04">Materno</td>
      <td nowrap="nowrap" class="sanLR04">Nombre(s)</td>
      <td nowrap="nowrap" class="sanLR04">Filler</td>
      <td nowrap="nowrap" class="sanLR04">Fecha</td>
      <td nowrap="nowrap" class="sanLR04">Filler</td>
      <td nowrap="nowrap" class="sanLR04">Movimiento</td>
      <td nowrap="nowrap" class="sanLR04">Guia</td>
      <td nowrap="nowrap" class="sanLR04">C. Trabajador</td>
      <td nowrap="nowrap" class="sanLR04">Causa de Baja</td>
      <td nowrap="nowrap" class="sanLR04">Filler</td>
      <td nowrap="nowrap" class="sanLR04" width="100%">Identificador</td> <!--Ya quedo-->
    </tr>
    </thead>
    <tbody>
		<?php $i=1;
		$idEmpleado=0;
		while($rServicio=mysqli_fetch_array($rsServicios,MYSQLI_ASSOC)){
			if($idEmpleado!=$rServicio["idEmpleado"]){
				$fech=$rServicio["fhFecha"];
				$fecha=explode("-",$fech);
				$abc=$fecha[2].$fecha[1].$fecha[0]; ?>
				<tr>
					<input type="hidden" id="id<?=$i?>" name="id<?=$i?>" value="<?=$rServicio["numeral"]?>">
					<td nowrap="nowrap" class="sanLR04" height="20" align="center"><input type="checkbox" name="chek<?=$i?>" id="chek<?=$i?>" value="<?=$rServicio{'numeral'};?>" ></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><b><?=$rServicio{'rp'};?></b></td>
					<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rServicio["IMSS"]); ?></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=($rServicio["Turno"]);?></td>
					<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rServicio["idEmpleado"]);?></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rServicio["Paterno"]); ?></td>
					<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rServicio["materno"]); ?></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rServicio["nombre"]); ?></td>
					<td nowrap="nowrap" class="sanLR04"></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($abc); ?></td>
					<td nowrap="nowrap" class="sanLR04"></td>
					<td nowrap="nowrap" class="sanLR04 columnB">02</td>
					<td nowrap="nowrap" class="sanLR04">03400</td>
					<td nowrap="nowrap" class="sanLR04 columnB">ESTIBADOR</td>
					<td nowrap="nowrap" class="sanLR04">TERMINO DE CONTRATO</td>
					<td nowrap="nowrap" class="sanLR04 columnB"></td>
					<td nowrap="nowrap" class="sanLR04" width="100%">9</td>
				</tr>
				<?php $i++;
			}
			$idEmpleado=$rServicio["idEmpleado"];
		} ?>
    </tbody>
  </table>
</div>
<?php }else{
$select=" SELECT * ".
		" FROM cattipoempleado ".
		" WHERE tCodEstatus='AC' ".
		" ORDER BY tNombreCorto ";
$rsTiposEmpleados=mysqli_query($link,$select);
//
$select=" SELECT * ".
        " FROM catcausabaja ".
        " WHERE tCodEstatus='AC' ".
        " ORDER BY tNombre ASC ";
$rsCausasBajas=mysqli_query($link,$select);

$select=" SELECT * ".
        " FROM catentidades ".
        " WHERE tCodEstatus='AC' ".
        " ORDER BY tSiglas ";
$rsEntidades=mysqli_query($link,$select); ?>
<script type="text/javascript">
function nuevo(){
	document.location = "?ePagina=2.3.3.1.1.php";
}

function consultar(){
  document.location = "?ePagina=2.5.1.1.php";
}

function Seleccionar(){
  if(dojo.byId('actMarca').checked){
    dojo.query("[id*=\"chek\"]").forEach(function(nodo, index, arr){
      z = nodo.id.replace(/chek/, "");
      if(!dojo.byId("chek"+z).disabled)
        dojo.byId("chek"+z).checked=true;
    });
  }else{
    dojo.query("[id*=\"chek\"]").forEach(function(nodo, index, arr){
      z = nodo.id.replace(/chek/, "");
        dojo.byId("chek"+z).checked=false;
    });
  }
}

function guardar(){
	var eChk = 0;
	var bandera = false;
	var mensaje = "¡Verifique lo siguiente!\n";
	dojo.byId('eAccion').value = 1;
	dojo.query('input[id^=chek]:checked').forEach(function(nodo, index, arr){
		eChk++;
	});

	if(!dojo.byId("folio").value){
		mensaje+="* Folio\n";
		bandera = true;
	}
	if(eChk==0){
		mensaje+="* Seleccione Empleado\n";
		bandera = true;
	}
	if(bandera==true){
		alert(mensaje);
	}else{
		if(confirm("¿Desea registrar el folio?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+'.php', handleAs: "text", load: function(tRespuesta, ioArgs){
				dojo.byId('dvCNS').innerHTML = tRespuesta;
				alert("¡El Registro se realizo exitosamente!");
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});
		}
	}
	dojo.byId('eAccion').value = "";
	dojo.byId('folio').value = "";
}
dojo.addOnLoad(function(){filtrarConsulta();});
</script>
<form name="Datos" id="Datos" method="post" action="" onsubmit="return false;">
  <input type="hidden" name="eUsuario" id="eUsuario" value="<?=(int)$_SESSION['sesionUsuario']['eCodUsuario'];?>" />
  <input type="hidden" value="0" name="ePagina" id="ePagina" />
  <input type="hidden" value="" name="eAccion" id="eAccion" />
  <table width="100%" border="0">
    <tr>
      <td colpan="3" width="100%"></td>
      <td align="right" nowrap="nowrap"><a class="fntBR11" href="javaScript:mostrarFiltros('Busqueda');">Busqueda de Registros</a></td>
    </tr>
    <tr id="trBusqueda" style="display:none">
      <td colspan="4"><table width="100%" bgcolor="#f9f9f9">
          <tr>
            <td class="sanLR04" height="5"></td>
          </tr>
          <tr>
            <td class="sanLR04" height="20">Fecha</td>
            <td class="sanLR04" width="50%"><input type="date" name="fecha" dojoType="dijit.form.TextBox" id="fecha" value="<?=$_POST["fecha"] ? $_POST["fecha"] : date("Y-m-d")?>" style="width:140px"></td>
            <td class="sanLR04">Folio</td>
            <td class="sanLR04" width="50%"><input type="text" name="folio" dojoType="dijit.form.TextBox" id="folio" value="" style="width:80px"></td>
          </tr>
          <tr>
          	<td class="sanLR04" height="20" nowrap>Tipo de Trabajador</td>
            <td class="sanLR04" width="50%">
            	<select name="eCodTipoEntidad" id="eCodTipoEntidad" style="width:180px; height:25px;">
            		<option value="">Seleccione...</option>
                    <?php while($rTipoEmpleado=mysqli_fetch_array($rsTiposEmpleados,MYSQLI_ASSOC)){ ?>
                        <option value="<?=$rTipoEmpleado{'eCodTipoEntidad'}?>" <?=($rTipoEmpleado{'eCodTipoEntidad'}==2 ? "selected" : "")?>><?=utf8_encode($rTipoEmpleado{'tNombreCorto'})?></option>
                    <?php } ?>
                </select>
            </td>
            <td class="sanLR04" height="20" nowrap>Entidad</td>
            <td class="sanLR04" width="50%">
                <select name="eCodEntidad" id="eCodEntidad" style="width:180px; height:25px;">
                    <option value="">Seleccione...</option>
                    <?php while($rEntidad=mysqli_fetch_array($rsEntidades,MYSQLI_ASSOC)){ ?>
                        <option value="<?=$rEntidad{'eCodEntidad'}?>" <?=($rEntidad{'bPrincipal'}==1 ? "selected" : "");?> ><?=utf8_encode($rEntidad{'tSiglas'})?></option>
                    <?php } ?>
                </select>
            </td>
          </tr>
          <tr>
            <td class="sanLR04" height="5"></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td colspan="4"><div id="dvCNS"></div></td>
    </tr>
  </table>
</form>
<?php } ?>
