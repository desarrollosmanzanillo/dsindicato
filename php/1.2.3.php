<td></td>
<div id="intro">
		<div class="pikachoose">
			<ul id="pikame" >
				<li><a href="#"><img src="images/polcalidad.jpg" width="900" height="280" alt="picture"/></a><span>Politica de Calidad</span></li>				
			</ul>
		</div>
	</div>

<div class="holder_content" align="center">
  <section>
  <td></td>
    <h3 align="center">POLITICA DE CALIDAD</h3>
    <p align="justify"> Satisfacer al cliente, brindándole servicios de almacenaje, manipulacíon, inspección, mantenimiento y reparación de todo tipo de contenedores, dentro de los parámetros de un sistema de gestión de calidad. Para esto, le ofrecemos al cliente un valor agregado mediante servicios oportunos y profesionales, siguiendo procesos de mejoramiento continuo.</p>
    <p align="center"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ISO9001.png" width="120" height="120" alt=""/> </p>
    <p align="justify"> El grupo cuenta con una certificacion por la <B>American Registrar of Management Systems</B> como compa&ntilde;ia certificada en <b>ISO 9001</b>.</p>
      </section>
</div>