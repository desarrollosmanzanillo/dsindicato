<style>
#divgral {
     overflow:scroll;
     height:450px;
     width:970px;
}
</style>
<?php require_once("conexion/soluciones-mysql.php");
$link = getLink();

if($_POST){
	if($_POST['eAccion']==1){
		foreach($_POST as $k => $valor){
			$nombre = strval($k);
			$campo = "eCodEntidad";
			if(strstr($nombre,$campo) && (int)$valor>0){
				$update=" UPDATE catchecadoras SET Estado='CA', fhFechaEliminado=current_timestamp, eCodUsuarioEliminado=".(int)$_POST['eUsuario']." WHERE id=".$valor;
				print $update;
				mysqli_query($link,$update);
			}
		}
	}

	$select=" SELECT emp.* ".
			" FROM catchecadoras as emp ".
			" WHERE 1=1".
			($_POST['tRFC']				? " AND emp.rfc LIKE '%".$_POST['tRFC']."%'"			: "").
			($_POST['tNSS']				? " AND emp.IMSS LIKE '%".$_POST['tNSS']."%'"			: "").
			($_POST['tCURP']			? " AND emp.curp LIKE '%".$_POST['tCURP']."%'"			: "").
			($_POST['tNombre']			? " AND emp.Empleado LIKE '%".$_POST['tNombre']."%'"	: "").
			($_POST['eCodEntidad']		? " AND emp.id=".$_POST['eCodEntidad']					: "").
			($_POST['eCodCategoria']	? " AND emp.eCodCategoria=".$_POST['eCodCategoria']		: "").
			($_POST['eCodTipoCredito']	? " AND emp.eCodTipoCredito=".$_POST['eCodTipoCredito']	: "").
			" ORDER BY emp.id ".($_POST['orden'] ? $_POST['orden'] : "ASC").
			" LIMIT ".( $_POST['limite'] ? $_POST['limite'] : "100");
	$rsEntidades=mysqli_query($link,$select); 
	$registros=(int)mysqli_num_rows($rsEntidades); ?>
    <table cellspacing="0" border="0" width="100%">
	    <tr>
		    <td width="50%"><hr color="#666666" /></td>
		    <td class="sanLR04" nowrap="nowrap"><b>Registros Encontrados: ( <?=$registros;?> )</b></td>
			<td width="50%"><hr color="#666666" /></td>
	    </tr>
    </table>
	<div id="divgral" style="display:block; top:0; left:0; width:970px; z-index=1; overflow: auto;">
	<table cellspacing="0" border="0" width="970px" >		
		<thead>
			<tr class="thEncabezado">
				<td nowrap="nowrap" class="sanLR04" height="20" align="center">C</td>
				<td nowrap="nowrap" class="sanLR04">E</td>
				<td nowrap="nowrap" class="sanLR04">VIP</td>
				<td nowrap="nowrap" class="sanLR04">C&oacute;digo</td>
                <td nowrap="nowrap" class="sanLR04">RFC</td>
				<td nowrap="nowrap" class="sanLR04">NSS</td>
				<td nowrap="nowrap" class="sanLR04">Nombre</td>
				<td nowrap="nowrap" class="sanLR04">CURP</td>
				<td nowrap="nowrap" class="sanLR04" width="100%">Fecha Alta</td>
			</tr>
		</thead>
		<tbody>
			<?php $i=1; while($rEntidad=mysqli_fetch_array($rsEntidades,MYSQLI_ASSOC)){ ?>
				<tr>
					<td nowrap="nowrap" class="sanLR04" height="20" align="center"><input type="checkbox" name="eCodEntidad<?=$i?>" id="eCodEntidad<?=$i?>" value="<?=$rEntidad{'id'};?>" ></td>
					<td nowrap="nowrap" class="sanLR04"><img width="16" height="16" alt="Usuario" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-<?=$rEntidad{'Estado'};?>.png"></td>
					<td nowrap="nowrap" class="sanLR04"><?=((int)$rEntidad{'bVIP'}>0 ? "<img width=\"16\" height=\"16\" alt=\"VIP\" src=\"https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/vip.png\">" : "");?></td>
					<td nowrap="nowrap" class="sanLR04 colmenu" ><a href="?ePagina=2.3.2.7.1.php&eCodEntidad=<?=$rEntidad{'id'};?>"><b><?=sprintf("%07d",$rEntidad{'id'});?></b></a></td>
					<td nowrap="nowrap" class="sanLR04"><a class="txtCO12" href="?ePagina=2.3.2.7.2.php&eCodEntidad=<?=$rEntidad{'id'};?>"><?=utf8_encode($rEntidad{'RFC'});?></a></td>
                    <td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rEntidad{'IMSS'});?></a></td>
                    <td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rEntidad{'Empleado'});?></a></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rEntidad{'CURP'});?></td>
					<td nowrap="nowrap" class="sanLR04" width="100%"><?=($rEntidad{'FechaRegistro'} ? date("d/m/Y H:i", strtotime($rEntidad{'FechaRegistro'})) : "")?></td>
				</tr>
			<?php $i++; } ?>
		</tbody>
	</table>
	</div>
<?php }else{ ?>	
<script type="text/javascript">
	dojo.require("dojo.number");
	dojo.require("dijit.form.NumberTextBox");
	dojo.require("dijit.form.DateTextBox");
	dojo.require("dojo.io.iframe");

	function nuevo(){
		document.location = "?ePagina=2.3.2.7.1.php";
	}

	function eliminar(){
		var eChk = 0;
		dojo.byId('eAccion').value = 1;
		dojo.query("[id*=\"eCodEntidad\"]:checked").forEach(function(nodo, index, array){eChk++;});
		if(eChk!=0){
			if(confirm("¿Desea eliminar el registro?")){
				dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+'.php', handleAs: "text", load: function(tRespuesta, ioArgs){
					dojo.byId('dvCNS').innerHTML = tRespuesta; 
					alert("¡Los registros se han eliminado exitosamente!");	
				}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});
			}
		}else{
			alert("No ha seleccionado ningun Empleado");
		}
		dojo.byId('eAccion').value = "";	
	}

	function mChk(fila){
		dojo.byId('eCodEntidad'+fila).checked=true;
	}

	function descanso(){
		var eChk = 0;
		dojo.byId('eAccion').value = 2;
		dojo.query("[id*=\"eCodEntidad\"]:checked").forEach(function(nodo, index, array){eChk++;});
		if(eChk!=0){
			if(confirm("¿Desea actualizar el registro?")){
				dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+'.php', handleAs: "text", load: function(tRespuesta, ioArgs){
					dojo.byId('dvCNS').innerHTML = tRespuesta; 
					alert("¡Los registros se han actualizado exitosamente!");	
				}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});
			}
		}else{
			alert("Seleccionar empleado(s)!");
		}
		dojo.byId('eAccion').value = "";	
	}	

	function generaExcel(){
		var UrlExcel = "php/excel/2.3.2.7.php?"+
		(dojo.byId('tRFC').value			? "&tRFC="+dojo.byId('tRFC').value						: "")+
		(dojo.byId('tNSS').value			? "&tNSS="+dojo.byId('tNSS').value						: "")+
		(dojo.byId('tCURP').value			? "&tCURP="+dojo.byId('tCURP').value					: "")+
		(dojo.byId('orden').value			? "&orden="+dojo.byId('orden').value					: "")+
		(dojo.byId('limite').value			? "&limite="+dojo.byId('limite').value					: "")+
		(dojo.byId('tNombre').value 		? "&tNombre="+dojo.byId('tNombre').value				: "")+
		(dojo.byId('eCodEntidad').value 	? "&eCodEntidad="+dojo.byId('eCodEntidad').value		: "");
		var Datos = document.createElement("FORM");
		document.body.appendChild(Datos);
		Datos.name='Datos';
		Datos.method = "POST";
		Datos.action = UrlExcel;
		Datos.submit();
	}

	dojo.addOnLoad(function(){ filtrarConsulta();});
	</script>
	<form name="Datos" id="Datos" method="post" action="" onsubmit="return false;">
		<input type="hidden" value="0" name="ePagina" id="ePagina" />
		<input type="hidden" value="" name="eAccion" id="eAccion" />
		<input type="hidden" name="eUsuario" id="eUsuario" value="<?=(int)$_SESSION['sesionUsuario']['eCodUsuario'];?>" />
		<table width="100%" border="0">
			<tr>
				<td colpan="3" width="100%"></td>
				<td align="right" nowrap="nowrap"><a class="fntBR11" href="javaScript:mostrarFiltros('Busqueda');">Busqueda de Registros</a></td>
			</tr>
			<tr id="trBusqueda" style="display:none">
				<td colspan="4">
					<table width="100%" bgcolor="#f9f9f9">
						<tr><td class="sanLR04" height="5"></td></tr>
						<tr>
							<td class="sanLR04" height="20">C&oacute;digo</td>
							<td class="sanLR04" width="50%"><input type="text" name="eCodEntidad" dojoType="dijit.form.TextBox" id="eCodEntidad" value="" style="width:80px"></td>
							<td class="sanLR04">RFC</td>
							<td class="sanLR04" width="50%"><input type="text" name="tRFC" dojoType="dijit.form.TextBox" id="tRFC" value="" style="width:175px"></td>
						</tr>
						<tr>
							<td class="sanLR04" height="20">Nombre</td>
							<td class="sanLR04" width="50%"><input type="text" name="tNombre" dojoType="dijit.form.TextBox" id="tNombre" value="" style="width:175px"></td>
							<td class="sanLR04" >CURP</td>
							<td class="sanLR04" width="50%"><input type="text" name="tCURP" dojoType="dijit.form.TextBox" id="tCURP" value="" style="width:175px"></td>
							
						</tr>
						<tr>
				            <td class="sanLR04" nowrap="nowrap">NSS</td>
							<td class="sanLR04" width="50%"><input type="text" name="tNSS" dojoType="dijit.form.TextBox" id="tNSS" value="" style="width:175px"></td>
							<td nowrap="nowrap" class="sanLR04" height="20"></td>
				            <td class="sanLR04" width="50%"></td>
				        </tr>
				        <tr>
			              <td nowrap="nowrap" class="sanLR04" height="5"><b>[Ordenamiento]</b></td>
			              <td nowrap="nowrap" class="sanLR04" width="50%"><input type="radio" name="orden" id="orden" checked="checked" value="ASC" > ASCENDENTE / <input type="radio" name="orden" id="orden" value="DESC" > DESENDENTE</td>
			              <td nowrap="nowrap" class="sanLR04"><b>[Maximos Registros]:</b></td>
			              <td nowrap="nowrap" class="sanLR04" width="50%">
			              	<select name="limite" id="limite" style="width:175px">
                                  <option value="100">100</option>
                                  <option value="1000">1,000</option>
                                  <option value="10000">10,000</option>
                                  <option value="100000">100,000</option>
                          	</select>
                          </td>
            			</tr>
						<tr><td class="sanLR04" height="5"></td></tr>
					</table>
				</td>
			</tr>
			<tr><td colspan="4"><div id="dvCNS"></div></td></tr>
		</table>
	</form>	
<?php } ?>