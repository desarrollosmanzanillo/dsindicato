<?php require_once("conexion/soluciones-mysql.php");  
$link = getLink();
	
$select=" SELECT tpo.* , tsa.tNombre AS Salario, tjo.tNombre As Jornada, ttr.tNombre As Trabajador, cau.tNombre as causa ".
		" FROM cattipoempleado AS tpo ".
		" INNER JOIN cattiposalario as tsa ON tsa.eCodSalario=tpo.eCodTipoSalario".
		" INNER JOIN cattipojornada as tjo ON tjo.eCodJornada=tpo.eCodTipoJornada ".
		" INNER JOIN cattipotrabajador as ttr ON ttr.eCodTrabajador=tpo.eCodTipoTrabajador ".
		" INNER JOIN catcausabaja AS cau ON cau.eCodCausa=tpo.eCodCausaBaja ".
		" WHERE eCodTipoEntidad=".$_GET['eCodTipoEntidad'];
$rTipoEntidad = mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC); ?>
<script type="text/javascript">
function consultar(){
	document.location = './?ePagina=2.3.2.3.php';
}
function nuevo(){
	document.location = './?ePagina=2.3.2.3.1.php';
}
</script>
<div id="dvCNS" style="display:none;"></div>
<form action="" method="post" name="Datos" id="Datos" onsubmit="return false;">
<div style="display:block; top:0; left:0; width:965px; z-index=1; overflow: auto; height:400px;">
<table border="0" cellpadding="0" cellspacing="0">
    <tr><td height="20"></td></tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"> C&oacute;digo</td>
	    <td width="50%" nowrap class="sanLR04"><b><?=sprintf("%07d",$rTipoEntidad{'eCodTipoEntidad'});?></b></td>
		<td nowrap class="sanLR04"> Nombre</td>
	    <td width="50%" nowrap class="sanLR04"><b><?=utf8_encode($rTipoEntidad{'tNombre'})?></b></td>
    </tr>
    <tr>
	    <td height="23" nowrap class="sanLR04"> Nombre Corto </td>
	    <td nowrap class="sanLR04"><b><?=$rTipoEntidad{'tNombreCorto'}?></b></td>
	    <td height="23" nowrap class="sanLR04"> Factor de Integraci&oacute;n </td>
	    <td nowrap class="sanLR04"><b><?=$rTipoEntidad{'dFactorIntegracion'}?></b></td>
    </tr>
    <tr>
	    <td height="23" nowrap class="sanLR04"> Tipo de Salario </td>
	    <td nowrap class="sanLR04"><b><?=$rTipoEntidad{'Salario'}?></b></td>
	    <td height="23" nowrap class="sanLR04"> Tipo de Jornada </td>
	    <td nowrap class="sanLR04"><b><?=$rTipoEntidad{'Jornada'}?></b></td>
    </tr>
    <tr>
	    <td height="23" nowrap class="sanLR04"> Tipo de Trabajador </td>
	    <td nowrap class="sanLR04"><b><?=$rTipoEntidad{'Trabajador'}?></b></td>
	    <td height="23" nowrap class="sanLR04">Causa de Baja</td>
	    <td nowrap class="sanLR04"><b><?=$rTipoEntidad{'causa'}?></b></td>
    </tr>
</table>
</div>
</form>