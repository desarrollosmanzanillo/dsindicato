<?php 
require_once("excel/phpexcel.php");
require_once("../conexion/soluciones-mysql.php");

$select=" SELECT osm.eCodSalida, oss.eCodSolicitud, oss.fhFechaEntrada, oss.tBL, oss.tCodContenedor, ".
		" cen.tNombre AS Naviera, oss.tPatente, cec.tNombre AS Cliente, cef.tNombre AS FacturarA, ".
		" cbu.tNombre AS Buque, oss.tNumeroViaje, cto.tNombre AS TipoServicio, ".
		" oss.eCodTipoServicio, oss.tCodEstatus, ctc.tNombreCorto AS TipoContenedor, oss.tObservaciones	".
		" FROM opesalidasmercancias osm ".
		" INNER JOIN opeentradasmercancias oss ON oss.eCodSalida=osm.eCodSalida ".
		" INNER JOIN catentidades cen ON cen.eCodEntidad=osm.eCodNaviera ".
		" INNER JOIN catentidades cec ON cec.eCodEntidad=osm.eCodCliente ".
		" INNER JOIN catentidades cef ON cef.eCodEntidad=osm.eCodFacturarA ".
		" INNER JOIN catbuques cbu ON cbu.eCodBuque=oss.eCodBuque ".
		" INNER JOIN cattiposservicios cto ON cto.eCodTipoServicio=oss.eCodTipoServicio ".
		" LEFT JOIN cattiposcontenedores ctc ON ctc.eCodTipoContenedor=oss.eCodTipoContenedor ".
		" WHERE 1=1 ".
		($_POST['eCodSalida'] ? " AND osm.eCodSalida=".$_POST['eCodSalida'] : "").
		((int)$_POST['eEntidad']==1 ? "" : " AND (osm.eCodNaviera=".(int)$_POST['eEntidad']." OR osm.eCodCliente=".(int)$_POST['eEntidad'].")").
		" ORDER BY osm.eCodSalida DESC ";
$rsSolicitudes=mysql_query($select);

// Create new PHPExcel object
$excel = new PHPExcel();

// Valores a Utilizar
$titulo = "Entradas de Mercancías";
$columna = 'A';
$fila = 5;

// Arreglo con titulos columnas
$titulos = array(
	"Código",
	"T. Servicio",
	"F. Entrada",
	"BL",
	"Contenedor",
	"T. Contenedor",
	"Naviera",
	"Patente",
	"Cliente",
	"Buque",
	"Viaje",
	"Observaciones"
);

// Ponemos las propiedades del documento
$excel->getProperties()->setCreator("One Solution")
	  ->setLastModifiedBy("One Solution")
	  ->setTitle("One Solution")
	  ->setSubject("One Solution")
	  ->setDescription("One Solution")
	  ->setKeywords("One Solution")
	  ->setCategory("One Solution");

// Encabezados del documento
$excel->getActiveSheet()->setCellValue("A1", "Soluciones de Almacenamiento y Transporte SA de CV");
$excel->getActiveSheet()->setCellValue("A2", utf8_encode("Fecha del Reporte: ".date("d/m/Y H:i:s")));
$excel->getActiveSheet()->setCellValue("A3", $titulo);

// Arreglo con formato para los titulos columnas
$frEnc = new PHPExcel_Style();
$frTit = new PHPExcel_Style();
$frSep = new PHPExcel_Style();

$frEnc->applyFromArray(array('fill'	=> array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color'	=> array('rgb' => 'd6d6d6')),'font' => array('bold'=> true)));
$frTit->applyFromArray(array('fill' => array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color'	=> array('rgb' => 'f96f09')),'font'	=> array('color'=> array('rgb' => 'FFFFFF'))));
$frSep->applyFromArray(array('fill' => array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color'	=> array('rgb' => 'FFFFFF'))));

$numTitulos = sizeof($titulos);

// Ajuste de anchos de columnas
for($z=1;$z<=$numTitulos;$z++){
	// Ponemos el ancho de la columna
	//$excel->getActiveSheet()->getColumnDimension($columna)->setWidth(14);
	$excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);

	// Pintamos el titulo
	$celda = $columna."".$fila;
	$excel->setActiveSheetIndex(0)->setCellValue($celda, $titulos[$z-1]);
	
	if($z < $numTitulos){
		$columna++;
	}
}

// Formato de celdas
$excel->getActiveSheet()->setSharedStyle($frEnc, "A".$fila.":".$columna.$fila);

// Agrupamos las celdas de los encabezados
for($i=1; $i<=3; $i++){
	$excel->getActiveSheet()->setSharedStyle($frEnc, "A".$i);
	$excel->getActiveSheet()->mergeCells('A'.$i.':'.$columna.''.$i);
}
	
$excel->getActiveSheet()->setSharedStyle($frTit, "A3");
$excel->getActiveSheet()->setSharedStyle($frSep, "A4");
$excel->getActiveSheet()->setSharedStyle($frEnc, "A5");
$excel->getActiveSheet()->getRowDimension('4')->setRowHeight(5);

// Ajustamos formatos
$excel->getActiveSheet()->getStyle("A1")->getFont()->setSize(14);
$excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);

$excel->getActiveSheet()->getStyle("A3")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$excel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true);

while($rSolicitud=mysql_fetch_array($rsSolicitudes)){
	// aumentamos primero porque estabamos en fila de encabezados
	$fila++;
	$columna = 'A';
	$excel->getActiveSheet()->setCellValue($columna++.$fila, sprintf("%07d",$rSolicitud{'eCodEntrada'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'TipoServicio'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, date("d/m/Y", strtotime($rSolicitud{'fhFechaEntrada'})));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'tBL'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'tCodContenedor'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'TipoContenedor'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'Naviera'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'tPatente'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'Cliente'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'Buque'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'tNumeroViaje'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'tObservaciones'}));
}
// Cambiamos el nombre de la hoja
$excel->getActiveSheet()->setTitle('One Solution');

// Ocultamos las lineas de division
$excel->getActiveSheet()->setShowGridlines(false);

// Asignamos la primer hoja como la activa
$excel->setActiveSheetIndex(0);

// Redirect output to a client's web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename=Entradas_de_Mercancías.xls');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
$objWriter->save('php://output');
exit;
?>