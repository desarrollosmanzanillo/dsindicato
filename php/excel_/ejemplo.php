<?php require_once("excel/phpexcel.php");

	//$pagina = new clSistema($_POST['tCodSeccion']);	
	
	/*$select=" SELECT TOP ".$_POST['eMaxRegistros']." bc.bMigracion, bc.eCodCFD, bc.tCodEstatus AS Estatus, ".
			" bc.tSerie, bc.eFolio AS Folio, bc.fhFechaCreacion AS FechaCreacion, su.tNombre+' '+su.tApellidos AS Usuario, ".
			" bc.tHost, bc.fhFechaCancelacion AS FechaCancelacion, suc.tNombre+' '+suc.tApellidos AS UsuarioCancela, bc.tRFCEmisor AS RFCEmisor, ".
			" bc.tNombreEmisor AS NombreEmisor, bc.tRFCReceptor AS RFCReceptor, bc.tNombreReceptor AS NombreReceptor, cs.tNombre AS NombreSucursal, ce.tNombre AS Agente, ce.ePatente AS Patente, ".
			" cfp.tNombre AS FormaPago, bc.dSubtotal, cmp.tNombre AS MetodoPago, bc.dDescuento, bc.dImpuestosRetenidos, ".
			" cc.tNombre AS Comprobante, ctc.eCodTipoComprobante, ctc.tNombre AS TipoComprobante, bc.dImpuestosTrasladados, bc.dTotal, bc.dSaldo, cee.tSiglas, ".
			" CASE WHEN (DATEDIFF(DAY, bc.fhFechaCreacion, CURRENT_TIMESTAMP))>sec.eDiasCredito THEN sec.eDiasCredito ELSE (DATEDIFF(day, bc.fhFechaCreacion, CURRENT_TIMESTAMP)) END AS diasVencidos, ".
			" DATEADD(DAY, sec.eDiasCredito, bc.fhFechaCreacion) AS fechaVencimiento ".
			($pagina->bAll ? "" : ", (SELECT TOP 1 bImprimirComprobante FROM sisEntidadesConfiguracion WHERE eCodEntidad=".$_SESSION['sesionServicios']['eCodEntidad'].") AS Imprimir " ).
			" FROM BitCFDs bc ".
			" LEFT JOIN catSucursalesEntidades cs ON bc.eCodSucursalCliente = cs.eCodSucursal ".
			" LEFT JOIN sisUsuarios su ON bc.eCodUsuario = su.eCodUsuario ".
			" LEFT JOIN sisUsuarios suc ON bc.eCodUsuarioCancela = suc.eCodUsuario ".
			" LEFT JOIN catEntidades ce ON bc.eCodAgenciaAduanal = ce.eCodEntidad ".
			" LEFT JOIN CatFormasPagos cfp ON bc.eCodFormaPago = cfp.eCodFormaPago ". 
			" LEFT JOIN catMetodosPagos cmp ON bc.tMetodoPago = cmp.tNombre ".
			" LEFT JOIN catComprobantes cc ON bc.eCodComprobante = cc.eCodComprobante ".
			" LEFT JOIN catTiposComprobantes ctc ON cc.eCodTipoComprobante=ctc.eCodTipoComprobante ".
			" LEFT JOIN catEntidades cee ON cc.eCodEntidad=cee.eCodEntidad ".
			" LEFT JOIN sisEntidadesConfiguracion sec ON bc.eCodAgenciaAduanal=sec.eCodEntidad ".
			 ($pagina->bAll ? "WHERE bc.eCodCFD > 0 " : " WHERE (bc.eCodAgenciaAduanal=".$_SESSION['sesionServicios']['eCodEntidad']." OR (bc.ePatente IN (SELECT ce.ePatente ".
			" FROM relEntidadesPatentes re ".
			" LEFT JOIN catEntidades ce ON re.eCodEntidadPatente=ce.eCodEntidad ".
			" WHERE re.eCodEntidad=".$_SESSION['sesionServicios']['eCodEntidad'].") AND bc.bMigracion IS NOT NULL)  OR bc.eCodCliente=".$_SESSION['sesionServicios']['eCodEntidad']." OR cc.eCodEntidad=".$_SESSION['sesionServicios']['eCodEntidad'].")");

	if ($_POST['eFolio']!="") {$select.=" AND (bc.eFolio = '".$_POST['eFolio']."' OR bc.tSerie='".$_POST['eFolio']."') ";}	 
	if ($_POST['fhFecha']!="") {
		$fhFechaInicial = $_POST['fhFecha'].' 00:00:00'; 
		$fhFechaFinal = ($_POST['fhFecha2']!="" ? $_POST['fhFecha2'].' 23:59:59' : $fhFechaFinal=$_POST['fhFecha'].' 23:59:59');
	}
	if ($_POST['fhFecha']!="") {$select.=" AND bc.fhFechaCreacion  BETWEEN '".$fhFechaInicial."' AND  '".$fhFechaFinal."' ";}
	
	if ($_POST['fhFechaCan']!="") {
		$fhFechaInicialCan = $_POST['fhFechaCan'].' 00:00:00'; 
		$fhFechaFinalCan = ($_POST['fhFechaCan2']!="" ? $_POST['fhFechaCan2'].' 23:59:59' : $fhFechaFinal=$_POST['fhFechaCan'].' 23:59:59');
	}
	if ($_POST['fhFechaCan']!="") {$select.=" AND bc.fhFechaCancelacion  >= '".$fhFechaInicialCan."' AND bc.fhFechaCancelacion <= '".$fhFechaFinalCan."' ";}
	if ($_POST['eCodUsuario']!="") {$select.=" AND bc.eCodUsuario LIKE '%".$_POST['eCodUsuario']."%' ";}
	if ($_POST['tNombreEmisor']!="") {$select.=" AND bc.tNombreEmisor LIKE '%".utf8_decode($_POST['tNombreEmisor'])."%' ";}
	if ($_POST['tRFCEmisor']!="") {$select.=" AND bc.tRFCEmisor LIKE '%".utf8_decode($_POST['tRFCEmisor'])."%' ";}
	if ($_POST['eCodCliente']!="") {$select.=" AND bc.eCodCliente =".$_POST['eCodCliente'];}
	if ($_POST['eCodAgente']!="") {$select.=" AND bc.eCodAgenciaAduanal =".$_POST['eCodAgente'];}
	if ($_POST['tRFCReceptor']!="") {$select.=" AND bc.tRFCReceptor LIKE '%".utf8_decode($_POST['tRFCReceptor'])."%' ";}
	if ($_POST['eCodComprobante']!="") {$select.=" AND bc.eCodComprobante = ".$_POST['eCodComprobante'];}
	if ($_POST['tCodEstatus']!="") {$select.=" AND bc.tCodEstatus = '".$_POST['tCodEstatus']."' ";}
	if ($_POST['eCodFormaPago']!="") {$select.=" AND bc.eCodFormaPago = '".$_POST['eCodFormaPago']."' ";}
	if ($_POST['eCodMetodoPago']!="") {$select.=" AND cmp.eCodMetodoPago = '".$_POST['eCodMetodoPago']."' ";}
	if ($_POST['eCodMetodoPago']!="") {$select.=" AND cmp.eCodMetodoPago = '".$_POST['eCodMetodoPago']."' ";}
	if ($_POST['NoSolicitud']!="") {$select.=" AND bc.eCodCFD = (SELECT TOP 1 eCodCFD FROM relCFDsEtiquetas rce WHERE rce.tNombre='No Orden' AND tDatos LIKE '%".$_POST['NoSolicitud']."%') ";}
	if ($_POST['tReferencia']!="") {$select.=" AND bc.eCodCFD = (SELECT TOP 1 eCodCFD FROM relCFDsEtiquetas rce WHERE rce.tNombre='Referencia' AND tDatos LIKE '%".$_POST['tReferencia']."%') ";}
	if ($_POST['bAdeudo']!="") {$select.=" AND bc.dSaldo>0 ";}
	if ($_POST['ePatente']!="") {$select.=" AND bc.ePatente LIKE '%".$_POST['ePatente']."%' ";}
 	$select.=" ORDER BY ".$_POST['rdOrden']." ".$_POST['rdDireccion'];
	
	// Filtrado por Máximos Registros de Busqueda
	$select = " SELECT TOP ".$_POST['eMaxRegistros']." *, 1 AS Agrupar, ROW_NUMBER() OVER (ORDER BY ".$_POST['rdOrden']." ".$_POST['rdDireccion'].") as Fila ".
			  " FROM (".$select.") as Filtrado ";
	
	$pagina->totalRegistros = mssql_num_rows(mssql_query($select));	
	$selectTotales =" SELECT SUM(dSubtotal) AS Subtotal, SUM(dDescuento) AS Descuento, SUM(dImpuestosRetenidos) AS Impuestos, ".
					" SUM(dImpuestosTrasladados) AS Traslados, SUM(dTotal) AS Total, SUM(dSaldo) AS Saldo ".
					" FROM (".$select." ORDER BY eCodCFD ".$_POST['rdDireccion'].") AS Totales GROUP BY Agrupar ";
	$rsTotales = mssql_query($selectTotales);
	$rTotal = mssql_fetch_array($rsTotales);

	$select.=" ORDER BY ".$_POST['rdOrden']." ".$_POST['rdDireccion'];
	$rsCFDs = mssql_query($select);*/
	
	// Create new PHPExcel object
	$excel = new PHPExcel();

	// Valores a Utilizar
	$titulo = "CFDs Emitidos";
	$columna = 'A';
	$fila = 7;

	// Arreglo con titulos columnas
	$titulosColumnas = array(
			"Código",
			"Fecha",
			"Emisor",
			"Comprobante",
			"Tipo",
			"Usuario",
			"Servidor Remoto",
			"Cliente",
			"RFC",
			"Sucursal",
			"Agencia Aduanal",
			"No. Solicitud",
			"Referencia",
			"Forma de Pago",
			"Metodo de Pago",
			"F. Vencimiento",
			"Días Vencidos",
			"Subtotal",
			"Descuento",
			"Retenciones",
			"Traslados",
			"Total",
			"Saldo"
	);

	// Ponemos las propiedades del documento
	$excel->getProperties()->setCreator("Soluciones")
		  ->setLastModifiedBy("Solucion")
		  ->setTitle("Titulo")
		  ->setSubject("Subtitulo")
		  ->setDescription("Cunsulta")
		  ->setKeywords("P")
		  ->setCategory("PP");

	// Encabezados del documento
	$excel->getActiveSheet()->setCellValue("A1", "Empresa");
	$excel->getActiveSheet()->setCellValue("A2", "Proyecto");
	$excel->getActiveSheet()->setCellValue("A3", "Ruta Sistema");
	$excel->getActiveSheet()->getCell('A3')->getHyperlink()->setUrl("PaginaWeb");
	
	$excel->getActiveSheet()->setCellValue("A4", utf8_encode("Fecha del Reporte: ".date("d/m/Y H:i:s")));
	$excel->getActiveSheet()->setCellValue("A5", $titulo);
	
	// Arreglo con formato para los titulos columnas
	$frEnc = new PHPExcel_Style();
	$frTit = new PHPExcel_Style();
	$frSep = new PHPExcel_Style();
	
	$frEnc->applyFromArray(array('fill' 	=> array('type'			=> PHPExcel_Style_Fill::FILL_SOLID,'color'			=> array('rgb' => 'F1F1F1')),'font' 	=> array('bold'=> true)));
	$frTit->applyFromArray(array('fill' 	=> array('type'			=> PHPExcel_Style_Fill::FILL_SOLID,'color'			=> array('rgb' => 'FFDF59')),'font' 	=> array('color'=> array('rgb' => '000000'))));
	$frSep->applyFromArray(array('fill' 	=> array('type'			=> PHPExcel_Style_Fill::FILL_SOLID,'color'			=> array('rgb' => 'FFFFFF'))));

	$numTitulos = sizeof($titulosColumnas);

	// Ajuste de anchos de columnas
	for($z=1;$z<=$numTitulos;$z++)
	{
		// Ponemos el ancho de la columna
		//$excel->getActiveSheet()->getColumnDimension($columna)->setWidth(14);
		$excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);


		// Pintamos el titulo
		$celda = $columna."".$fila;
		$excel->setActiveSheetIndex(0)->setCellValue($celda, $titulosColumnas[$z-1]);
		
		if($z < $numTitulos)
			{
			$columna++;
			}
	}

	// Formato de celdas
	$excel->getActiveSheet()->setSharedStyle($frEnc, "A".$fila.":".$columna.$fila);

	// Agrupamos las celdas de los encabezados
	for($i=1; $i<=6; $i++)
		{
		$excel->getActiveSheet()->setSharedStyle($frEnc, "A".$i);
		$excel->getActiveSheet()->mergeCells('A'.$i.':'.$columna.''.$i);
		}
		
	$excel->getActiveSheet()->setSharedStyle($frTit, "A5");
	$excel->getActiveSheet()->setSharedStyle($frSep, "A6");
	$excel->getActiveSheet()->setSharedStyle($frEnc, "A7");
	$excel->getActiveSheet()->getRowDimension('6')->setRowHeight(5);
	
	// Ajustamos formatos
	$excel->getActiveSheet()->getStyle("A1")->getFont()->setSize(14);
	$excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);

	$excel->getActiveSheet()->getStyle("A5")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$excel->getActiveSheet()->getStyle("A5")->getFont()->setBold(true);
	
	// aumentamos primero porque estabamos en fila de encabezados
	$fila++;

	$columna = 'A';

	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rCFD{'tSerie'})."".sprintf("%09d",$rCFD{'Folio'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, ($rCFD{'FechaCreacion'} ? date("d/m/Y H:i", strtotime($rCFD{'FechaCreacion'})) : ""));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rCFD{'tSiglas'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rCFD{'Comprobante'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rCFD{'TipoComprobante'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rCFD{'Usuario'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, $rCFD{'tHost'});
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rCFD{'NombreReceptor'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rCFD{'RFCReceptor'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rCFD{'NombreSucursal'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rCFD{'Agente'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, sprintf("%09d",$rSolicitud{'Solicitud'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rReferencia{'Referencia'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rCFD{'FormaPago'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rCFD{'MetodoPago'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, ($rCFD{'dSaldo'}>0&&$rCFD{'Estatus'}=='AC' &&$rCFD{'eCodTipoComprobante'}==2 ? ($rCFD{'fechaVencimiento'} ? date("d/m/Y", strtotime($rCFD{'fechaVencimiento'})) : date("d/m/Y", strtotime($rCFD{'FechaCreacion'}))) : ""));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, ($rCFD{'dSaldo'}>0&&$rCFD{'Estatus'}=='AC'&&$rCFD{'eCodTipoComprobante'}==2 ? $rCFD{'diasVencidos'} : ""));
	$excel->getActiveSheet()->setCellValue($columna.$fila,($rCFD{'dSubtotal'}))->getStyle($columna++.$fila)->getNumberFormat()->setFormatCode('$#,##0.00');
	$excel->getActiveSheet()->setCellValue($columna.$fila,($rCFD{'dDescuento'}))->getStyle($columna++.$fila)->getNumberFormat()->setFormatCode('$#,##0.00');
	$excel->getActiveSheet()->setCellValue($columna.$fila,($rCFD{'dImpuestosRetenidos'}))->getStyle($columna++.$fila)->getNumberFormat()->setFormatCode('$#,##0.00');
	$excel->getActiveSheet()->setCellValue($columna.$fila,($rCFD{'dImpuestosTrasladados'}))->getStyle($columna++.$fila)->getNumberFormat()->setFormatCode('$#,##0.00');
	$excel->getActiveSheet()->setCellValue($columna.$fila,($rCFD{'dTotal'}))->getStyle($columna++.$fila)->getNumberFormat()->setFormatCode('$#,##0.00');	
	$excel->getActiveSheet()->setCellValue($columna.$fila,($rCFD{'dSaldo'}))->getStyle($columna++.$fila)->getNumberFormat()->setFormatCode('$#,##0.00');
	
// Cambiamos el nombre de la hoja
$excel->getActiveSheet()->setTitle('RSP');

// Ocultamos las lineas de division
$excel->getActiveSheet()->setShowGridlines(false);

// Asignamos la primer hoja como la activa
$excel->setActiveSheetIndex(0);

// Redirect output to a client's web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename='.date('(His)-').'.xls');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
$objWriter->save('php://output');
exit;
?>