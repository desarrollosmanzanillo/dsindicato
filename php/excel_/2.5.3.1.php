<?php
require_once("excel/phpexcel.php");
require_once("../conexion/soluciones-mysql.php");

if($_POST['fhFechaProgramacionInicio']!=""){
	$fhFechaProgramacionInicio = $_POST['fhFechaProgramacionInicio'].' 00:00:00';
	$fhFechaProgramacionFin = ($_POST['fhFechaProgramacionFin']!="" ? $_POST['fhFechaProgramacionFin'] : $_POST['fhFechaProgramacionInicio']).' 23:59:59';
}
if($_POST['fhFechaServicioInicio']!=""){
	$fhFechaServicioInicio = $_POST['fhFechaServicioInicio'].' 00:00:00';
	$fhFechaServicioFin = ($_POST['fhFechaServicioFin']!="" ? $_POST['fhFechaServicioFin'] : $_POST['fhFechaServicioInicio']).' 23:59:59';
}

$select=" SELECT oss.eCodSolicitud, oss.fhFecha, oss.fhFechaServicio, ".
		" cen.tNombre AS Naviera, oss.tPatente, cec.tNombre AS Cliente, cef.tNombre AS FacturarA, ".
		" cbu.tNombre AS Buque, oss.tNumeroViaje, cts.eCodTipoSolicitud, cts.tNombre AS TipoSolicitud, cto.tNombre AS TipoServicio, ".
		" cto.eCodTipoServicio as eCodTipoServ, oss.eCodTipoServicio, oss.tCodEstatus ".
		" FROM opesolicitudesservicios oss ".
		" INNER JOIN catentidades cen ON cen.eCodEntidad=oss.eCodNaviera ".
		" INNER JOIN catentidades cec ON cec.eCodEntidad=oss.eCodCliente ".
		" INNER JOIN catentidades cef ON cef.eCodEntidad=oss.eCodFacturarA ".
		" INNER JOIN catbuques cbu ON cbu.eCodBuque=oss.eCodBuque ".
		" INNER JOIN cattipossolicitudes cts ON cts.eCodTipoSolicitud=oss.eCodTipoSolicitud ".
		" INNER JOIN cattiposservicios cto ON cto.eCodTipoServicio=oss.eCodTipoServicio ".
		" WHERE 1=1 ".
		($_POST['eCodSolicitud'] ? " AND oss.eCodSolicitud=".$_POST['eCodSolicitud'] : "").
		($_POST['Cliente']       ? " AND cec.tNombre   LIKE '%".$_POST['Cliente']."%'" : "").
		($_POST['tPatente']      ? " AND oss.tPatente  LIKE '%".$_POST['tPatente']."%'" : "").
		($_POST['Naviera']       ? " AND cen.tNombre   LIKE '%".$_POST['Naviera']."%'" : "").
		($_POST['TipoSolicitud'] ? " AND cts.eCodTipoSolicitud=".$_POST['TipoSolicitud'] : "").
		($_POST['TipoServicio']  ? " AND cto.eCodTipoServicio=".$_POST['TipoServicio'] : "").
		($_POST['Buque']       ? " AND cbu.tNombre   LIKE '%".$_POST['Buque']."%'" : "").
		($_POST['tNumeroViaje']      ? " AND oss.tNumeroViaje  LIKE '%".$_POST['tNumeroViaje']."%'" : "").
		($_POST['fhFechaProgramacionInicio']	? " AND oss.fhFecha			BETWEEN    '".$fhFechaProgramacionInicio. "' AND '" .$fhFechaProgramacionFin."'" : "" ).
		($_POST['fhFechaServicioInicio']		? " AND oss.fhFechaServicio	BETWEEN    '".$fhFechaServicioInicio. "' AND '" .$fhFechaServicioFin."'" : "" ).
		((int)$_POST['eEntidad']==1 ? "" : " AND (oss.eCodNaviera=".(int)$_POST['eEntidad']." OR oss.eCodCliente=".(int)$_POST['eEntidad'].")").
		" ORDER BY oss.eCodSolicitud DESC ";
$rsSolicitudes=mysql_query($select);
$rsUsuarios=mysql_query($select);
$registros=(int)mysql_num_rows($rsUsuarios);

// Create new PHPExcel object
$excel = new PHPExcel();

// Valores a Utilizar
$titulo = "Solicitudes de Servicios";
$columna = 'A';
$fila = 5;

// Arreglo con titulos columnas
$titulos = array(
	"Código ",
	"T. Solicitud",
	"T. Servicio",
	"F. Solicitud",
	"F. Servicio",
	"Naviera",
	"Patente",
	"Cliente",
	"Buque",
	"Viaje",
	"Contenedores",
	"No. Contenedores"
);

// Ponemos las propiedades del documento
$excel->getProperties()->setCreator("One Solution")
	  ->setLastModifiedBy("One Solution")
	  ->setTitle("One Solution")
	  ->setSubject("One Solution")
	  ->setDescription("One Solution")
	  ->setKeywords("One Solution")
	  ->setCategory("One Solution");

// Encabezados del documento
$excel->getActiveSheet()->setCellValue("A1", "Soluciones de Almacenamiento y Transporte SA de CV");
$excel->getActiveSheet()->setCellValue("A2", ("Fecha del Reporte: ".date("d/m/Y H:i:s")));
$excel->getActiveSheet()->setCellValue("A3", $titulo);

// Arreglo con formato para los titulos columnas
$frEnc = new PHPExcel_Style();
$frTit = new PHPExcel_Style();
$frSep = new PHPExcel_Style();

$frEnc->applyFromArray(array('fill'	=> array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color'	=> array('rgb' => 'd6d6d6')),'font' => array('bold'=> true)));
$frTit->applyFromArray(array('fill' => array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color'	=> array('rgb' => 'f96f09')),'font'	=> array('color'=> array('rgb' => 'FFFFFF'))));
$frSep->applyFromArray(array('fill' => array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color'	=> array('rgb' => 'FFFFFF'))));

$numTitulos = sizeof($titulos);

// Ajuste de anchos de columnas
for($z=1;$z<=$numTitulos;$z++){
	// Ponemos el ancho de la columna
	//$excel->getActiveSheet()->getColumnDimension($columna)->setWidth(14);
	$excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);

	// Pintamos el titulo
	$celda = $columna."".$fila;
	$excel->setActiveSheetIndex(0)->setCellValue($celda, $titulos[$z-1]);
	
	if($z < $numTitulos){
		$columna++;
	}
}

// Formato de celdas
$excel->getActiveSheet()->setSharedStyle($frEnc, "A".$fila.":".$columna.$fila);

// Agrupamos las celdas de los encabezados
for($i=1; $i<=3; $i++){
	$excel->getActiveSheet()->setSharedStyle($frEnc, "A".$i);
	$excel->getActiveSheet()->mergeCells('A'.$i.':'.$columna.''.$i);
}
	
$excel->getActiveSheet()->setSharedStyle($frTit, "A3");
$excel->getActiveSheet()->setSharedStyle($frSep, "A4");
$excel->getActiveSheet()->setSharedStyle($frEnc, "A5");
$excel->getActiveSheet()->getRowDimension('4')->setRowHeight(5);

// Ajustamos formatos
$excel->getActiveSheet()->getStyle("A1")->getFont()->setSize(14);
$excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);

$excel->getActiveSheet()->getStyle("A3")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$excel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true);

while($rSolicitud=mysql_fetch_array($rsSolicitudes)){
	// aumentamos primero porque estabamos en fila de encabezados
	if((int)$rSolicitud{'eCodTipoSolicitud'}==1){
		$select=" SELECT * FROM relsolicitudesserviciosmercancias WHERE eCodSolicitud=".$rSolicitud{'eCodSolicitud'};
	}else{
		$select=" SELECT oem.tCodContenedor FROM relsolicitudesserviciosmercanciassalidas rs ".
				" INNER JOIN opeentradasmercancias oem ON oem.eCodEntrada=rs.eCodEntrada ".
				" WHERE rs.eCodSolicitud=".$rSolicitud{'eCodSolicitud'};
	}
	$rsContenedores=mysql_query($select);
	$eContenedores=mysql_num_rows($rsContenedores);
	$tContenedor="";
	$eContenedor=0;
	while($rContenedor=mysql_fetch_array($rsContenedores)){
		if($eContenedor<=2){
			$tContenedor.=((int)$eContenedor==0 ? "" : ",").$rContenedor{'tCodContenedor'};
		}
		$eContenedor++;
	}

	$fila++;
	$columna = 'A';
	$excel->getActiveSheet()->setCellValue($columna++.$fila, sprintf("%07d",$rSolicitud{'eCodSolicitud'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'TipoSolicitud'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'TipoServicio'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, date("d/m/Y H:i", strtotime($rSolicitud{'fhFecha'})));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, date("d/m/Y", strtotime($rSolicitud{'fhFechaServicio'})));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'Naviera'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'tPatente'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'Cliente'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'Buque'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'tNumeroViaje'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($tContenedor));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, sprintf("%03d",(int)$eContenedores));
}
// Cambiamos el nombre de la hoja
$excel->getActiveSheet()->setTitle('One Solution');

// Ocultamos las lineas de division
$excel->getActiveSheet()->setShowGridlines(false);

// Asignamos la primer hoja como la activa
$excel->setActiveSheetIndex(0);

// Redirect output to a client's web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename=Solicitud_de_Servicios.xls');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
$objWriter->save('php://output');
exit;
?>