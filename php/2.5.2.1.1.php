<?php require_once("conexion/soluciones-mysql.php");  
$link = getLink();
$select=" SELECT oss.*, ces.tNombre AS Estatus, cu.tNombre AS nUsuario, cu.tApellidos AS aUsuario, ".
		" cte.tNombre AS TipoSolicitud, cp.tNombre AS Pais, cest.tNombre AS Estado, cci.tNombre AS Ciudad, ".
		" oss.tCodEstatus, cecl.tNombre AS Cliente, cecl.tSiglas AS sCliente, ceag.tNombre AS Agente, ceag.tSiglas AS sAgente ".
		" FROM opesolicitudesaltasclientes oss ".
		" INNER JOIN catestatus ces ON ces.tCodEstatus=oss.tCodEstatus ".
		" INNER JOIN cattipossolicitudesaltas cte ON cte.eCodTipoSolicitud=oss.eCodTipoSolicitud ".
		" LEFT JOIN catusuarios cu ON cu.eCodUsuario=oss.eCodUsuario ".
		" LEFT JOIN catpaises cp ON cp.eCodPais=oss.eCodPais ".
		" LEFT JOIN catestados cest ON cest.eCodEstado=oss.eCodEstado ".
		" LEFT JOIN catciudades cci ON cci.eCodCiudad=oss.eCodCiudad ".
		" LEFT JOIN catentidades cecl ON cecl.eCodEntidad=oss.eCodEntidadRelacion ".
		" LEFT JOIN catentidades ceag ON ceag.eCodEntidad=oss.eCodEntidadSolicitante ".
		" WHERE oss.eCodRegistro=".$_GET['eCodRegistro'];
$rSolicitud=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
?>
<script type="text/javascript">
function consultar(){
	document.location = './?ePagina=2.5.2.1.php';
}
function nuevo(){
	document.location = './?ePagina=2.4.2.1.php';
}
</script>
<div id="dvCNS" style="display:none;"></div>
<form action="" method="post" name="Datos" id="Datos" onsubmit="return false;">
<table border="0" cellpadding="0" cellspacing="0" width="980px">
    <tr><td height="20"></td></tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> C&oacute;digo</td>
	    <td width="50%" nowrap class="sanLR04"><?=sprintf("%07d",$rSolicitud{'eCodRegistro'});?></td>
		<td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Estatus</td>
	    <td width="50%" nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'Estatus'});?></td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Usuario</td>
	    <td width="50%" nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'nUsuario'}." ".$rSolicitud{'aUsuario'});?></td>
		<td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Fecha de Solicitud</td>
	    <td width="50%" nowrap class="sanLR04"><?=date("d/m/Y H:i", strtotime($rSolicitud{'fhFecha'}));?></td>
    </tr>
	<tr>
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Tipo de Solicitud </td>
	    <td width="50%" nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'TipoSolicitud'});?></td>
    </tr>
	<tr><td colspan="4" align="center"><hr width="95%" size="0" align="center" color="#CACACA"></td></tr>
	<?php if((int)$rSolicitud{'eCodTipoSolicitud'}==3){ ?>
		<tr>
			<td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Cliente </td>
			<td width="50%" nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'Cliente'}." (".$rSolicitud{'sCliente'}.")");?></td>
			<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Agente</td>
			<td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'Agente'}." (".$rSolicitud{'sAgente'}.")");?></td>
		</tr>
	<?php }else{ ?>
		<tr>
			<td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> RFC </td>
			<td width="50%" nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'tRFC'});?></td>
			<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Tipo</td>
			<td nowrap class="sanLR04">Cliente</td>
		</tr>
		<tr>
			<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Nombre</td>
			<td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'tNombre'});?></td>
			<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Siglas </td>
			<td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'tSiglas'});?></td>
		</tr>
		<tr>
			<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Pa&iacute;s </td>
			<td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'Pais'});?></td>
			<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Estado</td>
			<td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'Estado'});?></td>
		</tr>
		<tr>
			<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Ciudad</td>
			<td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'Ciudad'});?></td>
			<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Colonia</td>
			<td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'tColonia'});?></td>
		</tr>
		<tr>
			<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Calle</td>
			<td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'tDireccion'});?></td>
			<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> C.P.</td>
			<td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'tCodigoPostal'});?></td>
		</tr>
		<tr>
			<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> N&uacute;mero Exterior</td>
			<td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'tNumeroExterior'});?></td>
			<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> N&uacute;mero Interior</td>
			<td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'tNumeroInterior'});?></td>
		</tr>
		<tr>
			<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Telefono</td>
			<td nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'tTelefono'});?></td>
		</tr>
		<tr>
			<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Referencia</td>
			<td nowrap class="sanLR04" colspan="3"><?=utf8_encode($rSolicitud{'tReferencia'});?></td>
		</tr>
		<? if($rSolicitud{'tURLRFC'}){ ?>
			<tr>
				<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Archivo</td>
				<td nowrap class="sanLR04" colspan="3"><a href="Patio/<?=$rSolicitud{'tURLRFC'};?>" target="_blank" class="sanT12" >RFC</a></td>
			</tr>
		<?php } ?>
	<?php } ?>
</table>
</form>