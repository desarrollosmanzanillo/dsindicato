<?php 

require("fpdf.php");

		class PDF extends FPDF
{
//Cabecera de página
		
	
    function Header()
   {
    //Logo
    $this->Image("LogoEmpresa.png" , 30 ,50, 80 , 60 , "PNG" ,"http://www.solucionesmzo.com");
    $this->Image("Logo.jpg" , 470 ,50, 80 , 60 , "JPG" ,"http://www.solucionesmzo.com");
    $this->Image("Marca.png" , 150 ,250, 380 , 380 , "PNG" ,"");
    $this->SetDrawColor(255); // color del borde
	$this->SetLineWidth(.3); //ancho del borde
    //Movernos a la derecha
    $ancho=90;
    $alto=10;
    $this->SetY(50);
    $this->Cell($ancho,$alto,'',0,0,'C');
    //Salto de línea
    $this->Ln();
    $this->Ln();
    $this->SetFont('Arial','B',8);
    $this->Cell($ancho,$alto,'',0,0,'C');
    $this->Cell($ancho,$alto,'One Solution',0,0,'L');
    $this->Ln();
    $this->Cell($ancho,$alto,'',0,0,'C');
    $this->Cell($ancho,$alto,'Soluciones de Almacenamiento y Transporte SA de CV',0,0,'L');
    //Título
    $this->Ln();
    $this->Ln();
    $this->Ln();
    $this->Ln();
    $this->SetFont('Arial','B',12);
    $this->Cell(0,16,'Detalle de Solicitud de Servicio',0,0,'C');    
	
   }
   function division()
   {
   	$this->SetX(45);
   	$this->SetFillColor(254,112,35);
   	$this->Cell(0,0.3,'',0,0,'C',true);
   	$this->SetFillColor(255);
   	$this->Ln();
   	$this->Cell(0,10,'',0,0,'C');
   	$this->Ln();   	
   }
   
      
   //Pie de página
   function Footer()
   {
    //Posición: a 1,5 cm del final
    $this->SetY(-40);
    $ancho=90;
    $alto=10;
    $this->SetFont('Arial','B',8);    
    $this->Cell(0,$alto,utf8_decode('Soluciones de Almacenamiento y Transporte SA de CV, © '.date('Y').', Todos los Derechos Reservados'),0,0,'C');
    $this->Ln();    
    $this->Cell(0,$alto,'www.solucionesmzo.com',0,0,'C',false,'www.solucionesmzo.com');
    $this->Ln(); 
    //Arial italic 8
    $this->SetFont('Arial','I',8);
    //Número de página
    $this->Cell(0,$alto,'Paginas '.$this->PageNo().'/{nb}',0,0,'C');
   }
  
   
}

//obtengo los datos a imprimir

require_once("../conexion/soluciones-mysql.php"); 
$link = getLink(); 

$select=" SELECT oss.eCodSolicitud, ces.tNombre AS Estatus, oss.fhFecha, oss.fhFechaServicio, ".
		" cen.tNombre AS Naviera, oss.tPatente, cec.tNombre AS Cliente, ".
		" cef.tNombre AS FacturarA, ".		
		" cbu.tNombre AS Buque, oss.tNumeroViaje, cts.tNombre AS TipoSolicitud, cto.tNombre AS TipoServicio, ".
		" oss.eCodTipoServicio, oss.eCodTipoSolicitud, oss.tBL ".
		" FROM opesolicitudesservicios oss ".
		" INNER JOIN catestatus ces ON ces.tCodEstatus=oss.tCodEstatus ".
		" INNER JOIN catentidades cen ON cen.eCodEntidad=oss.eCodNaviera ".
		" INNER JOIN catentidades cec ON cec.eCodEntidad=oss.eCodCliente ".
		" INNER JOIN catentidades cef ON cef.eCodEntidad=oss.eCodFacturarA ".
		" INNER JOIN catbuques cbu ON cbu.eCodBuque=oss.eCodBuque ".
		" INNER JOIN cattipossolicitudes cts ON cts.eCodTipoSolicitud=oss.eCodTipoSolicitud ".
		" INNER JOIN cattiposservicios cto ON cto.eCodTipoServicio=oss.eCodTipoServicio ".
		" WHERE oss.eCodSolicitud=".(int)$_GET['eCodSolicitud'];
$rSolicitud=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

$select=" SELECT cef.*, cep.tNombre as tPais, cee.tNombre as tEstado, cec.tNombre as tCiudad ".		
		" FROM opesolicitudesservicios oss ".
		" INNER JOIN catentidades cef ON cef.eCodEntidad=oss.eCodFacturarA ".
		" INNER JOIN catpaises cep ON cep.eCodPais=cef.eCodPais ".
		" INNER JOIN catestados cee ON cee.eCodEstado=cef.eCodEstado ".		
		" INNER JOIN catciudades cec ON cec.eCodCiudad=cef.eCodCiudad ".
		" WHERE oss.eCodSolicitud=".(int)$_GET['eCodSolicitud'];
$rFacturar=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

$select=" SELECT rs.eCantidad, rs.dImporte, rs.dSubtotal, cs.tNombre AS Servicio ".
		" FROM relsolicitudesserviciosservicios rs ".
		" INNER JOIN catservicios cs ON cs.eCodServicio=rs.eCodServicio ".
		" WHERE rs.eCodSolicitud=".(int)$_GET['eCodSolicitud'];
$rsServicios=mysqli_query($link,$select);

$select=" SELECT rsi.dValorImpuesto, rsi.dImpuesto, ci.tNombre AS Impuesto ".
		" FROM relsolicitudesserviciosimpuestos rsi ".
		" INNER JOIN catimpuestos ci ON ci.eCodImpuesto=rsi.eCodImpuesto ".
		" WHERE rsi.eCodSolicitud=".(int)$_GET['eCodSolicitud'];
$rsImpuestos=mysqli_query($link,$select);

if((int)$rSolicitud{'eCodTipoSolicitud'}==1){
	$select=" SELECT ".((int)$rSolicitud{'eCodTipoServicio'}==1 ? "rs.tCodContenedor" : "")." AS tMercancia, ".
			((int)$rSolicitud{'eCodTipoServicio'}==1 ? "ctc.tNombreCorto" : "")." AS TipoEmbalaje, rs.tObservaciones ".
			" FROM relsolicitudesserviciosmercancias rs ".
			" LEFT JOIN cattiposcontenedores ctc ON ctc.eCodTipoContenedor=rs.eCodTipoContenedor ".
			" WHERE rs.eCodSolicitud=".(int)$_GET['eCodSolicitud'];
}else{
	$select=" SELECT ".((int)$rSolicitud{'eCodTipoServicio'}==1 ? "rs.tCodContenedor" : "")." AS tMercancia, ".
			((int)$rSolicitud{'eCodTipoServicio'}==1 ? "ctc.tNombreCorto" : "")." AS TipoEmbalaje, rs.tObservaciones ".
			" FROM opeentradasmercancias rs ".
			" INNER JOIN relsolicitudesserviciosmercanciassalidas rse ON rse.eCodEntrada=rs.eCodEntrada ".
			" LEFT JOIN cattiposcontenedores ctc ON ctc.eCodTipoContenedor=rs.eCodTipoContenedor ".
            " WHERE rse.eCodSolicitud=".(int)$_POST['codigo'];
			//" WHERE rse.eCodSolicitud=".(int)$_GET['eCodSolicitud'];
}
$rsMercancias=mysqli_query($link,$select);




// incio el PDF.

$pdf= new PDF('P','pt','Letter');
$pdf->AddPage('P','Letter');

//creo el detalle de la solicitud
	$TipoServicio=90;
    $Embalaje=150;
    $Observaciones=100;
	$anchoEtiquetaI=90;
	$anchoEtiquetaD=90;
	$anchoDatosI=200;
	$anchoDatosD=90;
	$cantidad=90;
	$Descripcion=200;
	$PUnitario=90;
	$importe=90;
    $alto=10;
    $type='Arial';
    $size=8;
    $pdf->SetY(250);
    $pdf->SetX(45);
    $pdf->SetFont($type,'B',$size);
    $pdf->Cell($anchoEtiquetaI,$alto,utf8_decode('Código'),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosI,$alto,utf8_decode(sprintf("%07d",$rSolicitud{'eCodSolicitud'})),0,0,'L');
    $pdf->SetFont($type,'B',$size);
    $pdf->Cell($anchoEtiquetaD,$alto,utf8_decode('Estatus'),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosD,$alto,utf8_decode($rSolicitud{'Estatus'}),0,0,'L');
    $pdf->Ln();
    $pdf->Ln();
    $pdf->SetX(45);
    $pdf->SetFont($type,'B',$size);
    $pdf->Cell($anchoEtiquetaI,$alto,utf8_decode('Fecha de Solicitud'),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosI,$alto,utf8_decode(date("d/m/Y H:i", strtotime($rSolicitud{'fhFecha'}))),0,0,'L');
    $pdf->SetFont($type,'B',$size);
    $pdf->Cell($anchoEtiquetaD,$alto,utf8_decode('Fecha de Servicio'),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosD,$alto,utf8_decode(date("d/m/Y", strtotime($rSolicitud{'fhFechaServicio'}))),0,0,'L');
    
    $pdf->Ln();
    $pdf->Ln();
    $pdf->SetX(45);
    $pdf->SetFont($type,'B',$size);
    $pdf->Cell($anchoEtiquetaI,$alto,utf8_decode('Cliente'),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosI,$alto,utf8_encode($rSolicitud{'Cliente'}),0,0,'L');
    $pdf->SetFont($type,'B',$size);
    $pdf->Cell($anchoEtiquetaD,$alto,utf8_decode('BL'),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosD,$alto,utf8_encode($rSolicitud{'tBL'}) ,0,0,'L');
    $pdf->Ln();
    $pdf->Ln();
    $pdf->division();    
    $pdf->SetX(45);
    $pdf->SetFont($type,'B',$size);
    $pdf->Cell($anchoEtiquetaI,$alto,utf8_decode('Facturar A'),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosI,$alto,utf8_encode($rSolicitud{'FacturarA'}),0,0,'L');
    $pdf->Ln();
    $pdf->Ln();
    $pdf->SetX(45);
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoEtiquetaI,$alto,utf8_decode(''),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosI,$alto,utf8_encode($rFacturar{'tDireccion'}." # ".$rFacturar{'tNumeroExterior'}." Int ".$rFacturar{'tNumeroInterior'}),0,0,'L');
    $pdf->Ln();
    $pdf->Ln();
    $pdf->SetX(45);
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoEtiquetaI,$alto,utf8_decode(''),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosI,$alto,utf8_encode($rFacturar{'tColonia'}.", ".$rFacturar{'tCiudad'}.", ".$rFacturar{'tEstado'}.", ".$rFacturar{'tPais'}),0,0,'L');
    $pdf->Ln();
    $pdf->Ln();
    $pdf->SetX(45);
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoEtiquetaI,$alto,utf8_decode(''),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosI,$alto,utf8_encode("C.P. ".$rFacturar{'tCodigoPostal'}." RFC ".$rFacturar{'tRFC'}),0,0,'L');
    
    $pdf->Ln();
    $pdf->Ln();
    $pdf->SetX(45);
    $pdf->SetFont($type,'B',$size);
    $pdf->Cell($anchoEtiquetaI,$alto,utf8_decode('Tipo de Solicitud'),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosI,$alto,utf8_encode($rSolicitud{'TipoSolicitud'}),0,0,'L');
    $pdf->SetFont($type,'B',$size);
    $pdf->Cell($anchoEtiquetaD,$alto,utf8_encode('Tipo de Servicio'),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosD,$alto,utf8_encode($rSolicitud{'TipoServicio'}),0,0,'L');
    $pdf->Ln();
    $pdf->Ln();
    $pdf->division();    
    $pdf->SetX(45);
    $pdf->SetFont($type,'B',$size);
    $pdf->Cell($cantidad,$alto,utf8_decode('Cantidad'),0,0,'L');
    $pdf->Cell($Descripcion,$alto,utf8_encode('Descripcion'),0,0,'L');    
    $pdf->Cell($PUnitario,$alto,utf8_encode('Precio Unitario'),0,0,'R');    
    $pdf->Cell($importe,$alto,utf8_encode('Importe'),0,0,'R');
    $pdf->Ln();
    $pdf->Ln();
    $pdf->division();
    				$dSubtotal=0;
					$dTotal=0;
					while($rServicio=mysqli_fetch_array($rsServicios,MYSQLI_ASSOC))
					{
						$pdf->SetX(45);
    					$pdf->SetFont($type,'',$size);
    					$pdf->Cell($cantidad,$alto,utf8_decode(sprintf("%02d",$rServicio{'eCantidad'})),0,0,'L');
    					$pdf->Cell($Descripcion,$alto,utf8_encode($rServicio{'Servicio'}),0,0,'L');    
    					$pdf->Cell($PUnitario,$alto,number_format($rServicio{'dImporte'},2),0,0,'R');    
    					$pdf->Cell($importe,$alto,number_format($rServicio{'dSubtotal'},2),0,0,'R');
    					$pdf->Ln();
    					$pdf->Ln();
						$dSubtotal=(float)$dSubtotal+(float)$rServicio{'dSubtotal'};
					}
						$dTotal=$dSubtotal;
						$pdf->SetX(45);
    					$pdf->SetFont($type,'',$size);
    					$pdf->Cell($cantidad,$alto,'',0,0,'L');
    					$pdf->Cell($Descripcion,$alto,'',0,0,'L');  
    					$pdf->SetFont($type,'B',$size);  
    					$pdf->Cell($PUnitario,$alto,utf8_decode('Subtotal'),0,0,'R'); 
    					$pdf->SetFont($type,'',$size);   
    					$pdf->Cell($importe,$alto,number_format($dSubtotal, 2),0,0,'R');
    					
    					$pdf->Ln();
    					while($rImpuesto=mysqli_fetch_array($rsImpuestos,MYSQLI_ASSOC))
    					{
    						$pdf->SetX(45);
    						$pdf->SetFont($type,'',$size);
    						$pdf->Cell($cantidad,$alto,'',0,0,'L');
    						$pdf->Cell($Descripcion,$alto,'',0,0,'L');  
    					 	$pdf->SetFont($type,'B',$size);  
    						$pdf->Cell($PUnitario,$alto,utf8_decode($rImpuesto{'Impuesto'}." ".number_format($rImpuesto{'dValorImpuesto'},2)." ".'%'),0,0,'R'); 
    						$pdf->SetFont($type,'',$size);   
    						$pdf->Cell($importe,$alto,number_format($rImpuesto{'dImpuesto'}, 2),0,0,'R');
    						
    						$pdf->Ln();
    						$dTotal=(float)$dTotal+(float)$rImpuesto{'dImpuesto'}; 
    					}
    					$pdf->SetX(45);
    					$pdf->SetFont($type,'',$size);
    					$pdf->Cell($cantidad,$alto,'',0,0,'L');
    					$pdf->Cell($Descripcion,$alto,'',0,0,'L');  
    					$pdf->SetFont($type,'B',$size);  
    					$pdf->Cell($PUnitario,$alto,utf8_decode('Total'),0,0,'R'); 
    					$pdf->SetFont($type,'',$size);   
    					$pdf->Cell($importe,$alto,number_format($dTotal, 2),0,0,'R');
    					$pdf->Ln();
    					$pdf->Ln();
    $pdf->Ln();
    $pdf->Ln();
    $pdf->division();
    $pdf->SetX(45);
    $pdf->SetFont($type,'B',$size);
    $pdf->Cell($TipoServicio,$alto,utf8_decode(((int)$rSolicitud{'eCodTipoServicio'}==1 ? "Contenedor" : "Mercancía")),0,0,'L');
    $pdf->Cell($Embalaje,$alto,utf8_encode(((int)$rSolicitud{'eCodTipoServicio'}==1 ? "Tipo" : "Embalaje")),0,0,'L');    
    $pdf->Cell($Observaciones,$alto,utf8_encode('Observaciones'),0,0,'L');    
    $pdf->Cell($importe,$alto,'',0,0,'R');
    $pdf->Ln();
    $pdf->Ln();
    $pdf->division();
    		while($rMercancia=mysqli_fetch_array($rsMercancias,MYSQLI_ASSOC))
    		{
    			$pdf->SetX(45);
    			$pdf->SetFont($type,'',$size);
    			$pdf->Cell($TipoServicio,$alto,utf8_encode($rMercancia{'tMercancia'}),0,0,'L');
    			$pdf->Cell($Embalaje,$alto,utf8_encode($rMercancia{'TipoEmbalaje'}),0,0,'L');    
   				$pdf->Cell($Observaciones,$alto,utf8_encode($rMercancia{'tObservaciones'}),0,0,'L');    
    			$pdf->Cell($importe,$alto,'',0,0,'R');
    			$pdf->Ln();
    			$pdf->Ln();
    		}
    $pdf->division();

   
$pdf->AliasNbPages();
$pdf->Output('Solicitud '.sprintf("%07d",$rSolicitud{'eCodSolicitud'}).".pdf", 'I' );
?>