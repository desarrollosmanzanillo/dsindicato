<?php 

require("fpdf.php");

		class PDF extends FPDF
{
//Cabecera de página
		
	
    function Header()
   {
    //Logo
    $this->Image("LogoEmpresa.png" , 30 ,50, 80 , 60 , "PNG" ,"http://www.solucionesmzo.com");
    $this->Image("Logo.jpg" , 470 ,50, 80 , 60 , "JPG" ,"http://www.solucionesmzo.com");
    $this->Image("Marca.png" , 150 ,250, 380 , 380 , "PNG" ,"");
    $this->SetDrawColor(255); // color del borde
	$this->SetLineWidth(.3); //ancho del borde
    //Movernos a la derecha
    $ancho=90;
    $alto=10;
    $this->SetY(50);
    $this->Cell($ancho,$alto,'',0,0,'C');
    //Salto de línea
    $this->Ln();
    $this->Ln();
    $this->SetFont('Arial','B',8);
    $this->Cell($ancho,$alto,'',0,0,'C');
    $this->Cell($ancho,$alto,'One Solution',0,0,'L');
    $this->Ln();
    $this->Cell($ancho,$alto,'',0,0,'C');
    $this->Cell($ancho,$alto,'Soluciones de Almacenamiento y Transporte SA de CV',0,0,'L');
    //Título
    $this->Ln();
    $this->Ln();
    $this->Ln();
    $this->Ln();
    $this->SetFont('Arial','B',12);
    $this->Cell(0,16,'Detalle de Comprobante Fiscal Digital',0,0,'C');    
	
   }
   function division()
   {
   	$this->SetX(45);
   	$this->SetFillColor(254,112,35);
   	$this->Cell(0,0.3,'',0,0,'C',true);
   	$this->SetFillColor(255);
   	$this->Ln();
   	$this->Cell(0,10,'',0,0,'C');
   	$this->Ln();   	
   }
   
      
   //Pie de página
   function Footer()
   {
    //Posición: a 1,5 cm del final
    $this->SetY(-60);
    $ancho=90;
    $alto=10;
    $this->SetFont('Arial','B',8);
    $this->division();
    $this->Ln();   
    $this->Cell(0,$alto,utf8_decode('Soluciones de Almacenamiento y Transporte SA de CV, © '.date('Y').', Todos los Derechos Reservados'),0,0,'C');
    $this->Ln();    
    $this->Cell(0,$alto,'www.solucionesmzo.com',0,0,'C',false,'www.solucionesmzo.com');
    $this->Ln(); 
    //Arial italic 8
    $this->SetFont('Arial','I',8);
    //Número de página
    $this->Cell(0,$alto,'Paginas '.$this->PageNo().'/{nb}',0,0,'C');
   }
  
   
}

//obtengo los datos a imprimir

require_once("../conexion/soluciones-mysql.php");
$link = getLink();
/*
$select=" SELECT oc.eCodCFD, oc.tCodEstatus, cc.tNombre AS Comprobante, oc.tSerie, oc.eFolio, ".
        " oc.fhFecha, oc.fhFechaCreacion, cu.tNombre AS Usuario, cu.tApellidos AS UsuarioA, ".
        " sea.tNombre AS Naviera, ce.tNombre AS Estatus, ccm.tNombre AS Comprobante, oc.tFormaPago, ".
        " oc.tMetodoPago, oc.tCondicionesPago, oc.tMotivoCancelacion, oc.tNombreEmisor, oc.tCalleEmisor, ".
        " oc.tNumeroExteriorEmisor, oc.tColoniaEmisor, oc.tMunicipioEmisor, oc.tEstadoEmisor, ".
        " oc.tPaisEmisor, oc.tCPEmisor, oc.tRFCEmisor, oc.tNombreReceptor, oc.tRFCReceptor, ".
        " oc.tCalleReceptor, oc.tNumeroExteriorReceptor, oc.tColoniaReceptor, oc.tCPReceptor, ".
        " oc.tMunicipioReceptor, oc.tEstadoReceptor, oc.tPaisReceptor, oc.dImpuestosTrasladados ".
        " FROM opecfds oc ".
        " INNER JOIN catcomprobantes cc ON cc.eCodComprobante=oc.eCodComprobante ".
        " LEFT JOIN catusuarios cu ON cu.eCodUsuario=oc.eCodUsuario ".
        " LEFT JOIN catentidades sea ON sea.eCodEntidad=oc.eCodNaviera ".
        " LEFT JOIN catestatus ce ON ce.tCodEstatus=oc.tCodEstatus ".
        " LEFT JOIN catcomprobantes ccm ON ccm.eCodComprobante = oc.eCodComprobante ".
        " WHERE oc.eCodCFD=".(int)$_GET['eCodCFD'];
$rCFD=mysql_fetch_array(mysql_query($select));

$select=" SELECT rc.dCantidad, rc.tUnidad, cs.tCuentaContable, ".
        " rc.tDescripcion, rc.dValorUnitario, rc.dImporte, rc.dImporte ".
        " FROM relcfdsconceptos rc ".
        " LEFT JOIN catservicios cs ON cs.eCodServicio=rc.eCodServicio ".
        " WHERE rc.eCodCFD=".(int)$_GET['eCodCFD'];
$rsConceptos=mysql_query($select); 

$select=" SELECT rsi.dValorImpuesto, rsi.dImpuesto, ci.tNombre AS Impuesto ".
        " FROM relcfdserviciosimpuestos rsi ".
        " INNER JOIN catimpuestos ci ON ci.eCodImpuesto=rsi.eCodImpuesto ".
        " WHERE rsi.eCodCFD=".(int)$_GET['eCodCFD'];
$rsImpuestos=mysql_query($select);
*/
$select=" SELECT oc.eCodCFD, oc.tCodEstatus, cc.tNombre AS Comprobante, oc.tSerie, oc.eFolio, ".
        " oc.fhFecha, oc.fhFechaCreacion, cu.tNombre AS Usuario, cu.tApellidos AS UsuarioA, ".
        " sea.tNombre AS Naviera, ce.tNombre AS Estatus, ccm.tNombre AS Comprobante, oc.tFormaPago, ".
        " oc.tMetodoPago, oc.tCondicionesPago, oc.tMotivoCancelacion, oc.tNombreEmisor, oc.tCalleEmisor, ".
        " oc.tNumeroExteriorEmisor, oc.tColoniaEmisor, oc.tMunicipioEmisor, oc.tEstadoEmisor, ".
        " oc.tPaisEmisor, oc.tCPEmisor, oc.tRFCEmisor, oc.tNombreReceptor, oc.tRFCReceptor, oc.bTimbrado, ".
        " oc.tCalleReceptor, oc.tNumeroExteriorReceptor, oc.tColoniaReceptor, oc.tCPReceptor, ".
        " oc.tMunicipioReceptor, oc.tEstadoReceptor, oc.tPaisReceptor, oc.dImpuestosTrasladados, ".
        " oc.eCodUsuarioCancelacion, cuc.tNombre AS UsuarioC, cuc.tApellidos AS UsuarioCA, ".
        " oc.fhFechaCancelacion, oc.tReferencia, rcs.eCodSolicitud ".//
        " FROM opecfds oc ".
        " INNER JOIN catcomprobantes cc ON cc.eCodComprobante=oc.eCodComprobante ".
        " LEFT JOIN catusuarios cu ON cu.eCodUsuario=oc.eCodUsuario ".
        " LEFT JOIN catusuarios cuc ON cuc.eCodUsuario=oc.eCodUsuarioCancelacion ".
        " LEFT JOIN catentidades sea ON sea.eCodEntidad=oc.eCodNaviera ".
        " LEFT JOIN catestatus ce ON ce.tCodEstatus=oc.tCodEstatus ".
        " LEFT JOIN catcomprobantes ccm ON ccm.eCodComprobante = oc.eCodComprobante ".
        " LEFT JOIN relcfdssolicitudesservicios rcs ON rcs.eCodCFD=oc.eCodCFD ".
        " WHERE oc.eCodCFD=".(int)$_GET['eCodCFD'];
$rCFD=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

$select=" SELECT rc.eCodConcepto, rc.dCantidad, rc.tUnidad, cs.tCuentaContable, ".
        " rc.tDescripcion, rc.dValorUnitario, rc.dImporte, rc.dImporte ".
        " FROM relcfdsconceptos rc ".
        " LEFT JOIN catservicios cs ON cs.eCodServicio=rc.eCodServicio ".
        " WHERE rc.eCodCFD=".(int)$_GET['eCodCFD'];
$rsConceptos=mysqli_query($link,$select);



// incio el PDF.

$pdf= new PDF('P','pt','Letter');
$pdf->AddPage('P','Letter');

//creo el detalle de la solicitud
	$TipoServicio=90;
    $Embalaje=150;
    $Observaciones=100;
	$anchoEtiquetaI=90;
	$anchoEtiquetaD=90;
	$anchoDatosI=200;
	$anchoDatosD=90;
	$cantidad=45;
    $Unidad=45;
    $ctacontable=70;
	$Descripcion=200;
	$PUnitario=90;
	$importe=90;    
    $alto=10;
    $cabAlto=15;
    $type='Arial';
    $size=8;
    $pdf->SetY(175);
    $pdf->SetX(45);
    $pdf->SetFont($type,'B',$size);
    $pdf->Cell($anchoEtiquetaI,$alto,utf8_decode('Código'),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosI,$alto,utf8_decode($rCFD{'tSerie'}.sprintf("%07d",$rCFD{'eFolio'})),0,0,'L');
    $pdf->SetFont($type,'B',$size);
    $pdf->Cell($anchoEtiquetaD,$alto,utf8_decode('Estatus'),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosD,$alto,utf8_decode($rCFD{'Estatus'}),0,0,'L');
    $pdf->Ln();
    $pdf->Ln();
    $pdf->SetX(45);
    $pdf->SetFont($type,'B',$size);
    $pdf->Cell($anchoEtiquetaI,$alto,utf8_decode('Comprobante'),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosI,$alto,utf8_decode($rCFD{'Comprobante'}),0,0,'L');
    $pdf->SetFont($type,'B',$size);
    $pdf->Cell($anchoEtiquetaD,$alto,utf8_decode('Fecha'),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosD,$alto,utf8_decode(date("d/m/Y H:i", strtotime($rCFD{'fhFechaCreacion'}))),0,0,'L');
    $pdf->Ln();
    $pdf->Ln();
    $pdf->SetX(45);
    $pdf->SetFont($type,'B',$size);
    $pdf->Cell($anchoEtiquetaI,$alto,utf8_decode('Usuario'),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosI,$alto,utf8_encode($rCFD{'Usuario'}." ".$rCFD{'UsuarioA'}),0,0,'L');
    $pdf->SetFont($type,'B',$size);
    $pdf->Cell($anchoEtiquetaD,$alto,utf8_decode(''),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosD,$alto,utf8_encode() ,0,0,'L');
    $pdf->Ln();
    $pdf->Ln();
    $pdf->SetX(45);
    $pdf->SetFont($type,'B',$size);
    $pdf->Cell($anchoEtiquetaI,$alto,utf8_decode('Forma de Pago'),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosI,$alto,utf8_decode($rCFD{'tFormaPago'}),0,0,'L');
    $pdf->SetFont($type,'B',$size);
    $pdf->Cell($anchoEtiquetaD,$alto,utf8_decode('Metodo de Pago'),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosD,$alto,utf8_encode($rCFD{'tMetodoPago'}) ,0,0,'L');
    $pdf->Ln();
    $pdf->Ln();
    $pdf->SetX(45);
    $pdf->SetFont($type,'B',$size);
    $pdf->Cell($anchoEtiquetaI,$alto,utf8_decode('Condiciones de Pago'),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosI,$alto,utf8_encode($rCFD{'tCondicionesPago'}),0,0,'L');
    $pdf->SetFont($type,'B',$size);
    $pdf->Cell($anchoEtiquetaD,$alto,utf8_decode(''),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosD,$alto,utf8_encode() ,0,0,'L');
    $pdf->Ln();
    $pdf->Ln();
    $pdf->division();    
    $pdf->SetX(45);
    $pdf->SetFont($type,'B',$size);
    $pdf->Cell($anchoEtiquetaI,$alto,utf8_decode('Emisor'),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosI,$alto,utf8_encode($rCFD{'tNombreEmisor'}." (".$rCFD{'tRFCEmisor'}.")"),0,0,'L');
    $pdf->Ln();
    $pdf->Ln();
    $pdf->SetX(45);
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoEtiquetaI,$alto,utf8_decode(''),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosI,$alto,utf8_decode($rCFD{'tCalleEmisor'}." ".$rCFD{'tNumeroExteriorEmisor'}.", Col. ".$rCFD{'tColoniaEmisor'}." C.P.".$rCFD{'tCPEmisor'}),0,0,'L');
    $pdf->Ln();
    $pdf->Ln();
    $pdf->SetX(45);
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoEtiquetaI,$alto,utf8_decode(''),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosI,$alto,utf8_decode($rCFD{'tMunicipioEmisor'}.", ".$rCFD{'tEstadoEmisor'}.", ".$rCFD{'tPaisEmisor'}),0,0,'L');    
    $pdf->Ln();
    $pdf->Ln();
    $pdf->division();        
    $pdf->SetX(45);
    $pdf->SetFont($type,'B',$size);
    $pdf->Cell($anchoEtiquetaI,$alto,utf8_decode('Receptor'),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosI,$alto,utf8_decode($rCFD{'tNombreReceptor'}." (".$rCFD{'tRFCReceptor'}.")"),0,0,'L');
    $pdf->Ln();
    $pdf->Ln();
    $pdf->SetX(45);
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoEtiquetaI,$alto,utf8_decode(''),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosI,$alto,utf8_decode($rCFD{'tCalleReceptor'}." ".$rCFD{'tNumeroExteriorReceptor'}.", Col. ".$rCFD{'tColoniaReceptor'}." C.P.".$rCFD{'tCPReceptor'}),0,0,'L');
    $pdf->Ln();
    $pdf->Ln();
    $pdf->SetX(45);
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoEtiquetaI,$alto,utf8_decode(''),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosI,$alto,utf8_decode($rCFD{'tMunicipioReceptor'}.", ".$rCFD{'tEstadoReceptor'}.", ".$rCFD{'tPaisReceptor'}),0,0,'L');    
    $pdf->Ln();
    $pdf->Ln();
    $pdf->division();
    $pdf->SetX(45);
    $pdf->SetFont($type,'B',$size);
    $pdf->Cell($anchoEtiquetaI,$alto,utf8_decode('Referencia'),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosI,$alto,utf8_encode($rCFD{'tReferencia'}),0,0,'L');
    $pdf->SetFont($type,'B',$size);
    $pdf->Cell($anchoEtiquetaD,$alto,utf8_encode('Solicitud'),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosD,$alto,utf8_encode(sprintf("%07d",$rCFD{'eCodSolicitud'})),0,0,'L');
    $pdf->Ln();
    $pdf->Ln();        
    $pdf->SetX(45);
    $pdf->SetFont($type,'B',$size);
    $pdf->SetFillColor(254,112,35);
    $pdf->Cell($cantidad,$cabAlto,utf8_decode('Cantidad'),0,0,'L',true);
    $pdf->Cell($Unidad,$cabAlto,utf8_encode('Unidad'),0,0,'L',true);    
    $pdf->Cell($ctacontable,$cabAlto,utf8_encode('Clave'),0,0,'C',true);    
    $pdf->Cell($Descripcion,$cabAlto,utf8_decode('Descripción'),0,0,'L',true);    
    $pdf->Cell($PUnitario,$cabAlto,utf8_encode('Precio Unitario'),0,0,'R',true);
    $pdf->Cell($importe,$cabAlto,utf8_encode('Importe'),0,0,'R',true);
    $pdf->Ln();
    $pdf->Ln();
    $pdf->SetFillColor(255);
    
    				$dSubtotal=0;
					$dTotal=0;
					while($rConcepto=mysqli_fetch_array($rsConceptos,MYSQLI_ASSOC))
					{
						$tDescripcion="";
                        $pdf->SetX(45);
    					$pdf->SetFont($type,'',$size);
    					$pdf->Cell($cantidad,$alto,utf8_decode(sprintf("%02d",$rConcepto{'dCantidad'})),0,0,'C');
    					$pdf->Cell($Unidad,$alto,utf8_encode($rConcepto{'tUnidad'}),0,0,'C');
    					$pdf->Cell($ctacontable,$alto,utf8_encode($rConcepto{'tCuentaContable'}),0,0,'C');
                        $pdf->Cell($Descripcion,$alto,utf8_encode($rConcepto{'tDescripcion'}),0,0,'L');
                        $pdf->Cell($PUnitario,$alto,number_format($rConcepto{'dValorUnitario'},2),0,0,'R');
    					$pdf->Cell($importe,$alto,number_format($rConcepto{'dImporte'},2),0,0,'R');
    					$pdf->Ln();
    					$pdf->Ln();
						$dSubtotal+=(float)$rConcepto{'dImporte'};
                        $select=" SELECT * ".
                                " FROM relcfdsdescripcionesconceptos ".
                                " WHERE eCodCFD=".(int)$_GET['eCodCFD']." AND eCodConcepto=".$rConcepto{'eCodConcepto'};
                        $rsDescripcionConceptos=mysqli_query($link,$select);
                        while($rDescripcionConcepto=mysqli_fetch_array($rsDescripcionConceptos,MYSQLI_ASSOC))
                        { 
                                $pdf->SetX(45);
                                $pdf->SetFont($type,'',$size);
                                $pdf->Cell($cantidad,$alto,'',0,0,'C');
                                $pdf->Cell($Unidad,$alto,'',0,0,'C');
                                $pdf->Cell($ctacontable,$alto,'',0,0,'C');
                                $pdf->Cell($Descripcion,$alto,utf8_encode($rDescripcionConcepto{'tValor'}),0,0,'L');
                                $pdf->Cell($PUnitario,$alto,'',0,0,'R');
                                $pdf->Cell($importe,$alto,'',0,0,'R');
                                $pdf->Ln();
                                $pdf->Ln();                        
                        }
					}
						$dTotal=$dSubtotal;
						$pdf->SetX(45);
    					$pdf->SetFont($type,'',$size);
    					$pdf->Cell($cantidad,$alto,'',0,0,'L');
                        $pdf->Cell($Unidad,$alto,'',0,0,'C');
                        $pdf->Cell($ctacontable,$alto,'',0,0,'C');
    					$pdf->Cell($Descripcion,$alto,'',0,0,'L');  
    					$pdf->SetFont($type,'B',$size);  
    					$pdf->Cell($PUnitario,$alto,utf8_decode('Subtotal'),0,0,'R'); 
    					$pdf->SetFont($type,'',$size);   
    					$pdf->Cell($importe,$alto,number_format($dSubtotal, 2),0,0,'R');
    					
    					$pdf->Ln();
    					//while($rImpuesto=mysql_fetch_array($rsImpuestos))
    					//{
    						$pdf->SetX(45);
                            $pdf->SetFont($type,'',$size);
                            $pdf->Cell($cantidad,$alto,'',0,0,'L');
                            $pdf->Cell($Unidad,$alto,'',0,0,'C');
                            $pdf->Cell($ctacontable,$alto,'',0,0,'C');
                            $pdf->Cell($Descripcion,$alto,'',0,0,'L');  
                            $pdf->SetFont($type,'B',$size);    
    						//$pdf->Cell($PUnitario,$alto,utf8_decode($rImpuesto{'Impuesto'}." ".number_format($rImpuesto{'dValorImpuesto'},2)." ".'%'),0,0,'R'); 
                            $pdf->Cell($PUnitario,$alto,utf8_decode('IVA 16.00 %'),0,0,'R'); 
    						$pdf->SetFont($type,'',$size);   
    						$pdf->Cell($importe,$alto,number_format($rCFD{'dImpuestosTrasladados'}, 2),0,0,'R');
    						
    						$pdf->Ln();
    						$dTotal=(float)$dSubtotal+(float)$rCFD{'dImpuestosTrasladados'};
    					//}
    					$pdf->SetX(45);
                        $pdf->SetFont($type,'',$size);
                        $pdf->Cell($cantidad,$alto,'',0,0,'L');
                        $pdf->Cell($Unidad,$alto,'',0,0,'C');
                        $pdf->Cell($ctacontable,$alto,'',0,0,'C');
                        $pdf->Cell($Descripcion,$alto,'',0,0,'L');  
                        $pdf->SetFont($type,'B',$size);    
    					$pdf->Cell($PUnitario,$alto,utf8_decode('Total'),0,0,'R'); 
    					$pdf->SetFont($type,'',$size);   
    					$pdf->Cell($importe,$alto,number_format($dTotal, 2),0,0,'R');
    					$pdf->Ln();
    					$pdf->Ln();
    $pdf->Ln();
    $pdf->Ln();   
    

   
$pdf->AliasNbPages();
$pdf->Output('Solicitud '.sprintf("%07d",$rSolicitud{'eCodSolicitud'}).".pdf", 'I' );
?>