<?php 
require("fpdf.php");

		class PDF extends FPDF
{
//Cabecera de p�gina
		
	
    function Header()
   {
    //Logo
    $this->Image("LogoEmpresa.png" , 30 ,50, 80 , 60 , "PNG" ,"http://www.solucionesmzo.com");
    $this->Image("Logo.jpg" , 470 ,50, 80 , 60 , "JPG" ,"http://www.solucionesmzo.com");
    //$this->Image("Marca.png" , 150 ,250, 380 , 380 , "PNG" ,"");
    $this->SetDrawColor(255); // color del borde
	$this->SetLineWidth(.3); //ancho del borde
    //Movernos a la derecha
    $ancho=90;
    $alto=10;
    $this->SetY(50);
    $this->Cell($ancho,$alto,'',0,0,'C');
    //Salto de l�nea
    $this->Ln();
    $this->Ln();
    $this->SetFont('Arial','B',8);
    $this->Cell($ancho,$alto,'',0,0,'C');
    $this->Cell($ancho,$alto,'One Solution',0,0,'L');
    $this->Ln();
    $this->Cell($ancho,$alto,'',0,0,'C');
    $this->Cell($ancho,$alto,'Soluciones de Almacenamiento y Transporte SA de CV',0,0,'L');
    //T�tulo
    $this->Ln();
    $this->Ln();
    $this->Ln();
    $this->Ln();
    $this->SetFont('Arial','B',12);
    $this->Cell(0,16,'Detalle de EIR de Entrada',0,0,'C');    
	
   }
   function division()
   {
   	$this->SetX(45);
   	$this->SetFillColor(254,112,35);
   	$this->Cell(0,0.3,'',0,0,'C',true);
   	$this->SetFillColor(255);
   	$this->Ln();
   	$this->Cell(0,10,'',0,0,'C');
   	$this->Ln();   	
   }
   
      
   //Pie de p�gina
   function Footer()
   {
    //Posici�n: a 1,5 cm del final
    $this->SetY(-40);
    $ancho=90;
    $alto=10;
    $this->SetFont('Arial','B',8);    
    $this->Cell(0,$alto,('Soluciones de Almacenamiento y Transporte SA de CV, � '.date('Y').', Todos los Derechos Reservados'),0,0,'C');
    $this->Ln();    
    $this->Cell(0,$alto,'www.solucionesmzo.com',0,0,'C',false,'www.solucionesmzo.com');
    $this->Ln(); 
    //Arial italic 8
    $this->SetFont('Arial','I',8);
    //N�mero de p�gina
    $this->Cell(0,$alto,'Paginas '.$this->PageNo().'/{nb}',0,0,'C');
   }
  
   
}

//obtengo los datos a imprimir

require_once("../conexion/soluciones-mysql.php");
require_once("../conexion/sistema.php");
$link = getLink();
$pagina = new sistema();
$select=" SELECT oss.eCodEntrada, oss.fhFechaEntrada, bei.eCodEIR, oss.tCodContenedor, ".
		" ctc.tNombreCorto AS TipoContenedor ".
		" FROM biteirs bei ".
		" INNER JOIN opeentradasmercancias oss ON oss.eCodEntrada=bei.eCodEntrada ".
		" LEFT JOIN cattiposcontenedores ctc ON ctc.eCodTipoContenedor=oss.eCodTipoContenedor  ".
		" WHERE bei.eCodEIR=".(int)$_GET['eCodEIR'];
$rSolicitud=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);


// incio el PDF.

$pdf= new PDF('P','pt','Letter');
$pdf->AddPage('P','Letter');

//creo el detalle de la solicitud
	
	$anchoEtiquetaI=90;
	$anchoEtiquetaD=90;
	$anchoDatosI=200;
	$anchoDatosD=90;

    $anchodanos=50;
    $anchodanosP=30;
    $anchodanosG=70;
    $setXdanios=350; 
    $setYdanios=240;
    $setXdaniosT=400;
	$incrementodanios=20;

    $x=150;
	$y=275;
	$base=150;
	$altura=100;
	$incremento=150;
	$setxEIR=60;

    $posX=135;
    $posY=260;

    $posXi=150;
    $posYi=270;

    $posXd=250;
    $posYd=370;

    $posXt=250;
    $posYt=270;

    $posXb=200;
    $posYb=370;

    $alto=10;
    $type='Arial';
    $size=8;
    $Tsize=14;

    $pdf->SetY(150);
    $pdf->SetX(45);
    $pdf->SetFont($type,'B',$size);
    $pdf->Cell($anchoEtiquetaI,$alto,('C�digo'),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosI,$alto,utf8_decode(sprintf("%07d",$rSolicitud{'eCodEIR'})),0,0,'L');
    $pdf->SetFont($type,'B',$size);
    $pdf->Cell($anchoEtiquetaD,$alto,utf8_decode('Codigo de Entrada'),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosD,$alto,utf8_decode(sprintf("%07d",$rSolicitud{'eCodEntrada'})),0,0,'L');
    $pdf->Ln();
    $pdf->Ln();
    $pdf->SetX(45);
    $pdf->SetFont($type,'B',$size);
    $pdf->Cell($anchoEtiquetaI,$alto,utf8_decode('Fecha de Entrada'),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosI,$alto,utf8_decode(date("d/m/Y", strtotime($rSolicitud{'fhFechaEntrada'}))),0,0,'L');
    $pdf->SetFont($type,'B',$size);
    $pdf->Cell($anchoEtiquetaD,$alto,utf8_decode('Contenedor'),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosD,$alto,utf8_decode($rSolicitud{'tCodContenedor'}),0,0,'L');
    $pdf->Ln();
    $pdf->Ln();
    $pdf->SetX(45);
    $pdf->SetFont($type,'B',$size);
    $pdf->Cell($anchoEtiquetaI,$alto,utf8_decode('Tipo'),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosI,$alto,utf8_encode($rSolicitud{'TipoContenedor'}),0,0,'L');
    $pdf->SetFont($type,'B',$size);
    $pdf->Cell($anchoEtiquetaD,$alto,utf8_decode(''),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosD,$alto,utf8_encode('') ,0,0,'L');
    $pdf->Ln();
    $pdf->Ln();
    $pdf->SetX(45);
    $pdf->SetFont($type,'B',$size);
    $pdf->Cell($anchoEtiquetaI,$alto,utf8_decode('EIR'),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosI,$alto,utf8_encode(''),0,0,'L');
    $pdf->SetFont($type,'B',$size);
    $pdf->Cell($anchoEtiquetaD,$alto,utf8_decode(''),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosD,$alto,utf8_encode('') ,0,0,'L');
    $pdf->Ln();
    $pdf->Ln();

    //Genero las Imagenes con los da�os y etiquetas de vistas.

    $pdf->division();    
    $pdf->SetX($setxEIR);
    $pdf->SetFont($type,'B',$Tsize);   	
    $pdf->Cell($anchoEtiquetaI,$Tsize,('Inspecci�n'),0,0,'L');
    $pdf->SetFont($type,'',$size);
    $pdf->Cell($anchoDatosI,$alto,utf8_encode(''),0,0,'L');
    		$i=0; 
			$select=" SELECT * ".
					" FROM catvistascontenedores ".
					" WHERE ecodvista IN (1,2,3) ";
			$rsVistas = mysqli_query($link,$select);

            while($rVista=mysqli_fetch_array($rsVistas,MYSQLI_ASSOC))
            {
				$select=" SELECT rvc.*, cc.tNombre AS Cara ".
						" FROM relvistascontenedorescarascontenedores rvc ".
						" INNER JOIN catcarascontenedores cc ON cc.eCodCara = rvc.eCodCara ".
						" WHERE rvc.eCodVista=".$rVista{'eCodVista'};
                        
				$rsCaras = mysqli_query($link,$select);
				$ePix = 10;
				$tImagen = $pagina->imagenDanios(array('eir'=>$rSolicitud{'eCodEIR'},'vista'=>$rVista{'eCodVista'},'pix'=>$ePix, 'nivel'=>'../../'));
				 
				$arrImagenes[]=$tImagen['directorio'];
				if($i)
				{
					$pdf->SetX($setxEIR);
    				$pdf->SetFont($type,'B',$size);
    				$pdf->Cell($anchoEtiquetaI,$alto,'',0,0,'L');
    			}
    			$pdf->Ln();
    			$pdf->Ln();
    			$pdf->SetXY($posX,$posY);
    			$pdf->SetFont($type,'B',$size);
                $pdf->SetFillColor(254,112,35);
    			$pdf->Cell($anchoDatosI,$alto,utf8_decode($rVista{'tNombre'}),0,0,'C',true);
    			$pdf->Ln();
    			$pdf->Ln();
    			$pdf->SetXY($posXi,$posYi);
    			$pdf->SetFont($type,'',$size);
    			$pdf->Cell($anchoDatosI,$alto,utf8_decode($rVista{'tCostadoI'}),0,0,'L');
    			$pdf->Ln();
    			$pdf->Ln();
    			$pdf->SetXY($posXt,$posYt);
    			$pdf->SetFont($type,'',$size);
    			$pdf->Cell($anchoDatosI,$alto,utf8_decode($rVista{'tCostadoT'}),0,0,'L');
    			$pdf->Ln();
    			$pdf->Ln();  
    			$pdf->Image($tImagen['directorio'], $x ,$y, $base , $altura , "PNG");
    			   			
    			$pdf->Ln();
    			$pdf->Ln();
    			$pdf->SetXY($posXb,$posYb);
    			$pdf->SetFont($type,'',$size);
    			$pdf->Cell($anchoDatosI,$alto,utf8_decode($rVista{'tCostadoB'}),0,0,'L');
    			$pdf->Ln();
    			$pdf->Ln();
    			$pdf->SetXY($posXd,$posYd);
    			$pdf->SetFont($type,'',$size);
    			$pdf->Cell($anchoDatosI,$alto,utf8_decode($rVista{'tCostadoD'}),0,0,'L');
    			$i++;
                $y=$y+$incremento;
                $posY=$posY+$incremento; 
                $posYi=$posYi+$incremento;
                $posYd=$posYd+$incremento;
                $posYt=$posYt+$incremento;
                $posYb=$posYb+$incremento;
    		}
			
			foreach($arrImagenes as $rArchivo)
            {
				unlink($rArchivo);
			}
            $pdf->Ln();
            $pdf->Ln();   
            $pdf->division();

            //genero la lista de Da�os.
            $pdf->SetXY($setXdaniosT, $setYdanios);
            $setYdanios=$setYdanios+$incrementodanios;
            $pdf->SetFont($type,'B',$Tsize);    
            $pdf->Cell($anchoEtiquetaI,$Tsize,'Da�os Reportados',0,0,'C');

            $select=" SELECT re.*, cv.tNombre AS Vista, cc.tNombre AS Cara, cd.tCodDanio, cd.tNombre AS Danio ".
                    " FROM releirsdanios re ".
                    " LEFT JOIN catvistascontenedores cv ON re.eCodVista=cv.eCodVista ".
                    " LEFT JOIN catcarascontenedores cc ON re.eCodCara=cc.eCodCara ".
                    " LEFT JOIN catdanioscontenedores cd ON re.eCodDanio=cd.eCodDanio ".
                    " WHERE re.eCodEIR=".$rSolicitud{'eCodEIR'};
            $rsDanosReportados = mysqli_query($link,$select);
            /*if(mysql_num_rows($rsDanosReportados)>0)
            {*/
                while($rDanoReportado=mysqli_fetch_array($rsDanosReportados,MYSQLI_ASSOC))
                {
                    
                    $pdf->SetXY($setXdanios, $setYdanios);
                    $pdf->SetFont($type,'B',$size);
                    $pdf->Cell($anchodanos,$alto,"(".utf8_decode(trim($rDanoReportado{'Vista'})).")",0,0,'L');
                    $pdf->SetFont($type,'B',$size);
                    $pdf->Cell($anchodanosG,$alto,"(".utf8_decode(trim($rDanoReportado{'Cara'})).")",0,0,'L');
                    $pdf->SetFont($type,'B',$size);
                    $pdf->SetTextColor(128,0,0);
                    $pdf->Cell($anchodanosP,$alto,"(".utf8_decode(trim($rDanoReportado{'tCodDanio'})).")",0,0,'C');
                    $pdf->SetFont($type,'B',$size);
                    $pdf->Cell($anchodanos,$alto,$rDanoReportado{'Danio'},0,0,'L');
                    $pdf->SetTextColor(0,0,0);
                    $setYdanios=$setYdanios+$incrementodanios;

                }
            /*}else
            {
                print "Ninguno";
            } */
    

   
$pdf->AliasNbPages();
$pdf->Output('EIR '.sprintf("%07d",$rSolicitud{'eCodEIR'}).".pdf", 'I' );
?>












