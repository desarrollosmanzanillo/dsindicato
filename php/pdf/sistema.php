<?php
class sistema{
	var $ePagina;

	public function sistema($ePagina){
		global $cxsis, $bdsis;
		$this->iniciarSistema($ePagina ? $ePagina : $this->ePagina);
	}

	public function redirigirInicio(){
		if(!$_GET['ePagina']){
			unset($_SESSION['sesionUsuario']);
			session_destroy();
			$this->redireccionar("?ePagina=1.1.php");
		}
	}

	public function redireccionar($url){
		header('Location: '.$url);
	}

	public function mostrarContenido($eUsuarioSeccion){
		if((int)$eUsuarioSeccion>=2 && !isset($_SESSION['sesionUsuario'])){
			$this->redireccionar("?ePagina=1.1.php");
		}else{
			$pagina = new sistema($this->ePagina);
			if(file_exists('php/'.$this->ePagina)){
				include('php/'.$this->ePagina);
			}else{
				unset($_SESSION['sesionUsuario']);
				session_destroy();
				$this->redireccionar("?ePagina=1.1.php");
			}
		}
    }

	private function iniciarSistema($ePagina){
		$this->tSeccion = $ePagina;
		if(!$ePagina){
			$ePagina='1.1.php';
			//if($_GET['bCerrar']=='true')
				//unset($_SESSION['sesionServicios']);
		}
		/*$select=" SELECT DISTINCT TOP 1 ss.tCodSeccion, ss.tCodVinculo, ss.tParametros, ss.ePosicion, ss.tCodPadre, ss.bFiltros,".
				" CASE WHEN ss.tCodVinculo IS NOT NULL THEN sv.tTitulo ELSE ss.tTitulo END AS tTitulo, ss.tSubtitulo, ss.bOcultarTitulo, ss.bPublico,".
				" CASE WHEN su.tCodGrupo='A' THEN 1 ELSE CASE WHEN sspd.tCodPerfil IS NULL THEN NULL ELSE 1 END END AS bDelete,".
				" CASE WHEN su.tCodGrupo='A' THEN 1 ELSE CASE WHEN sspa.tCodPerfil IS NULL THEN NULL ELSE 1 END END AS bAll,".
				" CASE WHEN ssb.tCodBoton IS NULL THEN NULL ELSE 1 END AS bAcciones".
				" FROM sissecciones ss".
				" LEFT JOIN sissecciones sv ON ss.tCodVinculo = sv.tCodSeccion".
				" LEFT JOIN sisusuarios su ON su.eCodUsuario=".(int)$_SESSION['sesionServicios']['eCodUsuario']." OR ss.bPublico=1".
				" LEFT JOIN sisseccionesperfiles ssp ON ssp.tCodPerfil = su.tCodPerfil AND ssp.tCodSeccion=ss.tCodSeccion OR ss.bPublico=1 OR su.tCodGrupo='A'".
				" LEFT JOIN sisseccionesperfiles sspd ON sspd.tCodPerfil = su.tCodPerfil AND sspd.tCodSeccion=ss.tCodSeccion AND sspd.tCodPermiso='D'".
				" LEFT JOIN sisseccionesperfiles sspa ON sspa.tCodPerfil = su.tCodPerfil AND sspa.tCodSeccion=ss.tCodSeccion AND sspa.tCodPermiso='A'".
				" LEFT JOIN sisSeccionesbotones ssb ON ssb.tCodPadre=ss.tCodSeccion".
				" WHERE ss.tCodSeccion='".$ePagina."' AND ss.tCodEstatus='AC'".
				" ORDER BY ss.ePosicion ASC";
		$Seccion=mssql_fetch_array(mssql_query($select));*/

		$this->ePagina =$ePagina;
	}
	
	public function ingresos($datos){
		if(isset($_SESSION['sesionServicios'])){
			$insert = " INSERT INTO sisaccesos (eCodUsuario, fhFecha, tIP) VALUES (".$_SESSION['sesionUsuario']['eCodUsuario'].", GETDATE(), '".$_SERVER['REMOTE_ADDR']."')";
			mysql_query($insert);
		}
	}
	
	public function crearBotones(){
		$html="";
		$tSeccion=$this->quitarExtencion($this->ePagina);
		$select=" SELECT ssb.tCodSeccion, ssb.eCodBoton, sb.tFuncion AS Funcion, ".
				" sb.tNombre, ssb.ePosicion, sb.tIcono ".
				" FROM sisseccionesbotones ssb ".
				" INNER JOIN sisbotones sb ON sb.eCodBoton=ssb.eCodBoton ".
				" WHERE ssb.tCodSeccion ='".$tSeccion."' ".
				" ORDER BY ssb.ePosicion ASC ";
		$rsBotones=mysql_query($select);
		if((int)mysql_num_rows($rsBotones)>0){
		$html.="<table border=\"0\" width=\"980px\">";
		$html.="<tr><td colspan=\"2\" width=\"100%\"><hr width=\"99%\" size=\"0\" align=\"center\" color=\"#CACACA\"></td></tr><tr><td class=\"\">";
		while($rBoton=mysql_fetch_array($rsBotones)){
			$html.="<button id=\"btn".$rBoton{'eCodBoton'}."_".$rBoton{'ePosicion'}."\" onClick=\"".$rBoton{'Funcion'}."\">". //dojoType=\"dijit.form.Button\"
				   "<img width=\"16\" height=\"16\" src=\"https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/".$rBoton{'tIcono'}."\">".
				   "<span>&nbsp;&nbsp;".$rBoton{'tNombre'}."</span>".
				   "</button>";
		}
		$html.="</td><td ></td></tr>";
		$html.="<span style=\"display:none;\" id=\"spn\">&nbsp;&nbsp;<img src=\"../../gif/ic-cargador-16x16.gif\" width=\"16\" height=\"16\" align=\"absmiddle\" /></span>";
		$html.="</table>";
		}
		print $html;
	}
	
	public function tituloSeccion(){
		$tSeccion=$this->quitarExtencion($this->ePagina);
		$rTitulo=mysql_fetch_array(mysql_query(" SELECT tNombre FROM sissecciones WHERE tCodSeccion='".$tSeccion."' AND tCodPadre!=1"));
		print utf8_encode($rTitulo{'tNombre'});
	}
	
	public function quitarExtencion($tSeccion){
		return str_replace(".php", "",$tSeccion);
	}
	
	public function cargarFecha(){
		date_default_timezone_set('America/Mexico_City'); 
		$hoy=getdate();		
		switch ($hoy[wday]){case 0: $dia="Domingo";break; case 1: $dia="Lunes";break; case 2: $dia="Martes";break; case 3: $dia="Miercoles";break; case 4: $dia="Jueves";break; case 5: $dia="Viernes";break; case 6: $dia="Sabado";break;}	
		switch ($hoy[mon]){case 1: $mes="Enero";break; case 2: $mes="Febrero";break; case 3: $mes="Marzo";break; case 4: $mes="Abril";break; case 5: $mes="Mayo";break; case 6: $mes="Junio";break; case 7: $mes="Julio";break; case 8: $mes="Agosto";break; case 9: $mes="Septiembre";break; case 10: $mes="Octubre";break; case 11: $mes="Noviembre";break; case 12: $mes="Diciembre";break;} 
		if ($hoy["hours"]<10)
		$hora="0".$hoy["hours"];
		else
		$hora=$hoy["hours"];

		if ($hoy["minutes"]<10)
		$minutos="0".$hoy["minutes"];
		else
		$minutos=$hoy["minutes"];

		$fecha=$dia.", ".$hoy["mday"]." de ".$mes. " del ". $hoy["year"]. " (".$hora.":".$minutos." Horas)"; 	
		return $fecha;
	}
	
//
}
?>
	