<?php require_once("conexion/soluciones-mysql.php");  
$link = getLink();

if($_POST){
	if($_POST['eProceso']==1){
		$exito		= 0;
		$eCodCosto 	= ($_POST['eCodCosto']	? (int)$_POST['eCodCosto']							: "NULL");
		$tArea		= ($_POST['tArea']		? "'".trim(utf8_decode($_POST['tArea']))."'"		: "NULL");
		$dSalario 	= ($_POST['dSalario']	? str_replace(",", "", $_POST['dSalario'])			: "NULL");
		$tCodEstatus= ($_POST['tCodEstatus']? "'".trim(utf8_decode($_POST['tCodEstatus']))."'"	: "NULL");
		$dSalarioVIP= ($_POST['dSalarioVIP']? str_replace(",", "", $_POST['dSalarioVIP'])		: "NULL");

		if((int)$eCodCosto>0){
			$update=" UPDATE catcostoschecadoras ".
					" SET tCodEstatus=".$tCodEstatus.",".
					" tArea=".$tArea.",".
					" dSalario=".$dSalario.",".
					" dSalarioVIP=".$dSalarioVIP.
					" WHERE eCodCosto=".$eCodCosto;
			if($res=mysqli_query($link,$update)){
				$exito=1;
			}else{
				$exito=0;
			}
		}else{
			$insert=" INSERT INTO catcostoschecadoras (tCodEstatus, tArea, dSalario, dSalarioVIP) ".
					" VALUES ('AC', ".$tArea.", ".$dSalario.", ".$dSalarioVIP.")";
			if($res=mysqli_query($link,$insert)){
				$exito=1;				
				$select=" select last_insert_id() AS Llave ";
				$rCodigo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
				$eCodCosto=(int)$rCodigo['Llave'];
			}else{
				$exito=0;
			}
		}
		print "<input type=\"hidden\" value=\"".($exito==1 ? $eCodCosto : 0)."\" id=\"eCodCosto\" name=\"eCodCosto\" />";
	}
	
	if($_POST['eProceso']==2){
		$rServicio=mysqli_fetch_array(mysqli_query($link,"SELECT 1 AS Servicio FROM catcostoschecadoras WHERE tArea='".trim($_POST['tArea'])."' AND eCodCosto!=".(int)$_POST['eCodCosto']),MYSQLI_ASSOC);
		print "<input name=\"eCosto\" id=\"eCosto\" value=\"".(int)$rServicio{'Servicio'}."\">";
	}
}else{
$select=" SELECT * ".
		" FROM catcostoschecadoras ".
		" WHERE eCodCosto=".$_GET['eCodCosto'];
$rServicio = mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC); 

$select=" SELECT * 
		FROM catestatus
		WHERE tCodEstatus IN ('AC', 'EL')";
$rsEstatus=mysqli_query($link,$select); ?>
<script type="text/javascript">
dojo.require("dojo.number");
dojo.require("dijit.form.NumberTextBox");
function guardar(){
	dojo.byId('eProceso').value=1;
	bandera = false;
	mensaje = "¡Verifique lo siguiente!\n";
	tCodEstatus
	if (!dojo.byId("tCodEstatus").value){
		mensaje+="* Estatus\n";
		bandera = true;		
	}
	if (!dojo.byId("tArea").value){
		mensaje+="* Area\n";
		bandera = true;		
	}
	if (!dojo.byId("dSalario").value){
		mensaje+="* Salario\n";
		bandera = true;		
	}	
	if (!dojo.byId("dSalarioVIP").value){
		mensaje+="* Salario VIP\n";
		bandera = true;		
	}	

	if (bandera==true){
		alert(mensaje);		
	}else{
		if (confirm("¿Desea Guardar la Información?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				if(dojo.byId("eCodCosto").value>0){
					alert("¡La información se guardó correctamente!");
					tURL = './?ePagina=2.3.3.2.2.php&eCodCosto='+dojo.byId("eCodCosto").value;
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
		}
	}
}	

function verificarIndicativo(){
	if(dojo.byId('tNombre').value){
		dojo.byId('eProceso').value=2;
		dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
			dojo.byId("dvCNS").innerHTML = tRespuesta;
			if(dojo.byId('eBuque').value==1){
				alert("¡Este servicio ya existe!");
				dojo.byId('tNombre').value="";
			}
		}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
	}
}

function formato(){
	dojo.query("[id^=\"dImporte\"]").forEach(function(nodo, index, array){
		eNumero=parseFloat(nodo.value.replace(/,/ig,''));
		if(eNumero>=0){
			nodo.value=dojo.number.format(eNumero,{places:2,round:0});
		}else{
			nodo.value="";
		}
	});
}

function consultar(){
	document.location = './?ePagina=2.3.3.2.php';
}
</script>
<div id="dvCNS" style="display:none;"></div>
<form action="" method="post" name="Datos" id="Datos" onsubmit="return false;">
<input type="hidden" name="eProceso" id="eProceso" value="" />
<input type="hidden" name="eCodCosto" id="eCodCosto" value="<?=$_GET['eCodCosto'];?>" />
<table border="0" cellpadding="0" cellspacing="0">
    <tr><td height="20"></td></tr>
	<tr>
	    <td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Estatus </td>
	    <td width="50%" nowrap class="sanLR04">
			<select name="tCodEstatus" id="tCodEstatus" style="width:175px">
                <option value="">Seleccione...</option>
				<?php while($rEstatu=mysqli_fetch_array($rsEstatus,MYSQLI_ASSOC)){ ?>
                	<option value="<?=$rEstatu{'tCodEstatus'}?>" <?=($rEstatu{'tCodEstatus'}==$rServicio{'tCodEstatus'} ? "selected" : "");?>><?=$rEstatu{'tNombre'};?></option>
				<?php } ?>
            </select>
        </td>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Nombre </td>
	    <td width="50%" nowrap class="sanLR04">
           	<input name="tArea" type="text" dojoType="dijit.form.TextBox" id="tArea" value="<?=utf8_encode($rServicio{'tArea'});?>" style="width:175px">
        </td>
    </tr>
    <tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Salario </td>
	    <td width="50%" nowrap class="sanLR04">
           	<input name="dSalario" type="text" dojoType="dijit.form.TextBox" id="dSalario" value="<?=number_format($rServicio{'dSalario'},2);?>" style="width:175px">
        </td>
		<td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Salario VIP </td>
	    <td width="50%" nowrap class="sanLR04">
			<input name="dSalarioVIP" type="text" dojoType="dijit.form.TextBox" id="dSalarioVIP" value="<?=number_format($rServicio{'dSalarioVIP'},2);?>" style="width:175px" >
        </td>
    </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle" /> <font class="fntG10K">Indica que es obligatorio llenar el campo.
    </font></td>
    </tr>
</table>
</form>
<?php } ?>