<?php require_once("conexion/soluciones-mysql.php");  
$link = getLink();
if($_POST){
	if($_POST['eProceso']==1){
		$exito				= 0;
		$tPatente			= ($_POST['tPatente']				? "'".trim(utf8_decode(addslashes($_POST['tPatente'])))."'"				: "NULL");
		$eCodCliente		= ($_POST['eCodCliente']			? (int)$_POST['eCodCliente']											: "NULL");
		$eCodNaviera		= ($_POST['eCodNaviera']			? (int)$_POST['eCodNaviera']											: "NULL");
		$tCodEstatus		= ($_POST['tCodEstatus']			? "'".$_POST['tCodEstatus']."'"											: "NULL");
		$eCodUsuario		= ($_POST['eUsuario']				? (int)$_POST['eUsuario']												: "NULL");
		$eCodSolicitud		= ($_POST['eCodSolicitud']			? (int)$_POST['eCodSolicitud']											: "NULL");
		$eCodFacturarA		= ($_POST['eCodFacturarA']			? (int)$_POST['eCodFacturarA']											: "NULL");
		$fhFechaServicio	= ($_POST['fhFechaProgramacion']	? "'".$_POST['fhFechaProgramacion']."'"									: "NULL");
		$eCodTipoSolicitud	= ($_POST['eCodTipoSolicitud']		? (int)$_POST['eCodTipoSolicitud']										: "NULL");
		$tBL				= ($_POST['tBL']					? "'".trim(utf8_decode(addslashes($_POST['tBL'])))."'"					: "NULL");
		$tBuque				= ($_POST['tBuque']					? "'".trim(utf8_decode(addslashes($_POST['tBuque'])))."'"				: "NULL");
		$tNumeroViaje		= ($_POST['tNumeroViaje']			? "'".trim(utf8_decode(addslashes($_POST['tNumeroViaje'])))."'"			: "NULL");
		$tMotivoCancelacion	= ($_POST['tMotivoCancelacion']		? "'".trim(utf8_decode(addslashes($_POST['tMotivoCancelacion'])))."'"	: "NULL");
		
		mysqli_query($link,"BEGIN TRANSACTION");

		$update=" UPDATE opesolicitudesservicios ".
				" SET	tCodEstatus=".$tCodEstatus.",".
				"		fhFechaServicio=".$fhFechaServicio.",".
				"		eCodNaviera=".$eCodNaviera.",".
				"		tPatente=".$tPatente.",".
				"		eCodCliente=".$eCodCliente.",".
				"		eCodFacturarA=".$eCodFacturarA.",";
				if($_POST['tCodEstatus']=='AU'){
					$update.="	eCodUsuarioAutorizacion=".$eCodUsuario.",".
							"	fhFechaAutorizacion=CURRENT_TIMESTAMP ";
				}
				if($_POST['tCodEstatus']=='CA'){
					$update.="	eCodUsuarioCancelacion=".$eCodUsuario.",".
							"	fhFechaCancelacion=CURRENT_TIMESTAMP,".
							"	tMotivoCancelacion=".$tMotivoCancelacion;
				}
		$update.=" WHERE eCodSolicitud=".$eCodSolicitud;
		if($res=mysqli_query($link,$update)){
			$exito=1;
			$dTotal=0;
			
			$delete=" DELETE FROM relsolicitudesserviciosservicios WHERE eCodSolicitud=".$eCodSolicitud;
			mysqli_query($link,$delete);
			$delete=" DELETE FROM relsolicitudesserviciosimpuestos WHERE eCodSolicitud=".$eCodSolicitud;
			mysqli_query($link,$delete);
			foreach($_POST as $k => $valor){
				$nombre = strval($k);
				$campo = "eCodServicio";
				if(strstr($nombre,$campo) && (int)$valor>0){
					$i = str_replace("eCodServicio", "", $nombre);
					
					$eCantidad		= ($_POST['eCantidad'.$i]		? (int)$_POST['eCantidad'.$i]	: "NULL");
					$dImporte		= ($_POST['dImporte'.$i]		? (float)$_POST['dImporte'.$i]	: "NULL");
					$tObservaciones		= ($_POST['tObservaciones'.$i]		? "'".$_POST['tObservaciones'.$i]."'"	: "NULL");
					$eCodTipoContenedor	= ($_POST['eCodTipoContenedor'.$i]	? (int)$_POST['eCodTipoContenedor'.$i]	: "NULL");
					$dSubtotal			= $eCantidad*$dImporte;
					$dTotal				= $dTotal+$dSubtotal;

					$insert=" INSERT INTO relsolicitudesserviciosservicios(eCodSolicitud, eCodServicio, eCantidad, dImporte,  dSubtotal) ".
							" VALUES (".$eCodSolicitud.", ".$valor.", ".$eCantidad.", ".$dImporte.", ".$dSubtotal.")";
					if($res=mysqli_query($link,$insert)){
						print "ok";
					}else{
						$exito=0;
					}
				}
				$campo = "dImpuesto";
				if(strstr($nombre,$campo) && (float)$valor>0){
					$i = str_replace("dImpuesto", "", $nombre);
					
					print (float)$valor."*".(float)$dTotal;
					$dImpuesto			= (float)$valor*(float)$dTotal;

					$insert=" INSERT INTO relsolicitudesserviciosimpuestos(eCodSolicitud, eCodImpuesto, dValorImpuesto, dImpuesto) ".
							" VALUES (".$eCodSolicitud.", ".$i.", ".((float)$valor*100).", ".$dImpuesto.")";
					if($res=mysqli_query($link,$insert)){
						print "ok";
					}else{
						$exito=0;
					}
				}
			}
		}else{
			$exito=0;
		}

		if((int)$exito==1){
			mysqli_query($link,"COMMIT TRANSACTION");
			
			switch ($_POST['tCodEstatus']){
			case 'AU':   //SI LE DIERON CLICK EN EL BOTON AUTORIZAR ENVIAR NOTIFICACION DE CANCELACION
				//if($_POST['tCodEstatus']!='NU'){
			
				$select=" SELECT tNombre FROM catentidades WHERE eCodEntidad=".$eCodNaviera;
				$tNaviera=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
				$select=" SELECT tNombre FROM catentidades WHERE eCodEntidad=".$eCodCliente;
				$tCliente=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
				$select=" SELECT tNombre, tApellidos FROM catusuarios WHERE eCodUsuario=".$eCodUsuario;
				$tUser=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
				//$select=" SELECT tNombre FROM catbuques WHERE eCodBuque=".$eCodBuque;
				//$tBuque=mysql_fetch_array(mysql_query($select));
						
				$tMensa='<html><body>'.
				'<table border="0" cellspacing="0" cellpadding="2">'.
					'<tr>'.
						'<td colspan="2">Notificacion de Autorizacion de la Solicitud de Servicios de '.($eCodTipoSolicitud==1 ? "Entrada" : "Salida").'</td>'.
					'</tr>'.
					'<tr>'.
						'<td nowrap>C&oacute;digo</td>'.
						'<td><b>'.sprintf("%07d",$eCodSolicitud).'</b></td>'.
					'</tr>'.
					'<tr>'.
						'<td nowrap>Fecha de Solicitud</td>'.
						'<td width="100%"><b>'.date("d/m/Y", strtotime(date('Y-m-d'))).'</b></td>'.
					'</tr>'.
					'<tr>'.
						'<td nowrap>Fecha de Servicio</td>'.
						'<td width="100%"><b>'.date("d/m/Y", strtotime($fhFechaServicio)).'</b></td>'.
					'</tr>'.
					'<tr>'.
						'<td nowrap>Naviera</td>'.
						'<td><b>'.$tNaviera{'tNombre'}.'</b></td>'.
					'</tr>'.
					'<tr>'.
						'<td nowrap>Cliente</td>'.
						'<td><b>'.$tCliente{'tNombre'}.'</b></td>'.
					'</tr>'.
					'<tr>'.
						'<td nowrap>Patente</td>'.
						'<td><b>'.$tPatente.'</b></td>'.
					'</tr>'.
					'<tr>'.
						'<td nowrap>Buque - Viaje</td>'.
						'<td><b>'.$tBuque.' - '.$tNumeroViaje.'</b></td>'.
					'</tr>'.
					'<tr>'.
						'<td nowrap>Conocimiento</td>'.
						'<td><b>'.$tBL.'</b></td>'.
					'</tr>'.
					'<tr>'.
						'<td nowrap>Servicio(s):</td>'.
						'<td><b></b></td>'.
					'</tr>'.
					'<tr>'.
						'<td nowrap></td>'.
						'<td></td>'.
					'</tr>'.
					'<tr>'.
					'</tr>'.
					'<tr>'.
					'</tr>'.
					'<tr>'.
						'<td nowrap><B>SOLUCIONES DE ALMACENAMIENTO Y TRANSPORTE SA DE CV</B></td>'.
						'</tr>'.
						'<tr>'.
						'<td nowrap><b>One Solution &copy;</b></td>'.
						'</tr>'.
						'<tr>'.
						'<td nowrap><a href="mailto: soluciones@solucionesmzo.com"><span>soluciones@solucionesmzo.com</span></a></td>'.
						'</tr>'.
						'<tr>'.
						'<td nowrap><a href="www.solucionesmzo.com"><span>www.solucionesmzo.com</span></a></td>'.
						'<td></td>'.
					'</tr>'.
				'</table>'.
			'</body></html>';
			
			require("class.phpmailer.php");
			require("class.smtp.php");
			
			$mail = new PHPMailer();
			
			//$mail­>CharSet = "UTF­8";
			//$mail­>Encoding = "quoted­printable";
			
			$mail->From     = "rsp@tapterminal.com"; // Mail de origen
			$mail->FromName = "One Solution - Patio de Contenedores"; // Nombre del que envia
			$mail->AddAddress("sistemas@tapterminal.com","chuylic@gmail.com"); // Mail destino, podemos agregar muchas direcciones
			$mail->AddReplyTo("sistemas@tapterminal.com"); // Mail de respuesta

			$mail->WordWrap = 50; // Largo de las lineas
			$mail->IsHTML(true); // Podemos incluir tags html
			$mail->Subject  =  "Autorizacion de Solicitud de Servicio";
			$mail->Body     =  utf8_encode("Notificacion de Autorizacion de Solicitud")." \n<br />" . $tMensa;

			//$mail->AltBody  =  strip_tags($mail->Body); // Este es el contenido alternativo sin html

			$mail->IsSMTP(); // vamos a conectarnos a un servidor SMTP
			$mail->Host = "mail.tapterminal.com"; // direccion del servidor
			$mail->SMTPAuth = true; // usaremos autenticacion
			$mail->Username = "rsp@tapterminal.com"; // usuario
			$mail->Password = "T2012rsp80"; // contraseña

			$mail->Mailer = "smtp";
			//$mail->Host = “ssl://smtp.gmail.com”;
			$mail->Port = 26;
			$mail->SMTPAuth = true;
			$mail->Username = "rsp@tapterminal.com"; // SMTP username
			$mail->Password = "T2012rsp80"; // SMTP password
			
			if ($mail->Send())
				echo "Enviado";
			else
				echo "Error en el envio de mail";
				
				break;
				
			case 'CA':		//SI LE DIERON CLICK EN EL BOTON CANCELAR ENVIAR NOTIFICACION DE CANCELACION
			
			$select=" SELECT tNombre FROM catentidades WHERE eCodEntidad=".$eCodNaviera;
						$tNaviera=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
						$select=" SELECT tNombre FROM catentidades WHERE eCodEntidad=".$eCodCliente;
						$tCliente=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
						$select=" SELECT tNombre, tApellidos FROM catusuarios WHERE eCodUsuario=".$eCodUsuario;
						$tUser=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
						//$select=" SELECT tNombre FROM catbuques WHERE eCodBuque=".$eCodBuque;
						//$tBuque=mysql_fetch_array(mysql_query($select));
						
						$tMensa='<html><body>'.
						'<table border="0" cellspacing="0" cellpadding="2">'.
							'<tr>'.
								'<td colspan="2">Notificacion de Cancelacion de Solicitud de Servicios de '.($eCodTipoSolicitud==1 ? "Entrada" : "Salida").'</td>'.
							'</tr>'.
							'<tr>'.
								'<td nowrap>C&oacute;digo</td>'.
								'<td><b>'.sprintf("%07d",$eCodSolicitud).'</b></td>'.
							'</tr>'.
							'<tr>'.
								'<td nowrap>Fecha de Solicitud</td>'.
								'<td width="100%"><b>'.date("d/m/Y", strtotime(date('Y-m-d'))).'</b></td>'.
							'</tr>'.
							'<tr>'.
								'<td nowrap>Fecha de Servicio</td>'.
								'<td width="100%"><b>'.date("d/m/Y", strtotime($fhFechaServicio)).'</b></td>'.
							'</tr>'.
							'<tr>'.
								'<td nowrap>Naviera</td>'.
								'<td><b>'.$tNaviera{'tNombre'}.'</b></td>'.
							'</tr>'.
							'<tr>'.
								'<td nowrap>Cliente</td>'.
								'<td><b>'.$tCliente{'tNombre'}.'</b></td>'.
							'</tr>'.
							'<tr>'.
								'<td nowrap>Patente</td>'.
								'<td><b>'.$tPatente.'</b></td>'.
							'</tr>'.
							'<tr>'.
								'<td nowrap>Buque - Viaje</td>'.
								'<td><b>'.$tBuque.' - '.$tNumeroViaje.'</b></td>'.
							'</tr>'.
							'<tr>'.
								'<td nowrap>Conocimiento</td>'.
								'<td><b>'.$tBL.'</b></td>'.
							'</tr>'.
							'<tr>'.
								'<td nowrap>Servicio(s):</td>'.
								'<td><b></b></td>'.
							'</tr>'.
							'<tr>'.
								'<td nowrap></td>'.
								'<td></td>'.
							'</tr>'.
							'<tr>'.
							'</tr>'.
							'<tr>'.
							'</tr>'.
							'<tr>'.
								'<td nowrap><B>SOLUCIONES DE ALMACENAMIENTO Y TRANSPORTE SA DE CV</B></td>'.
								'</tr>'.
								'<tr>'.
								'<td nowrap><b>One Solution &copy;</b></td>'.
								'</tr>'.
								'<tr>'.
								'<td nowrap><a href="mailto: soluciones@solucionesmzo.com"><span>soluciones@solucionesmzo.com</span></a></td>'.
								'</tr>'.
								'<tr>'.
								'<td nowrap><a href="www.solucionesmzo.com"><span>www.solucionesmzo.com</span></a></td>'.
								'<td></td>'.
							'</tr>'.
						'</table>'.
					'</body></html>';
			//
			
			require("class.phpmailer.php");
			require("class.smtp.php");
			
			$mail = new PHPMailer();
			
			//$mail­>CharSet = "UTF­8";
			//$mail­>Encoding = "quoted­printable";
			
			$mail->From     = "rsp@tapterminal.com"; // Mail de origen
			$mail->FromName = "One Solution - Patio de Contenedores"; // Nombre del que envia
			$mail->AddAddress("sistemas@tapterminal.com", "chuylic@gmail.com"); // Mail destino, podemos agregar muchas direcciones
			$mail->AddReplyTo("sistemas@tapterminal.com"); // Mail de respuesta

			$mail->WordWrap = 50; // Largo de las lineas
			$mail->IsHTML(true); // Podemos incluir tags html
			$mail->Subject  =  "Cancelacion de Solicitud de Servicio";
			$mail->Body     =  utf8_encode("Notificacion de rechazo de Solicitud")." \n<br />" . $tMensa;

			//$mail->AltBody  =  strip_tags($mail->Body); // Este es el contenido alternativo sin html

			$mail->IsSMTP(); // vamos a conectarnos a un servidor SMTP
			$mail->Host = "mail.tapterminal.com"; // direccion del servidor
			$mail->SMTPAuth = true; // usaremos autenticacion
			$mail->Username = "rsp@tapterminal.com"; // usuario
			$mail->Password = "T2012rsp80"; // contraseña

			$mail->Mailer = "smtp";
			//$mail->Host = “ssl://smtp.gmail.com”;
			$mail->Port = 26;
			$mail->SMTPAuth = true;
			$mail->Username = "rsp@tapterminal.com"; // SMTP username
			$mail->Password = "T2012rsp80"; // SMTP password
			
			if ($mail->Send())
				echo "Enviado";
			else
				echo "Error en el envio de mail";

			break;
				
			}
		}else{
			mysqli_query($link,"ROLLBACK TRANSACTION");
		}
		print "<input type=\"text\" value=\"".($exito==1 ? $eCodSolicitud : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}

//	
}
if(!$_POST){

$select=" SELECT oss.eCodSolicitud, cbu.tNombre AS Buque, ces.tNombre AS Estatus, ".
		" oss.fhFecha, oss.fhFechaServicio, oss.eCodNaviera, oss.tPatente, oss.eCodCliente, ".
		" oss.eCodFacturarA, oss.tNumeroViaje, cts.tNombre AS TipoSolicitud, oss.tCodEstatus, ".
		" cte.tNombre AS TipoServicio, oss.tBL, oss.eCodTipoServicio, oss.eCodTipoSolicitud ".
		" FROM opesolicitudesservicios oss ".
		" INNER JOIN catbuques cbu ON cbu.eCodBuque=oss.eCodBuque ".
		" INNER JOIN catestatus ces ON ces.tCodEstatus=oss.tCodEstatus ".
		" INNER JOIN cattipossolicitudes cts ON cts.eCodTipoSolicitud=oss.eCodTipoSolicitud ".
		" INNER JOIN cattiposservicios cte ON cte.eCodTipoServicio=oss.eCodTipoServicio ".
		" WHERE oss.eCodSolicitud=".$_GET['eCodSolicitud'];
$rSolicitud=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

$select=" SELECT * ".
		" FROM catentidades ".
		" WHERE tCodEstatus='AC' ".
		" ORDER BY tNombre ";
$rsNavieras=mysqli_query($link,$select);

$select=" SELECT * ".
		" FROM catentidades ".
		" WHERE tCodEstatus='AC' ".
		" ORDER BY tNombre ";
$rsClientes=mysqli_query($link,$select);

$select=" SELECT * ".
		" FROM catentidades ".
		" WHERE tCodEstatus='AC' ".
		" ORDER BY tNombre ";
$rsFacturarA=mysqli_query($link,$select);
 ?>
<script type="text/javascript">
dojo.require("dojo.number");
dojo.require("dijit.form.NumberTextBox");
dojo.require("dijit.form.DateTextBox");
dojo.require("dojo.io.iframe");
function autorizar(){
	if(dojo.byId('tCodEstatusSolicitud').value=="AU" || dojo.byId('tCodEstatusSolicitud').value=="CA"){
		if(dojo.byId('tCodEstatusSolicitud').value=="AU"){
			alert('Solicitud autorizada');
		}else{
			alert('No se puede autorizar una solicitud cancelada');
		}
	}else{
		dojo.byId('tCodEstatus').value="AU";
		guardar();
	}
}

function cancelar(){
	if(dojo.byId('tCodEstatusSolicitud').value=="CA"){
		alert('No se puede cancelar una solicitud cancelada');
	}else{
		dojo.byId('tCodEstatus').value="CA";
		guardar();
	}
}

function guardar(){
	dojo.byId('eProceso').value=1;
	var bandera = false;
	var bServicios = false;
	var bMercancias = false;
	mensaje = "¡Verifique lo siguiente!\n";
	if (!dojo.byId("fhFechaProgramacion").value){
		mensaje+="* Fecha de Servicio\n";
		bandera = true;		
	}
	if (!dojo.byId("eCodNaviera").value){
		mensaje+="* Naviera\n";
		bandera = true;		
	}
	if (!dojo.byId("eCodCliente").value){
		mensaje+="* Cliente\n";
		bandera = true;		
	}
	if (!dojo.byId("eCodFacturarA").value){
		mensaje+="* Facturar a\n";
		bandera = true;		
	}
	dojo.query('input[id^=eCodServicio]:checked').forEach(function(nodo, index, arr){
		bServicios=true;
	});
	if (bServicios==false){
		mensaje+="* Servicios\n";
		bandera = true;		
	}
	if (dojo.byId('tCodEstatus').value=="CA"){
		if (!dojo.byId("tMotivoCancelacion").value){
			mensaje+="* Motivo de Cancelación\n";
			bandera = true;		
		}
	}

	if (bandera==true){
		alert(mensaje);		
	}else{
		if (confirm("¿Desea Guardar la Información?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				if(dojo.byId("eCodigo").value>0){
					alert("¡La información se guardó correctamente!");
					tURL = './?ePagina=2.5.3.1.1.php&eCodSolicitud='+dojo.byId("eCodigo").value;
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
		}
	}
}	

function calculo(){
	var eMercancia=0;
	var dTotal=0;
	var dSubtotal=0;
	var dTotalServicios=0;
	eMercancia=parseInt(dojo.byId('eFilas').value);
	dojo.query('input[id^=eCodServicio]').forEach(function(nodo, index, arr){
		if(nodo.checked==true){
			dojo.byId('eCantidad'+nodo.value).value=padCero(eMercancia,2);
			dTotal=parseFloat(dojo.byId('dImporte'+nodo.value).value)*parseFloat(eMercancia)
			dojo.byId('eTotal'+nodo.value).innerHTML=dojo.number.format(dTotal,{places:2,round:-1});
			dSubtotal+=parseFloat(dTotal);
			dTotalServicios+=parseFloat(dTotal);
		}else{
			dojo.byId('eCantidad'+nodo.value).value=padCero(0,2);
			dojo.byId('eTotal'+nodo.value).innerHTML=dojo.number.format(0,{places:2,round:-1});
		}
	});
	dojo.byId('tSubTotal').innerHTML=dojo.number.format(dSubtotal,{places:2,round:-1});
	dojo.query('input[id^=dImpuesto]').forEach(function(nodo, index, arr){
		var eFilaImpuesto=parseInt(nodo.id.replace(/dImpuesto/ig,''));
		var dImpuesto=0;
		dImpuesto=parseFloat(dSubtotal)*parseFloat(nodo.value);
		dojo.byId('tImpuesto'+eFilaImpuesto).innerHTML=dojo.number.format(dImpuesto,{places:2,round:-1});
		dTotalServicios+=parseFloat(dImpuesto);
	});
	dojo.byId('tTotal').innerHTML=dojo.number.format(dTotalServicios,{places:2,round:-1});
}

function consultar(){
	document.location = './?ePagina=2.5.3.1.php';
}
dojo.addOnLoad(function(){
	calculo();
});
</script>
<div id="dvCNS" style="display:none;"></div>
<form action="" method="post" name="Datos" id="Datos" onsubmit="return false;">
<input type="hidden" name="eProceso" id="eProceso" value="" />
<input type="hidden" name="tCodEstatus" id="tCodEstatus" value="<?=$rSolicitud{'tCodEstatus'};?>" />
<input type="hidden" name="tCodEstatusSolicitud" id="tCodEstatusSolicitud" value="<?=$rSolicitud{'tCodEstatus'};?>" />
<input type="hidden" name="eCodSolicitud" id="eCodSolicitud" value="<?=$rSolicitud{'eCodSolicitud'};?>" />
<input type="hidden" name="eCodTipoSolicitud" id="eCodTipoSolicitud" value="<?=$rSolicitud{'eCodTipoSolicitud'};?>" />
<input type="hidden" name="eUsuario" id="eUsuario" value="<?=(int)$_SESSION['sesionUsuario']['eCodUsuario'];?>" />
<table border="0" cellpadding="0" cellspacing="0" width="980px">
    <tr><td height="20"></td></tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> C&oacute;digo</td>
	    <td width="50%" nowrap class="sanLR04"><?=sprintf("%07d",$rSolicitud{'eCodSolicitud'});?></td>
		<td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Estatus</td>
	    <td width="50%" nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'Estatus'});?></td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Fecha de Solicitud</td>
	    <td width="50%" nowrap class="sanLR04"><?=date("d/m/Y H:i", strtotime($rSolicitud{'fhFecha'}));?></td>
		<td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Fecha de Servicio</td>
	    <td width="50%" nowrap class="sanLR04">
			<input name="fhFechaProgramacion"
            id="fhFechaProgramacion"
            type="text"
            dojoType="dijit.form.DateTextBox"
            required="false"
            style="width:80px;"
            hasDownArrow="false"
            displayMessage="false"
            value="<?=date("Y-m-d", strtotime($rSolicitud{'fhFechaServicio'}));?>"
            constraints="{datePattern:'dd/MM/yyyy'}"
            />
		</td>
    </tr>
	<tr><td colspan="4" align="center"><hr width="95%" size="0" align="center" color="#CACACA"></td></tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Naviera </td>
	    <td width="50%" nowrap class="sanLR04">
           	<select name="eCodNaviera" id="eCodNaviera" style="width:175px">
				<option value="">Seleccione...</option>
				<?php while($rNaviera=mysqli_fetch_array($rsNavieras,MYSQLI_ASSOC)){ ?>
					<option value="<?=$rNaviera{'eCodEntidad'}?>" <?=($rNaviera{'eCodEntidad'}==$rSolicitud{'eCodNaviera'} ? "selected='selected'" : "");?> ><?=utf8_encode($rNaviera{'tNombre'});?></option>
				<?php } ?>
			</select>
        </td>
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Patente </td>
	    <td width="50%" nowrap class="sanLR04">
			<input name="tPatente" type="text" dojoType="dijit.form.TextBox" id="tPatente" value="<?=utf8_encode($rSolicitud{'tPatente'});?>" style="width:80px" UpperCase="true">
        </td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Cliente </td>
	    <td width="50%" nowrap class="sanLR04">
           	<select name="eCodCliente" id="eCodCliente" style="width:175px">
				<option value="">Seleccione...</option>
				<?php while($rCliente=mysqli_fetch_array($rsClientes,MYSQLI_ASSOC)){ ?>
					<option value="<?=$rCliente{'eCodEntidad'}?>" <?=($rCliente{'eCodEntidad'}==$rSolicitud{'eCodCliente'} ? "selected='selected'" : "");?> ><?=utf8_encode($rCliente{'tNombre'})?></option>
				<?php } ?>
			</select>
        </td>
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Facturar a </td>
	    <td width="50%" nowrap class="sanLR04">
			<select name="eCodFacturarA" id="eCodFacturarA" style="width:175px">
				<option value="">Seleccione...</option>
				<?php while($rFacturarA=mysqli_fetch_array($rsFacturarA,MYSQLI_ASSOC)){ ?>
					<option value="<?=$rFacturarA{'eCodEntidad'}?>" <?=($rFacturarA{'eCodEntidad'}==$rSolicitud{'eCodFacturarA'} ? "selected='selected'" : "");?> ><?=utf8_encode($rFacturarA{'tNombre'})?></option>
				<?php } ?>
			</select>
        </td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Buque </td>
	    <td width="50%" nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'Buque'});?><input type="hidden" name="tBuque" id="tBuque" value="<?=utf8_encode($rSolicitud{'Buque'});?>"/></td>
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Viaje </td>
	    <td width="50%" nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'tNumeroViaje'});?><input type="hidden" name="tNumeroViaje" id="tNumeroViaje" value="<?=utf8_encode($rSolicitud{'tNumeroViaje'});?>"/></td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Tipo de Solicitud </td>
	    <td width="50%" nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'TipoSolicitud'});?></td>
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Tipo de Servicio </td>
	    <td width="50%" nowrap class="sanLR04"><?=utf8_encode($rSolicitud{'TipoServicio'});?></td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> BL </td>
	    <td nowrap class="sanLR04" colspan="3"><?=utf8_encode($rSolicitud{'tBL'});?><input type="hidden" name="tBL" id="tBL" value="<?=utf8_encode($rSolicitud{'tBL'});?>"/></td>
    </tr>
	<tr>
		<td height="23" nowrap class="sanLR04" valign="top"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Servicios</td>
		<td height="23" nowrap class="sanLR04" colspan="3">
		<?php
		$select=" SELECT cs.eCodServicio, cs.tNombre, cs.dImporte, rss.eCodSolicitud ".
			" FROM catservicios cs ".
			" INNER JOIN relserviciostiposservicios rse ON rse.eCodServicio=cs.eCodServicio ".
			" INNER JOIN relserviciostipossolicitudes rso ON rso.eCodServicio=cs.eCodServicio ".
			" LEFT JOIN relsolicitudesserviciosservicios rss ON rss.eCodServicio=cs.eCodServicio AND rss.eCodSolicitud=".(int)$rSolicitud['eCodSolicitud'].
			" WHERE rse.eCodTipoServicio=".$rSolicitud['eCodTipoServicio']." AND rso.eCodTipoSolicitud=".$rSolicitud['eCodTipoSolicitud'].
			" AND cs.tCodEstatus='AC' ".
			" ORDER BY cs.tNombre ";
		$rsServicios=mysqli_query($link,$select); 
		
		$select=" SELECT * ".
				" FROM catimpuestos ";
		$rsImpuestos=mysqli_query($link,$select); ?>
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td height="20">
					<table cellspacing="0" border="0" width="100%">
						<thead>
							<tr class="thEncabezado">
								<td nowrap="nowrap" class="sanLR04" height="20" align="center">S</td>
								<td nowrap="nowrap" class="sanLR04">Cantidad</td>
								<td nowrap="nowrap" class="sanLR04" width="100%">Descripci&oacute;n</td>
								<td nowrap="nowrap" class="sanLR04">Precio Unitario</td>
								<td nowrap="nowrap" class="sanLR04" align="right">Total</td>
							</tr>
						</thead>
						<tbody>
							<?php $i=1; while($rServicio=mysqli_fetch_array($rsServicios,MYSQLI_ASSOC)){ ?>
								<tr>
									<td nowrap="nowrap" class="sanLR04" height="20" align="center"><input type="checkbox" name="eCodServicio<?=$rServicio{'eCodServicio'};?>" id="eCodServicio<?=$rServicio{'eCodServicio'};?>" value="<?=$rServicio{'eCodServicio'};?>" <?=((int)$rServicio{'eCodSolicitud'}>0 ? "checked='checked'" : "");?> onclick="calculo();" ></td>
									<td nowrap="nowrap" class="sanLR04"><input readonly="readonly" name="eCantidad<?=$rServicio{'eCodServicio'};?>" type="text" dojoType="dijit.form.TextBox" id="eCantidad<?=$rServicio{'eCodServicio'};?>" value="<?=sprintf("%02d",0);?>" style="width:80px" class="numero"></td>
									<td nowrap="nowrap" class="sanLR04" width="100%"><?=utf8_encode($rServicio{'tNombre'});?></a></td>
									<td nowrap="nowrap" class="sanLR04" align="right">
										<?=number_format($rServicio{'dImporte'},2);?>
										<input name="dImporte<?=$rServicio{'eCodServicio'};?>" type="hidden" id="dImporte<?=$rServicio{'eCodServicio'};?>" value="<?=$rServicio{'dImporte'};?>">
									</td>
									<td nowrap="nowrap" class="sanLR04" align="right"><div id="eTotal<?=$rServicio{'eCodServicio'};?>"><?=number_format(0,2);?></div></td>
								</tr>
							<?php $i++; } ?>
							<tr>
								<td nowrap="nowrap" class="sanLR04" height="20" colspan="4" align="right"><b>Subtotal</b></td>
								<td nowrap="nowrap" class="sanLR04" align="right"><div id="tSubTotal">$0.00</div></td>
							</tr>
							<?php while($rImpuesto=mysqli_fetch_array($rsImpuestos,MYSQLI_ASSOC)){ ?>
							<tr>
								<td nowrap="nowrap" class="sanLR04" height="20" colspan="4" align="right">
								<b><?=$rImpuesto{'tNombre'}." ".number_format($rImpuesto{'eValor'},2);?>%</b>
								<input name="dImpuesto<?=$rImpuesto{'eCodImpuesto'};?>" type="hidden" id="dImpuesto<?=$rImpuesto{'eCodImpuesto'};?>" value="<?=$rImpuesto{'eValor'}/100;?>">
								</td>
								<td nowrap="nowrap" class="sanLR04" align="right"><div id="tImpuesto<?=$rImpuesto{'eCodImpuesto'};?>">$0.00</div></td>
							</tr>
							<?php } ?>
							<tr>
								<td nowrap="nowrap" class="sanLR04" height="20" colspan="4" align="right"><b>Total</b></td>
								<td nowrap="nowrap" class="sanLR04" align="right"><div id="tTotal">$0.00</div></td>
							</tr>
							<tr><td nowrap="nowrap" class="sanLR04" height="20" colspan="5"></td></tr>
						</tbody>
					</table>
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr>
		<td height="23" nowrap class="sanLR04" valign="top" colspan="4">
			<?php
			$eTipoServicio	= (int)$rSolicitud['eCodTipoServicio'];
			if((int)$rSolicitud{'eCodTipoSolicitud'}==1){
				$select=" SELECT rs.*, ctc.tNombreCorto AS TipoContenedor, ce.tNombre AS Embalaje ".
						" FROM relsolicitudesserviciosmercancias rs ".
						" LEFT JOIN cattiposcontenedores ctc ON ctc.eCodTipoContenedor=rs.eCodTipoContenedor ".
						" LEFT JOIN catembalajes ce ON ce.eCodEmbalaje=rs.eCodEmbalaje ".
						" WHERE rs.eCodSolicitud=".(int)$rSolicitud['eCodSolicitud'];
			}else{
				$select=" SELECT rs.*, ctc.tNombreCorto AS TipoContenedor, ce.tNombre AS Embalaje  ".
						" FROM opeentradasmercancias rs ".
						" INNER JOIN relsolicitudesserviciosmercanciassalidas rse ON rse.eCodEntrada=rs.eCodEntrada ".
						" LEFT JOIN cattiposcontenedores ctc ON ctc.eCodTipoContenedor=rs.eCodTipoContenedor ".
						" LEFT JOIN catembalajes ce ON ce.eCodEmbalaje=rs.eCodEmbalaje ".
						" WHERE rse.eCodSolicitud=".(int)$rSolicitud['eCodSolicitud'];
			}
			$rsMercancias=mysqli_query($link,$select); ?>
			<table cellspacing="0" border="0" width="100%" id="tablaMercancias" name="tablaMercancias">
				<thead>
					<tr class="thEncabezado">
						<td nowrap="nowrap" class="sanLR04" height="23"> <?=($eTipoServicio==1 ? "Contenedor" : "Mercanc&iacute;a");?></td>
						<td nowrap="nowrap" class="sanLR04"> <?=($eTipoServicio==1 ? "Tipo" : "Embalaje");?></td>
						<td nowrap="nowrap" class="sanLR04"> Observaciones</td>
						<td nowrap="nowrap" class="sanLR04" width="100%"></td>
					</tr>
				</thead>
				<tbody id="tbMercancias" name="tbMercancias">
					<?php $eFila=0; while($rMercancia=mysqli_fetch_array($rsMercancias,MYSQLI_ASSOC)){ ?>
					<tr id="filaMercancia1" name="filaMercancia1">
						<td nowrap="nowrap" class="sanLR04" height="23">
							<?=($eTipoServicio==1 ? $rMercancia{'tCodContenedor'} : $rMercancia{'tMercancia'});?>
						</td>
						<td nowrap="nowrap" class="sanLR04">
						<?=utf8_encode($eTipoServicio==1 ? $rMercancia{'TipoContenedor'} : $rMercancia{'Embalaje'})?>
						</td>
						<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rMercancia{'tObservaciones'});?></td>
					</tr>
					<?php $eFila++; } ?>
					<input name="eFilas" id="eFilas" value="<?=$eFila;?>" type="hidden">
				</tbody>
			</table>
		</td>
	</tr>
	<tr><td height="23" nowrap class="sanLR04" colspan="4"></td></tr>
	<tr>
	    <td height="23" nowrap class="sanLR04" valign="top"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" width="16" align="absmiddle"> Motivo de Cancelaci&oacute;n</td>
	    <td nowrap class="sanLR04" colspan="3"><textarea name="tMotivoCancelacion" id="tMotivoCancelacion" rows="4" cols="96"></textarea></td>
    </tr>
	<?php
	$select=" SELECT rs.*, ct.tNombre AS Archivo ".
			" FROM relsolicitudesserviciosmercanciasarchivos rs ".
			" LEFT JOIN cattiposarchivos ct ON ct.eCodTipoArchivo=rs.eCodTipoArchivo ".
			" WHERE rs.eCodSolicitud=".(int)$rSolicitud['eCodSolicitud'];
	$rsArchivos=mysqli_query($link,$select);
	if((int)mysqli_num_rows($rsArchivos)>0){ ?>
		<tr><td height="23"></td></tr>
		<?php $eArchivo=1;
		while($rArchivo=mysqli_fetch_array($rsArchivos,MYSQLI_ASSOC)){ ?>
			<tr>
				<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> <?=($eArchivo==1 ?  "Archivos" : "");?></td>
				<td nowrap class="sanLR04" colspan="3"><a href="Patio/<?=$rArchivo{'tURL'};?>" target="_blank" class="sanT12" ><?=$rArchivo{'Archivo'};?></a></td>
			</tr>
			<?php $eArchivo++;
		}
	} ?>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle" /> <font class="fntG10K">Indica que es obligatorio llenar el campo.
    </font></td>
    </tr>
</table>
</form>
<?php } ?>