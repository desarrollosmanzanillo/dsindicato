<?php require_once("conexion/soluciones-mysql.php");  
$link = getLink();
if($_POST){
	if($_POST['eProceso']==1){
		$exito			= 0;
		$tNombre		= ($_POST['tNombre']		? "'".trim(utf8_decode($_POST['tNombre']))."'"		: "NULL");
		$tApellido		= ($_POST['tApellido']		? "'".trim(utf8_decode($_POST['tApellido']))."'"	: "NULL");
		$eCodEntidad	= ($_POST['eCodEntidad']	? (int)$_POST['eCodEntidad']						: "NULL");
		$tUsuario		= ($_POST['tUsuario']		? "'".trim(utf8_decode($_POST['tUsuario']))."'"		: "NULL");
		$eCodUsuario	= ($_POST['eCodUsuario']	? (int)$_POST['eCodUsuario']						: "NULL");
		$tContrasena	= ($_POST['tContrasena']	? "'".trim(utf8_decode($_POST['tContrasena']))."'"	: "NULL");
		
		if((int)$eCodUsuario>0){
			$update=" UPDATE catusuarios ".
					" SET tNombre=".$tNombre.",".
					" tApellidos=".$tApellido.",".
					" eCodEntidad=".$eCodEntidad.",".
					" tUsuario=".$tUsuario.",".
					" tContrasena=".$tContrasena.
					" WHERE eCodUsuario=".$eCodUsuario;
			if($res=mysqli_query($link,$update)){
				$exito=1;
			}else{
				$exito=0;
			}
		}else{
			$insert=" INSERT INTO catusuarios(tCodEstatus, tNombre, tApellidos, tUsuario, tContrasena, eCodEntidad ) ".
					" VALUES ('AC', ".$tNombre.", ".$tApellido.", ".$tUsuario.", ".$tContrasena.",".$eCodEntidad.")";
			if($res=mysqli_query($link,$insert)){
				$exito=1;
				$select=" select last_insert_id() AS Llave ";
				$rCodigo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
				$eCodUsuario=(int)$rCodigo['Llave'];
			}else{
				$exito=0;
			}
		}
		print "<input type=\"text\" value=\"".($exito==1 ? (int)$eCodUsuario : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}
	
	if($_POST['eProceso']==2){
		$rUsuario=mysqli_fetch_array(mysqli_query($link,"SELECT 1 AS Usuario FROM catusuarios WHERE tUsuario='".trim($_POST['tUsuario'])."' AND eCodUsuario!=".(int)$_POST['eCodUsuario']),MYSQLI_ASSOC);
		print "<input name=\"eUsuario\" id=\"eUsuario\" value=\"".(int)$rUsuario{'Usuario'}."\">";
	}
	
	if($_POST['eProceso']==3){
		$eIdenticas=($_POST['tContrasena']==$_POST['tContrasenaValida'] ? 1 : 0);
		print "<input name=\"eIdenticas\" id=\"eIdenticas\" value=\"".(int)$eIdenticas."\">";
	}
}
if(!$_POST){
	
$select=" SELECT * ".
		" FROM catusuarios ".
		" WHERE eCodUsuario=".$_GET['eCodUsuario'];
$rUsuario = mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC); ?>
<script type="text/javascript">
function guardar(){
	dojo.byId('eProceso').value=1;
	bandera = false;
	mensaje = "¡Verifique lo siguiente!\n";
	if (!dojo.byId("tNombre").value){
		mensaje+="* Nombre\n";
		bandera = true;		
	}
	if (!dojo.byId("tApellido").value){
		mensaje+="* Apellido\n";
		bandera = true;		
	}
	if (!dojo.byId("tUsuario").value){
		mensaje+="* Correo\n";
		bandera = true;		
	}
	if (!dojo.byId("tContrasena").value){
		mensaje+="* Contraseña\n";
		bandera = true;		
	}
	if (!dojo.byId("eCodEntidad").value){
		mensaje+="* Entidad\n";
		bandera = true;		
	}
	if (!dojo.byId("tContrasenaValida").value){
		mensaje+="* Contraseña de Verificación\n";
		bandera = true;		
	}

	if (bandera==true){
		alert(mensaje);		
	}else{
		if (confirm("¿Desea Guardar la Información?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				if(dojo.byId("eCodigo").value>0){
					alert("¡La información se guardó correctamente!");
					tURL = './?ePagina=2.2.1.1.2.php&eCodUsuario='+dojo.byId("eCodigo").value;
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
		}
	}
}	

function verificarCorreo(){
	if(dojo.byId('tUsuario').value){
		dojo.byId('eProceso').value=2;
		dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
			dojo.byId("dvCNS").innerHTML = tRespuesta;
			if(dojo.byId('eUsuario').value==1){
				alert("¡Este usuario ya existe!");
				dojo.byId('tUsuario').value="";
			}
		}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
	}
}

function validarContrasenia(){
	if(dojo.byId('tContrasena').value.length>=8){
		if(dojo.byId('tContrasenaValida').value){
			dojo.byId('eProceso').value=3;
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				if(dojo.byId('eIdenticas').value==0){
					alert("¡La contraseña de verificación no es igual!");
					dojo.byId('tContrasenaValida').value="";
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});
		}else{
			dojo.byId('tContrasenaValida').value=""
		}
	}else{
		alert('¡La contraseña es menor a 8 caracteres!');
		dojo.byId('tContrasenaValida').value=""
	}
}

function consultar(){
	document.location = './?ePagina=2.2.1.1.php';
}
</script>

<div id="dvCNS" style="display:none;"></div>
<form action="" method="post" name="Datos" id="Datos" onsubmit="return false;">
  <input type="hidden" name="eProceso" id="eProceso" value="" />
  <input type="hidden" name="eCodUsuario" id="eCodUsuario" value="<?=$_GET['eCodUsuario'];?>" />
  <table border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td height="20"></td>
    </tr>
    <tr>
      <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Nombre </td>
      <td width="50%" nowrap class="sanLR04"><input name="tNombre" type="text" dojoType="dijit.form.TextBox" id="tNombre" value="<?=utf8_encode($rUsuario{'tNombre'});?>" style="width:175px">
      </td>
      <td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Apellido </td>
      <td width="50%" nowrap class="sanLR04"><input name="tApellido" type="text" dojoType="dijit.form.TextBox" id="tApellido" value="<?=utf8_encode($rUsuario{'tApellidos'});?>" style="width:175px;">
      </td>
    </tr>
    <tr>
      <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Correo </td>
      <td nowrap class="sanLR04"><input name="tUsuario" type="text" dojoType="dijit.form.TextBox" id="tUsuario" value="<?=$rUsuario{'tUsuario'}?>" style="width:175px" onblur="verificarCorreo();">
      </td>
      <td class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Entidad </td>
      <td class="sanLR04">
      	
        <select name='eCodEntidad' id='eCodEntidad' style="width:175px">
          <option value=''></option>
          <?php 
		  
		  $sel = mysqli_query($link,"SELECT eCodEntidad, tSiglas FROM catentidades where tCodEstatus='AC' and eCodTipoEntidad=1 ");                
                              while($row = mysqli_fetch_array($sel,MYSQLI_ASSOC)){ ?>
          <option value='<?php echo $row["eCodEntidad"]; ?>'<?php if($row["eCodEntidad"]==$rUsuario{'eCodEntidad'}) { print "selected=\"selected\"";}?>><?php echo $row["tSiglas"]; ?> </option>
          <?php } ?>
        </select>        
        
        </td>
    </tr>
    <tr>
      <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Contraseña </td>
      <td width="50%" nowrap class="sanLR04"><input name="tContrasena" type="password" dojoType="dijit.form.TextBox" id="tContrasena" value="<?=$rUsuario{'tContrasena'}?>" style="width:175px" onblur="validarContrasenia();">
      </td>
      <td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Contraseña de Verificaci&oacute;n </td>
      <td width="50%" nowrap class="sanLR04"><input name="tContrasenaValida" type="password" dojoType="dijit.form.TextBox" id="tContrasenaValida" value="<?=$rUsuario{'tContrasena'}?>" style="width:175px;" onblur="validarContrasenia();">
      </td>
    </tr>
    <tr>
      <td height="23" nowrap class="sanLR04"></td>
      <td nowrap class="sanLR04" colspan="3"><label class="txtGK10">M&iacute;nimo 8 caracteres</label></td>
    </tr>
  </table>
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td valign="top" nowrap="nowrap" class="sanLR04">&nbsp;</td>
    </tr>
    <tr>
      <td valign="top" nowrap="nowrap" class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle" /> <font class="fntG10K">Indica que es obligatorio llenar el campo. </font></td>
    </tr>
  </table>
</form>
<?php } ?>
