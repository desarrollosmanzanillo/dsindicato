<?php require_once("conexion/soluciones-mysql.php");  
$link = getLink();
	

		$select=" SELECT ope.*, bon.tNombre as Bono, usr.tNombre as usuario, usr.tApellidos as Apellido, bon.dImporte ".
			" FROM opemaniobrabono ope ".
			" INNER JOIN cattipobono AS bon ON bon.eCodBono=ope.eCodBono	".			
			" INNER JOIN catusuarios AS usr ON usr.eCodUsuario=ope.eCodUsuario	".	
			" WHERE ope.eCodManiobra=".$_GET['eCodManiobra'];
		//print($select);

$rAsistencia = mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
		
		$select="SELECT rel.*, emp.Empleado, asi.dBono ".
		" FROM relmaniobrasbonosasistencias AS rel ".
		" INNER JOIN asistencias AS asi ON asi.id=rel.eCodAsistencia	".	
		" INNER JOIN empleados AS emp ON emp.id=asi.idEmpleado ".
		" INNER JOIN opemaniobrabono AS ope ON ope.eCodManiobra=rel.eCodManiobra ".
		" WHERE rel.eCodManiobra=".$_GET['eCodManiobra'];
		//print($select);
		$rsEmpleadosBono = mysqli_query($link,$select);
		$registros=(int)mysqli_num_rows($rsEmpleadosBono);

 ?>
<script type="text/javascript">
function consultar(){
	document.location = './?ePagina=2.5.1.5.php';
}
function nuevo(){
	document.location = './?ePagina=2.4.1.4.php';
}
</script>
<div id="dvCNS" style="display:none;"></div>
<form action="" method="post" name="Datos" id="Datos" onsubmit="return false;">
	<div style="display:block; top:0; left:0; width:965px; z-index=1; overflow: auto; height:400px;">
<table border="0" cellpadding="0" cellspacing="0">
    <tr><td height="20"></td></tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"> Folio </td>
	    <td width="50%" nowrap class="sanLR04"><b><?=sprintf("%07d",$rAsistencia{'eCodManiobra'});?></b></td>
		<td nowrap class="sanLR04"> Status </td>
	    <td width="50%" nowrap class="sanLR04"><b><?=utf8_encode( $rAsistencia{'tCodEstatus'}=='AC' ? " Activo "	: " Cancelado ");?></b></td>
	</tr>
	<tr>
		<td nowrap class="sanLR04">F. Maniobra </td>	    
	    <td nowrap class="sanLR04"><b><?=date("d/m/Y", strtotime($rAsistencia{'fhFecha'}));?></b></td>
		<td height="23" nowrap class="sanLR04"> Turno </td>
	    <td nowrap class="sanLR04"><b><?=utf8_encode($rAsistencia{'dTurno'});?></b></td>
	</tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"> Maniobra</td>
	    <td nowrap class="sanLR04"><b><?=utf8_encode($rAsistencia{'Bono'});?></b></td>
	    <td height="23" nowrap class="sanLR04"> Tarifa Bono</td>
	    <td nowrap class="sanLR04"><b><?=utf8_encode($rAsistencia{'dImporte'});?></b></td>
    </tr>
    <tr>		
		<td height="23" nowrap class="sanLR04"> Toneladas </td>
	    <td nowrap class="sanLR04"><b><?=utf8_encode($rAsistencia{'dPeso'});?></b></td>
		<td height="23" nowrap class="sanLR04"> Bono Total </td>
	    <td nowrap class="sanLR04"><b><?=number_format(($rAsistencia{'dPeso'}*$rAsistencia{'dImporte'}),2);?></b></td>
    </tr>    
    <tr>		
		<td height="23" nowrap class="sanLR04"> U. Registro </td>
		<td nowrap class="sanLR04"><b><?=utf8_encode( $rAsistencia{'eCodUsuario'}>0 ? $rAsistencia{'usuario'}." ".$rAsistencia{'Apellido'}	: " N/D ");?></b></td>	    
		<td height="23" nowrap class="sanLR04"> F. Registro </td>
		<td nowrap class="sanLR04"><b><?=utf8_encode( $rAsistencia{'fhFechaRegistro'}>'1969-01-01' ? date("d/m/Y H:i:s", strtotime($rAsistencia{'fhFechaRegistro'}))	: " N/D ");?></b></td>	    
	    
    </tr>
    </table>
     <table cellspacing="0" border="0" width="965px">
		<tr>
			<td width="50%"><hr color="#666666" /></td>
			<td class="sanLR04" nowrap="nowrap"><b>Empleados que Aplicaron al Bono: ( <?=$registros;?> )</b></td>
			<td width="50%"><hr color="#666666" /></td>
		</tr>
    </table>
     <table cellspacing="0" border="0" width="965px">
		<thead>
			<tr class="thEncabezado">
				
				<td nowrap="nowrap" class="sanLR04">C. Asistencia</td>				
				<td nowrap="nowrap" class="sanLR04">Empleado</td>				
				<td nowrap="nowrap" class="sanLR04" width="100%">Bono</td>
			</tr>
		</thead>
		<tbody>
    <?
     while($rEmpleadosBono=mysqli_fetch_array($rsEmpleadosBono,MYSQLI_ASSOC))
     {    
    ?>
    			<tr>
    				<td nowrap="nowrap" class="sanLR04 colmenu"><b><a href="?ePagina=2.5.1.1.2.php&eCodAsistencia=<?=$rEmpleadosBono{'eCodAsistencia'};?>"><?=sprintf("%07d",$rEmpleadosBono{'eCodAsistencia'});?></a></b></td>										
                    <td nowrap="nowrap" class="sanLR04 columnB" ><?=utf8_encode($rEmpleadosBono{'Empleado'});?></td>
                    <td nowrap="nowrap" class="sanLR04 " ><?=number_format($rEmpleadosBono{'dBono'},2);?></td>					
				</tr>
	<?}
	?>
</tbody>
	 </table>
</div>
    



</form>