<?php require_once("conexion/soluciones-mysql.php");  
$link = getLink();
if($_POST){
	if($_POST['eProceso']==1){
		$exito				= 0;
		$tNombre			= ($_POST['tNombre']			? "'".trim(utf8_decode($_POST['tNombre']))."'"			: "NULL");
		$eCodPais			= ($_POST['eCodPais']			? (int)$_POST['eCodPais']								: "NULL");
		$tColonia			= ($_POST['tColonia']			? "'".trim(utf8_decode($_POST['tColonia']))."'"			: "NULL");
		$tTelefono			= ($_POST['tTelefono']			? "'".trim(utf8_decode($_POST['tTelefono']))."'"		: "NULL");
		$eCodEstado			= ($_POST['eCodEstado']			? (int)$_POST['eCodEstado']								: "NULL");
		$eCodCiudad			= ($_POST['eCodCiudad']			? (int)$_POST['eCodCiudad']								: "NULL");
		$tDireccion			= ($_POST['tDireccion']			? "'".trim(utf8_decode($_POST['tDireccion']))."'"		: "NULL");
		$tReferencia		= ($_POST['tReferencia']		? "'".trim(utf8_decode($_POST['tReferencia']))."'"		: "NULL");
		$eCodEntidad		= ($_POST['eCodEntidad']		? (int)$_POST['eCodEntidad']							: "NULL");
		$eCodSucursal		= ($_POST['eCodSucursal']		? (int)$_POST['eCodSucursal']							: "NULL");
		$tCodigoPostal		= ($_POST['tCodigoPostal']		? "'".trim(utf8_decode($_POST['tCodigoPostal']))."'"	: "NULL");
		$tNumeroExterior	= ($_POST['tNumeroExterior']	? "'".trim(utf8_decode($_POST['tNumeroExterior']))."'"	: "NULL");
		$tNumeroInterior	= ($_POST['tNumeroInterior']	? "'".trim(utf8_decode($_POST['tNumeroInterior']))."'"	: "NULL");
		$eCodTipoEntidad	= ($_POST['eCodTipoEntidad']	? (int)$_POST['eCodTipoEntidad']						: "NULL");
		
		if((int)$eCodSucursal>0){
			$update=" UPDATE catentidades ".
					" SET tNombre=".$tNombre.",".
					" eCodPais=".$eCodPais.",".
					" eCodEstado=".$eCodEstado.",".
					" eCodCiudad=".$eCodCiudad.",".
					" tColonia=".$tColonia.",".
					" tDireccion=".$tDireccion.",".
					" tCodigoPostal=".$tCodigoPostal.",".
					" tNumeroExterior=".$tNumeroExterior.",".
					" tNumeroInterior=".$tNumeroInterior.",".
					" tTelefono=".$tTelefono.",".
					" tReferencia=".$tReferencia.
					" WHERE eCodSucursal=".$eCodSucursal;
			if($res=mysqli_query($link,$update)){
				$exito=1;
			}else{
				$exito=0;
			}
		}else{
			$insert=" INSERT INTO catentidades(tCodEstatus, eCodEntidad, tNombre, eCodPais, eCodEstado, eCodCiudad, tColonia, ".
					" tDireccion, tCodigoPostal, tNumeroExterior, tNumeroInterior, tTelefono, tReferencia) ".
					" VALUES ('AC', ".$eCodEntidad.", ".$tNombre.", ".$eCodPais.", ".$eCodEstado.", ".$eCodCiudad.", ".$tColonia.
					", ".$tDireccion.", ".$tCodigoPostal.", ".$tNumeroExterior.", ".$tNumeroInterior.", ".$tTelefono.", ".$tReferencia.")";
			print $insert;
			if($res=mysqli_query($link,$insert)){
				$exito=1;
				$select=" select last_insert_id() AS Llave ";
				$rCodigo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
				$eCodSucursal=(int)$rCodigo['Llave'];
			}else{
				$exito=0;
			}
		}
		print "<input type=\"text\" value=\"".($exito==1 ? (int)$eCodSucursal : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}

	if($_POST['eProceso']==3){
		$select=" SELECT * ".
				" FROM catestados ".
				" WHERE tCodEstatus='AC' ".
				" AND eCodPais=".$_POST['eCodPais'];
		$rsEstados=mysqli_query($link,$select); ?>
		<select name="eCodEstado" id="eCodEstado" style="width:175px" onchange="cargarCiudad();">
			<option value="">Seleccione...</option>
			<?php while($rEstado=mysqli_fetch_array($rsEstados,MYSQLI_ASSOC)){ ?>
				<option value="<?=$rEstado{'eCodEstado'}?>"><?=utf8_encode($rEstado{'tNombre'})?></option>
			<?php } ?>
		</select>
	<?php }
	
	if($_POST['eProceso']==4){
		$select=" SELECT * ".
				" FROM catciudades ".
				" WHERE tCodEstatus='AC' ".
				" AND eCodEstado=".$_POST['eCodEstado'];
		$rsCiudades=mysqli_query($link,$select); ?>
		<select name="eCodCiudad" id="eCodCiudad" style="width:175px">
			<option value="">Seleccione...</option>
			<?php while($rCiudad=mysqli_fetch_array($rsCiudades,MYSQLI_ASSOC)){ ?>
				<option value="<?=$rCiudad{'eCodCiudad'}?>"><?=utf8_encode($rCiudad{'tNombre'})?></option>
			<?php } ?>
		</select>
	<?php }

}
if(!$_POST){
	
$select=" SELECT * ".
		" FROM catsucursalesentidades ".
		" WHERE eCodSucursal=".$_GET['eCodSucursal'];
$rSucursal = mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC); 

$select=" SELECT * ".
		" FROM catentidades ".
		" WHERE tCodEstatus='AC' ".
		" ORDER BY tNombre ASC ";
$rsEntidades = mysqli_query($link,$select);

$select=" SELECT * ".
		" FROM catpaises ".
		" WHERE tCodEstatus='AC' ".
		" ORDER BY tNombre ASC ";
$rsPaises = mysqli_query($link,$select); 

$select=" SELECT * ".
		" FROM catestados ".
		" WHERE tCodEstatus='AC' ".
		" AND eCodPais=".$rSucursal{'eCodPais'};
$rsEstados=mysqli_query($link,$select);

$select=" SELECT * ".
		" FROM catciudades ".
		" WHERE tCodEstatus='AC' ".
		" AND eCodEstado=".$rSucursal{'eCodEstado'};
$rsCiudades=mysqli_query($link,$select);
?>
<script type="text/javascript">
function guardar(){
	dojo.byId('eProceso').value=1;
	bandera = false;
	mensaje = "¡Verifique lo siguiente!\n";
	if (!dojo.byId("eCodEntidad").value){
		mensaje+="* Entidad\n";
		bandera = true;		
	}
	if (!dojo.byId("tNombre").value){
		mensaje+="* Nombre\n";
		bandera = true;		
	}
	if (!dojo.byId("eCodPais").value){
		mensaje+="* País\n";
		bandera = true;		
	}
	if (!dojo.byId("eCodEstado").value){
		mensaje+="* Estado\n";
		bandera = true;		
	}
	if (!dojo.byId("eCodCiudad").value){
		mensaje+="* Ciudad\n";
		bandera = true;		
	}
	if (!dojo.byId("tDireccion").value){
		mensaje+="* Calle\n";
		bandera = true;		
	}

	if (bandera==true){
		alert(mensaje);		
	}else{
		if (confirm("¿Desea Guardar la Información?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				if(dojo.byId("eCodigo").value>0){
					alert("¡La información se guardó correctamente!");
					tURL = './?ePagina=2.3.2.2.2.php&eCodSucursal='+dojo.byId("eCodigo").value;
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
		}
	}
}

function cargarEstados(){
	dojo.byId('eProceso').value=3;
	dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
		dojo.byId("tdEstado").innerHTML = tRespuesta;
		cargarCiudad();
	}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});
}

function cargarCiudad(){
	dojo.byId('eProceso').value=4;
	dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
		dojo.byId("tdCiudad").innerHTML = tRespuesta;
	}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});
}

function consultar(){
	document.location = './?ePagina=2.3.2.2.php';
}
</script>
<div id="dvCNS" style="display:none;"></div>
<form action="" method="post" name="Datos" id="Datos" onsubmit="return false;">
<input type="hidden" name="eProceso" id="eProceso" value="" />
<input type="hidden" name="eCodSucursal" id="eCodSucursal" value="<?=$_GET['eCodSucursal'];?>" />
<table border="0" cellpadding="0" cellspacing="0">
    <tr><td height="20"></td></tr>
	<tr>
	    <td class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Entidad </td>
	    <td width="50%" class="sanLR04">
			<select name="eCodEntidad" id="eCodEntidad" style="width:175px">
				<option value="">Seleccione...</option>
				<?php while($rEntidad=mysqli_fetch_array($rsEntidades,MYSQLI_ASSOC)){ ?>
					<option value="<?=$rEntidad{'eCodEntidad'}?>" <?=($rEntidad{'eCodEntidad'}==$rSucursal{'eCodEntidad'} ? "selected='selected'" : "");?> ><?=utf8_encode($rEntidad{'tNombre'})?></option>
				<?php } ?>
			</select>
		</td>
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Nombre </td>
	    <td width="50%" nowrap class="sanLR04">
           	<input name="tNombre" type="text" dojoType="dijit.form.TextBox" id="tNombre" value="<?=utf8_encode($rSucursal{'tNombre'});?>" style="width:175px">
        </td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Pa&iacute;s </td>
	    <td width="50%" nowrap class="sanLR04">
			<select name="eCodPais" id="eCodPais" style="width:175px" onchange="cargarEstados();">
				<option value="">Seleccione...</option>
				<?php while($rPais=mysqli_fetch_array($rsPaises,MYSQLI_ASSOC)){ ?>
					<option value="<?=$rPais{'eCodPais'}?>" <?=($rPais{'eCodPais'}==$rSucursal{'eCodPais'} ? "selected='selected'" : "");?>><?=utf8_encode($rPais{'tNombre'})?></option>
				<?php } ?>
			</select>
        </td>
		<td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" width="16" height="16" align="absmiddle"> Estado </td>
	    <td width="50%" nowrap class="sanLR04" id="tdEstado">
           <select name="eCodEstado" id="eCodEstado" style="width:175px" onchange="cargarCiudad();">
			<option value="">Seleccione...</option>
			<?php while($rEstado=mysqli_fetch_array($rsEstados,MYSQLI_ASSOC)){ ?>
				<option value="<?=$rEstado{'eCodEstado'}?>" <?=($rEstado{'eCodEstado'}==$rSucursal{'eCodEstado'} ? "selected='selected'" : "");?>><?=utf8_encode($rEstado{'tNombre'})?></option>
			<?php } ?>
		</select>
        </td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Ciudad </td>
	    <td width="50%" nowrap class="sanLR04" id="tdCiudad">
           	<select name="eCodCiudad" id="eCodCiudad" style="width:175px">
				<option value="">Seleccione...</option>
				<?php while($rCiudad=mysqli_fetch_array($rsCiudades,MYSQLI_ASSOC)){ ?>
					<option value="<?=$rCiudad{'eCodCiudad'}?>" <?=($rCiudad{'eCodCiudad'}==$rSucursal{'eCodCiudad'} ? "selected='selected'" : "");?>><?=utf8_encode($rCiudad{'tNombre'})?></option>
				<?php } ?>
			</select>
        </td>
		<td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Colonia </td>
	    <td width="50%" nowrap class="sanLR04">
           	<input name="tColonia" type="text" dojoType="dijit.form.TextBox" id="tColonia" value="<?=$rSucursal{'tColonia'}?>" style="width:175px">
        </td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Calle </td>
	    <td width="50%" nowrap class="sanLR04">
           	<input name="tDireccion" type="text" dojoType="dijit.form.TextBox" id="tDireccion" value="<?=$rSucursal{'tDireccion'}?>" style="width:175px">
        </td>
		<td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> C.P. </td>
	    <td width="50%" nowrap class="sanLR04">
           	<input name="tCodigoPostal" type="text" dojoType="dijit.form.TextBox" id="tCodigoPostal" value="<?=$rSucursal{'tCodigoPostal'}?>" style="width:80px">
        </td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> N&uacute;mero Exterior </td>
	    <td width="50%" nowrap class="sanLR04">
           	<input name="tNumeroExterior" type="text" dojoType="dijit.form.TextBox" id="tNumeroExterior" value="<?=$rSucursal{'tNumeroExterior'}?>" style="width:175px">
        </td>
		<td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> N&uacute;mero Interior </td>
	    <td width="50%" nowrap class="sanLR04">
           	<input name="tNumeroInterior" type="text" dojoType="dijit.form.TextBox" id="tNumeroInterior" value="<?=$rSucursal{'tNumeroInterior'}?>" style="width:175px">
        </td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Telefono </td>
	    <td nowrap class="sanLR04" colspan="3">
			<input name="tTelefono" type="text" dojoType="dijit.form.TextBox" id="tTelefono" value="<?=$rSucursal{'tTelefono'}?>" style="width:175px">
        </td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04" valign="top"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" width="16" height="0" align="absmiddle"> Referencia </td>
	    <td nowrap class="sanLR04" colspan="3">
			<textarea name="tReferencia" id="tReferencia" cols="75"><?=$rSucursal{'tReferencia'}?></textarea>
        </td>
    </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle" /> <font class="fntG10K">Indica que es obligatorio llenar el campo.
    </font></td>
    </tr>
</table>
</form>
<?php } ?>