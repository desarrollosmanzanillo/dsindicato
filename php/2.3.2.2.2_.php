<?php require_once("conexion/soluciones-mysql.php");  
$link = getLink();
	
$select=" SELECT cse.*, ce.tNombre AS Entidad, cte.tNombre AS TipoEntidad, cp.tNombre AS Pais, ".
		" ces.tNombre AS Estado, cci.tNombre AS Ciudad ".
		" FROM catsucursalesentidades cse ".
		" INNER JOIN catentidades ce ON ce.eCodEntidad=cse.eCodEntidad ".
		" INNER JOIN cattiposentidades cte ON cte.eCodTipoEntidad=ce.eCodTipoEntidad ".
		" INNER JOIN catpaises cp ON cp.eCodPais=ce.eCodPais ".
		" LEFT JOIN catestados ces ON ces.eCodEstado=ce.eCodEstado ".
		" LEFT JOIN catciudades cci ON cci.eCodCiudad=ce.eCodCiudad ".
		" WHERE cse.eCodSucursal=".$_GET['eCodSucursal'];
$rEstado = mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC); ?>
<script type="text/javascript">
function consultar(){
	document.location = './?ePagina=2.3.2.2.php';
}
function nuevo(){
	document.location = './?ePagina=2.3.2.2.1.php';
}
</script>
<div id="dvCNS" style="display:none;"></div>
<form action="" method="post" name="Datos" id="Datos" onsubmit="return false;">
<table border="0" cellpadding="0" cellspacing="0">
    <tr><td height="20"></td></tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"> C&oacute;digo</td>
	    <td width="50%" nowrap class="sanLR04"><?=sprintf("%07d",$rEstado{'eCodCiudad'});?></td>
		<td nowrap class="sanLR04">  </td>
	    <td width="50%" nowrap class="sanLR04"></td>
    </tr>
    <tr>
		<td height="23" nowrap class="sanLR04"> Entidad</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rEstado{'Entidad'});?></td>
		<td height="23" nowrap class="sanLR04"> Nombre </td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rEstado{'tNombre'});?></td>
    </tr>
	<tr>
		<td height="23" nowrap class="sanLR04"> Pa&iacute;s </td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rEstado{'Pais'});?></td>
	    <td height="23" nowrap class="sanLR04"> Estado</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rEstado{'Estado'});?></td>
	</tr>
	<tr>
		<td height="23" nowrap class="sanLR04"> Ciudad</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rEstado{'Ciudad'});?></td>
	    <td height="23" nowrap class="sanLR04"> Colonia</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rEstado{'tColonia'});?></td>
	</tr>
	<tr>
		<td height="23" nowrap class="sanLR04"> Calle</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rEstado{'tDireccion'});?></td>
	    <td height="23" nowrap class="sanLR04"> C.P.</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rEstado{'tCodigoPostal'});?></td>
	</tr>
	<tr>
		<td height="23" nowrap class="sanLR04"> N&uacute;mero Exterior</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rEstado{'tNumeroExterior'});?></td>
	    <td height="23" nowrap class="sanLR04"> N&uacute;mero Interior</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rEstado{'tNumeroInterior'});?></td>
	</tr>
	<tr>
		<td height="23" nowrap class="sanLR04"> Telefono</td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rEstado{'tTelefono'});?></td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"> Referencia</td>
	    <td nowrap class="sanLR04" colspan="3"><?=utf8_encode($rEstado{'tReferencia'});?></td>
    </tr>
</table>
</form>