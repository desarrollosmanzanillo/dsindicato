<?php require_once("conexion/soluciones-mysql.php");  
$link = getLink();
	
$select=" SELECT cc.*, ce.tNombre AS Estado, cp.tNombre AS Pais ".
		" FROM catciudades cc ".
		" INNER JOIN catestados ce ON ce.eCodEstado=cc.eCodEstado ".
		" INNER JOIN catpaises cp ON cp.eCodPais=ce.eCodPais ".
		" WHERE eCodCiudad=".$_GET['eCodCiudad'];
$rEstado = mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC); ?>
<script type="text/javascript">
function consultar(){
	document.location = './?ePagina=2.3.4.5.php';
}
function nuevo(){
	document.location = './?ePagina=2.3.4.5.1.php';
}
</script>
<div id="dvCNS" style="display:none;"></div>
<form action="" method="post" name="Datos" id="Datos" onsubmit="return false;">
<table width="900px" border="0" cellpadding="0" cellspacing="0">
    <tr><td height="20"></td></tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"> C&oacute;digo</td>
	    <td width="50%" nowrap class="sanLR04"><?=sprintf("%07d",$rEstado{'eCodCiudad'});?></td>
		<td nowrap class="sanLR04"> Nombre</td>
	    <td width="50%" nowrap class="sanLR04"><?=utf8_encode($rEstado{'tNombre'});?></td>
    </tr>
    <tr>
	    <td height="23" nowrap class="sanLR04"> Nombre Corto </td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rEstado{'tNombreCorto'});?></td>
		<td height="23" nowrap class="sanLR04"> Pa&iacute;s </td>
	    <td nowrap class="sanLR04"><?=utf8_encode($rEstado{'Pais'});?></td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"> Estado </td>
	    <td nowrap class="sanLR04" colspan="3"><?=utf8_encode($rEstado{'Estado'});?></td>
    </tr>
</table>
</form>