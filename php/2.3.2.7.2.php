<?php require_once("conexion/soluciones-mysql.php");  
$link = getLink();
	
$select=" SELECT ce.*, cee.tNombre AS Estatus, CONCAT_WS(' ', cu.tNombre, cu.tApellidos) AS UsuarioEl
		FROM catchecadoras ce
		LEFT JOIN catestatus cee ON cee.tCodEstatus=ce.Estado
		LEFT JOIN catusuarios cu ON cu.eCodUsuario=ce.eCodUsuarioEliminado
		WHERE ce.id=".$_GET['eCodEntidad'];
$rEstado=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

$select=" SELECT *
		FROM catdias
		WHERE eCodDia=".$rEstado['descanso'];
$rDias=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

 ?>
<script type="text/javascript">
function consultar(){
	document.location='./?ePagina=2.3.2.7.php';
}
function nuevo(){
	document.location='./?ePagina=2.3.2.7.1.php';
}
</script>
<div id="dvCNS" style="display:none;"></div>
<form action="" method="post" name="Datos" id="Datos" onsubmit="return false;">
<table border="0" cellpadding="0" cellspacing="0" width="980px">
    <tr><td height="20"></td></tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"> C&oacute;digo</td>
	    <td width="33%" nowrap class="sanLR04"><b><?=sprintf("%07d",$rEstado{'id'});?></b></td>
		<td nowrap class="sanLR04"> RFC </td>
	    <td width="33%" nowrap class="sanLR04"><b><?=utf8_encode($rEstado{'RFC'});?></b></td>
	    <td nowrap class="sanLR04"><b><?=($rEstado{'bVIP'}==1 ? "Es VIP" : "")?></b></td>
	    <td width="33%" nowrap class="sanLR04"></td>
    </tr>
    <tr>
		<td height="23" nowrap class="sanLR04"> Nombre</td>
	    <td nowrap class="sanLR04"><b><?=utf8_encode($rEstado{'Nombre'});?></b></td>
		<td height="23" nowrap class="sanLR04"> Paterno </td>
	    <td nowrap class="sanLR04"><b><?=utf8_encode($rEstado{'Paterno'});?></b></td>
	    <td height="23" nowrap class="sanLR04"> Materno </td>
	    <td nowrap class="sanLR04"><b><?=utf8_encode($rEstado{'Materno'});?></b></td>
    </tr>
	<tr>
		<td height="23" nowrap class="sanLR04"> NSS </td>
	    <td nowrap class="sanLR04"><b><?=utf8_encode($rEstado{'IMSS'});?></b></td>
	    <td height="23" nowrap class="sanLR04"> CURP</td>
	    <td nowrap class="sanLR04"><b><?=utf8_encode($rEstado{'CURP'});?></b></td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"> Clinica</td>
	    <td nowrap class="sanLR04"><b><?=utf8_encode(sprintf("%03d",$rEstado{'Clinica'}));?></b></td>
		<td height="23" nowrap class="sanLR04"> Registro</td>
	    <td nowrap class="sanLR04"><b><?=($rEstado{'FechaRegistro'} ? date("d/m/Y H:i", strtotime($rEstado{'FechaRegistro'})) : "");?></b></td>
	    <td height="23" nowrap class="sanLR04"> Actualizaci&oacute;n</td>
	    <td nowrap class="sanLR04"><b><?=($rEstado{'fechaactualizacion'} ? date("d/m/Y H:i", strtotime($rEstado{'fechaactualizacion'})) : "");?></b></td>
    </tr>	
    <tr>
	    <td height="23" nowrap class="sanLR04"> Estatus</td>
	    <td nowrap class="sanLR04"><b><?=utf8_encode($rEstado{'Estatus'});?></b></td>
    </tr>
    <?php if($rEstado{'Estado'}=='CA'){ ?>
	    <tr>
			<td height="23" nowrap class="sanLR04"> Usuario Eliminaci&oacute;n</td>
		    <td nowrap class="sanLR04"><b><?=utf8_encode($rEstado{'UsuarioEl'});?></b></td>
		    <td height="23" nowrap class="sanLR04"> Fecha Eliminaci&oacute;n</td>
		    <td nowrap class="sanLR04"><b><?=date("d/m/Y H:i", strtotime($rEstado{'fhFechaEliminado'}));?></b></td>
	    </tr>
    <?php } ?>
</table>
</form>