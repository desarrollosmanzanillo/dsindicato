<?php require_once("conexion/soluciones-mysql.php");  
$link = getLink();
if($_POST){
	if($_POST['eProceso']==1){
		$exito				= 0;
		$bVIP				= ($_POST['bVIP']				? 1														: "NULL");
		$tRFC				= ($_POST['RFC']				? "'".trim(utf8_decode($_POST['RFC']))."'"				: "NULL");
		$tIMSS				= ($_POST['IMSS']				? "'".trim(utf8_decode($_POST['IMSS']))."'"				: "NULL");
		$tCURP				= ($_POST['CURP']				? "'".trim(utf8_decode($_POST['CURP']))."'"				: "NULL");
		$Estado 			= ($_POST['Estado']				? "'".trim(utf8_decode($_POST['Estado']))."'"			: "NULL");
		$tNombre			= ($_POST['nombre']				? "'".trim(utf8_decode($_POST['nombre']))."'"			: "NULL");
		$tPaterno			= ($_POST['paterno']			? "'".trim(utf8_decode($_POST['paterno']))."'"			: "NULL");
		$tMaterno			= ($_POST['materno']			? "'".trim(utf8_decode($_POST['materno']))."'"			: "NULL");
		$tClinica			= ($_POST['clinica']			? "'".trim(utf8_decode($_POST['clinica']))."'"			: "NULL");
		$eCodEntidad		= ($_POST['eCodEntidad']		? (int)$_POST['eCodEntidad']							: "NULL");
		$eCodCategoria		= ($_POST['eCodCategoria']		? (int)$_POST['eCodCategoria']							: "NULL");
		$eCodTipoCredito	= ($_POST['eCodTipoCredito']	? (int)$_POST['eCodTipoCredito']						: 1);
		$descanso           = $_POST['descanso'];
		$tEmpleado="'".utf8_decode(trim($_POST['paterno']).(trim($_POST['materno']) ? " ".trim($_POST['materno']) : "")." ".trim($_POST['nombre']))."'";
		
		if((int)$eCodEntidad>0){
			$update=" UPDATE catchecadoras
					SET Paterno=".$tPaterno.",
						Materno=".$tMaterno.",
						Nombre=".$tNombre.",
						Clinica=".$tClinica.",
						IMSS=".$tIMSS.",
						CURP=".$tCURP.",
						RFC=".$tRFC.",
						Empleado=".$tEmpleado.",
						bVIP=".$bVIP.",
						Estado=".$Estado.",
						fechaactualizacion=current_timestamp
					WHERE id=".$eCodEntidad;
			if($res=mysqli_query($link,$update)){
				$exito=1;
			}else{
				$exito=0;
			}
		}else{
			$insert=" INSERT INTO catchecadoras (Estado, Paterno, Materno, Nombre, Clinica, IMSS, CURP, RFC, Empleado, fechaactualizacion, FechaRegistro, bVIP) ".
					" VALUES (".$Estado.", ".$tPaterno.", ".$tMaterno.", ".$tNombre.", ".$tClinica.", ".$tIMSS.", ".$tCURP.", ".$tRFC.", ".$tEmpleado.", current_timestamp, current_timestamp, ".$bVIP.")";
			if($res=mysqli_query($link,$insert)){
				$exito=1;
				$select=" select last_insert_id() AS Llave ";
				$rCodigo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
				$eCodEntidad=(int)$rCodigo['Llave'];
			}else{
				$exito=0;
			}
		}
		print "<input type=\"hidden\" value=\"".($exito==1 ? (int)$eCodEntidad : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}
	
	if($_POST['eProceso']==2){
		$rRFC=mysqli_fetch_array(mysqli_query($link,"SELECT 1 AS RFC FROM catchecadoras WHERE RFC='".trim($_POST['RFC'])."' AND id!=".(int)$_POST['eCodEntidad']),MYSQLI_ASSOC);
		print "<input name=\"eEntidad\" id=\"eEntidad\" value=\"".(int)$rRFC{'RFC'}."\">";
	}
	
	if($_POST['eProceso']==3){
		$select=" SELECT * ".
				" FROM catestados ".
				" WHERE tCodEstatus='AC' ".
				" AND eCodPais=".$_POST['eCodPais'];
		$rsEstados=mysqli_query($link,$select); ?>
		<select name="eCodEstado" id="eCodEstado" style="width:175px" onchange="cargarCiudad();">
			<option value="">Seleccione...</option>
			<?php while($rEstado=mysqli_fetch_array($rsEstados,MYSQLI_ASSOC)){ ?>
				<option value="<?=$rEstado{'eCodEstado'}?>"><?=utf8_encode($rEstado{'tNombre'})?></option>
			<?php } ?>
		</select>
	<?php }
	
	if($_POST['eProceso']==4){
		$select=" SELECT * ".
				" FROM catciudades ".
				" WHERE tCodEstatus='AC' ".
				" AND eCodEstado=".$_POST['eCodEstado'];
		$rsCiudades=mysqli_query($link,$select); ?>
		<select name="eCodCiudad" id="eCodCiudad" style="width:175px">
			<option value="">Seleccione...</option>
			<?php while($rCiudad=mysqli_fetch_array($rsCiudades,MYSQLI_ASSOC)){ ?>
				<option value="<?=$rCiudad{'eCodCiudad'}?>"><?=utf8_encode($rCiudad{'tNombre'})?></option>
			<?php } ?>
		</select>
	<?php }

	if($_POST['eProceso']==5){
		$rRFC=mysqli_fetch_array(mysqli_query($link,"SELECT 1 AS RFC FROM catchecadoras WHERE IMSS='".trim($_POST['IMSS'])."' AND id!=".(int)$_POST['eCodEntidad']),MYSQLI_ASSOC);
		print "<input name=\"eEntidad\" id=\"eEntidad\" value=\"".(int)$rRFC{'RFC'}."\">";
	}

	if($_POST['eProceso']==6){
		$rRFC=mysqli_fetch_array(mysqli_query($link,"SELECT 1 AS RFC FROM catchecadoras WHERE CURP='".trim($_POST['CURP'])."' AND id!=".(int)$_POST['eCodEntidad']),MYSQLI_ASSOC);
		print "<input name=\"eEntidad\" id=\"eEntidad\" value=\"".(int)$rRFC{'RFC'}."\">";
	}
//
}else{
	
$select=" SELECT * ".
		" FROM catchecadoras ".
		" WHERE id=".$_GET['eCodEntidad'];
$rEntidad = mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC); 

$select=" SELECT * ".
		" FROM cattipoempleado ".
		" WHERE tCodEstatus='AC' ".
		" ORDER BY tNombre ASC ";
$rsTiposEntidades = mysqli_query($link,$select);

$select=" SELECT * ".
		" FROM cattipocredito ".
		" WHERE tCodEstatus='AC' ".
		" ORDER BY eCodCredito ASC ";
$rsTiposCredito = mysqli_query($link,$select);

$select=" SELECT * ".
		" FROM categorias ".
		" WHERE tCodEstatus='AC' ".
		" ORDER BY Categoria ASC ";
$rsCategorias = mysqli_query($link,$select); ?>
<script type="text/javascript">
function guardar(){
	dojo.byId('eProceso').value=1;
	bandera = false;
	mensaje = "¡Verifique lo siguiente!\n";
	if (!dojo.byId("paterno").value){
		mensaje+="* Paterno\n";
		bandera = true;		
	}
	if (!dojo.byId("nombre").value){
		mensaje+="* Nombre(s)\n";
		bandera = true;		
	}
	if (!dojo.byId("RFC").value){
		mensaje+="* RFC\n";
		bandera = true;		
	}
	if (!dojo.byId("IMSS").value){
		mensaje+="* NSS\n";
		bandera = true;		
	}
	if (!dojo.byId("clinica").value){
		mensaje+="* Clinica\n";
		bandera = true;		
	}
	if (!dojo.byId("CURP").value){
		mensaje+="* CURP\n";
		bandera = true;		
	}
	if (!dojo.byId("Estado").value){
		mensaje+="* Estatus\n";
		bandera = true;		
	}
	

	if (bandera==true){
		alert(mensaje);		
	}else{
		if (confirm("¿Desea Guardar la Información?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				if(dojo.byId("eCodigo").value>0){
					alert("¡La información se guardó correctamente!");
					tURL = './?ePagina=2.3.2.7.2.php&eCodEntidad='+dojo.byId("eCodigo").value;
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
		}
	}
}	

function verificarRFC(){
	if(dojo.byId('RFC').value){
		dojo.byId('eProceso').value=2;
		dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
			dojo.byId("dvCNS").innerHTML = tRespuesta;
			if(dojo.byId('eEntidad').value==1){
				alert("¡El RFC ya existe!");
				dojo.byId('RFC').value="";
			}
		}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
	}
}

function verificarIMSS(){
	if(dojo.byId('IMSS').value){
		dojo.byId('eProceso').value=5;
		dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
			dojo.byId("dvCNS").innerHTML = tRespuesta;
			if(dojo.byId('eEntidad').value==1){
				alert("¡El NSS ya existe!");
				dojo.byId('IMSS').value="";
			}
		}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
	}
}

function verificarCURP(){
	if(dojo.byId('CURP').value){
		dojo.byId('eProceso').value=6;
		dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
			dojo.byId("dvCNS").innerHTML = tRespuesta;
			if(dojo.byId('eEntidad').value==1){
				alert("¡El CURP ya existe!");
				dojo.byId('CURP').value="";
			}
		}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
	}
}

function consultar(){
	document.location = './?ePagina=2.3.2.7.php';
}

dojo.addOnLoad(function(){ });
</script>
<div id="dvCNS" style="display:none;"></div>
<form action="" method="post" name="Datos" id="Datos" onsubmit="return false;">
<input type="hidden" name="eProceso" id="eProceso" value="" />
<input type="hidden" name="eCodEntidad" id="eCodEntidad" value="<?=$_GET['eCodEntidad'];?>" />
<table border="0" cellpadding="0" cellspacing="0">
    <tr><td height="20"></td></tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Paterno </td>
	    <td width="33%" nowrap class="sanLR04"><input name="paterno" type="text" dojoType="dijit.form.TextBox" id="paterno" value="<?=utf8_encode($rEntidad{'Paterno'});?>" style="width:175px">        </td>
		<td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Materno </td>
	    <td width="33%" nowrap class="sanLR04"><input name="materno" type="text" dojoType="dijit.form.TextBox" id="materno" value="<?=utf8_encode($rEntidad{'Materno'});?>" style="width:175px;">        </td>
        <td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Nombre(s) </td>
        <td width="33%" nowrap class="sanLR04"><input name="nombre" type="text" dojoType="dijit.form.TextBox" id="nombre" value="<?=utf8_encode($rEntidad{'Nombre'});?>" style="width:175px;">
        </td>
    </tr>
    <tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> RFC </td>
	    <td width="33%" nowrap class="sanLR04"><input name="RFC" type="text" dojoType="dijit.form.TextBox" id="RFC" value="<?=$rEntidad{'RFC'};?>" style="width:175px" onblur="verificarRFC();"></td>
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Clinica</td>	    
        <td width="33%" nowrap class="sanLR04"><input name="clinica" type="text" dojoType="dijit.form.TextBox" id="clinica" value="<?=utf8_encode(sprintf("%03d",$rEntidad{'Clinica'}));?>" style="width:85px;">
        </td>
        <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> NSS </td>
	    <td width="33%" nowrap class="sanLR04"><input name="IMSS" type="text" dojoType="dijit.form.TextBox" id="IMSS" value="<?=utf8_encode($rEntidad{'IMSS'});?>" style="width:175px;" onblur="verificarIMSS();"></td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> CURP </td>
	    <td width="33%" nowrap class="sanLR04"><input name="CURP" type="text" dojoType="dijit.form.TextBox" id="CURP" value="<?=utf8_encode($rEntidad{'CURP'});?>" style="width:175px;" onblur="verificarCURP();"></td>
	    <td class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Estatus </td>
	    <td width="33%" class="sanLR04">
			<select name="Estado" id="Estado" style="width:175px">
				<option value="">Seleccione...</option>
				<option value="AC" <?=($rEntidad{'Estado'}=='AC' || (int)$_GET['eCodEntidad']==0  ? "selected" : "")?>>ACTIVO</option>
				<option value="CA" <?=($rEntidad{'Estado'}=='CA' ? "selected" : "")?>>CANCELADO</option>
			</select>
		</td>
		<td height="23" nowrap class="sanLR04"></td>
		<td height="23" nowrap class="sanLR04"><label><input type="checkbox" name="bVIP" id="bVIP" value="1" <?=($rEntidad{'bVIP'}==1 ? "checked" : "")?>>VIP S&iacute;</label></td>
    </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle" /> <font class="fntG10K">Indica que es obligatorio llenar el campo.
    </font></td>
    </tr>
</table>
</form>
<?php } ?>