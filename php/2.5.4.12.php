﻿<?php require_once("conexion/soluciones-mysql.php");
$link = getLink();
if($_POST){
	date_default_timezone_set('America/Mexico_City');
  $idUsuario = ($_POST['eUsuario'] ? $_POST['eUsuario'] : "NULL");
	$varfecha = isset($_POST["fecha"]) ? $_POST["fecha"] : date("Y-m-d");
  $fechaayerC = date('Y-m-d',strtotime('-1 days', strtotime($varfecha)));
  $fechaantier = date('Y-m-d',strtotime('-2 days', strtotime($varfecha)));
	$fechabaja = date('Y-m-d',strtotime('-2 days', strtotime($varfecha)));
	$fechaayer = date('Y-m-d',strtotime('-1 days', strtotime($varfecha)));

  //////////////////////////////////////
  //////Inicio Continuaciones-->
  $select=" SELECT * ".
          " FROM sisconfiguracion ";
  $configura=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

  $select=" SELECT * ".
          " FROM categorias ".
          " WHERE indice=11 ";
  $salarioMin=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

  $select=" SELECT idEmpleado ".
          " FROM incapacidades ".
          " WHERE Estado='AC' AND FechaInicial<='".$fechaayerC."' AND FechaFinal>='".$fechaayerC."' ";
  $incapacidades=mysqli_query($link,$select);
  $UsuariosInc="0";
  while($incapacidad=mysqli_fetch_array($incapacidades,MYSQLI_ASSOC)){
      $UsuariosInc.=", ".$incapacidad{'idEmpleado'};
  }
  $select=" SELECT assi.id ".
          " FROM movimientosafiliatorios mos ".
          " INNER JOIN relasistenciamovimientos rassi ON rassi.eCodMovimiento=mos.eCodMovimiento ".
          " INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia ".
          " LEFT JOIN empleados emp ON assi.idEmpleado = emp.id ".
          " WHERE assi.idEmpresa=".(int)$_POST['eCodEntidad'].
          " AND assi.Fecha Between '".$fechaantier."' AND '".$fechaayerC."' ".
          ((int)$_POST['eCodTipoEntidad']>0 ? " AND emp.eCodTipo=".(int)$_POST['eCodTipoEntidad'] : "");
  $afiliatorios=mysqli_query($link,$select);
  $UsuariosAfil="0";
  while($afiliatorio=mysqli_fetch_array($afiliatorios,MYSQLI_ASSOC)){
      $UsuariosAfil.=", ".$afiliatorio{'id'};
  }
  $select=" SELECT assihh.idEmpleado ".
          " FROM movimientosafiliatorios moshh ".
          " INNER JOIN relasistenciamovimientos rassihh ON rassihh.eCodMovimiento=moshh.eCodMovimiento ".
          " INNER JOIN asistencias assihh ON assihh.id=rassihh.eCodAsistencia ".
          " LEFT JOIN empleados emp ON assihh.idEmpleado=emp.id ".
          " WHERE moshh.fhFecha='".$fechaantier."' AND assihh.idEmpresa=".(int)$_POST['eCodEntidad']." AND moshh.tCodTipoMovimiento NOT IN ('02') AND moshh.eCodEstatus='AC' ".
          ((int)$_POST['eCodTipoEntidad']>0 ? " AND emp.eCodTipo=".(int)$_POST['eCodTipoEntidad'] : "");
  $movimientos=mysqli_query($link,$select);
  $UsiariosMovi="0";
  while($movimiento=mysqli_fetch_array($movimientos,MYSQLI_ASSOC)){
      $UsiariosMovi.=", ".$movimiento{'idEmpleado'};
  }

  $select=" SELECT DISTINCT emple.id AS numeral, emple.IMSS AS imss, cent.tRegistroPatronal AS rp, emple.Paterno AS pat, ".
            " Materno AS mat, Nombre AS nomb, ctiemp.eCodTipoJornada AS tipjo, '".$fechaayerC."' AS Fecha, emple.Clinica AS clin, emple.CURP AS curp, ".
" CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
    FROM asistencias asish 
    WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayerC."'
    AND asish.idEmpresa=".(int)$_POST['eCodEntidad']."
    AND asish.Estado='AC' 
    AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
    AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
    AND asish.id NOT IN (".$UsuariosAfil.")
    AND asish.idEmpleado IN (".$UsiariosMovi.")
    AND asish.idEmpleado=asis.idEmpleado)>".(float)$configura{'dTopeSalario'}." THEN ".(float)$configura{'dTopeSalario'}.
" ELSE CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
        FROM asistencias asish 
        WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayerC."'
        AND asish.idEmpresa=".(int)$_POST['eCodEntidad']."
        AND asish.Estado='AC' 
        AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
        AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
        AND asish.id NOT IN (".$UsuariosAfil.")
        AND asish.idEmpleado IN (".$UsiariosMovi.")
        AND asish.idEmpleado=asis.idEmpleado)=0 THEN FORMAT((".(float)$salarioMin{'Turno1'}."*ctiemp.dFactorIntegracion),2) ".
        " ELSE CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
            FROM asistencias asish 
            WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayerC."'
            AND asish.idEmpresa=".(int)$_POST['eCodEntidad']."
            AND asish.Estado='AC' 
            AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
            AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
            AND asish.id NOT IN (".$UsuariosAfil.")
            AND asish.idEmpleado IN (".$UsiariosMovi.")
            AND asish.idEmpleado=asis.idEmpleado)<".$configura{'dSalarioMinimo'}." THEN ".$configura{'dSalarioMinimo'}.
            " ELSE (SELECT sum(asish.Salario+asish.dBono)
                FROM asistencias asish 
                WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayerC."'
                AND asish.idEmpresa=".(int)$_POST['eCodEntidad']."
                AND asish.Estado='AC' 
                AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
                AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
                AND asish.id NOT IN (".$UsuariosAfil.")
                AND asish.idEmpleado IN (".$UsiariosMovi.")
                AND asish.idEmpleado=asis.idEmpleado)
            END 
        END
    END AS tSalario ".
    " FROM asistencias asis ".
    " INNER JOIN empleados emple ON emple.id=asis.idEmpleado ".
    " INNER JOIN cattipoempleado ctiemp ON ctiemp.eCodTipoEntidad=emple.eCodTipo ".
    " LEFT JOIN catentidades cent ON cent.eCodEntidad=asis.idEmpresa ".
    " WHERE asis.Estado='AC' AND asis.idEmpresa=".(int)$_POST['eCodEntidad']." AND asis.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asis.id) ".
    " AND asis.idEmpleado NOT IN (SELECT assi.idEmpleado FROM movimientosafiliatorios mos INNER JOIN relasistenciamovimientos rassi ON ".
    " rassi.eCodMovimiento=mos.eCodMovimiento INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia WHERE assi.id=asis.id AND assi.idEmpresa=".(int)$_POST['eCodEntidad'].") ".
    " AND asis.Fecha Between '".$fechaantier."' AND '".$fechaayerC."' AND asis.idCategorias NOT IN (33) ".((int)$_POST['eCodTipoEntidad']>0 ? " AND emple.eCodTipo=".(int)$_POST['eCodTipoEntidad'] : "")." AND asis.idEmpleado NOT IN (".$UsuariosInc.") ".
    "AND asis.idEmpleado IN (SELECT assi.idEmpleado FROM movimientosafiliatorios mos INNER JOIN relasistenciamovimientos rassi ON rassi.eCodMovimiento=mos.eCodMovimiento ".
    " INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia WHERE assi.idEmpleado=asis.idEmpleado AND assi.idEmpresa=".(int)$_POST['eCodEntidad']." AND mos.fhFecha='".$fechaantier."' ".
    " AND mos.tCodTipoMovimiento NOT IN ('02') AND mos.eCodEstatus='AC') AND ".
" CASE WHEN (SELECT mosa.dImporte FROM movimientosafiliatorios mosa 
INNER JOIN relasistenciamovimientos rassia ON rassia.eCodMovimiento=mosa.eCodMovimiento 
INNER JOIN asistencias assia ON assia.id=rassia.eCodAsistencia
WHERE assia.idEmpleado=asis.idEmpleado AND mosa.fhFecha='".$fechaantier."' AND assia.idEmpresa=".(int)$_POST['eCodEntidad']." AND mosa.tCodTipoMovimiento NOT IN ('02') AND mosa.eCodEstatus='AC' LIMIT 1) IS NULL 
THEN 0 ELSE (SELECT mosa.dImporte
FROM movimientosafiliatorios mosa 
INNER JOIN relasistenciamovimientos rassia ON rassia.eCodMovimiento=mosa.eCodMovimiento 
INNER JOIN asistencias assia ON assia.id=rassia.eCodAsistencia
WHERE assia.idEmpleado=asis.idEmpleado AND mosa.fhFecha='".$fechaantier."' AND assia.idEmpresa=".(int)$_POST['eCodEntidad']." AND mosa.tCodTipoMovimiento NOT IN ('02') AND mosa.eCodEstatus='AC' LIMIT 1) END = 
CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayerC."'
AND asish.idEmpresa=".(int)$_POST['eCodEntidad']."
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)>".(float)$configura{'dTopeSalario'}." THEN ".(float)$configura{'dTopeSalario'}." ELSE ".
    " CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayerC."'
AND asish.idEmpresa=".(int)$_POST['eCodEntidad']."
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)=0 THEN FORMAT((".(float)$salarioMin{'Turno1'}."*ctiemp.dFactorIntegracion),2) ELSE ".
    " CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayerC."'
AND asish.idEmpresa=".(int)$_POST['eCodEntidad']."
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)<".$configura{'dSalarioMinimo'}." THEN ".$configura{'dSalarioMinimo'}." ELSE (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayerC."'
AND asish.idEmpresa=".(int)$_POST['eCodEntidad']."
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado) END ".
    " END ".
    " END
UNION 
SELECT DISTINCT emple.id AS numeral, emple.IMSS AS imss, cent.tRegistroPatronal AS rp, emple.Paterno AS pat, Materno AS mat, Nombre AS nomb, ctiemp.eCodTipoJornada AS tipjo, '".$fechaayerC."' AS Fecha, emple.Clinica AS clin, emple.CURP AS curp, CASE WHEN (SELECT mosa.dImporte
FROM movimientosafiliatorios mosa 
INNER JOIN relasistenciamovimientos rassia ON rassia.eCodMovimiento=mosa.eCodMovimiento 
INNER JOIN asistencias assia ON assia.id=rassia.eCodAsistencia
WHERE assia.idEmpleado=asis.idEmpleado AND mosa.fhFecha='".$fechaantier."' AND assia.idEmpresa=".(int)$_POST['eCodEntidad']." AND mosa.tCodTipoMovimiento NOT IN ('02') AND mosa.eCodEstatus='AC' LIMIT 1)>".(float)$configura{'dTopeSalario'}." THEN ".(float)$configura{'dTopeSalario'}." ELSE (SELECT mosa.dImporte
FROM movimientosafiliatorios mosa 
INNER JOIN relasistenciamovimientos rassia ON rassia.eCodMovimiento=mosa.eCodMovimiento 
INNER JOIN asistencias assia ON assia.id=rassia.eCodAsistencia
WHERE assia.idEmpleado=asis.idEmpleado AND mosa.fhFecha='".$fechaantier."' AND assia.idEmpresa=".(int)$_POST['eCodEntidad']." AND mosa.tCodTipoMovimiento NOT IN ('02') AND mosa.eCodEstatus='AC' LIMIT 1) END AS tSalario
FROM asistencias asis
INNER JOIN empleados emple ON emple.id=asis.idEmpleado
INNER JOIN cattipoempleado ctiemp ON ctiemp.eCodTipoEntidad=emple.eCodTipo
LEFT JOIN catentidades cent ON cent.eCodEntidad=asis.idEmpresa
WHERE asis.Fecha Between '".$fechaantier."' AND '".$fechaayerC."' 
AND asis.idEmpresa=".(int)$_POST['eCodEntidad']."
AND asis.Estado='AC' 
AND asis.idEmpleado NOT IN (".$UsuariosInc.") 
AND asis.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asis.id) 
AND asis.idEmpleado NOT IN (SELECT assi.idEmpleado 
FROM movimientosafiliatorios mos 
INNER JOIN relasistenciamovimientos rassi ON rassi.eCodMovimiento=mos.eCodMovimiento 
INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia
WHERE assi.id=asis.id AND assi.idEmpresa=".(int)$_POST['eCodEntidad'].")
AND asis.idEmpleado IN (SELECT assi.idEmpleado 
FROM movimientosafiliatorios mos 
INNER JOIN relasistenciamovimientos rassi ON rassi.eCodMovimiento=mos.eCodMovimiento 
INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia
WHERE assi.idEmpleado=asis.idEmpleado AND mos.fhFecha='".$fechaantier."' AND assi.idEmpresa=".(int)$_POST['eCodEntidad']." AND mos.tCodTipoMovimiento NOT IN ('02') AND mos.eCodEstatus='AC')
AND asis.idEmpleado IN (SELECT eCodEmpleado FROM ausentismo WHERE fhFecha='".$fechaayerC."' AND eCodEstatus='AC' AND eCodEmpleado=asis.idEmpleado)
AND asis.idCategorias NOT IN (33) ".((int)$_POST['eCodTipoEntidad']>0 ? " AND emple.eCodTipo=".$_POST['eCodTipoEntidad'] : "");
  if($_POST['eAccion']==888){
      print $select."<br>";
  }
  /*$vContinua=$select;
  $continuaciones=mysql_query($select);
  $select=" SELECT * FROM sisconfiguracion ";
  $configura2=mysql_fetch_array(mysql_query($select));
  while($continua=mysql_fetch_array($continuaciones)){
      $dSalarioR=((float)$continua{'tSalario'}>(float)$configura2{'dTopeSalario'} ? (float)$configura2{'dTopeSalario'} : (float)$continua{'tSalario'});
      //print "Empleado: ".$idUsuario." val->".$dSalarioR."==".(float)$configura2{'dTopeSalarioMinimoDescanso'}."<br>";
      //if($dSalarioR==(float)$configura2{'dTopeSalarioMinimoDescanso'}){
          //" VALUES (".$idUsuario.", '".$fechaayerC."', '01', 'AC', ".(float)$rAsistencia{'tSalario'}.", 'NULL', CURRENT_TIMESTAMP) ";
          $insert=" INSERT INTO movimientosafiliatorios (eCodUsuario, fhFecha, tCodTipoMovimiento,  eCodEstatus, dImporte, tFolio, fhFechaRegistro) ".                        
                  " VALUES (".$idUsuario.", '".$fechaayerC."', '01', 'AC', ".$dSalarioR." , 'NA', CURRENT_TIMESTAMP) ";
          if($_POST['eAccion']==888){
            print $insert."<br>";
          }
          if($res=mysql_query($insert)){
            if($_POST['eAccion']==888){
              print "insert Ok <br>";
            }
            $exito=1;
            $select=" select last_insert_id() AS Llave ";
            $rCodigo=mysql_fetch_array(mysql_query($select));
            $eCodMovimiento=(int)$rCodigo['Llave'];

            $select=" SELECT asish.id
                    FROM asistencias asish 
                    WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayerC."'
                    AND asish.idEmpresa=".(int)$_POST['eCodEntidad']."
                    AND asish.Estado='AC' 
                    AND asish.idEmpleado NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayerC."' AND FechaFinal>='".$fechaayerC."') 
                    AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
                    AND asish.idEmpleado NOT IN (SELECT assih.idEmpleado 
                    FROM movimientosafiliatorios mosh 
                    INNER JOIN relasistenciamovimientos rassih ON rassih.eCodMovimiento=mosh.eCodMovimiento 
                    INNER JOIN asistencias assih ON assih.id=rassih.eCodAsistencia
                    WHERE assih.id=asish.id AND assih.idEmpresa=".(int)$_POST['eCodEntidad'].")
                    AND asish.idEmpleado IN (SELECT assihh.idEmpleado 
                    FROM movimientosafiliatorios moshh 
                    INNER JOIN relasistenciamovimientos rassihh ON rassihh.eCodMovimiento=moshh.eCodMovimiento 
                    INNER JOIN asistencias assihh ON assihh.id=rassihh.eCodAsistencia
                    WHERE assihh.idEmpleado=asish.idEmpleado AND moshh.fhFecha='".$fechaantier."' AND assihh.idEmpresa=".(int)$_POST['eCodEntidad']." AND moshh.tCodTipoMovimiento NOT IN ('02') AND moshh.eCodEstatus='AC')
                    AND asish.idEmpleado=".$continua{'numeral'};
            $asistenciasEmp=mysql_query($select);
            while($asisEmp=mysql_fetch_array($asistenciasEmp)){
                $insertt=" INSERT INTO relasistenciamovimientos (eCodAsistencia, eCodMovimiento) ".
                        " VALUES (".$asisEmp{'id'}.", ".$eCodMovimiento.") ";
                if($_POST['eAccion']==888){
                  print $insertt."<br>";
                }
                if($res=mysql_query($insertt)){
                  if($_POST['eAccion']==888){
                    print "insertt Ok <br>";
                  }
                  $exito=1;
                }else{
                  if($_POST['eAccion']==888){
                    print "insertt Error <br>";
                  }
                  $exito=0;
                }
            }
          }else{
            if($_POST['eAccion']==888){
              print "insert Error <br>";
            }
            $exito=0;
          }
      //}
  }*/
  //////<--Fin Continuaciones idCategorias
  //////////////////////////////////////$fechaayerC fechaantier


	//Consulta para validar si tienen continuacion de asistencia dia siguiente mismo empleado.
	$select=" SELECT asi.idEmpleado ".
    			" FROM asistencias asi ".
    			" LEFT JOIN relasistenciamovimientos AS rel ON rel.eCodAsistencia=asi.id ".
    			" LEFT JOIN movimientosafiliatorios AS mov ON mov.eCodMovimiento=rel.eCodMovimiento AND mov.eCodEstatus='AC' ".
    			" LEFT JOIN empleados AS emp ON asi.idEmpleado=emp.id ".
    			" WHERE asi.Estado='AC' AND mov.fhFecha='".$fechaayer."' AND asi.idEmpresa=".(int)$_POST['eCodEntidad'].
    			($_POST['eCodTipoEntidad'] ? " AND emp.eCodTipo=".$_POST['eCodTipoEntidad'] : "").
          " GROUP BY asi.idEmpleado order by asi.idEmpleado ";
	$continua="";
	if($Rsdianext=mysqli_query($link,$select)){
		while($rdianext=mysqli_fetch_array($Rsdianext,MYSQLI_ASSOC)){
			$continua=$continua.$rdianext["idEmpleado"]." , ";
		}
	}
	$continua=$continua." 0 ";

	$select=" SELECT asi.idEmpleado ".
    			" FROM asistencias asi ".
    			" LEFT JOIN relasistenciamovimientos AS rel ON rel.eCodAsistencia=asi.id ".
    			" LEFT JOIN movimientosafiliatorios AS mov ON mov.eCodMovimiento=rel.eCodMovimiento AND mov.eCodEstatus='AC' ".
    			" LEFT JOIN empleados AS emp ON asi.idEmpleado=emp.id ".
    			" WHERE asi.Estado='AC' AND mov.tCodTipoMovimiento='02' AND mov.fhFecha='".$fechabaja."' AND asi.idEmpresa=".(int)$_POST['eCodEntidad'].
    			($_POST['eCodTipoEntidad'] ? " AND emp.eCodTipo=".$_POST['eCodTipoEntidad'] : "").
    			" GROUP BY asi.idEmpleado order by asi.idEmpleado ";
	$baja="";
	if($Rsdianext=mysqli_query($link,$select)){
		while($rdianext=mysqli_fetch_array($Rsdianext,MYSQLI_ASSOC)){
			$baja=$baja.$rdianext["idEmpleado"]." , ";
		}
	}
	$baja=$baja." 0 ";

  $select=" SELECT idEmpleado ".
          " FROM incapacidades ".
          " WHERE Estado='AC' ".
          " AND FechaInicial<='".$fechaayerC."' AND FechaFinal>='".$fechaayerC."' ";
  $incap="";
  if($rsIncapacidades=mysqli_query($link,$select)){
    while($rIncapacidad=mysqli_fetch_array($rsIncapacidades,MYSQLI_ASSOC)){
      $incap=$incap.$rIncapacidad["idEmpleado"]." , ";
    }
  }
  $incap=$incap." 0 ";

	$select=" SELECT asi.id AS numeral, asi.*, cat.eCodEntidad AS iden, cat.tRegistroPatronal AS rp, emp.IMSS, emp.Paterno , emp.materno, emp.nombre, ".
    			" mov.tFolio, mov.fhFecha, mov.tCodTipoMovimiento, ccb.tNombre AS CausaBaja ".
    			" FROM asistencias asi ".
    			" LEFT JOIN relasistenciamovimientos AS rel ON rel.eCodAsistencia=asi.id ".
    			" LEFT JOIN movimientosafiliatorios AS mov ON mov.eCodMovimiento=rel.eCodMovimiento AND mov.eCodEstatus='AC' ".
    			" LEFT JOIN catentidades AS cat ON asi.idEmpresa = cat.eCodEntidad ".
    			" LEFT JOIN empleados AS emp ON asi.idEmpleado = emp.id ".
    			" LEFT JOIN categorias AS ser ON ser.indice = asi.idCategorias ".
          " LEFT JOIN catcausabaja ccb ON ccb.eCodCausa=".(int)$_POST['eCodCausa'].
    			" WHERE asi.Estado='AC' AND mov.fhFecha='".$fechabaja."' AND mov.tFolio IS NOT NULL AND asi.idEmpresa=".(int)$_POST['eCodEntidad'].//AND asi.idCategorias NOT IN (17) 
          " AND emp.id not in (".$continua." , ".$baja.", ".$incap.") ".
          ($_POST['eCodTipoEntidad'] ? " AND emp.eCodTipo=".$_POST['eCodTipoEntidad'] : "").
    			" GROUP BY asi.idEmpleado order by asi.id ";
    if($_POST['eAccion']==999){
      print $select;
    }
    $rsServicios=mysqli_query($link,$select);
    $cont=0;
    $registros=(int)mysqli_num_rows($rsServicios); ?>
<table cellspacing="0" border="0" width="980px">
  <tr>
    <td width="50%"><hr color="#666666" /></td>
    <td class="sanLR04" nowrap="nowrap"><b>Registros Encontrados: (<?=$registros;?>)</b></td>
    <td width="50%"><hr color="#666666" /></td>
  </tr>
</table>
<div style="display:block; top:0; left:0; width:965px; z-index=1; overflow: auto; height:400px;">
  <table cellspacing="0" border="0" width="980px">
    <thead>
      <tr class="thEncabezado">	      	
      <td nowrap="nowrap" class="sanLR04">R. Patronal</td>
	    <td nowrap="nowrap" class="sanLR04">NSS</td>
      <td nowrap="nowrap" class="sanLR04">C. Empleado</td>
	    <td nowrap="nowrap" class="sanLR04">Paterno</td> 
	    <td nowrap="nowrap" class="sanLR04">Materno</td>
      <td nowrap="nowrap" class="sanLR04">Nombre(s)</td>
      <td nowrap="nowrap" class="sanLR04">Filler</td>
      <td nowrap="nowrap" class="sanLR04">Fecha</td>
      <td nowrap="nowrap" class="sanLR04">Filler</td>
      <td nowrap="nowrap" class="sanLR04">Movimiento</td>      
      <td nowrap="nowrap" class="sanLR04">Guia</td>
      <td nowrap="nowrap" class="sanLR04">C. Trabajador</td>
      <td nowrap="nowrap" class="sanLR04">Causa de Baja</td>
      <td nowrap="nowrap" class="sanLR04">Filler</td>      
	    <td nowrap="nowrap" class="sanLR04" width="100%">Identificador</td> <!--Ya quedo-->                    
	  </tr>   
    </thead>
    <tbody>
      <?php $i=1;
      while($rServicio=mysqli_fetch_array($rsServicios,MYSQLI_ASSOC)){
    		$fech=$rServicio["fhFecha"];
    		$fecha=explode("-",$fech);
    		$abc=$fecha[2].$fecha[1].$fecha[0]; ?>
        <tr>
            <input type="hidden" id="id<?=$i?>" name="id<?=$i?>" value="<?=$rServicio["asi.id"];?>">
            <td nowrap="nowrap" class="sanLR04 columnB"><b><?=$rServicio{'rp'};?></b></td>
            <td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rServicio["IMSS"]);?></td>
            <td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rServicio["idEmpleado"]);?></td>
            <td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rServicio["Paterno"]);?></td>
            <td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rServicio["materno"]);?></td>
            <td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rServicio["nombre"]);?></td>
            <td nowrap="nowrap" class="sanLR04 columnB"></td>
            <td nowrap="nowrap" class="sanLR04"><?=utf8_encode($abc);?></td>
            <td nowrap="nowrap" class="sanLR04 columnB"></td>
            <td nowrap="nowrap" class="sanLR04">02</td>
            <td nowrap="nowrap" class="sanLR04 columnB">03400</td>
            <td nowrap="nowrap" class="sanLR04">ESTIBADOR</td>
            <td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rServicio["CausaBaja"]);?></td>
            <td nowrap="nowrap" class="sanLR04"></td>
            <td nowrap="nowrap" class="sanLR04 columnB" width="100%">9</td>
        </tr>
      <?php $i++; } ?>
    </tbody>
  </table>
</div>
<?php }else{ 
$select=" SELECT *
        FROM cattipoempleado
        WHERE tCodEstatus='AC'
        ORDER BY tNombreCorto ";
$rsTiposEmpleados=mysqli_query($link,$select);
//
$select=" SELECT *
        FROM catcausabaja
        WHERE tCodEstatus='AC'
        ORDER BY tNombre ASC ";
$rsCausasBajas=mysqli_query($link,$select);

$select=" SELECT *
        FROM catentidades
        WHERE tCodEstatus='AC'
        ORDER BY tSiglas ";
$rsEntidades=mysqli_query($link,$select); ?>
<script type="text/javascript">
function generarBatch(){
	var UrlBatch = "php/batch/2.5.4.6.php?a=1"+
    (dojo.byId('fecha').value           ? "&fecha="+dojo.byId('fecha').value                      : "")+
    (dojo.byId('eCodCausa').value       ? "&eCodCausa="+dojo.byId('eCodCausa').value              : "")+
    (dojo.byId('eCodEntidad').value     ? "&eCodEntidad="+dojo.byId('eCodEntidad').value          : "")+
    //(dojo.byId('chkMasivo').value       ? "&chkMasivo="+dojo.byId('chkMasivo').value              : "")+
    (dojo.byId('eCodTipoEntidad').value ? "&eCodTipoEntidad="+dojo.byId('eCodTipoEntidad').value  : "");

    var Datos = document.createElement("FORM");
    document.body.appendChild(Datos);
    Datos.name='Datos';
    Datos.method = "POST";
    Datos.action = UrlBatch;
    Datos.submit();
}

dojo.addOnLoad(function(){
  filtrarConsulta();
});
</script>
<form name="Datos" id="Datos" method="post" action="" onsubmit="return false;">
  <input type="hidden" name="eUsuario" id="eUsuario" value="<?=(int)$_SESSION['sesionUsuario']['eCodUsuario'];?>" />
  <input type="hidden" value="0" name="ePagina" id="ePagina" />
  <input type="hidden" value="" name="eAccion" id="eAccion" />
  <table width="100%" border="0">
    <tr>
      <td colpan="3" width="100%"></td>
      <td align="right" nowrap="nowrap"><a class="fntBR11" href="javaScript:mostrarFiltros('Busqueda');">Busqueda de Registros</a></td>
    </tr>
    <tr id="trBusqueda" style="display:none">
      <td colspan="4"><table width="100%" bgcolor="#f9f9f9">
          <tr>
            <td class="sanLR04" height="5"></td>
          </tr>
          <tr>
            <td class="sanLR04" height="20">Fecha</td>
            <td class="sanLR04" width="50%"><input type="date" name="fecha" dojoType="dijit.form.TextBox" id="fecha" value="<?=$_POST["fecha"] ? $_POST["fecha"] : date("Y-m-d")?>" style="width:140px"></td>
            <td class="sanLR04" height="20" nowrap>Tipo de Trabajador</td>
            <td class="sanLR04" width="50%">
                <select name="eCodTipoEntidad" id="eCodTipoEntidad" style="width:180px; height:25px;">
                  <option value="">Seleccione...</option>
                  <?php while($rTipoEmpleado=mysqli_fetch_array($rsTiposEmpleados,MYSQLI_ASSOC)){ ?>
                      <option value="<?=$rTipoEmpleado{'eCodTipoEntidad'}?>" <?=($rTipoEmpleado{'eCodTipoEntidad'}==2 ? "selected" : "")?>><?=utf8_encode($rTipoEmpleado{'tNombreCorto'})?></option>
                  <?php } ?>
                </select>
            </td>
          </tr>
          <tr>
            <td class="sanLR04" nowrap>Causa Baja</td>
            <td class="sanLR04">
              <select name="eCodCausa" id="eCodCausa" style="width:180px; height:25px;">
                <?php while($rCausaBaja=mysqli_fetch_array($rsCausasBajas,MYSQLI_ASSOC)){ ?>
                    <option value="<?=$rCausaBaja{'eCodCausa'}?>" <?=((int)$rCausaBaja{'bPredefinido'}==1 ? "selected" : "")?>><?=utf8_encode($rCausaBaja{'tNombre'})?></option>
                <?php } ?>
              </select>
            </td>
            <td class="sanLR04" height="20" nowrap>Entidad</td>
            <td class="sanLR04" width="50%">
                <select name="eCodEntidad" id="eCodEntidad" style="width:180px; height:25px;">
                    <option value="">Seleccione...</option>
                    <?php while($rEntidad=mysqli_fetch_array($rsEntidades,MYSQLI_ASSOC)){ ?>
                        <option value="<?=$rEntidad{'eCodEntidad'}?>" <?=($rEntidad{'bPrincipal'}==1 ? "selected" : "");?> ><?=utf8_encode($rEntidad{'tSiglas'})?></option>
                    <?php } ?>
                </select>
            </td>
          </tr>
          <tr>
            <td class="sanLR04" height="5"></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td colspan="4"><div id="dvCNS"></div></td>
    </tr>
  </table>
</form>
<?php } ?>