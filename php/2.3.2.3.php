<style>
#divgral {
     overflow:scroll;
     height:450px;
     width:900px;
}
</style>
<?php
require_once("conexion/soluciones-mysql.php"); 
$link = getLink();
if($_POST){
	if($_POST['eAccion']==1){
		foreach($_POST as $k => $valor){
			$nombre = strval($k);
			$campo = "eCodTipoEntidad";
			if(strstr($nombre,$campo) && (int)$valor>0){
				$update=" UPDATE cattipoempleado SET tCodEstatus='CA' WHERE eCodTipoEntidad=".$valor;
				mysqli_query($link,$update);
			}
		}
	}

	$select=" SELECT tem.eCodTipoEntidad , tem.tCodEstatus,  tem.tNombre, tem.tNombreCorto, tem.dFactorIntegracion,  tra.tNombre AS eCodTipoTrabajador, jor.tNombre AS eCodTipoJornada, sal.tNombre AS eCodTipoSalario, cau.tNombre as causa ".
			" FROM cattipoempleado AS tem ".
      " INNER JOIN cattiposalario AS sal ON sal.eCodSalario=tem.eCodTipoSalario ".
      " INNER JOIN cattipojornada AS jor ON jor.eCodJornada=tem.eCodTipoJornada ".
      " INNER JOIN cattipotrabajador AS tra ON tra.eCodTrabajador=tem.eCodTipoTrabajador ".
      " INNER JOIN catcausabaja AS cau ON cau.eCodCausa=tem.eCodCausaBaja ".
			" WHERE 1=1".
			($_POST['eCodTipoEntidad'] ? " AND tem.eCodTipoEntidad=".$_POST['eCodTipoEntidad'] : "").
			($_POST['tNombre'] ? " AND tem.tNombre LIKE '%".$_POST['tNombre']."%'" : "").
			($_POST['tNombreCorto'] ? " AND tem.tNombreCorto LIKE '%".$_POST['tNombreCorto']."%'" : "");	
  
  //print($select);

	$rsTiposEntidades=mysqli_query($link,$select);
	$rsUsuarios=mysqli_query($link,$select);
	$registros=(int)mysqli_num_rows($rsUsuarios);

	?>

<table cellspacing="0" border="0" width="900px">
  <tr>
    <td width="50%"><hr color="#666666" /></td>
    <td class="sanLR04" nowrap="nowrap"><b>Registros Encontrados: (
      <?=$registros;?>
      )</b></td>
    <td width="50%"><hr color="#666666" /></td>
  </tr>
</table>
<div style="display:block; top:0; left:0; width:965px; z-index=1; overflow: auto; height:400px;">
  <table cellspacing="0" border="0" width="900px">
    <thead>
      <tr class="thEncabezado">
        <td nowrap="nowrap" class="sanLR04" height="20" align="center">C</td>
        <td nowrap="nowrap" class="sanLR04" align="center">E</td>
        <td nowrap="nowrap" class="sanLR04">C&oacute;digo</td>
        <td nowrap="nowrap" class="sanLR04">Nombre</td>
        <td nowrap="nowrap" class="sanLR04" >Nombre Corto</td>
        <td nowrap="nowrap" class="sanLR04" >T. Salario</td>
        <td nowrap="nowrap" class="sanLR04" >T. Jornada</td>
        <td nowrap="nowrap" class="sanLR04" >T. Trabajador</td>
        <td nowrap="nowrap" class="sanLR04" >Causa de Baja</td>
        <td nowrap="nowrap" class="sanLR04" width="100%">Factor Integracion</td>
      </tr>
    </thead>
    <tbody>
      <?php $i=1; while($rTipoEntidad=mysqli_fetch_array($rsTiposEntidades,MYSQLI_ASSOC)){ ?>
      <tr>
        <td nowrap="nowrap" class="sanLR04" height="20" align="center"><input type="checkbox" name="eCodTipoEntidad<?=$i?>" id="eCodTipoEntidad<?=$i?>" value="<?=$rTipoEntidad{'eCodTipoEntidad'};?>" ></td>
        <td nowrap="nowrap" class="sanLR04" height="20" align="center"><img width="16" height="16" alt="Usuario" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-<?=$rTipoEntidad{'tCodEstatus'};?>.png"></td>
        <td nowrap="nowrap" class="sanLR04 colmenu"><a href="?ePagina=2.3.2.3.1.php&eCodTipoEntidad=<?=$rTipoEntidad{'eCodTipoEntidad'};?>"><b><?=sprintf("%07d",$rTipoEntidad{'eCodTipoEntidad'});?></b></a></td>
        <td nowrap="nowrap" class="sanLR04"><a class="txtCO12" href="?ePagina=2.3.2.3.2.php&eCodTipoEntidad=<?=$rTipoEntidad{'eCodTipoEntidad'};?>"><?=utf8_encode($rTipoEntidad{'tNombre'});?></a></td>
        <td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rTipoEntidad{'tNombreCorto'});?></td>
        <td nowrap="nowrap" class="sanLR04 "><?=utf8_encode($rTipoEntidad{'eCodTipoSalario'});?></td>
        <td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rTipoEntidad{'eCodTipoJornada'});?></td>
        <td nowrap="nowrap" class="sanLR04 "><?=utf8_encode($rTipoEntidad{'eCodTipoTrabajador'});?></td>
        <td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rTipoEntidad{'causa'});?></td>
        <td nowrap="nowrap" class="sanLR04 " width="100%"><?=utf8_encode($rTipoEntidad{'dFactorIntegracion'});?></td>
      </tr>
      <?php $i++; } ?>
    </tbody>
  </table>
</div>
<?php }else{ ?>
<script type="text/javascript">

	function nuevo(){

		document.location = "?ePagina=2.3.2.3.1.php";

	}



	function eliminar(){

		var eChk = 0;

		dojo.byId('eAccion').value = 1;	

		dojo.query("[id*=\"eCodTipoEntidad\"]:checked").forEach(function(nodo, index, array){eChk++;});

	

		if(eChk!=0){

			if(confirm("¿Desea eliminar los tipos de Empleados?")){

				dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+'.php', handleAs: "text", load: function(tRespuesta, ioArgs){

					dojo.byId('dvCNS').innerHTML = tRespuesta; 

					alert("¡Los registros se han eliminado exitosamente!");	

				}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});

			}

		}else{

			alert("No ha seleccionado ningun tipo de Empleado");

		}

		dojo.byId('eAccion').value = "";	

	}

	dojo.addOnLoad(function(){filtrarConsulta();});

	</script>
<form name="Datos" id="Datos" method="post" action="" onsubmit="return false;">
  <input type="hidden" value="0" name="ePagina" id="ePagina" />
  <input type="hidden" value="" name="eAccion" id="eAccion" />
  <table width="900px" border="0">
    <tr>
      <td colpan="3" width="100%"></td>
      <td align="right" nowrap="nowrap"><a class="fntBR11" href="javaScript:mostrarFiltros('Busqueda');">Busqueda de Registros</a></td>
    </tr>
    <tr id="trBusqueda" style="display:none">
      <td colspan="4"><table width="100%" bgcolor="#f9f9f9">
          <tr>
            <td class="sanLR04" height="5"></td>
          </tr>
          <tr>
            <td class="sanLR04" height="20">C&oacute;digo</td>
            <td class="sanLR04" width="50%"><input type="text" name="eCodTipoEntidad" dojoType="dijit.form.TextBox" id="eCodTipoEntidad" value="" style="width:80px"></td>
            <td class="sanLR04">Nombre</td>
            <td class="sanLR04" width="50%"><input type="text" name="tNombre" dojoType="dijit.form.TextBox" id="tNombre" value="" style="width:175px"></td>
          </tr>
          <tr>
            <td class="sanLR04" height="20">Nombre Corto</td>
            <td class="sanLR04" width="50%"><input type="text" name="tNombreCorto" dojoType="dijit.form.TextBox" id="tNombreCorto" value="" style="width:175px"></td>
            <td class="sanLR04"></td>
            <td class="sanLR04" width="50%"></td>
          </tr>
          <tr>
            <td class="sanLR04" height="5"></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td colspan="4"><div id="dvCNS"></div></td>
    </tr>
  </table>
</form>
<?php } ?>
