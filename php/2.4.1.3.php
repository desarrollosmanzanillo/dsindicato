<?php 
require_once("conexion/soluciones-mysql.php");
$link = getLink();
if($_POST){
	$arrDias[1]="Domingo";
	$arrDias[2]="Lunes";
	$arrDias[3]="Martes";
	$arrDias[4]="Mi&eacute;rcoles";
	$arrDias[5]="Jueves";
	$arrDias[6]="Viernes";
	$arrDias[7]="Sabado";
	date_default_timezone_set('America/Mexico_City');
	$fechaayer=isset($_POST["fecha"]) ? $_POST["fecha"] : date("Y-m-d");
	$fechaAsis=$fechaayer;
	$select=" 	SELECT empl.id, empl.Empleado, IFNULL(cv.dFactorIntegracion, ctipo.dFactorIntegracion) AS dFactorIntegracion,
					empl.descanso, cat.Categoria, cat.Turno1, empl.eCodEntidad, cee.tSiglas AS Empresa,
					(SELECT idEmpleado FROM asistencias WHERE Estado='AC' AND Fecha='".$fechaayer."' AND idEmpleado=empl.id LIMIT 1) AS bAsis
				FROM empleados empl
				INNER JOIN categorias cat ON cat.indice=empl.eCodCategoria
				INNER JOIN cattipoempleado ctipo ON ctipo.eCodTipoEntidad=empl.eCodTipo
				LEFT JOIN catentidades cee ON cee.eCodEntidad=empl.eCodEntidad
				LEFT JOIN catvacaciones cv ON cv.eCodVacaciones=CASE WHEN DATE_ADD(empl.fhFechaIngreso, INTERVAL ROUND(DATEDIFF('".$fechaayer."', empl.fhFechaIngreso)/365) YEAR)<CURRENT_DATE OR ROUND(DATEDIFF('".$fechaayer."', empl.fhFechaIngreso)/365)=0 THEN ROUND(DATEDIFF('".$fechaayer."', empl.fhFechaIngreso)/365)+1 ELSE ROUND(DATEDIFF('".$fechaayer."', empl.fhFechaIngreso)/365) END AND 1=empl.eCodTipo
				WHERE empl.eCodTipo=1 AND  empl.Estado='AC'
				AND empl.descanso=".(date("w",strtotime($fechaayer))+1)."
				AND empl.id NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."')
				AND empl.id NOT IN (SELECT eCodEmpleado FROM ausentismo WHERE fhFecha='".$fechaayer."' AND eCodEstatus='AC' AND eCodEmpleado=empl.id) 
				ORDER BY Empleado";
				//" AND empl.id NOT IN (SELECT idEmpleado FROM asistencias WHERE Estado='AC' AND Fecha='".$fechaayer."') ".
	$descansos=mysqli_query($link,$select);
	if($_POST['eAccion']==999){
		print "select: ".$select."<br>";
		print "fechaayer: ".$fechaayer."<br>";
		print "descanso: ".(date("w",strtotime($fechaayer))+1)."<br>";
	}
  	$registros=(int)mysqli_num_rows($descansos); ?>
<table cellspacing="0" border="0" width="980px">
	<tr>
		<td width="50%"><hr color="#666666" /></td>
		<td class="sanLR04" nowrap="nowrap"><b>Registros Encontrados: (<?=$registros;?>)</b></td>
		<td width="50%"><hr color="#666666" /></td>
	</tr>
</table>
<div style="display:block; top:0; left:0; width:965px; z-index=1; overflow: auto; height:400px;">
	<table cellspacing="0" border="0" width="100%">
		<thead>
			<tr class="thEncabezado">
					<td nowrap="nowrap" class="sanLR04">#</td>
					<td nowrap="nowrap" class="sanLR04" height="20" align="center"><input type="checkbox" id="actMarca" name="actMarca" onclick="Seleccionar();"></td>
					<td nowrap="nowrap" class="sanLR04">C&oacute;digo</td>
					<td nowrap="nowrap" class="sanLR04" colspan="2">Empleado</td>
				<td nowrap="nowrap" class="sanLR04">Categor&iacute;a</td>
				<td nowrap="nowrap" class="sanLR04" width="100%"></td>
			</tr>   
		</thead>
		<tbody>
		<?php
			if((int)$registros>0){
				$select=" SELECT *
								FROM sisconfiguracion ";
					$configura=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

					$eContador				= 1;
				$eDiaDescanso			= date("w",strtotime($fechaayer))+$eContador;
				$eDiaInicioLaboral= (int)$configura{'eDiaInicioSemana'};
				$vuelta=1;
				while($eDiaDescanso<>$eDiaInicioLaboral){
					//print $eDiaDescanso." (".$arrDias[$eDiaDescanso].") <>".$eDiaInicioLaboral." (".$arrDias[$eDiaInicioLaboral].")<br>";
					$eContador--;
					$fechaR=$fechaayer;
					$fechaayer = date('Y-m-d',strtotime('-1 days', strtotime($fechaR)));

					if($eContador==0){
						$eContador=7;
					}
					$eDiaDescanso = date("w",strtotime($fechaayer))+1;
					if($vuelta==10){
						print "..";
						die();
					}
					$vuelta++;
				}
				$fechaR=$fechaayer;
				$fechaayer = date('Y-m-d',strtotime('-1 days', strtotime($fechaR)));
				$fechaI = date('Y-m-d',strtotime('-6 days', strtotime($fechaayer)));
				//print "f: ".$eDiaDescanso." (".$arrDias[$eDiaDescanso].") <>".$eDiaInicioLaboral." (".$arrDias[$eDiaInicioLaboral].") de: ".$fechaI." a ".$fechaayer."<br>";
			}
			$NoID=1;
			while($rdescanso=mysqli_fetch_array($descansos,MYSQLI_ASSOC)){
				$EmplSuma 		=0;
				$EmplSumaS 		=0;
				$dAtrasEmpl 	=8;
				$fechaayerEmpl=$fechaI;
				$salario=0; ?>
				<tr>
					<td nowrap="nowrap" class="sanLR04 columnB" height="20" align="center"><?=$NoID;?></td>
					<td nowrap="nowrap" class="sanLR04" height="20" align="center">
						<input type="checkbox" name="chek<?=$rdescanso{'id'};?>" id="chek<?=$rdescanso{'id'};?>" value="<?=$rdescanso{'id'};?>" <?=((int)$_POST['chek'.$rdescanso{'id'}]>0 || (int)$rdescanso{'bAsis'}>0 ? "disabled" : "");?> >
					</td>
					<td nowrap="nowrap" class="sanLR04 columnB" align="right"><b><?=$rdescanso{'id'};?></b></td>                              
					<td nowrap="nowrap" class="sanLR04 columnB" colspan="2"><b>(<?=utf8_encode($rdescanso{'Empresa'});?>)</b> <?=utf8_encode($rdescanso{'Empleado'});?></td>
					<td nowrap="nowrap" class="sanLR04 columnB" colspan="2" width="100%"><?= utf8_encode($rdescanso{'Categoria'});?> <b>(<?=utf8_encode($rdescanso{'Empresa'});?>)</b></td>
				</tr>
				<?php
				while($dAtrasEmpl>1){
					$sDia=0;
					$domingo=(date("w",strtotime($fechaayerEmpl))+1);
					
					$select0=" SELECT asi.id, asi.Turno, IFNULL(cat1.Turno1, IFNULL(cat2.Turno2, cat3.Turno3)) AS Salario,
									IFNULL(cat1.Categoria, IFNULL(cat2.Categoria, cat3.Categoria)) AS Categoria
									FROM asistencias asi
									LEFT JOIN categorias cat1 ON cat1.indice=asi.idCategorias AND asi.Turno=1
									LEFT JOIN categorias cat2 ON cat2.indice=asi.idCategorias AND asi.Turno=2
									LEFT JOIN categorias cat3 ON cat3.indice=asi.idCategorias AND asi.Turno=3
									WHERE asi.Fecha='".$fechaayerEmpl."' AND asi.Estado='AC' AND asi.idEmpleado=".$rdescanso{'id'};
					$rsAsistencias=mysqli_query($link,$select0);
					while($rAsistencia=mysqli_fetch_array($rsAsistencias,MYSQLI_ASSOC)){
						$salario = (float)$rAsistencia{'Salario'};
						$descanso = (int)$rdescanso{'descanso'};
						$SalarioNeto=($descanso==$domingo && $domingo==1 //Día de Descanso y Domingo
								? ($salario+($salario*((float)$configura{'dPrimaDominical'}/100))+$salario)*(float)$configura{'dFactorDescanso'}
								: (float)$rAsistencia{'Salario'}*(float)$configura{'dFactorDescanso'}); //Normal
						$SalarioNetoS=($descanso==$domingo && $domingo==1 //Día de Descanso y Domingo
								? ($salario+($salario*((float)$configura{'dPrimaDominical'}/100))+$salario)
								: (float)$rAsistencia{'Salario'}); //Normal	
						$EmplSumaS+=$SalarioNetoS;
						//$EmplSuma+=$SalarioNeto; ?>
						<tr>
							<td nowrap="nowrap" class="sanLR04" height="20" colspan="2"> </td>
							<td nowrap="nowrap" class="sanLR04" colspan=""> <b><?=utf8_encode("".$arrDias[(date("w",strtotime($fechaayerEmpl))+1)]).": ";?></b> </td>
							<td nowrap="nowrap" class="sanLR04" > <?=date("d/m/Y", strtotime($fechaayerEmpl))." (<i>Turno ".$rAsistencia{'Turno'}."</i>) &nbsp;&nbsp;";?> </td>
							<td nowrap="nowrap" class="sanLR04" > <?=$rAsistencia{'Categoria'};?> </td>
							<td nowrap="nowrap" class="sanLR04"> C&aacute;lculo:
								<i><?=($descanso==$domingo && $domingo==1
											? "(".number_format($salario, 2)."+(".number_format($salario, 2)."*".((float)$configura{'dPrimaDominical'}/100).")+".$salario.")*".(float)$configura{'dFactorDescanso'} 
											: number_format((float)$rAsistencia{'Salario'}, 2)." <b>*</b> ".(float)$configura{'dFactorDescanso'});?></i>
							</td>
							<td nowrap="nowrap" class="sanLR04" colspan=""> = <i><b><?=number_format($SalarioNeto, 2);?></b></i></td>
						</tr>
						<tr style="display:none;">
							<td nowrap="nowrap" class="sanLR04" height="20" colspan="6"><textarea ><?=$select0;?></textarea></td>
						</tr>
						<?php 
					}
					//no domingo }
					$dAtrasEmpl--;
					$fechaayerEmpl = date('Y-m-d',strtotime('+1 days', strtotime($fechaayerEmpl)));
					//print "2.- ".$fechaayerEmpl."<br>";
				}
				$Previa=0;
				$EmplSuma=$rdescanso{'Turno1'};
				if((int)$_POST['chek'.$rdescanso{'id'}]>0){
					$select=" SELECT * 
									FROM asistencias 
									WHERE idEmpleado=".$rdescanso{'id'}." 
									AND idCategorias=".(int)$configura{'dCategoriaDescansos'}." 
									AND idEmpresa=".(int)$rdescanso{'eCodEntidad'}." 
									AND Fecha='".$fechaAsis."' 
									AND Estado='AC' ";
					$aPrevia=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
					if((int)$aPrevia{'id'}==0){
						$insert=" INSERT INTO asistencias (idEmpleado, idCategorias, idEmpresa, Turno, Salario, Fecha, fechaRegistro, idUsuario, Estado, dFactorIntegracion, eCodMotivoAsistencia)
										SELECT ".$rdescanso{'id'}.", ".(int)$configura{'dCategoriaDescansos'}.", ".(int)$rdescanso{'eCodEntidad'}.", 1, ".(float)$EmplSuma*(float)$rdescanso{'dFactorIntegracion'}.", '".$fechaAsis."', CURRENT_TIMESTAMP, ".
								(int)$_POST['eUsuario'].", 'AC', ".(float)$rdescanso{'dFactorIntegracion'}.", 6";
						mysqli_query($link,$insert);
					}else{
						$Previa=1;
					}
				} ?>
				<tr>
				<td nowrap="nowrap" class="sanLR04" colspan="2"></td>
				<td nowrap="nowrap" class="sanLR04" align="right"><?=utf8_encode("Salario:");?></td>
				<td nowrap="nowrap" class="sanLR04" colspan="4">
					<b><?=" ".number_format($EmplSuma,2)." * ".(float)$rdescanso{'dFactorIntegracion'}." = ".number_format($EmplSuma*(float)$rdescanso{'dFactorIntegracion'},2); ?><?=((int)$_POST['chek'.$rdescanso{'id'}]>0 || (int)$rdescanso{'bAsis'}>0 ? " -->Almacenado".($Previa==1 ? " previamente por otro usuario" : "") : "");?></b>
					<b>(<?=utf8_encode($rdescanso{'Empresa'});?>)</b>
				</td>
				</tr>
				<?php $i++; 
					$NoID++;
			} ?>
		</tbody>
	</table>
</div>
<?php }else{ ?>
<script type="text/javascript">
function consultar(){
  document.location = "?ePagina=2.5.1.1.php";
}

function Seleccionar(){
	if(dojo.byId('actMarca').checked){
		dojo.query("[id*=\"chek\"]").forEach(function(nodo, index, arr){  
		z = nodo.id.replace(/chek/, "");
		if(!dojo.byId("chek"+z).disabled)
			dojo.byId("chek"+z).checked=true;
		});
	}else{
		dojo.query("[id*=\"chek\"]").forEach(function(nodo, index, arr){  
		z = nodo.id.replace(/chek/, "");
			dojo.byId("chek"+z).checked=false;
		});
	}
}

function guardar(){
	var eChk = 0;
	var bandera = false;
	var mensaje = "¡Verifique lo siguiente!\n";
	dojo.byId('eAccion').value = 1;	
	dojo.query('input[id^=chek]:checked').forEach(function(nodo, index, arr){
		eChk++;
	});
	if(eChk==0){
		mensaje+="* Seleccione Empleado\n";
		bandera = true;  
	}
	if(bandera==true){
		alert(mensaje);
	}else{
		if(confirm("¿Desea registrar los registros?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+'.php', handleAs: "text", load: function(tRespuesta, ioArgs){
				dojo.byId('dvCNS').innerHTML = tRespuesta;
				alert("¡El Registro se realizo exitosamente!");				
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});
		}
	}
}
dojo.addOnLoad(function(){filtrarConsulta();});
</script>
<form name="Datos" id="Datos" method="post" action="" onsubmit="return false;">
	<input type="hidden" name="eUsuario" id="eUsuario" value="<?=(int)$_SESSION['sesionUsuario']['eCodUsuario'];?>" />
	<input type="hidden" value="0" name="ePagina" id="ePagina" />
	<input type="hidden" value="" name="eAccion" id="eAccion" />
	<table width="980px" border="0">
		<tr>
			<td colpan="3" width="100%"></td>
			<td align="right" nowrap="nowrap"><a class="fntBR11" href="javaScript:mostrarFiltros('Busqueda');">Busqueda de Registros</a></td>
		</tr>
		<tr id="trBusqueda" style="display:none">
			<td colspan="4">
				<table width="980px" bgcolor="#f9f9f9">
					<tr>
						<td class="sanLR04" height="5"></td>
					</tr>
					<tr>
						<td class="sanLR04" height="20">Fecha</td>
						<td class="sanLR04" width="50%"><input type="date" name="fecha" dojoType="dijit.form.TextBox" id="fecha" value="<?=$_POST["fecha"] ? $_POST["fecha"] : date("Y-m-d")?>" style="width:140px"></td>
						<td class="sanLR04"></td>
						<td class="sanLR04" width="50%"></td>
					</tr>
					<tr>
						<td class="sanLR04" height="5"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="4"><div id="dvCNS"></div></td>
		</tr>
	</table>
</form>
<?php } ?>