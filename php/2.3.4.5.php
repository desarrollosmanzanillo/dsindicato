<?php
require_once("conexion/soluciones-mysql.php"); 
$link = getLink();
if($_POST){
	if($_POST['eAccion']==1){
		foreach($_POST as $k => $valor){
			$nombre = strval($k);
			$campo = "eCodCiudad";
			if(strstr($nombre,$campo) && (int)$valor>0){
				$update=" UPDATE catciudades SET tCodEstatus='CA' WHERE eCodCiudad=".$valor;
				mysqli_query($link,$update);
			}
		}
	}

	$select=" SELECT cc.*, ce.tNombre AS Estado, cp.tNombre AS Pais ".
			" FROM catciudades cc ".
			" INNER JOIN catestados ce ON ce.eCodEstado=cc.eCodEstado ".
			" INNER JOIN catpaises cp ON cp.eCodPais=ce.eCodPais ".
			" WHERE 1=1".
			($_POST['eCodCiudad'] ? " AND eCodCiudad=".$_POST['eCodCiudad'] : "").
			($_POST['tNombre'] ? " AND tNombre LIKE '%".$_POST['tNombre']."%'" : "").
			($_POST['tNombreCorto'] ? " AND tNombreCorto LIKE '%".$_POST['tNombreCorto']."%'" : "");	
	$rsCiudades=mysqli_query($link,$select);
	$rsUsuarios=mysqli_query($link,$select);
	$registros=(int)mysqli_num_rows($rsUsuarios);	
	?>
    <table cellspacing="0" border="0" width="900px">
    <tr>
    <td width="50%"><hr color="#666666" /></td>
    <td class="sanLR04" nowrap="nowrap"><b>Registros Encontrados: ( <?=$registros;?> )</b></td>
	<td width="50%"><hr color="#666666" /></td>
    </tr>
    </table>
	<div style="display:block; top:0; left:0; width:900px; z-index=1; overflow: auto;">
	<table cellspacing="0" border="0" width="900px">
		<thead>
			<tr class="thEncabezado">
				<td nowrap="nowrap" class="sanLR04" height="20" align="center">C</td>
				<td nowrap="nowrap" class="sanLR04" align="center">E</td>
				<td nowrap="nowrap" class="sanLR04">C&oacute;digo</td>
				<td nowrap="nowrap" class="sanLR04">Nombre</td>
				<td nowrap="nowrap" class="sanLR04">Nombre Corto</td>
				<td nowrap="nowrap" class="sanLR04">Pa&iacute;s</td>
				<td nowrap="nowrap" class="sanLR04" width="100%">Estado</td>
			</tr>
		</thead>
		<tbody>
			<?php $i=1; while($rCiudad=mysqli_fetch_array($rsCiudades,MYSQLI_ASSOC)){ ?>
				<tr>
					<td nowrap="nowrap" class="sanLR04" height="20" align="center"><input type="checkbox" name="eCodCiudad<?=$i?>" id="eCodCiudad<?=$i?>" value="<?=$rCiudad{'eCodCiudad'};?>" ></td>
					<td nowrap="nowrap" class="sanLR04" height="20" align="center"><img width="16" height="16" alt="Usuario" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-<?=$rCiudad{'tCodEstatus'};?>.png"></td>
					<td nowrap="nowrap" class="sanLR04 colmenu"><a href="?ePagina=2.3.4.5.1.php&eCodCiudad=<?=$rCiudad{'eCodCiudad'};?>"><b><?=sprintf("%07d",$rCiudad{'eCodCiudad'});?></b></a></td>
					<td nowrap="nowrap" class="sanLR04"><a class="txtCO12" href="?ePagina=2.3.4.5.2.php&eCodCiudad=<?=$rCiudad{'eCodCiudad'};?>"><?=utf8_encode($rCiudad{'tNombre'});?></a></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rCiudad{'tNombreCorto'});?></td>
					<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rCiudad{'Pais'});?></td>
					<td nowrap="nowrap" class="sanLR04 columnB" width="100%"><?=utf8_encode($rCiudad{'Estado'});?></td>
				</tr>
			<?php $i++; } ?>
		</tbody>
	</table>
	</div>
<?php }else{ ?>
	<script type="text/javascript">
	function nuevo(){
		document.location = "?ePagina=2.3.4.5.1.php";
	}

	function eliminar(){
		var eChk = 0;
		dojo.byId('eAccion').value = 1;	
		dojo.query("[id*=\"eCodCiudad\"]:checked").forEach(function(nodo, index, array){eChk++;});
	
		if(eChk!=0){
			if(confirm("¿Desea eliminar las ciudades?")){
				dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+'.php', handleAs: "text", load: function(tRespuesta, ioArgs){
					dojo.byId('dvCNS').innerHTML = tRespuesta; 
					alert("¡Los registros se han eliminado exitosamente!");	
				}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});
			}
		}else{
			alert("No ha seleccionado ningún estado");
		}
		dojo.byId('eAccion').value = "";	
	}
	dojo.addOnLoad(function(){filtrarConsulta();});
	</script>
	<form name="Datos" id="Datos" method="post" action="" onsubmit="return false;">
		<input type="hidden" value="0" name="ePagina" id="ePagina" />
		<input type="hidden" value="" name="eAccion" id="eAccion" />
		<table width="900px" border="0">
			<tr>
				<td colpan="3" width="100%"></td>
				<td align="right" nowrap="nowrap"><a class="fntBR11" href="javaScript:mostrarFiltros('Busqueda');">Busqueda de Registros</a></td>
			</tr>
			<tr id="trBusqueda" style="display:none">
				<td colspan="4">
					<table width="100%" bgcolor="#f9f9f9">
						<tr><td class="sanLR04" height="5"></td></tr>
						<tr>
							<td class="sanLR04" height="20">C&oacute;digo</td>
							<td class="sanLR04" width="50%"><input type="text" name="eCodCiudad" dojoType="dijit.form.TextBox" id="eCodCiudad" value="" style="width:80px"></td>
							<td class="sanLR04">Nombre</td>
							<td class="sanLR04" width="50%"><input type="text" name="tNombre" dojoType="dijit.form.TextBox" id="tNombre" value="" style="width:175px"></td>
						</tr>
						<tr>
							<td class="sanLR04" height="20" nowrap>Nombre Corto</td>
							<td class="sanLR04" width="50%"><input type="text" name="tNombreCorto" dojoType="dijit.form.TextBox" id="tNombreCorto" value="" style="width:175px"></td>
							<td class="sanLR04"></td>
							<td class="sanLR04" width="50%"></td>
						</tr>
						<tr><td class="sanLR04" height="5"></td></tr>
					</table>
				</td>
			</tr>
			<tr><td colspan="4"><div id="dvCNS"></div></td></tr>
		</table>
	</form>
<?php } ?>