<style>
#divgral {
     overflow:scroll;
     height:450px;
     width:980px;
}
</style>
<?php require_once("conexion/soluciones-mysql.php"); 
$link = getLink();
if($_POST){
	if ($_POST['fhFechaAsistenciaInicio']!=""){
  		$fhFechaAsistenciaInicio = $_POST['fhFechaAsistenciaInicio'].' 00:00:00';
  		$fhFechaAsistenciaFin = ($_POST['fhFechaAsistenciaFin']!="" ? $_POST['fhFechaAsistenciaFin'] : $_POST['fhFechaAsistenciaInicio']).' 23:59:59';
 	}
	$select=" SELECT DISTINCT asis.idEmpleado AS numeral, mov.eCodEstatus AS Estado, mov.tCodTipoMovimiento,
			mov.fhFecha AS Fecha, asis.idEmpleado, mov.dImporte AS Salario, mov.tFolio, ent.tSiglas
			FROM asistenciaschecadoras asis
			INNER JOIN relasistenciamovimientoschecadoras ram ON ram.eCodAsistencia=asis.id
			INNER JOIN movimientosafiliatorioschecadoras mov ON mov.eCodMovimiento=ram.eCodMovimiento
			LEFT JOIN catentidades ent ON ent.eCodEntidad=asis.idEmpresa
			WHERE asis.Estado='AC' AND mov.tFolio IS NOT NULL AND mov.tCodTipoMovimiento NOT IN ('01','02') ".
			($_POST['eCodEntidad']				? " AND asis.idEmpresa=".$_POST['eCodEntidad'] : "").
			($_POST['tFolio']					? " AND mov.tFolio='".$_POST['tFolio']."'" : "").
			($_POST['eCodIncidencia']			? " AND asis.idEmpleado=".$_POST['eCodIncidencia'] : "").
			($_POST['fhFechaAsistenciaInicio']	? " AND mov.fhFecha BETWEEN '".$fhFechaAsistenciaInicio. "' AND '" .$fhFechaAsistenciaFin."'" : "" ).
			" Order by mov.fhFecha ".($_POST['orden'] ? $_POST['orden'] : "DESC").", mov.tCodTipoMovimiento DESC, asis.idEmpleado DESC ".
			" LIMIT ".($_POST['limite'] ? $_POST['limite'] : "100");
	$rsMovimientos=mysqli_query($link,$select); 
	if($_POST['eAccion']==999){
		print $select."<br>";
	}
	//
	$select=" SELECT DISTINCT asis.idEmpleado AS numeral, mov.eCodEstatus AS Estado, mov.tCodTipoMovimiento,
			mov.fhFecha AS Fecha, asis.idEmpleado, mov.dImporte AS Salario, mov.tFolio
			FROM asistenciaschecadoras asis
			INNER JOIN relasistenciamovimientoschecadoras ram ON ram.eCodAsistencia=asis.id
			INNER JOIN movimientosafiliatorioschecadoras mov ON mov.eCodMovimiento=ram.eCodMovimiento
			WHERE asis.Estado='AC' AND mov.tFolio IS NOT NULL AND mov.tCodTipoMovimiento='02' ".
			($_POST['eCodEntidad']				? " AND asis.idEmpresa=".$_POST['eCodEntidad'] : "").
			($_POST['tFolio']					? " AND mov.tFolio='".$_POST['tFolio']."'" : "").
			($_POST['eCodIncidencia']			? " AND asis.idEmpleado=".$_POST['eCodIncidencia'] : "").
			($_POST['fhFechaAsistenciaInicio']	? " AND mov.fhFecha BETWEEN '".$fhFechaAsistenciaInicio. "' AND '" .$fhFechaAsistenciaFin."'" : "" ).
			" Order by mov.fhFecha ".($_POST['orden'] ? $_POST['orden'] : "DESC").
			" LIMIT ".($_POST['limite'] ? $_POST['limite'] : "100");
	$rsBajas=mysqli_query($link,$select);
	if($_POST['eAccion']==999){
		print $select."<br>";
	}
	$registros=(int)mysqli_num_rows($rsMovimientos)+(int)mysqli_num_rows($rsBajas); ?>
	<input type="hidden" value="<?=$_POST['orden'];?>" name="tOrden" id="tOrden" />
    <table cellspacing="0" border="0" width="980px">
    <tr>
    <td width="50%"><hr color="#666666" /></td>
    <td class="sanLR04" nowrap="nowrap"><b>Registros Encontrados: ( <?=$registros;?> )</b></td>
	<td width="50%"><hr color="#666666" /></td>
    </tr>
    </table>
	<div id="divgral" style="display:block; top:0; left:0; width:980px; z-index=1; overflow: auto;">
	<table cellspacing="0" border="0" width="980px" >		
		<thead>
			<tr class="thEncabezado">				
				<td nowrap="nowrap" class="sanLR04">E</td>
				<td nowrap="nowrap" class="sanLR04">Empleado</td>
				<td nowrap="nowrap" class="sanLR04">Movimiento</td>
                <td nowrap="nowrap" class="sanLR04">Fecha</td>
                <td nowrap="nowrap" class="sanLR04">Diario</td>
                <td nowrap="nowrap" class="sanLR04">Variable</td>
				<td nowrap="nowrap" class="sanLR04">Integrado</td>
				<td nowrap="nowrap" class="sanLR04">Folio</td>
				<td nowrap="nowrap" class="sanLR04" width="100%">Empresa</td>
			</tr>
		</thead>
		<tbody>
			<?php $i=1; while($rMovimiento=mysqli_fetch_array($rsMovimientos,MYSQLI_ASSOC)){ ?>
				<tr>					
					<td nowrap="nowrap" class="sanLR04"><img width="16" height="16" alt="Usuario" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-<?=$rMovimiento{'Estado'};?>.png"></td>
					<td nowrap="nowrap" class="sanLR04 colmenu" ><b>7<?=sprintf("%04d",$rMovimiento{'idEmpleado'});?></b></td>
					<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rMovimiento{'tCodTipoMovimiento'});?></a></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=date("d/m/Y", strtotime($rMovimiento{'Fecha'}));?></td>
					<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rMovimiento{'Salario'});?></a></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode("0.00");?></a></td>
                    <td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rMovimiento{'Salario'});?></a></td>
                    <td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rMovimiento{'tFolio'});?></a></td>
                    <td nowrap="nowrap" class="sanLR04" width="100%"><?=utf8_encode($rMovimiento{'tSiglas'});?></a></td>
				</tr>
			<?php $i++; } ?>
			<?php $i=1; while($rBaja=mysqli_fetch_array($rsBajas,MYSQLI_ASSOC)){ ?>
				<tr>					
					<td nowrap="nowrap" class="sanLR04"><img width="16" height="16" alt="Usuario" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-<?=$rBaja{'Estado'};?>.png"></td>
					<td nowrap="nowrap" class="sanLR04 colmenu" ><b>7<?=sprintf("%04d",$rBaja{'idEmpleado'});?></b></td>
					<td nowrap="nowrap" class="sanLR04"><?=utf8_encode("02");?></a></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=date("d/m/Y", strtotime($rBaja{'Fecha'}));?></td>
					<td nowrap="nowrap" class="sanLR04"><?=utf8_encode("0.00");?></a></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode("0.00");?></a></td>
                    <td nowrap="nowrap" class="sanLR04"><?=utf8_encode("0.00");?></a></td>
                    <td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rBaja{'tFolio'});?></a></td>
                    <td nowrap="nowrap" class="sanLR04" width="100%"><?=utf8_encode($rMovimiento{'tSiglas'});?></a></td>
				</tr>
			<?php $i++; } ?>
		</tbody>
	</table>
	</div>
<?php }else{ 
$select=" SELECT tSiglas, eCodEntidad
        FROM catentidades
        WHERE bPrincipal IS NOT NULL
        ORDER BY tSiglas ASC ";
$rsEmpresas=mysqli_query($link,$select); ?>
<script type="text/javascript">
dojo.require("dojo.number");
dojo.require("dijit.form.NumberTextBox");
dojo.require("dijit.form.DateTextBox");
dojo.require("dojo.io.iframe");

function generaExcel(){
	var UrlExcel = "php/excel/2.5.3.6.php?"+
	(dojo.byId('tFolio').value ? "&tFolio="+dojo.byId('tFolio').value : "")+
	(dojo.byId('eCodIncidencia').value ? "&eCodIncidencia="+dojo.byId('eCodIncidencia').value : "")+
	(dojo.byId('fhFechaAsistenciaInicio').value ? "&fhFechaAsistenciaInicio="+dojo.byId('fhFechaAsistenciaInicio').value : "")+
	(dojo.byId('fhFechaAsistenciaFin').value ? "&fhFechaAsistenciaFin="+dojo.byId('fhFechaAsistenciaFin').value : "")+
	(dojo.byId('eCodEntidad').value ? "&eCodEntidad="+dojo.byId('eCodEntidad').value : "")+
	(dojo.byId('tOrden').value ? "&orden="+dojo.byId('tOrden').value : "")+
	(dojo.byId('eAccion').value ? "&eAccion="+dojo.byId('eAccion').value : "")+
	(dojo.byId('limite').value ? "&limite="+dojo.byId('limite').value : "");		
	var Datos = document.createElement("FORM");
	document.body.appendChild(Datos);
	Datos.name='Datos';
	Datos.method = "POST";
	Datos.action = UrlExcel;
	Datos.submit();
}

dojo.addOnLoad(function(){ filtrarConsulta();});
</script>
<form name="Datos" id="Datos" method="post" action="" onsubmit="return false;">
	<input type="hidden" value="0" name="ePagina" id="ePagina" />
	<input type="hidden" value="" name="eAccion" id="eAccion" />
	<table width="980px" border="0">
		<tr>
			<td colpan="3" width="100%"></td>
			<td align="right" nowrap="nowrap"><a class="fntBR11" href="javaScript:mostrarFiltros('Busqueda');">Busqueda de Registros</a></td>
		</tr>
		<tr id="trBusqueda" style="display:none">
			<td colspan="4">
				<table width="100%" bgcolor="#f9f9f9">
					<tr><td class="sanLR04" height="5"></td></tr>
					<tr>
						<td class="sanLR04" height="20">C&oacute;digo</td>
						<td class="sanLR04" width="50%"><input type="text" name="eCodIncidencia" dojoType="dijit.form.TextBox" id="eCodIncidencia" value="" style="width:80px; height:25px;"></td>
						<td class="sanLR04" nowrap="nowrap">Fecha</td>
						<td class="sanLR04" width="50%" nowrap="nowrap"><input name="fhFechaAsistenciaInicio" id="fhFechaAsistenciaInicio" type="date" required="false"/> - <input name="fhFechaAsistenciaFin" id="fhFechaAsistenciaFin" type="date" required="false"/></td>
					</tr>
					<tr>
						<td class="sanLR04" height="20">Folio</td>
						<td class="sanLR04" width="50%"><input type="text" name="tFolio" dojoType="dijit.form.TextBox" id="tFolio" style="width:80px; height:25px;"></td>
						<td class="sanLR04" height="20" nowrap>Empresa</td>
			            <td class="sanLR04" width="50%">
			              <select name="eCodEntidad" id="eCodEntidad" style="width:180px; height:25px;">
			                  <option value="">Seleccione...</option>
			                  <?php while($rEmpresa=mysqli_fetch_array($rsEmpresas,MYSQLI_ASSOC)){ ?>
			                      <option value="<?=$rEmpresa{'eCodEntidad'}?>"><?=utf8_encode($rEmpresa{'tSiglas'})?></option>
			                  <?php } ?>
			              </select>
			            </td>
					</tr>
			        <tr>
		              <td nowrap="nowrap" class="sanLR04" height="5"><b>[Ordenamiento]</b></td>
		              <td nowrap="nowrap" class="sanLR04" width="50%"><input type="radio" name="orden" id="orden" value="ASC"> ASCENDENTE / <input type="radio" name="orden" id="orden" value="DESC" checked="checked"> DESCENDENTE</td>
		              <td nowrap="nowrap" class="sanLR04"><b>[Maximos Registros]:</b></td>
		              <td nowrap="nowrap" class="sanLR04" width="50%">
		              	<select name="limite" id="limite" style="width:175px">
                              <option value="100">100</option>
                              <option value="1000">1,000</option>
                              <option value="10000">10,000</option>
                              <option value="100000">100,000</option>
                      	</select>
                      </td>
        			</tr>
					<tr><td class="sanLR04" height="5"></td></tr>
				</table>
			</td>
		</tr>
		<tr><td colspan="4"><div id="dvCNS"></div></td></tr>
	</table>
</form>
<?php } ?>