<?php
require_once("conexion/soluciones-mysql.php"); 
$link = getLink();
if($_POST){
	if($_POST['eAccion']==1){
		foreach($_POST as $k => $valor){
			$nombre = strval($k);
			$campo = "eCodCategoria";
			if(strstr($nombre,$campo) && (int)$valor>0){
				$update=" UPDATE categorias SET tCodEstatus='CA' WHERE indice=".$valor;
				mysqli_query($link,$update);
			}
		}
	}

	$select=" SELECT cc.*, ct.eCodEntidad , ct.tNombre AS Empresa ".
			" FROM categorias cc ".
			" INNER JOIN catentidades ct ON ct.eCodEntidad=cc.idEmpresa ".			
			" WHERE 1=1".
			($_POST['indice'] ? " AND cc.indice=".$_POST['indice'] : "").			
			($_POST['clave'] ? " AND cc.id LIKE '%".$_POST['clave']."%'" : "").
			($_POST['tCategoria'] ? " AND cc.Categoria LIKE '%".$_POST['tCategoria']."%'" : "");
	$rsComprobantes=mysqli_query($link,$select);
	$registros=(int)mysqli_num_rows($rsComprobantes);	
	?>
    <table cellspacing="0" border="0" width="900px">
    <tr>
    <td width="50%"><hr color="#666666" /></td>
    <td class="sanLR04" nowrap="nowrap"><b>Registros Encontrados: ( <?=$registros;?> )</b></td>
	<td width="50%"><hr color="#666666" /></td>
    </tr>
    </table>
	<div style="display:block; top:0; left:0; width:900px; z-index=1; overflow: auto; height:400px;">
	<table cellspacing="0" border="0" width="900px">
		<thead>
			<tr class="thEncabezado">
				<td nowrap="nowrap" class="sanLR04" height="20" align="center">C</td>
                <td nowrap="nowrap" class="sanLR04" align="center">E</td>
				<td nowrap="nowrap" class="sanLR04" >C&oacute;digo</td>
                <td nowrap="nowrap" class="sanLR04" >Categoria</td>
                <td nowrap="nowrap" class="sanLR04" >Clave</td>
                <td nowrap="nowrap" class="sanLR04" align="right" title="Base Sindicalizado">Turno 1</td>
                <td nowrap="nowrap" class="sanLR04" align="right" title="Base Sindicalizado">Turno 2</td>
                <td nowrap="nowrap" class="sanLR04" align="right" title="Base Sindicalizado">Turno 3</td>
                <td nowrap="nowrap" class="sanLR04" align="right" title="Eventual Sindicalizado">Turno E. 1</td>
                <td nowrap="nowrap" class="sanLR04" align="right" title="Eventual Sindicalizado">Turno E. 2</td>
                <td nowrap="nowrap" class="sanLR04" align="right" title="Eventual Sindicalizado">Turno E. 3</td>
                <td nowrap="nowrap" class="sanLR04" width="100%" ></td>
            </tr>
		</thead>
		<tbody>
			<?php $i=1; while($rComprobante=mysqli_fetch_array($rsComprobantes,MYSQLI_ASSOC)){ ?>
				<tr>
					<td nowrap="nowrap" class="sanLR04" height="20" align="center"><input type="checkbox" name="eCodCategoria<?=$i?>" id="eCodCategoria<?=$i?>" value="<?=$rComprobante{'indice'};?>" ></td>
                    <td nowrap="nowrap" class="sanLR04" height="20" align="center"><img width="16" alt="Facturaci&oacute;n" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-<?=$rComprobante{'tCodEstatus'};?>.png"></td>
					<td nowrap="nowrap" class="sanLR04 colmenu"><a href="?ePagina=2.3.6.1.1.php&eCodCategoria=<?=$rComprobante{'indice'};?>"><b><?=sprintf("%07d",$rComprobante{'indice'});?></b></a></td>                    
					<td nowrap="nowrap" class="sanLR04 columnB"><a class="txtCO12" href="?ePagina=2.3.6.1.2.php&eCodCategoria=<?=$rComprobante{'indice'};?>"><?=utf8_encode($rComprobante{'Categoria'});?></a></td>
					<td nowrap="nowrap" class="sanLR04" ><?=utf8_encode($rComprobante{'id'});?></td>
					<td nowrap="nowrap" class="sanLR04 columnB" align="right" title="Base Sindicalizado"><?=utf8_encode(number_format($rComprobante{'Turno1'},2));?></td>
					<td nowrap="nowrap" class="sanLR04" align="right" title="Base Sindicalizado"><?=utf8_encode(number_format($rComprobante{'Turno2'},2));?></td>
					<td nowrap="nowrap" class="sanLR04 columnB" align="right" title="Base Sindicalizado"><?=utf8_encode(number_format($rComprobante{'Turno3'},2));?></td>
					<td nowrap="nowrap" class="sanLR04" align="right" title="Eventual Sindicalizado"><?=utf8_encode(number_format($rComprobante{'TurnoE1'},2));?></td>
					<td nowrap="nowrap" class="sanLR04 columnB" align="right" title="Eventual Sindicalizado"><?=utf8_encode(number_format($rComprobante{'TurnoE2'},2));?></td>
					<td nowrap="nowrap" class="sanLR04" align="right" title="Eventual Sindicalizado"><?=utf8_encode(number_format($rComprobante{'TurnoE3'},2));?></td>
                    <td nowrap="nowrap" class="sanLR04 columnB" width="100%"></td>
				</tr>
			<?php $i++; } ?>
		</tbody>
	</table>
	</div>
<?php }else{ ?>
	<script type="text/javascript">
	function nuevo(){
		document.location = "?ePagina=2.3.6.1.1.php";
	}

	function eliminar(){
		var eChk = 0;
		dojo.byId('eAccion').value = 1;	
		dojo.query("[id*=\"eCodCategoria\"]:checked").forEach(function(nodo, index, array){eChk++;});
	
		if(eChk!=0){
			if(confirm("¿Desea eliminar los comprobantes?")){
				dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+'.php', handleAs: "text", load: function(tRespuesta, ioArgs){
					dojo.byId('dvCNS').innerHTML = tRespuesta; 
					alert("¡Los registros se han eliminado exitosamente!");	
				}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});
			}
		}else{
			alert("No ha seleccionado ningún comprobante");
		}
		dojo.byId('eAccion').value = "";	
	}

	function generaExcel(){
		var UrlExcel = "php/excel/2.3.6.1.php?"+		
		(dojo.byId('indice').value ? "&indice="+dojo.byId('indice').value : "")+
		(dojo.byId('clave').value ? "&clave="+dojo.byId('clave').value : "")+
		(dojo.byId('tCategoria').value ? "&tCategoria="+dojo.byId('tCategoria').value : "");		
		var Datos = document.createElement("FORM");
		document.body.appendChild(Datos);
		Datos.name='Datos';
		Datos.method = "POST";
		Datos.action = UrlExcel;
		Datos.submit();
		}

	dojo.addOnLoad(function(){filtrarConsulta();});
	</script>
	<form name="Datos" id="Datos" method="post" action="" onsubmit="return false;">
		<input type="hidden" value="0" name="ePagina" id="ePagina" />
		<input type="hidden" value="" name="eAccion" id="eAccion" />
		<table width="900px" border="0">
			<tr>
				<td colpan="3" width="100%"></td>
				<td align="right" nowrap="nowrap"><a class="fntBR11" href="javaScript:mostrarFiltros('Busqueda');">Busqueda de Registros</a></td>
			</tr>
			<tr id="trBusqueda" style="display:none">
				<td colspan="4">
					<table width="100%" bgcolor="#f9f9f9">
						<tr><td class="sanLR04" height="5"></td></tr>
						<tr>
							<td class="sanLR04" height="20">C&oacute;digo</td>
							<td class="sanLR04" width="50%"><input type="text" name="indice" dojoType="dijit.form.TextBox" id="indice" value="" style="width:80px"></td>
							<td class="sanLR04">Categoria</td>
							<td class="sanLR04" width="50%"><input type="text" name="tCategoria" dojoType="dijit.form.TextBox" id="tCategoria" value="" style="width:175px"></td>
						</tr>
						<tr>
							<td class="sanLR04" height="20">Clave</td>
							<td class="sanLR04" width="50%"><input type="text" name="clave" dojoType="dijit.form.TextBox" id="clave" value="" style="width:80px"></td>
							<td class="sanLR04"></td>
							<td class="sanLR04" width="50%"></td>
						</tr>
						<tr><td class="sanLR04" height="5"></td></tr>
					</table>
				</td>
			</tr>
			<tr><td colspan="4"><div id="dvCNS"></div></td></tr>
		</table>
	</form>
<?php } ?>