<td></td>
<div id="intro">
		<div class="pikachoose">
			<ul id="pikame" >
				<li><a href="#"><img src="images/C-121.jpg" width="900" height="280" alt="picture"/></a><span>¿Quienes Somos?</span></li>				
			</ul>
		</div>
	</div>
<div class="holder_content" align="center">
  <section>
  <td></td>
    <h3 align="center">¿QUIENES SOMOS?</h3>
    <p align="justify">
    <strong>&quot;SOLUCIONES DE ALMACENAMIENTO Y TRANSPORTE S.A. DE C.V&quot;</strong>. Somos una  empresa Mexicana especializada en el manejo de todo tipo de contenedores DRY CARGO, REEFERS, ISO  TANQUES,OPEN-TOP, FLAT RACK; Operamos carga general,  Contamos  con modernos equipos y tecnología avanzada que nos permiten administrar las  necesidades logísticas y de información requeridas por las compañías navieras y clientes en los servicios de  almacenaje, mantenimiento y reparación de  contenedores, Además de contar con  una red computacional que mantiene en línea a las distintas  terminales y clientes, facilitando su acceso con una  entrega ágil y oportuna  de la información.</p>
    <td></td>
    <p align="justify">
    Contamos con equipo Especializado para la descarga  de Contenedores Llenos y vacíos, tenemos unidades de lavado de alta presión para realizar el servicio de lavado de todo tipo de contenedores utilizando productos biodegradables, Además, contamos con taller para la reparación de contenedores, estos servicios se realizan de acuerdo a las normas del IICL y UCIRC.
    </p>
    <td></td>
    <p align="justify"> Nuestro corporativo cuenta  con una  línea transportista con un  parque vehicular de 60 unidades con Chasises y plataformas, con permiso para  cargas peligrosas, para la carga de contenedores y carga en general ,  dicha línea hermana es <strong>&quot;TRANPORTES JOSÉ GUAJARDO  S.A. DE CV&quot;</strong>. Empresa mexicana certificada ISO9001 ,con 23 años de experiencia en  el transporte ,fundada en 1990 por el Sr. José Guajardo.</p>
  </section>
</div>
