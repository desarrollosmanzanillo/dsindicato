<?php require_once("conexion/soluciones-mysql.php");
$link = getLink();
if($_POST){

	if ($_POST['fhFechaVacacionInicio']!=""){
  		$fhFechaVacacionInicio = $_POST['fhFechaVacacionInicio'].' 00:00:00';
  		$fhFechaVacacionFin = ($_POST['fhFechaVacacionFin']!="" ? $_POST['fhFechaVacacionFin'] : $_POST['fhFechaVacacionInicio']).' 23:59:59';
 	}
	$select=" SELECT bv.tCodEstatus, bv.eCodVacacion, bv.fhFecha, bv.eCodEmpleado, emp.Empleado, 
			CONCAT_WS(' ', usr.tNombre, usr.tApellidos) AS UsuarioR, bv.fhFechaRegistro,
			CONCAT_WS(' ', usc.tNombre, usc.tApellidos) AS UsuarioC, bv.fhFechaCancelacion, 
			bv.tMotivoCancelacion, ce.tNombre AS Estatus, cd.tNombre AS tDiaDescanso
			FROM bitvacaciones bv
			LEFT JOIN empleados emp ON emp.id=bv.eCodEmpleado
			LEFT JOIN catestatus ce ON ce.tCodEstatus=bv.tCodEstatus
			LEFT JOIN catusuarios usr ON usr.eCodUsuario=bv.eCodUsuario
			LEFT JOIN catusuarios usc ON usc.eCodUsuario=bv.eCodUsuarioCancelacion
			LEFT JOIN catdias cd ON cd.eCodDia=bv.eCodDiaDescanso
			WHERE 1=1 ".
			($_POST['Empleado'] 				? " AND (emp.Empleado like '%".utf8_decode($_POST['Empleado'])."%' OR bv.eCodEmpleado=".(int)$_POST['Empleado'].")" : "").
			($_POST['tCodEstatus'] 				? " AND bv.tCodEstatus='".$_POST['tCodEstatus']."'"			: "").
			($_POST['eCodVacacion'] 			? " AND bv.eCodVacacion=".(int)$_POST['eCodVacacion'] 																: "").
			($_POST['fhFechaVacacionInicio']	? " AND bv.fhFecha BETWEEN '".$fhFechaVacacionInicio."' AND '".$fhFechaVacacionFin."'" 							: "").
			" ORDER BY bv.eCodVacacion ".($_POST['orden'] ? $_POST['orden'] : "DESC" ).
			" LIMIT ".($_POST['limite'] ? $_POST['limite'] : "1000");
	$rsVacaciones=mysqli_query($link,$select);
	$rsUsuarios=mysqli_query($link,$select);
	$registros=(int)mysqli_num_rows($rsUsuarios); ?>
    <table cellspacing="0" border="0" width="965px">
		<tr>
			<td width="50%"><hr color="#666666" /></td>
			<td class="sanLR04" nowrap="nowrap"><b>Registros Encontrados: ( <?=$registros;?> )</b></td>
			<td width="50%"><hr color="#666666" /></td>
		</tr>
    </table>
	<div style="display:block; top:0; left:0; width:965px; z-index=1; overflow: auto; height:400px;">
	<table cellspacing="0" border="0" width="965px">
		<thead>
			<tr class="thEncabezado">
				<td nowrap="nowrap" class="sanLR04" height="20" align="center">E</td>
				<td nowrap="nowrap" class="sanLR04">C&oacute;digo</td>
				<td nowrap="nowrap" class="sanLR04">Fecha</td>
				<td nowrap="nowrap" class="sanLR04" >#</td>
				<td nowrap="nowrap" class="sanLR04" >Empleado</td>
				<td nowrap="nowrap" class="sanLR04" title="Día de Descanso Registrado">DDR</td>
				<td nowrap="nowrap" class="sanLR04" >Usuario Registro</td>
				<td nowrap="nowrap" class="sanLR04" width="100%">F. Registro</td>
				<td nowrap="nowrap" class="sanLR04" >Usuario Cancelación</td>
				<td nowrap="nowrap" class="sanLR04" width="100%">F. Cancelación</td>
				<td nowrap="nowrap" class="sanLR04" >M. Cancelación</td>
			</tr>
		</thead>
		<tbody>
			<?php $i=1; while($rVacacion=mysqli_fetch_array($rsVacaciones, MYSQLI_ASSOC)){ ?>
				<tr>
					<td nowrap="nowrap" class="sanLR04" height="20" align="center"><img width="16" height="16" alt="Empresa" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-<?=$rVacacion{'tCodEstatus'};?>.png" title="<?=$rVacacion{'Estatus'};?>"></td>
					<td nowrap="nowrap" class="sanLR04 colmenu"><b><?=sprintf("%07d",$rVacacion{'eCodVacacion'});?></b></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=date("d/m/Y", strtotime($rVacacion{'fhFecha'}));?></td>
					<td nowrap="nowrap" class="sanLR04 "><?=utf8_encode($rVacacion{'eCodEmpleado'});?></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rVacacion{'Empleado'});?></td>
					<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rVacacion{'tDiaDescanso'});?></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rVacacion{'UsuarioR'});?></td>
					<td nowrap="nowrap" class="sanLR04"><?=date("d/m/Y H:i", strtotime($rVacacion{'fhFechaRegistro'}));?></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rVacacion{'UsuarioC'});?></td>
					<td nowrap="nowrap" class="sanLR04"><?=($rVacacion{'fhFechaCancelacion'} ? date("d/m/Y H:i", strtotime($rVacacion{'fhFechaCancelacion'})) : "");?></td>
					<td nowrap="nowrap" class="sanLR04 columnB" width="100%"><?=utf8_encode($rVacacion{'tMotivoCancelacion'});?></td>
				</tr>
			<?php $i++; } ?>
		</tbody>
	</table>
	</div>
<?php }else{ ?>
<script type="text/javascript">
dojo.require("dojo.number");
dojo.require("dijit.form.NumberTextBox");
dojo.require("dijit.form.DateTextBox");
dojo.require("dojo.io.iframe");

function nuevo(){
	document.location = "?ePagina=2.4.1.5.php";
}

function generaExcel(){
	var UrlExcel = "php/excel/2.5.1.4.php?"+
	(dojo.byId('eCodVacacion').value ? "&eCodVacacion="+dojo.byId('eCodVacacion').value : "")+
	(dojo.byId('fhFechaVacacionInicio').value ? "&fhFechaVacacionInicio="+dojo.byId('fhFechaVacacionInicio').value : "")+
	(dojo.byId('fhFechaVacacionFin').value ? "&fhFechaVacacionFin="+dojo.byId('fhFechaVacacionFin').value : "")+		
	(dojo.byId('Empleado').value ? "&Empleado="+dojo.byId('Empleado').value : "")+				
	(dojo.byId('tCodEstatus').value ? "&tCodEstatus="+dojo.byId('tCodEstatus').value : "")+
	(dojo.byId('eCodTipoAusentismo').value ? "&eCodTipoAusentismo="+dojo.byId('eCodTipoAusentismo').value : "")+
	(dojo.byId('orden').value ? "&orden="+dojo.byId('orden').value : "")+
	(dojo.byId('limite').value ? "&limite="+dojo.byId('limite').value : "");		
	var Datos = document.createElement("FORM");
	document.body.appendChild(Datos);
	Datos.name='Datos';
	Datos.method = "POST";
	Datos.action = UrlExcel;
	Datos.submit();
}

dojo.addOnLoad(function(){filtrarConsulta();});
</script>
<form name="Datos" id="Datos" method="post" action="" onsubmit="return false;">
	<input type="hidden" value="0" name="ePagina" id="ePagina" />
	<input type="hidden" value="" name="eAccion" id="eAccion" />
	<input type="hidden" value="<?=(int)$_SESSION['sesionUsuario']['eCodEntidad'];?>" name="eEntidad" id="eEntidad" />
	<table width="965px" border="0">
		<tr>
			<td colpan="3" width="100%"></td>
			<td align="right" nowrap="nowrap"><a class="fntBR11" href="javaScript:mostrarFiltros('Busqueda');">Busqueda de Registros</a></td>
		</tr>
		<tr id="trBusqueda" style="display:none">
			<td colspan="4">
				<table width="100%"  bgcolor="#f9f9f9">
					<tr><td class="sanLR04" height="5"></td></tr>
					<tr>
						<td class="sanLR04" height="20">C&oacute;digo</td>
						<td class="sanLR04" width="40%"><input type="text" name="eCodVacacion" dojoType="dijit.form.TextBox" id="eCodVacacion" value="" style="width:80px; height:25px;"></td>
						<td class="sanLR04" nowrap="nowrap">Fecha de Vacaciones</td>
						<td class="sanLR04" width="50%">
						<input name="fhFechaVacacionInicio" id="fhFechaVacacionInicio" type="date"  required="false"  /> -
						<input name="fhFechaVacacionFin" id="fhFechaVacacionFin" type="date"  required="false"  />
						</td>
					</tr>						
					<tr>
						<td class="sanLR04" height="20">Empleado</td>
						<td class="sanLR04" width="50%"><input type="text" name="Empleado" dojoType="dijit.form.TextBox" id="Empleado" value="" style="width:180px; height:25px;"></td>
						<td class="sanLR04" height="20">Estatus</td>
						<td class="sanLR04" width="50%"><select name="tCodEstatus" id="tCodEstatus" style="width:142px; height:25px;">
								<option value="">Seleccione...</option>
								<option value="AC" selected>Activo</option>
								<option value="CA">Cancelado</option>									
							</select></td>
					</tr>
					<tr>
		              <td nowrap="nowrap" class="sanLR04" height="5"><b>[Ordenamiento]</b></td>
		              <td nowrap="nowrap" class="sanLR04" width="50%"><input type="radio" name="orden" id="orden"  value="ASC" > ASCENDENTE / <input type="radio" name="orden" id="orden" checked="checked" value="DESC" > DESENDENTE</td>
		              <td nowrap="nowrap" class="sanLR04"><b>[Maximos Registros]:</b></td>
		              <td nowrap="nowrap" class="sanLR04" width="50%">
		              	<select name="limite" id="limite" style="width:142px; height:25px;">
                              <option value="100">100</option>
                              <option value="1000" selected>1,000</option>
                              <option value="10000" >10,000</option>
                              <option value="100000">100,000</option>
                      	</select>
                      </td>
        			</tr>
					<tr><td class="sanLR04" height="5"></td></tr>
				</table>
			</td>
		</tr>
		<tr><td colspan="4"><div id="dvCNS"></div></td></tr>
	</table>
</form>
<?php } ?>