﻿<?php require_once("conexion/soluciones-mysql.php");
$link = getLink();

if($_POST){
	if($_POST['eAccion']==1){
		$select=" SELECT * FROM sisconfiguracion ";
		$configura=mysqli_fetch_array(mysqli_query($link,$select));
		
		$varfecha=isset($_POST["fecha"]) ? $_POST["fecha"] : date("Y-m-d");
		$fechaayer = date('Y-m-d',strtotime('-1 days', strtotime($varfecha)));
		$fechaantier = date('Y-m-d',strtotime('-2 days', strtotime($varfecha)));
		$idUsuario = ($_POST['eUsuario'] ? $_POST['eUsuario'] : "NULL");
		//
		$SalEmp	=" SELECT mosh.dImporte FROM movimientosafiliatorios mosh ".
				 " INNER JOIN relasistenciamovimientos rassih ON rassih.eCodMovimiento = mosh.eCodMovimiento ".
				 " INNER JOIN asistencias assih ON assih.id = rassih.eCodAsistencia ".
				 " WHERE mosh.fhFecha='".$fechaantier."' AND assih.idEmpleado=";
		//
		$NoAsis	=" SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."' ";
		//
		$NoAsisE=" SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=";
		//
		$NoEmp	=" SELECT assih.idEmpleado FROM movimientosafiliatorios mosh ".
				 " INNER JOIN relasistenciamovimientos rassih ON rassih.eCodMovimiento=mosh.eCodMovimiento ".
				 " INNER JOIN asistencias assih ON assih.id=rassih.eCodAsistencia WHERE assih.id=";
		//		 
		$SiEmp	=" SELECT assihh.idEmpleado FROM movimientosafiliatorios moshh ".
				 " INNER JOIN relasistenciamovimientos rassihh ON rassihh.eCodMovimiento=moshh.eCodMovimiento ".
				 " INNER JOIN asistencias assihh ON assihh.id=rassihh.eCodAsistencia ".
				 " WHERE moshh.fhFecha='".$fechaantier."' AND moshh.tCodTipoMovimiento NOT IN ('02') AND moshh.eCodEstatus='AC' AND assihh.idEmpleado=";

		foreach($_POST as $k => $valor){
			$nombre = strval($k);
			$campo = "chek";
			if(strstr($nombre,$campo) && (int)$valor>0){
				$select=" SELECT asish.id, (".$SalEmp."asish.idEmpleado) AS SalarioAy ".
						" FROM asistencias asish ".
						" WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."' AND asish.Estado='AC' ".
						" AND asish.idEmpleado NOT IN (".$NoAsis.") ".
						" AND asish.id NOT IN (".$NoAsisE."asish.id) ".
						" AND asish.idEmpleado NOT IN (".$NoEmp."asish.id) ".
						" AND asish.idEmpleado IN (".$SiEmp."asish.idEmpleado) ".
						" AND asish.idEmpleado=".$valor." AND asish.idCategorias=33 ";
				$asisAusen=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
				
				$select=" SELECT sum(asish.Salario+asish.dBono) AS tSalario ".
						" FROM asistencias asish ".
						" WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."' ".
						" AND asish.Estado='AC' ".
						" AND asish.idEmpleado NOT IN (".$NoAsis.") ".
						" AND asish.id NOT IN (".$NoAsisE."asish.id) ".
						" AND asish.idEmpleado NOT IN (".$NoEmp."asish.id) ".
						" AND asish.idEmpleado IN (".$SiEmp.") ".
						" AND asish.idEmpleado=".$valor;
				$rsAsistencias=mysqli_query($link,$select);
				while($rAsistencia=mysqli_fetch_array($rsAsistencias,MYSQLI_ASSOC)){
					$tTipoMv='07';
					if($asisAusen{'id'}>0){
						$dSalarioR=(float)$configura{'dTopeSalarioMinimoDescanso'};
						$tTipoMv=((float)$SalarioAy{'SalarioAy'}==(float)$configura{'dTopeSalarioMinimoDescanso'} ? '01' : '07');
					}else{
						$dSalarioR=((float)$rAsistencia{'tSalario'}>(float)$configura{'dTopeSalario'} ? (float)$configura{'dTopeSalario'} : (float)$rAsistencia{'tSalario'});
					}
					
					$insert=" INSERT INTO movimientosafiliatorios (eCodUsuario, fhFecha, tCodTipoMovimiento,  eCodEstatus, dImporte, tFolio, fhFechaRegistro) ".
							" VALUES (".$idUsuario.", '".$fechaayer."', '".$tTipoMv."', 'AC', ".(float)$dSalarioR.", '".$_POST['folio']."', CURRENT_TIMESTAMP) ";
					if($res=mysqli_query($link,$insert)){
						$exito=1;
						$select=" select last_insert_id() AS Llave ";
						$rCodigo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
						$eCodMovimiento=(int)$rCodigo['Llave'];
					
						$selectt=" SELECT asish.id ".
								 " FROM asistencias asish ".
								 " WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."' ".
								 " AND asish.Estado='AC' ".
								 " AND asish.idEmpleado NOT IN (".$NoAsis.") ".
								 " AND asish.id NOT IN (".$NoAsisE."asish.id) ".
								 " AND asish.idEmpleado NOT IN (".$NoEmp."asish.id) ".
								 " AND asish.idEmpleado IN (".$SiEmp.") ".
								 " AND asish.idEmpleado=".$valor;
						$asistenciasEmp=mysqli_query($link,$selectt);
						while($asisEmp=mysqli_fetch_array($asistenciasEmp,MYSQLI_ASSOC)){
							$insertt=" INSERT INTO relasistenciamovimientos (eCodAsistencia, eCodMovimiento) ".
									 " VALUES (".$asisEmp{'id'}.", ".$eCodMovimiento.") ";
							if($res=mysqli_query($link,$insertt)){
								$exito=1;
							}else{
								$exito=0;
							}
						}
					}else{
						$exito=0;
					}
				}
			}
		}
	}

	date_default_timezone_set('America/Mexico_City');
	$varfecha=isset($_POST["fecha"]) ? $_POST["fecha"] : date("Y-m-d");
	$fechaayer = date('Y-m-d',strtotime('-1 days', strtotime($varfecha)));
	$fechaantier = date('Y-m-d',strtotime('-2 days', strtotime($varfecha)));
	$idUsuario = ($_POST['eUsuario'] ? $_POST['eUsuario'] : "NULL");
	$select=" SELECT * FROM sisconfiguracion ";
	$configura=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
	
	//Inicio Continuaciones
	$select=" SELECT * FROM categorias WHERE indice=27 ";
	$salarioMin=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
	//
	$NoAsis	=" SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."' ";
	//
	$NoAsisE=" SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=";
	//
	$NoEmp	=" SELECT assih.idEmpleado FROM movimientosafiliatorios mosh ".
			 " INNER JOIN relasistenciamovimientos rassih ON rassih.eCodMovimiento=mosh.eCodMovimiento ".
			 " INNER JOIN asistencias assih ON assih.id=rassih.eCodAsistencia WHERE assih.id=";
	//
	/*
	$select=" SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."' ";
	$incapacidades=mysql_query($select);
	$UsuariosInc="0";
	while($incapacidad=mysql_fetch_array($incapacidades)){
		$UsuariosInc.=", ".$incapacidad{'idEmpleado'};
	}
	$select=" SELECT assi.id ".
			" FROM movimientosafiliatorios mos ".
			" INNER JOIN relasistenciamovimientos rassi ON rassi.eCodMovimiento=mos.eCodMovimiento ".
			" INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia ".
			" WHERE assi.Fecha Between '".$fechaantier."' AND '".$fechaayer."' ";
	$afiliatorios=mysql_query($select);
	$UsuariosAfil="0";
	while($afiliatorio=mysql_fetch_array($afiliatorios)){
		$UsuariosAfil.=", ".$afiliatorio{'id'};
	}
	$select=" SELECT assihh.idEmpleado ".
			" FROM movimientosafiliatorios moshh ".
			" INNER JOIN relasistenciamovimientos rassihh ON rassihh.eCodMovimiento=moshh.eCodMovimiento ".
			" INNER JOIN asistencias assihh ON assihh.id=rassihh.eCodAsistencia ".
			" WHERE moshh.fhFecha='".$fechaantier."' AND moshh.tCodTipoMovimiento NOT IN ('02') AND moshh.eCodEstatus='AC' ";
	$movimientos=mysql_query($select);
	$UsiariosMovi="0";
	while($movimiento=mysql_fetch_array($movimientos)){
		$UsiariosMovi.=", ".$movimiento{'idEmpleado'};
	}

	$select=" SELECT DISTINCT asis.idCategorias, emple.id AS numeral, emple.IMSS AS imss, cent.tRegistroPatronal AS rp, emple.Paterno AS pat, Materno AS mat, ".
			" Nombre AS nomb, ctiemp.eCodTipoJornada AS tipjo, '".$fechaayer."' AS Fecha, emple.Clinica AS clin, emple.CURP AS curp, 
CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (".$NoAsisE."asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)>".(float)$configura{'dTopeSalario'}." 
THEN ".(float)$configura{'dTopeSalario'}." 
ELSE 
	CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (".$NoAsisE."asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)=0 
	THEN FORMAT((".(float)$salarioMin{'Turno1'}."*ctiemp.dFactorIntegracion),2) ELSE 
		CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (".$NoAsisE."asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)<".$configura{'dSalarioMinimo'}." 
		THEN ".$configura{'dSalarioMinimo'}." 
		ELSE (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (".$NoAsisE."asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado) 
		END 
	END
END AS tSalario
FROM asistencias asis
INNER JOIN empleados emple ON emple.id=asis.idEmpleado
INNER JOIN cattipoempleado ctiemp ON ctiemp.eCodTipoEntidad=emple.eCodTipo
LEFT JOIN catentidades cent ON cent.eCodEntidad=asis.idEmpresa
WHERE asis.Fecha Between '".$fechaantier."' AND '".$fechaayer."' 
AND emple.eCodTipo=1
AND asis.Estado='AC' 
AND asis.idEmpleado NOT IN (".$UsuariosInc.") 
AND asis.id NOT IN (".$NoAsisE."asis.id) 
AND asis.idEmpleado NOT IN (".$NoEmp."asis.id)
AND asis.idEmpleado IN (SELECT assi.idEmpleado 
FROM movimientosafiliatorios mos 
INNER JOIN relasistenciamovimientos rassi ON rassi.eCodMovimiento=mos.eCodMovimiento 
INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia
WHERE assi.idEmpleado=asis.idEmpleado AND mos.fhFecha='".$fechaantier."' AND mos.tCodTipoMovimiento NOT IN ('02') AND mos.eCodEstatus='AC')
AND 
CASE WHEN (SELECT mosa.dImporte
FROM movimientosafiliatorios mosa 
INNER JOIN relasistenciamovimientos rassia ON rassia.eCodMovimiento=mosa.eCodMovimiento 
INNER JOIN asistencias assia ON assia.id=rassia.eCodAsistencia
WHERE assia.idEmpleado=asis.idEmpleado AND mosa.fhFecha='".$fechaantier."' AND mosa.tCodTipoMovimiento NOT IN ('02') AND mosa.eCodEstatus='AC' LIMIT 1) IS NULL 
THEN 0 
ELSE (SELECT mosa.dImporte
FROM movimientosafiliatorios mosa 
INNER JOIN relasistenciamovimientos rassia ON rassia.eCodMovimiento=mosa.eCodMovimiento 
INNER JOIN asistencias assia ON assia.id=rassia.eCodAsistencia
WHERE assia.idEmpleado=asis.idEmpleado AND mosa.fhFecha='".$fechaantier."' AND mosa.tCodTipoMovimiento NOT IN ('02') AND mosa.eCodEstatus='AC' LIMIT 1) END = 
CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (".$NoAsisE."asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)>".(float)$configura{'dTopeSalario'}." 
THEN ".(float)$configura{'dTopeSalario'}." 
ELSE 
	CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (".$NoAsisE."asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)=0 
	THEN FORMAT((".(float)$salarioMin{'Turno1'}."*ctiemp.dFactorIntegracion),2) 
	ELSE 
		CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (".$NoAsisE."asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)<".$configura{'dSalarioMinimo'}." 
		THEN ".$configura{'dSalarioMinimo'}." 
		ELSE (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (".$NoAsisE."asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado) 
		END
	END
END
UNION 
SELECT DISTINCT asis.idCategorias, emple.id AS numeral, emple.IMSS AS imss, cent.tRegistroPatronal AS rp, emple.Paterno AS pat, Materno AS mat, Nombre AS nomb, ctiemp.eCodTipoJornada AS tipjo, '".$fechaayer."' AS Fecha, emple.Clinica AS clin, emple.CURP AS curp, CASE WHEN (SELECT mosa.dImporte
FROM movimientosafiliatorios mosa 
INNER JOIN relasistenciamovimientos rassia ON rassia.eCodMovimiento=mosa.eCodMovimiento 
INNER JOIN asistencias assia ON assia.id=rassia.eCodAsistencia
WHERE assia.idEmpleado=asis.idEmpleado AND mosa.fhFecha='".$fechaantier."' AND mosa.tCodTipoMovimiento NOT IN ('02') AND mosa.eCodEstatus='AC' LIMIT 1)>".(float)$configura{'dTopeSalario'}." THEN ".(float)$configura{'dTopeSalario'}." ELSE (SELECT mosa.dImporte
FROM movimientosafiliatorios mosa 
INNER JOIN relasistenciamovimientos rassia ON rassia.eCodMovimiento=mosa.eCodMovimiento 
INNER JOIN asistencias assia ON assia.id=rassia.eCodAsistencia
WHERE assia.idEmpleado=asis.idEmpleado AND mosa.fhFecha='".$fechaantier."' AND mosa.tCodTipoMovimiento NOT IN ('02') AND mosa.eCodEstatus='AC' LIMIT 1) END AS tSalario
FROM asistencias asis
INNER JOIN empleados emple ON emple.id=asis.idEmpleado
INNER JOIN cattipoempleado ctiemp ON ctiemp.eCodTipoEntidad=emple.eCodTipo
LEFT JOIN catentidades cent ON cent.eCodEntidad=asis.idEmpresa
WHERE asis.Fecha Between '".$fechaantier."' AND '".$fechaayer."' 
AND asis.Estado='AC' 
AND asis.idEmpleado NOT IN (".$UsuariosInc.") 
AND asis.id NOT IN (".$NoAsisE."asis.id) 
AND asis.idEmpleado NOT IN (".$NoEmp."asis.id)
AND asis.idEmpleado IN (SELECT assi.idEmpleado 
FROM movimientosafiliatorios mos 
INNER JOIN relasistenciamovimientos rassi ON rassi.eCodMovimiento=mos.eCodMovimiento 
INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia
WHERE assi.idEmpleado=asis.idEmpleado AND mos.fhFecha='".$fechaantier."' AND mos.tCodTipoMovimiento NOT IN ('02') AND mos.eCodEstatus='AC')
AND asis.idEmpleado IN (SELECT eCodEmpleado FROM ausentismo WHERE fhFecha='".$fechaayer."' AND eCodEstatus='AC' AND eCodEmpleado=asis.idEmpleado) 
AND emple.eCodTipo=1 ";
$vContinua=$select;
			$continuaciones=mysql_query($select);
			$select=" SELECT * FROM sisconfiguracion ";
			$configura2=mysql_fetch_array(mysql_query($select));
			while($continua=mysql_fetch_array($continuaciones)){
				$dSalarioR=((float)$continua{'tSalario'}>(float)$configura2{'dTopeSalario'} ? (float)$configura2{'dTopeSalario'} : (float)$continua{'tSalario'});
				if($dSalarioR==(float)$configura2{'dTopeSalarioMinimoDescanso'} || $continua{'idCategorias'}!=33){
					//" VALUES (".$idUsuario.", '".$fechaayer."', '01', 'AC', ".(float)$rAsistencia{'tSalario'}.", 'NULL', CURRENT_TIMESTAMP) ";
					$insert=" INSERT INTO movimientosafiliatorios (eCodUsuario, fhFecha, tCodTipoMovimiento,  eCodEstatus, dImporte, tFolio, fhFechaRegistro) ".						
							" VALUES (".$idUsuario.", '".$fechaayer."', '01', 'AC', ".$dSalarioR." , 'NA', CURRENT_TIMESTAMP) ";
					if($res=mysql_query($insert)){
						$exito=1;
						$select=" select last_insert_id() AS Llave ";
						$rCodigo=mysql_fetch_array(mysql_query($select));
						$eCodMovimiento=(int)$rCodigo['Llave'];
	
						$select=" SELECT asish.id
	FROM asistencias asish 
	WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
	AND asish.Estado='AC' 
	AND asish.idEmpleado NOT IN (".$NoAsis.") 
	AND asish.id NOT IN (".$NoAsisE."asish.id) 
	AND asish.idEmpleado NOT IN (SELECT assih.idEmpleado 
	FROM movimientosafiliatorios mosh 
	INNER JOIN relasistenciamovimientos rassih ON rassih.eCodMovimiento=mosh.eCodMovimiento 
	INNER JOIN asistencias assih ON assih.id=rassih.eCodAsistencia
	WHERE assih.id=asish.id)
	AND asish.idEmpleado IN (SELECT assihh.idEmpleado 
	FROM movimientosafiliatorios moshh 
	INNER JOIN relasistenciamovimientos rassihh ON rassihh.eCodMovimiento=moshh.eCodMovimiento 
	INNER JOIN asistencias assihh ON assihh.id=rassihh.eCodAsistencia
	WHERE assihh.idEmpleado=asish.idEmpleado AND moshh.fhFecha='".$fechaantier."' AND moshh.tCodTipoMovimiento NOT IN ('02') AND moshh.eCodEstatus='AC')
	AND asish.idEmpleado=".$continua{'numeral'};
						$asistenciasEmp=mysql_query($select);
						while($asisEmp=mysql_fetch_array($asistenciasEmp)){
							$insertt=" INSERT INTO relasistenciamovimientos (eCodAsistencia, eCodMovimiento) ".
									" VALUES (".$asisEmp{'id'}.", ".$eCodMovimiento.") ";
							if($res=mysql_query($insertt)){
								$exito=1;
							}else{
								$exito=0;
							}
						}
					}else{
						$exito=0;
					}
				}
			}
		//Fin Continuaciones
	*/
	$select=" SELECT * FROM categorias WHERE indice=27 ";
	$salarioMin=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
//$varfecha = ($_POST['fhFechaProgramacion'] ? "'".$_POST['fhFechaProgramacion']."'"                : "NULL");
	$select=" SELECT assi.id ".
			" FROM movimientosafiliatorios mos ".
			" INNER JOIN relasistenciamovimientos rassi ON rassi.eCodMovimiento=mos.eCodMovimiento ".
			" INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia ".
			" WHERE assi.Fecha Between '".$fechaantier."' AND '".$fechaayer."' ";
	$afiliatorios=mysqli_query($link,$select);
	$UsuariosAfil="0";
	while($afiliatorio=mysqli_fetch_array($afiliatorios,MYSQLI_ASSOC)){
		$UsuariosAfil.=", ".$afiliatorio{'id'};
	}
	$select=" SELECT assihh.idEmpleado ".
			" FROM movimientosafiliatorios moshh ".
			" INNER JOIN relasistenciamovimientos rassihh ON rassihh.eCodMovimiento=moshh.eCodMovimiento ".
			" INNER JOIN asistencias assihh ON assihh.id=rassihh.eCodAsistencia ".
			" WHERE moshh.fhFecha='".$fechaantier."' AND moshh.tCodTipoMovimiento NOT IN ('02') AND moshh.eCodEstatus='AC' ";
	$movimientos=mysqli_query($link,$select);
	$UsiariosMovi="0";
	while($movimiento=mysqli_fetch_array($movimientos,MYSQLI_ASSOC)){
		$UsiariosMovi.=", ".$movimiento{'idEmpleado'};
	}

   $select=" 
	SELECT DISTINCT emple.id AS numeral, emple.IMSS AS imss, cent.tRegistroPatronal AS rp, emple.Paterno AS pat, Materno AS mat, Nombre AS nomb, ctiemp.eCodTipoJornada AS tipjo, '".$fechaayer."' AS Fecha, emple.Clinica AS clin, emple.CURP AS curp, 
	CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$NoAsis.") 
AND asish.id NOT IN (".$NoAsisE."asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)>".(float)$configura{'dTopeSalario'}." THEN ".(float)$configura{'dTopeSalario'}." ELSE 
	CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$NoAsis.") 
AND asish.id NOT IN (".$NoAsisE."asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)=0 THEN FORMAT((".(float)$salarioMin{'Turno1'}."*ctiemp.dFactorIntegracion),2) ELSE ".
	" CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$NoAsis.") 
AND asish.id NOT IN (".$NoAsisE."asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)<".$configura{'dSalarioMinimo'}." THEN ".$configura{'dSalarioMinimo'}." ELSE (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$NoAsis.") 
AND asish.id NOT IN (".$NoAsisE."asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado) END ".
	" END
	END AS Salario
FROM asistencias asis
INNER JOIN empleados emple ON emple.id=asis.idEmpleado
INNER JOIN cattipoempleado ctiemp ON ctiemp.eCodTipoEntidad=emple.eCodTipo
LEFT JOIN catentidades cent ON cent.eCodEntidad=asis.idEmpresa
WHERE asis.Fecha Between '".$fechaantier."' AND '".$fechaayer."' 
AND emple.eCodTipo=1
AND asis.Estado='AC' 
AND asis.idEmpleado NOT IN (".$NoAsis.") 
AND asis.id NOT IN (".$NoAsisE."asis.id) 
AND asis.id NOT IN (".$UsuariosAfil.")
AND asis.idEmpleado IN (SELECT assi.idEmpleado 
FROM movimientosafiliatorios mos 
INNER JOIN relasistenciamovimientos rassi ON rassi.eCodMovimiento=mos.eCodMovimiento 
INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia
WHERE assi.idEmpleado=asis.idEmpleado AND mos.fhFecha='".$fechaantier."' AND mos.tCodTipoMovimiento NOT IN ('02') AND mos.eCodEstatus='AC')
AND 
CASE WHEN (SELECT mosa.dImporte
FROM movimientosafiliatorios mosa 
INNER JOIN relasistenciamovimientos rassia ON rassia.eCodMovimiento=mosa.eCodMovimiento 
INNER JOIN asistencias assia ON assia.id=rassia.eCodAsistencia
WHERE assia.idEmpleado=asis.idEmpleado AND mosa.fhFecha='".$fechaantier."' AND mosa.tCodTipoMovimiento NOT IN ('02') AND mosa.eCodEstatus='AC' LIMIT 1) IS NULL 
THEN 0 
ELSE (SELECT mosa.dImporte
FROM movimientosafiliatorios mosa 
INNER JOIN relasistenciamovimientos rassia ON rassia.eCodMovimiento=mosa.eCodMovimiento 
INNER JOIN asistencias assia ON assia.id=rassia.eCodAsistencia
WHERE assia.idEmpleado=asis.idEmpleado AND mosa.fhFecha='".$fechaantier."' AND mosa.tCodTipoMovimiento NOT IN ('02') AND mosa.eCodEstatus='AC' LIMIT 1) END<>
CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$NoAsis.") 
AND asish.id NOT IN (".$NoAsisE."asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)>".(float)$configura{'dTopeSalario'}." 
THEN ".(float)$configura{'dTopeSalario'}." 
ELSE 
	CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$NoAsis.") 
AND asish.id NOT IN (".$NoAsisE."asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)=0 
	THEN FORMAT((".(float)$salarioMin{'Turno1'}."*ctiemp.dFactorIntegracion),2) 
	ELSE 
		CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$NoAsis.") 
AND asish.id NOT IN (".$NoAsisE."asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)<".$configura{'dSalarioMinimo'}." 
		THEN ".$configura{'dSalarioMinimo'}." 
		ELSE (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$NoAsis.") 
AND asish.id NOT IN (".$NoAsisE."asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado) 
		END
	END
END ";
    $rsServicios=mysqli_query($link,$select); 
    $cont=0;
    $registros=(int)mysqli_num_rows($rsServicios);
  ?>
<table cellspacing="0" border="0" width="980px">
  <tr>
    <td width="50%"><hr color="#666666" /></td>
    <td class="sanLR04" nowrap="nowrap"><b>Registros Encontrados: (     <?=$registros;?>     )</b></td>
    <td width="50%"><hr color="#666666" /></td>
  </tr>
</table>
<div style="display:block; top:0; left:0; width:965px; z-index=1; overflow: auto; height:400px;">
  <table cellspacing="0" border="0" width="980px">
    <thead>
      <tr class="thEncabezado">
        <td nowrap="nowrap" class="sanLR04" height="20" align="center"><input type="checkbox" id="actMarca" name="actMarca" onclick="Seleccionar();"></td>
      <td nowrap="nowrap" class="sanLR04">R. Patronal</td>
      <td nowrap="nowrap" class="sanLR04">NSS</th>
      <td nowrap="nowrap" class="sanLR04">Paterno</td> 
      <td nowrap="nowrap" class="sanLR04">Materno</td>
      <td nowrap="nowrap" class="sanLR04">Nombre(s)</td>
      <td nowrap="nowrap" class="sanLR04">S.D.I.</td>
      <td nowrap="nowrap" class="sanLR04">S. Infornavit</td>
      <td nowrap="nowrap" class="sanLR04">T. Trabajador</td>
      <td nowrap="nowrap" class="sanLR04">T. Salario</td>
      <td nowrap="nowrap" class="sanLR04">Jornada</td>
      <td nowrap="nowrap" class="sanLR04">F. Movimiento</td>
      <td nowrap="nowrap" class="sanLR04">U.M.F</td>
      <td nowrap="nowrap" class="sanLR04">Movimiento</td>
      <td nowrap="nowrap" class="sanLR04">Guia</td>
      <td nowrap="nowrap" class="sanLR04">C. Trabajador</td>
      <td nowrap="nowrap" class="sanLR04">Filler</td>
      <td nowrap="nowrap" class="sanLR04">CURP</td>      
      <td nowrap="nowrap" class="sanLR04" width="100%">Identificador</td> <!--Ya quedo-->                    
    </tr>   
    </thead>
    <tbody>
      <?php $i=1; while($rServicio=mysqli_fetch_array($rsServicios,MYSQLI_ASSOC)){
                       // $salario=0;
                      /*if($rServicio["Turno"]==1)
                        $salario=$rServicio["Turno1"];
                      if($rServicio["Turno"]==2)
                        $salario=$rServicio["Turno2"];
                      if($rServicio["Turno"]==3)
                        $salario=$rServicio["Turno3"];*/

                        $salario=explode(".",str_replace(",", "", number_format($rServicio["Salario"], 2)));
                        if(strlen($salario[0])<=3)
                          $cadena="0".$salario[0].$salario[1];
                        else
                          $cadena=$salario[0].$salario[1];
                        $fech=$rServicio["Fecha"]; 
                        $fecha=explode("-",$fech); 
                        $abc=$fecha[2].$fecha[1].$fecha[0]; 
        ?>
      <tr>        
        <input type="hidden" id="id<?=$i?>" name="id<?=$i?>" value="<?=$rServicio["numeral"]?>">
        <td nowrap="nowrap" class="sanLR04" height="20" align="center"><input type="checkbox" name="chek<?=$i?>" id="chek<?=$i?>" value="<?=$rServicio{'numeral'};?>" ></td>
        <td nowrap="nowrap" class="sanLR04 columnB"><b><?=$rServicio{'rp'};?></b></td>                              
        <td nowrap="nowrap" class="sanLR04"><?= utf8_decode($rServicio["imss"]); ?></td>
        <td nowrap="nowrap" class="sanLR04 columnB"><?= utf8_decode($rServicio["pat"]); ?></td>
        <td nowrap="nowrap" class="sanLR04"><?= utf8_decode($rServicio["mat"]); ?></td>
        <td nowrap="nowrap" class="sanLR04 columnB"><?= utf8_decode($rServicio["nomb"]); ?></td>
        <td nowrap="nowrap" class="sanLR04"><?= utf8_decode($cadena); ?></td>
        <td nowrap="nowrap" class="sanLR04 columnB">000000</td>
        <td nowrap="nowrap" class="sanLR04">2</td>
        <td nowrap="nowrap" class="sanLR04 columnB">0</td>
        <td nowrap="nowrap" class="sanLR04"><?= utf8_decode($rServicio["tipjo"]); ?></td>
        <td nowrap="nowrap" class="sanLR04 columnB"><?= utf8_decode($abc); ?></td>
        <td nowrap="nowrap" class="sanLR04 "><?= utf8_decode($rServicio["clin"]); ?></td>
        <td nowrap="nowrap" class="sanLR04 columnB">08</td>
        <td nowrap="nowrap" class="sanLR04 ">03400</td>
        <td nowrap="nowrap" class="sanLR04 columnB">ESTIBADOR</td>
        <td nowrap="nowrap" class="sanLR04 "></td>
        <td nowrap="nowrap" class="sanLR04 columnB"><?= utf8_decode($rServicio["curp"]); ?></td>
        <td nowrap="nowrap" class="sanLR04 " width="100%">9</td>
        
      </tr>
      <?php $i++; } ?>
    </tbody>
  </table>
</div>
<?php }else{ ?>
<script type="text/javascript">
function nuevo(){
	document.location = "?ePagina=2.3.3.1.1.php";
}

function consultar(){
  document.location = "?ePagina=2.5.1.1.php";
}

function Seleccionar(){
  if(dojo.byId('actMarca').checked){
    dojo.query("[id*=\"chek\"]").forEach(function(nodo, index, arr){  
      z = nodo.id.replace(/chek/, "");
      if(!dojo.byId("chek"+z).disabled)
        dojo.byId("chek"+z).checked=true;
    });
  }else{
    dojo.query("[id*=\"chek\"]").forEach(function(nodo, index, arr){  
      z = nodo.id.replace(/chek/, "");
        dojo.byId("chek"+z).checked=false;
    });
  }
}

function guardar(){
	var eChk = 0;
	var bandera = false;
	var mensaje = "¡Verifique lo siguiente!\n";
	dojo.byId('eAccion').value = 1;	
	dojo.query('input[id^=chek]:checked').forEach(function(nodo, index, arr){
		eChk++;
	});
	
	if(!dojo.byId("folio").value){
		mensaje+="* Folio\n";
		bandera = true;   
	}
	if(eChk==0){
		mensaje+="* Seleccione Empleado\n";
		bandera = true;  
	}
	if(bandera==true){
		alert(mensaje);
	}else{
		if(confirm("¿Desea registrar el folio?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+'.php', handleAs: "text", load: function(tRespuesta, ioArgs){
				dojo.byId('dvCNS').innerHTML = tRespuesta;
				alert("¡El Registro se realizo exitosamente!");	
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});
		}
	}
	dojo.byId('eAccion').value = "";	
	dojo.byId('folio').value = "";	
}
dojo.addOnLoad(function(){filtrarConsulta();});
</script>
<form name="Datos" id="Datos" method="post" action="" onsubmit="return false;">
  <input type="hidden" name="eUsuario" id="eUsuario" value="<?=(int)$_SESSION['sesionUsuario']['eCodUsuario'];?>" />
  <input type="hidden" value="0" name="ePagina" id="ePagina" />
  <input type="hidden" value="" name="eAccion" id="eAccion" />
  <table width="900px" border="0">
    <tr>
      <td colpan="3" width="100%"></td>
      <td align="right" nowrap="nowrap"><a class="fntBR11" href="javaScript:mostrarFiltros('Busqueda');">Busqueda de Registros</a></td>
    </tr>
    <tr id="trBusqueda" style="display:none">
      <td colspan="4"><table width="900px" bgcolor="#f9f9f9">
          <tr>
            <td class="sanLR04" height="5"></td>
          </tr>
          <tr>
            <td class="sanLR04" height="20">Fecha</td>
            <td class="sanLR04" width="50%"><input type="date" name="fecha" dojoType="dijit.form.TextBox" id="fecha" value="<?=$_POST["fecha"] ? $_POST["fecha"] : date("Y-m-d")?>" style="width:140px"></td>
            <td class="sanLR04">Folio</td>
            <td class="sanLR04" width="50%"><input type="text" name="folio" dojoType="dijit.form.TextBox" id="folio" value="" style="width:80px"></td>
          </tr>
          <tr>
            <td class="sanLR04" height="5"></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td colspan="4"><div id="dvCNS"></div></td>
    </tr>
  </table>
</form>
<?php } ?>