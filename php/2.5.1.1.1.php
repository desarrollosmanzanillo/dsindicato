<?php require_once("conexion/soluciones-mysql.php");  
$link = getLink();
if($_POST){
	if($_POST['eProceso']==1){
		$exito					= 0;
		$fhFecha				= ($_POST['fecha']					? "'".($_POST['fecha'])."'"					: "NULL");
		$dSalario				= ($_POST['Salario']				? str_replace(",", "", $_POST['Salario'])	: "NULL");
		$eCodTArea				= ($_POST['area']					? (int)$_POST['area']						: "NULL");
		$eCodTTurno				= ($_POST['Turno']					? (int)$_POST['Turno']						: "NULL");
		$eCodUsuario			= ($_POST['eUsuario']				? (int)$_POST['eUsuario']					: "NULL");
		$tModificacion			= ($_POST['tModificacion']			? "'".($_POST['tModificacion'])."'"			: "NULL");
		$eCodAsistencia			= ($_POST['eCodAsistencia']			? (int)$_POST['eCodAsistencia']				: "NULL");
		$eCodTCategoria			= ($_POST['categorias']				? (int)$_POST['categorias']					: "NULL");
		$eCodTipoEmpleado		= ($_POST['eCodTipoEmpleado']		? (int)$_POST['eCodTipoEmpleado']			: "NULL");
		$eCodMotivoAsistencia	= ($_POST['eCodMotivoAsistencia']	? (int)$_POST['eCodMotivoAsistencia']		: "NULL");

		if((int)$eCodAsistencia>0){
			$insert=" INSERT INTO logasistencias (eCodUsuario, fhFecha, eCodAsistencia, eTurno, eCodMotivoAsistencia, fhFechaAsistencia, 
					eCodCategoria, eCodCentroCosto, tMotivoModificacion, Salario) 
					SELECT ".$eCodUsuario.", current_timestamp, ".$eCodAsistencia.", Turno, eCodMotivoAsistencia, Fecha, idCategorias, 
					area, ".$tModificacion.", ".$dSalario."
					FROM asistencias
					WHERE id=".$eCodAsistencia;
			print $insert;
			if($res1=mysqli_query($link,$insert)){
				$update=" UPDATE asistencias ".
						" SET Fecha 			=".$fhFecha.",".
						" area 					=".$eCodTArea.",".
						" Turno 				=".$eCodTTurno.",".
						" Salario 				=".$dSalario.",".
						" eCodCosto 			=".$eCodTArea.",".
						" idCategorias 			=".$eCodTCategoria.",".
						" bModificacion 		=1,".
						" fhFechaActualiza 		=current_timestamp,".
						" eCodUsuarioActualiza 	=".$eCodUsuario.",".
						" eCodMotivoAsistencia 	=".$eCodMotivoAsistencia.
						" WHERE id=".$eCodAsistencia;
				if($res=mysqli_query($link,$update)){
					$exito=1;
				}else{
					$exito=0;
				}
			}else{
				$exito=0;
			}
		}
		print "<input type=\"text\" value=\"".($exito==1 ? (int)$eCodAsistencia : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}

	if($_POST['eProceso']==2){
		$exito				= 0;
		$tMotivo			= ($_POST['cancelacion']	? "'".trim(utf8_decode($_POST['cancelacion']))."'"	: "NULL");
		$eCodUsuario		= ($_POST['eUsuario']		? (int)$_POST['eUsuario']							: "NULL");
		$eCodAsistencia		= ($_POST['eCodAsistencia']	? (int)$_POST['eCodAsistencia']						: "NULL");
		//$rRFC=mysqli_fetch_array(mysql_query($link,"SELECT 1 AS RFC FROM empleados WHERE RFC='".trim($_POST['RFC'])."' AND id!=".(int)$_POST['eCodEntidad']), MYSQLI_NUM);

		if((int)$eCodAsistencia>0){
			$update=" UPDATE asistencias
					SET Estado='CA',
					tMotivoCancelacion=".$tMotivo.",
					eCodUsuarioCancela=".$eCodUsuario.",
					fhFechaCancela=current_timestamp ".
					" WHERE id=".$eCodAsistencia;
			print $update;
			if($res=mysqli_query($link,$update)){
				$exito=1;
			}else{
				$exito=0;
			}
		}
		print "<input type=\"text\" value=\"".($exito==1 ? (int)$eCodAsistencia : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";		
	}
	
	if($_POST['eProceso']==3){
		//Revisa Fecha
		$select=" SELECT 1 as id ".
				" FROM asistencias ".
				" WHERE Estado='AC' ".
				" AND idEmpleado=".$_POST['eCodEmpleado']." AND Turno=".$_POST['Turno']." AND Fecha='".$_POST['fecha']."' AND id not in (".$_POST['eCodAsistencia'].")";
				print($select);
		$rsExiste=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_NUM); 
		print "<input name=\"eEntidad\" id=\"eEntidad\" value=\"".(int)$rsExiste['id']."\">";
	}
	
	if($_POST['eProceso']==4){
		//Revisa Fecha
		$select=" SELECT 1 as id ".
				" FROM asistencias ".
				" WHERE Estado='AC' ".
				" AND idEmpleado=".$_POST['eCodEmpleado']." AND Turno=".$_POST['Turno']." AND Fecha='".$_POST['fecha']."' AND id not in (".$_POST['eCodAsistencia'].")";
		$rsExiste=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_NUM);
		print "<input name=\"eEntidad\" id=\"eEntidad\" value=\"".(int)$rsExiste['id']."\">";
	}

	if($_POST['eProceso']==5){
		//Revisa Salario
		//fecha
		$fechaasis= date('Y-m-d',strtotime('+1 days', strtotime($_POST['fecha'])));
		$domingo=date("w",strtotime($fechaasis));
		$domingo = date("w",strtotime($fechaasis));
		$Turno 				= ($_POST['Turno']			? (int)$_POST['Turno']			: "NULL");
		$idEmpleado			= ($_POST['eCodEmpleado']	? (int)$_POST['eCodEmpleado']	: "NULL");
		$idCategorias		= ($_POST['categorias']		? (int)$_POST['categorias']		: "NULL");
		$fhFechaAsistencia	= ($_POST['fecha']			? "'".$_POST['fecha']."'"		: "NULL");

		$select=" SELECT Turno".$Turno." AS Salario, TurnoE".$Turno." AS SalarioE ".
				" FROM categorias ".
				" WHERE indice=".(int)$idCategorias;
		if((int)$_POST['eAccion']==888){
			print $select."<br>";
		}
		$salariocategoria=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_NUM);

		$select=" SELECT * ".
				" FROM sisconfiguracion ";
		if((int)$_POST['eAccion']==888){
			print $select."<br>";
		}
		$configura=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_NUM);

		$select=" SELECT eCodDia ".
				" FROM catdiasfestivos ".
				" WHERE ecodEstatus='AC' AND fhFecha=".$fhFechaAsistencia;
		if((int)$_POST['eAccion']==888){
			print $select."<br>";
		}
		$rFestivo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_NUM);

		$select=" SELECT empl.descanso, IFNULL(cv.dFactorIntegracion,ctipo.dFactorIntegracion) AS dFactorIntegracion, ctipo.eCodTipoEntidad ".
				" FROM empleados empl ".
				" INNER JOIN cattipoempleado ctipo ON ctipo.eCodTipoEntidad=empl.eCodTipo ".
				" LEFT JOIN catvacaciones cv ON cv.eCodVacaciones=CASE WHEN DATE_ADD(empl.fhFechaIngreso, INTERVAL ROUND(DATEDIFF(".$fhFechaAsistencia.", empl.fhFechaIngreso)/365) YEAR)<CURRENT_DATE OR ROUND(DATEDIFF(".$fhFechaAsistencia.", empl.fhFechaIngreso)/365)=0 THEN ROUND(DATEDIFF(".$fhFechaAsistencia.", empl.fhFechaIngreso)/365)+1 ELSE ROUND(DATEDIFF(".$fhFechaAsistencia.", empl.fhFechaIngreso)/365) END AND 1=empl.eCodTipo ".
				" WHERE empl.id=".(int)$idEmpleado;
		if((int)$_POST['eAccion']==888){
			print $fhFechaAsistencia." - ".$idEmpleado." - ".$Turno." - ".$select."<br>";
		}
		$factor = mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_NUM);
		$salario = ($factor['eCodTipoEntidad']==1 ? (float)$salariocategoria['Salario'] : (float)$salariocategoria['SalarioE']);
		$descanso = (int)$factor['descanso'];
		$SalarioNeto=($descanso==$domingo && $domingo==1 //Día de Descanso y Domingo
			? ($salario*$factor{'dFactorIntegracion'})+($salario*((float)$configura{'dPrimaDominical'}/100))+($factor{'eCodTipoEntidad'}==1 ? $salario : 0)
			: ($descanso==$domingo && $domingo!=1 //Día de Descanso y NO Domingo
				? ($salario*$factor{'dFactorIntegracion'})+($factor{'eCodTipoEntidad'}==1 ? $salario : 0) 
				: ($descanso!=$domingo && $domingo==1 //NO Día Descanso y Domingo
					? ($salario*$factor{'dFactorIntegracion'})+($salario*((float)$configura{'dPrimaDominical'}/100))
					: $factor{'dFactorIntegracion'}*$salario)))+((int)$rFestivo{'eCodDia'}>0 ? (float)$salario*2 : 0); //Normal
		if((int)$_POST['eAccion']==888){
			print $SalarioNeto."=(".$descanso."==".$domingo." && ".$domingo."==1 //Día de Descanso y Domingo
			? (".$salario."*".$factor{'dFactorIntegracion'}.")+(".$salario."*(".(float)$configura{'dPrimaDominical'}."/100))+(".$factor{'eCodTipoEntidad'}."==1 ? ".$salario." : 0)
			: (".$descanso."==".$domingo." && ".$domingo."!=1 //Día de Descanso y NO Domingo
				? (".$salario."*".$factor{'dFactorIntegracion'}.")+(".$factor{'eCodTipoEntidad'}."==1 ? ".$salario." : 0)
				: (".$descanso."!=".$domingo." && ".$domingo."==1 //NO Día Descanso y Domingo
					? (".$salario."*".$factor{'dFactorIntegracion'}.")+(".$salario."*(".(float)$configura{'dPrimaDominical'}."/100))
					: ".$factor{'dFactorIntegracion'}."*".$salario.")))+(".(int)$rFestivo{'eCodDia'}.">0 ? ".(float)$salario."*2 : 0)"; //Normal."<br>";
		}
		print "<input name=\"eEntidad\" id=\"eEntidad\" value=\"".number_format($SalarioNeto,2)."\">";
	}
//
}else{
$select=" SELECT ast.*, emp.Empleado AS Trabajador, cat.Categoria AS categoria, ast.Estado as tCodEstatus , emp.eCodTipo,
		ce.tNombre AS Entidad, ce.tRegistroPatronal
		FROM asistencias ast
		LEFT JOIN categorias cat ON cat.indice=ast.idCategorias
		LEFT JOIN empleados emp ON emp.id=ast.idEmpleado
		LEFT JOIN catentidades ce ON ce.eCodEntidad=ast.idEmpresa
		WHERE ast.id=".$_GET['eCodAsistencia'];
$rEntidad = mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC); 

$select=" SELECT * 
		FROM catmotivosasistencias ";
$rsMotivosAsistencias=mysqli_query($link,$select);

$select=" SELECT * 
		FROM categorias 
		WHERE tCodEstatus='AC' 
		ORDER BY Categoria ASC ";
$rsTiposCategorias = mysqli_query($link,$select);

$select=" SELECT *
		FROM catcostos
		WHERE tCodEstatus='AC'
		ORDER BY tArea ASC ";
$rsTiposArea = mysqli_query($link,$select); ?>
<script type="text/javascript">
function cancelar(){
	dojo.byId('eProceso').value=2;
	bandera = false;
	mensaje = "¡Verifique lo siguiente!\n";
	if (!dojo.byId("cancelacion").value){
		mensaje+="* Motivo de Cancelacion \n";
		bandera = true;		
	}

	if (bandera==true){
		alert(mensaje);		
	}else{
		if (confirm("¿Desea Cancelar el Registro de Asistencia?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				if(dojo.byId("eCodigo").value>0){
					alert("¡La Cancelacion se realizo correctamente!");
					tURL = './?ePagina=2.5.1.1.php';
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
		}
	}

}

function guardar(){
	dojo.byId('eProceso').value=1;
	bandera = false;
	mensaje = "¡Verifique lo siguiente!\n";

	if (!dojo.byId("eCodMotivoAsistencia").value){
		mensaje+="* Motivo de Asistencia\n";
		bandera = true;		
	}
	if (!dojo.byId("Turno").value){
		mensaje+="* Turno\n";
		bandera = true;		
	}
	if (!dojo.byId("fecha").value){
		mensaje+="* Fecha Asistencia\n";
		bandera = true;		
	}
	
	if (!dojo.byId("categorias").value){
		mensaje+="* Categoría\n";
		bandera = true;		
	}
	if (!dojo.byId("tModificacion").value){
		mensaje+="* Motivo de Modificación\n";
		bandera = true;		
	}

	if (bandera==true){
		alert(mensaje);		
	}else{
		if (confirm("¿Desea Guardar la Información?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				if(dojo.byId("eCodigo").value>0){
					alert("¡La información se guardó correctamente!");
					tURL = './?ePagina=2.5.1.1.2.php&eCodAsistencia='+dojo.byId("eCodigo").value;
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
		}
	}
}	

function verificarFecha(){
	if(dojo.byId('fecha').value){
		dojo.byId('eProceso').value=3;
		dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
			dojo.byId("dvCNS").innerHTML = tRespuesta;
			if(dojo.byId('eEntidad').value==1){
				alert("¡Ya Existe Asistencia Registrada en esa Fecha y Turno para este Empleado!");
				dojo.byId('fecha').value="";
			}
		}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
	}
}

function verificarTurno(){
	if(dojo.byId('Turno').value){
		dojo.byId('eProceso').value=4;
		dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
			dojo.byId("dvCNS").innerHTML = tRespuesta;
			if(dojo.byId('eEntidad').value==1){
				alert("¡Ya Existe Asistencia Registrada en esa Fecha y Turno para este Empleado!");
				dojo.byId('Turno').value="";
			}
		}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
	}
}

function verificaSalario(){
	if(dojo.byId('categorias').value){
		dojo.byId('eProceso').value=5;
		dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
			dojo.byId("dvCNS").innerHTML = tRespuesta;
			if(dojo.byId('eEntidad').value){
				//alert("¡Ya Existe Asistencia Registrada en esa Fecha y Turno para este Empleado!");
				dojo.byId('Salario').value=dojo.byId('eEntidad').value;
			}
		}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
	}
}

function consultar(){
	document.location = './?ePagina=2.5.1.1.php';
}
</script>
<div id="dvCNS" style="display:none;"></div>
<form action="" method="post" name="Datos" id="Datos" onsubmit="return false;">
<input type="hidden" name="eAccion" id="eAccion" value="" />
<input type="hidden" name="eProceso" id="eProceso" value="" />
<input type="hidden" name="eUsuario" id="eUsuario" value="<?=(int)$_SESSION['sesionUsuario']['eCodUsuario'];?>" />
<table border="0" cellpadding="0" cellspacing="0">
    <tr><td height="20"></td></tr>
    <tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" align="absmiddle"> Codigo</td>
	    <td width="100%" colspan="3" nowrap class="sanLR04"><b><?=sprintf("%07d",$rEntidad['id']);?></b><input name="eCodAsistencia" type="hidden" dojoType="dijit.form.TextBox" id="eCodAsistencia" value="<?=utf8_encode($rEntidad{'id'});?>" style="width:400px"></td>
    </tr>
    <tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" align="absmiddle"> Registro Patronal</td>
	    <td width="100%" colspan="4" nowrap class="sanLR04"><b><?=utf8_encode($rEntidad['id']." - ".$rEntidad['tRegistroPatronal']);?></b></td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" align="absmiddle"> Empleado </td>
	    <td width="100%" colspan="4" nowrap class="sanLR04"><b><?=utf8_encode($rEntidad{'Trabajador'});?></b>
	    <input name="eCodTipoEmpleado" type="hidden" dojoType="dijit.form.TextBox" id="eCodTipoEmpleado" value="<?=utf8_encode($rEntidad{'eCodTipo'});?>" style="width:400px">
	    <input name="eCodEmpleado" type="hidden" dojoType="dijit.form.TextBox" id="eCodEmpleado" value="<?=utf8_encode($rEntidad{'idEmpleado'});?>" style="width:400px">
	    </td>		
    </tr>
    <tr>
    	<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Motivo de Asistencia</td>
	    <td width="100%" colspan="3" nowrap class="sanLR04">
	    	<select name="eCodMotivoAsistencia" id="eCodMotivoAsistencia" style="width:105px">
	    		<option value="">Seleccione...</option>									
				<?php foreach($rsMotivosAsistencias as $key => $rMotivoAsistencia){ ?>
					<option value="<?=$rMotivoAsistencia{'eCodMotivoAsistencia'}?>" <?=($rMotivoAsistencia{'eCodMotivoAsistencia'}==$rEntidad{'eCodMotivoAsistencia'} ? "selected='selected'" : "");?>><?=utf8_encode($rMotivoAsistencia{'tNombre'})?></option>
				<?php } ?>
	    	</select>
	    </td>
	</tr>
    <tr>
	    <td class="sanLR04" nowrap><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Turno</td>
	    <td width="50%" class="sanLR04">
			<select name="Turno" id="Turno" style="width:175px" onblur="verificarTurno();" onchange="verificarTurno();">
				<option value="">Seleccione...</option>									
				<option value="1" <?=($rEntidad{'Turno'}==1 ? "selected='selected'" : "");?> >Turno 1</option>
				<option value="2" <?=($rEntidad{'Turno'}==2 ? "selected='selected'" : "");?> >Turno 2</option>
				<option value="3" <?=($rEntidad{'Turno'}==3 ? "selected='selected'" : "");?> >Turno 3</option>
			</select>
		</td>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Fecha Asistencia</td>
	    <td width="50%" nowrap class="sanLR04"><input name="fecha" type="date" dojoType="dijit.form.TextBox" id="fecha" value="<?=$rEntidad{'Fecha'};?>" style="width:175px" onclick="verificarFecha();" onchange="verificarFecha();" onblur="verificarFecha();"></td>
    </tr>
    <tr>	    
		<td class="sanLR04" nowrap><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Categoría</td>
	    <td width="50%" class="sanLR04">
			<select name="categorias" id="categorias" style="width:175px">
				<option value="">Seleccione...</option>
				<?php foreach($rsTiposCategorias as $key => $rTipoCategoria){ ?>
					<option value="<?=$rTipoCategoria{'indice'}?>" <?=($rTipoCategoria{'indice'}==$rEntidad{'idCategorias'} ? "selected='selected'" : "");?> ><?=utf8_encode($rTipoCategoria{'Categoria'})?></option>
				<?php } ?>
			</select>
		</td>		
		<td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" align="absmiddle"> Centro de Costo</td>	    
        <td width="50%" nowrap class="sanLR04">
        	<select name="area" id="area" style="width:175px">
				<option value="">Seleccione...</option>
				<?php foreach($rsTiposArea as $key => $rTipoArea){ ?>
					<option value="<?=$rTipoArea{'eCodCosto'}?>" <?=($rTipoArea{'eCodCosto'}==$rEntidad{'area'} ? "selected='selected'" : "");?> ><?=utf8_encode($rTipoArea{'tArea'})?></option>
				<?php } ?>
			</select>
        </td>        
    </tr>
    <tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Salario </td>
	    <td width="50%" nowrap class="sanLR04"><input name="Salario" type="text" dojoType="dijit.form.TextBox" id="Salario" value="<?=utf8_encode($rEntidad{'Salario'});?>" style="width:175px"></td>
	    <td height="23" nowrap class="sanLR04"></td>
	    <td width="50%" nowrap class="sanLR04"></td>	    
    </tr>
    <tr>
	    <td height="23" nowrap class="sanLR04" valign="top"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Motivo Cancelación </td>
	    <td width="100%" colspan="4" nowrap class="sanLR04"><textarea rows="2" name="cancelacion" id="cancelacion" cols="70"></textarea></td>
	</tr>
	<tr>
	    <td height="23" nowrap class="sanLR04" valign="top"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Motivo de Modificación </td>
	    <td width="100%" colspan="4" nowrap class="sanLR04"><textarea rows="2" name="tModificacion" id="tModificacion" cols="70"></textarea></td>
	</tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle" /> <font class="fntG10K">Indica que es obligatorio llenar el campo.
    </font></td>
    </tr>
</table>
</form>
<?php } ?>