﻿<?php 
require_once("conexion/soluciones-mysql.php"); 
$link = getLink();
if($_POST){
	if($_POST['eAccion']==1){
		foreach($_POST as $k => $v){
			$nombre = strval($k);
			if(strstr($nombre,"eCodReingreso") && $v){
				$eFila=str_replace("eCodReingreso", "", $nombre);
				$update=" UPDATE asistencias SET folioBaja='".$_POST['folio']."', fechaBaja=CURRENT_TIMESTAMP WHERE id=".$v;
				mysqli_query($link,$update);
			}
		}
		foreach($_POST as $k => $valor){
			$nombre = strval($k);
			$campo = "eCodCosto";
			if(strstr($nombre,$campo) && (int)$valor>0){
				$update=" UPDATE asistencias SET folioBaja=".$_POST['folio']." WHERE id=".$valor;
				mysqli_query($link,$update);
			}
		}
	}
date_default_timezone_set('America/Mexico_City');
$varfecha=isset($_POST["fecha"]) ? $_POST["fecha"] : date("Y-m-d");
$fechabaja = date('Y-m-d',strtotime('-2 days', strtotime($varfecha)));
$fechaayer = date('Y-m-d',strtotime('-1 days', strtotime($varfecha)));
$fechavalidanext = date('Y-m-d',strtotime('-3 days', strtotime($varfecha)));

//Consulta para validar si tienen continuacion de asistencia dia siguiente mismo empleado.
$dianext="SELECT asistencias.idEmpleado                       
FROM asistencias                        
where asistencias.Estado='AC' AND asistencias.Fecha ='".$fechaayer."'";

//print($dianext."<br><br>");

//Consulta en base a la fecha de baja, si estan protegidos por una incapacidad
$incapacitados="SELECT incapacidades.idEmpleado                       
FROM incapacidades                        
WHERE incapacidades.Estado='AC' AND incapacidades.FechaInicial <='".$fechaayer."' AND incapacidades.FechaFinal >='".$fechaayer."' AND idEmpleado not in($dianext)";

//print($incapacitados."<br><br>");                        

//Consulto si tiene algun empleado tiene continuacion de incapacidad.
/*$protegidosNext="SELECT incapacidades.idEmpleado                       
FROM incapacidades                        
WHERE 1=1 and incapacidades.Estado=1 AND incapacidades.FechaInicial ='".$fechabaja."' AND incapacidades.idEmpleado not IN(".$incapacitados.")";
*/
//Consulta los registros para mandarlos a baja, en base a la fecha final de incapacidad, exeptuando los que tienen continuacion de incapacidad
$protegidos="SELECT incapacidades.idEmpleado                       
FROM incapacidades                        
WHERE incapacidades.Estado='AC' AND incapacidades.FechaFinal ='".$fechabaja."' AND idEmpleado not in($dianext)";
//print($protegidosNext."<br><br>");
//print($protegidos."<br><br>");

$yaestan="SELECT asistencias.idEmpleado
FROM asistencias 
WHERE asistencias.Estado='AC' AND asistencias.Fecha= '".$fechabaja."' AND asistencias.folioBaja > 0";

//Consulta todos los empleados en base su fecha de asistencia listos para irse a baja.
$select="SELECT 
empleados.id, 
asistencias.id AS numeral,
asistencias.idEmpresa,
asistencias.Fecha,
asistencias.Turno,
asistencias.Estado AS tCodEstatus,
asistencias.folioIMSS,
asistencias.folioBaja,
catentidades.eCodEntidad AS iden,
catentidades.tRegistroPatronal AS rp,
asistencias.idEmpleado,
empleados.IMSS AS imss,
empleados.Paterno AS pat,
empleados.Materno AS mat,
empleados.Nombre AS nomb,
empleados.Empleado,
empleados.Clinica AS clin,
empleados.CURP AS curp
FROM asistencias                        
LEFT JOIN catentidades ON asistencias.idEmpresa = catentidades.eCodEntidad
LEFT JOIN empleados ON asistencias.idEmpleado = empleados.id 
where 
(asistencias.Fecha ='".$fechabaja."' or empleados.id in (".$protegidos.")) 
AND empleados.id not in (".$dianext.") 
AND empleados.id not in (".$incapacitados.")
AND empleados.id not in (".$yaestan.") 
AND empleados.Base='0' 
AND asistencias.folioBaja='0' 
AND asistencias.Estado='AC'
group by asistencias.idEmpleado
order by empleados.id";	  

    //print($select);
	  $rsServicios=mysqli_query($link,$select); 
	  $registros=(int)mysqli_num_rows($asi);                     
	  $cont=0;	
	  $rsUsuarios=mysqli_query($link,$select);
	  $registros=(int)mysqli_num_rows($rsUsuarios);
	?>

<table cellspacing="0" border="0" width="980px">
  <tr>
    <td width="50%"><hr color="#666666" /></td>
    <td class="sanLR04" nowrap="nowrap"><b>Registros Encontrados: (<?=$registros;?>)</b></td>
    <td width="50%"><hr color="#666666" /></td>
  </tr>
</table>
<div style="display:block; top:0; left:0; width:980px; z-index=1; overflow: auto;">
  <table cellspacing="0" border="0" width="980px">
    <thead>
      <tr class="thEncabezado">	  
      	<td nowrap="nowrap" class="sanLR04" height="20" align="center">C</td>
        <td nowrap="nowrap" class="sanLR04" align="center">E</td>
        <td nowrap="nowrap" class="sanLR04">C&oacute;digo</td>
	    <td nowrap="nowrap" class="sanLR04">Empleado</th>
	    <td nowrap="nowrap" class="sanLR04">Turno</td> <!--Ya quedo-->
	    <td nowrap="nowrap" class="sanLR04">Fecha</td> <!--Ya quedo-->
	    <td nowrap="nowrap" class="sanLR04">F. Reingreso / Modificaci&oacute;n</td> <!--Ya quedo-->                    
      <td nowrap="nowrap" class="sanLR04" width="100%">F. Baja</td> <!--Ya quedo-->                    
	  </tr>   
    </thead>
    <tbody>
      <?php $i=1; while($rServicio=mysqli_fetch_array($rsServicios,MYSQLI_ASSOC)){
      				if($rServicio{'tCodEstatus'})
      				{
      					$tCodEstatus="AC";
      				}
      				else
      				{
      					$tCodEstatus="CA";
      				}
				?>
      <tr>
        <td nowrap="nowrap" class="sanLR04" height="20" align="center"><input type="checkbox" checked name="eCodReingreso<?=$i?>" id="eCodReingreso<?=$i?>" value="<?=$rServicio{'numeral'};?>" ></td>
        <td nowrap="nowrap" class="sanLR04" height="20" align="center"><img width="16" height="16" alt="Usuario" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-<?=$tCodEstatus;?>.png"></td>
        <input type="hidden" id="id<?=$i?>" name="id<?=$i?>" value="<?=$rServicio["numeral"]?>">
        <td nowrap="nowrap" class="sanLR04 colmenu"><b><?=sprintf("%07d",$rServicio{'numeral'});?></b></a></td>                              
        <td nowrap="nowrap" class="sanLR04"><?php echo utf8_decode($rServicio["Empleado"]); ?></td>
        <td nowrap="nowrap" class="sanLR04 columnB" ><?php echo $rServicio["Turno"]; ?></td>
        <td nowrap="nowrap" class="sanLR04"><?php echo $rServicio["Fecha"]; ?></td>
        <td nowrap="nowrap" class="sanLR04 columnB"><?php echo $rServicio["folioIMSS"]; ?></td>
        <td nowrap="nowrap" class="sanLR04"><?php echo $rServicio["folioBaja"]; ?></td>
      </tr>
      <?php $i++; } ?>
    </tbody>
  </table>
</div>
<?php }else{ ?>
<script type="text/javascript">
function nuevo(){
	document.location = "?ePagina=2.3.3.1.1.php";
}

function consultar(){
  document.location = "?ePagina=2.5.1.1.php";
}

function guardar(){
	var eChk = 0;
	var bandera = false;
	var mensaje = "¡Verifique lo siguiente!\n";
	dojo.byId('eAccion').value = 1;	
	dojo.query('input[id^=eCodReingreso]:checked').forEach(function(nodo, index, arr){
		eChk++;
	});
	
	if(!dojo.byId("folio").value){
		mensaje+="* Folio\n";
		bandera = true;   
	}
	if(eChk==0){
		mensaje+="* Seleccione Empleado\n";
		bandera = true;  
	}
	if(bandera==true){
		alert(mensaje);
	}else{
		if(confirm("¿Desea registrar el folio?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+'.php', handleAs: "text", load: function(tRespuesta, ioArgs){
				dojo.byId('dvCNS').innerHTML = tRespuesta;
				alert("¡El Registro se realizo exitosamente!");	
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});
		}
	}
	dojo.byId('eAccion').value = "";	
	dojo.byId('folio').value = "";	
}
dojo.addOnLoad(function(){filtrarConsulta();});
</script>
<form name="Datos" id="Datos" method="post" action="" onsubmit="return false;">
  <input type="hidden" value="0" name="ePagina" id="ePagina" />
  <input type="hidden" value="" name="eAccion" id="eAccion" />
  <table width="900px" border="0">
    <tr>
      <td colpan="3" width="100%"></td>
      <td align="right" nowrap="nowrap"><a class="fntBR11" href="javaScript:mostrarFiltros('Busqueda');">Busqueda de Registros</a></td>
    </tr>
    <tr id="trBusqueda" style="display:none">
      <td colspan="4"><table width="900px" bgcolor="#f9f9f9">
          <tr>
            <td class="sanLR04" height="5"></td>
          </tr>
          <tr>
            <td class="sanLR04" height="20">Fecha</td>
            <td class="sanLR04" width="50%"><input type="date" name="fecha" dojoType="dijit.form.TextBox" id="fecha" value="<?=$_POST["fecha"] ? $_POST["fecha"] : date("Y-m-d")?>" style="width:140px"></td>
            <td class="sanLR04">Folio</td>
            <td class="sanLR04" width="50%"><input type="text" name="folio" dojoType="dijit.form.TextBox" id="folio" value="" style="width:80px"></td>
          </tr>
          <tr>
            <td class="sanLR04" height="5"></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td colspan="4"><div id="dvCNS"></div></td>
    </tr>
  </table>
</form>
<?php } ?>
