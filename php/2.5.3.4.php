<?php
require_once("conexion/soluciones-mysql.php"); 
$link = getLink();
if($_POST){
	if($_POST['fhFechaAsistenciaInicio']!=""){  		
  		$fhFechaAsistenciaInicio = date("Y-m-d", strtotime('+2 days', strtotime($_POST['fhFechaAsistenciaInicio'])));  		
  		$fhFechaAsistenciaFin =  date("Y-m-d", strtotime('+1 week 1 days', strtotime($_POST['fhFechaAsistenciaInicio'])));
 	}
	$select=" select emp.id AS idEmpleado, ast.Salario,  ast.Fecha, ast.Turno,  ast.Estado as tCodEstatus, emp.Empleado AS tNombre,   ".			
			" tpo.eCodTipoEntidad,  tpo.tNombreCorto ".
			" FROM empleados AS emp ".			
			" INNER JOIN  asistencias ast ON ast.idEmpleado=emp.id AND ast.Estado='AC' ".
			" LEFT JOIN cattipoempleado tpo ON emp.eCodTipo=tpo.eCodTipoEntidad ".			
			" WHERE emp.Estado='AC' ".			
			($_POST['fhFechaAsistenciaInicio']	? " AND ast.Fecha BETWEEN '".$fhFechaAsistenciaInicio. "' AND '" .$fhFechaAsistenciaFin."'" : "" ).			
			" group by emp.id Order by emp.id ".( $_POST['orden'] ? $_POST['orden']  : "ASC" )." LIMIT ".( $_POST['limite'] ? $_POST['limite']  : "100");
	$rsSolicitudes=mysqli_query($link,$select);
	$registros=(int)mysqli_num_rows($rsSolicitudes); ?>
    <table cellspacing="0" border="0" width="965px">
		<tr>
			<td width="50%"><hr color="#666666" /></td>
			<td class="sanLR04" nowrap="nowrap"><b>Registros Encontrados: ( <?=$registros;?> )</b></td>
			<td width="50%"><hr color="#666666" /></td>
		</tr>
    </table>
    <div style="display:block; top:0; left:0; width:965px; z-index=1; overflow: auto; height:400px;">
        <table cellspacing="0" border="0" width="965px" border="1">
            <thead>
                <tr class="thEncabezado">
                    <td nowrap="nowrap" class="sanLR04" height="20" align="center"></td>
                    <td nowrap="nowrap" class="sanLR04"></td>				
                    <td nowrap="nowrap" class="sanLR04"></td>
                    <td nowrap="nowrap" class="sanLR04" ></td>				
                    <td colspan="3" rowspan="2" nowrap="nowrap" class="sanLR04 colmenu">Miércoles</td>
                    <td colspan="3" rowspan="2" nowrap="nowrap" class="sanLR04 colmenu" >Jueves</td>
                    <td colspan="3" rowspan="2" nowrap="nowrap" class="sanLR04 colmenu" >Viernes</td>
                    <td colspan="3" rowspan="2" nowrap="nowrap" class="sanLR04 colmenu" >Sabado</td>
                    <td colspan="3" rowspan="2" nowrap="nowrap" class="sanLR04 colmenu" >Lunes</td>				
                    <td colspan="3" rowspan="2" nowrap="nowrap" class="sanLR04 colmenu">Martes</td>
                    <td rowspan="2" nowrap="nowrap" class="sanLR04 colmenu" valign="middle">Total</td>
                    <td rowspan="2" class="sanLR04 colmenu" valign="middle">SEPTIMO DIA NOMINA <?=substr($_POST['fhFechaAsistenciaInicio'],-2);?></td>
                    <td rowspan="2" class="sanLR04 colmenu" valign="middle">SEPTIMO DIA <?=((substr($_POST['fhFechaAsistenciaInicio'],-2)+1)>52 ? 1 : substr($_POST['fhFechaAsistenciaInicio'],-2)+1);?></td>
                    <td rowspan="2" nowrap="nowrap" class="sanLR04 colmenu" valign="middle">Total</td>
                    <td rowspan="2" class="sanLR04 colmenu" valign="middle">Total Turno 1</td>
                    <td rowspan="2" class="sanLR04 colmenu" valign="middle">Total Turno 2</td>
                    <td rowspan="2" class="sanLR04 colmenu" valign="middle">Total Turno 3</td>
                    <td rowspan="2" class="sanLR04 colmenu" valign="middle">Días Laborados</td>
                    <td rowspan="2" class="sanLR04 colmenu" valign="middle">% 7mo día</td>
                    <td rowspan="2" class="sanLR04 colmenu" valign="middle">Días</td>
                    <td rowspan="2" class="sanLR04 colmenu" valign="middle">S.D.N</td>
                </tr>
            </thead>
            <thead>
                <tr class="thEncabezado">
                    <td nowrap="nowrap" class="sanLR04" height="20" align="center"></td>
                    <td nowrap="nowrap" class="sanLR04"></td>				
                    <td nowrap="nowrap" class="sanLR04"></td>
                    <td nowrap="nowrap" class="sanLR04" ></td>				
                    <td colspan="3" nowrap="nowrap" class="sanLR04 colmenu">
						<?=date("d/m/Y", strtotime('+2 days', strtotime($_POST['fhFechaAsistenciaInicio'])));?>
                    </td>
                    <td colspan="3" nowrap="nowrap" class="sanLR04 colmenu">
						<?=date("d/m/Y", strtotime('+3 days', strtotime($_POST['fhFechaAsistenciaInicio'])));?>
                    </td>
                    <td colspan="3" nowrap="nowrap" class="sanLR04 colmenu">
						<?=date("d/m/Y", strtotime('+4 days', strtotime($_POST['fhFechaAsistenciaInicio'])));?>
                    </td>
                    <td colspan="3" nowrap="nowrap" class="sanLR04 colmenu">
						<?=date("d/m/Y", strtotime('+5 days', strtotime($_POST['fhFechaAsistenciaInicio'])));?>
                    </td>
                    <td colspan="3" nowrap="nowrap" class="sanLR04 colmenu">
						<?=date("d/m/Y", strtotime('+7 days', strtotime($_POST['fhFechaAsistenciaInicio'])));?>
                    </td>
                    <td colspan="3" nowrap="nowrap" class="sanLR04 colmenu" width="100%">
						<?=date("d/m/Y", strtotime('+8 days', strtotime($_POST['fhFechaAsistenciaInicio'])));?>
                    </td>
                    <td nowrap="nowrap" class="sanLR04 colmenu"></td>
                    <td nowrap="nowrap" class="sanLR04 colmenu"></td>
                    <td nowrap="nowrap" class="sanLR04 colmenu"></td>
                    <td nowrap="nowrap" class="sanLR04 colmenu"></td>
                    <td nowrap="nowrap" class="sanLR04 colmenu"></td>
                    <td nowrap="nowrap" class="sanLR04 colmenu"></td>
                    <td nowrap="nowrap" class="sanLR04 colmenu"></td>
                    <td nowrap="nowrap" class="sanLR04 colmenu"></td>
                    <td nowrap="nowrap" class="sanLR04 colmenu"></td>
                    <td nowrap="nowrap" class="sanLR04 colmenu"></td>
                    <td nowrap="nowrap" class="sanLR04 colmenu" width="100%"></td>
                </tr>
            </thead>
            <thead>
                <tr class="thEncabezado">
                    <td nowrap="nowrap" class="sanLR04" height="20" align="center">E</td>
                    <td nowrap="nowrap" class="sanLR04">C&oacute;digo</td>				
                    <td nowrap="nowrap" class="sanLR04">T. Trabajador</td>
                    <td nowrap="nowrap" class="sanLR04" >Empleado</td>
                    <td nowrap="nowrap" class="sanLR04" >1er Turno</td><!--Miercoles -->
                    <td nowrap="nowrap" class="sanLR04" >2do Turno</td>
                    <td nowrap="nowrap" class="sanLR04" >3er Turno</td>
                    <td nowrap="nowrap" class="sanLR04" >1er Turno</td><!--Jueves -->
                    <td nowrap="nowrap" class="sanLR04" >2do Turno</td>
                    <td nowrap="nowrap" class="sanLR04" >3er Turno</td>
                    <td nowrap="nowrap" class="sanLR04" >1er Turno</td><!--Viernes -->
                    <td nowrap="nowrap" class="sanLR04" >2do Turno</td>
                    <td nowrap="nowrap" class="sanLR04" >3er Turno</td>
                    <td nowrap="nowrap" class="sanLR04" >1er Turno</td><!--Sabado -->
                    <td nowrap="nowrap" class="sanLR04" >2do Turno</td>
                    <td nowrap="nowrap" class="sanLR04" >3er Turno</td>
                    <td nowrap="nowrap" class="sanLR04" >1er Turno</td><!--Lunes -->
                    <td nowrap="nowrap" class="sanLR04" >2do Turno</td>
                    <td nowrap="nowrap" class="sanLR04" >3er Turno</td>
                    <td nowrap="nowrap" class="sanLR04" >1er Turno</td><!--Martes -->
                    <td nowrap="nowrap" class="sanLR04" >2do Turno</td>				
                    <td nowrap="nowrap" class="sanLR04" >3er Turno</td>	
                    <td nowrap="nowrap" class="sanLR04" ></td>
                    <td nowrap="nowrap" class="sanLR04" ></td>
                    <td nowrap="nowrap" class="sanLR04" ></td>
                    <td nowrap="nowrap" class="sanLR04" ></td>
                    <td nowrap="nowrap" class="sanLR04" ></td>
                    <td nowrap="nowrap" class="sanLR04" ></td>
                    <td nowrap="nowrap" class="sanLR04" ></td>
                    <td nowrap="nowrap" class="sanLR04" ></td>
                    <td nowrap="nowrap" class="sanLR04" ></td>
                    <td nowrap="nowrap" class="sanLR04" ></td>
                    <td nowrap="nowrap" class="sanLR04" width="100%"></td>				
                </tr>
            </thead>
			<tbody>
            	<?php $i=1;
				while($rSolicitud=mysqli_fetch_array($rsSolicitudes,MYSQLI_ASSOC)){
					$select=" SELECT cte.dFactorIntegracion ".
							" FROM empleados emp ".
							" INNER JOIN cattipoempleado cte ON cte.eCodTipoEntidad=emp.eCodTipo ".
							" WHERE emp.id=".$rSolicitud{'idEmpleado'};
					$rFactor=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC); ?>
                    <tr>
                    	<td nowrap="nowrap" class="sanLR04" height="20" align="center"><img width="16" height="16" alt="Empresa" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-<?=$rSolicitud{'tCodEstatus'};?>.png"></td>
                        <td nowrap="nowrap" class="sanLR04 colmenu"><b><?=sprintf("%07d",$rSolicitud{'idEmpleado'});?></b></td>
                        <td nowrap="nowrap" class="sanLR04 " ><?=utf8_encode($rSolicitud{'tNombreCorto'});?></td>					
                        <td nowrap="nowrap" class="sanLR04 columnB" ><?=utf8_encode($rSolicitud{'tNombre'});?></td>
                    	<? 
						$count=$fhFechaAsistenciaInicio;
						$countsp=date("Y-m-d" ,strtotime('-7 days' , strtotime($count))); 
						$bFondo=0;
						$etotalEmpleado=0;
						$etotalEmpleadoSP=0;
						$dTotalSeptimo=0;
						$dTurnoUno=0;
						$dTurnoDos=0;
						$dTurnoTres=0;
						$dDiasLaborados=0;
						while($count<=$fhFechaAsistenciaFin){
							// Ciclo de dias
							$turno=1;
							$ValidarDia=1;
							while($turno<=3){
								 // ciclo de turnos
								$sql=" SELECT ast.Fecha, ast.Salario, ast.Turno, cte.dFactorIntegracion ".
									 " FROM asistencias AS ast ".
									 " INNER JOIN empleados emp ON emp.id=ast.idEmpleado ".
									 " INNER JOIN cattipoempleado cte ON cte.eCodTipoEntidad=emp.eCodTipo ".
									 " WHERE ast.Estado='AC' ".
									 " AND ast.idEmpleado=".$rSolicitud{'idEmpleado'}." AND ast.Fecha='".$count."' AND ast.Turno=".$turno.
									 " ORDER by ast.Turno ASC ";
								$rAsistencias=mysqli_fetch_array(mysqli_query($link,$sql),MYSQLI_ASSOC);
								if(date("w",strtotime($count))>0){ ?>
                                	<td nowrap="nowrap" class="sanLR04 <?=($bFondo==1 ? "columnB" : "");?>" >                                		
										<?=($rAsistencias{'Salario'} ? number_format($rAsistencias{'Salario'}/$rAsistencias{'dFactorIntegracion'},2) : "N/D");?>
                                    </td>
									<?php
									$dTurno=((float)$rAsistencias{'Salario'}>0 ? str_replace(',', '', number_format($rAsistencias{'Salario'}/$rAsistencias{'dFactorIntegracion'},2)) : 0);
									
									if($ValidarDia and $rAsistencias{'Salario'})
									{
										$dDiasLaborados=$dDiasLaborados+1;
										$ValidarDia=0;
									}

									if($turno==1)
									{
									 $dTurnoUno=$dTurnoUno+$dTurno;	
									}
									if($turno==2)
									{
									 $dTurnoDos=$dTurnoDos+$dTurno;	
									}
									if($turno==3)
									{
									 $dTurnoTres=$dTurnoTres+$dTurno;	
									}

									$etotalEmpleado+=$dTurno;
									$bFondo=($turno==3 ? ($bFondo==1 ? 0 : 1) : $bFondo);
								}
								$turno++;
							} // fin turno
							if(date("w",strtotime($countsp))>0){
								$sql=" SELECT sum(ast.Salario) AS Salario ".
									 " FROM asistencias AS ast ".
									 " INNER JOIN empleados emp ON emp.id=ast.idEmpleado ".
									 " INNER JOIN cattipoempleado cte ON cte.eCodTipoEntidad=emp.eCodTipo ".
									 " WHERE ast.Estado='AC' ".
									 " AND ast.idEmpleado=".$rSolicitud{'idEmpleado'}." AND ast.Fecha='".$countsp."'";
								$rAsistSemPas=mysqli_fetch_array(mysqli_query($link,$sql),MYSQLI_ASSOC);
								$dTurnoSP=((float)$rAsistSemPas{'Salario'}>0 ? str_replace(',', '', number_format($rAsistSemPas{'Salario'}/$rFactor{'dFactorIntegracion'},2)) : 0);
								$etotalEmpleadoSP+=$dTurnoSP;
								$dTotalSeptimo=$etotalEmpleado+$etotalEmpleadoSP*0.1666;
								$Dias=(1/6)*$dDiasLaborados;
								$DiasTotal=$Dias+$dDiasLaborados;
							}
							$count=date("Y-m-d" ,strtotime('+1 days' , strtotime($count)));
							$countsp=date("Y-m-d" ,strtotime('+1 days' , strtotime($countsp)));
						} ?>
                        <td nowrap="nowrap" class="sanLR04" align="right"><?=number_format($etotalEmpleado,2);?></td>	
                        <td nowrap="nowrap" class="sanLR04 columnB" align="right"><?=number_format(($etotalEmpleadoSP*0.1666),2);?></td>
                        <td nowrap="nowrap" class="sanLR04" align="right"><?=number_format(($etotalEmpleado*0.1666),2);?></td>
                        <td nowrap="nowrap" class="sanLR04 columnB" align="right"><?=number_format($dTotalSeptimo,2);?></td>
                        <td nowrap="nowrap" class="sanLR04" align="right"><?=number_format($dTurnoUno,2);?></td>
                        <td nowrap="nowrap" class="sanLR04 columnB" align="right"><?=number_format($dTurnoDos,2);?></td>
                        <td nowrap="nowrap" class="sanLR04" align="right"><?=number_format($dTurnoTres,2);?></td>
                        <td nowrap="nowrap" class="sanLR04 columnB" align="right"><?=utf8_encode($dDiasLaborados);?></td>
                        <td nowrap="nowrap" class="sanLR04" align="right"><?=number_format($Dias,2);?></td>
                        <td nowrap="nowrap" class="sanLR04 columnB" align="right"><?= number_format($DiasTotal,2);?></td>
                        <td nowrap="nowrap" class="sanLR04" align="right"><?=number_format($dTotalSeptimo/$DiasTotal,2)?></td>
                    </tr>
					<?php
					$i++;
				} ?>
			</tbody>
        </table>
    </div>
	<?php } else { 
	$select=" SELECT * ".
			" FROM categorias ".
			" WHERE tCodEstatus='AC' order by Categoria ";
	$rsTiposCategorias=mysqli_query($link,$select);
	?>
	<script type="text/javascript">
	dojo.require("dojo.number");
	dojo.require("dijit.form.NumberTextBox");
	dojo.require("dijit.form.DateTextBox");
	dojo.require("dojo.io.iframe");
	
	function nuevo(){
		document.location = "?ePagina=2.4.1.1.php";
	}

	function generaExcel(){
		var UrlExcel = "php/excel/2.5.3.4.php?"+
		(dojo.byId('eCodAsistencia').value ? "&eCodAsistencia="+dojo.byId('eCodAsistencia').value : "")+
		(dojo.byId('fhFechaAsistenciaInicio').value ? "&fhFechaAsistenciaInicio="+dojo.byId('fhFechaAsistenciaInicio').value : "")+
		(dojo.byId('fhFechaAsistenciaFin').value ? "&fhFechaAsistenciaFin="+dojo.byId('fhFechaAsistenciaFin').value : "")+
		(dojo.byId('eCodTipoCategoria').value ? "&eCodTipoCategoria="+dojo.byId('eCodTipoCategoria').value : "")+
		(dojo.byId('eCodTurno').value ? "&eCodTurno="+dojo.byId('eCodTurno').value : "")+
		(dojo.byId('Empleado').value ? "&Empleado="+dojo.byId('Empleado').value : "")+		
		(dojo.byId('folio').value ? "&folio="+dojo.byId('folio').value : "")+		
		(dojo.byId('tipoMovimiento').value ? "&tipoMovimiento="+dojo.byId('tipoMovimiento').value : "")+
		(dojo.byId('EcodEstatus').value ? "&EcodEstatus="+dojo.byId('EcodEstatus').value : "")+
		(dojo.byId('orden').value ? "&orden="+dojo.byId('orden').value : "")+
		(dojo.byId('limite').value ? "&limite="+dojo.byId('limite').value : "");		
		var Datos = document.createElement("FORM");
		document.body.appendChild(Datos);
		Datos.name='Datos';
		Datos.method = "POST";
		Datos.action = UrlExcel;
		Datos.submit();
		}
	
	dojo.addOnLoad(function(){filtrarConsulta();});
	</script>
	<form name="Datos" id="Datos" method="post" action="" onsubmit="return false;">
		<input type="hidden" value="0" name="ePagina" id="ePagina" />
		<input type="hidden" value="" name="eAccion" id="eAccion" />
		<input type="hidden" value="<?=(int)$_SESSION['sesionUsuario']['eCodEntidad'];?>" name="eEntidad" id="eEntidad" />
		<table width="965px" border="0">
			<tr>
				<td colpan="3" width="100%"></td>
				<td align="right" nowrap="nowrap"><a class="fntBR11" href="javaScript:mostrarFiltros('Busqueda');">Busqueda de Registros</a></td>
			</tr>
			<tr id="trBusqueda" style="display:none">
				<td colspan="4">
					<table width="100%"  bgcolor="#f9f9f9">
						<tr><td class="sanLR04" height="5"></td></tr>
						<tr>
							<td class="sanLR04" height="20"><b>No. Semana Nomina</b></td>
							<td class="sanLR04" width="40%"><input name="fhFechaAsistenciaInicio" id="fhFechaAsistenciaInicio" type="week"  required="false" value="<?=date('Y')."-W".date("W", strtotime(date('Y-m-d'))); ?>" /></td>
							<td class="sanLR04" nowrap="nowrap"></td>
							<td class="sanLR04" width="50%"></td>
						</tr>												
						<tr>
			              <td nowrap="nowrap" class="sanLR04" height="5"><b>[Ordenamiento]</b></td>
			              <td nowrap="nowrap" class="sanLR04" width="50%"><input type="radio" name="orden" id="orden"  checked="checked" value="ASC" > ASCENDENTE / <input type="radio" name="orden" id="orden"  value="DESC" > DESENDENTE</td>
			              
			              <td nowrap="nowrap" class="sanLR04"><b>[Maximos Registros]:</b></td>
			              <td nowrap="nowrap" class="sanLR04" width="50%">
			              	<select name="limite" id="limite" style="width:142px; height:25px;">
                                  <option value="100" selected>100</option>
                                  <option value="1000" >1,000</option>
                                  <option value="10000" >10,000</option>
                                  <option value="100000">100,000</option>
                          	</select>
                          </td>
            			</tr>
						<tr><td class="sanLR04" height="5"></td></tr>
					</table>
				</td>
			</tr>
			<tr><td colspan="4"><div id="dvCNS"></div></td></tr>
		</table>
	</form>
<?php } ?>