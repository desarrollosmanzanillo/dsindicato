<?php
require_once("../conexion/soluciones-mysql.php");
$link = getLink();
//$link=Conectarse();
$directorio="ALTAS.dat";

date_default_timezone_set('America/Mexico_City');
$varfecha=isset($_GET["fecha"]) ? $_GET["fecha"] : date("Y-m-d");
$fechaayer = date('Y-m-d',strtotime('-1 days', strtotime($varfecha)));   

    $registrado="SELECT asi.idEmpleado ".
    " FROM asistencias AS asi ".
    " LEFT JOIN relasistenciamovimientos AS rel ON rel.eCodAsistencia=asi.id ".
    " LEFT JOIN movimientosafiliatorios AS mov ON mov.eCodMovimiento=rel.eCodMovimiento ".
    " WHERE asi.Estado='AC' AND asi.fecha='".$varfecha."' AND mov.tFolio>'0'";
    
    $select=" SELECT EMP.id, chek.id AS numeral, chek.idEmpresa, chek.Turno, EMP.Empleado, chek.folioIMSS, chek.Estado as tCodEstatus, chek.dBono, 
          chek.Fecha, INC.eCodEntidad AS iden, INC.tRegistroPatronal AS rp,
          chek.idEmpleado, EMP.IMSS AS imss, EMP.Paterno AS pat,
          EMP.Materno AS mat, EMP.Nombre AS nomb, tem.eCodTipoSalario as TipoSalario, tem.dFactorIntegracion as FactorIntegracion, tem.eCodTipoJornada, tem.eCodTipoTrabajador, 
          EMP.Clinica AS clin, EMP.CURP AS curp, CAT.Turno1, CAT.Turno2, CAT.Turno3
          FROM asistencias AS chek          
          LEFT JOIN empleados AS EMP ON chek.idEmpleado = EMP.id
          INNER JOIN categorias AS CAT ON chek.idCategorias = CAT.indice          
          INNER JOIN cattipoempleado AS tem ON tem.eCodTipoEntidad=EMP.eCodTipo          
          LEFT JOIN catentidades AS INC ON chek.idEmpresa = INC.eCodEntidad
          LEFT JOIN relasistenciamovimientos AS rel ON rel.eCodAsistencia=chek.id 
          LEFT JOIN asistencias AS asi ON asi.idEmpleado=chek.idEmpleado AND asi.Estado='AC' AND asi.Fecha='".$fechaayer."'
          WHERE chek.Estado='AC' AND rel.eCodAsistencia is NULL AND chek.Fecha ='".$varfecha."' AND asi.idEmpleado is NULL AND chek.idEmpleado not in($registrado)
          group by chek.idEmpleado
          order by chek.Turno, EMP.Empleado ";

//print($sql);
  $asi=mysqli_query($link,$select);
  if(file_exists($directorio))
  {    
    if(unlink($directorio))
    {
    $ar=fopen($directorio,"a") or die("Problemas en la creacion");
    $count=0;
    $select=" SELECT * FROM sisconfiguracion ";
    $configuracion=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
        while($asist=mysqli_fetch_array($asi,MYSQLI_ASSOC))
        {
          $count=$count+1;
                      $salario=0;
                      if($asist["Turno"]==1)
                        $salario=$asist["Turno1"];
                      if($asist["Turno"]==2)
                        $salario=$asist["Turno2"];
                      if($asist["Turno"]==3)
                        $salario=$asist["Turno3"];
                        $monto=$salario*$asist["FactorIntegracion"];
                        $salario=number_format($salario*$asist["FactorIntegracion"],2);
                        $salario=$salario+$asist["dBono"];
                        if( (float)$configuracion{'dTopeSalario'} < (float)$monto )
                        {
                          $salario=number_format((float)$configuracion{'dTopeSalario'},2);
                        }
                        $salario=explode(".",$salario);
                        if(strlen($salario[0])<=3)
                          $cadena="0".$salario[0].$salario[1];
                        else
                          $cadena=str_replace(",", "", $salario[0]).$salario[1];

            fputs($ar,str_pad($asist['rp'], 11));
            fputs($ar,str_pad($asist['imss'], 11));
            fputs($ar,str_pad(($asist['pat']), 27));
            fputs($ar,str_pad(($asist['mat']), 27));
            fputs($ar,str_pad(($asist['nomb']), 27));            
            fputs($ar,str_pad($cadena, 6));//salario
            //fputs($ar,str_pad(((float)$configuracion{'dTopeSalario'}<(float)(str_replace(",", "", $salario[0])+($salario[1]/100)) ? (float)$configuracion{'dTopeSalario'} : $cadena ), 6));//salario
            fputs($ar,str_pad('000000', 6));//fillier
            fputs($ar,str_pad(($asist['eCodTipoTrabajador']), 1));//tipo de trabajador
            fputs($ar,str_pad(($asist['TipoSalario']), 1));//tipo de salario
            fputs($ar,str_pad(($asist['eCodTipoJornada']), 1));//semana jornada
            $fech=$asist["Fecha"]; $fecha=explode("-",$fech); $abc=$fecha[2].$fecha[1].$fecha[0]; echo $abc;
            fputs($ar,str_pad($abc, 8));
            fputs($ar,str_pad($asist['clin'], 3)); //clinica
            fputs($ar,str_pad('  ', 2)); //Filler (anote blancos)
            fputs($ar,str_pad('08', 2));//movimiento
            fputs($ar,str_pad('03400', 5));//guia
            fputs($ar,str_pad('ESTIBADOR', 10));//estibador
            fputs($ar,str_pad('', 1));//FillierB
            fputs($ar,str_pad(($asist['curp']), 18));//curp
            fputs($ar,str_pad('9', 1)."\r\n"); //indicador
        }
        mysqli_free_result($asi);
        fputs($ar,str_pad("*************",56));
        fputs($ar,str_pad($count,6,"0",STR_PAD_LEFT));
        fputs($ar,"                                                                       ");
        fputs($ar,"03400"); 
        fputs($ar,"                             9");       
        fclose($ar); 
        $enlace = $directorio;        
        header ("Location: halta.php");
        /*?>
      <script type="text/javascript"> 
      alert('Exito, se reemplazo el archivo Altas');
      window.location='../batch/altas.php';
      </script>
      <?php*/
    }
    else
    { 
      ?>
      <script type="text/javascript"> 
      alert('Error, no se pudo eliminar el archivo anterior');
      //indow.location='../batch/altas.php';
      </script>
      <?php
    }
   } 
  else
  {
    $ar=fopen($directorio,"a") or
    die("Problemas en la creacion");
    $count=0;
    $select=" SELECT * FROM sisconfiguracion ";
    $configuracion=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
    while($asist=mysqli_fetch_array($asi,MYSQLI_ASSOC))
    {
        $count=$count+1;
      
                      $salario=0;
                      if($asist["Turno"]==1)
                        $salario=$asist["Turno1"];
                      if($asist["Turno"]==2)
                        $salario=$asist["Turno2"];
                      if($asist["Turno"]==3)
                        $salario=$asist["Turno3"];
                      $monto=$salario*$asist["FactorIntegracion"];
                      $salario=number_format($salario*$asist["FactorIntegracion"],2);
                      $salario=$salario+$asist["dBono"];
                        if((float)$configuracion{'dTopeSalario'}<(float)$monto)
                        {
                          $salario=number_format((float)$configuracion{'dTopeSalario'},2);
                        }
                        $salario=explode(".",$salario);
                        if(strlen($salario[0])<=3)
                          $cadena="0".$salario[0].$salario[1];
                        else
                          $cadena=str_replace(",", "", $salario[0]).$salario[1];
        fputs($ar,str_pad($asist['rp'], 11));
        fputs($ar,str_pad($asist['imss'], 11));
        fputs($ar,str_pad(($asist['pat']), 27));
        fputs($ar,str_pad(($asist['mat']), 27));
        fputs($ar,str_pad(($asist['nomb']), 27));       
        fputs($ar,str_pad($cadena, 6));//salario
        //fputs($ar,str_pad(((float)$configuracion{'dTopeSalario'}<(float)(str_replace(",", "", $salario[0])+($salario[1]/100)) ? (float)$configuracion{'dTopeSalario'} : $cadena ), 6));//salario
        fputs($ar,str_pad('000000', 6));//fillier
        fputs($ar,str_pad(($asist['eCodTipoTrabajador']), 1));//tipo de trabajador
        fputs($ar,str_pad(($asist['TipoSalario']), 1));//tipo de salario
        fputs($ar,str_pad(($asist['eCodTipoJornada']), 1));//semana jornada
        $fech=$asist["Fecha"]; $fecha=explode("-",$fech); $abc=$fecha[2].$fecha[1].$fecha[0]; echo $abc;
        fputs($ar,str_pad($abc, 8)); //Fecha
        fputs($ar,str_pad($asist['clin'], 3)); //clinica
        fputs($ar,str_pad('  ', 2)); //Filler (anote blancos)
        fputs($ar,str_pad('08', 2));//movimiento
        fputs($ar,str_pad('03400', 5));//guia
        fputs($ar,str_pad('ESTIBADOR', 10));//estibador
        fputs($ar,str_pad('', 1));//FillierB
        fputs($ar,str_pad(($asist['curp']), 18));//curp
        fputs($ar,str_pad('9', 1)."\r\n"); //indicador
    }
    mysqli_free_result($asi);

        fputs($ar,str_pad("*************",56));
        fputs($ar,str_pad($count,6,"0",STR_PAD_LEFT));
        fputs($ar,"                                                                       ");
        fputs($ar,"03400"); 
        fputs($ar,"                             9"); 
    fclose($ar);
    $enlace = $directorio;    
    header ("Location: halta.php");
    /*
    ?>
      <script type="text/javascript"> 
      alert('Exito, se genero nuevo el archivo Altas');
      window.location='../batch/altas.php';
      </script>
      <?php */
  } 
  ?>

              