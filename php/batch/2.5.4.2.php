<?php
require_once("../conexion/soluciones-mysql.php");
$link = getLink();

$directorio="MODIFICACIONES.dat";


  date_default_timezone_set('America/Mexico_City');
  $varfecha=isset($_GET["fecha"]) ? $_GET["fecha"] : date("Y-m-d");
  $fechaayer = date('Y-m-d',strtotime('-1 days', strtotime($varfecha)));
  $fechaantier = date('Y-m-d',strtotime('-2 days', strtotime($varfecha)));

   $select=" SELECT * FROM sisconfiguracion ";
  $configura=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
  
  $select=" SELECT * FROM categorias WHERE indice=27 ";
  $salarioMin=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);//

  $select=" SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."' ";
  $incapacidades=mysqli_query($link,$select);
  $UsuariosInc="0";
  while($incapacidad=mysqli_fetch_array($incapacidades,MYSQLI_ASSOC)){
    $UsuariosInc.=", ".$incapacidad{'idEmpleado'};
  }
  $select=" SELECT assi.id ".
      " FROM movimientosafiliatorios mos ".
      " INNER JOIN relasistenciamovimientos rassi ON rassi.eCodMovimiento=mos.eCodMovimiento ".
      " INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia ".
      " WHERE assi.Fecha Between '".$fechaantier."' AND '".$fechaayer."' ";
  $afiliatorios=mysqli_query($link,$select);
  $UsuariosAfil="0";
  while($afiliatorio=mysqli_fetch_array($afiliatorios,MYSQLI_ASSOC)){
    $UsuariosAfil.=", ".$afiliatorio{'id'};
  }
  $select=" SELECT assihh.idEmpleado ".
      " FROM movimientosafiliatorios moshh ".
      " INNER JOIN relasistenciamovimientos rassihh ON rassihh.eCodMovimiento=moshh.eCodMovimiento ".
      " INNER JOIN asistencias assihh ON assihh.id=rassihh.eCodAsistencia ".
      " WHERE moshh.fhFecha='".$fechaantier."' AND moshh.tCodTipoMovimiento NOT IN ('02') AND moshh.eCodEstatus='AC' ";
  $movimientos=mysqli_query($link,$select);
  $UsiariosMovi="0";
  while($movimiento=mysqli_fetch_array($movimientos,MYSQLI_ASSOC)){
    $UsiariosMovi.=", ".$movimiento{'idEmpleado'};
  }
  //Asistencias de ayer
  //Asistencias activas
  //Empleado sin incapacidad
  //La asistencia no debe tener movimiento
  //
  $select=" 
  SELECT DISTINCT cent.tRegistroPatronal AS rp, emple.IMSS AS imss, emple.Paterno AS pat, Materno AS mat, Nombre AS nomb, ctiemp.eCodTipoTrabajador, ctiemp.eCodTipoSalario as TipoSalario, ctiemp.eCodTipoJornada AS tipjo, emple.Clinica AS clin, emple.CURP AS curp, 
  CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)>".(float)$configura{'dTopeSalario'}." THEN ".(float)$configura{'dTopeSalario'}." ELSE ".
  " CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)=0 THEN FORMAT((".(float)$salarioMin{'Turno1'}."*ctiemp.dFactorIntegracion),2) ELSE ".
  " CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)<".$configura{'dSalarioMinimo'}." THEN ".$configura{'dSalarioMinimo'}." ELSE (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado) END ".
  " END ".
  " END AS Salario
  
FROM asistencias asis
INNER JOIN empleados emple ON emple.id=asis.idEmpleado
INNER JOIN cattipoempleado ctiemp ON ctiemp.eCodTipoEntidad=emple.eCodTipo
LEFT JOIN catentidades cent ON cent.eCodEntidad=asis.idEmpresa
WHERE asis.Fecha Between '".$fechaantier."' AND '".$fechaayer."' 
AND asis.Estado='AC' 
AND asis.idEmpleado NOT IN (".$UsuariosInc.") 
AND asis.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asis.id) 
AND asis.id NOT IN (".$UsuariosAfil.")
AND asis.idEmpleado IN (SELECT assi.idEmpleado 
FROM movimientosafiliatorios mos 
INNER JOIN relasistenciamovimientos rassi ON rassi.eCodMovimiento=mos.eCodMovimiento 
INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia
WHERE assi.idEmpleado=asis.idEmpleado AND mos.fhFecha='".$fechaantier."' AND mos.tCodTipoMovimiento NOT IN ('02') AND mos.eCodEstatus='AC')
AND CASE WHEN (SELECT mosa.dImporte
FROM movimientosafiliatorios mosa 
INNER JOIN relasistenciamovimientos rassia ON rassia.eCodMovimiento=mosa.eCodMovimiento 
INNER JOIN asistencias assia ON assia.id=rassia.eCodAsistencia
WHERE assia.idEmpleado=asis.idEmpleado AND mosa.fhFecha='".$fechaantier."' AND mosa.tCodTipoMovimiento NOT IN ('02') AND mosa.eCodEstatus='AC' LIMIT 1) IS NULL THEN 0 ELSE (SELECT mosa.dImporte
FROM movimientosafiliatorios mosa 
INNER JOIN relasistenciamovimientos rassia ON rassia.eCodMovimiento=mosa.eCodMovimiento 
INNER JOIN asistencias assia ON assia.id=rassia.eCodAsistencia
WHERE assia.idEmpleado=asis.idEmpleado AND mosa.fhFecha='".$fechaantier."' AND mosa.tCodTipoMovimiento NOT IN ('02') AND mosa.eCodEstatus='AC' LIMIT 1) END
<> CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)>".(float)$configura{'dTopeSalario'}." THEN ".(float)$configura{'dTopeSalario'}." ELSE 
CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)=0 THEN FORMAT((".(float)$salarioMin{'Turno1'}."*ctiemp.dFactorIntegracion),2) ELSE ".
  " CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)<".$configura{'dSalarioMinimo'}." THEN ".$configura{'dSalarioMinimo'}." ELSE (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado) END ".
  " END ".
  " END ";
//$rsServicios=mysql_query($select);
$cont=0;


 $asi=mysqli_query($link,$select);
  if(file_exists($directorio))
  {    
    if(unlink($directorio))
    {
    $ar=fopen($directorio,"a") or die("Problemas en la creacion");
    $count=0;
        while($asist=mysqli_fetch_array($asi,MYSQLI_ASSOC))
        {
                      $count=$count+1;
                      /*$salario=0;

                      if($asist["Turno"]==1)
                        $salario=$asist["Turno1"];
                      if($asist["Turno"]==2)
                        $salario=$asist["Turno2"];
                      if($asist["Turno"]==3)
                        $salario=$asist["Turno3"];
                        $salario=explode(".",$salario);
                        if(strlen($salario[0])<=3)
                          $cadena="0".$salario[0].$salario[1];
                        else
                          $cadena=$salario[0].$salario[1];*/

                          $salario=explode(".",str_replace(",", "", number_format($asist["Salario"], 2)));
                          //$salario=explode(".",$asist["Salario"]);
                          $cadena=sprintf("%04d",$salario[0]).sprintf("%02d",$salario[1]);
                          /*
                          switch(strlen($salario[0]))
                          {
                          case 1:
                                  $cadena="000".$salario[0].$salario[1];
                                  break;
                          case 2:
                                  $cadena="00".$salario[0].$salario[1];
                                  break;
                          case 3:
                                  $cadena="0".$salario[0].$salario[1];
                                  break;
                          case 4:
                                  $cadena=$salario[0].$salario[1];
                                  break;
                          }
                          */

                        /*
                        if(strlen($salario[0])==4)
                          $cadena=$salario[0].$salario[1];
                        if(strlen($salario[0])==3)
                          $cadena="0".$salario[0].$salario[1];
                        if(strlen($salario[0])==2)
                          $cadena="00".$salario[0].$salario[1];
                        */

                        

            fputs($ar,str_pad($asist['rp'], 11));
            fputs($ar,str_pad($asist['imss'], 11));
            fputs($ar,str_pad(($asist['pat']), 27));
            fputs($ar,str_pad(($asist['mat']), 27));
            fputs($ar,str_pad(($asist['nomb']), 27));            
            fputs($ar,str_pad($cadena, 6));//salario            
            fputs($ar,str_pad('000000', 6));//fillier
            fputs($ar,str_pad($asist["eCodTipoTrabajador"], 1));//tipo de trabajador
            fputs($ar,str_pad($asist["TipoSalario"], 1));//tipo de salario
            fputs($ar,str_pad($asist["tipjo"], 1));//semana jornada
            //$fech=$asist["Fecha"]; $fecha=explode("-",$fech); $abc=$fecha[2].$fecha[1].$fecha[0]; echo $abc;
            $fech=$fechaayer; $fecha=explode("-",$fech); $abc=$fecha[2].$fecha[1].$fecha[0]; echo $abc;
            fputs($ar,str_pad($abc, 8));
            fputs($ar,str_pad('     ', 5)); //clinica
            //fputs($ar,str_pad('  ', 2)); //Filler (anote blancos)
            fputs($ar,str_pad('07', 2));//movimiento
            fputs($ar,str_pad('03400', 5));//guia
            fputs($ar,str_pad('ESTIBADOR', 10));//estibador
            fputs($ar,str_pad('', 1));//FillierB
            fputs($ar,str_pad(($asist['curp']), 18));//curp
            fputs($ar,str_pad('9', 1)."\r\n"); //indicador
          }
        mysqli_free_result($asi);
        fputs($ar,str_pad("*************",56));
        fputs($ar,str_pad($count,6,"0",STR_PAD_LEFT));
        fputs($ar,"                                                                       ");
        fputs($ar,"03400"); 
        fputs($ar,"                             9");       
        fclose($ar); 
        $enlace = $directorio;
        header ("Location: hmov.php");        
    }
    else
    { 
      ?>
      <script type="text/javascript"> 
      alert('Error, no se pudo eliminar el archivo anterior');
      //window.location='../batch/movimientos.php';
      </script>
      <?php
    }
   } 
  else
  {
    $ar=fopen($directorio,"a") or
    die("Problemas en la creacion");
    $count=0;
    while($asist=mysqli_fetch_array($asi,MYSQLI_ASSOC))
    {
        $count=$count+1;                     

                        $salario=explode(".",str_replace(",", "", number_format($asist["Salario"], 2)));
                        //$salario=explode(".",$asist["Salario"]);
                        $cadena=sprintf("%04d",$salario[0]).sprintf("%02d",$salario[1]);
                      

            fputs($ar,str_pad($asist['rp'], 11));
            fputs($ar,str_pad($asist['imss'], 11));
            fputs($ar,str_pad(($asist['pat']), 27));
            fputs($ar,str_pad(($asist['mat']), 27));
            fputs($ar,str_pad(($asist['nomb']), 27));            
            fputs($ar,str_pad($cadena, 6));//salario            
            fputs($ar,str_pad('000000', 6));//fillier
            fputs($ar,str_pad($asist["eCodTipoTrabajador"], 1));//tipo de trabajador
            fputs($ar,str_pad($asist["TipoSalario"], 1));//tipo de salario
            fputs($ar,str_pad($asist["tipjo"], 1));//semana jornada
           // $fech=$asist["Fecha"]; $fecha=explode("-",$fech); $abc=$fecha[2].$fecha[1].$fecha[0]; echo $abc;
            $fech=$fechaayer; $fecha=explode("-",$fech); $abc=$fecha[2].$fecha[1].$fecha[0]; echo $abc;
            fputs($ar,str_pad($abc, 8));
            fputs($ar,str_pad('     ', 5)); //clinica
            //fputs($ar,str_pad('  ', 2)); //Filler (anote blancos)
            fputs($ar,str_pad('07', 2));//movimiento
            fputs($ar,str_pad('03400', 5));//guia
            fputs($ar,str_pad('ESTIBADOR', 10));//estibador
            fputs($ar,str_pad('', 1));//FillierB
            fputs($ar,str_pad(($asist['curp']), 18));//curp
            fputs($ar,str_pad('9', 1)."\r\n"); //indicador
    }
    mysqli_free_result($asi);fputs($ar,str_pad("*************",56));
        fputs($ar,str_pad($count,6,"0",STR_PAD_LEFT));
        fputs($ar,"                                                                       ");
        fputs($ar,"03400"); 
        fputs($ar,"                             9"); 
    fclose($ar);
    $enlace = $directorio;
    header ("Location: hmov.php");
    
  } 
  ?>

              