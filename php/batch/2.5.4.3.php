<?php
require_once("../conexion/soluciones-mysql.php");
$link = getLink();

$directorio="BAJAS.dat";

date_default_timezone_set('America/Mexico_City');
$varfecha=isset($_GET["fecha"]) ? $_GET["fecha"] : date("Y-m-d");
$fechabaja = date('Y-m-d',strtotime('-2 days', strtotime($varfecha)));
$fechaayer = date('Y-m-d',strtotime('-1 days', strtotime($varfecha)));
//$fechavalidanext = date('Y-m-d',strtotime('-3 days', strtotime($varfecha)));

//Consulta para validar si tienen continuacion de asistencia dia siguiente mismo empleado.
$dianext="SELECT asi.idEmpleado ".
" FROM asistencias AS asi ".
" LEFT JOIN relasistenciamovimientos AS rel ON rel.eCodAsistencia=asi.id ".
" LEFT JOIN movimientosafiliatorios AS mov ON mov.eCodMovimiento=rel.eCodMovimiento AND mov.eCodEstatus='AC' ".
" WHERE asi.Estado='AC' AND mov.fhFecha='".$fechaayer."' ".
" group by asi.idEmpleado order by asi.idEmpleado ";  

$continua="";
if($Rsdianext=mysqli_query($link,$dianext))
{
  while($rdianext=mysqli_fetch_array($Rsdianext,MYSQLI_ASSOC))
  {
    $continua=$continua.$rdianext["idEmpleado"]." , ";
  }
}
  $continua=$continua." 0 ";

  $dianext="SELECT asi.idEmpleado ".
" FROM asistencias AS asi ".
" LEFT JOIN relasistenciamovimientos AS rel ON rel.eCodAsistencia=asi.id ".
" LEFT JOIN movimientosafiliatorios AS mov ON mov.eCodMovimiento=rel.eCodMovimiento AND mov.eCodEstatus='AC' ".
" WHERE asi.Estado='AC' AND mov.tCodTipoMovimiento='02' AND mov.fhFecha='".$fechabaja."' ".
" group by asi.idEmpleado order by asi.idEmpleado ";  

$baja="";
if($Rsdianext=mysqli_query($link,$dianext))
{
  while($rdianext=mysqli_fetch_array($Rsdianext,MYSQLI_ASSOC))
  {
    $baja=$baja.$rdianext["idEmpleado"]." , ";
  }
}
  $baja=$baja." 0 ";

//print($dianext."<br><br>");
//print($continua."<br><br>");

$select=" SELECT asi.id AS numeral, asi.*, cat.eCodEntidad AS iden, cat.tRegistroPatronal AS rp, emp.IMSS, emp.Paterno , emp.materno, emp.nombre, ".
        " mov.tFolio, mov.fhFecha, mov.tCodTipoMovimiento ".
        " FROM asistencias AS asi ".
        " LEFT JOIN relasistenciamovimientos AS rel ON rel.eCodAsistencia=asi.id ".
        " LEFT JOIN movimientosafiliatorios AS mov ON mov.eCodMovimiento=rel.eCodMovimiento AND mov.eCodEstatus='AC' ".
        " LEFT JOIN catentidades AS cat ON asi.idEmpresa = cat.eCodEntidad ".
        " LEFT JOIN empleados AS emp ON asi.idEmpleado = emp.id ".
        " LEFT JOIN categorias AS ser ON ser.indice = asi.idCategorias ".
        " WHERE asi.Estado='AC' AND mov.fhFecha='".$fechabaja."' AND asi.idCategorias not in ( 35 ) AND mov.tFolio IS NOT NULL ".
        " AND emp.id not in ( ".$continua." , ".$baja." ) ".        
        " group by asi.idEmpleado order by asi.id ";
//print($sql);
  $asi=mysqli_query($link,$select);
  if(file_exists($directorio))
  {    
    if(unlink($directorio))
    {
    $ar=fopen($directorio,"a") or die("Problemas en la creacion");
    $count=0;
        while($asist=mysqli_fetch_array($asi,MYSQLI_ASSOC))
        {
          $count=$count+1;
            fputs($ar,str_pad($asist['rp'], 11));
            fputs($ar,str_pad($asist['IMSS'], 11));
            fputs($ar,str_pad(($asist['Paterno']), 27));
            fputs($ar,str_pad(($asist['materno']), 27));
            fputs($ar,str_pad(($asist['nombre']), 27));
            //fputs($ar,str_pad($asist[''], 6));//salario
            fputs($ar,str_pad(' ', 15));//fillier
            //fputs($ar,str_pad('2', 1));//tipo de trabajador
            //fputs($ar,str_pad('2', 1));//tipo de salario
            //fputs($ar,str_pad('0', 1));//semana jornada
            $fech=$asist['fhFecha']; $fecha=explode("-",$fech); $abc=$fecha[2].$fecha[1].$fecha[0]; 
            fputs($ar,str_pad($abc, 8));
            fputs($ar,str_pad(' ', 5));
            fputs($ar,str_pad('02', 2));//movimiento
            fputs($ar,str_pad('03400', 5));//guia
            fputs($ar,str_pad('Estibador', 10));//estibador
            fputs($ar,str_pad('1', 1));//FillierB Causa del movimiento
            fputs($ar,str_pad(' ', 18));//curp
            fputs($ar,str_pad('9', 1)."\r\n"); //indicador
        }
        mysqli_free_result($asi);
        fputs($ar,str_pad("*************",56));
        fputs($ar,str_pad($count,6,"0",STR_PAD_LEFT));
        fputs($ar,"                                                                       ");
        fputs($ar,"03400"); 
        fputs($ar,"                             9");       
        fclose($ar); 
        $enlace = $directorio;        
        header ("Location: hbaja.php");
        /*?>
      <script type="text/javascript"> 
      alert('Exito, se reemplazo el archivo Bajas');
      window.location='../batch/bajas.php';
      </script>
      <?php*/
    }
    else
    { 
      ?>
      <script type="text/javascript"> 
      alert('Error, no se pudo eliminar el archivo anterior');
      //window.location='../batch/bajas.php';
      </script>
      <?php
    }
   } 
  else
  {
    $ar=fopen($directorio,"a") or
    die("Problemas en la creacion");
    $count=0;
    while($asist=mysqli_fetch_array($asi,MYSQLI_ASSOC))
    {
      $count=$count+1;
            fputs($ar,str_pad($asist['rp'], 11));
            fputs($ar,str_pad($asist['IMSS'], 11));
            fputs($ar,str_pad(($asist['Paterno']), 27));
            fputs($ar,str_pad(($asist['materno']), 27));
            fputs($ar,str_pad(($asist['nombre']), 27));
            //fputs($ar,str_pad($asist[''], 6));//salario
            fputs($ar,str_pad(' ', 15));//fillier
            //fputs($ar,str_pad('2', 1));//tipo de trabajador
            //fputs($ar,str_pad('2', 1));//tipo de salario
            //fputs($ar,str_pad('0', 1));//semana jornada
            $fech=$asist['fhFecha']; $fecha=explode("-",$fech); $abc=$fecha[2].$fecha[1].$fecha[0]; 
            fputs($ar,str_pad($abc, 8));
            fputs($ar,str_pad(' ', 5));
            fputs($ar,str_pad('02', 2));//movimiento
            fputs($ar,str_pad('03400', 5));//guia
            fputs($ar,str_pad('Estibador', 10));//estibador
            fputs($ar,str_pad('1', 1));//FillierB Causa del movimiento
            fputs($ar,str_pad(' ', 18));//curp
            fputs($ar,str_pad('9', 1)."\r\n"); //indicador
    }
    mysqli_free_result($asi);
        fputs($ar,str_pad("*************",56));
        fputs($ar,str_pad($count,6,"0",STR_PAD_LEFT));
        fputs($ar,"                                                                       ");
        fputs($ar,"03400"); 
        fputs($ar,"                             9");       
        fclose($ar); 
        $enlace = $directorio;        
        header ("Location: hbaja.php");
    
  } 
  ?>

              