<?php require_once("../conexion/soluciones-mysql.php");
$link = getLink();
    $directorio="MODIFICACIONES.dat";
    date_default_timezone_set('America/Mexico_City');
    $varfecha=isset($_GET["fecha"]) ? $_GET["fecha"] : date("Y-m-d");
    //if($_GET['eCodTipoEntidad']==2){
        $fechaayer = date('Y-m-d',strtotime($varfecha));
        $fechaantier = date('Y-m-d',strtotime('-1 days', strtotime($varfecha)));
    /*}else{
        $fechaayer = date('Y-m-d',strtotime('-1 days', strtotime($varfecha)));
        $fechaantier = date('Y-m-d',strtotime('-2 days', strtotime($varfecha)));
    }*/
    $idUsuario = ($_GET['eUsuario'] ? $_GET['eUsuario'] : "NULL");

    $select=" SELECT * FROM sisconfiguracion ";
    $configura=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

    $select=" SELECT * ".
            " FROM categorias ".
            " WHERE indice=27 ";
    $salarioMin=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

    $select=" SELECT assi.id, assi.idEmpleado ".
            " FROM movimientosafiliatorios mos ".
            " INNER JOIN relasistenciamovimientos rassi ON rassi.eCodMovimiento=mos.eCodMovimiento ".
            " INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia ".
            " INNER JOIN empleados emple ON emple.id=assi.idEmpleado ".
            " WHERE assi.Estado='AC' AND assi.idEmpresa=".(int)$_GET['eCodEntidad'].
            " AND (assi.Fecha='".$fechaayer."' OR mos.fhFecha='".$fechaayer."') AND mos.eCodEstatus='AC' ".
            ((int)$_GET['eCodTipoEntidad']>0 ? " AND emple.eCodTipo=".(int)$_GET['eCodTipoEntidad'] : "");

    $afiliatorios=mysqli_query($link,$select);
    $noUsuario="0";
    while($afiliatorio=mysqli_fetch_array($afiliatorios,MYSQLI_ASSOC)){
        $noUsuario.=", ".$afiliatorio{'idEmpleado'};
    }
    //
    $select=" SELECT assi.id, assi.idEmpleado ".
            " FROM movimientosafiliatorios mos ".
            " INNER JOIN relasistenciamovimientos rassi ON rassi.eCodMovimiento=mos.eCodMovimiento ".
            " INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia ".
            " INNER JOIN empleados emple ON emple.id=assi.idEmpleado ".
            " WHERE assi.Estado='AC' AND assi.idEmpresa=".(int)$_GET['eCodEntidad'].
            " AND assi.Fecha Between '".$fechaantier."' AND '".$fechaayer."' ".
            ((int)$_GET['eCodTipoEntidad']>0 ? " AND emple.eCodTipo=".(int)$_GET['eCodTipoEntidad'] : "");

    $afiliatorios=mysqli_query($link,$select);
    $UsuariosAfil="0";
    while($afiliatorio=mysqli_fetch_array($afiliatorios,MYSQLI_ASSOC)){
        $UsuariosAfil.=", ".$afiliatorio{'id'};
    }
    //
    $select=" SELECT assihh.idEmpleado ".
            " FROM movimientosafiliatorios moshh ".
            " INNER JOIN relasistenciamovimientos rassihh ON rassihh.eCodMovimiento=moshh.eCodMovimiento ".
            " INNER JOIN asistencias assihh ON assihh.id=rassihh.eCodAsistencia ".
            " INNER JOIN empleados emple ON emple.id=assihh.idEmpleado ".
            " WHERE assihh.Estado='AC' AND assihh.idEmpresa=".(int)$_GET['eCodEntidad'].
            " AND moshh.fhFecha='".$fechaantier."' AND moshh.tCodTipoMovimiento NOT IN ('02') ".
            " AND moshh.eCodEstatus='AC' ".((int)$_GET['eCodTipoEntidad']>0 ? " AND emple.eCodTipo=".(int)$_GET['eCodTipoEntidad'] : "");
    $movimientos=mysqli_query($link,$select);
    $UsiariosMovi="0";
    while($movimiento=mysqli_fetch_array($movimientos,MYSQLI_ASSOC)){
        $UsiariosMovi.=", ".$movimiento{'idEmpleado'};
    }

   $select=" SELECT DISTINCT emple.id AS numeral, emple.IMSS AS imss, cent.tRegistroPatronal AS rp, emple.Paterno AS pat, Materno AS mat, Nombre AS nomb, ctj.eCodJornada AS tipjo, '".$fechaayer."' AS Fecha, emple.Clinica AS clin, emple.CURP AS curp, ctiemp.eCodTipoTrabajador AS eCodTipoTrabajador, cts.eCodSalario AS TipoSalario,
    CASE WHEN (SELECT MAX(asish.Salario)
FROM asistencias asish
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.idEmpresa=".(int)$_GET['eCodEntidad']."
AND asish.Estado='AC'
AND asish.idEmpleado NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."')
AND asish.id NOT IN (SELECT r1a.eCodAsistencia FROM relasistenciamovimientos r1a INNER JOIN movimientosafiliatorios m1a ON m1a.eCodMovimiento=r1a.eCodMovimiento WHERE r1a.eCodAsistencia=asish.id)
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)>".(float)$configura{'dTopeSalario'}." THEN ".(float)$configura{'dTopeSalario'}." ELSE
    CASE WHEN (SELECT MAX(asish.Salario)
FROM asistencias asish
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.idEmpresa=".(int)$_GET['eCodEntidad']."
AND asish.Estado='AC'
AND asish.idEmpleado NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."')
AND asish.id NOT IN (SELECT r1a.eCodAsistencia FROM relasistenciamovimientos r1a INNER JOIN movimientosafiliatorios m1a ON m1a.eCodMovimiento=r1a.eCodMovimiento WHERE r1a.eCodAsistencia=asish.id)
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)=0 THEN FORMAT((".(float)$salarioMin{'Turno1'}."*ctiemp.dFactorIntegracion),2) ELSE ".
    " CASE WHEN (SELECT MAX(asish.Salario)
FROM asistencias asish
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.idEmpresa=".(int)$_GET['eCodEntidad']."
AND asish.Estado='AC'
AND asish.idEmpleado NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."')
AND asish.id NOT IN (SELECT r1a.eCodAsistencia FROM relasistenciamovimientos r1a INNER JOIN movimientosafiliatorios m1a ON m1a.eCodMovimiento=r1a.eCodMovimiento WHERE r1a.eCodAsistencia=asish.id)
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)<".$configura{'dSalarioMinimo'}." THEN ".$configura{'dSalarioMinimo'}." ELSE (SELECT MAX(asish.Salario)
FROM asistencias asish
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.idEmpresa=".(int)$_GET['eCodEntidad']."
AND asish.Estado='AC'
AND asish.idEmpleado NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."')
AND asish.id NOT IN (SELECT r1a.eCodAsistencia FROM relasistenciamovimientos r1a INNER JOIN movimientosafiliatorios m1a ON m1a.eCodMovimiento=r1a.eCodMovimiento WHERE r1a.eCodAsistencia=asish.id)
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado) END ".
    " END
    END AS Salario, ctiemp.eCodTipoSalario
FROM asistencias asis
INNER JOIN empleados emple ON emple.id=asis.idEmpleado
INNER JOIN cattipoempleado ctiemp ON ctiemp.eCodTipoEntidad=emple.eCodTipo
LEFT JOIN catentidades cent ON cent.eCodEntidad=asis.idEmpresa
LEFT JOIN cattiposalario cts ON cts.eCodSalario=ctiemp.eCodTipoSalario
LEFT JOIN cattipojornada ctj ON ctj.eCodJornada=ctiemp.eCodTipoJornada
WHERE asis.Fecha Between '".$fechaantier."' AND '".$fechaayer."' AND asis.idEmpresa=".(int)$_GET['eCodEntidad'].
((int)$_GET['eCodTipoEntidad']>0 ? " AND emple.eCodTipo=".$_GET['eCodTipoEntidad'] : "")."
AND asis.Estado='AC'
AND asis.idEmpleado NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."')
AND asis.id NOT IN (SELECT r1a.eCodAsistencia FROM relasistenciamovimientos r1a INNER JOIN movimientosafiliatorios m1a ON m1a.eCodMovimiento=r1a.eCodMovimiento WHERE r1a.eCodAsistencia=asis.id)
AND asis.id NOT IN (".$UsuariosAfil.")
AND asis.idEmpleado NOT IN (".$noUsuario.")
AND asis.idEmpleado IN (SELECT assi.idEmpleado
FROM movimientosafiliatorios mos
INNER JOIN relasistenciamovimientos rassi ON rassi.eCodMovimiento=mos.eCodMovimiento
INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia
WHERE assi.idEmpleado=asis.idEmpleado AND mos.fhFecha='".$fechaantier."' AND assi.idEmpresa=".(int)$_GET['eCodEntidad'].
" AND mos.tCodTipoMovimiento NOT IN ('02') AND mos.eCodEstatus='AC')
AND
IFNULL((SELECT mosa.dImporte
FROM movimientosafiliatorios mosa
INNER JOIN relasistenciamovimientos rassia ON rassia.eCodMovimiento=mosa.eCodMovimiento
INNER JOIN asistencias assia ON assia.id=rassia.eCodAsistencia
WHERE assia.idEmpleado=asis.idEmpleado AND mosa.fhFecha='".$fechaantier."' AND assia.idEmpresa=".(int)$_GET['eCodEntidad'].
" AND mosa.tCodTipoMovimiento NOT IN ('02') AND mosa.eCodEstatus='AC' AND assia.Estado='AC' LIMIT 1),0)
<>
CASE WHEN (
SELECT MAX(asish.Salario)
FROM asistencias asish
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.idEmpresa=".(int)$_GET['eCodEntidad'].
" AND asish.Estado='AC'
AND asish.idEmpleado NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."')
AND asish.id NOT IN (SELECT r1a.eCodAsistencia FROM relasistenciamovimientos r1a INNER JOIN movimientosafiliatorios m1a ON m1a.eCodMovimiento=r1a.eCodMovimiento WHERE r1a.eCodAsistencia=asish.id)
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)>".(float)$configura{'dTopeSalario'}." THEN ".(float)$configura{'dTopeSalario'}." ELSE
CASE WHEN (SELECT MAX(asish.Salario)
FROM asistencias asish
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.idEmpresa=".(int)$_GET['eCodEntidad']."
AND asish.Estado='AC'
AND asish.idEmpleado NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."')
AND asish.id NOT IN (SELECT r1a.eCodAsistencia FROM relasistenciamovimientos r1a INNER JOIN movimientosafiliatorios m1a ON m1a.eCodMovimiento=r1a.eCodMovimiento WHERE r1a.eCodAsistencia=asish.id)
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)=0 THEN FORMAT((".(float)$salarioMin{'Turno1'}."*ctiemp.dFactorIntegracion),2) ELSE ".
    " CASE WHEN (SELECT MAX(asish.Salario)
FROM asistencias asish
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.idEmpresa=".(int)$_GET['eCodEntidad']."
AND asish.Estado='AC'
AND asish.idEmpleado NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."')
AND asish.id NOT IN (SELECT r1a.eCodAsistencia FROM relasistenciamovimientos r1a INNER JOIN movimientosafiliatorios m1a ON m1a.eCodMovimiento=r1a.eCodMovimiento WHERE r1a.eCodAsistencia=asish.id)
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)<".$configura{'dSalarioMinimo'}." THEN ".$configura{'dSalarioMinimo'}." ELSE (SELECT MAX(asish.Salario)
FROM asistencias asish
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.idEmpresa=".(int)$_GET['eCodEntidad']."
AND asish.Estado='AC'
AND asish.idEmpleado NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."')
AND asish.id NOT IN (SELECT r1a.eCodAsistencia FROM relasistenciamovimientos r1a INNER JOIN movimientosafiliatorios m1a ON m1a.eCodMovimiento=r1a.eCodMovimiento WHERE r1a.eCodAsistencia=asish.id)
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado) END ".
    " END
END ";
$cont=0;
$asi=mysqli_query($link,$select);
if(file_exists($directorio)){
    if(unlink($directorio)){
        $ar=fopen($directorio,"a") or die("Problemas en la creacion");
        $count=0;
        while($asist=mysqli_fetch_array($asi,MYSQLI_ASSOC)){
            $count=$count+1;
            $salario=explode(".",str_replace(",", "", number_format($asist["Salario"], 2)));
            if(strlen($salario[0])<=3)
              $cadena="0".$salario[0].$salario[1];
            else
              $cadena=$salario[0].$salario[1];

            $fech=$fechaayer;
            $fecha=explode("-",$fech);
            $abc=$fecha[2].$fecha[1].$fecha[0];
            fputs($ar,str_pad($asist['rp'], 11));
            fputs($ar,str_pad($asist['imss'], 11));
            fputs($ar,str_pad(($asist['pat']), 27));
            fputs($ar,str_pad(($asist['mat']), 27));
            fputs($ar,str_pad(($asist['nomb']), 27));
            fputs($ar,str_pad($cadena, 6));//salario
            fputs($ar,str_pad('000000', 6));//fillier
            fputs($ar,str_pad($asist["eCodTipoTrabajador"], 1));//tipo de trabajador
            fputs($ar,str_pad($asist["eCodTipoSalario"], 1));//tipo de salario
            fputs($ar,str_pad('0', 1));//semana jornada
            fputs($ar,str_pad($abc, 8));
            fputs($ar,str_pad('     ', 5)); //clinica
            fputs($ar,str_pad('07', 2));//movimiento
            fputs($ar,str_pad('03400', 5));//guia
            fputs($ar,str_pad('ESTIBADOR', 10));//estibador
            fputs($ar,str_pad('', 1));//FillierB
            fputs($ar,str_pad(($asist['curp']), 18));//curp
        fputs($ar,str_pad('9', 1)."\r\n"); //indicador
        }
        mysqli_free_result($asi);
        fputs($ar,str_pad("*************",56));
        fputs($ar,str_pad($count,6,"0",STR_PAD_LEFT));
        fputs($ar,"                                                                       ");
        fputs($ar,"03400");
        fputs($ar,"                             9");
        fclose($ar);
        $enlace = $directorio;
        header ("Location: hmov.php");
    }else{ ?>
        <script type="text/javascript">
        alert('Error, no se pudo eliminar el archivo anterior');
        //window.location='../batch/movimientos.php';
        </script>
    <?php }
}else{
    $ar=fopen($directorio,"a") or
    die("Problemas en la creacion");
    $count=0;
    while($asist=mysqli_fetch_array($asi,MYSQLI_ASSOC)){
        $count=$count+1;
        $salario=explode(".",str_replace(",", "", number_format($asist["Salario"], 2)));
        if(strlen($salario[0])<=3)
          $cadena="0".$salario[0].$salario[1];
        else
          $cadena=$salario[0].$salario[1];

        $fech=$fechaayer;
        $fecha=explode("-",$fech);
        $abc=$fecha[2].$fecha[1].$fecha[0];
        fputs($ar,str_pad($asist['rp'], 11));
        fputs($ar,str_pad($asist['imss'], 11));
        fputs($ar,str_pad(($asist['pat']), 27));
        fputs($ar,str_pad(($asist['mat']), 27));
        fputs($ar,str_pad(($asist['nomb']), 27));
        fputs($ar,str_pad($cadena, 6));//salario
        fputs($ar,str_pad('000000', 6));//fillier
        fputs($ar,str_pad($asist["eCodTipoTrabajador"], 1));//tipo de trabajador
        fputs($ar,str_pad($asist["eCodTipoSalario"], 1));//tipo de salario
        fputs($ar,str_pad('0', 1));//semana jornada
        fputs($ar,str_pad($abc, 8));
        fputs($ar,str_pad('     ', 5)); //clinica
        fputs($ar,str_pad('07', 2));//movimiento
        fputs($ar,str_pad('03400', 5));//guia
        fputs($ar,str_pad('ESTIBADOR', 10));//estibador
        fputs($ar,str_pad('', 1));//FillierB
        fputs($ar,str_pad(($asist['curp']), 18));//curp
        fputs($ar,str_pad('9', 1)."\r\n"); //indicador
    }
    mysqli_free_result($asi);fputs($ar,str_pad("*************",56));
    fputs($ar,str_pad($count,6,"0",STR_PAD_LEFT));
    fputs($ar,"                                                                       ");
    fputs($ar,"03400");
    fputs($ar,"                             9");
    fclose($ar);
    $enlace = $directorio;
    header ("Location: hmov.php");
} ?>
