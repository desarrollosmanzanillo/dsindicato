<?php require_once("../conexion/soluciones-mysql.php");
$directorio="BAJAS.dat";
$link = getLink();

date_default_timezone_set('America/Mexico_City');
$varfecha=isset($_GET["fecha"]) ? $_GET["fecha"] : date("Y-m-d");
$fechabaja = date('Y-m-d',strtotime('-1 days', strtotime($varfecha)));
$fechaayer = date('Y-m-d',strtotime('-1 days', strtotime($varfecha)));
//$fechavalidanext = date('Y-m-d',strtotime('-3 days', strtotime($varfecha)));

//Consulta para validar si tienen continuacion de asistencia dia siguiente mismo empleado.
/*$select=" SELECT asi.idEmpleado ".
        " FROM asistenciaschecadoras asi ".
        " LEFT JOIN relasistenciamovimientoschecadoras AS rel ON rel.eCodAsistencia=asi.id ".
        " LEFT JOIN movimientosafiliatorioschecadoras AS mov ON mov.eCodMovimiento=rel.eCodMovimiento AND mov.eCodEstatus='AC' ".
        " LEFT JOIN catchecadoras AS emp ON asi.idEmpleado=emp.id ".
        " WHERE asi.Estado='AC' AND mov.fhFecha='".$fechaayer."' AND asi.idEmpresa=".(int)$_GET['eCodEntidad'].
        ($_GET['eCodTipoEntidad'] ? " AND emp.eCodTipo=".$_GET['eCodTipoEntidad'] : "").
        " GROUP BY asi.idEmpleado order by asi.idEmpleado ";
$continua="";
if($Rsdianext=mysql_query($select)){
  while($rdianext=mysql_fetch_array($Rsdianext)){
    $continua=$continua.$rdianext["idEmpleado"]." , ";
  }
}
$continua=$continua." 0 ";*/

$select=" SELECT asi.idEmpleado ".
        " FROM asistenciaschecadoras asi ".
        " LEFT JOIN relasistenciamovimientoschecadoras AS rel ON rel.eCodAsistencia=asi.id ".
        " LEFT JOIN movimientosafiliatorioschecadoras AS mov ON mov.eCodMovimiento=rel.eCodMovimiento AND mov.eCodEstatus='AC' ".
        " LEFT JOIN catchecadoras AS emp ON asi.idEmpleado=emp.id ".
        " WHERE asi.Estado='AC' AND mov.tCodTipoMovimiento='02' AND mov.fhFecha='".$fechabaja."' AND asi.idEmpresa=".(int)$_GET['eCodEntidad'].
        " GROUP BY asi.idEmpleado order by asi.idEmpleado ";
$baja="";
if($Rsdianext=mysqli_query($link,$select)){
  while($rdianext=mysqli_fetch_array($Rsdianext,MYSQLI_ASSOC)){
    $baja=$baja.$rdianext["idEmpleado"]." , ";
  }
}
$baja=$baja." 0 ";

/*$select=" SELECT idEmpleado ".
    " FROM incapacidades ".
    " WHERE Estado='AC' ".
    " AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."' ";
$incap="";
if($rsIncapacidades=mysql_query($select)){
  while($rIncapacidad=mysql_fetch_array($rsIncapacidades)){
    $incap=$incap.$rIncapacidad["idEmpleado"]." , ";
  }
}
$incap=$incap." 0 ";*/

$select=" SELECT asi.id AS numeral, asi.*, cat.eCodEntidad AS iden, cat.tRegistroPatronal AS rp, emp.IMSS, emp.Paterno , emp.materno, emp.nombre, ".
        " mov.tFolio, mov.fhFecha, mov.tCodTipoMovimiento, ccb.eCodIdse ".
        " FROM asistenciaschecadoras AS asi ".
        " LEFT JOIN relasistenciamovimientoschecadoras AS rel ON rel.eCodAsistencia=asi.id ".
        " LEFT JOIN movimientosafiliatorioschecadoras AS mov ON mov.eCodMovimiento=rel.eCodMovimiento AND mov.eCodEstatus='AC' ".
        " LEFT JOIN catentidades AS cat ON asi.idEmpresa = cat.eCodEntidad ".
        " LEFT JOIN catchecadoras AS emp ON asi.idEmpleado = emp.id ".
        " LEFT JOIN categorias AS ser ON ser.indice = asi.idCategorias ".
        " LEFT JOIN catcausabaja ccb ON ccb.eCodCausa=".(int)$_GET['eCodCausa'].
        " WHERE asi.Estado='AC' AND mov.fhFecha='".$fechabaja."' AND mov.tFolio IS NOT NULL AND asi.idEmpresa=".(int)$_GET['eCodEntidad'].//AND asi.idCategorias not in (17)
        " AND emp.id not in (".$baja.") ".//".$continua." , , ".$incap."
        ($_GET['eCodTipoEntidad'] ? " AND emp.eCodTipo=".$_GET['eCodTipoEntidad'] : "").
        " order by asi.id ";
$asi=mysqli_query($link,$select);
if(file_exists($directorio)){
  if(unlink($directorio)){
    $ar=fopen($directorio,"a") or die("Problemas en la creacion");
    $count=0;
    while($asist=mysqli_fetch_array($asi,MYSQLI_ASSOC)){
      $count=$count+1;
      fputs($ar,str_pad($asist['rp'], 11));
      fputs($ar,str_pad($asist['IMSS'], 11));
      fputs($ar,str_pad($asist['Paterno'], 27));
      fputs($ar,str_pad($asist['materno'], 27));
      fputs($ar,str_pad($asist['nombre'], 27));
      fputs($ar,str_pad(' ', 15));//fillier
      $fech=$asist['fhFecha']; $fecha=explode("-",$fech); $abc=$fecha[2].$fecha[1].$fecha[0];
      fputs($ar,str_pad($abc, 8));
      fputs($ar,str_pad(' ', 5));
      fputs($ar,str_pad('02', 2));//movimiento
      fputs($ar,str_pad('03400', 5));//guia
      fputs($ar,str_pad('Estibador', 10));//estibador
      fputs($ar,str_pad((int)$asist['eCodIdse'], 1));//FillierB Causa del movimiento
      fputs($ar,str_pad(' ', 18));//curp
      fputs($ar,str_pad('9', 1)."\r\n"); //indicador
    }
    mysqli_free_result($asi);
    fputs($ar,str_pad("*************",56));
    fputs($ar,str_pad($count,6,"0",STR_PAD_LEFT));
    fputs($ar,"                                                                       ");
    fputs($ar,"03400");
    fputs($ar,"                             9");
    fclose($ar);
    $enlace = $directorio;
    header ("Location: hbaja.php");
  }else{ ?>
    <script type="text/javascript">
    alert('Error, no se pudo eliminar el archivo anterior');
    </script>
  <?php }
 }else{
  $ar=fopen($directorio,"a") or
  die("Problemas en la creacion");
  $count=0;
  while($asist=mysqli_fetch_array($asi,MYSQLI_ASSOC)){
    $count=$count+1;
    fputs($ar,str_pad($asist['rp'], 11));
    fputs($ar,str_pad($asist['IMSS'], 11));
    fputs($ar,str_pad($asist['Paterno'], 27));
    fputs($ar,str_pad($asist['materno'], 27));
    fputs($ar,str_pad($asist['nombre'], 27));
    fputs($ar,str_pad(' ', 15));//fillier
    $fech=$asist['fhFecha']; $fecha=explode("-",$fech); $abc=$fecha[2].$fecha[1].$fecha[0];
    fputs($ar,str_pad($abc, 8));
    fputs($ar,str_pad(' ', 5));
    fputs($ar,str_pad('02', 2));//movimiento
    fputs($ar,str_pad('03400', 5));//guia
    fputs($ar,str_pad('Estibador', 10));//estibador
    fputs($ar,str_pad((int)$asist['eCodIdse'], 1));//FillierB Causa del movimiento
    fputs($ar,str_pad(' ', 18));//curp
    fputs($ar,str_pad('9', 1)."\r\n"); //indicador
  }
  mysqli_free_result($asi);
  fputs($ar,str_pad("*************",56));
  fputs($ar,str_pad($count,6,"0",STR_PAD_LEFT));
  fputs($ar,"                                                                       ");
  fputs($ar,"03400");
  fputs($ar,"                             9");
  fclose($ar);
  $enlace = $directorio;
  header ("Location: hbaja.php");
} ?>
