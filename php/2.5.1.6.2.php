<?php require_once("conexion/soluciones-mysql.php");
$link = getLink();

if($_POST){
	if($_POST['eAccion']==1){
		//Reingreso
		$select=" SELECT * 
				FROM movimientosafiliatorioschecadoras 
				WHERE tCodTipoMovimiento='08' 
				AND eCodMovimiento IN (SELECT eCodMovimiento 
									   FROM relasistenciamovimientoschecadoras
									   WHERE eCodAsistencia=".(int)$_POST['eCodAsistencia'].") ";
		$rsMovimientos=mysqli_query($link,$select);
		while($rMovimiento=mysqli_fetch_array($rsMovimientos,MYSQLI_ASSOC)){
			$insert=" INSERT INTO bitcancelacionmovimientosafilatorioschecadoras (eCodUsuario, fhFechaRegistro, eCodAsistencia, tCodTipoMovimiento, fhFecha, dImporte, tFolio)
					SELECT ".(int)$_POST['eUsuario'].", CURRENT_TIMESTAMP, ".(int)$_POST['eCodAsistencia'].", '08', '".$_POST['fhFecha']."', ".(float)$_POST['dImporte'].", '".$_POST['tFolio']."' ";
			if(!mysqli_query($link,$insert)){
				$exito=0;
			}else{
				$delete=" DELETE FROM relasistenciamovimientoschecadoras
						WHERE eCodAsistencia=".(int)$_POST['eCodAsistencia'].
						" AND eCodMovimiento=".(int)$rMovimiento{'eCodMovimiento'};
				if(!mysqli_query($link,$delete)){
					$exito=0;
				}else{
					$delete=" DELETE FROM movimientosafiliatorioschecadoras
							WHERE tCodTipoMovimiento='08' 
							AND eCodMovimiento IN (SELECT eCodMovimiento 
												   FROM relasistenciamovimientoschecadoras
												   WHERE eCodAsistencia=".(int)$_POST['eCodAsistencia'].") ";
					if(!mysqli_query($link,$delete)){
						$exito=0;
					}
				}
			}
		}
		print "<input type=\"hidden\" value=\"".((int)$exito>0 ? (int)$_POST['eCodAsistencia'] : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}

	if($_POST['eAccion']==2){
		//Modificacion
		$select=" SELECT * 
				FROM movimientosafiliatorioschecadoras 
				WHERE tCodTipoMovimiento='07' 
				AND eCodMovimiento IN (SELECT eCodMovimiento 
									   FROM relasistenciamovimientoschecadoras
									   WHERE eCodAsistencia=".(int)$_POST['eCodAsistencia'].") ";
		$rsMovimientos=mysqli_query($link,$select);
		while($rMovimiento=mysqli_fetch_array($rsMovimientos,MYSQLI_ASSOC)){
			$insert=" INSERT INTO bitcancelacionmovimientosafilatorioschecadoras (eCodUsuario, fhFechaRegistro, eCodAsistencia, tCodTipoMovimiento, fhFecha, dImporte, tFolio)
					SELECT ".(int)$_POST['eUsuario'].", CURRENT_TIMESTAMP, ".(int)$_POST['eCodAsistencia'].", '07', '".$_POST['fhFecha']."', ".(float)$_POST['dImporte'].", '".$_POST['tFolio']."' ";
			print $insert."
			";
			if(!mysqli_query($link,$insert)){
				$exito=0;
			}else{
				$delete=" DELETE FROM relasistenciamovimientoschecadoras
						WHERE eCodAsistencia=".(int)$_POST['eCodAsistencia'].
						" AND eCodMovimiento=".(int)$rMovimiento{'eCodMovimiento'};
				print $delete."
				";
				if(!mysqli_query($link,$delete)){
					$exito=0;
				}else{
					$delete=" DELETE FROM movimientosafiliatorioschecadoras
							WHERE tCodTipoMovimiento='07' 
							AND eCodMovimiento IN (SELECT eCodMovimiento 
												   FROM relasistenciamovimientoschecadoras
												   WHERE eCodAsistencia=".(int)$_POST['eCodAsistencia'].") ";
					if(!mysqli_query($link,$delete)){
						$exito=0;
					}
				}
			}
		}
		print "<input type=\"hidden\" value=\"".((int)$exito>0 ? (int)$_POST['eCodAsistencia'] : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}

	if($_POST['eAccion']==3){
		//Baja
		$exito=1;
		$select=" SELECT * 
				FROM movimientosafiliatorioschecadoras 
				WHERE tCodTipoMovimiento='02' 
				AND eCodMovimiento IN (SELECT eCodMovimiento 
									   FROM relasistenciamovimientoschecadoras
									   WHERE eCodAsistencia=".(int)$_POST['eCodAsistencia'].") ";
		print "select: ".$select."
		";
		$rsMovimientos=mysqli_query($link,$select);
		while($rMovimiento=mysqli_fetch_array($rsMovimientos,MYSQLI_ASSOC)){
			$insert=" INSERT INTO bitcancelacionmovimientosafilatorioschecadoras (eCodUsuario, fhFechaRegistro, eCodAsistencia, tCodTipoMovimiento, fhFecha, 
					dImporte, tFolio, eCodUsuarioOriginal, fhFechaRegistroOriginal)
					VALUES (".(int)$_POST['eUsuario'].", CURRENT_TIMESTAMP, ".(int)$_POST['eCodAsistencia'].", '02', '".$rMovimiento['fhFecha']."', 
					".(float)$rMovimiento['dImporte'].", '".$rMovimiento['tFolio']."', ".(int)$rMovimiento['eCodUsuario'].", '".$rMovimiento['fhFechaRegistro']."') ";
			print "insert: ".$insert."
			";
			if(!mysqli_query($link,$insert)){
				$exito=0;
			}else{
				$delete=" DELETE FROM relasistenciamovimientoschecadoras
						WHERE eCodAsistencia=".(int)$_POST['eCodAsistencia'].
						" AND eCodMovimiento=".(int)$rMovimiento{'eCodMovimiento'};
				print "delete: ".$delete."
				";
				if(!mysqli_query($link,$delete)){
					$exito=0;
				}else{
					$delete=" DELETE FROM movimientosafiliatorioschecadoras
							WHERE tCodTipoMovimiento='02' 
							AND eCodMovimiento IN (SELECT eCodMovimiento 
												   FROM relasistenciamovimientoschecadoras
												   WHERE eCodAsistencia=".(int)$_POST['eCodAsistencia'].") ";
					print "delete: ".$delete."
					";
					if(!mysqli_query($link,$delete)){
						$exito=0;
					}
				}
			}
		}
		print "<input type=\"hidden\" value=\"".((int)$exito>0 ? (int)$_POST['eCodAsistencia'] : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}

	if($_POST['eAccion']==4){
		//Continuacion
		$select=" SELECT * 
				FROM movimientosafiliatorioschecadoras 
				WHERE tCodTipoMovimiento='01' 
				AND eCodMovimiento IN (SELECT eCodMovimiento 
									   FROM relasistenciamovimientoschecadoras
									   WHERE eCodAsistencia=".(int)$_POST['eCodAsistencia'].") ";
		$rsMovimientos=mysqli_query($link,$select);
		while($rMovimiento=mysqli_fetch_array($rsMovimientos,MYSQLI_ASSOC)){
			$delete=" DELETE FROM relasistenciamovimientoschecadoras
					WHERE eCodAsistencia=".(int)$_POST['eCodAsistencia'].
					" AND eCodMovimiento=".(int)$rMovimiento{'eCodMovimiento'};
			if(!mysqli_query($link,$delete)){
				$exito=0;
			}else{
				$delete=" DELETE FROM movimientosafiliatorioschecadoras
						WHERE tCodTipoMovimiento='01' 
						AND eCodMovimiento IN (SELECT eCodMovimiento 
											   FROM relasistenciamovimientoschecadoras
											   WHERE eCodAsistencia=".(int)$_POST['eCodAsistencia'].") ";
				if(!mysqli_query($link,$delete)){
					$exito=0;
				}
			}
		}
		print "<input type=\"hidden\" value=\"".((int)$exito>0 ? (int)$_POST['eCodAsistencia'] : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}

}else{
$select=" SELECT inc.*, emp.Empleado, cat.Categoria, cto.tArea, reg.tNombre AS UserREG_Name, 
		cte.tNombreCorto AS tipoEmpleado, reg.tApellidos AS UserREG_Ap, act.tNombre AS UserACT_Name,
		act.tApellidos AS UserACT_Ap, bor.tNombre AS UserCAN_Name , bor.tApellidos AS UserCAN_Ap,
		mvs.tNombre AS UserMOV_Name , mvs.tApellidos AS UserMOV_Ap, cmo.tNombre As Movimiento,
		mov.fhFecha , mov.fhFechaRegistro, mov.dImporte, mov.tFolio, mov.eCodMovimiento,
		mov.eCodUsuario, mov.tCodTipoMovimiento
		FROM asistenciaschecadoras inc
		LEFT JOIN catchecadoras emp ON emp.id=inc.idEmpleado
		LEFT JOIN cattipoempleado cte ON cte.eCodTipoEntidad=2
		LEFT JOIN catusuarios reg ON reg.eCodUsuario=inc.idUsuario
		LEFT JOIN catusuarios act ON act.eCodUsuario=inc.eCodUsuarioActualiza
		LEFT JOIN catusuarios bor ON bor.eCodUsuario=inc.eCodUsuarioCancela
		LEFT JOIN categorias cat ON cat.indice=inc.idCategorias
		LEFT JOIN catcostos cto ON cto.eCodCosto=inc.area
		LEFT JOIN relasistenciamovimientoschecadoras rel ON rel.eCodAsistencia=inc.id
		LEFT JOIN movimientosafiliatorioschecadoras mov ON mov.eCodMovimiento=rel.eCodMovimiento
		LEFT JOIN catusuarios mvs ON mvs.eCodUsuario=mov.eCodUsuario
		LEFT JOIN cattipomovimientos cmo ON cmo.eCodMovimiento=mov.tCodTipoMovimiento
		LEFT JOIN catusuarios umv ON umv.eCodUsuario=mov.eCodUsuario
		WHERE inc.id=".$_GET['eCodAsistencia'];
$rAsistencia=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);

$select=" SELECT *
		FROM catdias
		WHERE eCodDia=".$rEstado['descanso'];
$rDias = mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC); ?>
<script type="text/javascript">
function eliminarMovimiento(){
	var bandera = false;
	var mensaje = "¡Verifique lo siguiente!\n";
	dojo.byId('eAccion').value = 1;	

	if(!dojo.byId("eCodAsistencia").value){
		mensaje+="* Asistencia\n";
		bandera = true;   
	}
	if(bandera==true){
		alert(mensaje);
	}else{
		if(confirm("¿Desea eliminar el movimiento?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+'.php', handleAs: "text", load: function(tRespuesta, ioArgs){
				dojo.byId('dvCNS').innerHTML = tRespuesta;
				if(dojo.byId("eCodigo").value>0){
					alert("¡El movimiento se ha eliminado exitosamente!");
					tURL = './?ePagina=2.5.1.6.2.php&eCodAsistencia='+dojo.byId("eCodigo").value;
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});
		}
	}
	dojo.byId('eAccion').value = "";	
	dojo.byId('folio').value = "";	
}

function eliminarBaj(){
	var bandera = false;
	var mensaje = "¡Verifique lo siguiente!\n";
	dojo.byId('eAccion').value = 3;	

	if(!dojo.byId("eCodAsistencia").value){
		mensaje+="* Asistencia\n";
		bandera = true;   
	}
	if(bandera==true){
		alert(mensaje);
	}else{
		if(confirm("¿Desea eliminar el movimiento?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+'.php', handleAs: "text", load: function(tRespuesta, ioArgs){
				dojo.byId('dvCNS').innerHTML = tRespuesta;
				if(dojo.byId("eCodigo").value>0){
					alert("¡El movimiento se ha eliminado exitosamente!");
					tURL = './?ePagina=2.5.1.6.2.php&eCodAsistencia='+dojo.byId("eCodigo").value;
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});
		}
	}
	dojo.byId('eAccion').value = "";	
	dojo.byId('folio').value = "";	
}
 
function consultar(){
	document.location = './?ePagina=2.5.1.6.php';
}

function nuevo(){
	document.location = './?ePagina=2.4.1.6.php';
}
</script>
<div id="dvCNS" style="display:none;"></div>
<form action="" method="post" name="Datos" id="Datos" onsubmit="return false;">
<input type="hidden" value="" name="eAccion" id="eAccion" />
<input type="hidden" value="<?=$_GET['eCodAsistencia'];?>" name="eCodAsistencia" id="eCodAsistencia" />
<input type="hidden" name="eUsuario" id="eUsuario" value="<?=(int)$_SESSION['sesionUsuario']['eCodUsuario'];?>" />
<table border="0" cellpadding="0" cellspacing="0">
    <tr><td height="20"></td></tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"> Asistencia </td>
	    <td width="50%" nowrap class="sanLR04"><b><?=sprintf("%07d",$rAsistencia{'id'});?></b></td>
		<td nowrap class="sanLR04"></td>
	    <td width="50%" nowrap class="sanLR04"></td>
	</tr>
	<tr>
		<td nowrap class="sanLR04">F. Asistencia </td>	    
	    <td nowrap class="sanLR04"><b><?=date("d/m/Y", strtotime($rAsistencia{'Fecha'}));?></b></td>
		<td nowrap class="sanLR04"> Status </td>
	    <td width="50%" nowrap class="sanLR04"><b><?=utf8_encode( $rAsistencia{'Estado'}=='AC' ? " Activo "	: " Cancelado ");?></b></td>
	</tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"> Nombre</td>
	    <td nowrap class="sanLR04"><b><?=utf8_encode($rAsistencia{'Empleado'});?></b></td>
	    <td height="23" nowrap class="sanLR04"> T. Empleado</td>
	    <td nowrap class="sanLR04"><b><?=utf8_encode($rAsistencia{'tipoEmpleado'});?></b></td>
    </tr>
    <tr>		
		<td height="23" nowrap class="sanLR04"> Categoria </td>
	    <td nowrap class="sanLR04"><b><?=utf8_encode($rAsistencia{'Categoria'});?></b></td>
		<td height="23" nowrap class="sanLR04"> Turno </td>
	    <td nowrap class="sanLR04"><b><?=utf8_encode($rAsistencia{'Turno'});?></b></td>
    </tr>
    <tr>		
		<td height="23" nowrap class="sanLR04"> Area </td>
	    <td nowrap class="sanLR04"><b><?=utf8_encode( $rAsistencia{'area'}>0 ? $rAsistencia{'tArea'} : " N/D ");?></b></td>
		<td height="23" nowrap class="sanLR04"> S.D.I </td>
	    <td nowrap class="sanLR04"><b><?=utf8_encode($rAsistencia{'Salario'});?></b></td>
    </tr>
    <tr>		
		<td height="23" nowrap class="sanLR04">  </td>
	    <td nowrap class="sanLR04"></td>
		<td height="23" nowrap class="sanLR04"> Bono </td>
	    <td nowrap class="sanLR04"><b><?=utf8_encode( $rAsistencia{'dBono'}>0 ? $rAsistencia{'dBono'}	: " N/D ");?></b></td>
    </tr>
    <tr>		
		<td height="23" nowrap class="sanLR04"> U. Registro </td>
		<td nowrap class="sanLR04"><b><?=utf8_encode( $rAsistencia{'idUsuario'}>0 ? $rAsistencia{'UserREG_Name'}." ".$rAsistencia{'UserREG_Ap'}	: " N/D ");?></b></td>	    
		<td height="23" nowrap class="sanLR04"> F. Registro </td>
		<td nowrap class="sanLR04"><b><?=utf8_encode( $rAsistencia{'fechaRegistro'}>'1969-01-01' ? date("d/m/Y H:i:s", strtotime($rAsistencia{'fechaRegistro'}))	: " N/D ");?></b></td>
    </tr>
    <tr>		
		<td height="23" nowrap class="sanLR04"> U. Actualizaci&oacute;n </td>
	    <td nowrap class="sanLR04"><b><?=utf8_encode( $rAsistencia{'eCodUsuarioActualiza'}>0 ? $rAsistencia{'UserACT_Name'}." ".$rAsistencia{'UserACT_Ap'}	: " N/D ");?></b></td>	    
		<td height="23" nowrap class="sanLR04"> F. Actualizaci&oacute;n </td>
		<td nowrap class="sanLR04"><b><?=utf8_encode( $rAsistencia{'fhFechaActualiza'}>'1969-01-01' ? date("d/m/Y H:i:s", strtotime($rAsistencia{'fhFechaActualiza'}))	: " N/D ");?></b></td>	    
    </tr>
    <tr>		
		<td height="23" nowrap class="sanLR04"> U. Cancelaci&oacute;n </td>
		<td nowrap class="sanLR04"><b><?=utf8_encode( $rAsistencia{'eCodUsuarioCancela'}>0 ? $rAsistencia{'UserCAN_Name'}." ".$rAsistencia{'UserCAN_Ap'}	: " N/D ");?></b></td>
		<td height="23" nowrap class="sanLR04"> F. Cancelaci&oacute;n </td>
		<td nowrap class="sanLR04">
			<b><?=($rAsistencia{'fhFechaCancela'}>'1969-01-01' ? date("d/m/Y H:i:s", strtotime($rAsistencia{'fhFechaCancela'})) : " N/D ");?></b>
		</td>
    </tr>
    <tr>		
		<td height="23" nowrap class="sanLR04"> M. Cancelaci&oacute;n </td>
		<td nowrap class="sanLR04" colspan="3"><b><?=utf8_encode( $rAsistencia{'Estado'}=='CA' ? $rAsistencia{'tMotivoCancelacion'} 	: " N/D ");?></b></td>
    </tr>
    <tr><td colspan="4">	<hr width="99%" size="0" align="center" color="#CACACA"></td></tr>
	<tr>
		<td height="23" nowrap class="sanLR04"> Movimiento </td>
	    <td nowrap class="sanLR04"><b><?=utf8_encode( $rAsistencia{'eCodMovimiento'}>0 ? sprintf("%07d",$rAsistencia{'eCodMovimiento'})	: " N/D ");?></b></td>	    
		<td height="23" nowrap class="sanLR04"></td>
	    <td nowrap class="sanLR04"></td>
	</tr>
	<tr>
		<td height="23" nowrap class="sanLR04"> T. Movimiento </td>
		<td nowrap class="sanLR04"><b><?=utf8_encode( $rAsistencia{'tCodTipoMovimiento'}>0 ? $rAsistencia{'Movimiento'}	: " En Proceso ");?></b></td>	    	    
		<td height="23" nowrap class="sanLR04"> F. Movimiento </td>
		<td nowrap class="sanLR04"><b><?=utf8_encode( $rAsistencia{'fhFecha'}>'1969-01-01' ? date("d/m/Y", strtotime($rAsistencia{'fhFecha'}))	: " N/D ");?></b></td>
	</tr>
	<tr>
		<td height="23" nowrap class="sanLR04"> S.D.I. </td>
		<td nowrap class="sanLR04"><b><?=utf8_encode( $rAsistencia{'dImporte'}>0 ? number_format(($rAsistencia{'dImporte'}),2)	: " N/D ");?></b></td>	    	    	    
		<td height="23" nowrap class="sanLR04"> Folio IDSE </td>
		<td nowrap class="sanLR04"><b><?=utf8_encode( $rAsistencia{'tFolio'} ? $rAsistencia{'tFolio'}	: " N/D ");?></b></td>
	</tr>
	<tr>		
		<td height="23" nowrap class="sanLR04"> U. Movimiento </td>
		<td nowrap class="sanLR04">
			<b><?=utf8_encode($rAsistencia{'eCodUsuario'}>0 ? $rAsistencia{'UserMOV_Name'}." ".$rAsistencia{'UserMOV_Ap'} : "N/D");?></b>
		</td>
		<td height="23" nowrap class="sanLR04"> F. Registro </td>
		<td nowrap class="sanLR04">
			<b><?=($rAsistencia{'fhFechaRegistro'}>'1969-01-01' ? date("d/m/Y H:i", strtotime($rAsistencia{'fhFechaRegistro'})) : "N/D");?></b>
		</td>
    </tr>
    <?php
    $select=" SELECT bc.fhFechaRegistro, CONCAT(cu.tNombre, ' ', cu.tApellidos) AS uCancela, 
			ct.tNombre AS tMovimiento, bc.fhFechaRegistroOriginal, bc.dImporte, bc.tFolio, 
			CONCAT(cuo.tNombre, ' ', cuo.tApellidos) AS uRegistro, bc.fhFechaRegistroOriginal
			FROM bitcancelacionmovimientosafilatorioschecadoras bc
			LEFT JOIN catusuarios cu ON cu.eCodUsuario=bc.eCodUsuario
			LEFT JOIN cattipomovimientos ct ON ct.eCodMovimiento=bc.tCodTipoMovimiento
			LEFT JOIN catusuarios cuo ON cuo.eCodUsuario=bc.eCodUsuarioOriginal
			WHERE bc.eCodAsistencia=".(int)$rAsistencia{'id'}.
			" ORDER BY bc.eCodRegistro DESC ";
    $rsCancelaciones=mysqli_query($link,$select);
	$registros=(int)mysqli_num_rows($rsCancelaciones,MYSQLI_ASSOC);
	if((int)$registros>0){ ?>
		<tr><td colspan="4">	<hr width="99%" size="0" align="center" color="#CACACA"></td></tr>
		<table cellspacing="0" border="0" width="100%">
		    <thead>
		        <tr class="thEncabezado">
		    	    <td nowrap="nowrap" class="sanLR04"></td>
		    	    <td nowrap="nowrap" class="sanLR04">Fecha Cancelaci&oacute;n</td>
		        	<td nowrap="nowrap" class="sanLR04">Usuario Cancelaci&oacute;n</td>
		            <td nowrap="nowrap" class="sanLR04">T. Movimiento</td>
		            <td nowrap="nowrap" class="sanLR04">F. Movimiento</td>
		            <td nowrap="nowrap" class="sanLR04">S. D. I.</td>
		            <td nowrap="nowrap" class="sanLR04">Folio IDSE</td>
		            <td nowrap="nowrap" class="sanLR04">Usuario Registro</td>
		            <td nowrap="nowrap" class="sanLR04">Fecha Registro</td>
		            <td nowrap="nowrap" class="sanLR04" width="100%"></td>
		        </tr>   
		    </thead>
		    <tbody>
		    	<?php while($rCancelacion=mysqli_fetch_array($rsCancelaciones,MYSQLI_ASSOC)){ ?>
		    		<tr>
				      	<td nowrap="nowrap" class="sanLR04" height="20" align="center"><img width="16" height="16" title="Estatus" src="images/png/ic-eliminar.png"></td>
						<td nowrap="nowrap" class="sanLR04 columnB"><?=($rCancelacion{'fhFechaRegistro'} ? date("d/m/Y H:i", strtotime($rCancelacion{'fhFechaRegistro'})) : "");?></td>
						<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rCancelacion{'uCancela'});?></td>
						<td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rCancelacion{'tMovimiento'});?></td>
						<td nowrap="nowrap" class="sanLR04"><?=($rCancelacion{'fhFechaRegistroOriginal'} ? date("d/m/Y", strtotime($rCancelacion{'fhFechaRegistroOriginal'})) : "");?></td>
						<td nowrap="nowrap" class="sanLR04 columnB"><?=number_format($rCancelacion{'dImporte'},2);?></td>
						<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rCancelacion{'tFolio'});?></td>
						<td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rCancelacion{'uRegistro'});?></td>
						<td nowrap="nowrap" class="sanLR04"><?=($rCancelacion{'fhFechaRegistroOriginal'} ? date("d/m/Y", strtotime($rCancelacion{'fhFechaRegistroOriginal'})) : "");?></td>
						<td nowrap="nowrap" class="sanLR04" width="100%"></td>
					</tr>
		    	<?php } ?>
		    </tbody>
		</table>
	<?php }
    ?>
</table>
</form>
<?php } ?>