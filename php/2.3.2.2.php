<style>
#divgral {
     overflow:scroll;
     height:450px;
     width:900px;
}
</style>
<?php

require_once("conexion/soluciones-mysql.php"); 
$link = getLink();


if($_POST){
	if($_POST['eAccion']==1){
		foreach($_POST as $k => $valor){
			$nombre = strval($k);
			$campo = "eCodEntidad";
			if(strstr($nombre,$campo) && (int)$valor>0){
				$update=" UPDATE catentidades SET tCodEstatus='CA' WHERE eCodEntidad=".$valor;
				mysqli_query($link,$update);
			}
		}
	}

			$select=" SELECT emp.* ,  tpe.eCodTipoEntidad,  tpe.tNombre as tipo ".
			" FROM catentidades as emp ".
			" INNER JOIN cattiposentidades as tpe on tpe.eCodTipoEntidad=emp.eCodTipoEntidad ".
			" WHERE 1=1".
			($_POST['eCodEntidad'] ? " AND emp.id=".$_POST['eCodEntidad'] : "").
			($_POST['tNombre'] ? " AND emp.Empleado LIKE '%".$_POST['tNombre']."%'" : "").
			($_POST['tRFC'] ? " AND emp.rfc LIKE '%".$_POST['tRFC']."%'" : "").
			($_POST['tNSS'] ? " AND emp.IMSS LIKE '%".$_POST['tNSS']."%'" : "").
			($_POST['tipoempleado'] ? " AND tpe.eCodTipoEntidad= ".$_POST['tipoempleado'] : "").
			($_POST['tCURP'] ? " AND emp.curp LIKE '%".$_POST['tCURP']."%'" : "");	
			//print($select);
	$rsEntidades=mysqli_query($link,$select); 
	$rsUsuarios=mysqli_query($link,$select);
	$registros=(int)mysqli_num_rows($rsUsuarios);
	?>

    <table cellspacing="0" border="0" width="900px">
    <tr>
    <td width="50%"><hr color="#666666" /></td>
    <td class="sanLR04" nowrap="nowrap"><b>Registros Encontrados: ( <?=$registros;?> )</b></td>
	<td width="50%"><hr color="#666666" /></td>
    </tr>
    </table>
	<div id="divgral" style="display:block; top:0; left:0; width:900px; z-index=1; overflow: auto;">
	<table cellspacing="0" border="0" width="900px" >		
		<thead>
			<tr class="thEncabezado">
				<td nowrap="nowrap" class="sanLR04" height="20" align="center">C</td>
				<td nowrap="nowrap" class="sanLR04">E</td>
				<td nowrap="nowrap" class="sanLR04">C&oacute;digo</td>
                <td nowrap="nowrap" class="sanLR04">RFC</td>
				<td nowrap="nowrap" class="sanLR04">Tipo</td>
				<td nowrap="nowrap" class="sanLR04">Nombre</td>
				<td nowrap="nowrap" class="sanLR04" width="100%">Registro Patronal</td>								
			</tr>
		</thead>
		<tbody>
			<?php $i=1; while($rEntidad=mysqli_fetch_array($rsEntidades,MYSQLI_ASSOC)){ ?>
				<tr>
					<td nowrap="nowrap" class="sanLR04" height="20" align="center"><input type="checkbox" name="eCodEntidad<?=$i?>" id="eCodEntidad<?=$i?>" value="<?=$rEntidad{'eCodEntidad'};?>" ></td>
					<td nowrap="nowrap" class="sanLR04"><img width="16" height="16" alt="Usuario" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-<?=$rEntidad{'tCodEstatus'};?>.png"></td>
					<td nowrap="nowrap" class="sanLR04 colmenu" ><a href="?ePagina=2.3.2.2.1.php&eCodEntidad=<?=$rEntidad{'eCodEntidad'};?>"><b><?=sprintf("%07d",$rEntidad{'eCodEntidad'});?></b></a></td>
					<td nowrap="nowrap" class="sanLR04"><a class="txtCO12" href="?ePagina=2.3.2.2.2.php&eCodEntidad=<?=$rEntidad{'eCodEntidad'};?>"><?=utf8_encode($rEntidad{'tRFC'});?></a></td>
                    <td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rEntidad{'tipo'});?></a></td>
                    <td nowrap="nowrap" class="sanLR04 "><?=utf8_encode($rEntidad{'tNombre'});?></a></td>
                    <td nowrap="nowrap" class="sanLR04 columnB" width="100%"><?=utf8_encode($rEntidad{'tRegistroPatronal'});?></a></td>                    					
				</tr>
			<?php $i++; } ?>
		</tbody>
	</table>
	</div>
<?php }else{ ?>	

	<script type="text/javascript">

	function nuevo(){

		document.location = "?ePagina=2.3.2.2.1.php";

	}



	function eliminar(){

		var eChk = 0;

		dojo.byId('eAccion').value = 1;	

		dojo.query("[id*=\"eCodSucursal\"]:checked").forEach(function(nodo, index, array){eChk++;});

	

		if(eChk!=0){

			if(confirm("¿Desea eliminar las sucursales?")){

				dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+'.php', handleAs: "text", load: function(tRespuesta, ioArgs){

					dojo.byId('dvCNS').innerHTML = tRespuesta; 

					alert("¡Los registros se han eliminado exitosamente!");	

				}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});

			}

		}else{

			alert("No ha seleccionado ninguna entidad");

		}

		dojo.byId('eAccion').value = "";	

	}

	dojo.addOnLoad(function(){filtrarConsulta();});

	</script>
<form name="Datos" id="Datos" method="post" action="" onsubmit="return false;">
  <input type="hidden" value="0" name="ePagina" id="ePagina" />
  <input type="hidden" value="" name="eAccion" id="eAccion" />
  <table width="900px" border="0">
    <tr>
      <td colpan="3" width="100%"></td>
      <td align="right" nowrap="nowrap"><a class="fntBR11" href="javaScript:mostrarFiltros('Busqueda');">Busqueda de Registros</a></td>
    </tr>
    <tr id="trBusqueda" style="display:none">
      <td colspan="4"><table width="100%" bgcolor="#f9f9f9">
          <tr>
            <td class="sanLR04" height="5"></td>
          </tr>
          <tr>
            <td class="sanLR04" height="20">C&oacute;digo</td>
            <td class="sanLR04" width="50%"><input type="text" name="eCodEntidad" dojoType="dijit.form.TextBox" id="eCodEntidad" value="" style="width:80px"></td>
            <td class="sanLR04">Nombre</td>
            <td class="sanLR04" width="50%"><input type="text" name="tNombre" dojoType="dijit.form.TextBox" id="tNombre" value="" style="width:175px"></td>
          </tr>
          <tr>
            <td class="sanLR04" height="20">RFC</td>
            <td class="sanLR04" width="50%"><input type="text" name="tRFC" dojoType="dijit.form.TextBox" id="tRFC" value="" style="width:175px"></td>
            <td class="sanLR04" nowrap="nowrap">Registro Patronal</td>
            <td class="sanLR04" width="50%"><input type="text" name="tregistroPatronal" dojoType="dijit.form.TextBox" id="tregistroPatronal" value="" style="width:175px"></td>
          </tr>
          <tr>
            <td class="sanLR04" height="5"></td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td colspan="4"><div id="dvCNS"></div></td>
    </tr>
  </table>
</form>
<?php } ?>