<?php require_once("conexion/soluciones-mysql.php");  
$link = getLink();
if($_POST){
	if($_POST['eProceso']==1){
		$exito				= 0;
		$tNombre			= ($_POST['tNombre']			? "'".trim(utf8_decode(addslashes($_POST['tNombre'])))."'"		: "NULL");
		$tNombreCorto		= ($_POST['tNombreCorto']		? "'".trim(utf8_decode(addslashes($_POST['tNombreCorto'])))."'"	: "NULL");
		$bRefrigerado		= ($_POST['bRefrigerado']		? $_POST['bRefrigerado']							: "NULL");
		$eCodTipoContenedor	= ($_POST['eCodTipoContenedor']	? (int)$_POST['eCodTipoContenedor']					: "NULL");

		if((int)$eCodTipoContenedor>0){
			$update=" UPDATE cattiposcontenedores ".
					" SET tNombre=".$tNombre.",".
					" tNombreCorto=".$tNombreCorto.",".
					" bRefrigerado=".$bRefrigerado.
					" WHERE eCodTipoContenedor=".$eCodTipoContenedor;
			if($res=mysqli_query($link,$update)){
				$exito=1;
			}else{
				$exito=0;
			}
		}else{
			$insert=" INSERT INTO cattiposcontenedores(tCodEstatus, tNombre, tNombreCorto, bRefrigerado) ".
					" VALUES ('AC', ".$tNombre.", ".$tNombreCorto.", ".$bRefrigerado.")";
			print $insert;
			if($res=mysqli_query($link,$insert)){
				$exito=1;
				$select=" select last_insert_id() AS Llave ";
				$rCodigo=mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC);
				$eCodTipoContenedor=(int)$rCodigo['Llave'];
			}else{
				$exito=0;
			}
		}
		print "<input type=\"text\" value=\"".($exito==1 ? $eCodTipoContenedor : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}
	
	if($_POST['eProceso']==2){
		$rTipoContenedor=mysqli_fetch_array(mysqli_query($link,"SELECT 1 AS TipoContenedor FROM cattiposcontenedores WHERE tNombre='".trim($_POST['tNombre'])."' AND eCodTipoContenedor!=".(int)$_POST['eCodTipoContenedor']),MYSQLI_ASSOC);
		print "<input name=\"eTipoContenedor\" id=\"eTipoContenedor\" value=\"".(int)$rTipoContenedor{'TipoContenedor'}."\">";
	}
}
if(!$_POST){
	
$select=" SELECT * ".
		" FROM cattiposcontenedores ".
		" WHERE eCodTipoContenedor=".$_GET['eCodTipoContenedor'];
$rTipoContenedor = mysqli_fetch_array(mysqli_query($link,$select),MYSQLI_ASSOC); ?>
<script type="text/javascript">
dojo.require("dojo.number");
dojo.require("dijit.form.NumberTextBox");
function guardar(){
	dojo.byId('eProceso').value=1;
	bandera = false;
	mensaje = "¡Verifique lo siguiente!\n";
	if (!dojo.byId("tNombre").value){
		mensaje+="* Nombre\n";
		bandera = true;		
	}
	if (!dojo.byId("tNombreCorto").value){
		mensaje+="* Nombre Corto\n";
		bandera = true;		
	}
	
	if (bandera==true){
		alert(mensaje);		
	}else{
		if (confirm("¿Desea Guardar la Información?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				if(dojo.byId("eCodigo").value>0){
					alert("¡La información se guardó correctamente!");
					tURL = './?ePagina=2.3.3.3.2.php&eCodTipoContenedor='+dojo.byId("eCodigo").value;
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
		}
	}
}	

function verificarTipoContenedor(){
	if(dojo.byId('tNombre').value){
		dojo.byId('eProceso').value=2;
		dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
			dojo.byId("dvCNS").innerHTML = tRespuesta;
			if(dojo.byId('eTipoContenedor').value==1){
				alert("¡Este tipo de contenedor ya existe!");
				dojo.byId('tNombre').value="";
			}
		}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
	}
}

function consultar(){
	document.location = './?ePagina=2.3.3.3.php';
}
</script>
<div id="dvCNS" style="display:none;"></div>
<form action="" method="post" name="Datos" id="Datos" onsubmit="return false;">
<input type="hidden" name="eProceso" id="eProceso" value="" />
<input type="hidden" name="eCodTipoContenedor" id="eCodTipoContenedor" value="<?=$_GET['eCodTipoContenedor'];?>" />
<table border="0" cellpadding="0" cellspacing="0">
    <tr><td height="20"></td></tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Nombre </td>
	    <td width="100%" nowrap class="sanLR04">
           	<input name="tNombre" type="text" dojoType="dijit.form.TextBox" id="tNombre" value="<?=utf8_encode($rTipoContenedor{'tNombre'});?>" style="width:175px" onblur="verificarTipoContenedor();">
        </td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Nombre Corto </td>
	    <td width="100%" nowrap class="sanLR04">
           	<input name="tNombreCorto" type="text" dojoType="dijit.form.TextBox" id="tNombreCorto" value="<?=utf8_encode($rTipoContenedor{'tNombreCorto'});?>" style="width:175px">
        </td>
    </tr>
	<tr>
	    <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/noobligatorio.png" height="0" width="16" align="absmiddle"> Refrigerado </td>
	    <td width="100%" nowrap class="sanLR04">
			<label>
           	<input name="bRefrigerado" type="checkbox" id="bRefrigerado" value="1" <?=($rTipoContenedor{'bRefrigerado'} ? "checked='checked'" : "");?>>
			S&iacute;</label>
        </td>
    </tr>
</table>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04">&nbsp;</td>
  </tr>
  <tr>
    <td valign="top" nowrap="nowrap" class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle" /> <font class="fntG10K">Indica que es obligatorio llenar el campo.
    </font></td>
    </tr>
</table>
</form>
<?php } ?>