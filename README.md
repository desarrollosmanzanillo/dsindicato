<p align="center">
    <img src="https://tapterminal.com/img/logoTap.png?raw=true" alt="Tapterminal Logo"/>
</p>

***
## 💪 IDE´s recomendados
* [![forthebadge](https://img.shields.io/badge/Visual_Studio_Code-0078D4?style=for-the-badge&logo=visual%20studio%20code&logoColor=white)](https://code.visualstudio.com/download)
* [![forthebadge](https://img.shields.io/badge/sublime_text-%23575757.svg?&style=for-the-badge&logo=sublime-text&logoColor=important)](https://www.sublimetext.com/3)
* [![forthebadge](https://img.shields.io/badge/Atom-66595C?style=for-the-badge&logo=Atom&logoColor=white)](ttps://atom.io/)
***
## 📣 Pasos a seguir
1.- Clonamos el repositorio 

[![forthebadge](https://img.shields.io/badge/Bitbucket-0747a6?style=for-the-badge&logo=bitbucket&logoColor=white)](http://forthebadge.com)
```sh
$git clone https://kaguilartap@bitbucket.org/desarrollosmanzanillo/sindicato.git
```

2.- En ubunto ingresamos a la sig. ruta donde ubicaremos nuestro proyecto
```sh
   cd var/www/html/
```

3.- Nos cambiamos de branch a la que nos proporcionen.
```sh
$cd sindicato/
$ git checkout NOMBRE_BRANCH
```

4.- Corremos la aplicación desde el navaegador web [![forthebadge](https://img.shields.io/badge/Google_chrome-4285F4?style=for-the-badge&logo=Google-chrome&logoColor=white)](http://thismypc.com/)
```sh
   localhost/sindicato
```
> 🔰 Navega a traves de http://localhost:4200/ 

> 🔰 La aplicación será automaticamente recargado si haces cambios en cualquier parte del codigo.

## 🤝 Contribuir

[![forthebadge](https://img.shields.io/badge/Jira-0052CC?style=for-the-badge&logo=Jira&logoColor=white)](http://forthebadge.com)
[![forthebadge](https://img.shields.io/badge/Zoom-2D8CFF?style=for-the-badge&logo=zoom&logoColor=white)](http://forthebadge.com)
[![forthebadge](https://img.shields.io/badge/Slack-4A154B?style=for-the-badge&logo=slack&logoColor=white)](http://forthebadge.com)

[![All Contributors][all-contributors-url]](#contributors)  [![Maintenance](https://img.shields.io/badge/Maintained%3F-yes-green.svg)](https://github.com/supunlakmal/thismypc/graphs/commit-activity) [![Commitizen friendly][commitizen]][commitizen-url]

1. 🖼️ Crea tu propia rama 
```sh
$ git checkout -b my-new-feature
```
2. 💬 Crea commits con tus cambios 
```sh
$ git commit -m 'Add some feature'
```
3. 🔝 Sube tus cambios 
```sh
$ git push origin my-new-feature
```
4. 🔁 Crea un nuevo 
```sh
$ pull request
```
***
## 📝 Licencia
[![License](https://img.shields.io/github/license/sourcerer-io/sourcerer-app.svg?colorB=ff0000)]()

`Copyright ©️`

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files `(the "Software")`, to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
***
