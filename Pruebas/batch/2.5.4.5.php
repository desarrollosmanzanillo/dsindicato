<?php require_once("../conexion/soluciones-mysql.php");
    $directorio="MODIFICACIONES.dat";
    date_default_timezone_set('America/Mexico_City');
    $varfecha=isset($_GET["fecha"]) ? $_GET["fecha"] : date("Y-m-d");
    if($_GET['eCodTipoEntidad']==2){
        $fechaayer = date('Y-m-d',strtotime($varfecha));
        $fechaantier = date('Y-m-d',strtotime('-1 days', strtotime($varfecha)));
    }else{
        $fechaayer = date('Y-m-d',strtotime('-1 days', strtotime($varfecha)));
        $fechaantier = date('Y-m-d',strtotime('-2 days', strtotime($varfecha)));
    }
    $idUsuario = ($_GET['eUsuario'] ? $_GET['eUsuario'] : "NULL");
    
    $select=" SELECT * FROM sisconfiguracion ";
    $configura=mysql_fetch_array(mysql_query($select));
    
    //////Inicio Continuaciones-->
    $select=" SELECT * ".
            " FROM categorias ".
            " WHERE indice=27 ";
    $salarioMin=mysql_fetch_array(mysql_query($select));
    
    $select=" SELECT idEmpleado ".
            " FROM incapacidades ".
            " WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."' ";
    $incapacidades=mysql_query($select);
    $UsuariosInc="0";
    while($incapacidad=mysql_fetch_array($incapacidades)){
        $UsuariosInc.=", ".$incapacidad{'idEmpleado'};
    }
    $select=" SELECT assi.id ".
            " FROM movimientosafiliatorios mos ".
            " INNER JOIN relasistenciamovimientos rassi ON rassi.eCodMovimiento=mos.eCodMovimiento ".
            " INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia ".
            " LEFT JOIN empleados AS emp ON assi.idEmpleado = emp.id ".
            " WHERE 1=1 ".
            " AND assi.Fecha Between '".$fechaantier."' AND '".$fechaayer."' ".
            " AND emp.eCodTipo=".$_GET['eCodTipoEntidad'];
    $afiliatorios=mysql_query($select);
    $UsuariosAfil="0";
    while($afiliatorio=mysql_fetch_array($afiliatorios)){
        $UsuariosAfil.=", ".$afiliatorio{'id'};
    }
    $select=" SELECT assihh.idEmpleado ".
            " FROM movimientosafiliatorios moshh ".
            " INNER JOIN relasistenciamovimientos rassihh ON rassihh.eCodMovimiento=moshh.eCodMovimiento ".
            " INNER JOIN asistencias assihh ON assihh.id=rassihh.eCodAsistencia ".
            " LEFT JOIN empleados AS emp ON assihh.idEmpleado=emp.id ".
            " WHERE moshh.fhFecha='".$fechaantier."' AND moshh.tCodTipoMovimiento NOT IN ('02') AND moshh.eCodEstatus='AC' ".
            " AND emp.eCodTipo=".$_GET['eCodTipoEntidad'];
    $movimientos=mysql_query($select);
    $UsiariosMovi="0";
    while($movimiento=mysql_fetch_array($movimientos)){
        $UsiariosMovi.=", ".$movimiento{'idEmpleado'};
    }

    $select=" SELECT DISTINCT emple.id AS numeral, emple.IMSS AS imss, cent.tRegistroPatronal AS rp, emple.Paterno AS pat, ".
            " Materno AS mat, Nombre AS nomb, ctiemp.eCodTipoJornada AS tipjo, '".$fechaayer."' AS Fecha, emple.Clinica AS clin, emple.CURP AS curp, ".
" CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
    FROM asistencias asish 
    WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
    AND asish.Estado='AC' 
    AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
    AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
    AND asish.id NOT IN (".$UsuariosAfil.")
    AND asish.idEmpleado IN (".$UsiariosMovi.")
    AND asish.idEmpleado=asis.idEmpleado)>".(float)$configura{'dTopeSalario'}." THEN ".(float)$configura{'dTopeSalario'}.
" ELSE CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
        FROM asistencias asish 
        WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
        AND asish.Estado='AC' 
        AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
        AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
        AND asish.id NOT IN (".$UsuariosAfil.")
        AND asish.idEmpleado IN (".$UsiariosMovi.")
        AND asish.idEmpleado=asis.idEmpleado)=0 THEN FORMAT((".(float)$salarioMin{'Turno1'}."*ctiemp.dFactorIntegracion),2) ".
        " ELSE CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
            FROM asistencias asish 
            WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
            AND asish.Estado='AC' 
            AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
            AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
            AND asish.id NOT IN (".$UsuariosAfil.")
            AND asish.idEmpleado IN (".$UsiariosMovi.")
            AND asish.idEmpleado=asis.idEmpleado)<".$configura{'dSalarioMinimo'}." THEN ".$configura{'dSalarioMinimo'}.
            " ELSE (SELECT sum(asish.Salario+asish.dBono)
                FROM asistencias asish 
                WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
                AND asish.Estado='AC' 
                AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
                AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
                AND asish.id NOT IN (".$UsuariosAfil.")
                AND asish.idEmpleado IN (".$UsiariosMovi.")
                AND asish.idEmpleado=asis.idEmpleado)
            END 
        END
    END AS tSalario ".

    " FROM asistencias asis ".
    " INNER JOIN empleados emple ON emple.id=asis.idEmpleado ".
    " INNER JOIN cattipoempleado ctiemp ON ctiemp.eCodTipoEntidad=emple.eCodTipo ".
    " LEFT JOIN catentidades cent ON cent.eCodEntidad=asis.idEmpresa ".
    " WHERE asis.Estado='AC' AND asis.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asis.id) ".
    " AND asis.idEmpleado NOT IN (SELECT assi.idEmpleado FROM movimientosafiliatorios mos INNER JOIN relasistenciamovimientos rassi ON ".
    " rassi.eCodMovimiento=mos.eCodMovimiento INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia WHERE assi.id=asis.id) ".
    " AND asis.Fecha Between '".$fechaantier."' AND '".$fechaayer."' AND asis.idCategorias NOT IN (33) AND emple.eCodTipo=".$_GET['eCodTipoEntidad']." AND asis.idEmpleado NOT IN (".$UsuariosInc.") ".
    "AND asis.idEmpleado IN (SELECT assi.idEmpleado FROM movimientosafiliatorios mos INNER JOIN relasistenciamovimientos rassi ON rassi.eCodMovimiento=mos.eCodMovimiento ".
    " INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia WHERE assi.idEmpleado=asis.idEmpleado AND mos.fhFecha='".$fechaantier."' ".
    " AND mos.tCodTipoMovimiento NOT IN ('02') AND mos.eCodEstatus='AC') AND ".
" CASE WHEN (SELECT mosa.dImporte FROM movimientosafiliatorios mosa 
INNER JOIN relasistenciamovimientos rassia ON rassia.eCodMovimiento=mosa.eCodMovimiento 
INNER JOIN asistencias assia ON assia.id=rassia.eCodAsistencia
WHERE assia.idEmpleado=asis.idEmpleado AND mosa.fhFecha='".$fechaantier."' AND mosa.tCodTipoMovimiento NOT IN ('02') AND mosa.eCodEstatus='AC' LIMIT 1) IS NULL THEN 0 ELSE (SELECT mosa.dImporte
FROM movimientosafiliatorios mosa 
INNER JOIN relasistenciamovimientos rassia ON rassia.eCodMovimiento=mosa.eCodMovimiento 
INNER JOIN asistencias assia ON assia.id=rassia.eCodAsistencia
WHERE assia.idEmpleado=asis.idEmpleado AND mosa.fhFecha='".$fechaantier."' AND mosa.tCodTipoMovimiento NOT IN ('02') AND mosa.eCodEstatus='AC' LIMIT 1) END = 
CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)>".(float)$configura{'dTopeSalario'}." THEN ".(float)$configura{'dTopeSalario'}." ELSE ".
    " CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)=0 THEN FORMAT((".(float)$salarioMin{'Turno1'}."*ctiemp.dFactorIntegracion),2) ELSE ".
    " CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)<".$configura{'dSalarioMinimo'}." THEN ".$configura{'dSalarioMinimo'}." ELSE (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (".$UsuariosInc.") 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado) END ".
    " END ".
    " END
UNION 
SELECT DISTINCT emple.id AS numeral, emple.IMSS AS imss, cent.tRegistroPatronal AS rp, emple.Paterno AS pat, Materno AS mat, Nombre AS nomb, ctiemp.eCodTipoJornada AS tipjo, '".$fechaayer."' AS Fecha, emple.Clinica AS clin, emple.CURP AS curp, CASE WHEN (SELECT mosa.dImporte
FROM movimientosafiliatorios mosa 
INNER JOIN relasistenciamovimientos rassia ON rassia.eCodMovimiento=mosa.eCodMovimiento 
INNER JOIN asistencias assia ON assia.id=rassia.eCodAsistencia
WHERE assia.idEmpleado=asis.idEmpleado AND mosa.fhFecha='".$fechaantier."' AND mosa.tCodTipoMovimiento NOT IN ('02') AND mosa.eCodEstatus='AC' LIMIT 1)>".(float)$configura{'dTopeSalario'}." THEN ".(float)$configura{'dTopeSalario'}." ELSE (SELECT mosa.dImporte
FROM movimientosafiliatorios mosa 
INNER JOIN relasistenciamovimientos rassia ON rassia.eCodMovimiento=mosa.eCodMovimiento 
INNER JOIN asistencias assia ON assia.id=rassia.eCodAsistencia
WHERE assia.idEmpleado=asis.idEmpleado AND mosa.fhFecha='".$fechaantier."' AND mosa.tCodTipoMovimiento NOT IN ('02') AND mosa.eCodEstatus='AC' LIMIT 1) END AS tSalario
FROM asistencias asis
INNER JOIN empleados emple ON emple.id=asis.idEmpleado
INNER JOIN cattipoempleado ctiemp ON ctiemp.eCodTipoEntidad=emple.eCodTipo
LEFT JOIN catentidades cent ON cent.eCodEntidad=asis.idEmpresa
WHERE asis.Fecha Between '".$fechaantier."' AND '".$fechaayer."' 
AND asis.Estado='AC' 
AND asis.idEmpleado NOT IN (".$UsuariosInc.") 
AND asis.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asis.id) 
AND asis.idEmpleado NOT IN (SELECT assi.idEmpleado 
FROM movimientosafiliatorios mos 
INNER JOIN relasistenciamovimientos rassi ON rassi.eCodMovimiento=mos.eCodMovimiento 
INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia
WHERE assi.id=asis.id)
AND asis.idEmpleado IN (SELECT assi.idEmpleado 
FROM movimientosafiliatorios mos 
INNER JOIN relasistenciamovimientos rassi ON rassi.eCodMovimiento=mos.eCodMovimiento 
INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia
WHERE assi.idEmpleado=asis.idEmpleado AND mos.fhFecha='".$fechaantier."' AND mos.tCodTipoMovimiento NOT IN ('02') AND mos.eCodEstatus='AC')
AND asis.idEmpleado IN (SELECT eCodEmpleado FROM ausentismo WHERE fhFecha='".$fechaayer."' AND eCodEstatus='AC' AND eCodEmpleado=asis.idEmpleado)
AND asis.idCategorias NOT IN (33)
AND emple.eCodTipo=".$_GET['eCodTipoEntidad'];
$vContinua=$select;
            $continuaciones=mysql_query($select);
            $select=" SELECT * FROM sisconfiguracion ";
            $configura2=mysql_fetch_array(mysql_query($select));
            while($continua=mysql_fetch_array($continuaciones)){
                $dSalarioR=((float)$continua{'tSalario'}>(float)$configura2{'dTopeSalario'} ? (float)$configura2{'dTopeSalario'} : (float)$continua{'tSalario'});
                //print $dSalarioR."==".(float)$configura2{'dTopeSalarioMinimoDescanso'}."<br>";
                if($dSalarioR==(float)$configura2{'dTopeSalarioMinimoDescanso'}){
                    //" VALUES (".$idUsuario.", '".$fechaayer."', '01', 'AC', ".(float)$rAsistencia{'tSalario'}.", 'NULL', CURRENT_TIMESTAMP) ";
                    $insert=" INSERT INTO movimientosafiliatorios (eCodUsuario, fhFecha, tCodTipoMovimiento,  eCodEstatus, dImporte, tFolio, fhFechaRegistro) ".                        
                            " VALUES (".$idUsuario.", '".$fechaayer."', '01', 'AC', ".$dSalarioR." , 'NA', CURRENT_TIMESTAMP) ";
                    if($res=mysql_query($insert)){
                        $exito=1;
                        $select=" select last_insert_id() AS Llave ";
                        $rCodigo=mysql_fetch_array(mysql_query($select));
                        $eCodMovimiento=(int)$rCodigo['Llave'];
    
                        $select=" SELECT asish.id
    FROM asistencias asish 
    WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
    AND asish.Estado='AC' 
    AND asish.idEmpleado NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."') 
    AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
    AND asish.idEmpleado NOT IN (SELECT assih.idEmpleado 
    FROM movimientosafiliatorios mosh 
    INNER JOIN relasistenciamovimientos rassih ON rassih.eCodMovimiento=mosh.eCodMovimiento 
    INNER JOIN asistencias assih ON assih.id=rassih.eCodAsistencia
    WHERE assih.id=asish.id)
    AND asish.idEmpleado IN (SELECT assihh.idEmpleado 
    FROM movimientosafiliatorios moshh 
    INNER JOIN relasistenciamovimientos rassihh ON rassihh.eCodMovimiento=moshh.eCodMovimiento 
    INNER JOIN asistencias assihh ON assihh.id=rassihh.eCodAsistencia
    WHERE assihh.idEmpleado=asish.idEmpleado AND moshh.fhFecha='".$fechaantier."' AND moshh.tCodTipoMovimiento NOT IN ('02') AND moshh.eCodEstatus='AC')
    AND asish.idEmpleado=".$continua{'numeral'};
                        $asistenciasEmp=mysql_query($select);
                        while($asisEmp=mysql_fetch_array($asistenciasEmp)){
                            $insertt=" INSERT INTO relasistenciamovimientos (eCodAsistencia, eCodMovimiento) ".
                                    " VALUES (".$asisEmp{'id'}.", ".$eCodMovimiento.") ";
                            if($res=mysql_query($insertt)){
                                $exito=1;
                            }else{
                                $exito=0;
                            }
                        }
                    }else{
                        $exito=0;
                    }
                }
            } 
    //////<--Fin Continuaciones idCategorias
    
    $select=" SELECT * ".
            " FROM categorias ".
            " WHERE indice=27 ";
    $salarioMin=mysql_fetch_array(mysql_query($select));

    $select=" SELECT assi.id ".
            " FROM movimientosafiliatorios mos ".
            " INNER JOIN relasistenciamovimientos rassi ON rassi.eCodMovimiento=mos.eCodMovimiento ".
            " INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia ".
            " INNER JOIN empleados emple ON emple.id=assi.idEmpleado ".
            " WHERE assi.Fecha Between '".$fechaantier."' AND '".$fechaayer."' ".
            " AND emple.eCodTipo=".$_GET['eCodTipoEntidad'];
    $afiliatorios=mysql_query($select);
    $UsuariosAfil="0";
    while($afiliatorio=mysql_fetch_array($afiliatorios)){
        $UsuariosAfil.=", ".$afiliatorio{'id'};
    }
    $select=" SELECT assihh.idEmpleado ".
            " FROM movimientosafiliatorios moshh ".
            " INNER JOIN relasistenciamovimientos rassihh ON rassihh.eCodMovimiento=moshh.eCodMovimiento ".
            " INNER JOIN asistencias assihh ON assihh.id=rassihh.eCodAsistencia ".
            " INNER JOIN empleados emple ON emple.id=assihh.idEmpleado ".
            " WHERE moshh.fhFecha='".$fechaantier."' AND moshh.tCodTipoMovimiento NOT IN ('02') ".
            " AND moshh.eCodEstatus='AC' AND emple.eCodTipo=".$_GET['eCodTipoEntidad'];
    $movimientos=mysql_query($select);
    $UsiariosMovi="0";
    while($movimiento=mysql_fetch_array($movimientos)){
        $UsiariosMovi.=", ".$movimiento{'idEmpleado'};
    }

   $select=" SELECT DISTINCT emple.id AS numeral, emple.IMSS AS imss, cent.tRegistroPatronal AS rp, emple.Paterno AS pat, Materno AS mat, Nombre AS nomb, ctj.eCodJornada AS tipjo, '".$fechaayer."' AS Fecha, emple.Clinica AS clin, emple.CURP AS curp, ctiemp.eCodTipoTrabajador AS eCodTipoTrabajador, cts.eCodSalario AS TipoSalario, 
    CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."') 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)>".(float)$configura{'dTopeSalario'}." THEN ".(float)$configura{'dTopeSalario'}." ELSE 
    CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."') 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)=0 THEN FORMAT((".(float)$salarioMin{'Turno1'}."*ctiemp.dFactorIntegracion),2) ELSE ".
    " CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."') 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)<".$configura{'dSalarioMinimo'}." THEN ".$configura{'dSalarioMinimo'}." ELSE (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."') 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado) END ".
    " END
    END AS Salario
FROM asistencias asis
INNER JOIN empleados emple ON emple.id=asis.idEmpleado
INNER JOIN cattipoempleado ctiemp ON ctiemp.eCodTipoEntidad=emple.eCodTipo
LEFT JOIN catentidades cent ON cent.eCodEntidad=asis.idEmpresa
LEFT JOIN cattiposalario cts ON cts.eCodSalario=ctiemp.eCodTipoSalario
LEFT JOIN cattipojornada ctj ON ctj.eCodJornada=ctiemp.eCodTipoJornada
WHERE asis.Fecha Between '".$fechaantier."' AND '".$fechaayer."' AND emple.eCodTipo=".$_GET['eCodTipoEntidad']."
AND asis.Estado='AC' 
AND asis.idEmpleado NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."') 
AND asis.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asis.id) 
AND asis.id NOT IN (".$UsuariosAfil.")
AND asis.idEmpleado IN (SELECT assi.idEmpleado 
FROM movimientosafiliatorios mos 
INNER JOIN relasistenciamovimientos rassi ON rassi.eCodMovimiento=mos.eCodMovimiento 
INNER JOIN asistencias assi ON assi.id=rassi.eCodAsistencia
WHERE assi.idEmpleado=asis.idEmpleado AND mos.fhFecha='".$fechaantier."' AND mos.tCodTipoMovimiento NOT IN ('02') AND mos.eCodEstatus='AC')
AND CASE WHEN (SELECT mosa.dImporte
FROM movimientosafiliatorios mosa 
INNER JOIN relasistenciamovimientos rassia ON rassia.eCodMovimiento=mosa.eCodMovimiento 
INNER JOIN asistencias assia ON assia.id=rassia.eCodAsistencia
WHERE assia.idEmpleado=asis.idEmpleado AND mosa.fhFecha='".$fechaantier."' AND mosa.tCodTipoMovimiento NOT IN ('02') AND mosa.eCodEstatus='AC' LIMIT 1) IS NULL THEN 0 ELSE (SELECT mosa.dImporte
FROM movimientosafiliatorios mosa 
INNER JOIN relasistenciamovimientos rassia ON rassia.eCodMovimiento=mosa.eCodMovimiento 
INNER JOIN asistencias assia ON assia.id=rassia.eCodAsistencia
WHERE assia.idEmpleado=asis.idEmpleado AND mosa.fhFecha='".$fechaantier."' AND mosa.tCodTipoMovimiento NOT IN ('02') AND mosa.eCodEstatus='AC' LIMIT 1) END<>
CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."') 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)>".(float)$configura{'dTopeSalario'}." THEN ".(float)$configura{'dTopeSalario'}." ELSE 
CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."') 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)=0 THEN FORMAT((".(float)$salarioMin{'Turno1'}."*ctiemp.dFactorIntegracion),2) ELSE ".
    " CASE WHEN (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."') 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado)<".$configura{'dSalarioMinimo'}." THEN ".$configura{'dSalarioMinimo'}." ELSE (SELECT sum(asish.Salario+asish.dBono)
FROM asistencias asish 
WHERE asish.Fecha Between '".$fechaantier."' AND '".$fechaayer."'
AND asish.Estado='AC' 
AND asish.idEmpleado NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."') 
AND asish.id NOT IN (SELECT eCodAsistencia FROM relasistenciamovimientos WHERE eCodAsistencia=asish.id) 
AND asish.id NOT IN (".$UsuariosAfil.")
AND asish.idEmpleado IN (".$UsiariosMovi.")
AND asish.idEmpleado=asis.idEmpleado) END ".
    " END
END
     ";
//$rsServicios=mysql_query($select);
$cont=0;


 $asi=mysql_query($select);
  if(file_exists($directorio))
  {    
    if(unlink($directorio))
    {
    $ar=fopen($directorio,"a") or die("Problemas en la creacion");
    $count=0;
        while($asist=mysql_fetch_array($asi))
        {
            $count=$count+1;
            $salario=explode(".",str_replace(",", "", number_format($asist["Salario"], 2)));
            if(strlen($salario[0])<=3)
              $cadena="0".$salario[0].$salario[1];
            else
              $cadena=$salario[0].$salario[1];
            $fech=$fechaayer; 
            $fecha=explode("-",$fech); 
            $abc=$fecha[2].$fecha[1].$fecha[0];                         
            fputs($ar,str_pad($asist['rp'], 11));
            fputs($ar,str_pad($asist['imss'], 11));
            fputs($ar,str_pad(($asist['pat']), 27));
            fputs($ar,str_pad(($asist['mat']), 27));
            fputs($ar,str_pad(($asist['nomb']), 27));            
            fputs($ar,str_pad($cadena, 6));//salario            
            fputs($ar,str_pad('000000', 6));//fillier
            fputs($ar,str_pad($asist["eCodTipoTrabajador"], 1));//tipo de trabajador
            fputs($ar,str_pad($asist["TipoSalario"], 1));//tipo de salario
            fputs($ar,str_pad($asist["tipjo"], 1));//semana jornada
            fputs($ar,str_pad($abc, 8));
            fputs($ar,str_pad('     ', 5)); //clinica            
            fputs($ar,str_pad('07', 2));//movimiento
            fputs($ar,str_pad('03400', 5));//guia
            fputs($ar,str_pad('ESTIBADOR', 10));//estibador
            fputs($ar,str_pad('', 1));//FillierB
            fputs($ar,str_pad(($asist['curp']), 18));//curp
            fputs($ar,str_pad('9', 1)."\r\n"); //indicador
          }
        mysql_free_result($asi);
        fputs($ar,str_pad("*************",56));
        fputs($ar,str_pad($count,6,"0",STR_PAD_LEFT));
        fputs($ar,"                                                                       ");
        fputs($ar,"03400"); 
        fputs($ar,"                             9");       
        fclose($ar); 
        $enlace = $directorio;
        header ("Location: hmov.php");        
    }
    else
    { 
      ?>
      <script type="text/javascript"> 
      alert('Error, no se pudo eliminar el archivo anterior');
      //window.location='../batch/movimientos.php';
      </script>
      <?php
    }
   } 
  else
  {
    $ar=fopen($directorio,"a") or
    die("Problemas en la creacion");
    $count=0;
    while($asist=mysql_fetch_array($asi))
    {
            $count=$count+1;
            $salario=explode(".",str_replace(",", "", number_format($asist["Salario"], 2)));
            if(strlen($salario[0])<=3)
              $cadena="0".$salario[0].$salario[1];
            else
              $cadena=$salario[0].$salario[1];
            $fech=$fechaayer; 
            $fecha=explode("-",$fech); 
            $abc=$fecha[2].$fecha[1].$fecha[0];
            fputs($ar,str_pad($asist['rp'], 11));
            fputs($ar,str_pad($asist['imss'], 11));
            fputs($ar,str_pad(($asist['pat']), 27));
            fputs($ar,str_pad(($asist['mat']), 27));
            fputs($ar,str_pad(($asist['nomb']), 27));            
            fputs($ar,str_pad($cadena, 6));//salario            
            fputs($ar,str_pad('000000', 6));//fillier
            fputs($ar,str_pad($asist["eCodTipoTrabajador"], 1));//tipo de trabajador
            fputs($ar,str_pad($asist["TipoSalario"], 1));//tipo de salario
            fputs($ar,str_pad($asist["tipjo"], 1));//semana jornada           
            fputs($ar,str_pad($abc, 8));
            fputs($ar,str_pad('     ', 5)); //clinica
            fputs($ar,str_pad('07', 2));//movimiento
            fputs($ar,str_pad('03400', 5));//guia
            fputs($ar,str_pad('ESTIBADOR', 10));//estibador
            fputs($ar,str_pad('', 1));//FillierB
            fputs($ar,str_pad(($asist['curp']), 18));//curp
            fputs($ar,str_pad('9', 1)."\r\n"); //indicador
    }
    mysql_free_result($asi);fputs($ar,str_pad("*************",56));
        fputs($ar,str_pad($count,6,"0",STR_PAD_LEFT));
        fputs($ar,"                                                                       ");
        fputs($ar,"03400"); 
        fputs($ar,"                             9"); 
    fclose($ar);
    $enlace = $directorio;
    header ("Location: hmov.php");
    
  } 
  ?>

              