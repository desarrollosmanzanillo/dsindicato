<?php
require_once("../conexion/soluciones-mysql.php");
//$link=Conectarse();
$directorio="ALTAS.dat";

    date_default_timezone_set('America/Mexico_City');
    $fhHoy=isset($_GET["fecha"]) ? $_GET["fecha"] : date("Y-m-d");
    $fhAyer = date('Y-m-d',strtotime('-1 days', strtotime($fhHoy)));

    $select=" SELECT DISTINCT asis.idEmpleado, asis.idEmpresa, asis.folioIMSS, asis.Estado AS tCodEstatus, asis.Fecha, asis.idEmpleado, empl.id, ".
      " empl.Empleado, empl.IMSS AS imss, empl.Paterno AS pat, empl.Materno AS mat, empl.Nombre AS nomb, empl.Clinica AS clin, empl.CURP AS curp, ".
      " caen.tRegistroPatronal AS rp, ctemp.dFactorIntegracion AS FactorIntegracion, cts.eCodSalario AS TipoSalario, ctemp.eCodTipoJornada, ".
      " (SELECT SUM(asi1.Salario+asi1.dBono) FROM asistencias asi1 WHERE asi1.Estado='AC' AND asi1.Fecha='".$fhHoy."' AND asi1.idEmpleado=asis.idEmpleado) ".
      " AS Salario, ctemp.eCodTipoTrabajador AS TipoEmpleado ".
      " FROM asistencias asis ".
      " INNER JOIN empleados empl ON empl.id=asis.idEmpleado ".
      " INNER JOIN cattipoempleado ctemp ON ctemp.eCodTipoEntidad=empl.eCodTipo ".
      " LEFT JOIN catentidades caen ON caen.eCodEntidad=asis.idEmpresa ".
      " LEFT JOIN relasistenciamovimientos ramo ON ramo.eCodAsistencia = asis.id ".
      " LEFT JOIN cattiposalario cts ON cts.eCodSalario=ctemp.eCodTipoSalario ".
      " WHERE asis.Estado='AC' AND asis.Fecha='".$fhHoy."' ".((int)$_POST['eCodTipoEntidad']>0 ? " AND empl.eCodTipo=".(int)$_POST['eCodTipoEntidad'] : "").
      " AND ramo.eCodAsistencia IS NULL ".
      " AND asis.idEmpleado NOT IN (SELECT DISTINCT asi.idEmpleado ".
      "    FROM asistencias AS asi ".
      "    LEFT JOIN relasistenciamovimientos AS rel ON rel.eCodAsistencia = asi.id ".
      "    LEFT JOIN movimientosafiliatorios AS mov ON mov.eCodMovimiento=rel.eCodMovimiento ".
      "    WHERE asi.Estado='AC' AND (asi.fecha = '".$fhAyer."' OR mov.fhFecha ='".$fhAyer."') AND mov.tFolio>'0' ".
      " AND asis.idEmpleado NOT IN (SELECT DISTINCT asi.idEmpleado ".
      "    FROM asistencias AS asi ".
      "    LEFT JOIN relasistenciamovimientos AS rel ON rel.eCodAsistencia = asi.id ".
      "    LEFT JOIN movimientosafiliatorios AS mov ON mov.eCodMovimiento=rel.eCodMovimiento ".
      "    WHERE asi.Estado='AC' AND (asi.fecha = '".$fhAyer."' OR mov.fhFecha ='".$fhAyer."') AND mov.tFolio>'0' ".
      "    AND mov.tCodTipoMovimiento='02')) ";
//print($select);
  $asi=mysql_query($select);
  if(file_exists($directorio))
  {    
    if(unlink($directorio))
    {
    $ar=fopen($directorio,"a") or die("Problemas en la creacion");
    $count=0;
    $select=" SELECT * FROM sisconfiguracion ";
    $configuracion=mysql_fetch_array(mysql_query($select));
        while($asist=mysql_fetch_array($asi))
        {
          $count=$count+1;
                    
                      $salario=(float)$asist{'Salario'};
                      $salario=number_format($salario,2);
                      $salario=explode(".",$salario);
                      if(strlen($salario[0])<=3)
                        $cadena="0".$salario[0].$salario[1];
                      else
                        $cadena=$salario[0].$salario[1];

                      //$cadena= ( (float)$configuracion{'dTopeSalario'}<(float)(str_replace(",", "", $salario[0])+($salario[1]/100)) ? (float)$configuracion{'dTopeSalario'} : (float)(str_replace(",", "", $salario[0])+($salario[1]/100)));
                      $cadena= ( (float)$configuracion{'dTopeSalario'}<(float)(str_replace(",", "", $salario[0])+($salario[1]/100)) ? (float)$configuracion{'dTopeSalario'} : $cadena );
                      $cadena = str_replace(",", "", $cadena );
                      $fech=$asist["Fecha"]; 
                      $fecha=explode("-",$fech); 
                      $abc=$fecha[2].$fecha[1].$fecha[0];

            fputs($ar,str_pad($asist['rp'], 11));
            fputs($ar,str_pad($asist['imss'], 11));
            fputs($ar,str_pad(($asist['pat']), 27));
            fputs($ar,str_pad(($asist['mat']), 27));
            fputs($ar,str_pad(($asist['nomb']), 27));                        
            fputs($ar,str_pad($cadena, 6));//salario
            fputs($ar,str_pad('000000', 6));//fillier
            fputs($ar,str_pad(($asist['TipoEmpleado']), 1));//tipo de trabajador
            fputs($ar,str_pad(($asist['TipoSalario']), 1));//tipo de salario
            fputs($ar,str_pad(($asist['eCodTipoJornada']), 1));//semana jornada            
            fputs($ar,str_pad($abc, 8));
            fputs($ar,str_pad($asist['clin'], 3)); //clinica
            fputs($ar,str_pad('  ', 2)); //Filler (anote blancos)
            fputs($ar,str_pad('08', 2));//movimiento
            fputs($ar,str_pad('03400', 5));//guia
            fputs($ar,str_pad('ESTIBADOR', 10));//estibador
            fputs($ar,str_pad('', 1));//FillierB
            fputs($ar,str_pad(($asist['curp']), 18));//curp
            fputs($ar,str_pad('9', 1)."\r\n"); //indicador
        }
        mysql_free_result($asi);
        fputs($ar,str_pad("*************",56));
        fputs($ar,str_pad($count,6,"0",STR_PAD_LEFT));
        fputs($ar,"                                                                       ");
        fputs($ar,"03400"); 
        fputs($ar,"                             9");       
        fclose($ar); 
        $enlace = $directorio;        
        header ("Location: halta.php");
      
    }
    else
    { 
      ?>
      <script type="text/javascript"> 
      alert('Error, no se pudo eliminar el archivo anterior');
      
      </script>
      <?php
    }
   } 
  else
  {
    $ar=fopen($directorio,"a") or die("Problemas en la creacion");
    $count=0;
    $select=" SELECT * FROM sisconfiguracion ";
    $configuracion=mysql_fetch_array(mysql_query($select));
        while($asist=mysql_fetch_array($asi))
        {
          $count=$count+1;
                    
                      $salario=(float)$asist{'Salario'};
                      $salario=number_format($salario,2);
                      $salario=explode(".",$salario);
                      if(strlen($salario[0])<=3)
                        $cadena="0".$salario[0].$salario[1];
                      else
                        $cadena=$salario[0].$salario[1];
                      //$cadena= ( (float)$configuracion{'dTopeSalario'}<(float)(str_replace(",", "", $salario[0])+($salario[1]/100)) ? (float)$configuracion{'dTopeSalario'} : (float)(str_replace(",", "", $salario[0])+($salario[1]/100)));
                      $cadena= ( (float)$configuracion{'dTopeSalario'}<(float)(str_replace(",", "", $salario[0])+($salario[1]/100)) ? (float)$configuracion{'dTopeSalario'} : $cadena );
                      $cadena = str_replace(",", "", $cadena );
                      $fech=$asist["Fecha"]; 
                      $fecha=explode("-",$fech); 
                      $abc=$fecha[2].$fecha[1].$fecha[0];

            fputs($ar,str_pad($asist['rp'], 11));
            fputs($ar,str_pad($asist['imss'], 11));
            fputs($ar,str_pad(($asist['pat']), 27));
            fputs($ar,str_pad(($asist['mat']), 27));
            fputs($ar,str_pad(($asist['nomb']), 27));                        
            fputs($ar,str_pad($cadena, 6));//salario
            fputs($ar,str_pad('000000', 6));//fillier
            fputs($ar,str_pad(($asist['TipoEmpleado']), 1));//tipo de trabajador
            fputs($ar,str_pad(($asist['TipoSalario']), 1));//tipo de salario
            fputs($ar,str_pad(($asist['eCodTipoJornada']), 1));//semana jornada            
            fputs($ar,str_pad($abc, 8));
            fputs($ar,str_pad($asist['clin'], 3)); //clinica
            fputs($ar,str_pad('  ', 2)); //Filler (anote blancos)
            fputs($ar,str_pad('08', 2));//movimiento
            fputs($ar,str_pad('03400', 5));//guia
            fputs($ar,str_pad('ESTIBADOR', 10));//estibador
            fputs($ar,str_pad('', 1));//FillierB
            fputs($ar,str_pad(($asist['curp']), 18));//curp
            fputs($ar,str_pad('9', 1)."\r\n"); //indicador
        }    
    mysql_free_result($asi);

        fputs($ar,str_pad("*************",56));
        fputs($ar,str_pad($count,6,"0",STR_PAD_LEFT));
        fputs($ar,"                                                                       ");
        fputs($ar,"03400"); 
        fputs($ar,"                             9"); 
    fclose($ar);
    $enlace = $directorio;    
    header ("Location: halta.php");
    
  } 
  ?>

              