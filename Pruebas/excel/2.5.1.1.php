<?php 
require_once("excel/phpexcel.php");
require_once("../conexion/soluciones-mysql.php");

if ($_GET['fhFechaAsistenciaInicio']!=""){
  		$fhFechaAsistenciaInicio = $_GET['fhFechaAsistenciaInicio'].' 00:00:00';
  		$fhFechaAsistenciaFin = ($_GET['fhFechaAsistenciaFin']!="" ? $_GET['fhFechaAsistenciaFin'] : $_GET['fhFechaAsistenciaInicio']).' 23:59:59';
 	}
	$select=" SELECT ast.*, emp.Empleado AS tNombre, cat.Categoria AS categoria, ast.Estado as tCodEstatus, ".
			" mov.tFolio , mov.tCodTipoMovimiento, mov.dImporte, ".
			" ent.tSiglas, tpo.eCodTipoEntidad,  tpo.tNombreCorto, cco.tSiglas AS CentroCosto ".
			" FROM asistencias ast ".
			" LEFT JOIN relmaniobrasbonosasistencias AS rbn ON rbn.eCodAsistencia=ast.id ".
			" LEFT JOIN opemaniobrabono AS omb ON omb.eCodManiobra=rbn.eCodManiobra ".
			" LEFT JOIN catcostos cco ON cco.eCodCosto=ast.eCodCosto ".
			" LEFT JOIN relasistenciamovimientos AS rel ON rel.eCodAsistencia=ast.id	".
			" LEFT JOIN movimientosafiliatorios AS mov ON mov.eCodMovimiento=rel.eCodMovimiento	".
			" LEFT JOIN categorias cat ON cat.indice=ast.idCategorias ".
			" LEFT JOIN empleados emp ON emp.id=ast.idEmpleado ".
			" LEFT JOIN cattipoempleado tpo ON emp.eCodTipo=tpo.eCodTipoEntidad ".
			" LEFT JOIN catentidades ent ON ent.eCodEntidad=ast.idEmpresa ".
			" WHERE 1=1 ".
			($_GET['eCodTipoEntidad'] 			? " AND emp.eCodTipo=".$_GET['eCodTipoEntidad'] 		: "").
			($_GET['eCodAsistencia'] 			? " AND ast.id=".$_GET['eCodAsistencia'] 				: "").			
			($_GET['Empleado'] 				? " AND emp.Empleado like '%".$_GET['Empleado']."%'" 		: "").
			($_GET['fhFechaAsistenciaInicio']	? " AND ast.Fecha BETWEEN '".$fhFechaAsistenciaInicio. "' AND '" .$fhFechaAsistenciaFin."'" : "" ).
			($_GET['eCodTipoCategoria'] 		? " AND ast.idCategorias=".$_GET['eCodTipoCategoria'] 	: "").
			($_GET['eCodTurno'] 				? " AND ast.Turno=".$_GET['eCodTurno'] 					: "").
			($_GET['baja'] 						? " AND ast.folioBaja=".$_GET['baja'] 					: "").
			($_GET['tipoMovimiento'] 			? " AND ast.tipoMovimiento=".$_GET['tipoMovimiento'] 	: "").
			($_GET['EcodEstatus'] 				? " AND ast.Estado='".$_GET['EcodEstatus']."'"			: "").			
			($_POST['eCodCosto'] 				? " AND ast.eCodCosto=".$_POST['eCodCosto']			: "").
			($_GET['folio'] 					? " AND mov.tFolio like '%".$_GET['folio']."%'" 		: "").
			" Order by ast.Fecha DESC , ast.Turno DESC LIMIT ".( $_GET['limite'] ? $_GET['limite']  : "100" );							

$rsSolicitudes=mysql_query($select);
// Create new PHPExcel object
$excel = new PHPExcel();

// Valores a Utilizar
$titulo = "Reporte de Asistencias";
$columna = 'A';
$fila = 5;

// Arreglo con titulos columnas
$titulos = array(
	"Código",	
	"Fecha",
	"Turno",
	"Centro de Costo",
	"Categoria",
	"Salario",
	"C. Empleado",
	"Empleado",
	"T. Empleado",
	"Folio",
	"S.D.I.",
	"Movimiento"	
);

// Ponemos las propiedades del documento
$excel->getProperties()->setCreator("Sindicato - Mzomicrosystems.com")
	  ->setLastModifiedBy("Sindicato - Mzomicrosystems.com")
	  ->setTitle("Sindicato - Mzomicrosystems.com")
	  ->setSubject("Sindicato - Mzomicrosystems.com")
	  ->setDescription("Sindicato - Mzomicrosystems.com")
	  ->setKeywords("Sindicato - Mzomicrosystems.com")
	  ->setCategory("Sindicato - Mzomicrosystems.com");

$select=" SELECT * ".
		" FROM catentidades WHERE eCodEntidad=1";		
$rEmpresa = mysql_fetch_array(mysql_query($select));
// Encabezados del documento
$excel->getActiveSheet()->setCellValue("A1", utf8_encode($rEmpresa{'tNombre'}));
$excel->getActiveSheet()->setCellValue("A2", utf8_encode("Fecha del Reporte: ".date("d/m/Y H:i:s")));
$excel->getActiveSheet()->setCellValue("A3", $titulo);

// Arreglo con formato para los titulos columnas
$frEnc = new PHPExcel_Style();
$frTit = new PHPExcel_Style();
$frSep = new PHPExcel_Style();

$frEnc->applyFromArray(array('fill'	=> array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color'	=> array('rgb' => 'd6d6d6')),'font' => array('bold'=> true)));
$frTit->applyFromArray(array('fill' => array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color'	=> array('rgb' => 'FFD200')),'font'	=> array('color'=> array('rgb' => 'FFFFFF'))));
$frSep->applyFromArray(array('fill' => array('type'	=> PHPExcel_Style_Fill::FILL_SOLID,'color'	=> array('rgb' => 'FFFFFF'))));

$numTitulos = sizeof($titulos);

// Ajuste de anchos de columnas
for($z=1;$z<=$numTitulos;$z++){
	// Ponemos el ancho de la columna
	//$excel->getActiveSheet()->getColumnDimension($columna)->setWidth(14);
	$excel->getActiveSheet()->getColumnDimension($columna)->setAutoSize(true);

	// Pintamos el titulo
	$celda = $columna."".$fila;
	$excel->setActiveSheetIndex(0)->setCellValue($celda, $titulos[$z-1]);
	
	if($z < $numTitulos){
		$columna++;
	}
}

// Formato de celdas
$excel->getActiveSheet()->setSharedStyle($frEnc, "A".$fila.":".$columna.$fila);

// Agrupamos las celdas de los encabezados
for($i=1; $i<=3; $i++){
	$excel->getActiveSheet()->setSharedStyle($frEnc, "A".$i);
	$excel->getActiveSheet()->mergeCells('A'.$i.':'.$columna.''.$i);
}
	
$excel->getActiveSheet()->setSharedStyle($frTit, "A3");
$excel->getActiveSheet()->setSharedStyle($frSep, "A4");
$excel->getActiveSheet()->setSharedStyle($frEnc, "A5");
$excel->getActiveSheet()->getRowDimension('4')->setRowHeight(5);

// Ajustamos formatos
$excel->getActiveSheet()->getStyle("A1")->getFont()->setSize(14);
$excel->getActiveSheet()->getStyle("A1")->getFont()->setBold(true);

$excel->getActiveSheet()->getStyle("A3")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$excel->getActiveSheet()->getStyle("A3")->getFont()->setBold(true);

while($rSolicitud=mysql_fetch_array($rsSolicitudes)){
	// aumentamos primero porque estabamos en fila de encabezados
	$tipoMovimiento="";
	if($rSolicitud{'tCodTipoMovimiento'}=='01')
	{
		$tipoMovimiento="Continuacion";
	}
	if($rSolicitud{'tCodTipoMovimiento'}=='08')
	{
		$tipoMovimiento="Reingreso";
	}
	if($rSolicitud{'tCodTipoMovimiento'}=='07')
	{
		$tipoMovimiento="Modificacion";
	}
	if($rSolicitud{'tCodTipoMovimiento'}=='00')
	{
		$tipoMovimiento="N/D";
	}	
	$fila++;
	$columna = 'A';
	$excel->getActiveSheet()->setCellValue($columna++.$fila, sprintf("%07d",$rSolicitud{'id'}));	
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode(date("d/m/Y", strtotime($rSolicitud{'Fecha'}))));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'Turno'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'CentroCosto'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'categoria'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'Salario'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'idEmpleado'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'tNombre'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'tNombreCorto'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'tFolio'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($rSolicitud{'dImporte'}));
	$excel->getActiveSheet()->setCellValue($columna++.$fila, utf8_encode($tipoMovimiento));	
}
// Cambiamos el nombre de la hoja
$excel->getActiveSheet()->setTitle('Sindicato - Mzomicrosystems.com');

// Ocultamos las lineas de division
$excel->getActiveSheet()->setShowGridlines(false);

// Asignamos la primer hoja como la activa
$excel->setActiveSheetIndex(0);

// Redirect output to a client's web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename=Reporte_Asistencias.xls');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
$objWriter->save('php://output');
exit;
?>