<?php require_once("conexion/soluciones-mysql.php");

if($_POST){

	if($_POST['eProceso']==1){
		$exito				= 1;
		$asistencias		= 0;
		$Turno				= ($_POST['turno']				? $_POST['turno']						: "NULL");
		$idEmpresa			= ($_POST['eCodEmisor']			? $_POST['eCodEmisor']					: "NULL");
		$idUsuario			= ($_POST['eUsuario']			? $_POST['eUsuario']					: "NULL");
		$fhFechaAsistencia	= ($_POST['fhFechaAsistencia']	? "'".$_POST['fhFechaAsistencia']."'"	: "NULL");
		
		$select=" SELECT * ".
				" FROM sisconfiguracion ";
		$configura=mysql_fetch_array(mysql_query($select));
		$fechaasis= date('Y-m-d',strtotime('+1 days', strtotime($_POST['fhFechaAsistencia'])));
		$domingo=date("w",strtotime($fechaasis));
		
		$select=" SELECT eCodDia ".
				" FROM catdiasfestivos ".
				" WHERE ecodEstatus='AC' AND fhFecha=".$fhFechaAsistencia;
		$rFestivo=mysql_fetch_array(mysql_query($select));
		
		foreach($_POST as $k => $v){
			$nombre = strval($k);
			if(strstr($nombre,"eRegistro") && $v && (int)$_POST['idEmpleado'.$v]>0){
				$eFila=str_replace("eRegistro", "", $nombre);

				$area 			= ($_POST['eCodCosto'.$eFila]	? $_POST['eCodCosto'.$eFila]	: "NULL");
				$idEmpleado 	= ($_POST['idEmpleado'.$eFila]	? $_POST['idEmpleado'.$eFila]	: "NULL");
				$idCategorias 	= ($_POST['idCategoria'.$eFila] ? $_POST['idCategoria'.$eFila]	: "NULL");
				$select=" SELECT Turno".$Turno." AS Salario FROM categorias WHERE indice=".(int)$idCategorias;
				$salariocategoria=mysql_fetch_array(mysql_query($select));
				$select=" SELECT empl.descanso, ctipo.* ".
						" FROM empleados empl ".
						" INNER JOIN cattipoempleado ctipo ON ctipo.eCodTipoEntidad=empl.eCodTipo ".
						" WHERE empl.id=".(int)$idEmpleado.
						" AND empl.id NOT IN (SELECT idEmpleado ".
						"					FROM asistencias ".
						"					WHERE Estado='AC' ".
						"					AND Turno=".(int)$Turno." AND Fecha=".$fhFechaAsistencia." AND idEmpleado=".(int)$idEmpleado.") ";
				$factor=mysql_fetch_array(mysql_query($select));
				$salario = (float)$salariocategoria{'Salario'};
				$descanso = (int)$factor{'descanso'};
				if((int)$idCategorias>0){
					$SalarioNeto=($descanso==$domingo && $domingo==1 //Día de Descanso y Domingo
						? ($salario*$factor{'dFactorIntegracion'})+($salario*((float)$configura{'dPrimaDominical'}/100))+$salario 
						: ($descanso==$domingo && $domingo!=1 //Día de Descanso y NO Domingo
							? ($salario*$factor{'dFactorIntegracion'})+$salario 
							: ($descanso!=$domingo && $domingo==1 //NO Día Descanso y Domingo
								? ($salario*$factor{'dFactorIntegracion'})+($salario*((float)$configura{'dPrimaDominical'}/100))
								: $factor{'dFactorIntegracion'}*$salario)))+((int)$factor{'eCodTipoEntidad'}==2 && (int)$rFestivo{'eCodDia'}>0 ? (float)$salario*2 : 0); //Normal
					$insert=" INSERT INTO asistencias (idEmpleado, idCategorias, idEmpresa, area, Turno, Salario, Fecha, ".
							" fechaRegistro, idUsuario, Estado, eCodCosto) ".
							" VALUES (".$idEmpleado.", ".$idCategorias.", ".$idEmpresa.", ".$area.", ".$Turno.", ".(float)$SalarioNeto.", 
							".$fhFechaAsistencia.", CURRENT_TIMESTAMP, ".$idUsuario.", 'AC', ".$area.") ";
					if($res=mysql_query($insert)){
						$cadena.=$insert;
						$asistencias++;
					}else{
						$cadena.=$insert;
						$exito=0;
					}
				}//
			}
		}
		if((int)$Turno==1){
			$diasAtras=7;
			$varfecha=isset($_POST["fhFechaAsistencia"]) ? $_POST["fhFechaAsistencia"] : date("Y-m-d");
			$fechaayer = date('Y-m-d',strtotime('-7 days', strtotime($varfecha)));
			$select=" SELECT * FROM sisconfiguracion ";
			$configura=mysql_fetch_array(mysql_query($select));
			$select=" SELECT Turno3 AS Salario FROM categorias WHERE indice=".(int)$configura{'dCategoriaDescansos'};
			$salario=mysql_fetch_array(mysql_query($select));

			while($diasAtras>1){
				$fechaAntier= date('Y-m-d',strtotime($fechaayer));
				$fechaayer= date('Y-m-d',strtotime('+1 days', strtotime($fechaayer)));
				
				//Asistencia Garantia
				/*Pr$select=" SELECT empl.id, ctipo.dFactorIntegracion ".
						" FROM empleados empl ".
						" INNER JOIN cattipoempleado ctipo ON ctipo.eCodTipoEntidad=empl.eCodTipo ".
						" WHERE empl.eCodTipo=1 AND empl.Estado='AC' AND empl.FechaRegistro<='".$fechaayer."' ".
						" AND empl.descanso<>".(date("w",strtotime($fechaayer))+1).
						" AND empl.id NOT IN (SELECT idEmpleado FROM asistencias WHERE Fecha='".$fechaayer."' AND Estado='AC') ".
						" AND empl.id NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."') ".
						" AND empl.id NOT IN (SELECT eCodEmpleado FROM ausentismo WHERE fhFecha='".$fechaayer."' AND eCodEstatus='AC' AND eCodEmpleado=empl.id)";
				$ausentismos=mysql_query($select);
				while($rausentismo=mysql_fetch_array($ausentismos)){
					$insert=" INSERT INTO asistencias (idEmpleado, idCategorias, idEmpresa, area, Turno, Salario, Fecha, ".
							" fechaRegistro, idUsuario, Estado) ".
							" SELECT ".$rausentismo{'id'}.", ".(int)$configura{'dCategoriaDescansos'}.", 1, 0, 3, ".((float)$salario{'Salario'}*(float)$rausentismo{'dFactorIntegracion'}).", '".$fechaayer."', ".
							" CURRENT_TIMESTAMP, ".$idUsuario.", 'AC' ";
					if($res=mysql_query($insert)){
						$cadena.=$insert;
						$asistencias++;
					}else{
						$cadena.=$insert;
						$exito=0;
					}
				}Pr*/
				//Asistencia Ausentismo
				$select=" SELECT Turno3 AS Salario FROM categorias WHERE indice=".(int)$configura{'dCategoriaAusentismo'};
				$salarioau=mysql_fetch_array(mysql_query($select));
				$select=" SELECT empl.id, ctipo.dFactorIntegracion ".
						" FROM empleados empl ".
						" INNER JOIN cattipoempleado ctipo ON ctipo.eCodTipoEntidad=empl.eCodTipo ".
						" WHERE empl.eCodTipo=1 AND empl.Estado='AC' AND empl.FechaRegistro<='".$fechaayer."' ".
						" AND empl.descanso<>".(date("w",strtotime($fechaayer))+1).
						" AND empl.id NOT IN (SELECT idEmpleado FROM asistencias WHERE Fecha='".$fechaayer."' AND Estado='AC') ".
						" AND empl.id NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."') ".
						" AND empl.id IN (SELECT eCodEmpleado FROM ausentismo WHERE fhFecha='".$fechaayer."' AND eCodEstatus='AC' AND eCodEmpleado=empl.id)";
				$ausentismos=mysql_query($select);
				while($rausentismo=mysql_fetch_array($ausentismos)){
					$insert=" INSERT INTO asistencias (idEmpleado, idCategorias, idEmpresa, area, Turno, Salario, Fecha, ".
							" fechaRegistro, idUsuario, Estado) ".
							" SELECT ".$rausentismo{'id'}.", ".(int)$configura{'dCategoriaAusentismo'}.", 1, 0, 3, 0, '".$fechaayer."', ".
							" CURRENT_TIMESTAMP, ".$idUsuario.", 'AC' ";
					if($res=mysql_query($insert)){
						$cadena.=$insert;
						$asistencias++;
						$select=" select last_insert_id() AS Llave ";
						$rCodigo=mysql_fetch_array(mysql_query($select));
						$eCodAsistencia=(int)$rCodigo['Llave'];
						$select=" SELECT * FROM asistencias WHERE idEmpleado=".$rausentismo{'id'}." AND Fecha='".$fechaAntier."' AND Estado='AC' ORDER BY id  DESC ";
						$salarioantier=mysql_fetch_array(mysql_query($select));
						$select=" SELECT movim.dImporte FROM relasistenciamovimientos asist LEFT JOIN movimientosafiliatorios movim ON movim.eCodMovimiento=asist.eCodMovimiento WHERE asist.eCodAsistencia=".(int)$salarioantier{'id'};
						$salarioausen=mysql_fetch_array(mysql_query($select));
						
						//$insert=" INSERT INTO movimientosafiliatorios (eCodUsuario, fhFecha, tCodTipoMovimiento, eCodEstatus, dImporte, tFolio, fhFechaRegistro) ".
						//		" VALUES (".$idUsuario.", '".$fechaayer."', '01', 'AC', ".(float)$salarioausen{'dImporte'}.", 'NA', CURRENT_TIMESTAMP) ";
						//if($res=mysql_query($insert)){
						//	$cadena.=$insert;
						//	$select=" select last_insert_id() AS Llave ";
						//	$rCodigo=mysql_fetch_array(mysql_query($select));
						//	$eCodMovimiento=(int)$rCodigo['Llave'];
						//	$insertt=" INSERT INTO relasistenciamovimientos (eCodAsistencia, eCodMovimiento) ".
						//			" VALUES (".$eCodAsistencia.", ".$eCodMovimiento.") ";
						//	if($res=mysql_query($insertt)){
						//		$exito=1;
						//	}else{
						//		$exito=0;
						//	}
						//}else{
						//	$cadena.=$insert;
						//	$exito=0;
						//}
					}else{
						$cadena.=$insert;
						$exito=0;
					}
				}
				//Asistencia Incapacidad
				$select=" SELECT Turno3 AS Salario FROM categorias WHERE indice=".(int)$configura{'dCategoriaIncapacidad'};
				$salarioin=mysql_fetch_array(mysql_query($select));
				$select=" SELECT empl.id, ctipo.dFactorIntegracion ".
						" FROM empleados empl ".
						" INNER JOIN cattipoempleado ctipo ON ctipo.eCodTipoEntidad=empl.eCodTipo ".
						" WHERE empl.eCodTipo=1 AND empl.Estado='AC' AND empl.FechaRegistro<='".$fechaayer."' ".
						" AND empl.descanso<>".(date("w",strtotime($fechaayer))+1).
						" AND empl.id NOT IN (SELECT idEmpleado FROM asistencias WHERE Fecha='".$fechaayer."' AND Estado='AC') ".
						" AND empl.id IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."') ".
						" AND empl.id NOT IN (SELECT eCodEmpleado FROM ausentismo WHERE fhFecha='".$fechaayer."' AND eCodEstatus='AC' AND eCodEmpleado=empl.id) ";
				$incapacidades=mysql_query($select);
				while($incapacidad=mysql_fetch_array($incapacidades)){
					$insert=" INSERT INTO asistencias (idEmpleado, idCategorias, idEmpresa, area, Turno, Salario, Fecha, ".
							" fechaRegistro, idUsuario, Estado) ".
							" SELECT ".$incapacidad{'id'}.", ".(int)$configura{'dCategoriaIncapacidad'}.", 1, 0, 3, ".((float)$salarioin{'Salario'}*(float)$incapacidad{'dFactorIntegracion'}).", '".$fechaayer."', ".
							" CURRENT_TIMESTAMP, ".$idUsuario.", 'AC' ";
					if($res=mysql_query($insert)){
						$cadena.=$insert;
						$asistencias++;
						$select=" select last_insert_id() AS Llave ";
						$rCodigo=mysql_fetch_array(mysql_query($select));
						$eCodAsistencia=(int)$rCodigo['Llave'];

						$select=" SELECT mosh.dImporte FROM movimientosafiliatorios mosh ".
								" INNER JOIN relasistenciamovimientos rassih ON rassih.eCodMovimiento = mosh.eCodMovimiento ".
								" INNER JOIN asistencias assih ON assih.id = rassih.eCodAsistencia ".
								" WHERE assih.idEmpleado = ".$incapacidad{'id'}." AND mosh.fhFecha='".$fechaAntier."'";
						$salarioAy=mysql_fetch_array(mysql_query($select));

						$insert=" INSERT INTO movimientosafiliatorios (eCodUsuario, fhFecha, tCodTipoMovimiento, eCodEstatus, dImporte, tFolio, fhFechaRegistro) ".
								" VALUES (".$idUsuario.", '".$fechaayer."', '01', 'AC', ".(float)$salarioAy{'dImporte'}.", 'NA', CURRENT_TIMESTAMP) ";
						if($res=mysql_query($insert)){
							$cadena.=$insert;
							$select=" select last_insert_id() AS Llave ";
							$rCodigo=mysql_fetch_array(mysql_query($select));
							$eCodMovimiento=(int)$rCodigo['Llave'];
							$insertt=" INSERT INTO relasistenciamovimientos (eCodAsistencia, eCodMovimiento) ".
									" VALUES (".$eCodAsistencia.", ".$eCodMovimiento.") ";
							if($res=mysql_query($insertt)){
								$exito=1;
							}else{
								$exito=0;
							}
						}else{
							$cadena.=$insert;
							$exito=0;
						}
					}else{
						$cadena.=$insert;
						$exito=0;
					}
				}
				//
				
				///
				$select=" SELECT empl.id ".
						" FROM empleados empl ".
						" INNER JOIN cattipoempleado ctipo ON ctipo.eCodTipoEntidad=empl.eCodTipo ".
						" WHERE empl.eCodTipo=1 AND empl.Estado='AC' ".
						" AND empl.descanso=".(date("w",strtotime($fechaayer))+1).
						" AND empl.id NOT IN (SELECT idEmpleado FROM asistencias WHERE Fecha='".$fechaayer."' AND Estado='AC') ".
						" AND empl.id NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."') ".
						" AND empl.id NOT IN (SELECT eCodEmpleado FROM ausentismo WHERE fhFecha='".$fechaayer."' AND eCodEstatus='AC' AND eCodEmpleado=empl.id)";
				$descansos=mysql_query($select);
				/*while($rdescanso=mysql_fetch_array($descansos)){
					$dAtrasEmpl=7;
					$varfechaEmpl=$fechaayer;
					$fechaayerEmpl = date('Y-m-d',strtotime('-7 days', strtotime($varfechaEmpl)));
					$select=" SELECT * FROM sisconfiguracion ";
					$configura=mysql_fetch_array(mysql_query($select));
					$salario=0;
					while($diasAtras>1){
						$fechaayerEmpl = date('Y-m-d',strtotime('+1 days', strtotime($fechaayerEmpl)));
						print $fechaayerEmpl."<br>";
						$dAtrasEmpl--;
					}
				}*/
				///
				
				$diasAtras--;
			}
			///
			$select=" SELECT *
					FROM empleados empl
					INNER JOIN cattipoempleado ctipo ON ctipo.eCodTipoEntidad=empl.eCodTipo
					WHERE empl.eCodTipo=1
					AND empl.descanso=".(date("w",strtotime($fechaayer))+1)."
					AND empl.id NOT IN (SELECT idEmpleado FROM asistencias WHERE Fecha='".$fechaayer."' AND Estado='AC')
					AND empl.id NOT IN (SELECT idEmpleado FROM incapacidades WHERE Estado='AC' AND FechaInicial<='".$fechaayer."' AND FechaFinal>='".$fechaayer."')  ";
			$empleados=mysql_query($select);
			while($empl=mysql_fetch_array($empleados)){
				
			}
		}
		print "<input type=\"text\" value=\"".((int)$exito>0 && (int)$asistencias>0 ? 1 : 0)."\" id=\"eCodigo\" name=\"eCodigo\" />";
	}

	if($_POST['eProceso']==2){
		$select=" SELECT id ".
				" FROM asistencias ".
				" WHERE Estado LIKE 'AC' AND Fecha='".$_POST['fhFechaAsistencia']."' AND Turno=".(int)$_POST['turno'].
				" AND idEmpleado=".(int)$_POST['idEmpleado'.(int)$_POST['eFilaEmp']];
		$asistencia=mysql_fetch_array(mysql_query($select));
		$select=" SELECT id ".
				" FROM incapacidades ".
				" WHERE Estado LIKE 'AC' AND idEmpleado=".(int)$_POST['idEmpleado'.(int)$_POST['eFilaEmp']].
				" AND FechaInicial<='".$_POST['fhFechaAsistencia']."' AND FechaFinal>='".$_POST['fhFechaAsistencia']."'";
		$incapacidad=mysql_fetch_array(mysql_query($select));
		print "<input type=\"hidden\" name=\"eEmpleadoBD\" id=\"eEmpleadoBD\" value=\"".(int)$asistencia{'id'}."\">";
		print "<input type=\"hidden\" name=\"eIncapacidad\" id=\"eIncapacidad\" value=\"".(int)$incapacidad{'id'}."\">";
	}
//	
}
if(!$_POST){
$select=" SELECT * ".
		" FROM catentidades ".
		" WHERE eCodEntidad=1 ";
$rsEmisores=mysql_query($select);

$select=" SELECT * ".
		" FROM catcomprobantes WHERE tCodEstatus='AC' ".
		" ORDER BY tNombre ASC ";
$rsComprobantes=mysql_query($select); 

$select=" SELECT * ".
		" FROM catformaspago ".
		" WHERE tCodEstatus='AC' ".
		" ORDER BY tNombre ";
$rsFormasPagos=mysql_query($select);

$select=" SELECT * ".
		" FROM catmetodospago ".
		" WHERE tCodEstatus='AC' ".
		" ORDER BY tNombre ";
$rsMetodosPagos=mysql_query($select);

$select=" SELECT * ".
		" FROM catentidades ".
		" WHERE tCodEstatus='AC' ".
		" AND eCodTipoEntidad=1 ".
		" ORDER BY tNombre ";
$rsClientes=mysql_query($select); 

$select=" SELECT * ".
		" FROM catcostos WHERE tCodEstatus='AC' ".
		" ORDER BY tSiglas ";
$rsCostos=mysql_query($select); 

$select=" SELECT * FROM categorias WHERE tCodEstatus='AC' ORDER BY Categoria ASC ";
$rsCategorias=mysql_query($select); ?>
<style>
.eNumero{
	text-align:right;
}
</style>
<script src="../js/dojo/dijit/form/FilteringSelect.js" type="text/javascript"></script> 
<script src="../js/dojo/dojo/data/ItemFileReadStore.js" type="text/javascript"></script> 
<script src="../js/dojo/dijit/form/FilteringSelect.js" type="text/javascript"></script> 
<script src="../js/dojo/dijit/TooltipDialog.js" type="text/javascript"></script> 
<script type="text/javascript">
dojo.require("dojo.number");
dojo.require("dijit.form.NumberTextBox");
dojo.require("dijit.form.DateTextBox");
dojo.require("dojo.io.iframe");

function guardar(){
	dojo.byId('eProceso').value=1;
	var bandera = false;
	var bServicios = false;
	var bMercancias = false;
	var eTipoSolicitud = 0;
	mensaje = "¡Verifique lo siguiente!\n";

	var ultimafila=0;
	var ultimafilaV=0;

	dojo.query("[id^=\"eRegistro\"]").forEach(function(nodo, index, array){
		ultimafila=parseInt(nodo.value);
		ultimafilaV++;
	});
	if (!dojo.byId("eCodEmisor").value){
		mensaje+="* Registro Patronal\n";
		bandera = true;		
	}
	if (!dojo.byId("turno").value){
		mensaje+="* Turno\n";
		bandera = true;
	}
	if (!dojo.byId("fhFechaAsistencia").value){
		mensaje+="* Fecha de Asistencia\n";
		bandera = true;		
	}else{
		if(parseFloat(dojo.byId("fhFechaAsistencia").value.replace(/-/gi, ""))>parseFloat(dojo.byId('eFDiaria').value)){
			mensaje+="* No Puede Guardar Asistencias Anticipadas\n";
			bandera = true;
		}
	}
	if(parseInt(ultimafilaV)<=1){
		mensaje+="* Asistencias\n";
		bandera = true;		
	}else{
		var bIncompleto=false;
		var bEmpleado=false;
		var bCategoria=false;
		var bCosto=false;
		dojo.query("[id^=\"eRegistro\"]").forEach(function(nodo, index, array){
			var eFilaValida=parseInt(nodo.value);
			if(ultimafila>eFilaValida){
				if(!dijit.byId('idEmpleado'+eFilaValida).value){
					bEmpleado=true;
					bIncompleto=true;
				}
				if(!dojo.byId('idCategoria'+eFilaValida).value){
					bIncompleto=true;
					bCategoria=true;
				}
				if(!dojo.byId('eCodCosto'+eFilaValida).value){
					bCosto=true;
					bIncompleto=true;
				}
			}else{
				if(dijit.byId('idEmpleado'+eFilaValida).value){
					bEmpleado=true;
					bIncompleto=true;
				}
				if(dojo.byId('idCategoria'+eFilaValida).value){
					bIncompleto=true;
					bCategoria=true;
				}
				if(dojo.byId('eCodCosto'+eFilaValida).value){
					bCosto=true;
					bIncompleto=true;
				}
			}
		});
		if(bIncompleto==true){
			mensaje+="* Conceptos incompletos\n";
			if(bEmpleado==true){
				mensaje+="   Empleado\n";
			}
			if(bCategoria==true){
				mensaje+="   Categoría\n";
			}
			if(bCosto==true){
				mensaje+="   Centro de Costo\n";
			}
			bandera = true;
		}
	}

	if (bandera==true){
		alert(mensaje);
	}else{
		if (confirm("¿Desea Guardar la Información?")){
			dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
				dojo.byId("dvCNS").innerHTML = tRespuesta;
				if(dojo.byId("eCodigo").value>0){
					alert("¡La información se guardó correctamente!");
					tURL = './?ePagina=2.5.1.1.php';
					document.location = tURL;
				}else{
					alert("¡Ocurrio un error, favor de intentarlo más tarde!");
				}
			}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
		}
	}
}

function borrarAsistencia(eFilaAsistencia){
	var ultimafila=0;
	dojo.query("[id^=\"eRegistro\"]").forEach(function(nodo, index, array){
		ultimafila=parseInt(nodo.id.replace('eRegistro',''));
	});

	if(eFilaAsistencia!=ultimafila){
		if(confirm("¿Seguro de eliminar la asistencia?")){
			dojo.destroy(dojo.byId('filaAsistencia'+eFilaAsistencia));
		}
	}
}

function verificarEmpleados(){
	dojo.query("[id^=\"idEmpleado\"]").forEach(function(nodo, index, array){
		var filaEmpleado=nodo.id.replace('idEmpleado','');
		if(parseInt(filaEmpleado)>0){
			validarEmpleado(filaEmpleado);
		}
	});
}

function validarEmpleado(fila){
	dojo.byId('eProceso').value=2;
	dojo.byId('eFilaEmp').value=fila;
	dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+".php", load: function(tRespuesta, ioArgs){
		dojo.byId("dvCNS").innerHTML = tRespuesta;
		if(parseInt(dijit.byId('idEmpleado'+fila).value)>0){
			if(parseInt(dojo.byId('eIncapacidad').value)>0){
				alert("¡El empleado cuenta con registro de incapacidad!");
				dojo.byId('idEmpleado'+fila).value="";
				dijit.byId('idEmpleado'+fila).value="";
				dojo.byId('idEmpleado'+fila).focus();
			}else{
				if(parseInt(dojo.byId("eEmpleadoBD").value)>0){
					alert("¡El empleado ya tiene asistencia en el turno!");
					dojo.byId('idEmpleado'+fila).value="";
					dijit.byId('idEmpleado'+fila).value="";
					dojo.byId('idEmpleado'+fila).focus();
				}else{
					validarFila(fila);
				}
			}
		}
	}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});	
}

function agregarConcepto(filaAsistencia){ 
	var tb = dojo.byId('tbAsistencias');
	var tr = null;
	var td = null;
	var fila = 0;
	fila = parseInt(dojo.byId("eFilas").value)+1;
	var rows = document.getElementById('tablaAsistencias').getElementsByTagName('tbody')[0].getElementsByTagName('tr');
    for (i = 0; i < rows.length; i++) {
		if(rows[i].id == "filaAsistencia"+filaAsistencia){
			tr = tb.insertRow(rows[i].rowIndex);
		}
    }

	tr.id = "filaAsistencia"+fila;
	td = tr.insertCell(0);
	td.height = '23px';
	td.className = "sanLR04";
	td.innerHTML="<img width=\"16\" align=\"absmiddle\" height=\"16\" onclick=\"borrarAsistencia("+fila+");\" id=\"filaConcepto"+fila+"\" src=\"https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-borrar.png\" title=\"Eliminar Concepto\">";	
	td.noWrap = "true";

	td = tr.insertCell(1);
	td.className = "sanLR04";
	td.innerHTML='<input type="hidden" name="eRegistro'+fila+'" id="eRegistro'+fila+'" value="'+fila+'"><div dojoType="dojo.data.ItemFileReadStore" jsId="acEmpledos'+fila+'" url="php/auto/empleados.php" clearOnClose="true" urlPreventCache="true"></div><input dojoType="dijit.form.FilteringSelect" value="" jsId="cmbEmpleados'+fila+'" store="acEmpledos'+fila+'" searchAttr="nombre" hasDownArrow="false" name="idEmpleado'+fila+'" id="idEmpleado'+fila+'" autoComplete="false" searchDelay="300" highlightMatch="none" placeHolder="Empleados" style="width:380px;" displayMessage="false" required="false" queryExpr = "*" pageSize="10" onChange="validarEmpleado('+fila+');">';
	td.noWrap = "true";

	td = tr.insertCell(2);
	td.className = "sanLR04";
	td.innerHTML="<select name=\"idCategoria"+fila+"\" id=\"idCategoria"+fila+"\" style=\"width:250px\" onchange=\"validarFila("+fila+");\"><option value=\"\">Seleccione...</option><?php while($rCategoria=mysql_fetch_array($rsCategorias)){ ?><option value=\"<?=$rCategoria{'indice'}?>\" ><?=($rCategoria{'Categoria'})?></option><?php } ?></select>";
	td.noWrap = "true";
	
	td = tr.insertCell(3);
	td.className = "sanLR04";
	td.innerHTML="<select name=\"eCodCosto"+fila+"\" id=\"eCodCosto"+fila+"\" style=\"width:250px\" onchange=\"validarFila("+fila+");\"><option value=\"\">Seleccione...</option><?php while($rCosto=mysql_fetch_array($rsCostos)){ ?><option value=\"<?=$rCosto{'eCodCosto'}?>\" ><?=($rCosto{'tSiglas'})?></option><?php } ?></select>";
	td.noWrap = "true";

	dojo.byId("eFilas").value++;
	dojo.parser.parse('tablaAsistencias');
	asignaEmpleados();
}

function validarFila(filaEmpleado){
	
	var ultimafila=0;
	var bDistinto=true;
	dojo.query("[id^=\"idEmpleado\"]").forEach(function(nodo, index, array){
		ultimafila=nodo.id.replace('idEmpleado','');
		if(ultimafila>0){
			if(ultimafila!=filaEmpleado && dijit.byId(nodo.id).value){
				if(dijit.byId('idEmpleado'+filaEmpleado).value==dijit.byId(nodo.id).value){
					alert('¡Este empleado ya se encuentra seleccionado!');
					dojo.byId('idEmpleado'+filaEmpleado).value="";
					dijit.byId('idEmpleado'+filaEmpleado).value="";
					dojo.byId('idEmpleado'+filaEmpleado).focus();
					bDistinto=false;
				}
			}
		}
	});

	if(bDistinto==true){
		var ultimafila=0;
		var bGenerarLinea=true;
		var eCodTipoServicio=0;
		
		dojo.query("[id^=\"eRegistro\"]").forEach(function(nodo, index, array){
			ultimafila=parseInt(nodo.id.replace('eRegistro',''));
		});
			
		if(filaEmpleado==ultimafila){
			if(dojo.byId('eCodCosto'+filaEmpleado)){
				if(!dojo.byId('eCodCosto'+filaEmpleado).value){
					bGenerarLinea=false;
				}
			}
			if(dojo.byId('idCategoria'+filaEmpleado)){
				if(!dojo.byId('idCategoria'+filaEmpleado).value){
					bGenerarLinea=false;
				}
			}
			if(dojo.byId('idEmpleado'+filaEmpleado)){
				if(!dijit.byId('idEmpleado'+filaEmpleado).value){
					bGenerarLinea=false;
				}
			}
			if(bGenerarLinea==true){
				agregarConcepto(filaEmpleado);
			}
		}
	}
}

function consultar(){
	document.location = './?ePagina=2.5.1.1.php';
}

function listaempleados(codigo){
	var id = codigo.replace("idEmpleado","")
	var busqueda = dojo.byId(codigo).value;
	var acEmp='acEmpledos'+id;
	var cmbEmp='cmbEmpleados'+id;
	eval(acEmp).url = "php/auto/empleados.php?busqueda="+busqueda;
	eval(acEmp).close();
	eval(cmbEmp).store=eval(acEmp);
}

function asignaEmpleados(){
	dojo.query("[id*=\"idEmpleado\"]").forEach(function(nodo, index, array){
		dojo.connect(dijit.byId(nodo.id), "onKeyUp", function(event){
			if(event.keyCode!=40&&event.keyCode!=38){
				listaempleados(nodo.id);
			}
		});
	});
}

dojo.addOnLoad(function(){
	dojo.parser.parse('tablaAsistencias');
	asignaEmpleados();
});
</script>
<div id="dvCNS" style="display:none;"></div>
<form id="Datos" name="Datos" method="post" onSubmit="return false;" enctype="multipart/form-data">
<input type="hidden" name="eProceso" id="eProceso" value="" />
<input type="hidden" name="eServicio" id="eServicio" value="" />
<input type="hidden" name="eUsuario" id="eUsuario" value="<?=(int)$_SESSION['sesionUsuario']['eCodUsuario'];?>" />
<input type="hidden" name="eCodEntidad" id="eCodEntidad" value="<?=(int)$_SESSION['sesionUsuario']['eCodEntidad'];?>" />
<input type="hidden" name="mail" id="mail" value="<?=$_SESSION['sesionUsuario']['Correo'];?>" />
<input type="hidden" name="nombreusuario" id="nombreusuario" value="<?=$_SESSION['sesionUsuario']['Usuario'];?>" />
<input type="hidden" name="eFilaEmp" id="eFilaEmp" value="" />
<input type="hidden" name="eFDiaria" id="eFDiaria" value="<?=date('Ymd');?>" />
<div id="dvFacturacion"></div>
<table border="0" cellpadding="0" cellspacing="0" width="980px">
	 <tr>
        <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Registro Patronal </td>
        <td nowrap class="sanLR04" colspan="3">
        	<select name="eCodEmisor" id="eCodEmisor" style="width:300px">
                <?php while($rEmisor=mysql_fetch_array($rsEmisores)){ ?>
                    <option value="<?=$rEmisor{'eCodEntidad'}?>" ><?=($rEmisor{'tNombre'}." - ".$rEmisor{'tRegistroPatronal'} )?></option>
                <?php } ?>
            </select>
        </td>
    </tr>     
    <tr>
        <td height="23" nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle"> Turno </td>
        <td width="50%" nowrap class="sanLR04">
        	<select name="turno" id="turno" style="width:175px" onChange="verificarEmpleados();">
                <option value="">Seleccione...</option>
                <option value="1">Turno 1</option>
                <option value="2">Turno 2</option>
                <option value="3">Turno 3</option>                
            </select>
        </td>
        <td nowrap class="sanLR04"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" width="16" height="16" align="absmiddle"> Fecha de Asistencia </td>
        <td width="50%" nowrap class="sanLR04">
			<input name="fhFechaAsistencia" type="date" dojoType="dijit.form.TextBox" id="fhFechaAsistencia" value="<?=date('Y-m-d');?>" style="width:140px" onChange="verificarEmpleados();">	
        </td>
    </tr>
    <tr>
        <td height="23" nowrap class="sanLR04"></td>
        <td width="50%" nowrap class="sanLR04"></td>
        <td nowrap class="sanLR04"></td>
        <td width="50%" nowrap class="sanLR04"></td>
    </tr>    
    <?php mysql_data_seek($rsCostos,0);?>
    <?php mysql_data_seek($rsCategorias,0);?>
    <tr>
    	<td colspan="4">
            <table cellspacing="0" border="0" width="100%" id="tablaAsistencias" name="tablaAsistencias">
                <thead>
                    <tr class="thEncabezado">
                        <td nowrap="nowrap" class="sanLR04" height="23"> </td>
                        <td nowrap="nowrap" class="sanLR04" height="23"> Empleado</td>
                        <td nowrap="nowrap" class="sanLR04" height="23"> Categor&Iacute;a</td>
                        <td nowrap="nowrap" class="sanLR04" width="100%"> Centro de Costo</td>                        
                    </tr>
                </thead>
                <tbody id="tbAsistencias" name="tbAsistencias">
                    <tr id="filaAsistencia1" name="filaAsistencia1">
                        <td nowrap="nowrap" class="sanLR04" height="23"><img width="16" align="absmiddle" height="16" onclick="borrarAsistencia(1);" id="filaConcepto1" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-borrar.png" title="Eliminar Concepto"></td>
                        <td nowrap="nowrap" class="sanLR04" height="23"><input type="hidden" name="eRegistro1" id="eRegistro1" value="1">
                        <div dojoType="dojo.data.ItemFileReadStore" jsId="acEmpledos1" url="php/auto/empleados.php" clearOnClose="true" urlPreventCache="true"></div>
                        <input dojoType="dijit.form.FilteringSelect" 
                            value="<?=$rDepositoMovimiento['eCodSucursalCliente'];?>"
                            jsId="cmbEmpleados1"
                            store="acEmpledos1"
                            searchAttr="nombre"
                            hasDownArrow="false"
                            name="idEmpleado1"
                            id="idEmpleado1"
                            autoComplete="false"
                            searchDelay="300"
                            highlightMatch="none"
                            placeHolder="Empleados" 
                            style="width:380px;"
                            displayMessage="false"
                            required="false"
                            queryExpr = "*"
                            pageSize="10"
                            onChange="validarEmpleado(1);"
                            >
                        </td>
                        <td nowrap="nowrap" class="sanLR04">
                        <select name="idCategoria1" id="idCategoria1" style="width:250px" onchange="validarFila(1);">
                        	<option value="">Seleccione...</option>
							<?php while($rCategoria=mysql_fetch_array($rsCategorias)){ ?>
                            	<option value="<?=$rCategoria{'indice'}?>"><?=($rCategoria{'Categoria'})?></option>
							<?php } ?>
                        </select>
                        </td>
                        <td nowrap="nowrap" class="sanLR04">
                        <select name="eCodCosto1" id="eCodCosto1" style="width:250px" onchange="validarFila(1);">
                        	<option value="">Seleccione...</option>
							<?php while($rCosto=mysql_fetch_array($rsCostos)){ ?>
                            	<option value="<?=$rCosto{'eCodCosto'}?>"><?=($rCosto{'tSiglas'})?></option>
							<?php } ?>
                        </select>
                        </td>
                    </tr>
                </tbody>
            </table>
            <input name="eFilas" id="eFilas" value="1" type="hidden">
        </td>
    </tr>
	<tr><td height="20"></td></tr>
    <tr><td valign="top" nowrap="nowrap" class="sanLR04" colspan="4"><img src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/obligatorio.png" align="absmiddle" /> <font class="fntG10K">Indica que es obligatorio llenar el campo.</font></td></tr>
</table>
</form>
<?php } ?>