<?php
require_once("conexion/soluciones-mysql.php");
if($_POST){

	if ($_POST['fhFechaAsistenciaInicio']!=""){
  		$fhFechaAsistenciaInicio = $_POST['fhFechaAsistenciaInicio'].' 00:00:00';
  		$fhFechaAsistenciaFin = ($_POST['fhFechaAsistenciaFin']!="" ? $_POST['fhFechaAsistenciaFin'] : $_POST['fhFechaAsistenciaInicio']).' 23:59:59';
 	}
	$select=" SELECT ast.id, ast.idEmpleado, ast.idCategorias, ast.idEmpresa, ast.Turno, ast.Salario, ast.Fecha, ast.dBono, ast.Estado, ".
			" emp.Empleado AS tNombre, cat.Categoria AS categoria, ast.Estado as tCodEstatus, bon.tNombre AS tipobono, ".
			" mov.tFolio , mov.tCodTipoMovimiento, mov.dImporte, ".
			" ent.tSiglas, tpo.eCodTipoEntidad,  tpo.tNombreCorto, cco.tSiglas AS CentroCosto ".
			" FROM asistencias ast ".
			" LEFT JOIN relmaniobrasbonosasistencias AS rbn ON rbn.eCodAsistencia=ast.id ".
			" LEFT JOIN opemaniobrabono AS omb ON omb.eCodManiobra=rbn.eCodManiobra ".
			" LEFT JOIN cattipobono AS bon ON bon.eCodBono=omb.eCodBono ".
			" LEFT JOIN relasistenciamovimientos AS rel ON rel.eCodAsistencia=ast.id	".
			" LEFT JOIN movimientosafiliatorios AS mov ON mov.eCodMovimiento=rel.eCodMovimiento	AND mov.eCodEstatus='AC' ".
			" LEFT JOIN categorias cat ON cat.indice=ast.idCategorias ".
			" LEFT JOIN empleados emp ON emp.id=ast.idEmpleado ".
			" LEFT JOIN cattipoempleado tpo ON emp.eCodTipo=tpo.eCodTipoEntidad ".
			" LEFT JOIN catentidades ent ON ent.eCodEntidad=ast.idEmpresa ".
			" LEFT JOIN catcostos cco ON cco.eCodCosto=ast.eCodCosto ".
			" WHERE 1=1 ".
			($_POST['eCodTipoEntidad'] 			? " AND emp.eCodTipo=".$_POST['eCodTipoEntidad'] 				: "").
			($_POST['eCodAsistencia'] 			? " AND ast.id=".$_POST['eCodAsistencia'] 				: "").			
			($_POST['Empleado'] 					? " AND emp.Empleado like '%".$_POST['Empleado']."%'" 	: "").
			($_POST['fhFechaAsistenciaInicio']	? " AND ast.Fecha BETWEEN '".$fhFechaAsistenciaInicio. "' AND '" .$fhFechaAsistenciaFin."'" : "" ).
			($_POST['eCodTipoCategoria'] 		? " AND ast.idCategorias=".$_POST['eCodTipoCategoria'] 	: "").
			($_POST['eCodTurno'] 				? " AND ast.Turno=".$_POST['eCodTurno'] 				: "").
			($_POST['baja'] 						? " AND ast.folioBaja=".$_POST['baja'] 					: "").
			($_POST['tipoMovimiento'] 			? " AND mov.tCodTipoMovimiento='".$_POST['tipoMovimiento']."'" 	: "").
			($_POST['EcodEstatus'] 				? " AND ast.Estado='".$_POST['EcodEstatus']."'"			: "").			
			($_POST['eCodCosto'] 				? " AND ast.eCodCosto=".$_POST['eCodCosto']			: "").			
			($_POST['folio'] 					? " AND mov.tFolio like '%".$_POST['folio']."%'" 		: "").
			" Order by ast.Fecha ".( $_POST['orden'] ? $_POST['orden']  : "DESC" )." , ast.Turno DESC LIMIT ".( $_POST['limite'] ? $_POST['limite']  : "100" );			
	$rsSolicitudes=mysql_query($select);	
	$registros=(int)mysql_num_rows($rsSolicitudes);
	?>
    <table cellspacing="0" border="0" width="965px">
		<tr>
			<td width="50%"><hr color="#666666" /></td>
			<td class="sanLR04" nowrap="nowrap"><b>Registros Encontrados: ( <?=$registros;?> )</b></td>
			<td width="50%"><hr color="#666666" /></td>
		</tr>
    </table>
	<div style="display:block; top:0; left:0; width:965px; z-index=1; overflow: auto; height:400px;">
	<table cellspacing="0" border="0" width="965px">
		<thead>
			<tr class="thEncabezado">
				<td nowrap="nowrap" class="sanLR04" height="20" align="center">E</td>
				<td nowrap="nowrap" class="sanLR04">C&oacute;digo</td>				
				<td nowrap="nowrap" class="sanLR04">Fecha</td>
				<td nowrap="nowrap" class="sanLR04">Turno</td>
				<td nowrap="nowrap" class="sanLR04">Centro de Costo</td>
				<td nowrap="nowrap" class="sanLR04" >Categoria</td>
				<td nowrap="nowrap" class="sanLR04" >Salario</td>
				<!--td nowrap="nowrap" class="sanLR04" >Bono</td>
				<td nowrap="nowrap" class="sanLR04" >T. Bono</td-->
				<td nowrap="nowrap" class="sanLR04" title="Clave de Empleado">C. Empleado</td>
				<td nowrap="nowrap" class="sanLR04" >Empleado</td>
				<td nowrap="nowrap" class="sanLR04" >T. Empleado</td>
				<td nowrap="nowrap" class="sanLR04" >Folio</td>
				<td nowrap="nowrap" class="sanLR04" >S.D.I.</td>
				<td nowrap="nowrap" class="sanLR04" width="100%">Movimiento</td>				
			</tr>
		</thead>
		<tbody>
			<?php $i=1; while($rSolicitud=mysql_fetch_array($rsSolicitudes)){ 
				$tipoMovimiento="";
				if($rSolicitud{'tCodTipoMovimiento'}=='01')
				{
					$tipoMovimiento="Continuaci&oacute;n";
				}
				if($rSolicitud{'tCodTipoMovimiento'}=='08')
				{
					$tipoMovimiento="Reingreso";
				}
				if($rSolicitud{'tCodTipoMovimiento'}=='07')
				{
					$tipoMovimiento="Modificaci&oacute;n";
				}
				if($rSolicitud{'tCodTipoMovimiento'}=='00')
				{
					$tipoMovimiento="N/D";
				}
				if($rSolicitud{'tCodTipoMovimiento'}=='02')
				{
					$tipoMovimiento="Baja";
				}

				?>
				<tr>
					<td nowrap="nowrap" class="sanLR04" height="20" align="center"><img width="16" height="16" alt="Empresa" src="https://sindicato-ec2.s3.us-west-2.amazonaws.com/resources/images/png/ic-<?=$rSolicitud{'Estado'};?>.png"></td>
					<td nowrap="nowrap" class="sanLR04 colmenu"><b><?if(is_null($rSolicitud{'tCodTipoMovimiento'}) AND ($rSolicitud{'Estado'}=='AC')){?><a href="?ePagina=2.5.1.1.1.php&eCodAsistencia=<?=$rSolicitud{'id'};?>"><?}?><?=sprintf("%07d",$rSolicitud{'id'});?></b><?if (is_null($rSolicitud{'tCodTipoMovimiento'})){?></a><?}?></td>					
					<td nowrap="nowrap" class="sanLR04 columnB"><a href="?ePagina=2.5.1.1.2.php&eCodAsistencia=<?=$rSolicitud{'id'};?>"><?=date("d/m/Y", strtotime($rSolicitud{'Fecha'}));?></a></td>
					<td nowrap="nowrap" class="sanLR04"><?=utf8_encode($rSolicitud{'Turno'});?></td>
					<td nowrap="nowrap" class="sanLR04 columnB"><?=utf8_encode($rSolicitud{'CentroCosto'});?></td>
					<td nowrap="nowrap" class="sanLR04" ><?=utf8_encode($rSolicitud{'categoria'});?></td>
					<td nowrap="nowrap" class="sanLR04 columnB" ><?=utf8_encode(number_format($rSolicitud{'Salario'},2));?></td>
					<!--td nowrap="nowrap" class="sanLR04 columnB" >< ?=utf8_encode(number_format($rSolicitud{'dBono'},2));?></td>
					<td nowrap="nowrap" class="sanLR04 " >< ?=utf8_encode($rSolicitud{'tipobono'});?></td-->
					<td nowrap="nowrap" class="sanLR04" ><?=utf8_encode($rSolicitud{'idEmpleado'});?></td>
					<td nowrap="nowrap" class="sanLR04 columnB" ><?=utf8_encode($rSolicitud{'tNombre'});?></td>
					<td nowrap="nowrap" class="sanLR04" ><?=utf8_encode($rSolicitud{'tNombreCorto'});?></td>
					<td nowrap="nowrap" class="sanLR04 columnB" ><?=utf8_encode($rSolicitud{'tFolio'});?></td>
					<td nowrap="nowrap" class="sanLR04" ><?=utf8_encode($rSolicitud{'dImporte'});?></td>
					<td nowrap="nowrap" class="sanLR04 columnB" width="100%"><?=utf8_encode($tipoMovimiento);?></td>					
				</tr>
			<?php $i++; } ?>
		</tbody>
	</table>
	</div>
<?php }else{ 
	$select=" SELECT * ".
			" FROM categorias ".
			" WHERE tCodEstatus='AC' order by Categoria ";
	$rsTiposCategorias=mysql_query($select);
	$select=" SELECT * ".
			" FROM catcostos ".
			" WHERE tCodEstatus='AC' order by tSiglas ";
	$rsCentrosCostos=mysql_query($select);
	$select=" SELECT * ".
			" FROM cattipoempleado ".
			" WHERE tCodEstatus='AC' ".
			" ORDER BY tNombreCorto ";
	$rsTiposEmpleados=mysql_query($select);
	
	$FechaHoy=date("Y-m-d");
	$fechaayer=date('Y-m-d',strtotime('-5 days', strtotime($FechaHoy)));
	?>
	<script type="text/javascript">
	dojo.require("dojo.number");
	dojo.require("dijit.form.NumberTextBox");
	dojo.require("dijit.form.DateTextBox");
	dojo.require("dojo.io.iframe");
	
	function nuevo(){
		document.location = "?ePagina=2.4.1.1.php";
	}

	function generaExcel(){
		var UrlExcel = "php/excel/2.5.1.1.php?"+
		(dojo.byId('eCodAsistencia').value ? "&eCodAsistencia="+dojo.byId('eCodAsistencia').value : "")+
		(dojo.byId('fhFechaAsistenciaInicio').value ? "&fhFechaAsistenciaInicio="+dojo.byId('fhFechaAsistenciaInicio').value : "")+
		(dojo.byId('fhFechaAsistenciaFin').value ? "&fhFechaAsistenciaFin="+dojo.byId('fhFechaAsistenciaFin').value : "")+
		(dojo.byId('eCodTipoCategoria').value ? "&eCodTipoCategoria="+dojo.byId('eCodTipoCategoria').value : "")+
		(dojo.byId('eCodTurno').value ? "&eCodTurno="+dojo.byId('eCodTurno').value : "")+
		(dojo.byId('Empleado').value ? "&Empleado="+dojo.byId('Empleado').value : "")+		
		(dojo.byId('eCodTipoEntidad').value ? "&eCodTipoEntidad="+dojo.byId('eCodTipoEntidad').value : "")+		
		(dojo.byId('folio').value ? "&folio="+dojo.byId('folio').value : "")+		
		(dojo.byId('tipoMovimiento').value ? "&tipoMovimiento="+dojo.byId('tipoMovimiento').value : "")+
		(dojo.byId('EcodEstatus').value ? "&EcodEstatus="+dojo.byId('EcodEstatus').value : "")+
		(dojo.byId('eCodCosto').value ? "&eCodCosto="+dojo.byId('eCodCosto').value : "")+
		(dojo.byId('orden').value ? "&orden="+dojo.byId('orden').value : "")+
		(dojo.byId('limite').value ? "&limite="+dojo.byId('limite').value : "");		
		var Datos = document.createElement("FORM");
		document.body.appendChild(Datos);
		Datos.name='Datos';
		Datos.method = "POST";
		Datos.action = UrlExcel;
		Datos.submit();
	}

	function rDiaria(){
		var UrlExcel = "php/excel/2.5.1.1.a.php?"+
		(dojo.byId('eCodAsistencia').value ? "&eCodAsistencia="+dojo.byId('eCodAsistencia').value : "")+
		(dojo.byId('fhFechaAsistenciaInicio').value ? "&fhFechaAsistenciaInicio="+dojo.byId('fhFechaAsistenciaInicio').value : "")+
		(dojo.byId('fhFechaAsistenciaFin').value ? "&fhFechaAsistenciaFin="+dojo.byId('fhFechaAsistenciaFin').value : "")+
		(dojo.byId('eCodTipoCategoria').value ? "&eCodTipoCategoria="+dojo.byId('eCodTipoCategoria').value : "")+
		(dojo.byId('eCodTurno').value ? "&eCodTurno="+dojo.byId('eCodTurno').value : "")+
		(dojo.byId('Empleado').value ? "&Empleado="+dojo.byId('Empleado').value : "")+		
		(dojo.byId('eCodTipoEntidad').value ? "&eCodTipoEntidad="+dojo.byId('eCodTipoEntidad').value : "")+		
		(dojo.byId('folio').value ? "&folio="+dojo.byId('folio').value : "")+		
		(dojo.byId('tipoMovimiento').value ? "&tipoMovimiento="+dojo.byId('tipoMovimiento').value : "")+
		(dojo.byId('EcodEstatus').value ? "&EcodEstatus="+dojo.byId('EcodEstatus').value : "")+
		(dojo.byId('eCodCosto').value ? "&eCodCosto="+dojo.byId('eCodCosto').value : "")+
		(dojo.byId('orden').value ? "&orden="+dojo.byId('orden').value : "")+
		(dojo.byId('limite').value ? "&limite="+dojo.byId('limite').value : "");		
		var Datos = document.createElement("FORM");
		document.body.appendChild(Datos);
		Datos.name='Datos';
		Datos.method = "POST";
		Datos.action = UrlExcel;
		Datos.submit();
	}
	
	dojo.addOnLoad(function(){filtrarConsulta();});
	</script>
	<form name="Datos" id="Datos" method="post" action="" onsubmit="return false;">
		<input type="hidden" value="0" name="ePagina" id="ePagina" />
		<input type="hidden" value="" name="eAccion" id="eAccion" />
		<input type="hidden" value="<?=(int)$_SESSION['sesionUsuario']['eCodEntidad'];?>" name="eEntidad" id="eEntidad" />
		<table width="965px" border="0">
			<tr>
				<td colpan="3" width="100%"></td>
				<td align="right" nowrap="nowrap"><a class="fntBR11" href="javaScript:mostrarFiltros('Busqueda');">Busqueda de Registros</a></td>
			</tr>
			<tr id="trBusqueda" style="display:none">
				<td colspan="4">
					<table width="100%"  bgcolor="#f9f9f9">
						<tr><td class="sanLR04" height="5"></td></tr>
						<tr>
							<td class="sanLR04" height="20">C&oacute;digo</td>
							<td class="sanLR04" width="40%"><input type="text" name="eCodAsistencia" dojoType="dijit.form.TextBox" id="eCodAsistencia" value="" style="width:80px; height:25px;"></td>
							<td class="sanLR04" nowrap="nowrap">Fecha de Asistencia</td>
							<td class="sanLR04" width="50%">
							<input name="fhFechaAsistenciaInicio" id="fhFechaAsistenciaInicio" type="date"  required="false" value="<?=$fechaayer;?>" /> -
							<input name="fhFechaAsistenciaFin" id="fhFechaAsistenciaFin" type="date"  required="false" value="<?=$FechaHoy;?>"  />
							</td>
						</tr>
						<tr>
							<td class="sanLR04" height="20" nowrap="nowrap">Tipo de Categoria</td>
							<td class="sanLR04" width="50%">
								<select name="eCodTipoCategoria" id="eCodTipoCategoria" style="width:180px; height:25px;">
									<option value="">Seleccione...</option>
									<?php while($rTipoCategoria=mysql_fetch_array($rsTiposCategorias)){ ?>
										<option value="<?=$rTipoCategoria{'indice'}?>"><?=utf8_encode($rTipoCategoria{'Categoria'})?></option>
									<?php } ?>
								</select>
							</td>
							<td class="sanLR04" height="20">Turno</td>
							<td class="sanLR04" width="50%"><select name="eCodTurno" id="eCodTurno" style="width:142px; height:25px;">
									<option value="">Seleccione...</option>
									<option value="1">Turno 1</option>
									<option value="2">Turno 2</option>
									<option value="3">Turno 3</option>									
								</select></td>							
						</tr>
						<tr>
							<td class="sanLR04" height="20">Empleado</td>
							<td class="sanLR04" width="50%"><input type="text" name="Empleado" dojoType="dijit.form.TextBox" id="Empleado" value="" style="width:180px; height:25px;"></td>
							<td class="sanLR04" height="20">Folio</td>
							<td class="sanLR04" width="50%"><input type="text" name="folio" dojoType="dijit.form.TextBox" id="folio" value="" style="width:142px; height:25px;"></td>
						</tr>
						<tr>
							<td class="sanLR04" height="20">Estatus</td>
							<td class="sanLR04" width="50%"><select name="EcodEstatus" id="EcodEstatus" style="width:142px; height:25px;">
									<option value="">Seleccione...</option>
									<option value="AC" selected>Activo</option>
									<option value="CA">Cancelado</option>									
								</select></td>
							<td class="sanLR04" height="20">Tipo Movimiento</td>
							<td class="sanLR04" width="50%"><select name="tipoMovimiento" id="tipoMovimiento" style="width:142px; height:25px;">
									<option value="">Seleccione...</option>
									<option value="01">Continuacion</option>
									<option value="07">Modificacion</option>
									<option value="08">Reingreso</option>									
								</select></td>
						</tr>
						<tr>
							<td class="sanLR04" height="20">Centro de Costo</td>
							<td class="sanLR04" width="50%"><select name="eCodCosto" id="eCodCosto" style="width:142px; height:25px;">
									<option value="">Seleccione...</option>
									<?php while($rCentroCosto=mysql_fetch_array($rsCentrosCostos)){ ?>
										<option value="<?=$rCentroCosto{'eCodCosto'}?>"><?=utf8_encode($rCentroCosto{'tSiglas'})?></option>
									<?php } ?>
								</select></td>
                            <td class="sanLR04" height="20">Tipo de Empleado</td>
                            <td class="sanLR04" width="50%">
                                <select name="eCodTipoEntidad" id="eCodTipoEntidad" style="width:180px; height:25px;">
                                	<option value="">Seleccione...</option>
                                    <?php while($rTipoEmpleado=mysql_fetch_array($rsTiposEmpleados)){ ?>
                                        <option value="<?=$rTipoEmpleado{'eCodTipoEntidad'}?>"><?=utf8_encode($rTipoEmpleado{'tNombreCorto'})?></option>
                                    <?php } ?>
                                </select>
                            </td>
						</tr>						
						<tr>
			              <td nowrap="nowrap" class="sanLR04" height="5"><b>[Ordenamiento]</b></td>
			              <td nowrap="nowrap" class="sanLR04" width="50%"><input type="radio" name="orden" id="orden"  value="ASC" > ASCENDENTE / <input type="radio" name="orden" id="orden" checked="checked" value="DESC" > DESENDENTE</td>
			              
			              <td nowrap="nowrap" class="sanLR04"><b>[Maximos Registros]:</b></td>
			              <td nowrap="nowrap" class="sanLR04" width="50%">
			              	<select name="limite" id="limite" style="width:142px; height:25px;">
                                  <option value="100" selected>100</option>
                                  <option value="1000" >1,000</option>
                                  <option value="10000" >10,000</option>
                                  <option value="100000">100,000</option>
                          	</select>
                          </td>
            			</tr>
						<tr><td class="sanLR04" height="5"></td></tr>
					</table>
				</td>
			</tr>
			<tr><td colspan="4"><div id="dvCNS"></div></td></tr>
		</table>
	</form>
<?php } ?>