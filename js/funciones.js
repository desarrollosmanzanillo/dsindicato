﻿function iniciarSesion(){
	//procesoGuardar(true);
	dojo.xhrPost({url: "php/conexion/inicio_sesion.php", load: function(tRespuesta, ioArgs){
		alert(tRespuesta);
		if(parseInt(tRespuesta)==1){
			setTimeout("document.location='../?ePagina=2.1'",300);
		}else{
			//procesoGuardar(false);					  							
			alert('¡Nombre de usuario o contraseña incorrecta!');
		}
	}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:'inicioSesion'});			
}

function filtrarConsulta(){
document.Datos.ePagina.value = 0;
	dojo.byId('dvCNS').innerHTML = "Cargando..."; 
	dojo.xhrPost({url: "php/"+dojo.byId('ePagina').value+'.php', handleAs: "text",load: function(tRespuesta, ioArgs){
		dojo.byId('dvCNS').innerHTML = tRespuesta; 
	}, error: function(tRespuesta, ioArgs) {return tRespuesta;}, form:"Datos"});
}

function mostrarFiltros(){
	if(dojo.byId('trBusqueda').style.display=="none"){
		dojo.byId('trBusqueda').style.display='';
	}else{
		dojo.byId('trBusqueda').style.display='none';
	}
}

function cerrarSesion(){
	if (confirm("¿Desea cerrar la sesión?")==1){
		document.location = "../?eCerrar=1";
		//document.location = "?ePagina=1.5.php"; 
	}
}

function padCero(n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function mousePos(e) {
    e = e || window.event;
    var cursor = {x:0, y:0};
    if (e.pageX || e.pageY) {
        cursor.x = e.pageX;
        cursor.y = e.pageY;
    }else{
        cursor.x = e.clientX + 
            (document.documentElement.scrollLeft || 
            document.body.scrollLeft) - 
            document.documentElement.clientLeft;
        cursor.y = e.clientY + 
            (document.documentElement.scrollTop || 
            document.body.scrollTop) - 
            document.documentElement.clientTop;
    }
	cursor.x;
	cursor.y;
	
    return cursor;
}

function valorRadio(elemento){
	if(elemento!=undefined){
		if(elemento.length==undefined){
			return elemento.value;			
		}else{
			for (var i=0; i < elemento.length; i++){
				if (elemento[i].checked){
					return elemento[i].value;
				}
			}
		}
	}
	return 0;
}